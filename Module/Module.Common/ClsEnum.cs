﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Module.Common
{
    /// <summary>
    /// hoạt động người dùng đơn khởi kiện
    /// </summary>
    public class ENUM_HD_NGUOIDUNG_DKK
    {
        public const string DANGNHAP = "LOGIN";
        public const string DANGXUAT = "LOGOUT";
        public const string LOCK_ACCOUNT = "LOCK_ACCOUNT";
        public const string ACTIVE_ACCOUNT = "ACTIVE_ACCOUNT";

        public const string GUIDONKKMOI = "GUIDONKKMOI";
        public const string SAVEDONKK = "SAVEDONKK";
        public const string GUIBOSUNGDONKK = "GUIBOSUNGDONKK";
        public const string XOADONKK = "XOADONKK";

        public const string THEM = "THEM";
        public const string SUA = "SUA";
        public const string XOA = "XOA";

        public const string THEMDUONGSU = "THEMDUONGSU";
        public const string SUADUONGSU = "SUADUONGSU";
        public const string XOADUONGSU = "XOADUONGSU";

        public const string THEMTAILIEU = "THEMTAILIEU";
        public const string XOATAILIEU = "XOATAILIEU";

        public const string DKNHANVB = "DKNHANVB";
        public const string XOADKNHANVB = "XOADKNHANVB";
        public const string LOCKDKNHANVB = "LOCKDKNHANVB";
        public const string UNLOCKDKNHANVB = "UNLOCKDKNHANVB";
        public const string NHANVB = "NHANVB";
    }
    public class ENUM_CONNECTION
    {
        public const string DonKhoiKien = "DKKConnection";
        public const string ThongKe = "ThongKeConnection";
        public const string GSTP = "GSTPConnection";
    }
    public class ENUM_MAQUOCTICH
    {
        public const string VIETNAM = "VN";
    }
    public class ENUM_LOAITHONGBAO_QH
    {
        public const Decimal TBTINHTHE = 1;
        public const Decimal TBKQTRALOI = 2;
        public const Decimal TRALOIDON_AHS = 3;
    }
    public class ENUM_QH_NHANTHAN
    {
        public const string BO = "BO";
        public const string ME = "ME";
        public const string ANH = "ANH";
        public const string CHI = "CHI";
        public const string EM = "EM";
        public const string VO_CHONG = "VO_CHONG";
        public const string CON = "CON";
    }
    public class ENUM_CHUCDANH
    {
        public const string CHUCDANH_THAMPHAN = "TP";
        public const string CHUCDANH_THUKY = "TK";
        public const string CHUCDANH_HTND = "HTND";
        public const string CHUCDANH_TTV = "TTV";
        public const string CHUCDANH_KSV = "KSV";

    }
    public class ENUM_NHOMHINHPHAT
    {
        public const string NHOM_HPCHINH = "NHOMHINHPHAT_CHINH";
        public const string NHOM_HPBOSUNG = "NHOMHINHPHAT_BOSUNG";
        public const string NHOM_QDKHAC = "NHOMHINHPHAT_QDKHAC";
    }
    public class ENUM_LOAIHINHPHAT
    {
        public const int DANG_TRUE_FALSE_VALUE = 1;
        public const int DANG_THOI_GIAN_VALUE = 2;
        public const int DANG_SO_HOC_VALUE = 3;
        public const int DANG_KHAC_VALUE = 4;
        public const int DEFAULT_TRUE = 5;

        //public const String DANG_TRUE_FALSE_TEXT = "Có / Không";
        //public const String DANG_THOI_GIAN_TEXT = "Thời gian";
        //public const String DANG_SO_HOC_TEXT = "Số học";
        //public const String DANG_KHAC_TEXT = "Khác";
    }
    public class ENUM_HINHPHAT
    {
        public const String TU_HINH = "TUHINH";
        public const String TU_CHUNGTHAN = "TUCHUNGTHAN";
    }
    public class ENUM_TINHTRANGGIAMGIU
    {
        public const string TAMGIAM = "84";
        public const string DANGTAINGOAI = "85";
        public const string DANGBOTRON = "86";
        public const string DANGTAMGIAM_VUANKHAC = "87";
    }

    public class ENUM_AHS_BPNGANCHAN
    {
        public const string TAMGIAM = "BPNC_03";
    }


    public class ENUM_LOAI_QD_AHS
    {
        public const string BAT_TAMGIAM = "BTG";
    }
    public class ENUM_CHUCVU
    {
        public const string CHUCVU_CA = "CA";//Chánh án
        public const string CHUCVU_PCA = "PCA";//Phó chánh án
        public const string CHUCVU_VT = "VT";//Vụ trưởng
        public const string CHUCVU_PVT = "PVT";//Phó vụ trưởng
    }
    public class ENUM_GIAIDOANVUAN
    {
        public const int HOSO = 1; //anhvh bỏ giai đoạn hồ sơ đi và add nó cùng = sơ thẩm;sua noi dung hien thanh: Tiếp nhận và xử lý (AHS:: Hồ sơ vụ án)
        public const int SOTHAM = 2;
        public const int PHUCTHAM = 3;
        public const int THULYGDT = 4;
        public const int DINHCHI = 5;
        //public const int THULYTT = 6; anhvh add vì hiện tại chưa dùng đến cấp này
    }
    public class ENUM_AHS_TRANGTHAIGIAONHAN_HS
    {
        public const string VKSGiaoHSXuSoTham = "1";
        public const string VKSChapNhanDieuTraBoSung = "2";
        public const string VKSKoChapNhanDieuTraBoSung = "3";
    }

    public class ENUM_AHS_TUCACHTHAMGIATT
    {
        public const string BIHAI = "TGTTHS_01";
        public const string LUATSU = "TGTTHS_06";
        public const string BAOVEQUYENLOIDUONGSU = "TGTTHS_10";
        public const string BAOCHUAKHAC = "TGTTHS_17";
        // public const string VKSChapNhanDieuTraBoSung = "2";
        //public const string VKSKoChapNhanDieuTraBoSung = "3";
    }


    public class ENUM_ADS_BIENPHAPGQ
    {
        public const string ADS_ChuyenDonTrongNganh = "1";
        public const string ADS_ChuyenDonNgoaiNganh = "2";
        public const string ADS_TraLaiDon = "3";
        public const string ADS_YCBoSungDon = "4";
        public const string ADS_ThuLy = "5";
    }

    public class ENUM_LOAIVUVIEC
    {
        public const string AN_HINHSU = "01";
        public const string AN_DANSU = "02";
        public const string AN_HONNHAN_GIADINH = "03";
        public const string AN_KINHDOANH_THUONGMAI = "04";
        public const string AN_LAODONG = "05";
        public const string AN_HANHCHINH = "06";
        public const string AN_PHASAN = "07";
        public const string BPXLHC = "08";
        public const string AN_GDTTT = "09";
        public const string AN_THA = "10";
    }

    public class ENUM_BAOCAO_THONGKE
    {
        public const string BAOCAOCHITIEUVEDONTHEOKY_LOAIAN = "bctk_tla";
        public const string BAOCAOCHITIEUVEDONTHEOKY_THAMPHAN = "bctk_ttp";
    }
    public class ENUM_LOAIVUVIEC_NUMBER
    {
        public const string AN_HINHSU = "1";
        public const string AN_DANSU = "2";
        public const string AN_HONNHAN_GIADINH = "3";
        public const string AN_KINHDOANH_THUONGMAI = "4";
        public const string AN_LAODONG = "5";
        public const string AN_HANHCHINH = "6";
        public const string AN_PHASAN = "7";
        public const string BPXLHC = "8";
        public const string AN_GDTTT = "9";
        public const string AN_THA = "10";
    }
    public class ENUM_LOAIAN
    {
        public const string AN_HINHSU = "AN_HINHSU";
        public const string AN_DANSU = "AN_DANSU";
        public const string AN_HONNHAN_GIADINH = "AN_HNGD";
        public const string AN_KINHDOANH_THUONGMAI = "AN_KDTM";
        public const string AN_LAODONG = "AN_LAODONG";
        public const string AN_HANHCHINH = "AN_HANHCHINH";
        public const string AN_GSTP = "GSTP";
        public const string AN_PHASAN = "AN_PHASAN";
        public const string BPXLHC = "BPXLHC";
        public const string PCTT = "PCTP";
        public const string AN_GDTTT = "AN_GDTTT";
        public const string AN_THA = "THA";
    }
    public class ENUM_AHS_LOAITOIPHAM
    {
        public const string CHUAXACDINH = "LTP00";
        public const string IT_NGHIEMTRONG = "LTP01";
        public const string NGHIEMTRONG = "LTP02";
        public const string RAT_NGHIEMTRONG = "LTP03";
        public const string DACBIET_NGHIEMTRONG = "LTP04";
    }

    public class ENUM_DANHMUC
    {
        public const string DANTOC = "DANTOC";
        public const string QUOCTICH = "QUOCTICH";
        public const string TONGIAO = "TONGIAO";
        public const string LOAITOIPHAM = "LOAITOIPHAM";
        public const string QUANHEPL_YEUCAU = "QHPLYEUCAU";
        public const string QUANHEPL_TRANHCHAP = "QHPLTRANHCHAP";
        public const string QUANHEPL_YEUCAU_HNGD = "QHPLYEUCAUHNGD";
        public const string QUANHEPL_TRANHCHAP_HNGD = "QHPLTRANHCHAPHNGD";
        public const string QUANHEPL_YEUCAU_KDTM = "QHPLYEUCAUKDTM";
        public const string QUANHEPL_TRANHCHAP_KDTM = "QHPLTRANHCHAPKDTM";
        public const string QUANHEPL_YEUCAU_LD = "QHPLYEUCAULD";
        public const string QUANHEPL_TRANHCHAP_LD = "QHPLTRANHCHAPLD";
        public const string QUANHEPL_KHIEUKIEN_HC = "QHPLKHIEUKIENHC";
        public const string QUANHEPL_YEUCAUPS = "QHPLYEUCAUPS";
        public const string QUANHEPL_DENGHI_XLHC = "QHPL_DENGHI_XLHC";
        public const string LYDOLYHON = "LYDOLYHON";
        public const string TUCACHTOTUNG_DS = "TUCACHTOTUNG";
        public const string CHUCDANH = "CHUCDANH";
        public const string CHUCVU = "CHUCVU";
        public const string CHUCVUVKS = "CHUCVUVKS";
        public const string VAITROTHAMPHAN = "VAITROTHAMPHAN";
        public const string LOAITOA = "LOAITOA";
        public const string LOAIVIENKIEMSOAT = "LOAIVIENKIEMSOAT";
        public const string TUCACHTGTTDS = "TUCACHTGTTDS";
        public const string TUCACHTGTTHS = "TUCACHTGTTHS";
        public const string BIENPHAPNGANCHAN = "BIENPHAPNGANCHAN";
        public const string KETLUANDONGDTTT = "KLGIAIQUYETDONGDTT";
        //---------chua lam quan ly dm--------------
        public const string TRINHDOVANHOA = "TRINHDOVANHOA";
        public const string QUYETDINHCAOTRANG = "QUYETDINHCAOTRANG";
        public const string TINHTRANGGIAMGIU = "TINHTRANGGIAMGIU";
        public const string NGHENGHIEP = "NGHENGHIEP";
        public const string CHUCVUDANG = "CHUCVUDANG";
        public const string NHOMHINHPHAT = "NHOMHINHPHAT";
        public const string LOAIDONVIQDNGANCHAN = "LOAIDONVIQDNGANCHAN";
        public const string TRUONGHOPTHULYAN = "TRUONGHOPTHULYAN";
        public const string TRUONGHOP_GIAONHAN = "TRUONGHOP_GIAONHAN";
        public const string YEUCAUKCHINHSU = "YEUCAUKCHINHSU";
        public const string YEUCAUKNHINHSU = "YEUCAUKNHINHSU";
        public const string LYDOTRADON = "LYDOTRADON";
        public const string LYDOTRADONHC = "LYDOTRADONHC";
        public const string CAPTHAMPHAN = "	CAPTHAMPHAN";
        public const string LOAIUYQUYEN = "LOAIUYQUYEN";
        public const string KETLUANGDTTT = "KETLUANGDTTT";
        public const string QH_NHAN_THAN = "QH_NHAN_THAN";
        public const string LOAIQD_HC = "LOAIQD_HC";
        public const string DMLOAITOIPHAMHS = "DMLOAITOIPHAMHS";
        public const string LOAITHULY_AHS_PT = "LOAITHULY_AHS_PT";

        //------------------DM dung trong thi hanh an---------------
        public const string THA_YEUCAUTHULY = "THA_YEUCAUTHULY";
        public const string THA_LYDOTHULY = "THA_LYDOTHULY";
        public const string DOITUONGNOPYCPS = "DOITUONGNOPYCPS";
        public const string QUYETDINH_TB_THA = "QUYETDINH_TB_THA";
        //---------------
        public const string UYTHACTHA_LyDo = "UYTHACTHA_LyDo";

        // --- DM dùng trong GDTTTT
        public const string LOAICVGDTTT = "LOAICVGDTTT";
        public const string DONVIUUTIEN = "DONVIUUTIEN";
        public const string PHANLOAIKNTC = "PHANLOAIKNTC";
        public const string NGUOIKHANGNGHI = "NGUOIKHANGNGHI";
        // --- DM dùng trong TĐKT
        public const string DM_HINHTHUC_KHENTHUONG = "DM_HINHTHUC_KHENTHUONG";
        public const string DM_HINHTHUC_KYLUAT = "DM_HINHTHUC_KYLUAT";
        public const string DM_DANHHIEU_THIDUA = "DM_DANHHIEU_THIDUA";
        public const string DM_LOAIHINH_KHENTHUONG = "DM_LOAIHINH_KHENTHUONG";
        public const string BIEU_MAU_TDKT = "BIEU_MAU_TDKT";
    }
    public class ENUM_SESSION
    {
        public const string SESSION_USERID = "USERID";
        public const string SESSION_USERNAME = "USERNAME";
        public const string SESSION_USERTEN = "USERTEN";
        public const string SESSION_NHOMNSDID = "NHOMNSDID";
        public const string SESSION_DONVIID = "DONVIID";
        public const string SESSION_LOAIUSER = "LOAIUSER";
        public const string SESSION_MACANBO = "MACANBO";
        public const string SESSION_MADONVI = "DONVI_MA";
        public const string SESSION_TENDONVI = "DONVI_TEN";
        public const string SESSION_TINH_ID = "TINHID";
        public const string SESSION_QUAN_ID = "QUANID";
        public const string SESSION_CANBOID = "CANBO_ID";
        public const string SESSION_PHONGBANID = "PHONGBANID";
        //-----------dung trong DonKK--------------------
        public const string SESSION_MUCDICHSD = "MUCDICH_SD";
        public const string SESSION_ISPHANLOAIDON = "ISPHANLOAIDON";
        //---------------
        public const string SESSION_YEARS_TK = "YEARS_TK";
        /// <summary>
        /// Luu thong tin du lieu nguoi dung luc SSO ve
        /// </summary>
        public const string SESSION_DVCQG_USER_INFO = "DVCQG_USER_INFO";
    }

    public class ENUM_LOAI_TRIEUTAP
    {
        public const string DUONGSU = "DUONGSU";
        public const string NGUOITHAMGIATOTUNG = "NGUOITHAMGIATOTUNG";
        public const string BICANBICAO = "BICANBICAO";
    }


    public class ENUM_DANSU_TUCACHTOTUNG
    {
        public const string NGUYENDON = "NGUYENDON";
        public const string BIDON = "BIDON";
        public const string QUYENNVLQ = "QUYENNVLQ";
        public const string UYQUYEN = "UYQUYEN";
        public const string KHAC = "KHAC";
        public const string NGUOILAMCHUNG = "NGUOILAMCHUNG";
        public const string QUYENLOIICHDUOCBAOVE = "QUYENLOIICHDUOCBAOVE";

        //-------muc nay chi dung trong DonKK---------
        public const string NGUOIUYQUYEN = "NGUOIUYQUYEN";
    }
    public class ENUM_VAITROTHAMPHAN
    {
        public const string VTTP_GIAIQUYETDON = "VTTP_GIAIQUYETDON";
        public const string VTTP_GIAIQUYETSOTHAM = "VTTP_GIAIQUYETSOTHAM";
        public const string VTTP_GIAIQUYETPHUCTHAM = "VTTP_GIAIQUYETPHUCTHAM";
        public const string VTTP_CHUTOASOTHAM = "VTTP_CHUTOASOTHAM";
    }
    public class ENUM_NGUOITIENHANHTOTUNG
    {
        public const string THAMPHAN = "THAMPHAN";
        public const string THAMPHANHDXX = "THAMPHANHDXX";
        public const string THAMPHANDUKHUYET = "THAMPHANDUKHUYET";
        public const string HTND = "HTND";
        public const string THUKY = "THUKY";
        public const string THUKYDUKHUYET = "THUKYDUKHUYET";
        public const string KIEMSOATVIEN = "KIEMSOATVIEN";
        public const string THAMTRAVIEN = "THAMTRAVIEN";
    }
    public class ENUM_DS_TRANGTHAI
    {
        public const int TAOMOI = 1;
        public const int TRALAIDON = 2;
        public const int BOSUNGDON = 3;
        public const int NOPDUPHI = 4;
        public const int THULY = 5;
        public const int HOAGIAI = 6;
        public const int XETXUSOTHAM = 7;
    }

    public class ENUM_TRUONGHOP_GIAONHAN
    {
        public const string KHONGTHUOC_THAMQUYEN_XETXU = "01";
        public const string KHANGCAO_PHUCTHAM = "02";
        public const string KHANGCAO_KHANGNGHI_PHUCTHAM = "03";
        public const string KHANGNGHI_PHUCTHAM = "04";
        public const string XETXULAI_CAPSOTHAM = "05";
        public const string KHONGTHUOC_THAMQUYEN_GIAIQUYET = "06";
        public const string KHIEUNAI_PHUCTHAM = "07";
        public const string KHIEUNAI_KHANGNGHI_PHUCTHAM = "08";
        public const string CHUYENYEUCAU_VE_SOTHAM = "09";
        public const string GDT_CHUYEN_VE = "10";
        public const string TOA_CAPCAO_CHUYEN_VE = "11";
    }
    public class ENUM_QHPLTK
    {
        public const int DANSU = 0;
        public const int HONNHAN_GIADINH = 1;
        public const int KINHDOANH_THUONGMAI = 2;
        public const int LAODONG = 3;
        public const int HANHCHINH = 4;
        public const int PHASAN = 5;
        public const int NGANHKINHTE = 15;
    }
    public class ENUM_DOITUONG_NOPYCPS
    {
        public const string CHUNO_COBAODAM = "01";
        public const string CHUNO_BAODAM1PHAN = "02";
        public const string CHUNO_TCTINDUNG = "03";
        public const string CHUNO_BAOHIEM = "04";
        public const string NGUOILAODONG = "05";
        public const string CONGDOAN = "06";
        public const string CHU_DN_HTX = "07";
        public const string CODONG = "08";
        public const string NHNN = "09";
    }
    public class ENUM_XLHC_DENGHI
    {
        public const string HOAN = "1";
        public const string MIEN = "2";
        public const string GIAM = "3";
        public const string TAM_DINHCHI = "4";
        public const string MIEN_CONLAI = "5";
    }

    public class ENUM_GDT_LOAICV
    {
        public const string DE_NGHI = "CVDN";
        public const string KIENNGHI = "CVKN";
        public const string NHACLAI = "CVNL";
        public const string CV6_1 = "CV6.1";
        public const string CV9_3 = "CV9.3";
        public const string CV8_1 = "CV8.1";
        public const string CV8_1_DDBQH = "CV8.1.DDBQH";
        public const string CV8_1_DBQH = "CV8.1.DBQH";
        public const string CV8_1_CQQH = "CV8.1.CQQH";
        public const string CHUYEN = "CVCHUYEN";
        public const string LOAIKHAC = "CVLOAIKHAC";
    }

    public class MenuPermission
    {
        public decimal MENUID { get; set; }
        public bool XEM { get; set; }
        public bool TAOMOI { get; set; }
        public bool CAPNHAT { get; set; }
        public bool XOA { get; set; }
        public bool ISXINANGIAM { get; set; }
        public bool GDT_ISXINANGIAM { get; set; }
    }

    //------------------------------------------
    public class ENUM_NHOMQUYEN
    {
        public const int QUANTRIHT = 1;
    }
    /*--------------SD Trong DKK-------------------------*/
    public class ENUM_DKNHANVBTONGDAT
    {
        public const int DANGKY = 1;
        public const int KHONGDK = 0;
    }
    public class ENUM_ISREAD
    {
        public const int CHUADOC = 0;
        public const int DADOC = 1;
        public const int TATCA = 2;
    }
    public class ENUM_THA
    {
        public const string QD_GIAMAN = "THA_QD_GIAMAN";
    }
    public class TK_CANHBAO
    {
        public const string TINHTRANG_GIAIQUYET = "TINHTRANG_GIAIQUYET";
        public const string THOIHAN_GQ_GIAIQUYET = "THOIHAN_GQ_GIAIQUYET";
        public const string CAPXX = "CAPXX";
        public const string TINHTRANG_THULY = "TINHTRANG_THULY";
        public const string TUNGAY = "TUNGAY";
        public const string GQDON = "GQDON";
    }
    public class SS_TK
    {
        public const string ISHOME = "SSTK_ISHOME";
        public const string ISTUHINH = "SSTK_ISTUHINH";
        public const string ISDONGOC = "SSTK_ISDONGOC";
        public const string NGUOIGUI = "SSTK_NGUOIGUI";
        public const string SOBAQD = "SSTK_SOBAQD";
        public const string NGAYBAQD = "SSTK_NGAYBAQD";
        public const string TOAANXX = "SSTK_TOAANXX";
        public const string NGAYNHANTU = "SSTK_NGAYNHANTU";
        public const string NGAYNHANDEN = "SSTK_NGAYNHANDEN";
        public const string LOAICHUYEN = "SSTK_LOAICHUYEN";
        public const string PHONGBANCHUYEN = "SSTK_PHONGBANCHUYEN";
        public const string DIEUKIENCHUYEN = "SSTK_DIEUKIENCHUYEN";
        public const string THULYDON = "SSTK_THULYDON";
        public const string TOAKHACID = "SSTK_TOAKHACID";
        public const string TENNGOAITOAAN = "SSTK_TENNGOAITOAAN";
        public const string NGAYCHUYENTU = "SSTK_NGAYCHUYENTU";
        public const string NGAYCHUYENDEN = "SSTK_NGAYCHUYENDEN";
        public const string HINHTHUCDON = "SSTK_HINHTHUCDON";
        public const string SOCMND = "SSTK_SOCMND";
        public const string MADON = "SSTK_MADON";
        public const string TINHID = "SSTK_TINHID";
        public const string HUYENID = "SSTK_HUYENID";
        public const string DIACHICHITIET = "SSTK_DIACHICHITIET";
        public const string TRALOIDON = "SSTK_TRALOIDON";
        public const string DONVICV = "SSTK_DONVICV";
        public const string SOCV = "SSTK_SOCV";
        public const string NGAYCV = "SSTK_NGAYCV";
        public const string TRANGTHAICHUYEN = "SSTK_TRANGTHAICHUYEN";
        public const string CHANHANCHIDAO = "SSTK_CHIDAO";
        public const string TRAIGIAM = "SSTK_TRAIGIAM";
        public const string PHANCONGTTV = "SSTK_PHANCONGTTV";
        public const string THULY_TU = "SSTK_THULY_TU";
        public const string THULY_DEN = "SSTK_THULY_DEN";
        public const string SOTHULY = "SSTK_SOTHULY";
        public const string BC_SOCV = "BC_SOCV";
        public const string BC_NGAYCV = "BC_NGAYCV";
        public const string BC_NGUOIKY = "BC_NGUOIKY";
        public const string NGAYNHAPTU = "SSTK_NGAYNHAPTU";
        public const string NGAYNHAPDEN = "SSTK_NGAYNHAPDEN";
        public const string THAMPHAN = "SSTK_THAMPHAN";
        public const string LOAICV = "SSTK_LOAICV";
        public const string NGUOINHAP = "SSTK_NGUOINHAP";
        public const string ARRSELECTID = "SSTK_ARRSELECT";
        public const string MUONHOSO = "SSTK_MUONHOSO";
        public const string CHIDAO = "SSTK_CHIDAO";
        public const string NGUYENDON = "SSTK_NGUYENDON";
        public const string BIDON = "SSTK_BIDON";
        public const string LOAIAN = "SSTK_LOAIAN";
        public const string THAMTRAVIEN = "SSTK_THAMTRAVIEN";
        public const string LANHDAOPHUTRACH = "SSTK_LANHDAOPHUTRACH";
        public const string QHPKLT = "SSTK_QHPKLT";
        public const string QHPLDN = "SSTK_QHPLDN";
        public const string COQUANCHUYENDON = "SSTK_COQUANCHUYENDON";

        public const string TRANGTHAITHULY = "SSTK_TRANGTHAITHULY";
        public const string ISDANGKYBAOCAO = "SSTK_ISDANGKYBAOCAO";

        public const string TRANGTHAIYKIENTT = "SSTK_TRANGTHAIYKIENTT";
        public const string KETQUATHULY = "SSTK_KETQUATHULY";
        public const string KETQUAXETXU = "SSTK_KETQUAXETXU";
        public const string TENBAOCAO = "SSTK_TENBAOCAO";
        public const string TRANGTHAIXETXU = "SSTK_TRANGTHAIXETXU";

        public const string XXGDT_HDTP = "SSTK_XXGDT_HDTP";

        public const string TRANGTHAITOTRINH = "SSTK_TRANGTHAITOTRINH";

        public const string CVPC_TenCQ = "SSTK_CVPC_TenCQ";
        public const string CVPC_SO = "SSTK_CVPC_SO";
        public const string CVPC_NGAY = "SSTK_CVPC_NGAY";
        public const string GUITOI_CA_TA = "SSTK_GUITOI_CA_TA";
        public const string HOAN_THIHAHAN = "SSTK_HOAN_THIHAHAN";
        public const string ANQUOCHOI_THOIHIEU = "SSTK_ANQUOCHOI_THOIHIEU";
        public const string BUOCTT = "SSTK_BUOCTT";
        public const string ANTHOIHIEU = "SSTK_ANTHOIHIEU";
        public const string CHK_CONLAI_S = "SSTK_CHK_CONLAI";
        public const string PHANLOAIDON = "SSTK_PHANLOAIDON";
        public const string GhepDon_VuAn = "SSTK_GhepDon_VuAn";
        public const string GIAOTHS = "SSTK_GIAOTHS";
        public const string HCTP_GHICHU = "HCTP_GHICHU";
        //-----------------
        public const string TRANG_THAI_XLY_VT = "TRANG_THAI_XLY_VT";
        public const string TRANGTHAICHUYEN_VT = "TRANGTHAICHUYEN_VT";
        public const string NOI_NHAN_SEARCH = "NOI_NHAN_SEARCH";
        public const string LOAI_VB = "LOAI_VB";
        public const string SODEN = "SODEN";
        public const string SODEN_DEN = "SODEN_DEN";
        public const string NGAY_FROM = "NGAY_FROM";
        public const string NGAY_FROM_DEN = "NGAY_FROM_DEN";
        public const string NGUOI_GUI_BT = "NGUOI_GUI_BT";
        //-----------------
    }

    public class ENUM_GDTTT_TRANGTHAI
    {
        public const decimal THULY_MOI = 1;
        public const decimal PHANCONG_TTV = 2;
        public const decimal NGHIENCUU_HOSO = 3;
        public const decimal TRINH_PHOVT = 4;
        public const decimal TRINH_VUTRUONG = 5;
        public const decimal TRINH_THAMPHAN = 6;
        public const decimal TRINH_PHOCA = 7;
        public const decimal TRINH_CA = 8;
        public const decimal TRINH_TO_THAMPHAN = 9;
        public const decimal TRINH_HOIDONG_THAMPHAN = 17;
        public const decimal TRINH_NGHIENCUULAI = 10;
        public const decimal TRINH_DUTHAO_TRALOI_DON = 11;
        public const decimal TRINH_DUTHAO_KHANGNGHI = 12;
        public const decimal TRALOIDON = 13;
        public const decimal KHANGNGHI = 14;
        public const decimal THULY_XETXU_GDT = 15;
        public const decimal XEPDON = 16;
        public const decimal XUlY_KHAC = 18;
        public const decimal XEPDON_VKS_DANGGIAIQUYET = 19;
    }
    //
}