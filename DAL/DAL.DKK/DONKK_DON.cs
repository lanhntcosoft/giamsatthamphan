//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DKK
{
    using System;
    using System.Collections.Generic;
    
    public partial class DONKK_DON
    {
        public decimal ID { get; set; }
        public decimal TOAANID { get; set; }
        public Nullable<decimal> LOAIANID { get; set; }
        public string TENNGUOIGUI { get; set; }
        public System.DateTime NGAYSINH { get; set; }
        public Nullable<decimal> NAMSINH { get; set; }
        public string DIACHI_THUONGTRU { get; set; }
        public string DIENTHOAI { get; set; }
        public string FAX { get; set; }
        public string NOIDUNG { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public string MALOAIVUAN { get; set; }
        public Nullable<decimal> TRANGTHAI { get; set; }
        public string DIACHI_TAMTRU { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public Nullable<decimal> NGUOITAO { get; set; }
        public string MAVUVIEC { get; set; }
        public string TENVUVIEC { get; set; }
        public Nullable<decimal> YEUTONUOCNGOAI { get; set; }
        public Nullable<decimal> QUANHEPHAPLUATID { get; set; }
        public Nullable<decimal> STOPVB { get; set; }
        public string SOQD { get; set; }
        public Nullable<System.DateTime> NGAYQD { get; set; }
        public string TENQD { get; set; }
        public string HANHVIHC { get; set; }
        public string TOMTATHANHVIHCBIKIEN { get; set; }
        public Nullable<decimal> LOAIQDHANHCHINHID { get; set; }
        public string QDHC_CUA { get; set; }
        public string NOIDUNGQUYETDINH { get; set; }
        public string THONGTINTHEM { get; set; }
        public string YEUCAU { get; set; }
        public Nullable<System.DateTime> NGAYGUI { get; set; }
    }
}
