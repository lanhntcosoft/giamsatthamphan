//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class AHS_SOTHAM_QUYETDINH_BICAN
    {
        public decimal ID { get; set; }
        public Nullable<decimal> BICANID { get; set; }
        public Nullable<decimal> LOAIQDID { get; set; }
        public Nullable<decimal> QUYETDINHID { get; set; }
        public Nullable<decimal> LOAIDONVI { get; set; }
        public Nullable<decimal> DONVIID { get; set; }
        public string SOQUYETDINH { get; set; }
        public Nullable<System.DateTime> NGAYQD { get; set; }
        public Nullable<System.DateTime> HIEULUCTU { get; set; }
        public Nullable<decimal> THEOLUAT_THANG { get; set; }
        public Nullable<decimal> THEOLUAT_NGAY { get; set; }
        public Nullable<System.DateTime> THEOLUAT_NGAYKETTHUC { get; set; }
        public Nullable<decimal> THUCTE_THANG { get; set; }
        public Nullable<decimal> THUCTE_NGAY { get; set; }
        public Nullable<System.DateTime> THUCTE_NGAYKETTHUC { get; set; }
        public Nullable<decimal> NGUOIKYID { get; set; }
        public string CHUCVU { get; set; }
        public string GHICHU { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
        public string TENFILE { get; set; }
        public string KIEUFILE { get; set; }
        public byte[] NOIDUNGFILE { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public Nullable<System.DateTime> HIEULUCDEN { get; set; }
        public string NOIGIAMGIU { get; set; }
        public string DIEULUATAPDUNGTHEM { get; set; }
        public Nullable<System.DateTime> BIENBANNGHIAN_NGAY { get; set; }
        public Nullable<decimal> FILEID { get; set; }
    }
}
