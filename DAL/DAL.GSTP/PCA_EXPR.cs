//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCA_EXPR
    {
        public string RULE_ID { get; set; }
        public short FRM_NO { get; set; }
        public short EXPR_LINE { get; set; }
        public string COND { get; set; }
        public string RESULT { get; set; }
        public string NOTES { get; set; }
        public string VALUE_DATE { get; set; }
        public Nullable<short> IS_ACTIVE { get; set; }
        public Nullable<decimal> ID_TOA_AN { get; set; }
        public Nullable<decimal> SU_DUNG { get; set; }
    }
}
