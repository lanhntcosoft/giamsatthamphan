//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class AHC_XULY_VIPHAMHC
    {
        public decimal ID { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public Nullable<decimal> TONGSO { get; set; }
        public Nullable<decimal> CHINH_CANHCAO { get; set; }
        public Nullable<decimal> CHINH_PHATTIEN { get; set; }
        public Nullable<decimal> CHINH_SOTIEN { get; set; }
        public Nullable<decimal> CHINH_TICHTHU { get; set; }
        public Nullable<decimal> HINHPHAT_BOSUNG { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public Nullable<decimal> MAGIAIDOAN { get; set; }
    }
}
