//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class TUPHAP_MENU
    {
        public decimal ID { get; set; }
        public Nullable<decimal> HIEULUC { get; set; }
        public Nullable<decimal> CHUONGTRINHID { get; set; }
        public Nullable<decimal> CAPCHAID { get; set; }
        public string TENMENU { get; set; }
        public string MAACTION { get; set; }
        public string DUONGDAN { get; set; }
        public Nullable<decimal> ACTION { get; set; }
        public Nullable<decimal> TAOMOI { get; set; }
        public Nullable<decimal> CAPNHAT { get; set; }
        public Nullable<decimal> XOA { get; set; }
        public Nullable<decimal> QUYENSOTHAM { get; set; }
        public Nullable<decimal> QUYENPHUCTHAM { get; set; }
        public Nullable<decimal> QUYENTOICAO { get; set; }
        public Nullable<decimal> QUYENQUANTRI { get; set; }
        public string ARRSAPXEP { get; set; }
        public Nullable<decimal> THUTU { get; set; }
        public string ARRTHUTU { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public Nullable<decimal> ISPHANNHOM { get; set; }
        public Nullable<decimal> ISNOTMENU { get; set; }
        public Nullable<decimal> ISSOTHAM { get; set; }
        public Nullable<decimal> ISPHUCTHAM { get; set; }
    }
}
