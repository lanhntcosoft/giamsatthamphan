//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_QD_QUYETDINH_LYDO
    {
        public decimal ID { get; set; }
        public Nullable<decimal> QDID { get; set; }
        public string MA { get; set; }
        public string TEN { get; set; }
        public Nullable<decimal> HIEULUC { get; set; }
        public Nullable<decimal> THUTU { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
    }
}
