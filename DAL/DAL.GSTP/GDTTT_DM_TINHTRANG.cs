//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class GDTTT_DM_TINHTRANG
    {
        public decimal ID { get; set; }
        public string TENTINHTRANG { get; set; }
        public Nullable<decimal> THUTU { get; set; }
        public Nullable<decimal> GIAIDOAN { get; set; }
        public string MA { get; set; }
        public Nullable<decimal> HIEULUC { get; set; }
    }
}
