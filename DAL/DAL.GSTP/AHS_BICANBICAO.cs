//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class AHS_BICANBICAO
    {
        public decimal ID { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public string MABICAN { get; set; }
        public Nullable<decimal> BICANDAUVU { get; set; }
        public string HOTEN { get; set; }
        public string TENKHAC { get; set; }
        public Nullable<System.DateTime> NGAYSINH { get; set; }
        public Nullable<decimal> THANGSINH { get; set; }
        public Nullable<decimal> NAMSINH { get; set; }
        public Nullable<System.DateTime> NGAYTHAMGIA { get; set; }
        public string SOCMND { get; set; }
        public Nullable<decimal> TAMTRU { get; set; }
        public string TAMTRUCHITIET { get; set; }
        public Nullable<decimal> HKTT { get; set; }
        public string KHTTCHITIET { get; set; }
        public Nullable<decimal> TRINHDOVANHOAID { get; set; }
        public Nullable<decimal> NGHENGHIEPID { get; set; }
        public Nullable<decimal> DANTOCID { get; set; }
        public Nullable<decimal> QUOCTICHID { get; set; }
        public Nullable<decimal> GIOITINH { get; set; }
        public Nullable<decimal> TONGIAOID { get; set; }
        public Nullable<decimal> NGHIENHUT { get; set; }
        public Nullable<decimal> TAIPHAM { get; set; }
        public Nullable<decimal> TIENAN { get; set; }
        public Nullable<decimal> TIENSU { get; set; }
        public Nullable<decimal> TREMOCOI { get; set; }
        public Nullable<decimal> BOMELYHON { get; set; }
        public Nullable<decimal> TREBOHOC { get; set; }
        public Nullable<decimal> TRELANGTHANG { get; set; }
        public Nullable<decimal> CONGUOIXUIGIUC { get; set; }
        public Nullable<decimal> CHUCVUDANGID { get; set; }
        public Nullable<decimal> CHUCVUCHINHQUYENID { get; set; }
        public Nullable<decimal> TINHTRANGGIAMGIUID { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public Nullable<decimal> ISTREVITHANHNIEN { get; set; }
        public Nullable<decimal> LOAIDOITUONG { get; set; }
        public Nullable<decimal> HKTT_HUYEN { get; set; }
        public Nullable<decimal> TAMTRU_HUYEN { get; set; }
        public Nullable<decimal> TAIPHAMNGUYHIEM { get; set; }
        public Nullable<decimal> TUOI { get; set; }
        public string DIACHICOQUAN { get; set; }
        public Nullable<decimal> LOAITOIPHAMHS_ID { get; set; }
        public byte[] BICANBICAOID_TACC { get; set; }
    }
}
