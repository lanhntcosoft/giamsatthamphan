//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADS_SOTHAM_KHANGNGHI
    {
        public decimal ID { get; set; }
        public Nullable<decimal> DONID { get; set; }
        public string SOKN { get; set; }
        public Nullable<System.DateTime> NGAYKN { get; set; }
        public Nullable<decimal> DONVIKN { get; set; }
        public Nullable<decimal> CAPKN { get; set; }
        public Nullable<decimal> LOAIKN { get; set; }
        public Nullable<decimal> BANANID { get; set; }
        public Nullable<System.DateTime> NGAYBANAN { get; set; }
        public Nullable<decimal> TOAANRAQDID { get; set; }
        public string NOIDUNGKN { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
        public string TENFILE { get; set; }
        public string KIEUFILE { get; set; }
        public byte[] NOIDUNGFILE { get; set; }
        public Nullable<decimal> TOAAN_VKS_KN { get; set; }
    }
}
