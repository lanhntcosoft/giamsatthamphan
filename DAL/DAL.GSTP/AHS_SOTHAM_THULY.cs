//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class AHS_SOTHAM_THULY
    {
        public decimal ID { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public string MATHULY { get; set; }
        public Nullable<decimal> TRUONGHOPTHULY { get; set; }
        public Nullable<System.DateTime> NGAYTHULY { get; set; }
        public string SOTHULY { get; set; }
        public Nullable<decimal> THOIHAN_THANG { get; set; }
        public Nullable<decimal> THOIHAN_NGAY { get; set; }
        public Nullable<System.DateTime> THOIHANTUNGAY { get; set; }
        public Nullable<System.DateTime> THOIHANDENNGAY { get; set; }
        public string GHICHU { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<decimal> TT { get; set; }
        public Nullable<decimal> ISANDIEM { get; set; }
        public string SOBANCAOTRANG { get; set; }
        public Nullable<System.DateTime> NGAYBANCAOTRANG { get; set; }
        public decimal UTTPDI { get; set; }
    }
}
