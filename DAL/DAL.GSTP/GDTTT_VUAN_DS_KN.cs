//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class GDTTT_VUAN_DS_KN
    {
        public decimal ID { get; set; }
        public Nullable<decimal> NGUOIKHIEUNAIID { get; set; }
        public Nullable<decimal> BICAOID { get; set; }
        public Nullable<decimal> VUANID { get; set; }
        public string NOIDUNGKHIEUNAI { get; set; }
        public Nullable<decimal> DON_BICAO_NEW { get; set; }
        public Nullable<decimal> DON_NGUOIKHIEUNAI_NEW { get; set; }
    }
}
