//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class VT_VANBANDEN
    {
        public decimal ID { get; set; }
        public Nullable<decimal> LOAI_VB { get; set; }
        public Nullable<decimal> SODEN { get; set; }
        public Nullable<System.DateTime> NGAY_DEN { get; set; }
        public string NGUOI_GUI_BT { get; set; }
        public string DIACHI_GUI_BT { get; set; }
        public Nullable<System.DateTime> NGAY_BT { get; set; }
        public Nullable<decimal> NOI_NHAN { get; set; }
        public Nullable<decimal> NGUOI_NHAN { get; set; }
        public string GHICHU { get; set; }
        public string SO_VB { get; set; }
        public Nullable<System.DateTime> NGAY_VB { get; set; }
        public string NOIDUNG_VB { get; set; }
        public Nullable<decimal> SOLUONG_DON { get; set; }
        public Nullable<decimal> DVXULY_ID_DON { get; set; }
        public Nullable<decimal> BAQD_CHECK_DON { get; set; }
        public Nullable<decimal> LOAI_AN_DON { get; set; }
        public Nullable<decimal> CAP_XX_DON { get; set; }
        public string SO_BAQD_DON { get; set; }
        public Nullable<System.DateTime> NGAY_BAQD_DON { get; set; }
        public Nullable<decimal> TOAAN_BAQD_DON { get; set; }
        public string SO_CV { get; set; }
        public Nullable<System.DateTime> NGAY_CV { get; set; }
        public string DONVICHUYEN_CV { get; set; }
        public string NOIDUNG_CV { get; set; }
        public Nullable<decimal> CANBONHAN_ID { get; set; }
        public string SO_HS { get; set; }
        public Nullable<System.DateTime> NGAY_HS { get; set; }
        public Nullable<decimal> DONVICHUYEN_HS { get; set; }
        public string GHICHU_HS { get; set; }
        public string NGUOI_TAO { get; set; }
        public Nullable<System.DateTime> NGAY_TAO { get; set; }
        public string NGUOI_SUA { get; set; }
        public Nullable<System.DateTime> NGAY_SUA { get; set; }
        public Nullable<decimal> TOAANID { get; set; }
        public Nullable<decimal> NGUON_DEN { get; set; }
        public string MABD { get; set; }
        public string NGUOIDUNGDON { get; set; }
        public string DIACHI_NDD { get; set; }
        public string BIDON_DON { get; set; }
        public string NGUYENDON_DON { get; set; }
        public string QHPL_DS_DON { get; set; }
        public Nullable<decimal> QHPL_HS_DON { get; set; }
        public Nullable<decimal> SOLUONG_GAM { get; set; }
        public Nullable<System.DateTime> NGAY_GIAO_DON { get; set; }
        public Nullable<decimal> LOAI_GDTTTT { get; set; }
        public string DOMAT_ID { get; set; }
        public string DO_KHAN_ID { get; set; }
        public Nullable<decimal> DIACHI_NDD_ID { get; set; }
        public Nullable<decimal> DIACHI_GUI_BT_ID { get; set; }
    }
}
