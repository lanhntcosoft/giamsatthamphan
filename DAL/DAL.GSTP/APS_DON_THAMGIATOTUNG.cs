//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class APS_DON_THAMGIATOTUNG
    {
        public decimal ID { get; set; }
        public Nullable<decimal> DONID { get; set; }
        public string HOTEN { get; set; }
        public Nullable<decimal> TAMTRUID { get; set; }
        public string TAMTRUCHITIET { get; set; }
        public Nullable<decimal> HKTTID { get; set; }
        public string HKTTCHITIET { get; set; }
        public Nullable<System.DateTime> NGAYSINH { get; set; }
        public Nullable<decimal> THANGSINH { get; set; }
        public Nullable<decimal> NAMSINH { get; set; }
        public Nullable<decimal> GIOITINH { get; set; }
        public string TUCACHTGTTID { get; set; }
        public string NGUOIDAIDIEN { get; set; }
        public string CHUCVU { get; set; }
        public Nullable<System.DateTime> NGAYTHAMGIA { get; set; }
        public Nullable<System.DateTime> NGAYKETTHUC { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
        public string EMAIL { get; set; }
        public string DIENTHOAI { get; set; }
        public string FAX { get; set; }
        public Nullable<decimal> HKTTTINHID { get; set; }
        public Nullable<decimal> TAMTRUTINHID { get; set; }
        public byte[] ID_DUONGSU_TACC { get; set; }
        public string DUONGSUID { get; set; }
        public string SOCMND { get; set; }
    }
}
