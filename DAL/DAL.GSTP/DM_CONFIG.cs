//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_CONFIG
    {
        public decimal ID { get; set; }
        public string CONFIGNAME { get; set; }
        public string CONFIGCODE { get; set; }
        public string EMAIL { get; set; }
        public string PASS { get; set; }
        public string HOST { get; set; }
        public string POST { get; set; }
        public string SSL { get; set; }
        public string OTHER { get; set; }
    }
}
