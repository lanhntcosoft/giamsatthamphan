//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class XLHC_SOTHAM_KHANGCAO
    {
        public decimal ID { get; set; }
        public Nullable<decimal> DONID { get; set; }
        public Nullable<decimal> HINHTHUCNHAN { get; set; }
        public Nullable<System.DateTime> NGAYVIETDON { get; set; }
        public Nullable<System.DateTime> NGAYKHANGCAO { get; set; }
        public Nullable<decimal> DUONGSUID { get; set; }
        public Nullable<decimal> LOAIKHANGCAO { get; set; }
        public Nullable<decimal> SOQDBA { get; set; }
        public Nullable<decimal> ISQUAHAN { get; set; }
        public Nullable<System.DateTime> NGAYQDBA { get; set; }
        public Nullable<decimal> TOAANRAQDID { get; set; }
        public string NOIDUNGKHANGCAO { get; set; }
        public Nullable<decimal> ISMIENANPHI { get; set; }
        public Nullable<decimal> ANPHI { get; set; }
        public string SOBIENLAI { get; set; }
        public Nullable<System.DateTime> NGAYNOPANPHI { get; set; }
        public Nullable<System.DateTime> GQ_NGAY { get; set; }
        public Nullable<decimal> GQ_THAMPHANID { get; set; }
        public Nullable<decimal> GQ_ISCHAPNHAN { get; set; }
        public string GQ_GHICHU { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string TENFILE { get; set; }
        public string KIEUFILE { get; set; }
        public byte[] NOIDUNGFILE { get; set; }
        public Nullable<decimal> GQ_TINHTRANG { get; set; }
        public Nullable<decimal> GQ_TOAANID { get; set; }
    }
}
