//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class TDKT_KET_QUA_BX_DT
    {
        public decimal ID { get; set; }
        public Nullable<decimal> DOI_TUONG_ID { get; set; }
        public Nullable<decimal> HINH_THUC_KET_QUA_ID { get; set; }
        public Nullable<decimal> KET_QUA_ID { get; set; }
        public Nullable<decimal> LOAI_DE_NGHI_KET_QUA { get; set; }
    }
}
