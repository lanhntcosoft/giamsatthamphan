//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class TDKT_DANH_SACH
    {
        public decimal ID { get; set; }
        public string TEN_DANH_SACH { get; set; }
        public Nullable<decimal> TOAANID { get; set; }
        public Nullable<decimal> LOAI_DE_NGHI { get; set; }
        public string NGUOI_LAP { get; set; }
        public Nullable<System.DateTime> NGAYLAP { get; set; }
        public Nullable<decimal> NAM { get; set; }
        public string NGUOI_DUYET_TEN { get; set; }
        public string NGUOI_DUYET_CHUC_VU { get; set; }
        public string SOQD { get; set; }
        public Nullable<System.DateTime> NGAYQD { get; set; }
        public string FILE_TEN { get; set; }
        public string FILE_LOAI { get; set; }
        public byte[] FILE_NOIDUNG { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
    }
}
