//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.GSTP
{
    using System;
    using System.Collections.Generic;
    
    public partial class AHC_PHUCTHAM_HDXX
    {
        public decimal ID { get; set; }
        public Nullable<decimal> DONID { get; set; }
        public string MAVAITRO { get; set; }
        public Nullable<decimal> CANBOID { get; set; }
        public string HOTEN { get; set; }
        public Nullable<System.DateTime> NGAYPHANCONG { get; set; }
        public Nullable<System.DateTime> NGAYNHANPHANCONG { get; set; }
        public Nullable<System.DateTime> NGAYTHAMGIA { get; set; }
        public Nullable<System.DateTime> NGAYKETTHUC { get; set; }
        public Nullable<decimal> NGUOIPHANCONGID { get; set; }
        public Nullable<decimal> DUKHUYET { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
    }
}
