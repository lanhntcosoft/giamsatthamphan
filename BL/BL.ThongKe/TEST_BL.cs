﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using BL.THONGKE.Manager;
using BL.THONGKE.Info;
using System.Configuration;
using Oracle.ManagedDataAccess.Types;

namespace BL.THONGKE
{
    public class TEST_BL
    {
        public String CurrConnectionString = ENUM_CONNECTION.ThongKe;
        public DataTable GetAllPaging(Decimal UserID, int trangthai, string textsearch, DateTime? tu_ngay, DateTime? den_ngay, decimal PageIndex, decimal PageSize)
        {
            if (tu_ngay == DateTime.MinValue) tu_ngay = null;
            if (den_ngay == DateTime.MinValue) den_ngay = null;
            OracleParameter[] parameters = new OracleParameter[] {    new OracleParameter("user_id",UserID),
                                                                            new OracleParameter("trang_thai",trangthai),
                                                                            new OracleParameter("textsearch",textsearch),
                                                                            new OracleParameter("tu_ngay",tu_ngay),
                                                                            new OracleParameter("den_ngay",den_ngay),
                                                                            new OracleParameter("PageIndex",PageIndex),
                                                                            new OracleParameter("PageSize", PageSize),
                                                                            new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                          };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging(CurrConnectionString, "testttttttttt", parameters);
            return tbl;
        }

        public DataTable test1()
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("in_TOAANID",27),
                new OracleParameter("in_TOAANCAPCON","FALSE"),
                new OracleParameter("in_NGAYBATDAU","11/05/2018"),
                new OracleParameter("in_NGAYKETTHUC","10/09/2018"),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GSTP_SOTHULY.SO_DANSU_SOTHAM", parameters);
            return tbl;
        }
        
    }
}