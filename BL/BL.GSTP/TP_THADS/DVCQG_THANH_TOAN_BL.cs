﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using BL.GSTP.BANGSETGET;

namespace BL.GSTP.TP_THADS
{
    public class DVCQG_THANH_TOAN_BL
    {
        public bool DVCQG_THANH_TOAN_INSERT_UPDATE(decimal _TONGDAT_ID,String _MALOAIVUVIEC)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_DVCQG_APP.THANH_TOAN_IN_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_TONGDAT_ID"].Value = _TONGDAT_ID;
            comm.Parameters["V_MALOAIVUVIEC"].Value = _MALOAIVUVIEC;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public bool DVCQG_THANH_TOAN_UP_TONGDAT(decimal _TONGDAT_ID, String _MALOAIVUVIEC)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_DVCQG_APP.THANH_TOAN_UP_TONGDAT", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_TONGDAT_ID"].Value = _TONGDAT_ID;
            comm.Parameters["V_MALOAIVUVIEC"].Value = _MALOAIVUVIEC;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public bool DVCQG_THANH_TOAN_INSERT_ANPHI(decimal _MAGIAIDOAN, decimal _DONID, decimal _DONXULY_ID, decimal _ANPHI_ID, String _MALOAIVUVIEC)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_DVCQG_APP.THANH_TOAN_IN_ANPHI", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_MAGIAIDOAN"].Value = _MAGIAIDOAN;
            comm.Parameters["V_DONID"].Value = _DONID;
            comm.Parameters["V_DONXULY_ID"].Value = _DONXULY_ID;
            comm.Parameters["V_ANPHI_ID"].Value = _ANPHI_ID;
            comm.Parameters["V_MALOAIVUVIEC"].Value = _MALOAIVUVIEC;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public void DVCQG_TT_REMOVE_TONGDAT(decimal _TONGDAT_ID, String _MALOAIVUVIEC,ref decimal V_VALUE)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_DVCQG_APP.TT_REMOVE_TONGDAT", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_TONGDAT_ID"].Value = _TONGDAT_ID;
            comm.Parameters["V_MALOAIVUVIEC"].Value = _MALOAIVUVIEC;
            comm.Parameters["V_VALUE"].Direction = ParameterDirection.Output;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
            }
            catch
            {
                tran.Rollback();
            }
            finally
            {
                V_VALUE = Convert.ToDecimal(comm.Parameters["V_VALUE"].Value.ToString());
                conn.Close();
            }
        }
        public void DVCQG_THANH_TOAN_DELETE_XULY(decimal _DONXULY_ID,String _MALOAIVUVIEC, ref decimal V_VALUE)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_DVCQG_APP.THANH_TOAN_DELETE_XLY", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_DONXULY_ID"].Value = _DONXULY_ID;
            comm.Parameters["V_MALOAIVUVIEC"].Value = _MALOAIVUVIEC;
            comm.Parameters["V_VALUE"].Direction = ParameterDirection.Output;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
            }
            catch
            {
                tran.Rollback();
            }
            finally
            {
                V_VALUE = Convert.ToDecimal(comm.Parameters["V_VALUE"].Value.ToString());
                conn.Close();
            }
        }
        public string DVCQG_THANH_TOAN_SEARCH(decimal _MAGIAIDOAN, decimal _DONXULY_ID, String _MALOAIVUVIEC)
        {
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("V_MAGIAIDOAN", _MAGIAIDOAN),
            new OracleParameter("V_DONXULY_ID",_DONXULY_ID),
                new OracleParameter("V_MALOAIVUVIEC",_MALOAIVUVIEC),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            string V_MA_THONGBAO = "";
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_DVCQG.THANH_TOAN_SEARCH", parameters);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                V_MA_THONGBAO = tbl.Rows[0]["MA_THONGBAO"].ToString();
            }
            return V_MA_THONGBAO;
        }
    }
}