﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class TAM_UNG_AN_PHI_BL
    {
        public DataTable GetAll_AnPhi_Search(string _DATE_FROM, string _DATE_TO, string _STATUS, string _USERNAME, string _DONVITHA_ID, string vTuKhoaBasic, decimal PageIndex, decimal PageSize)
        {

            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_DATE_FROM",_DATE_FROM),
                new OracleParameter("V_DATE_TO",_DATE_TO),
                new OracleParameter("V_STATUS",_STATUS),
                new OracleParameter("V_USERNAME",_USERNAME),
                new OracleParameter("V_DONVITHA_ID",_DONVITHA_ID),
                new OracleParameter("vTuKhoaBasic",vTuKhoaBasic),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)

            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_TUPHAP_ANPHI.TC_DANHSACH_ANPHI", parameters);
            return tbl;
        }
        public DataTable Get_DONID_AnPhi(string _MALOAIVUVIEC, string _USERNAME, decimal V_DONID)
        {

            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                 new OracleParameter("V_MALOAIVUVIEC",_MALOAIVUVIEC),
                 new OracleParameter("V_USERNAME",_USERNAME),
                new OracleParameter("V_DONID",V_DONID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_TUPHAP_ANPHI.LOAD_EDIT", parameters);
            return tbl;
        }
        public DataTable GetAll_AnPhi_Search_DS(string _DATE_FROM, string _DATE_TO, string _STATUS, string _USERNAME, string _DONVITHA_ID, string vTuKhoaBasic, decimal PageIndex, decimal PageSize)
        {

            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_DATE_FROM",_DATE_FROM),
                new OracleParameter("V_DATE_TO",_DATE_TO),
                new OracleParameter("V_STATUS",_STATUS),
                new OracleParameter("V_USERNAME",_USERNAME),
                new OracleParameter("V_DONVITHA_ID",_DONVITHA_ID),
                new OracleParameter("vTuKhoaBasic",vTuKhoaBasic),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_TUPHAP_ANPHI.TC_DANHSACH_ANPHI_IN_DS", parameters);
            return tbl;
        }
        public DataTable Get_ThongBaoSoLuong(string _USERNAME, string _DONVITHA_ID)
        {

            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_USERNAME",_USERNAME),
                new OracleParameter("V_DONVITHA_ID",_DONVITHA_ID)

            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_TUPHAP_ANPHI.GET_SOLUONG", parameters);
            return tbl;
        }
        public DataTable GetAll_report(string v_Names, Decimal _OPTIONS, string _DATE_FROM, string _DATE_TO, string _DONVITHA_ID, string _CAP_THA)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("v_Names",v_Names),
                new OracleParameter("V_OPTIONS",_OPTIONS),
                new OracleParameter("V_DATE_FROM",_DATE_FROM),
                new OracleParameter("V_DATE_TO",_DATE_TO),
                new OracleParameter("V_DONVITHA_ID",_DONVITHA_ID),
                new OracleParameter("V_CAP_THA",_CAP_THA)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_TUPHAP_REPORT.GET_REPORT_THADS", parameters);
            return tbl;
        }
    }
}