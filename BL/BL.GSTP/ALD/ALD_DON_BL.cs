﻿using DAL.GSTP;
using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class ALD_DON_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable DON_SEARCH(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD, string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON, string V_UTTPDI, decimal vchecktk, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("V_UTTP",V_UTTPDI),
                        new OracleParameter("vchecktk",vchecktk),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_LAODONG.DON_SEARCH", parameters);
            return tbl;
        }
        public DataTable DON_SEARCH_PCTP(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD, string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_PCTP_LAODONG.DON_SEARCH", parameters);
            return tbl;
        }
        public DataTable DON_SEARCH_PCTP_PRINT(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD, string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize)
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_PCTP_LAODONG.DON_SEARCH_PRINT", parameters);
            return tbl;
        }
        public bool PHANCONGTP_INS_UP(String V_TRANGTHAI, String V_THAMPHANGQ_ID, String V_DONID, String V_NGAYPHANCONGTP, String V_THAM_PHAN_ID, String V_NGAYPHANCONGLD, String V_PHANCONGLD_ID, String V_CAPXX, String V_NGUOITAO, ref Decimal V_COUNTS, ref String V_THONGBAO)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_PCTP_LAODONG.PCTP_INS_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_TRANGTHAI"].Value = V_TRANGTHAI;
            comm.Parameters["V_THAMPHANGQ_ID"].Value = V_THAMPHANGQ_ID;
            comm.Parameters["V_DONID"].Value = V_DONID;
            comm.Parameters["V_NGAYPHANCONGTP"].Value = V_NGAYPHANCONGTP;
            comm.Parameters["V_THAM_PHAN_ID"].Value = V_THAM_PHAN_ID;
            comm.Parameters["V_NGAYPHANCONGLD"].Value = V_NGAYPHANCONGLD;
            comm.Parameters["V_PHANCONGLD_ID"].Value = V_PHANCONGLD_ID;
            comm.Parameters["V_CAPXX"].Value = V_CAPXX;
            comm.Parameters["V_NGUOITAO"].Value = V_NGUOITAO;
            comm.Parameters["V_COUNTS"].Direction = ParameterDirection.Output;
            comm.Parameters["V_THONGBAO"].Direction = ParameterDirection.Output;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                V_COUNTS = Convert.ToDecimal(comm.Parameters["V_COUNTS"].Value.ToString());
                V_THONGBAO = Convert.ToString(comm.Parameters["V_THONGBAO"].Value.ToString());
                conn.Close();
            }
        }
        public decimal GETNEWTT(decimal donviID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_DON_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;
        }
        //---------
        public decimal CHECKSTT_ALD(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile, decimal vSTT)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                        new OracleParameter("vNam",vNam),
                                                                        new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("vSTT",vSTT),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_LAODONG.ALD_FILE_CHECKSTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]);

        }

        public DataTable ALD_DON_TGTT_GETLIST(decimal vDONID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_DON_TGTT_GETLIST", parameters);
            return tbl;
        }
        public DataTable ALD_DON_BGTL_GETLIST(decimal vDONID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vDONID",vDONID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_DON_BGTL_GETLIST", parameters);
            return tbl;
        }
        public DataTable ALD_DON_BGTT_GETINFO(decimal vDONID, string NguoiGiao, decimal NguoiNhan, string NgayBanGiao)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("vNguoiGiao",NguoiGiao),
                                                                        new OracleParameter("vNguoiNhan",NguoiNhan),
                                                                        new OracleParameter("vNgayBanGiao",NgayBanGiao),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_DON_BGTT_GETINFO", parameters);
            return tbl;
        }
        public DataTable ALD_DON_GETTOAANBYVUVIEC(decimal vDonID)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vDONID",vDonID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("ALD_DON_GETTOAANBYVUVIEC", prm);
        }
        public DataTable ALD_DON_BanGiaoTaiLieu_GETLIST(decimal vDONID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vDONID",vDONID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_DON_BGTL_GETLIST", parameters);
            return tbl;
        }

        public DataTable ALD_TONGDAT_DOITUONG_GETBY(decimal vDONID, decimal vTOAANID, decimal vBIEUMAUID, decimal vIsOnlyNKK)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vTOAANID",vTOAANID),
                                                                          new OracleParameter("vBIEUMAUID",vBIEUMAUID),
                                                                          new OracleParameter("vIsOnlyNKK",vIsOnlyNKK),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_TONGDAT_DOITUONG_GETBY", parameters);
            return tbl;
        }
        public DataTable ALD_TONGDAT_GETLIST(decimal vDONID, decimal vToaAnID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_TONGDAT_GETLIST", parameters);
            return tbl;
        }
        public decimal GETFILENEWTT(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                         new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                          new OracleParameter("vNam",vNam),
                                                                           new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_FILE_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }

        public DataTable ALD_FILE_TONGDAT(decimal vDonID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ALD_FILE_TONGDAT", parameters);
            return tbl;
        }
        public bool DELETE_ALLDATA_BY_VUANID(string donID)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] { new OracleParameter("in_VUANID", donID) };
                decimal dbl = Cls_Comon.ExcuteProcResult("PKG_GSTP_DELETE.DELETE_DATA_ALD_BY_VUANID", parameters);
                return dbl == 1 ? true : false;
            }
            catch { return false; }
        }
        public bool Check_ThuLy(decimal DonID)
        {
            ALD_SOTHAM_THULY ObjThuLy = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == DonID).FirstOrDefault();
            if (ObjThuLy != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}