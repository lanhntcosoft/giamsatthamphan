﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class CANH_BAO_STPT
    {
        public DataTable GET_CANHBAO_TRAIGIAM(String V_LOAITOA, String V_DONVIID, int PageIndex, int PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                    new OracleParameter("V_LOAITOA",V_LOAITOA),
                    new OracleParameter("V_DONVIID",V_DONVIID),
                    new OracleParameter("PageIndex",PageIndex),
                    new OracleParameter("PageSize", PageSize),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT.GETBY_TAMGIAM_10NGAY", parameters);
            return tbl;
        }
        public DataTable GET_CANHBAO_HOSO_VKS(String V_DONVIID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_DONVIID",V_DONVIID),                                                                      
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_HOSO_PT.GETBY_COUNT_VKS", parameters);
            return tbl;
        }
        public DataTable GET_CANHBAO_VUAN_TAMDINHCHI(String V_CAPXX, String V_TOAAN_ID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("V_CAPXX",V_CAPXX),
                                                                        new OracleParameter("V_DONVIID",V_TOAAN_ID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_APP.CANHBAO_TDC_THOIHAN", parameters);
            return tbl;
        }
        public DataTable GET_STPT_LOGIN_CA_SOTHAM(String V_CAPXX, String V_TOAAN_ID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("V_CAPXX",V_CAPXX),
                                                                        new OracleParameter("V_DONVIID",V_TOAAN_ID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP_CACC_GET.SOTHAM_CA", parameters);
            return tbl;
        }
        public DataTable GET_STPT_LOGIN_CA_PHUCTHAM(String V_CAPXX, String V_TOAAN_ID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("V_CAPXX",V_CAPXX),
                                                                        new OracleParameter("V_DONVIID",V_TOAAN_ID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP_CACC_GET.PHUCTHAM_CA", parameters);
            return tbl;
        }
        public DataTable GET_DONKK_TOAKHAC_CHUYENDEN(String v_toa_an_id)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                    new OracleParameter("v_toa_an_id",v_toa_an_id),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT.SOLUONGDONKK_TOAKHAC", parameters);
            return tbl;
        }
    }
}