﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class TongHop_BL
    {
      
        public DataTable GETVUVIEC_NHANAN(decimal vToaAnID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.GETVUVIEC_NHANAN", parameters);
            return tbl;
        }

        public DataTable DS_NHANAN(decimal vToaAnID,string vMavuviec,string vTenvuviec,string vToachuyen,decimal vTruongHopGiaoNhan,DateTime? vTungay,DateTime? vDenngay,decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.DS_NHANAN", parameters);
            return tbl;
        }
        public DataTable DS_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.DS_CHUYENAN", parameters);
            return tbl;
        }

        public DataTable HC_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.HC_NHANAN", parameters);
            return tbl;
        }
   
        public DataTable HC_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.HC_CHUYENAN", parameters);
            return tbl;
        }


        public DataTable HN_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.HN_NHANAN", parameters);
            return tbl;
        }

        public DataTable HN_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.HN_CHUYENAN", parameters);
            return tbl;
        }

        public DataTable KT_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.KT_NHANAN", parameters);
            return tbl;
        }

        public DataTable KT_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.KT_CHUYENAN", parameters);
            return tbl;
        }


        public DataTable LD_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.LD_NHANAN", parameters);
            return tbl;
        }

        public DataTable LD_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.LD_CHUYENAN", parameters);
            return tbl;
        }
        public DataTable PS_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.PS_NHANAN", parameters);
            return tbl;
        }

        public DataTable PS_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.PS_CHUYENAN", parameters);
            return tbl;
        }
        public DataTable XLHC_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.XLHC_NHANAN", parameters);
            return tbl;
        }

        public DataTable XLHC_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.XLHC_CHUYENAN", parameters);
            return tbl;
        }
        public DataTable HS_NHANAN(decimal vToaAnID, string vMavuviec, string vTenvuviec, string vToachuyen, decimal vTruongHopGiaoNhan, DateTime? vTungay, DateTime? vDenngay, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vToachuyen",vToachuyen),
                                                                        new OracleParameter("vTruongHopGiaoNhan",vTruongHopGiaoNhan),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS_BC.HS_NHANAN", parameters);
            return tbl;
        }

        public DataTable HS_CHUYENAN(decimal vToaAnID, decimal vToaAnNhanID, string vMavuviec, string vTenvuviec, string vSoQD, string vSoBA, DateTime? vTungay, DateTime? vDenngay, string vDuongsu, decimal vTrangthai)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaAnNhanID",vToaAnNhanID),
                                                                        new OracleParameter("vMavuviec",vMavuviec),
                                                                        new OracleParameter("vTenvuviec",vTenvuviec),
                                                                        new OracleParameter("vSoQD",vSoQD),
                                                                        new OracleParameter("vSoBA",vSoBA),
                                                                        new OracleParameter("vTungay",vTungay),
                                                                        new OracleParameter("vDenngay",vDenngay),
                                                                         new OracleParameter("vDuongsu",vDuongsu),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_QLA_TH.HS_CHUYENAN", parameters);
            return tbl;
        }

        public DataTable THONGKE_STPT_CHANH_AN(decimal vToaAnID,  DateTime? vFromDate, DateTime? vToDate)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vFromDate",vFromDate),
                                                                        new OracleParameter("vToDate",vToDate),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("THONGKE_STPT_CHANH_AN", parameters);
            return tbl;
        }
        public DataTable TRANGCHU_THAMPHAN(decimal vToaAnID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("V_TOAANID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("TRANGCHU_THAMPHAN", parameters);
            return tbl;
        }
    }
}