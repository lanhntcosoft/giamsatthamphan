﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL.GSTP;
namespace BL.GSTP.ADS
{
    public class STPT_BAOCAO_BL
    {
        public DataTable Tong_hop_so_lieu_xx(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, string V_CAP_XET_XU_LOGIN,string v_toaan_id, string V_TUNGAY, string V_DENNGAY, string V_LOAIAN_ID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                        new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("v_toaan_id", v_toaan_id),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_LOAIAN_ID",V_LOAIAN_ID)
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_BC.THSL_XX", parameters);
            return tbl;
        }
        //Thongke_HCTP
        public DataTable Thongke_HCTP(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, decimal donviID, string MaChucDanh, string V_TUNGAY, string V_DENNGAY, string V_LOAIAN_ID,string V_CAP_XET_XU_LOGIN)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                        new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("vDonViID",donviID),
                        new OracleParameter("vChucDanh",MaChucDanh),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_BC.Thongke_HCTP", parameters);
            return tbl;
        }

        public DataTable Thongke_TTP(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, decimal vdonviID, decimal vThamphanId, string V_TUNGAY, string V_DENNGAY, string V_CAP_XET_XU_LOGIN)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                        new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("vDonViID",vdonviID),
                        new OracleParameter("vThamphanId",vThamphanId),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_BC.Thongke_TTP", parameters);
            return tbl;
        }

        public DataTable Thongke_TLA(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, decimal vdonviID, string vLoaiAnId,string vtenloaian, string V_TUNGAY, string V_DENNGAY, string V_CAP_XET_XU_LOGIN)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                        new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("vDonViID",vdonviID),
                        new OracleParameter("vLoaiAnId",vLoaiAnId),
                        new OracleParameter("vtenloaian",vtenloaian),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_BC.Thongke_TLA", parameters);
            return tbl;
        }

        public DataTable TL_XX_TRINHTU_PT_EXPORT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, string V_CAP_XET_XU_LOGIN, string v_toaan_id, string V_TUNGAY, string V_DENNGAY, string V_LOAIAN_ID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                        new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                        new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("v_toaan_id", v_toaan_id),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_LOAIAN_ID",V_LOAIAN_ID)
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_BC.TL_XX_TRINHTU_PT", parameters);
            return tbl;
        }
        //Bao cao tinh hinh nhap lieu 
        public DataTable Tong_hop_so_lieu_nhaplieu(decimal v_toaan_id, string v_TINHTRANG_THULY, string v_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, decimal v_SearchAll)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue), 
                        new OracleParameter("v_toaan_id", v_toaan_id),
                        new OracleParameter("v_TINHTRANG_THULY",v_TINHTRANG_THULY),
                        new OracleParameter("v_TINHTRANG_GIAIQUYET",v_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("v_SearchAll",v_SearchAll)
                        };

            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_SEARCH_ALL.NHAPLIEU_HS_DS_EXT_ALL", parameters);
            return tbl;
        }
    }
}