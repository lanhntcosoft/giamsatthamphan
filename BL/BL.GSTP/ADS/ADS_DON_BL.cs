﻿using DAL.GSTP;
using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class ADS_DON_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable DON_SEARCH(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD,string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON,string V_UTTPDI, decimal vchecktk, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("V_UTTP",V_UTTPDI),
                        new OracleParameter("vchecktk",vchecktk),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS.DON_SEARCH", parameters);
            return tbl;
        }


        public DataTable DON_SEARCH_PCTP(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD, string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_PCTP_DS.DON_SEARCH", parameters);
            return tbl;
        }
        public DataTable DON_SEARCH_PCTP_PRINT(string V_CAP_XET_XU_LOGIN, string V_TEN_VU_AN, string V_QHPL, string V_MA_VU_AN, string V_TENDUONGSU, string V_CAPXX, string V_TOAAN_ID, string V_TINHTRANG_THULY, string V_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string V_THAMPHAN_ID, string V_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string V_KETQUA, string V_SO_QD, string V_NGAY_QD, string V_THUKY_ID, string V_THOIHAN_GQ, string V_LOAIDON, string V_PT_RKINHNGHIEM, string V_GQDON, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                         new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("V_TEN_VU_AN",V_TEN_VU_AN),
                        new OracleParameter("V_QHPL",V_QHPL),
                        new OracleParameter("V_MA_VU_AN",V_MA_VU_AN),
                        new OracleParameter("V_TENDUONGSU",V_TENDUONGSU),
                        new OracleParameter("V_CAPXX",V_CAPXX),
                        new OracleParameter("V_TOAAN_ID", V_TOAAN_ID),
                        new OracleParameter("V_TINHTRANG_THULY",V_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("V_SOTHULY",V_SOTHULY),
                        new OracleParameter("V_THAMPHAN_ID",V_THAMPHAN_ID),
                        new OracleParameter("V_TINHTRANG_GIAIQUYET",V_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("V_KETQUA", V_KETQUA),
                        new OracleParameter("V_SO_QD", V_SO_QD),
                        new OracleParameter("V_NGAY_QD", V_NGAY_QD),
                        new OracleParameter("V_THUKY_ID",V_THUKY_ID),
                        new OracleParameter("V_THOIHAN_GQ",V_THOIHAN_GQ),
                        new OracleParameter("V_LOAIDON",V_LOAIDON),
                        new OracleParameter("V_PT_RKINHNGHIEM",V_PT_RKINHNGHIEM),
                        new OracleParameter("V_GQDON",V_GQDON),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize)
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_PCTP_DS.DON_SEARCH_PRINT", parameters);
            return tbl;
        }
        public bool PHANCONGTP_INS_UP(String V_TRANGTHAI, String V_THAMPHANGQ_ID, String V_DONID, String V_NGAYPHANCONGTP, String V_THAM_PHAN_ID, String V_NGAYPHANCONGLD, String V_PHANCONGLD_ID, String V_CAPXX, String V_NGUOITAO, ref Decimal V_COUNTS, ref String V_THONGBAO)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_PCTP_DS.PCTP_INS_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_TRANGTHAI"].Value = V_TRANGTHAI;
            comm.Parameters["V_THAMPHANGQ_ID"].Value = V_THAMPHANGQ_ID;
            comm.Parameters["V_DONID"].Value = V_DONID;
            comm.Parameters["V_NGAYPHANCONGTP"].Value = V_NGAYPHANCONGTP;
            comm.Parameters["V_THAM_PHAN_ID"].Value = V_THAM_PHAN_ID;
            comm.Parameters["V_NGAYPHANCONGLD"].Value = V_NGAYPHANCONGLD;
            comm.Parameters["V_PHANCONGLD_ID"].Value = V_PHANCONGLD_ID;
            comm.Parameters["V_CAPXX"].Value = V_CAPXX;
            comm.Parameters["V_NGUOITAO"].Value = V_NGUOITAO;
            comm.Parameters["V_COUNTS"].Direction = ParameterDirection.Output;
            comm.Parameters["V_THONGBAO"].Direction = ParameterDirection.Output;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                V_COUNTS = Convert.ToDecimal(comm.Parameters["V_COUNTS"].Value.ToString());
                V_THONGBAO = Convert.ToString(comm.Parameters["V_THONGBAO"].Value.ToString());
                conn.Close();
            }
        }
        public decimal GETNEWTT(decimal donviID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_DON_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }
        public decimal GETFILENEWTT(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                         new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                          new OracleParameter("vNam",vNam),
                                                                           new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_FILE_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }
        public decimal CHECKSTT_ADS(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile, decimal vSTT)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                        new OracleParameter("vNam",vNam),
                                                                        new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("vSTT",vSTT),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DS.ADS_FILE_CHECKSTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]);

        }
        public DataTable ADS_FILE_GETBYDON(decimal vDonID, decimal vLoaiAn, decimal vMaGiaiDoan)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vLoaiAn",vLoaiAn),
                                                                        new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT.ADS_FILE_GETBYDON", parameters);
            //tbl.DefaultView.Sort = "THUTU ASC";
            return tbl;
        }
        public DataTable ADS_FILE_TONGDAT(decimal vDonID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_FILE_TONGDAT", parameters);
            return tbl;
        }
        public DataTable ADS_DON_TGTT_GETLIST(decimal vDONID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_DON_TGTT_GETLIST", parameters);
            return tbl;
        }
        public DataTable ADS_DON_BanGiaoTaiLieu_GETLIST(decimal vDONID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vDONID",vDONID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_DON_BGTL_GETLIST", parameters);
            return tbl;
        }
        public DataTable ADS_DON_BGTT_GETINFO(decimal vDONID, string NguoiGiao, decimal NguoiNhan, string NgayBanGiao)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("vNguoiGiao",NguoiGiao),
                                                                        new OracleParameter("vNguoiNhan",NguoiNhan),
                                                                        new OracleParameter("vNgayBanGiao",NgayBanGiao),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_DON_BGTT_GETINFO", parameters);
            return tbl;
        }
        public DataTable ADS_DON_GETTOAANBYVUVIEC(decimal vDonID)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vDONID",vDonID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("ADS_DON_GETTOAANBYVUVIEC", prm);
        }
        public DataTable ADS_TONGDAT_DOITUONG_GETBY(decimal vDONID, decimal vTOAANID, decimal vBIEUMAUID, decimal vIsOnlyNKK)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vTOAANID",vTOAANID),
                                                                          new OracleParameter("vBIEUMAUID",vBIEUMAUID),
                                                                          new OracleParameter("vIsOnlyNKK",vIsOnlyNKK),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_TONGDAT_DOITUONG_GETBY", parameters);
            return tbl;
        }
        public DataTable ADS_TONGDAT_GETLIST(decimal vDONID, decimal vToaAnID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("ADS_TONGDAT_GETLIST", parameters);
            return tbl;
        }

        public bool DELETE_ALLDATA_BY_VUANID(string donID)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] { new OracleParameter("in_VUANID", donID) };
                decimal dbl = Cls_Comon.ExcuteProcResult("PKG_GSTP_DELETE.DELETE_DATA_ADS_BY_VUANID", parameters);
                return dbl == 1 ? true : false;
            }
            catch (Exception ex) { return false; }
        }
        public bool Check_ThuLy(decimal DonID)
        {
            ADS_SOTHAM_THULY ObjThuLy = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == DonID).FirstOrDefault();
            if (ObjThuLy != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable STPT_DANHSACH_AN_DANSUMORONG(string V_CAP_XET_XU_LOGIN, string v_ten_vu_an, string v_toidanh, string v_ma_vu_an, string v_bi_can, string v_Capxx, string v_toaan_id, string v_TINHTRANG_THULY, string v_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN, string v_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY, string v_KETQUA, string v_so_qd, string v_ngay_qd, string v_thamphan_id, string v_thuky_id, string v_THOIHAN_GQ, string v_QD_TAMGIAM, string V_UTTPDI, string V_LOAIAN_ID, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("V_CAP_XET_XU_LOGIN",V_CAP_XET_XU_LOGIN),
                        new OracleParameter("v_ten_vu_an",v_ten_vu_an),
                        new OracleParameter("v_toidanh",v_toidanh),
                        new OracleParameter("v_ma_vu_an",v_ma_vu_an),
                        new OracleParameter("v_bi_can",v_bi_can),
                        new OracleParameter("v_Capxx",v_Capxx),
                        new OracleParameter("v_toaan_id", v_toaan_id),
                        new OracleParameter("v_TINHTRANG_THULY",v_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("v_SOTHULY",v_SOTHULY),
                        new OracleParameter("v_TINHTRANG_GIAIQUYET",v_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),
                        new OracleParameter("v_KETQUA", v_KETQUA),
                        new OracleParameter("v_so_qd", v_so_qd),
                        new OracleParameter("v_ngay_qd", v_ngay_qd),
                        new OracleParameter("v_thamphan_id",v_thamphan_id),
                        new OracleParameter("v_thuky_id",v_thuky_id),
                        new OracleParameter("v_THOIHAN_GQ",v_THOIHAN_GQ),
                        new OracleParameter("v_QD_TAMGIAM",v_QD_TAMGIAM),
                        new OracleParameter("V_UTTP",V_UTTPDI),
                        new OracleParameter("V_LOAIAN_ID",V_LOAIAN_ID),
                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_DANHSACH.DANHSACH_AN_DANSUMORONG", parameters);
            return tbl;
        }

    }
}