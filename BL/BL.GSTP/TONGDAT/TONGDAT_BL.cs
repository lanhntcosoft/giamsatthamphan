﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL.GSTP;


namespace BL.GSTP.TONGDAT
{
    public class TONGDAT_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable Get_All_VANBANTONGDAT(string v_toaan_id, string V_LOAIAN_ID, string v_ten_vu_an, string VanBANTD
            , string v_so_qd, string v_ngay_qd, string TrangThaiTD, string vHinhthucTD
            , string v_bi_can, string v_thuky_id, string v_thamphan_id
            , string v_TINHTRANG_THULY, string v_SOTHULY, string V_NGAYTHULY_TU, string V_NGAYTHULY_DEN

            , string v_TINHTRANG_GIAIQUYET, string V_TUNGAY, string V_DENNGAY
            , decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("v_toaan_id", v_toaan_id),
                        new OracleParameter("V_LOAIAN_ID",V_LOAIAN_ID),
                        new OracleParameter("v_ten_vu_an",v_ten_vu_an),
                        new OracleParameter("v_VanBANTD", VanBANTD),
                        new OracleParameter("v_So_VB", v_so_qd),
                        new OracleParameter("v_Ngay_VB", v_ngay_qd),
                        new OracleParameter("v_TrangThaiTD",TrangThaiTD),
                        new OracleParameter("v_HinhthucTD",vHinhthucTD),
                        new OracleParameter("v_bi_can",v_bi_can),
                        new OracleParameter("v_thuky_id",v_thuky_id),
                        new OracleParameter("v_thamphan_id",v_thamphan_id),

                        new OracleParameter("v_TINHTRANG_THULY",v_TINHTRANG_THULY),
                        new OracleParameter("V_NGAYTHULY_TU",V_NGAYTHULY_TU),
                        new OracleParameter("V_NGAYTHULY_DEN",V_NGAYTHULY_DEN),
                        new OracleParameter("v_SOTHULY",v_SOTHULY),

                        new OracleParameter("v_TINHTRANG_GIAIQUYET",v_TINHTRANG_GIAIQUYET),
                        new OracleParameter("V_TUNGAY",V_TUNGAY),
                        new OracleParameter("V_DENNGAY",V_DENNGAY),

                        new OracleParameter("Page_Index",PageIndex),
                        new OracleParameter("Page_Size",PageSize),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_VANTHU_TONGDAT.SEARCH_ALL_VANBANTONGDAT", parameters);
            return tbl;
        }
        public DataTable DM_BIEUMAU_TONGDAT_GETALL(decimal loaivuviec)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("CURR_LOAIVUVIEC",loaivuviec),
                                                                        new OracleParameter("return_page",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_VANTHU_TONGDAT.DM_BIEUMAU_TONGDAT_GETALL", parameters);
            return tbl;
        }

        public DataTable GET_VANTHU_TONGDAT_GETLIST(decimal vDONID, decimal vToaAnID, decimal vLoaian, decimal vBieuMauID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                      new OracleParameter("vDONID",vDONID),
                                                                      new OracleParameter("vToaAnID",vToaAnID),
                                                                      new OracleParameter("vLoaian",vLoaian),
                                                                      new OracleParameter("vBieuMauID",vBieuMauID),
                                                                      new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_VANTHU_TONGDAT.VANTHU_TONGDAT_GETLIST", parameters);
            return tbl;
        }
        public DataTable FILE_TONGDAT(decimal vTongDatID,decimal vLoaian,decimal VuAnID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vTongDatID",vTongDatID),
                                                                        new OracleParameter("vLoaian",vLoaian),
                                                                        new OracleParameter("vVuAnID",VuAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_VANTHU_TONGDAT.ALL_FILE_TONGDAT", parameters);
            return tbl;
        }
        public DataTable TONGDAT_DOITUONG_GETBY(decimal vVuAnID,decimal vLoaian, decimal vToaAnID, decimal vBieuMauID, decimal vIsOnlyNKK)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vVuAnID",vVuAnID),
                                                                        new OracleParameter("vLoaian",vLoaian),
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vBieuMauID",vBieuMauID),
                                                                        new OracleParameter("vIsOnlyNKK",vIsOnlyNKK),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_VANTHU_TONGDAT.TONGDAT_DOITUONG_GETBY", parameters);
            return tbl;
        }
    }
}
