﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class AHN_DON_XULY_BL
    {
        public DataTable GetByDonID(decimal donID, int PageIndex, int PageSize)
        {       
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("CurrDonID",donID),
                                                                        new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),                                                                        
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                  };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("AHN_DON_XULY_GETBYDONID", parameters);
            return tbl;
        }
        public DataTable GetTinhTrangVuViec(decimal vu_viec_id)
        {
            string ma_loai = ENUM_LOAIAN.AN_HONNHAN_GIADINH;
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vu_viec_id",vu_viec_id),
                                                                        new OracleParameter("ma_loai_vu_viec",ma_loai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("AHN_Don_GetTinhTrang", parameters);
            return tbl;
        }
    }
}