using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BL.GSTP.BANGSETGET
{
    public partial class DVCQG_THANH_TOAN
    {
        public decimal ID { get; set; }
        public string MA_THONGBAO { get; set; }
        public Nullable<decimal> DONID { get; set; }
        public string MALOAIVUVIEC { get; set; }
        public string DONVITHA_ID { get; set; }
        public Nullable<decimal> MALOAIHINHTHU { get; set; }
        public string MAVUVIEC { get; set; }
        public string SOTAIKHOANKB { get; set; }
        public string MAKHOBAC { get; set; }
        public string TENKHOBAC { get; set; }
        public string MACHICUC { get; set; }
        public string TENCHICUC { get; set; }
        public string NGAYTHONGBAO { get; set; }
        public string SOTHONGBAO { get; set; }
        public string TENLOAIHINHTHU { get; set; }
        public string HOTENNGUOINOP { get; set; }
        public string SOCMNDNGUOINOP { get; set; }
        public string DIACHINGUOINOP { get; set; }
        public string HUYENNGUOINOP { get; set; }
        public string TINHNGUOINOP { get; set; }
        public string TONGTIEN { get; set; }
        public string HOTENNGUOINOPTIEN { get; set; }
        public string DIACHINGUOINOPTIEN { get; set; }
        public string TINHNGUOINOPTIEN { get; set; }
        public string HUYENNGUOINOPTIEN { get; set; }
        public string XANGUOINOPTIEN { get; set; }
        public string URLBIENLAI { get; set; }
        public string THOIGIANTHANHTOAN { get; set; }
        public string SOTIEN { get; set; }
        public Nullable<decimal> TRANGTHAITHANHTOAN { get; set; }
        public Nullable<System.DateTime> NGAY_TAO { get; set; }
        public Nullable<System.DateTime> NGAY_SUA { get; set; }
    }
}
