﻿using DAL.GSTP;
using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP
{
    public class XLHC_DON_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable XLHC_DON_SEARCH(decimal vdonviID, string vMaVuViec, string vTenVuViec, DateTime? vNgayNhanTu, DateTime? vNgayNhanDen, decimal vLoaiQuanHe, decimal vQuanHePLID, decimal vSoThuTu, string vDuongSu, decimal vTrangThai, decimal vHinhThucNhanDon, decimal thamphan_id,decimal Thukyid, decimal PhanCongTP, decimal PageIndex, decimal PageSize)
        {
            if (vNgayNhanTu == DateTime.MinValue) vNgayNhanTu = null;
            if (vNgayNhanDen == DateTime.MinValue) vNgayNhanDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",vdonviID),
                                                                        new OracleParameter("vMaVuViec",vMaVuViec),
                                                                        new OracleParameter("vTenVuViec",vTenVuViec),
                                                                        new OracleParameter("vNgayNhanTu",vNgayNhanTu),
                                                                        new OracleParameter("vNgayNhanDen",vNgayNhanDen),
                                                                        new OracleParameter("vLoaiQuanHe",vLoaiQuanHe),
                                                                        new OracleParameter("vQuanHePLID",vQuanHePLID),
                                                                        new OracleParameter("vSoThuTu",vSoThuTu),
                                                                        new OracleParameter("vDuongSu",vDuongSu),
                                                                        new OracleParameter("vTrangThai",vTrangThai),
                                                                        new OracleParameter("vHinhThucNhanDon",vHinhThucNhanDon),
                                                                        new OracleParameter("thamphan_id",thamphan_id),
                                                                        new OracleParameter("vThuKyId",Thukyid),
                                                                        new OracleParameter("PhanCongTP",PhanCongTP),
                                                                        new OracleParameter("Page_Index",PageIndex),
                                                                        new OracleParameter("Page_Size",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_DON_SEARCH", parameters);
            return tbl;
        }
        public decimal GETNEWTT(decimal donviID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_DON_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;
        }
        public DataTable XLHC_DON_TGTT_GETLIST(decimal vDONID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_DON_TGTT_GETLIST", parameters);
            return tbl;
        }
        public DataTable XLHC_DON_BanGiaoTaiLieu_GETLIST(decimal vDONID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vDONID",vDONID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_DON_BGTL_GETLIST", parameters);
            return tbl;
        }
        public DataTable XLHC_DON_BGTT_GETINFO(decimal vDONID, string NguoiGiao, decimal NguoiNhan, string NgayBanGiao)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("vNguoiGiao",NguoiGiao),
                                                                        new OracleParameter("vNguoiNhan",NguoiNhan),
                                                                        new OracleParameter("vNgayBanGiao",NgayBanGiao),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_DON_BGTT_GETINFO", parameters);
            return tbl;
        }
        public DataTable XLHC_DON_GETTOAANBYVUVIEC(decimal vDonID)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vDONID",vDonID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("XLHC_DON_GETTOAANBYVUVIEC", prm);
        }
        public DataTable XLHC_TONGDAT_DOITUONG_GETBY(decimal vDONID, decimal vTOAANID, decimal vBIEUMAUID, decimal vIsOnlyNKK)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vTOAANID",vTOAANID),
                                                                          new OracleParameter("vBIEUMAUID",vBIEUMAUID),
                                                                          new OracleParameter("vIsOnlyNKK",vIsOnlyNKK),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_TONGDAT_DOITUONG_GETBY", parameters);
            return tbl;
        }
        public DataTable XLHC_TONGDAT_GETLIST(decimal vDONID, decimal vToaAnID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("XLHC_TONGDAT_GETLIST", parameters);
            return tbl;
        }
        public bool DELETE_ALLDATA_BY_VUANID(string donID)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] { new OracleParameter("in_VUANID", donID) };
                decimal dbl = Cls_Comon.ExcuteProcResult("PKG_GSTP_DELETE.DELETE_DATA_XLHC_BY_VUANID", parameters);
                return dbl == 1 ? true : false;
            }
            catch { return false; }
        }
        public bool Check_ThuLy(decimal DonID)
        {
            XLHC_SOTHAM_THULY ObjThuLy = dt.XLHC_SOTHAM_THULY.Where(x => x.DONID == DonID).FirstOrDefault();
            if (ObjThuLy != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}