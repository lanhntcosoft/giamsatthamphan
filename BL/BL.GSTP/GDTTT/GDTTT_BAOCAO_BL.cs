﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL.GSTP;

namespace BL.GSTP.GDTTT
{
    public class GDTTT_BAOCAO_BL
    {
        ///-------------------------------------------------
           public DataTable BC_VGDKT_1_PRINT(string CHK_CONLAI_,string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
           , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
           , string vNguyendon, string vBidon
           , decimal vLoaiAn, decimal vThamtravien
           , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
           , decimal vTraloidon, decimal vLoaiCVID
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
           , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
           , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
           , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
           , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO.GDTTTT_VUAN_SEARCH_BC1", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_2_PRINT(string CHK_CONLAI_, string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
          , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
          , string vNguyendon, string vBidon
          , decimal vLoaiAn, decimal vThamtravien
          , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
          , decimal vTraloidon, decimal vLoaiCVID
          , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
          , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
          , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
          , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
          , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO.GDTTTT_VUAN_SEARCH_BC2", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_4_PRINT(decimal vToaAnID, decimal vPhongBanID
       , decimal vToaRaBAQD, string vSoBAQD, string vNgayBAQD
        , string vNguyendon, string vBidon, decimal vLoaiAn
        , decimal vThamtravien, decimal vLanhdao, decimal vThamphan

        , DateTime? vTuNgay, DateTime? vDenNgay, string vSoThuly
        , decimal vTrangthai, int captrinhtiep_id, int isdangkybc
        , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT
          , int ketquathuly, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
        , decimal PageIndex, decimal PageSize)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
            new OracleParameter("vToaAnID",vToaAnID),
            new OracleParameter("vPhongBanID",vPhongBanID),
            new OracleParameter("vToaRaBAQD",vToaRaBAQD),
            new OracleParameter("vSoBAQD",vSoBAQD),
            new OracleParameter("vNgayBAQD",vNgayBAQD),
            new OracleParameter("vNguyendon",vNguyendon),
            new OracleParameter("vBidon",vBidon),
            new OracleParameter("vLoaiAn",vLoaiAn),

            new OracleParameter("vThamtravien",vThamtravien),
            new OracleParameter("vLanhdao",vLanhdao),
            new OracleParameter("vThamphan",vThamphan),

            new OracleParameter("tt_TuNgay",vTuNgay),
            new OracleParameter("tt_DenNgay",vDenNgay),
            new OracleParameter("vSoThuly",vSoThuly),

            new OracleParameter("vTrangthai",vTrangthai),
            new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
            new OracleParameter("vIsDangKyBC",isdangkybc),

            new OracleParameter("isTTMuonHS",isMuonHoSo),
            new OracleParameter("isTTToTrinh", isToTrinh),
            new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
            new OracleParameter("isBuocTT",isBuocTT),

            new OracleParameter("vKetquathuly",ketquathuly),
            new OracleParameter("LoaiAnDB",LoaiAnDB),
            new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
            new OracleParameter("IsHoanTHA",IsHoanTHA),
            new OracleParameter("PageIndex",PageIndex),
            new OracleParameter("PageSize",PageSize)
           };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_TT.GDTTTT_QLTOTRINH_BC4", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_6_PRINT(string CHK_CONLAI_, string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
          , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
          , string vNguyendon, string vBidon
          , decimal vLoaiAn, decimal vThamtravien
          , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
          , decimal vTraloidon, decimal vLoaiCVID
          , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
          , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
          , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
          , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
          , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO.GDTTTT_VUAN_SEARCH_BC6", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_8_PRINT_GROUP(string CHK_CONLAI_, string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
         , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
         , string vNguyendon, string vBidon
         , decimal vLoaiAn, decimal vThamtravien
         , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
         , decimal vTraloidon, decimal vLoaiCVID
         , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
         , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
         , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
         , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
         , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO.GDTTTT_VUAN_SEARCH_BC8_GROUP", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_8_PRINT_ALL(string CHK_CONLAI_, string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
        , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
        , string vNguyendon, string vBidon
        , decimal vLoaiAn, decimal vThamtravien
        , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
        , decimal vTraloidon, decimal vLoaiCVID
        , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
        , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
        , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
        , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
        , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO.GDTTTT_VUAN_SEARCH_BC8_ALL", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_9_PRINT(string CHK_CONLAI_, string V_COLUME, string V_ASC_DESC, decimal vToaAnID, decimal vPhongBanID, decimal vToaRaBAQD, string vSoBAQD
          , string vNgayBAQD, string vNguoiGui, string vCoquanchuyendon
          , string vNguyendon, string vBidon
          , decimal vLoaiAn, decimal vThamtravien
          , decimal vLanhdao, decimal vThamphan, decimal vQHPLID, decimal vQHPLDNID
          , decimal vTraloidon, decimal vLoaiCVID
          , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen
          , string vSoThuly, decimal vTrangthai, decimal vKetquathuly, decimal vKetquaxetxu
          , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
          , int typetb, int isdangkybc, int captrinhtiep_id, int type_hoidongtp, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
          , decimal PageIndex, decimal PageSize)
        {
            if (vNgayThulyTu == DateTime.MinValue) vNgayThulyTu = null;
            if (vNgayThulyDen == DateTime.MinValue) vNgayThulyDen = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CONLAI_",CHK_CONLAI_),
                new OracleParameter("V_COLUME",V_COLUME),
                new OracleParameter("V_ASC_DESC",V_ASC_DESC),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vCoquanchuyendon",vCoquanchuyendon),
                new OracleParameter("vNguyendon",vNguyendon),
                new OracleParameter("vBidon",vBidon),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vThamtravien",vThamtravien),
                new OracleParameter("vLanhdao",vLanhdao),
                new OracleParameter("vThamphan",vThamphan),
                new OracleParameter("vQHPLID",vQHPLID),
                new OracleParameter("vQHPLDNID",vQHPLDNID),
                new OracleParameter("vTraloidon",vTraloidon),
                new OracleParameter("vLoaiCVID",vLoaiCVID),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
                new OracleParameter("vIsDangKyBC",isdangkybc),
                new OracleParameter("vKetquathuly",vKetquathuly),
                new OracleParameter("vKetquaxetxu",vKetquaxetxu),

                new OracleParameter("isTTMuonHS",isMuonHoSo),
                new OracleParameter("isTTToTrinh", isToTrinh),
                new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
                new OracleParameter("isBuocTT",isBuocTT),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
                new OracleParameter("IsHoanTHA",IsHoanTHA),
                new OracleParameter("vTypeTB",typetb),

                new OracleParameter("vTypeHDTP",type_hoidongtp),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),

                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA.GDTTTT_VUAN_SEARCH_BC9", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_10_PRINT(decimal vToaAnID, decimal vPhongBanID
       , decimal vToaRaBAQD, string vSoBAQD, string vNgayBAQD
        , string vNguyendon, string vBidon, decimal vLoaiAn
        , decimal vThamtravien, decimal vLanhdao, decimal vThamphan

        , DateTime? vTuNgay, DateTime? vDenNgay, string vSoThuly
        , decimal vTrangthai, int captrinhtiep_id, int isdangkybc
        , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT
          , int ketquathuly, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
        , decimal PageIndex, decimal PageSize)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
            new OracleParameter("vToaAnID",vToaAnID),
            new OracleParameter("vPhongBanID",vPhongBanID),
            new OracleParameter("vToaRaBAQD",vToaRaBAQD),
            new OracleParameter("vSoBAQD",vSoBAQD),
            new OracleParameter("vNgayBAQD",vNgayBAQD),
            new OracleParameter("vNguyendon",vNguyendon),
            new OracleParameter("vBidon",vBidon),
            new OracleParameter("vLoaiAn",vLoaiAn),

            new OracleParameter("vThamtravien",vThamtravien),
            new OracleParameter("vLanhdao",vLanhdao),
            new OracleParameter("vThamphan",vThamphan),

            new OracleParameter("tt_TuNgay",vTuNgay),
            new OracleParameter("tt_DenNgay",vDenNgay),
            new OracleParameter("vSoThuly",vSoThuly),

            new OracleParameter("vTrangthai",vTrangthai),
            new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
            new OracleParameter("vIsDangKyBC",isdangkybc),

            new OracleParameter("isTTMuonHS",isMuonHoSo),
            new OracleParameter("isTTToTrinh", isToTrinh),
            new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
            new OracleParameter("isBuocTT",isBuocTT),

            new OracleParameter("vKetquathuly",ketquathuly),
            new OracleParameter("LoaiAnDB",LoaiAnDB),
            new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
            new OracleParameter("IsHoanTHA",IsHoanTHA),
            new OracleParameter("PageIndex",PageIndex),
            new OracleParameter("PageSize",PageSize)
           };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_TT.GDTTTT_QLTOTRINH_BC10", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_11_PRINT(decimal vToaAnID, decimal vPhongBanID
       , decimal vToaRaBAQD, string vSoBAQD, string vNgayBAQD
        , string vNguyendon, string vBidon, decimal vLoaiAn
        , decimal vThamtravien, decimal vLanhdao, decimal vThamphan

        , DateTime? vTuNgay, DateTime? vDenNgay, string vSoThuly
        , decimal vTrangthai, int captrinhtiep_id, int isdangkybc
        , int isMuonHoSo, int isToTrinh, int isYKienKLToTrinh, int isBuocTT
          , int ketquathuly, int LoaiAnDB, String LoaiAnDB_TH, int IsHoanTHA
        , decimal PageIndex, decimal PageSize)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
            new OracleParameter("vToaAnID",vToaAnID),
            new OracleParameter("vPhongBanID",vPhongBanID),
            new OracleParameter("vToaRaBAQD",vToaRaBAQD),
            new OracleParameter("vSoBAQD",vSoBAQD),
            new OracleParameter("vNgayBAQD",vNgayBAQD),
            new OracleParameter("vNguyendon",vNguyendon),
            new OracleParameter("vBidon",vBidon),
            new OracleParameter("vLoaiAn",vLoaiAn),

            new OracleParameter("vThamtravien",vThamtravien),
            new OracleParameter("vLanhdao",vLanhdao),
            new OracleParameter("vThamphan",vThamphan),

            new OracleParameter("tt_TuNgay",vTuNgay),
            new OracleParameter("tt_DenNgay",vDenNgay),
            new OracleParameter("vSoThuly",vSoThuly),

            new OracleParameter("vTrangthai",vTrangthai),
            new OracleParameter("vCapTrinhTiep",captrinhtiep_id),
            new OracleParameter("vIsDangKyBC",isdangkybc),

            new OracleParameter("isTTMuonHS",isMuonHoSo),
            new OracleParameter("isTTToTrinh", isToTrinh),
            new OracleParameter("isTTYKienKLTotrinh",isYKienKLToTrinh),
            new OracleParameter("isBuocTT",isBuocTT),

            new OracleParameter("vKetquathuly",ketquathuly),
            new OracleParameter("LoaiAnDB",LoaiAnDB),
            new OracleParameter("vLoaiAnDB_TH",LoaiAnDB_TH),
            new OracleParameter("IsHoanTHA",IsHoanTHA),
            new OracleParameter("PageIndex",PageIndex),
            new OracleParameter("PageSize",PageSize)
           };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_TT.GDTTTT_QLTOTRINH_BC11", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_13_PRINT(String vToaAnID, String vPhongBanID, String vThamphanID, DateTime? vTuNgay, DateTime? vDenNgay)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vThamphanID",vThamphanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_TT.BAOCAO_TH_THULY_GDKT_13", parameters);
            return tbl;
        }
        //--------------------------------------------------
        public DataTable BC_VGDKT_12_PRINT( Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA.GDTTTT_VUAN_SEARCH_BC12", parameters);
            return tbl;
        }
        //--------------------------------------------------
        public DataTable BC_VGDKT_14_PRINT(Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA.GDTTTT_VUAN_SEARCH_BC14", parameters);
            return tbl;
        }
        //--------------------------------------------------
        public DataTable BC_VGDKT_15_PRINT(Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA.GDTTTT_VUAN_SEARCH_BC15", parameters);
            return tbl;
        }
        //--------------------------------------------------
        public DataTable BC_VGDKT_16_PRINT(Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA.GDTTTT_VUAN_SEARCH_BC16", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_17_PRINT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA_CC.GDTTTT_VUAN_SEARCH_BC17", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_18_PRINT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA_CC.GDTTTT_VUAN_SEARCH_BC18", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_19_PRINT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA_CC.GDTTTT_VUAN_SEARCH_BC19", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_20_PRINT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA_CC.GDTTTT_VUAN_SEARCH_BC20", parameters);
            return tbl;
        }
        public DataTable BC_VGDKT_21_PRINT(string V_CANBO_TK_ID, string V_LANHDAO_TK_ID, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, Decimal vLanhDaoID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBO_TK_ID",V_CANBO_TK_ID),
                new OracleParameter("V_LANHDAO_TK_ID",V_LANHDAO_TK_ID),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vLanhDaoID",vLanhDaoID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_VGDKT_BAOCAO_VA_CC.GDTTTT_VUAN_SEARCH_BC21", parameters);
            return tbl;
        }
        //--------------------------------------------------
        public DataTable GetAll_ThongKeChiTieu(String v_TenPhongban, Decimal vToaAnID, Decimal vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay, DateTime? vTuNgay_ky, DateTime? vDenNgay_ky, Decimal vLanhDaoID, Decimal vThamtravienID)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                 new OracleParameter("v_TenPhongban",v_TenPhongban),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vTuNgay_ky",vTuNgay_ky),
                new OracleParameter("vDenNgay_ky",vDenNgay_ky),
                new OracleParameter("vLanhDaoID",vLanhDaoID),
                new OracleParameter("vThamtravienID",vThamtravienID)
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_BAOCAO.BAOCAO_THONGKE_CHITIEU_01", parameters);
            return tbl;

        }
        public DataTable BAOCAO_TK_TP(String VTHAMPHANID_PCA, Decimal vToaAnID, Decimal vThamphanID, DateTime? vTuNgay, DateTime? vDenNgay,String vYears)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("VTHAMPHANID_PCA",VTHAMPHANID_PCA),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vThamphanID",vThamphanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vYears",vYears)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_BAOCAO.BAOCAO_TK_TP_GET", parameters);
            return tbl;
        }
        public DataTable BAOCAO_TK_VU(decimal vToaAnID, decimal vPhongBanID, decimal vLanhdaoVu, decimal vThamTraVien)
        {
            OracleParameter[] prm = new OracleParameter[]
             {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vLanhdaoVu",vLanhdaoVu),
                new OracleParameter("vThamTraVien",vThamTraVien)
             };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_BAOCAO.BAOCAO_TK_VU", prm);
            return tbl;
        }
        public DataTable BaoCao_GQ_DonDN_GDTTT(Decimal vToaAnID, Int32 vPhongBanID, DateTime? vTuNgay, DateTime? vDenNgay)
        { //--
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_BC_GQ_DONDN_GDTTT.BC_GQ_DONDN_GDTTT", parameters);
            return tbl;
        }
        public DataTable BAOCAO_TH_THULY_TP(String vToaAnID, String vThamphanID, String vThamphanID_Login, DateTime? vTuNgay, DateTime? vDenNgay)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vThamphanID",vThamphanID),
                new OracleParameter("vThamphanID_Login",vThamphanID_Login),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_BAOCAO.BAOCAO_TH_THULY_TP", parameters);
            return tbl;
        }
        public DataTable BAOCAO_TH_THULY_VU_GDKT(String vToaAnID, String vPhongBanID, String vThamphanID, DateTime? vTuNgay, DateTime? vDenNgay)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vThamphanID",vThamphanID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_BAOCAO.BAOCAO_TH_THULY_GDKT", parameters);
            return tbl;
        }
        public DataTable Letters_twcw_8A_Export(String _COURT_EXTID, String v_COURT_NAME,String _COURT_ID, String Time_ID, String Time_ID2)
        { 
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("V_COURT_EXTID",_COURT_EXTID),
                new OracleParameter("v_COURT_NAME",v_COURT_NAME),
                new OracleParameter("v_COURT_ID",_COURT_ID),
                new OracleParameter("v_REPORT_TIME_ID",Time_ID),
                new OracleParameter("v_REPORT_TIME_ID2",Time_ID2)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_LETTERS_TWCW_8A.BC_LETTERS_8A_EXP", parameters);
            return tbl;

        }
        public DataTable Letters_twcw_2C_Export(String _COURT_EXTID, String v_COURT_NAME, String _COURT_ID, String Time_ID, String Time_ID2)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
                new OracleParameter("V_COURT_EXTID",_COURT_EXTID),
                new OracleParameter("v_COURT_NAME",v_COURT_NAME),
                new OracleParameter("v_COURT_ID",_COURT_ID),
                new OracleParameter("v_REPORT_TIME_ID",Time_ID),
                new OracleParameter("v_REPORT_TIME_ID2",Time_ID2)
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_LETTERS_TWCW_8A.BC_LETTERS_2C_EXP", parameters);
            return tbl;

        }
    }
}