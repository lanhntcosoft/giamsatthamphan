﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL.GSTP;
namespace BL.GSTP.GDTTT
{
    public class GDTTT_APP_BL
    {
        public DataTable Permi_Add_BC(string vMenuPath, decimal vUserID)
        {
            OracleParameter[] parameters = new OracleParameter[]
               {
                new OracleParameter("vMenuPath",vMenuPath),
                new OracleParameter("vUserID",vUserID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
               };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.PERMI_MENU_BAOCAO", parameters);
            return tbl;
        }
        public DataTable THONGKE_CHUNG_S(decimal vToaAnID, decimal vPhongBanID, decimal vLanhdaoVu, decimal vThamTraVien, int LoaiAnDB)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vLanhdaoVu",vLanhdaoVu),
                new OracleParameter("vThamTraVien",vThamTraVien),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP.THONGKE_CHUNG", prm);
        }
        public DataTable DieuLuat_ToiDanh_Thongke()
        {
            OracleParameter[] parameters = new OracleParameter[]
               {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
               };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_BOLUAT_TOIDANH", parameters);
            return tbl;
        }
        public DataTable Get_Select_TP_5(Int32 v_VuAnID, Int32 Loai_hd, Int32 vToaAnID, Int32 vPhongBanID, Int32 vLoaiAn)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                 new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                 new OracleParameter("v_VuAnID",v_VuAnID),
                 new OracleParameter("v_Loai_hd",Loai_hd),
                 new OracleParameter("vPhongBanID",vPhongBanID),
                 new OracleParameter("vToaAnID",vToaAnID),
                 new OracleParameter("vLoaiAn",vLoaiAn),
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_SELECT_TP5", parameters);
            return tbl;
        }
        public DataTable Get_Select_TP(Int32 v_VuAnID,Int32 Loai_hd, Int32 vToaAnID, Int32 vPhongBanID, Int32 vLoaiAn)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                 new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                 new OracleParameter("v_VuAnID",v_VuAnID),
                 new OracleParameter("v_Loai_hd",Loai_hd),
                  new OracleParameter("vPhongBanID",vPhongBanID),
                 new OracleParameter("vToaAnID",vToaAnID),
                 new OracleParameter("vLoaiAn",vLoaiAn),
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_SELECT_TP", parameters);
            return tbl;
        }
        public DataTable THONGKE_TONGHOP_S(decimal vToaAnID, decimal vPhongBanID, decimal vLanhdaoVu, decimal vThamTraVien, int LoaiAnDB)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vLanhdaoVu",vLanhdaoVu),
                new OracleParameter("vThamTraVien",vThamTraVien),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP.THONGKE_TONGHOP", prm);
        }
        public DataTable THONGKE_ANQUOCHOI_THOIHIEU(decimal vToaAnID, decimal vPhongBanID, decimal vLoaiAn, decimal vLanhdaoVu, decimal vThamTraVien, decimal vAnQuocHoi, decimal vAnThoiHieu)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vLoaiAn",vLoaiAn),
                new OracleParameter("vLanhdaoVu",vLanhdaoVu),
                new OracleParameter("vThamTraVien",vThamTraVien),
                new OracleParameter("vAnQuocHoi",vAnQuocHoi),
                new OracleParameter("vAnThoiHieu",vAnThoiHieu),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.THONGKE_ANQUOCHOI_THOIHIEU", prm);
        }
        public DataTable THONGKE_THEO_THAMPHAN(Decimal vToaAnID, Decimal vThamphanID, DateTime? vTuNgay, DateTime? vDenNgay, int LoaiAnDB,String vYears)
        {
            if (vTuNgay == DateTime.MinValue) vTuNgay = null;
            if (vDenNgay == DateTime.MinValue) vDenNgay = null;
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vThamphanID",vThamphanID),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("LoaiAnDB",LoaiAnDB),
                                                                        new OracleParameter("vYears",vYears),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };

            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP.THONGKE_THEO_THAMPHAN", parameters);
            return tbl;
        }
        public DataTable THONGKE_THEO_THAMPHAN_GET(String VTHAMPHANID_PCA, Decimal VTOAANID, Decimal VTHAMPHANID, DateTime? VTUNGAY, DateTime? VDENNGAY, int VLOAIANDB, String VYEARS)
        {
            if (VTUNGAY == DateTime.MinValue) VTUNGAY = null;
            if (VDENNGAY == DateTime.MinValue) VDENNGAY = null;
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.ReturnValue ),
            new OracleParameter("VTHAMPHANID_PCA",VTHAMPHANID_PCA),
            new OracleParameter("VTOAANID",VTOAANID),
            new OracleParameter("VTHAMPHANID",VTHAMPHANID),
            new OracleParameter("VTUNGAY",VTUNGAY),
            new OracleParameter("VDENNGAY",VDENNGAY),
            new OracleParameter("VLOAIANDB",VLOAIANDB),
            new OracleParameter("VYEARS",VYEARS)
             };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP_CACC_GET.THONGKE_THEO_THAMPHAN_GET", parameters);
            return tbl;
        }
        //dang làm giở
        public DataTable THONGKE_TONGHOP_STPT(decimal vToaAnID, decimal vPhongBanID, decimal vLanhdaoVu, decimal vThamTraVien, int LoaiAnDB)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vPhongBanID",vPhongBanID),
                new OracleParameter("vLanhdaoVu",vLanhdaoVu),
                new OracleParameter("vThamTraVien",vThamTraVien),
                new OracleParameter("LoaiAnDB",LoaiAnDB),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_APP.THONGKE_TONGHOP", prm);
        }
        public DataTable THONGKE_THEO_HCTP_GET(Decimal VTOAANID, String VstrUsername, String strNhomID ,String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP.TK_HCTP_CREATE_DATA", parameters);
            return tbl;
        }
        public DataTable THONGKE_THEO_HCTP_CC_GET(Decimal VTOAANID, String VstrUsername, String strNhomID, String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_CC_HCTP.TK_HCTP_CREATE_DATA", parameters);
            return tbl;
        }
        public DataTable THONGKE_THEO_HCTP_EXPORT(Decimal VTOAANID, String VstrUsername, String strNhomID, String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY)                    
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP.TK_HCTP_EXPORT", parameters);
            return tbl;
        }
        public DataTable THONGKE_THEO_HCTP_CC_EXPORT(Decimal VTOAANID, String VstrUsername, String strNhomID, String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY)
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_CC_HCTP.TK_HCTP_EXPORT", parameters);
            return tbl;
        }
        public DataTable THONGKE_NGUOIDUNG_HCTP_GET(Decimal VTOAANID, String VstrUsername, String strNhomID, String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP.TK_NGUOIDUNG_CREATE_DATA", parameters);
            return tbl;
        }
        public DataTable THONGKE_NGUOIDUNG_HCTP_CC_GET(Decimal VTOAANID, String VstrUsername, String strNhomID, String VTUNGAY, String VDENNGAY)
        {
            OracleParameter[] parameters = new OracleParameter[]
                    {
                    new OracleParameter("VTOAANID",VTOAANID),
                    new OracleParameter("VstrUsername",VstrUsername),
                    new OracleParameter("VstrNhomID",strNhomID),
                    new OracleParameter("VTUNGAY",VTUNGAY),
                    new OracleParameter("VDENNGAY",VDENNGAY),
                    new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
                    };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_CC_HCTP.TK_NGUOIDUNG_CREATE_DATA", parameters);
            return tbl;
        }
    }
}