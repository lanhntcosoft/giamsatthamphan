﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL.GSTP;
using BL.GSTP.BANGSETGET;

namespace BL.GSTP
{
    public class GDTTT_DON_BL
    {
        public bool IN_UP_VANTHU_V_NULL_CC(string V_DON_ID)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_CC.UP_VANTHU_V_NULL", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_DON_ID"].Value = V_DON_ID;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public void SO_THULY_RETURN(String V_TOAANID, String V_LOAI_VB, String V_BAQD_LOAIAN, ref Decimal V_SOTL)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_CC.SO_THU_LY", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_SOTL"].Direction = ParameterDirection.Output;
            comm.Parameters["V_TOAANID"].Value = V_TOAANID;
            comm.Parameters["V_LOAI_VB"].Value = V_LOAI_VB;
            comm.Parameters["V_BAQD_LOAIAN"].Value = V_BAQD_LOAIAN;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
            }
            catch
            {
                tran.Rollback();
            }
            finally
            {
                V_SOTL = Convert.ToDecimal(comm.Parameters["V_SOTL"].Value);
                conn.Close();
            }
        }
        public bool INS_UP_TRUNG_CC(string V_DON_TRUNG, string V_DONID_MOI, string V_LOAIAN)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_CC.INS_UP_TRUNG", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_DON_TRUNG"].Value = V_DON_TRUNG;
            comm.Parameters["V_DONID_MOI"].Value = V_DONID_MOI;
            comm.Parameters["V_LOAIAN"].Value = V_LOAIAN;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable GDTTT_SUACONGVAN_SEARCH(decimal vToaAnID, string vSoCongVan, string vNgayCongVan)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_APP.SUACONGVAN_SEARCH", parameters);
            return tbl;
        }
        public void CHECK_DONTRUNGID_RETURN(Decimal V_ID, ref Decimal V_IS_DONTRUNG)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand cmd = new OracleCommand("PKG_GDTTT_HCTP_APP.CHECK_DONTRUNGID", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(cmd);
            cmd.Parameters["V_ID"].Value = V_ID;
            cmd.Parameters["V_IS_DONTRUNG"].Direction = ParameterDirection.Output;
            try
            {
                cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                V_IS_DONTRUNG = Convert.ToDecimal(cmd.Parameters["V_IS_DONTRUNG"].Value);
                conn.Close();
            }
        }
        public DataTable GDTTT_DON_SEARCH(String V_DONVI_CHUYEN_ID, String V_TRANGTHAICHUYEN, String V_LOAI_VB, String V_SODEN_TU, String V_SODEN_DEN, String V_NGAY_FROM, String V_NGAY_TO, String V_NGUOI_GUI_BT,
            String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal vLOAI_GDTTT, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("V_DONVI_CHUYEN_ID",V_DONVI_CHUYEN_ID),
            new OracleParameter("V_TRANGTHAICHUYEN",V_TRANGTHAICHUYEN),
            new OracleParameter("V_LOAI_VB",V_LOAI_VB),
            new OracleParameter("V_SODEN_TU",V_SODEN_TU),
            new OracleParameter("V_SODEN_DEN",V_SODEN_DEN),
            new OracleParameter("V_NGAY_FROM",V_NGAY_FROM),
            new OracleParameter("V_NGAY_TO",V_NGAY_TO),
            new OracleParameter("V_NGUOI_GUI_BT",V_NGUOI_GUI_BT),
            //------------------------
            new OracleParameter("v_ID_USER",v_ID_USER),
            new OracleParameter("vToaAnID",vToaAnID),
            new OracleParameter("vToaRaBAQD",vToaRaBAQD),
            new OracleParameter("vSoBAQD",vSoBAQD),
            new OracleParameter("vNgayBAQD",vNgayBAQD),
            new OracleParameter("vNguoiGui",vNguoiGui),
            new OracleParameter("vSoCMND",vSoCMND),
            new OracleParameter("vTuNgay",vTuNgay),
            new OracleParameter("vDenNgay",vDenNgay),
            new OracleParameter("vHinhThucDon",vHinhThucDon),
            new OracleParameter("vSoHieuDon",vSoHieuDon),
            new OracleParameter("vDiaChiTinh",vDiaChiTinh),
            new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
            new OracleParameter("vDiaChiCT",vDiaChiCT),
            new OracleParameter("vSoCongVan",vSoCongVan),
            new OracleParameter("vNgayCongVan",vNgayCongVan),
            new OracleParameter("vTraLoi",vTraLoi),
            new OracleParameter("vNguoiNhap",vNguoiNhap),
            new OracleParameter("vNoiChuyen",vNoiChuyen),
            new OracleParameter("vTrangthai",vTrangthai),
            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
            new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
            new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
            new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
            new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
            new OracleParameter("vArrSelectID",vArrSelectID),
            new OracleParameter("vIsThuLy",vIsThuLy),
            new OracleParameter("vPhanloaixuly",vPhanloaixuly),
            new OracleParameter("vNgayThulyTu",vNgayThulyTu),
            new OracleParameter("vNgayThulyDen",vNgayThulyDen),
            new OracleParameter("vSoThuly",vSoThuly),
            new OracleParameter("vChidao",vChidao),
            new OracleParameter("vTraigiam",vTraigiam),
            new OracleParameter("vTBQuahan",vTBQuahan),
            new OracleParameter("vNgayQuahan",vNgayQuahan),
            new OracleParameter("vThamphanID",vThamphanID),
            new OracleParameter("vThamtravienID",vThamtravienID),
            new OracleParameter("vLoaiCVID",vLoaiCVID),
            new OracleParameter("vNgayNhapTu",vNgayNhapTu),
            new OracleParameter("vNgayNhapDen",vNgayNhapDen),
            new OracleParameter("vIsDonGoc",vIsDonGoc),
            new OracleParameter("vIsTuHinh",vIsTuHinh),
            new OracleParameter("vLoaiAn",vLoaiAn),
            new OracleParameter("vCVPC_So",vCVPC_So),
            new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
            new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
            new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
            new OracleParameter("vLOAI_GDTTT",vLOAI_GDTTT),
            new OracleParameter("PageIndex",PageIndex),
            new OracleParameter("PageSize",PageSize),
            new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_APP.DON_SEARCH", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_SEARCH_THU_LY_MOI(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.DON_SEARCH_PCHUYEN_TLM", parameters);
            return tbl;
        }
        public DataTable GDTTT_GUI_COQUAN_CHUYENDON(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.GUI_COQUAN_CHUYENDON", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_SEARCH_TTRINH_PHANCONG(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.DON_SEARCH_TTRINH_PHANCONG", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_TP_GIAI_QUYET(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_APP.DON_SEARCH_TP_GIAI_QUYET", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_DS_TL_MOI(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
           string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
           string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
           decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
           decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
           decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_APP.DON_SEARCH_DS_TL_MOI", parameters);
            return tbl;
        }
        //public DataTable GDTTT_DON_DS_TL_MOI_CC(String ID_USER, string vArrSelectID)
        //{
        //    //OracleParameter[] parameters = new OracleParameter[] {
        //    //                                                      new OracleParameter("USERID",ID_USER),
        //    //                                                      new OracleParameter("vArrSelectID",vArrSelectID),
        //    //                                                      new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
        //    //                                                          };
        //    //DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_APP.DON_SEARCH_DS_DON_TL_MOI", parameters);
        //    //return tbl;
        //}

        public DataTable GDTTT_DON_TRUNG(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
          string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
          decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
          string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
          decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
          decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
          , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
          decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_DS.DON_SEARCH_DS_DON_TRUNG", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_CHUYEN_TOA_AN_KHAC(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
         string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
         decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
         string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
         decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
         decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
         , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
         decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_DS.DS_CHUYEN_TOA_AN_KHAC", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_CHUYEN_NGOAI_TOA(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
        string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
        decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
        string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
        decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
        decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
        , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
        decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_DS.DS_CHUYEN_NGOAI_TOA", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_CHUYEN_CQ_QUOC_HOI(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
        string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
        decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
        string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
        decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
        decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
        , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
        decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.DS_KQ_DO_Q_HOI", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_DS_DON_CHUA_DU_DK(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
           string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
           string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
           decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
           decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
           decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_DS.DS_DON_CHUA_DU_DK", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_SUA_SEARCH(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn,
            string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                     new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                     new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                     new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                      new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_SUA_SEARCH", parameters);
            return tbl;
        }



        public DataTable GDTTT_DON_GETPCTP(decimal vToaAnID, DateTime? vTuNgay, DateTime? vDenNgay,
          decimal vNoiChuyen, decimal vTrangthai, decimal vIsThuLy, string vNguoiNhap, string varrLoaiAn)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                  new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                             new OracleParameter("varrLoaiAn",varrLoaiAn),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_GETCHUAPCTP", parameters);
            return tbl;
        }
        public DataTable GDTTT_CONGVAN_SEARCH(decimal vLoaiVuviec, decimal vToaRaBAQD, string vSoBAQD, string vNgayBAQD, DateTime? vTuNgay, DateTime? vDenNgay, string vSoHieuDon, string vDonViGui, string vSoCongVan, string vNgayCongVan, decimal vTraLoi, string vNguoiNhap)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                      new OracleParameter("vDonViGui",vDonViGui),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                            new OracleParameter("vLoaiVuviec",vLoaiVuviec),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.CONGVAN_SEARCH", parameters);
            return tbl;
        }

        public decimal GETNEWTT(decimal donviID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }
        public DataTable GDTTT_DON_YKIEN_GETLIST(decimal vDonID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_YKIEN_GETLIST", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_CHECKDONTRUNG(string ToaXetXu, string NgayXetXu, string SoBAQD, string NguoiGui, string isBanAn)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("vToaXetXu",ToaXetXu),
                new OracleParameter("vNgayXetXu",NgayXetXu),
                new OracleParameter("vSoBAQD",SoBAQD),
                new OracleParameter("vNguoiGui",NguoiGui),
                new OracleParameter("vIsBanAn",isBanAn),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_CHECKDONTRUNG", pr);
        }
        public DataTable GDTTT_DON_GETDONTRUNG(decimal vCurrDonID, string vNguoiGui, string vSoBAQD, string vNgayBAQD, string vToaXetXu)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                 new OracleParameter("vCurrDonID",vCurrDonID),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vToaXetXu",vToaXetXu),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_GETDONTRUNG", pr);
        }
        public DataTable GDTTT_DON_GETDONTRUNG_CC(decimal V_LOAIDON, decimal v_toaanid, decimal vCurrDonID, string vNguoiGui, string vSoBAQD, string vNgayBAQD, string vToaXetXu, string vCapXetXu, string vIsBanAn)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_LOAIDON",V_LOAIDON),
                new OracleParameter("v_toaanid",v_toaanid),
                new OracleParameter("vCurrDonID",vCurrDonID),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vToaXetXu",vToaXetXu),
                new OracleParameter("vCapXetXu",vCapXetXu),
                new OracleParameter("vIsBanAn",vIsBanAn),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_CC.DON_GETDONTRUNG", pr);
        }
        public DataTable GDTTT_DON_GETCONGVAN(string vTenDonVi, string vToaAn, string vSoCongVan, string vNgayCongVan, decimal vIsDVTrongNganh)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("vTenDonVi",vTenDonVi),
                new OracleParameter("vToaAn",vToaAn),
                new OracleParameter("vSoCongVan",vSoCongVan),
                new OracleParameter("vNgayCongVan",vNgayCongVan),
                new OracleParameter("vIsDVTrongNganh",vIsDVTrongNganh),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_GETCONGVAN", pr);
        }
        public decimal PHANCONGNGAUNHIEN(decimal vToaAnID, DateTime vTuNgay, DateTime vDenNgay, string vNguoiNhap, string varrLoaiAn, decimal vNguoithuchien)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                 new OracleParameter("vNguoiNhap",vNguoiNhap),
                new OracleParameter("varrLoaiAn",varrLoaiAn),
                new OracleParameter("vNguoithuchien",vNguoithuchien)
            };
            return Cls_Comon.ExcuteProcResult("PKG_GDTTT.PHANCONGNGAUNHIEN", prm);
        }
        public DataTable DON_GETTHEOKETQUAID(decimal vToaAnID, decimal vKetQuaID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vNgayThuly, string vSoThuly, decimal vThamphanID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vKetQuaID",vKetQuaID),
                  new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                  new OracleParameter("vNgayThuly",vNgayThuly),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vThamphanID",vThamphanID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_GETTHEOKETQUAID", prm);
        }
        public DataTable DON_GETTHEOKETQUAID_EXPORT(decimal vToaAnID, decimal vKetQuaID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vNgayThuly, string vSoThuly, decimal vThamphanID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vKetQuaID",vKetQuaID),
                  new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                  new OracleParameter("vNgayThuly",vNgayThuly),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vThamphanID",vThamphanID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_CC.DON_GETTHEOKETQUAID", prm);
        }
        public DataTable DON_LICHSUPHANCONG(decimal vToaAnID, string vSoTT, string vNgayTT, string vPC_TuNgay, string vPC_DenNgay, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vNgayThuly, string vSoThuly, decimal vThamphanID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vSoToTrinh",vSoTT),
                new OracleParameter("vNgayToTrinh",vNgayTT),
                new OracleParameter("vPC_TuNgay",vPC_TuNgay),
                new OracleParameter("vPC_DenNgay",vPC_DenNgay),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vNgayThuly",vNgayThuly),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vThamphanID",vThamphanID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_LICHSUPHANCONG", prm);
        }

        public DataTable GDTTT_NHANDON_SEARCH(decimal vToaAnID, decimal vToaChuyenID, decimal vLoaiAn, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vMaDon, string vSoCongVan, decimal vTrangthai)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaChuyenID",vToaChuyenID),
                                                                        new OracleParameter("vLoaiAn",vLoaiAn),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vMaDon",vMaDon),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_NHAN_SEARCH", parameters);
            return tbl;
        }
        public DataTable LICHSUDON(decimal vID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vID",vID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.LICHSUDON", prm);
        }
        public DataTable DANHSACHDONTRUNG(decimal vID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vID",vID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DANHSACHDONTRUNG", prm);
        }

        public DataTable DANHSACHDONTHEOID(string varrID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("varrID",varrID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DANHSACHDONTHEOID", prm);
        }
        //public DataTable DANHSACHDONTHEOVuAnID(Decimal VuAnID)
        //{

        //    OracleParameter[] prm = new OracleParameter[]
        //    {
        //        new OracleParameter("vVuAnID",VuAnID),
        //         new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
        //    };
        //    return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DANHSACHDONTHEOVuAnID", prm);
        //}
        public DataTable DonAHS_GetAllNguoiKN(Decimal DonID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                  new OracleParameter("vDonID",DonID)
                 ,new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("GDTTT_DonAHS_GetAllNguoiKN", prm);
        }
        public DataTable DANHSACHDONCHIDAO_ByVuAnID(Decimal VuAnID, Decimal trangthai)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                  new OracleParameter("vVuAnID",VuAnID)
                 ,new OracleParameter("vTrangThaiChuyen",trangthai)
                 ,new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("GDTTT_DON_ANCHIDAO_GETBYVUANID", prm);
        }

        public DataTable GetAllByVuAn_TrangThaiChuyenDon(Decimal VuAnID, Decimal trangthai, Decimal LoaiDon, Decimal vLoaiCVID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vVuAnID",VuAnID)
                ,new OracleParameter("vTrangThaiChuyen",trangthai)
                , new OracleParameter ("vLoaiDon", LoaiDon)
                 , new OracleParameter ("vLoaiCVID", vLoaiCVID)

                ,new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("GDTTT_DON_GETBYVUANID", prm);
        }

        public DataTable CANBO_GETBYDONVI(decimal donviID, string vChucDanh)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("donviID",donviID),
                                                                         new OracleParameter("vChucDanh",vChucDanh),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("CANBO_GETBYDONVI", parameters);
            return tbl;
        }
        public DataTable CANBO_GETBYDONVI_TP(decimal donviID, string vChucDanh, string v_CANBO_ID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_CANBO_ID",v_CANBO_ID),
                new OracleParameter("donviID",donviID),
                new OracleParameter("vChucDanh",vChucDanh),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_APP.CANBO_GETBYDONVI", parameters);
            return tbl;
        }
        public DataTable CANBO_GETBYDONVI_XX(decimal donviID, string vChucDanh)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("donviID",donviID),
                                                                         new OracleParameter("vChucDanh",vChucDanh),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_APP.CANBO_GETBYDONVI_XX", parameters);
            return tbl;
        }
        public DataTable QHPL_DINHNGHIA_LIST(decimal vPhongbanID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vPhongbanID",vPhongbanID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.QHPL_DINHNGHIA_LIST", parameters);
            return tbl;
        }
        public DataTable QHPL_DINHNGHIA_LIST(decimal vPhongbanID, decimal loaian)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vPhongbanID",vPhongbanID),
                                                                        new OracleParameter("vLoai",loaian),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("QHPL_DINHNGHIA_GetByDK", parameters);
            return tbl;
        }
        public DataTable CANBO_GETBYPHONGBAN(decimal donviID, decimal vPhongbanID, string vChucDanh)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("donviID",donviID),
                                                                         new OracleParameter("vPhongbanID",vPhongbanID),
                                                                         new OracleParameter("vChucDanh",vChucDanh),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.CANBO_GETBYPHONGBAN", parameters);
            return tbl;
        }

        ///Lay tat ca can bo (Dang ctac + nghi Ctac) theo donviid
        public DataTable GDTTT_VuAn_GetAllCBTheoPB(decimal vToaAnID, decimal vPhongBanID, string vChucDanh)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vPhongBanID",vPhongBanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vChucDanh",vChucDanh),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("GDTTT_VuAn_GetAllCBTheoPB", parameters);
            return tbl;
        }
        public DataTable GDTTT_VUAN_GETALLCBTHEOPB_XX(decimal vToaAnID, decimal vPhongBanID, string vChucDanh)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vPhongBanID",vPhongBanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vChucDanh",vChucDanh),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_APP.GDTTT_VUAN_GETALLCBTHEOPB", parameters);
            return tbl;
        }
        public DataTable GDTTT_Getall_TTV_TheoTP(decimal vToaAnID, decimal vThamPhanID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vThamPhanID",vThamPhanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GDTTT_GETTTT_THEOTP", parameters);
            return tbl;
        }
        public DataTable DM_CANBO_PB_CHUCDANH_THEOTP(decimal vToaAnID, decimal vThamPhanID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vThamPhanID",vThamPhanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.DM_CANBO_PB_CHUCDANH", parameters);
            return tbl;
        }
        public DataTable Get_Year_Theo_TP(decimal vToaAnID, decimal vThamPhanID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vThamPhanID",vThamPhanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_YEAR_THEO_TP", parameters);
            return tbl;
        }
        public DataTable GDTTT_Tp_Duoc_Phutrach(String vToaAnID, Int32 vThamphan_id)
        {
            OracleParameter[] parameters = new OracleParameter[] {
            new OracleParameter("vToaAnID",vToaAnID),
            new OracleParameter("vThamphan_id",vThamphan_id),
            new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_PHUTRACH_TP", parameters);
            return tbl;
        }
        public DataTable GDTTT_Tp_Theo_Don_Vi(String vToaAnID, String vPhongBanID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_TOAANID",vToaAnID),
                new OracleParameter("V_PHONGBANID",vPhongBanID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
              };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_TP_VU_GDKT", parameters);
            return tbl;
        }
        public DataTable GDTTT_Tp_Duoc_Phutrach_BC(Int32 vThamphan_id)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vThamphan_id",vThamphan_id),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.GET_PHUTRACH_TP_BC", parameters);
            return tbl;
        }
        public DataTable GDTTT_CanBo_GetTP_HD5(decimal vToaAnID, decimal vPhongBanID, decimal vLoaiAn)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vPhongBanID",vPhongBanID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vLoaiAn",vLoaiAn),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("GDTTT_CanBo_GetTP_HD5", parameters);
            return tbl;
        }


        public DataTable CANBO_GETBYDONVI_2CHUCVU(decimal vDonViID, decimal vPhongbanID, string vChucVu1, string vChucVu2)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonViID",vDonViID),
                                                                         new OracleParameter("vPhongbanID",vPhongbanID),
                                                                         new OracleParameter("vChucVu1",vChucVu1),
                                                                         new OracleParameter("vChucVu2",vChucVu2),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("CANBO_GETBYDONVI_2CHUCVU", parameters);
            return tbl;
        }
        public DataTable CANBO_GETBYDONVI_LANHDAO(decimal vDonViID, decimal vPhongbanID, string vChucVu)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                        new OracleParameter("vDonViID",vDonViID),
                        new OracleParameter("vPhongbanID",vPhongbanID),
                        new OracleParameter("vChucVu",vChucVu),
                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                        };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_GET.CANBO_GETBYDONVI_LANHDAO", parameters);
            return tbl;
        }
        public decimal TL_GETMAXTT(decimal donviID, decimal vYear, decimal vLoaiAn)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vYear",vYear),
                                                                         new OracleParameter("vLoaiAn",vLoaiAn),

                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
                DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_TL_GETMAXTT", parameters);
                return Convert.ToDecimal(tbl.Rows[0][0]) + 1;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        public bool DON_TL_CHECK(decimal donviID, decimal vYear, decimal vLoaiAn, string vTL_SO, decimal vDonID)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vYear",vYear),
                                                                         new OracleParameter("vLoaiAn",vLoaiAn),
                                                                         new OracleParameter("vTL_SO",vTL_SO),
                                                                          new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
                DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_TL_CHECK", parameters);
                return tbl.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool DON_TL_CHECK_CC(decimal vdonviID, decimal vYear, decimal vLoaiAn, string vTL_SO, decimal vDonID, decimal vHinhThucDon)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",vdonviID),
                                                                        new OracleParameter("vYear",vYear),
                                                                         new OracleParameter("vLoaiAn",vLoaiAn),
                                                                         new OracleParameter("vTL_SO",vTL_SO),
                                                                         new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                          new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
                DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_CC.DON_TL_CHECK_CC", parameters);
                return tbl.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public decimal CV_GETMAXTT(decimal donviID, decimal vYear, decimal vNoiChuyen)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vYear",vYear),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),

                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
                DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_CV_GETMAXTT", parameters);
                return Convert.ToDecimal(tbl.Rows[0][0]) + 1;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
        public bool DON_CV_CHECK(decimal vdonviID,decimal vNoiChuyen,string vSO_CV,decimal vYear)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",vdonviID),
                                                                        new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                        new OracleParameter("vSO_CV",vSO_CV),
                                                                        new OracleParameter("vYear",vYear),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
                DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_CV_CHECK", parameters);
                return tbl.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataTable GDTTT_DON_KEM_DONTRUNG_SEARCH(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
           string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
           string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
           decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
           decimal vCD_DONVIID, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vPhancongTTV, decimal vloaian)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                        new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                        new OracleParameter("vTrangthai",vTrangthai),
                                                                        new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                        new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                        new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                        new OracleParameter("vArrSelectID",vArrSelectID),
                                                                        new OracleParameter("vIsThuLy",vIsThuLy),
                                                                        new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                        new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                        new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                        new OracleParameter("vSoThuly",vSoThuly),
                                                                        new OracleParameter("vPhancongTTV",vPhancongTTV),
                                                                        new OracleParameter("vloaian",vloaian),

                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                               };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("DON_GIAIQUYET_SEARCH_ShowAll", parameters);
            return tbl;
        }
        public DataTable GDTTT_GIAIQUYET_SEARCH(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
              string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
              decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
              string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
              decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
              decimal vCD_DONVIID, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
              , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly
              , decimal vPhancongTTV, decimal vloaian, decimal vGiaoTHS, int IsGhepVuAn, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
              , decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoCMND",vSoCMND),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vHinhThucDon",vHinhThucDon),
                new OracleParameter("vSoHieuDon",vSoHieuDon),
                new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                new OracleParameter("vDiaChiCT",vDiaChiCT),
                new OracleParameter("vSoCongVan",vSoCongVan),
                new OracleParameter("vNgayCongVan",vNgayCongVan),
                new OracleParameter("vTraLoi",vTraLoi),
                new OracleParameter("vNguoiNhap",vNguoiNhap),
                new OracleParameter("vNoiChuyen",vNoiChuyen),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                new OracleParameter("vArrSelectID",vArrSelectID),
                new OracleParameter("vIsThuLy",vIsThuLy),
                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vPhancongTTV",vPhancongTTV),
                new OracleParameter("vloaian",vloaian),
                new OracleParameter("vGiaoTHS",vGiaoTHS),

                new OracleParameter("IsGhepVuAn",IsGhepVuAn),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_DON_APP.DON_GIAIQUYET_SEARCH", parameters);
            return tbl;
        }

        public DataTable GDTTT_GIAIQUYET_SEARCH_HISTORY(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
                 string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
                 decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
                 string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
                 decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
                 decimal vCD_DONVIID, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
                 , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly
                 , decimal vPhancongTTV, decimal vloaian, decimal vGiaoTHS, int IsGhepVuAn, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
                 , decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoCMND",vSoCMND),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vHinhThucDon",vHinhThucDon),
                new OracleParameter("vSoHieuDon",vSoHieuDon),
                new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                new OracleParameter("vDiaChiCT",vDiaChiCT),
                new OracleParameter("vSoCongVan",vSoCongVan),
                new OracleParameter("vNgayCongVan",vNgayCongVan),
                new OracleParameter("vTraLoi",vTraLoi),
                new OracleParameter("vNguoiNhap",vNguoiNhap),
                new OracleParameter("vNoiChuyen",vNoiChuyen),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                new OracleParameter("vArrSelectID",vArrSelectID),
                new OracleParameter("vIsThuLy",vIsThuLy),
                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vPhancongTTV",vPhancongTTV),
                new OracleParameter("vloaian",vloaian),
                new OracleParameter("vGiaoTHS",vGiaoTHS),

                new OracleParameter("IsGhepVuAn",IsGhepVuAn),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_DON_APP.DON_GIAIQUYET_SEARCH_HISTORY", parameters);
            return tbl;
        }
        public DataTable GDTTT_KNTC_SEARCH(decimal vToaAnID, string vDoiTuongBiKNTC, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
      decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
      string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
      decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
      decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vPhanloaixuly
      , decimal vChidao, decimal vTBQuahan, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vDoiTuongBiKNTC",vDoiTuongBiKNTC),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_KNTC_SEARCH", parameters);
            return tbl;
        }
        public decimal UPDATESOLUONGDON(decimal vDonID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.DON_UPDATESOLUONGDON", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }
        public DataTable BOSUNGTAILIEU(decimal vID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vID",vID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT.BOSUNGTAILIEU", prm);
        }
        public DataTable GetThongTinAnQH(decimal VuAnID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vVuAnID",VuAnID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("GDTTTT_DON_GetThongTinAnQH", prm);
        }
        public DataTable GetDonDaNhanChuaMapVA()
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("GDTTT_GetDonDaNhanChuaMapVA", prm);
        }
        public void Update_Don_TH(decimal VUVIECID)
        {
            GSTPContext dt = new GSTPContext();
            OracleParameter[] prm = new OracleParameter[]
                                    {
                                         new OracleParameter("vuviec_id",VUVIECID),
                                         new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
                                    };
            Cls_Comon.ExcuteProc("GDTTT_DON_UPDATETH", prm);

            ////--------Lay ds cac muc = cv quoc hoi-----------------------
            //decimal cv_quochoi_id = 1023;
            //String StrCVQH = ";";
            //DM_DATAITEM_BL da = new DM_DATAITEM_BL();
            //DataTable tblDA = da.DM_DATAITEM_GET_ALL_CHILD_BYPARENTID(cv_quochoi_id);
            //foreach (DataRow rDA in tblDA.Rows)
            //    StrCVQH += rDA["ID"] + ";";

            ////-------------------------------
            //Decimal temp = 0, ISANQUOCHOI =0, IsAnChiDao =0, IsThongBaoCV = 0, CountAll =0 ;
            //String ARRNGUOIKHIEUNAI = "";
            //if (VUVIECID > 0)
            //{
            //    GDTTT_VUAN objVA = dt.GDTTT_VUAN.Where(x => x.ID == VUVIECID).Single();
            //    List<GDTTT_DON> lstDon = dt.GDTTT_DON.Where(x => x.VUVIECID == VUVIECID).ToList();
            //    if (lstDon != null && lstDon.Count > 0)
            //    {
            //        CountAll = lstDon.Count;
            //        foreach (GDTTT_DON itemDon in lstDon)
            //        {
            //            temp = 0;
            //            if (!string.IsNullOrEmpty(itemDon.NGUOIGUI_HOTEN + ""))
            //                ARRNGUOIKHIEUNAI = (string.IsNullOrEmpty(ARRNGUOIKHIEUNAI + "")) ? "" : ", " + Cls_Comon.FormatTenRieng(itemDon.NGUOIGUI_HOTEN);
            //            //--------------------------------
            //            temp = String.IsNullOrEmpty(itemDon.LOAICONGVAN + "") ? 0 : (Decimal)itemDon.LOAICONGVAN;
            //            if (temp > 0)
            //            {
            //                if (StrCVQH.Contains(";" + temp + ";"))
            //                    ISANQUOCHOI++;
            //            }
            //            //--------------------------
            //            temp = String.IsNullOrEmpty(itemDon.CHIDAO_COKHONG + "") ? 0 : (Decimal)itemDon.CHIDAO_COKHONG;
            //            if (temp > 0)
            //                IsAnChiDao++;
            //            //--------------------------
            //            temp = String.IsNullOrEmpty(itemDon.CV_YEUCAUTHONGBAO + "") ? 0 : (Decimal)itemDon.CV_YEUCAUTHONGBAO;
            //            if (temp > 0)
            //                IsThongBaoCV++;
            //        }
            //    }
            //    decimal Old_IsAnQH = (string.IsNullOrEmpty(objVA.ISANQUOCHOI + "")) ? 0 : (decimal)objVA.ISANQUOCHOI;
            //    objVA.TONGDON = CountAll;
            //    objVA.ARRNGUOIKHIEUNAI = ARRNGUOIKHIEUNAI;
            //    objVA.ISANQUOCHOI = (ISANQUOCHOI > 0) ? 1 : Old_IsAnQH;
            //    objVA.ISANCHIDAO = (IsAnChiDao > 0) ? 1 : 0;
            //    objVA.ISTHONGBAOCV = (IsThongBaoCV > 0) ? 1 : 0;
            //    dt.SaveChanges();
            //}
        }
        public void Update_TH_AnChuyenHS_VKS(decimal VUVIECID)
        {
            GSTPContext dt = new GSTPContext();

            //--------Lay ds cac muc = cv quoc hoi-----------------------
            decimal cv_quochoi_id = 1023;
            String StrCVQH = ";";
            DM_DATAITEM_BL da = new DM_DATAITEM_BL();
            DataTable tblDA = da.DM_DATAITEM_GET_ALL_CHILD_BYPARENTID(cv_quochoi_id);
            foreach (DataRow rDA in tblDA.Rows)
                StrCVQH += rDA["ID"] + ";";

            //-------------------------------
            Decimal temp = 0, ISANQUOCHOI = 0, IsAnChiDao = 0, IsThongBaoCV = 0, CountAll = 0;
            String ARRNGUOIKHIEUNAI = "";
            if (VUVIECID > 0)
            {
                GDTTT_VUAN objVA = dt.GDTTT_VUAN.Where(x => x.ID == VUVIECID).Single();
                List<GDTTT_DON> lstDon = dt.GDTTT_DON.Where(x => x.VUVIECID == VUVIECID).ToList();
                if (lstDon != null && lstDon.Count > 0)
                {
                    CountAll = lstDon.Count;
                    foreach (GDTTT_DON itemDon in lstDon)
                    {
                        temp = 0;
                        if (!string.IsNullOrEmpty(itemDon.NGUOIGUI_HOTEN + ""))
                            ARRNGUOIKHIEUNAI = (string.IsNullOrEmpty(ARRNGUOIKHIEUNAI + "")) ? "" : ", " + Cls_Comon.FormatTenRieng(itemDon.NGUOIGUI_HOTEN);
                        //--------------------------------
                        temp = String.IsNullOrEmpty(itemDon.LOAICONGVAN + "") ? 0 : (Decimal)itemDon.LOAICONGVAN;
                        if (temp > 0)
                        {
                            if (StrCVQH.Contains(";" + temp + ";"))
                                ISANQUOCHOI++;
                        }
                        //--------------------------
                        temp = String.IsNullOrEmpty(itemDon.CHIDAO_COKHONG + "") ? 0 : (Decimal)itemDon.CHIDAO_COKHONG;
                        if (temp > 0)
                            IsAnChiDao++;
                        //--------------------------
                        temp = String.IsNullOrEmpty(itemDon.CV_YEUCAUTHONGBAO + "") ? 0 : (Decimal)itemDon.CV_YEUCAUTHONGBAO;
                        if (temp > 0)
                            IsThongBaoCV++;
                    }
                }
                objVA.TONGDON = CountAll;
                objVA.ARRNGUOIKHIEUNAI = ARRNGUOIKHIEUNAI;
                //objVA.ISANQUOCHOI = (ISANQUOCHOI > 0) ? 1 : Old_IsAnQH;
                //objVA.ISANCHIDAO = (IsAnChiDao > 0) ? 1 : 0;
                objVA.ISTHONGBAOCV = (IsThongBaoCV > 0) ? 1 : 0;
                objVA.ISVIENTRUONGKN = 1;
                dt.SaveChanges();
            }
        }
        public DataTable GDTTT_GIAONHAN_THS(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly
            , decimal vPhancongTTV, decimal vloaian, decimal vGiaoTHS, int IsGhepVuAn, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
            , decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoCMND",vSoCMND),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vHinhThucDon",vHinhThucDon),
                new OracleParameter("vSoHieuDon",vSoHieuDon),
                new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                new OracleParameter("vDiaChiCT",vDiaChiCT),
                new OracleParameter("vSoCongVan",vSoCongVan),
                new OracleParameter("vNgayCongVan",vNgayCongVan),
                new OracleParameter("vTraLoi",vTraLoi),
                new OracleParameter("vNguoiNhap",vNguoiNhap),
                new OracleParameter("vNoiChuyen",vNoiChuyen),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                new OracleParameter("vArrSelectID",vArrSelectID),
                new OracleParameter("vIsThuLy",vIsThuLy),
                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vPhancongTTV",vPhancongTTV),
                new OracleParameter("vloaian",vloaian),
                new OracleParameter("vGiaoTHS",vGiaoTHS),
                new OracleParameter("IsGhepVuAn",IsGhepVuAn),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_DON_APP.DON_GDTTT_GIAONHAN_THS", parameters);
            return tbl;
        }
        public bool GDTTT_DON_GIAONHAN_INSERT_UPDAT(GDTTT_DON_GIAONHAN_THS obj)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_DON_APP.GDTTT_GIAONHAN_THS_UP_IN", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["v_ID"].Value = obj.ID;
            comm.Parameters["v_DONID"].Value = obj.DONID;
            comm.Parameters["v_NGUOICHUYEN_ID"].Value = obj.NGUOICHUYEN_ID;
            comm.Parameters["v_NGUOINHAN_ID"].Value = obj.NGUOINHAN_ID;
            if (obj.NGAYCHUYEN != Convert.ToDateTime("01/01/0001"))
                comm.Parameters["v_NGAYCHUYEN"].Value = obj.NGAYCHUYEN;
            else
                comm.Parameters["v_NGAYCHUYEN"].Value = null;

            if (obj.NGAYNHAN != Convert.ToDateTime("01/01/0001"))
                comm.Parameters["v_NGAYNHAN"].Value = obj.NGAYNHAN;
            else
                comm.Parameters["v_NGAYNHAN"].Value = null;

            comm.Parameters["v_TRANGTHAI"].Value = obj.TRANGTHAI;
            comm.Parameters["v_GHICHU"].Value = obj.GHICHU;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        public bool GDTTT_DON_GIAONHAN_DEL(Decimal ID)
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_DON_APP.GDTTT_GIAONHAN_THS_DEL", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["v_ID"].Value = ID;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable GDTTT_DON_GIAONHAN_THS_PRINT(decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
           string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
           string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
           decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
           decimal vCD_DONVIID, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly
           , decimal vPhancongTTV, decimal vloaian, int IsGhepVuAn, decimal _ISXINANGIAM, decimal _GDT_ISXINANGIAM
           , decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("vToaAnID",vToaAnID),
                new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                new OracleParameter("vSoBAQD",vSoBAQD),
                new OracleParameter("vNgayBAQD",vNgayBAQD),
                new OracleParameter("vNguoiGui",vNguoiGui),
                new OracleParameter("vSoCMND",vSoCMND),
                new OracleParameter("vTuNgay",vTuNgay),
                new OracleParameter("vDenNgay",vDenNgay),
                new OracleParameter("vHinhThucDon",vHinhThucDon),
                new OracleParameter("vSoHieuDon",vSoHieuDon),
                new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                new OracleParameter("vDiaChiCT",vDiaChiCT),
                new OracleParameter("vSoCongVan",vSoCongVan),
                new OracleParameter("vNgayCongVan",vNgayCongVan),
                new OracleParameter("vTraLoi",vTraLoi),
                new OracleParameter("vNguoiNhap",vNguoiNhap),
                new OracleParameter("vNoiChuyen",vNoiChuyen),
                new OracleParameter("vTrangthai",vTrangthai),
                new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                new OracleParameter("vArrSelectID",vArrSelectID),
                new OracleParameter("vIsThuLy",vIsThuLy),
                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                new OracleParameter("vSoThuly",vSoThuly),
                new OracleParameter("vPhancongTTV",vPhancongTTV),
                new OracleParameter("vloaian",vloaian),
                new OracleParameter("IsGhepVuAn",IsGhepVuAn),
                new OracleParameter("v_ISXINANGIAM",_ISXINANGIAM),
                new OracleParameter("v_GDT_ISXINANGIAM",_GDT_ISXINANGIAM),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize),
                 };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_VUAN_INBC.GDTTTT_DON_GIAONHAN_THS_PRINT", parameters);
            return tbl;
        }
        public DataTable Get_DV_tu_LoaianID(String V_LOAIANID, String V_TOAANID)
        {

            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("V_LOAIANID",V_LOAIANID),
                new OracleParameter("V_TOAANID",V_TOAANID),
                 new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_CC.GDTTT_LOAD_DONVI", prm);
        }
        public Decimal GDTTT_DON_CC_REIDS()
        {
            //Hàm trả về một giá trị
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_CC.GDTTT_DON_CC_REID", conn);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add("", OracleDbType.Decimal).Direction = ParameterDirection.ReturnValue;
            comm.ExecuteNonQuery();
            try
            {
                return Convert.ToDecimal(comm.Parameters[0].Value.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }
        public Decimal GDTTT_VUAN_REIDS()
        {
            //Hàm trả về một giá trị
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_GDTTT_CC.GDTTT_VUAN_REID", conn);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add("", OracleDbType.Decimal).Direction = ParameterDirection.ReturnValue;
            comm.ExecuteNonQuery();
            try
            {
                return Convert.ToDecimal(comm.Parameters[0].Value.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable GDTTT_DON_SEARCH_GIAY_XAC_NHAN(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.DON_SEARCH_GIAYXACNHANCC", parameters);
            return tbl;
        }
        public DataTable GetGiayTrieuTap(decimal vVbToTungID,string strDiadiem,string strDiaDiemToaAn)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vVbToTungID",vVbToTungID),
                new OracleParameter("strDiadiem",strDiadiem),
                new OracleParameter("strDiaDiemToaAn",strDiaDiemToaAn),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.GIAYTRIEUTAP", parameters);
            return tbl;
        }

        public DataTable GetGiayTrieuTap_AHS(decimal vVbToTungID, string strDiadiem, string strDiaDiemToaAn)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vVbToTungID",vVbToTungID),
                new OracleParameter("strDiadiem",strDiadiem),
                new OracleParameter("strDiaDiemToaAn",strDiaDiemToaAn),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.GIAYTRIEUTAP_AHS", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_SEARCH_THONG_BAO_YCBSCC(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
            string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
            decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
            string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
            decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
            decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
            , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
            decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC.DON_SEARCH_THONG_BAO_YCBSCC", parameters);
            return tbl;
        }

        public DataTable GDTTT_REPORT_TIEU_HO_SO_CC(string vArrSelectID, Decimal v_ID_USER)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vArrSelectID",vArrSelectID), new OracleParameter("v_ID_USER",v_ID_USER),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_CC.REPORT_TIEUHOSO_CC", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_DS_VB_CC(string vArrSelectID, Decimal v_ID_USER)
        {
            OracleParameter[] parameters = new OracleParameter[] {
               new OracleParameter("vArrSelectID",vArrSelectID), new OracleParameter("v_ID_USER",v_ID_USER),
               new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_CC.DON_SEARCH_DS_VB_CC", parameters);
            return tbl;
        }
        public DataTable GDTTT_DON_DS_TL_MOI_CC(String V_BC_NGAYDK, String V_BC_Nguoiky, String V_BC_SoCV, String v_ID_USER, decimal vToaAnID, decimal vToaRaBAQD, string vSoBAQD,
           string vNgayBAQD, string vNguoiGui, string vSoCMND, DateTime? vTuNgay, DateTime? vDenNgay,
           decimal vHinhThucDon, string vSoHieuDon, decimal vDiaChiTinh, decimal vDiaChiHuyen,
           string vDiaChiCT, string vSoCongVan, string vNgayCongVan,
           decimal vTraLoi, string vNguoiNhap, decimal vNoiChuyen, decimal vTrangthai,
           decimal vCD_DONVIID, decimal vCD_TA_TRANGTHAI, string vCD_TENDONVI, DateTime? vNgaychuyenTu, DateTime? vNgaychuyenDen, string vArrSelectID, decimal vIsThuLy, decimal vPhanloaixuly
           , DateTime? vNgayThulyTu, DateTime? vNgayThulyDen, string vSoThuly, decimal vChidao, decimal vTraigiam, decimal vTBQuahan, DateTime? vNgayQuahan, decimal vThamphanID,
           decimal vThamtravienID, decimal vLoaiCVID, DateTime? vNgayNhapTu, DateTime? vNgayNhapDen, decimal vIsDonGoc, decimal vIsTuHinh, decimal vLoaiAn, string vCVPC_So, string vCVPC_Ngay, string vCVPC_TenCQ, decimal vGuitoiCA_TA, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("V_BC_NGAYDK",V_BC_NGAYDK),
                new OracleParameter("V_BC_Nguoiky",V_BC_Nguoiky),
                new OracleParameter("V_BC_SoCV",V_BC_SoCV),
                new OracleParameter("v_ID_USER",v_ID_USER),

                                                                        new OracleParameter("vToaAnID",vToaAnID),
                                                                         new OracleParameter("vToaRaBAQD",vToaRaBAQD),
                                                                        new OracleParameter("vSoBAQD",vSoBAQD),
                                                                        new OracleParameter("vNgayBAQD",vNgayBAQD),
                                                                        new OracleParameter("vNguoiGui",vNguoiGui),
                                                                        new OracleParameter("vSoCMND",vSoCMND),
                                                                        new OracleParameter("vTuNgay",vTuNgay),
                                                                        new OracleParameter("vDenNgay",vDenNgay),
                                                                        new OracleParameter("vHinhThucDon",vHinhThucDon),
                                                                        new OracleParameter("vSoHieuDon",vSoHieuDon),
                                                                        new OracleParameter("vDiaChiTinh",vDiaChiTinh),
                                                                        new OracleParameter("vDiaChiHuyen",vDiaChiHuyen),
                                                                        new OracleParameter("vDiaChiCT",vDiaChiCT),
                                                                        new OracleParameter("vSoCongVan",vSoCongVan),
                                                                        new OracleParameter("vNgayCongVan",vNgayCongVan),
                                                                        new OracleParameter("vTraLoi",vTraLoi),
                                                                        new OracleParameter("vNguoiNhap",vNguoiNhap),
                                                                         new OracleParameter("vNoiChuyen",vNoiChuyen),
                                                                           new OracleParameter("vTrangthai",vTrangthai),
                                                                            new OracleParameter("vCD_DONVIID",vCD_DONVIID),
                                                                             new OracleParameter("vCD_TA_TRANGTHAI",vCD_TA_TRANGTHAI),
                                                                              new OracleParameter("vCD_TENDONVI",vCD_TENDONVI),
                                                                               new OracleParameter("vNgaychuyenTu",vNgaychuyenTu),
                                                                                new OracleParameter("vNgaychuyenDen",vNgaychuyenDen),
                                                                                new OracleParameter("vArrSelectID",vArrSelectID),
                                                                                new OracleParameter("vIsThuLy",vIsThuLy),
                                                                                new OracleParameter("vPhanloaixuly",vPhanloaixuly),
                                                                                new OracleParameter("vNgayThulyTu",vNgayThulyTu),
                                                                                new OracleParameter("vNgayThulyDen",vNgayThulyDen),
                                                                                new OracleParameter("vSoThuly",vSoThuly),
                                                                                 new OracleParameter("vChidao",vChidao),
                                                                                  new OracleParameter("vTraigiam",vTraigiam),
                                                                                   new OracleParameter("vTBQuahan",vTBQuahan),
                                                                                   new OracleParameter("vNgayQuahan",vNgayQuahan),
                                                                                    new OracleParameter("vThamphanID",vThamphanID),
                                                                                   new OracleParameter("vThamtravienID",vThamtravienID),
                                                                                     new OracleParameter("vLoaiCVID",vLoaiCVID),
                                                                                       new OracleParameter("vNgayNhapTu",vNgayNhapTu),
                                                                                     new OracleParameter("vNgayNhapDen",vNgayNhapDen),
                                                                                      new OracleParameter("vIsDonGoc",vIsDonGoc),
                                                                                       new OracleParameter("vIsTuHinh",vIsTuHinh),
                                                                                       new OracleParameter("vLoaiAn",vLoaiAn),
                                                                                        new OracleParameter("vCVPC_So",vCVPC_So),
                                                                                       new OracleParameter("vCVPC_Ngay",vCVPC_Ngay),
                                                                                       new OracleParameter("vCVPC_TenCQ",vCVPC_TenCQ),
                                                                                       new OracleParameter("vGuitoiCA_TA",vGuitoiCA_TA),
                                                                                 new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_GDTTT_HCTP_BC_CC.DON_SEARCH_DS_TL_MOI_CC", parameters);
            return tbl;
        }
    }
}