﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace BL.GSTP.THA
{
    public class THA_BIAN_BL
    {
        public DataTable GetAnHSTrongHeThong(Decimal toaan_ID, string ma_bi_an, string ten_bi_an, string ma_vu_an, string ten_vu_an, string so_ban_an, DateTime? ngaybanan, decimal PageIndex, decimal PageSize)
        {
            if (ngaybanan == DateTime.MinValue) ngaybanan = null;
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("toa_an_id",toaan_ID),
                                                                        new OracleParameter("ma_bi_an",ma_vu_an),
                                                                        new OracleParameter("ten_bi_an",ten_vu_an),

                                                                        new OracleParameter("ma_vu_an",ma_vu_an),
                                                                        new OracleParameter("ten_vu_an",ten_vu_an),
                                                                        new OracleParameter("so_ban_an",so_ban_an),
                                                                        new OracleParameter("ngay_ban_an",ngaybanan),

                                                                        new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize", PageSize),

                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("THA_BiAn_GetAnHSTrongHT_Paging", parameters);
            return tbl;
        }

        public DataTable GetAnHSNgoaiHeThong(Decimal toaan_ID, string ma_bi_an, string ten_bi_an, string ma_vu_an, string ten_vu_an, string so_ban_an, DateTime? ngaybanan, decimal PageIndex, decimal PageSize)
        {
            if (ngaybanan == DateTime.MinValue) ngaybanan = null;
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("toa_an_id",toaan_ID),
                                                                        new OracleParameter("ma_bi_an",ma_vu_an),
                                                                        new OracleParameter("ten_bi_an",ten_vu_an),

                                                                        new OracleParameter("ma_vu_an",ma_vu_an),
                                                                        new OracleParameter("ten_vu_an",ten_vu_an),
                                                                        new OracleParameter("so_ban_an",so_ban_an),
                                                                        new OracleParameter("ngay_ban_an",ngaybanan),

                                                                        new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize", PageSize),

                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("THA_BiAn_GetAnNgoaiHT", parameters);
            return tbl;
        }

        
        public DataTable GetByVuAn_Paging(string textsearch, decimal vuan_id,  decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("textsearch",textsearch),
                                                                        new OracleParameter("vuan_id",vuan_id),
                                                                        new OracleParameter("PageIndex",PageIndex),
                                                                        new OracleParameter("PageSize", PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("THA_BIAN_GetByVuAnPaging", parameters);
            return tbl;
        }
        public DataRow GetInfo(decimal bian_id)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("curr_bian_id",bian_id),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("THA_BIAN_GetInfo", parameters);
            if (tbl != null && tbl.Rows.Count > 0)
                return tbl.Rows[0];
            else return null;
        }

    }
}