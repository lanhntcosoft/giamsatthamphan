﻿using DAL.GSTP;
using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP.APS
{
    public class APS_DON_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable APS_DON_SEARCH(decimal vdonviID, string vMaVuViec, string vTenVuViec, DateTime? vNgayNhanTu, DateTime? vNgayNhanDen, decimal vLoaiQuanHe, decimal vQuanHePLID, decimal vSoThuTu, string vDuongSu, decimal vTrangThai, decimal vHinhThucNhanDon, decimal thamphan_id,decimal vThuKyID, decimal PhanCongTP, string V_UTTPDI, decimal vchecktk, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",vdonviID),
                                                                        new OracleParameter("vMaVuViec",vMaVuViec),
                                                                        new OracleParameter("vTenVuViec",vTenVuViec),
                                                                        new OracleParameter("vNgayNhanTu",vNgayNhanTu),
                                                                        new OracleParameter("vNgayNhanDen",vNgayNhanDen),
                                                                        new OracleParameter("vLoaiQuanHe",vLoaiQuanHe),
                                                                        new OracleParameter("vQuanHePLID",vQuanHePLID),
                                                                        new OracleParameter("vSoThuTu",vSoThuTu),
                                                                        new OracleParameter("vDuongSu",vDuongSu),
                                                                        new OracleParameter("vTrangThai",vTrangThai),
                                                                        new OracleParameter("vHinhThucNhanDon",vHinhThucNhanDon),
                                                                        new OracleParameter("thamphan_id",thamphan_id),
                                                                        new OracleParameter("vThuKyID",vThuKyID),
                                                                        new OracleParameter("PhanCongTP",PhanCongTP),
                                                                        new OracleParameter("V_UTTP",V_UTTPDI),
                                                                        new OracleParameter("vchecktk",vchecktk),
                                                                        new OracleParameter("Page_Index",PageIndex),
                                                                        new OracleParameter("Page_Size",PageSize),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_PS.APS_DON_SEARCH", parameters);
            return tbl;
        }
        public decimal GETNEWTT(decimal donviID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_DON_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }
        public decimal CHECKSTT_APS(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile, decimal vSTT)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                        new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                        new OracleParameter("vNam",vNam),
                                                                        new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("vSTT",vSTT),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("PKG_STPT_PS.APS_FILE_CHECKSTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]);

        }

        public DataTable APS_DON_TGTT_GETLIST(decimal vDONID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_DON_TGTT_GETLIST", parameters);
            return tbl;
        }
        public DataTable APS_DON_BanGiaoTaiLieu_GETLIST(decimal vDONID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("vDONID",vDONID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_DON_BGTL_GETLIST", parameters);
            return tbl;
        }
        public DataTable APS_DON_BGTT_GETINFO(decimal vDONID, string NguoiGiao, decimal NguoiNhan, string NgayBanGiao)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                        new OracleParameter("vNguoiGiao",NguoiGiao),
                                                                        new OracleParameter("vNguoiNhan",NguoiNhan),
                                                                        new OracleParameter("vNgayBanGiao",NgayBanGiao),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_DON_BGTT_GETINFO", parameters);
            return tbl;
        }
        public DataTable APS_DON_GETTOAANBYVUVIEC(decimal vDonID)
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("vDONID",vDonID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon.GetTableByProcedurePaging("APS_DON_GETTOAANBYVUVIEC", prm);
        }
        public DataTable APS_TONGDAT_DOITUONG_GETBY(decimal vDONID, decimal vTOAANID, decimal vBIEUMAUID, decimal vIsOnlyNKK)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vTOAANID",vTOAANID),
                                                                          new OracleParameter("vBIEUMAUID",vBIEUMAUID),
                                                                          new OracleParameter("vIsOnlyNKK",vIsOnlyNKK),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_TONGDAT_DOITUONG_GETBY", parameters);
            return tbl;
        }
        public DataTable APS_TONGDAT_GETLIST(decimal vDONID, decimal vToaAnID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDONID",vDONID),
                                                                         new OracleParameter("vToaAnID",vToaAnID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_TONGDAT_GETLIST", parameters);
            return tbl;
        }
        public decimal GETFILENEWTT(decimal donviID, decimal vMaGiaiDoan, decimal vNam, decimal vLoaiFile)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vdonviID",donviID),
                                                                         new OracleParameter("vMaGiaiDoan",vMaGiaiDoan),
                                                                          new OracleParameter("vNam",vNam),
                                                                           new OracleParameter("vLoaiFile",vLoaiFile),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_FILE_GETMAXTT", parameters);
            return Convert.ToDecimal(tbl.Rows[0][0]) + 1;

        }

        public DataTable APS_FILE_TONGDAT(decimal vDonID)
        {

            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vDonID",vDonID),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("APS_FILE_TONGDAT", parameters);
            return tbl;
        }
        public bool DELETE_ALLDATA_BY_VUANID(string donID)
        {
            try
            {
                OracleParameter[] parameters = new OracleParameter[] { new OracleParameter("in_VUANID", donID) };
                decimal dbl = Cls_Comon.ExcuteProcResult("PKG_GSTP_DELETE.DELETE_DATA_APS_BY_VUANID", parameters);
                return dbl == 1 ? true : false;
            }
            catch { return false; }
        }
        public bool Check_ThuLy(decimal DonID)
        {
            APS_SOTHAM_THULY ObjThuLy = dt.APS_SOTHAM_THULY.Where(x => x.DONID == DonID).FirstOrDefault();
            if (ObjThuLy != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}