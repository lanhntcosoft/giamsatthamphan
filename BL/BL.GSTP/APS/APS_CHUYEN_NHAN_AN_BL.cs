﻿using DAL.GSTP;
using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BL.GSTP.APS
{
    public class APS_CHUYEN_NHAN_AN_BL
    {
        GSTPContext dt = new GSTPContext();
        public DataTable APS_CHUYEN_NHAN_AN_GETCHUYEN()
        {
            OracleParameter[] prm = new OracleParameter[]
            {
                new OracleParameter("CurReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            return Cls_Comon.GetTableByProcedurePaging("APS_CHUYEN_NHAN_AN_GETCHUYEN", prm);
        }
        public string Check_NhanAn(decimal DonID, string Message_Ex)
        {
            string Result = "";
            APS_CHUYEN_NHAN_AN ObjNhan = dt.APS_CHUYEN_NHAN_AN.Where(x => x.VUANID == DonID).FirstOrDefault();
            if (ObjNhan != null)
            {
                if (ObjNhan.TRANGTHAI == 1) // Đã nhận
                {
                    Result = "Án đã được ";
                    DM_TOAAN ObjToaAn = dt.DM_TOAAN.Where(x => x.ID == ObjNhan.TOANHANID).FirstOrDefault();
                    if (ObjToaAn != null)
                    {
                        Result += ObjToaAn.MA_TEN;
                    }
                    Result += " nhận. " + Message_Ex;
                }
            }
            return Result;
        }
    }
}