﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace BL.GSTP.AHS
{
    public class AHS_PHUCTHAM_BICANBICAO_BL
    {
        public DataTable GetAllBiCanPhucTham(decimal vuan_id)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vu_an_id",vuan_id),
                                                                        new OracleParameter("CurReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon.GetTableByProcedurePaging("AHS_PT_BiCao_GetAll", parameters);
            return tbl;
        }
    }
}