create or replace PROCEDURE BC_TEMP_ANHVH 
IS
    VCount_canbo NUMBER:=0;V_STT NUMBER:=0;VSapvehuu_nam NUMBER:=0;VSapvehuu_nu NUMBER:=0;VSapvehuu NUMBER:=0;
    VTinhgiam NUMBER:=0;Vcount_thamphan NUMBER:=0;
    vDayHetNhiemKy NUMBER DEFAULT 60;--> Trước 60 ngày so với ngày kết thúc nhiệm kỳ sẽ cảnh báo sắp hết nhiệm kỳ
     vGroupChucDanhID NUMBER;vDangCongTac NUMBER DEFAULT 1;V_TP_NHIEMKY NUMBER;
      vDungXetXu NUMBER DEFAULT 3;V_TP_DUNG_XETXU NUMBER;
BEGIN
   ------
   DELETE BC_ANHVH_TEMP;
   COMMIT;
   --------
    FOR item IN(
    SELECT OG.*  FROM HU_ORGANIZATION@dblink_qlcb OG  WHERE OG.STATUS='A'
     START WITH OG.PARENT_ID = 'EA421F3E2EE54060BA9A0127147C2A14'
     CONNECT BY PRIOR OG.ID = OG.PARENT_ID AND OG.ID <> OG.PARENT_ID 
     )
     LOOP
        SELECT COUNT(*) INTO VCount_canbo FROM HU_EMPLOYEE@dblink_qlcb WHERE ORG_ID=item.ID
       and EMPLOYEE_STATUS in ('06822982F92E43D1A505FA5A6A9FF98E','B11674ED2FC84A4D95F52B47B4946264');
        --select * from OTHER_LIST
        --where id in ('06822982F92E43D1A505FA5A6A9FF98E','B11674ED2FC84A4D95F52B47B4946264','2F8FAD6D3B1041439AACCF1CDA8388DA');
        ----(Chính thức,Tập sự,Nghỉ việc)
        --tổng số cán bộ đang công tác
        IF(VCount_canbo>0)THEN
            V_STT:=V_STT+1;
            INSERT INTO BC_ANHVH_TEMP
            (ID_DONVI,TEN_DONVI,CB_DANGCONGTAC,STT,ID_DONVI_CHA)
            VALUES(item.ID,item.NAME,VCount_canbo,V_STT,item.PARENT_ID) ;
        END IF;
       --chuẩn bị về hưu dưới 2 tháng
        --      select * from OTHER_LIST
        --      where id in ('1D1CA022D3A49F42A8AAC70FDDCA102C','AC0B64EC198A0B4E86EAEAC0BB9C24FE');
        ----   (Nam,Nữ)
        ----------------
        VSapvehuu_nam:=0;
       SELECT COUNT(*) INTO VSapvehuu_nam FROM HU_EMPLOYEE@dblink_qlcb E
       INNER JOIN HU_EMPLOYEE_CV@dblink_qlcb C ON E.ID = C.EMPLOYEE_ID
       WHERE ORG_ID=item.ID and EMPLOYEE_STATUS in ('06822982F92E43D1A505FA5A6A9FF98E')
       AND C.GENDER='1D1CA022D3A49F42A8AAC70FDDCA102C' 
       AND ((60*12)+3)-TRUNC(MONTHS_BETWEEN(sysdate,C.BIRTH_DATE ))<=2 ;
       ----------------
       VSapvehuu_nu:=0;
      SELECT COUNT(*) INTO VSapvehuu_nu FROM HU_EMPLOYEE@dblink_qlcb E
       INNER JOIN HU_EMPLOYEE_CV@dblink_qlcb C ON E.ID = C.EMPLOYEE_ID
       WHERE ORG_ID=item.ID and EMPLOYEE_STATUS in ('06822982F92E43D1A505FA5A6A9FF98E')
       AND C.GENDER='AC0B64EC198A0B4E86EAEAC0BB9C24FE' 
       AND ((55*12)+4)-TRUNC(MONTHS_BETWEEN(sysdate,C.BIRTH_DATE ))<=2;
       ---------------
       VSapvehuu:=0;
       VSapvehuu:=(VSapvehuu_nam+VSapvehuu_nu);
       IF(VSapvehuu>0)THEN
            UPDATE BC_ANHVH_TEMP
            SET CB_CHUANBIVEHU=VSapvehuu
            WHERE ID_DONVI=item.ID;
       ELSE
          UPDATE BC_ANHVH_TEMP
            SET CB_CHUANBIVEHU=0
            WHERE ID_DONVI=item.ID;
       END IF;
       --đã giảm biên chế
      SELECT COUNT(*) INTO VTinhgiam FROM HU_EMPLOYEE@dblink_qlcb WHERE ORG_ID=item.ID
       and EMPLOYEE_STATUS in ('2F8FAD6D3B1041439AACCF1CDA8388DA');
       IF(VTinhgiam>0)THEN
            UPDATE BC_ANHVH_TEMP
            SET CB_DATINHGIAM=VTinhgiam
            WHERE ID_DONVI=item.ID;
       ELSE
          UPDATE BC_ANHVH_TEMP
            SET CB_DATINHGIAM=0
            WHERE ID_DONVI=item.ID;
       END IF;
              --tổng số thẩm phán 
       SELECT COUNT(*)into Vcount_thamphan FROM HU_EMPLOYEE@dblink_qlcb WHERE ORG_ID=item.ID
       and EMPLOYEE_STATUS in ('06822982F92E43D1A505FA5A6A9FF98E','B11674ED2FC84A4D95F52B47B4946264')
       AND POSITION_ID IN ('96D6CDE58F374DC18F5C9C822CBC0CEC','CEABF16F73E148FAB95712ABD649FD67','E5EBA672AC2E4B2D870F8C434A078756'
       ,'0B16841055E1A5498008D49EC8E203CD');
       IF(Vcount_thamphan>0)THEN
            UPDATE BC_ANHVH_TEMP
            SET CB_THAMPHAN=Vcount_thamphan
            WHERE ID_DONVI=item.ID;
       ELSE
          UPDATE BC_ANHVH_TEMP
            SET CB_THAMPHAN=0
            WHERE ID_DONVI=item.ID;
       END IF;
       ------------------
       
   -- Tổng số Thẩm phán sắp hết nhiệm kỳ (còn dưới 2 tháng) -- V_TP_NHIEMKY
   SELECT ID INTO vGroupChucDanhID FROM DM_DATAGROUP where MA='CHUCDANH';
   
    SELECT COUNT(T2.ID) INTO V_TP_NHIEMKY 
    FROM DM_CANBO T2
    INNER JOIN DM_DATAITEM T3 ON T3.ID=T2.CHUCDANHID
    WHERE T2.MADONGBO=item.ID AND T2.HIEULUC=vDangCongTac
      AND T3.GROUPID=vGroupChucDanhID
      AND INSTR('TPSC,TPTC,TPCC',T3.MA)>0 --> Thẩm phán
      AND ABS(TRUNC(SYSDATE - T2.NGAYKETTHUC))<=vDayHetNhiemKy
    ;
    ----------
    UPDATE BC_ANHVH_TEMP
            SET CB_NHIEMKY=V_TP_NHIEMKY
            WHERE ID_DONVI=item.ID;
            
   -- Tổng số TP bị dừng xét xử/ xử lý kỷ luật -- V_TP_DUNG_XETXU 
    SELECT COUNT(T2.ID) INTO V_TP_DUNG_XETXU 
    FROM DM_CANBO T2
    INNER JOIN DM_DATAITEM T3 ON T3.ID=T2.CHUCDANHID
    WHERE T2.MADONGBO=item.ID
     AND T2.HIEULUC=vDungXetXu
      AND T3.GROUPID=vGroupChucDanhID
      AND INSTR('TPSC,TPTC,TPCC',T3.MA)>0 --> Thẩm phán
      AND T2.HIEULUC=vDungXetXu;
      --------
       UPDATE BC_ANHVH_TEMP
            SET CB_DUNG_XX=V_TP_DUNG_XETXU
            WHERE ID_DONVI=item.ID;
     END LOOP;
    COMMIT;
END BC_TEMP_ANHVH;