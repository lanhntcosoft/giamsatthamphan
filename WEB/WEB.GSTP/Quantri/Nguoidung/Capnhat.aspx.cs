﻿using DAL.GSTP;
using BL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB.GSTP.Quantri.Nguoidung
{
    public partial class Capnhat : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCombobox();
                string strID = Request["CID"] + "";
                if (strID != "")
                {
                    QT_NGUOIDUNG_BL oBL = new QT_NGUOIDUNG_BL();
                    QT_NGUOISUDUNG oT = oBL.GetByID(Convert.ToDecimal(strID));
                    if (oT == null) return;
                    ddlDonvi.SelectedValue = oT.DONVIID.ToString();
                    txtUsername.Text = oT.USERNAME;

                    chkHieuluc.Checked = oT.HIEULUC == 1 ? true : false;
                    chkIsPhanloaidon.Checked = oT.ISPHANLOAIDON == 1 ? true : false;
                    txtDienthoai.Text = oT.DIENTHOAI;
                    txtEmail.Text = oT.EMAIL;
                    txtGhichu.Text = oT.GHICHU;
                    chkIsDomain.Checked = oT.ISACCDOMAIN == 1 ? true : false;
                    ddlLoaiNhom.SelectedValue = oT.LOAIUSER.ToString();
                    LoadNhomquyen(Convert.ToDecimal(ddlDonvi.SelectedValue));
                    LoadPhongban(Convert.ToDecimal(ddlDonvi.SelectedValue));
                    if (oT.NHOMNSDID != null)
                        ddlNhomquyen.SelectedValue = oT.NHOMNSDID.ToString();
                    else
                        ddlNhomquyen.SelectedValue = "0";
                    if (oT.PHONGBANID != null)
                        ddlPhongban.SelectedValue = oT.PHONGBANID.ToString();
                    else
                        ddlPhongban.SelectedValue = "0";
                    txtPass.Enabled = false; txtRePass.Enabled = false;

                    LoadCanbo(Convert.ToDecimal(ddlDonvi.SelectedValue), Convert.ToDecimal(ddlPhongban.SelectedValue));
                    if (oT.CANBOID != null)
                        ddlCanbo.SelectedValue = oT.CANBOID.ToString();
                    hddID.Value = oT.ID.ToString();
                }
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            }
        }
        private void LoadCanbo(decimal DonviID, decimal PhongBanID)
        {
            //Load cán bộ
            ddlCanbo.Items.Clear();
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.DM_CANBO_GETBYDONVI_PHONGBAN(DonviID, PhongBanID);
            ddlCanbo.DataSource = oCBDT;
            ddlCanbo.DataTextField = "MA_TEN";
            ddlCanbo.DataValueField = "ID";
            ddlCanbo.DataBind();
            ddlCanbo.Items.Insert(0, new ListItem("--Chọn cán bộ--", "0"));

        }
        private void LoadCombobox()
        {
            string strDonViID = Session["DonViID"] + "";
            if (strDonViID != "")
            {
                decimal DonViID = Convert.ToDecimal(strDonViID);
                DM_TOAAN oT = dt.DM_TOAAN.Where(x => x.ID == DonViID).FirstOrDefault();
                if (oT.LOAITOA == "CAPCAO")
                {
                    ddlDonvi.Items.Insert(0, new ListItem(oT.TEN, oT.ID.ToString()));
                }
                else
                {
                    DM_TOAAN_BL oBL = new DM_TOAAN_BL();
                    ddlDonvi.DataSource = oBL.DM_TOAAN_GETBY(DonViID);
                    ddlDonvi.DataTextField = "arrTEN";
                    ddlDonvi.DataValueField = "ID";
                    ddlDonvi.DataBind();
                }
            }
            string strLoaiUser = Session["LoaiUser"] + "";

            ddlLoaiNhom.Items.Clear();
            ddlLoaiNhom.Items.Add(new ListItem("Mặc định", "0"));
            if (strLoaiUser == "2")
            {
                ddlLoaiNhom.Items.Add(new ListItem("Quản trị đơn vị", "1"));
                ddlLoaiNhom.Items.Add(new ListItem("Quản trị hệ thống", "2"));
            }
            LoadNhomquyen(Convert.ToDecimal(strDonViID));
            LoadPhongban(Convert.ToDecimal(strDonViID));
            LoadCanbo(Convert.ToDecimal(strDonViID),Convert.ToDecimal(ddlPhongban.SelectedValue));
        }
        private void LoadPhongban(decimal DonViID)
        {
            ddlPhongban.Items.Clear();
            ddlPhongban.DataSource = dt.DM_PHONGBAN.Where(x => x.TOAANID == DonViID).ToList();
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
        }
        private void LoadNhomquyen(decimal DonViID)
        {
            ddlNhomquyen.Items.Clear();
            DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == DonViID).FirstOrDefault();
            QT_NHOMNGUOIDUNG_BL bl = new QT_NHOMNGUOIDUNG_BL();
            ddlNhomquyen.DataSource = dt.QT_NHOMNGUOIDUNG.Where(x => x.LOAITOA == oTA.LOAITOA).ToList();
            ddlNhomquyen.DataTextField = "TEN";
            ddlNhomquyen.DataValueField = "ID";
            ddlNhomquyen.DataBind();
            ddlNhomquyen.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                lbthongbao.Text = "";
                if (txtUsername.Text.Trim() == "")
                {
                    lbthongbao.Text = "User name không được để trống !";
                    return;
                }
                if (ddlNhomquyen.SelectedIndex == 0)
                {
                    lbthongbao.Text = "Chưa chọn nhóm quyền !";
                    return;
                }
                QT_NGUOIDUNG_BL oBL = new QT_NGUOIDUNG_BL();
                List<QT_NGUOISUDUNG> lstnguoidung = oBL.GetByUserName(txtUsername.Text.Trim());
                // string strID = Request["CID"] + "";

                if (hddID.Value == "" || hddID.Value == "0")
                {
                    if (lstnguoidung.Count > 0)
                    {
                        lbthongbao.Text = "Tên đăng nhập đã tồn tại!";
                        txtUsername.Focus();
                        return;
                    }
                    if (chkIsDomain.Checked == false)
                    {
                        //Kiểm tra mật khẩu
                        if (txtPass.Text.Length < 6)
                        {
                            lbthongbao.Text = "Mật khẩu chưa hợp lệ !";
                            txtPass.Focus();
                            return;
                        }
                        if (txtPass.Text != txtRePass.Text)
                        {
                            lbthongbao.Text = "Nhập lại mật khẩu chưa đúng !";
                            txtRePass.Focus();
                            return;
                        }
                    }
                    string strPass = Cls_Comon.MD5Encrypt(txtPass.Text);

                    QT_NGUOISUDUNG oT = new QT_NGUOISUDUNG();
                    oT.DONVIID = Convert.ToDecimal(ddlDonvi.SelectedValue);
                    oT.USERNAME = txtUsername.Text;
                    oT.PASSWORD = strPass;
                    oT.HOTEN = ddlCanbo.SelectedItem.Text;
                    oT.HIEULUC = chkHieuluc.Checked ? 1 : 0;
                    oT.ISPHANLOAIDON = chkIsPhanloaidon.Checked ? 1 : 0;
                    oT.DIENTHOAI = txtDienthoai.Text;
                    oT.EMAIL = txtEmail.Text;
                    oT.GHICHU = txtGhichu.Text;
                    oT.NHOMNSDID = Convert.ToDecimal(ddlNhomquyen.SelectedValue);
                    oT.PHONGBANID = Convert.ToDecimal(ddlPhongban.SelectedValue);
                    oT.LOAIUSER = Convert.ToDecimal(ddlLoaiNhom.SelectedValue);
                    oT.ISACCDOMAIN = chkIsDomain.Checked ? 1 : 0;
                    oT.NGAYTAO = DateTime.Now;
                    oT.CANBOID = Convert.ToDecimal(ddlCanbo.SelectedValue);
                    oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.QT_NGUOISUDUNG.Add(oT);
                    dt.SaveChanges();
                }
                else
                {
                    decimal ID = Convert.ToDecimal(hddID.Value);
                    QT_NGUOISUDUNG oT = dt.QT_NGUOISUDUNG.Where(x => x.ID == ID).FirstOrDefault();
                    if (lstnguoidung.Count >= 1 && oT.USERNAME.Trim().ToUpper() != txtUsername.Text.Trim().ToUpper())
                    {
                        lbthongbao.Text = "Tên đăng nhập đã tồn tại!";
                        txtUsername.Focus();
                        return;
                    }

                    if (oT == null) return;
                    oT.DONVIID = Convert.ToDecimal(ddlDonvi.SelectedValue);
                    oT.USERNAME = txtUsername.Text;
                    oT.HOTEN = ddlCanbo.SelectedItem.Text;
                    oT.HIEULUC = chkHieuluc.Checked ? 1 : 0;
                    oT.ISPHANLOAIDON = chkIsPhanloaidon.Checked ? 1 : 0;
                    oT.DIENTHOAI = txtDienthoai.Text;
                    oT.EMAIL = txtEmail.Text;
                    oT.GHICHU = txtGhichu.Text;
                    oT.ISACCDOMAIN = chkIsDomain.Checked ? 1 : 0;
                    oT.NHOMNSDID = Convert.ToDecimal(ddlNhomquyen.SelectedValue);
                    oT.PHONGBANID = Convert.ToDecimal(ddlPhongban.SelectedValue);
                    oT.LOAIUSER = Convert.ToDecimal(ddlLoaiNhom.SelectedValue);
                    oT.NGAYSUA = DateTime.Now;
                    oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    oT.CANBOID = Convert.ToDecimal(ddlCanbo.SelectedValue);
                    dt.SaveChanges();
                }
                lbthongbao.Text = "Lưu thành công !";
                hddID.Value = "0";
                txtUsername.Text = txtPass.Text = txtRePass.Text = "";
                ddlCanbo.SelectedIndex = 0;
                txtDienthoai.Text = txtEmail.Text = txtGhichu.Text = "";
                //Response.Redirect("Danhsach.aspx");
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lblquaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsach.aspx");
        }
        protected void ddlDonvi_TextChanged(object sender, EventArgs e)
        {
            LoadNhomquyen(Convert.ToInt32(ddlDonvi.SelectedValue));
            LoadPhongban(Convert.ToDecimal(ddlDonvi.SelectedValue));
            LoadCanbo(Convert.ToDecimal(ddlDonvi.SelectedValue), Convert.ToDecimal(ddlPhongban.SelectedValue));
        }
        protected void chkIsDomain_CheckedChanged(object sender, EventArgs e)
        {
            string strID = Request["CID"] + "";

            if (chkIsDomain.Checked)
            {
                txtPass.Enabled = false; txtRePass.Enabled = false;
            }
            else
            {
                if (strID == "") { txtPass.Enabled = true; txtRePass.Enabled = true; }
            }
        }
        protected void ddlPhongban_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCanbo(Convert.ToDecimal(ddlDonvi.SelectedValue), Convert.ToDecimal(ddlPhongban.SelectedValue));
        }
    }
}
