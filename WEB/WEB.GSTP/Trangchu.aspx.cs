﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP
{
    public partial class Trangchu : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MaHeThong"] + "" != "")
            {
                decimal HeThongID = Convert.ToDecimal(Session["MaHeThong"].ToString()),
                        UserID = Session[ENUM_SESSION.SESSION_USERID] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                string strMaCT = Session["MaChuongTrinh"] + "";
                QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.ID == HeThongID).FirstOrDefault();
                if (oT != null)
                {
                    if (oT.MA == "GSTP" && (strMaCT == "" || strMaCT == "0"))
                    {
                        Response.Redirect("GSTP/Danhsachtonghop.aspx");
                    }
                }
                #region Nếu User là chánh án hoặc phó chánh án
                //DM_CANBO_BL bl = new DM_CANBO_BL();
                //DataTable tbl = bl.DM_CANBO_GETCA_PCA_BY_USERID(UserID);
                //    if (tbl != null && tbl.Rows.Count > 0)
                //{
                //    ThongKe.Visible = true;
                //    VB_CTTCT_ChuaDang1.Visible = false;
                //}
                //else
                //{
                //    VB_CTTCT_ChuaDang1.Visible = true;
                //    ThongKe.Visible = false;
                //}
                QT_MENU_BL qtBL = new QT_MENU_BL();
                decimal IDNhom = 0;
                if (Session[ENUM_SESSION.SESSION_NHOMNSDID] != null && Session[ENUM_SESSION.SESSION_NHOMNSDID] + "" != "")
                {
                    IDNhom = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID] + "");
                }
                DataTable lst_home = qtBL.Qt_Nhom_ISHome_Get_List(IDNhom);
                VB_CTTCT_ChuaDang1.Visible = false;
                if (lst_home != null && lst_home.Rows.Count > 0 && lst_home.Rows[0]["VIEW_TK"] + "" == "1")
                {
                    if (Session["MA_HDSD"] + "" == "QLA"|| Session["MA_HDSD"] + "" == "")
                        {
                        ThongKe.Visible = false;//anhvh vh đóng vào để tập huấn
                    }
                    else
                        ThongKe.Visible = false;
                   // VB_CTTCT_ChuaDang1.Visible = false;
                }
                else
                {
                   // VB_CTTCT_ChuaDang1.Visible = true;
                    ThongKe.Visible = false;
                }
                //----------------------
                if(Session["CAP_XET_XU"]+""== "CAPCAO")
                {
                    H_ThongKe.Visible = false;
                    H_ThongKe_CC.Visible = true;
                }
                else if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    H_ThongKe.Visible = true;
                    H_ThongKe_CC.Visible = false;
                }
                #endregion
            }
        }
    }
}