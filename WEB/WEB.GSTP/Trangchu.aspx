﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="Trangchu.aspx.cs" Inherits="WEB.GSTP.Trangchu" %>

<%@ Register Src="~/UserControl/QLA/NhanAn.ascx" TagPrefix="uc1" TagName="NhanAn" %>
<%@ Register Src="~/UserControl/DKK/DangKyNhanVBTongDat.ascx" TagPrefix="uc1" TagName="DangKyNhanVBTongDat" %>
<%@ Register Src="~/UserControl/DKK/DonKKOnline.ascx" TagPrefix="uc1" TagName="DonKKOnline" %>
<%@ Register Src="~/UserControl/TP/ThongKe.ascx" TagPrefix="uc1" TagName="ThongKe" %>
<%@ Register Src="~/QLAN/GDTTT/VuAn/BaoCao/uThongKe.ascx" TagPrefix="uc1" TagName="GDTThongKe" %>
<%@ Register Src="~/UserControl/QLA/ThongKeVanBanTongDatChuaDangCTTDT.ascx" TagPrefix="uc1" TagName="VB_CTTCT_ChuaDang" %>
<%@ Register Src="~/QLAN/GDTTT/Hoso/H_ThongKe.ascx" TagPrefix="uc1" TagName="H_ThongKe" %>
<%@ Register Src="~/QLAN/GDTTT/Hoso/H_ThongKe_CC.ascx" TagPrefix="uc1" TagName="H_ThongKe_CC" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <uc1:H_ThongKe runat="server" ID="H_ThongKe" />

    <uc1:H_ThongKe_CC runat="server" ID="H_ThongKe_CC" />
    <uc1:ThongKe runat="server" ID="ThongKe" Visible="false" />
    <uc1:DonKKOnline runat="server" ID="DonKKOnline" />
    <uc1:DangKyNhanVBTongDat runat="server" ID="DangKyNhanVBTongDat" />
    <uc1:NhanAn runat="server" ID="NhanAn" />
    <uc1:GDTThongKe runat="server" ID="GDTThongKe1" />
    <uc1:VB_CTTCT_ChuaDang runat="server" ID="VB_CTTCT_ChuaDang1" />
</asp:Content>
