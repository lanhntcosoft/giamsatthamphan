﻿using DAL.GSTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Module.Common;

namespace WEB.GSTP
{
    public partial class ChangePass : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strUserID = Session[ENUM_SESSION.SESSION_USERID] + "";
                if (strUserID == "") Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");              
                txtUserName.Text = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oT = dt.QT_NGUOISUDUNG.Where(x => x.ID == current_id).FirstOrDefault();
                if (oT.ISACCDOMAIN == 1)
                {
                    txtPass.Enabled = txtRePass.Enabled = cmdLogIn.Enabled = false;
                    lttMsg.Text = "<div class='error_msg' style='color: red; '>Tài khoản domain không cho phép đổi mật khẩu !</div>";
                }
                List<DM_TRANGTINH> lstHT = dt.DM_TRANGTINH.Where(x => x.MATRANG == "hotrologingscm").ToList();
                if (lstHT.Count > 0)
                {
                    lsthotro.Text = lstHT[0].NOIDUNG;
                }
            }
            catch (Exception ex) { }
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
        }
        protected void lblBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(Cls_Comon.GetRootURL() + "/Launcher.aspx");
        }

        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            if(txtPass.Text.Trim()=="")
            {
                lttMsg.Text = "Chưa nhập mật khẩu mới !";
                return;
            }
            if (txtPass.Text.Length<6)
            {
                lttMsg.Text = "Mật khẩu phải trên 6 ký tự !";
                return;
            }
            if (txtPass.Text!=txtRePass.Text)
            {
                lttMsg.Text = "Nhập lại mật khẩu không đúng !";
                return;
            }
            decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);           
            QT_NGUOISUDUNG oT = dt.QT_NGUOISUDUNG.Where(x => x.ID == current_id).FirstOrDefault();
            oT.PASSWORD = Cls_Comon.MD5Encrypt(txtPass.Text);
            oT.NGAYSUA = DateTime.Now;
            oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            dt.SaveChanges();
            lttMsg.Text = "Đổi mật khẩu thành công !";
        }
    }
}