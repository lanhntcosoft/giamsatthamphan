﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.GSTP.Models
{
    /* Thông tin thụ lý (Object) */
    public class THU_LY
    {
        /* Số thụ lý */
        public int SOTHULY { get; set; }
        /* Ngày thụ lý */
        public string NGAYTHULY { get; set; }
        /* Mã tòa án thụ lý */
        public string TOAANTHULY { get; set; }
    }
}