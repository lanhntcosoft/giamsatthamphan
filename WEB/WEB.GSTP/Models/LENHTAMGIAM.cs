﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.GSTP.Models
{
    /* Lệnh tạm giam */
    public class LENHTAMGIAM
    {
        /* Hiệu lực từ ngày dd-MM-yyyy */
        public string HIEULUCTUNGAY { get; set; }
        /* Hiệu lực đến ngày dd-MM-yyyy */
        public string HIEULUCDENNGAY { get; set; }
    }
}