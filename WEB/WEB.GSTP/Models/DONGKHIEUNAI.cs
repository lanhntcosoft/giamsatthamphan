﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.GSTP.Models
{
    /* Đồng khiếu nại kiểu <object> */
    public class DONGKHIEUNAI
    {
        /* Tên người đồng khiếu nại */
        public string TEN { get; set; }
        /* Địa chỉ người đồng khiếu nại */
        public string DIACHI { get; set; }
    }
}