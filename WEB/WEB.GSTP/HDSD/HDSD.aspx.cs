﻿using System;
using System.Collections.Generic;
using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System.IO;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using BL.GSTP.GDTTT;
using System.Text;

namespace WEB.GSTP.HDSD
{
    public partial class HDSD : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        private const decimal ROOT = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["MaChuongTrinh"] = "HDSD_APP";
                Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
                DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault();
                DM_DATAITEM oCD = new DM_DATAITEM();
                decimal chucdanh_id = 0;
                if (oCB != null)
                {
                    chucdanh_id = (string.IsNullOrEmpty(oCB.CHUCDANHID + "")) ? 0 : Convert.ToDecimal(oCB.CHUCDANHID);
                    oCD = dt.DM_DATAITEM.Where(x => x.ID == chucdanh_id).FirstOrDefault();
                }
                //////////-----------
                String pb = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
                String Ma_HT = Session["MA_HDSD"] + "";
                if (Ma_HT != "")
                {
                    switch (Ma_HT)
                    {
                        case "GSTP":
                            break;
                        case "QLA":
                            iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_STPT.pdf";
                            break;
                        case "GDT":
                            if (oCD.MA == "TPTATC")
                            {
                                iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_GDTTT_TP.pdf";
                            }
                            else
                            {
                                if (pb == "2" || pb == "3" || pb == "4" || pb == "14" || pb == "15" || pb == "16")
                                {
                                    iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_GDTTT.pdf";
                                }
                                else if (pb == "1" || pb == "4" || pb == "13")
                                {
                                }
                            }
                            break;
                        case "TDKT":
                            break;
                        case "TCCB":
                            break;
                        case "QTHT":
                            break;
                    }
                }
                else
                {
                    string strUserID = Session[ENUM_SESSION.SESSION_USERID] + "";
                    QT_NGUOIDUNG_BL oBL = new QT_NGUOIDUNG_BL();
                    decimal USERID = Convert.ToDecimal(strUserID);
                    DataTable lstHT;
                    string strSessionKeyHT = "HETHONGGETBY_" + USERID.ToString();
                    if (Session[strSessionKeyHT] == null)
                        lstHT = oBL.QT_HETHONG_GETBYUSER(USERID);
                    else
                        lstHT = (DataTable)Session[strSessionKeyHT];

                    if (lstHT.Rows.Count > 0)
                    {
                        foreach (DataRow r in lstHT.Rows)
                        {
                            switch (r["MA"] + "")
                            {
                                case "GSTP":
                                    break;
                                case "QLA":
                                    iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_STPT.pdf";
                                    break;
                                case "GDT":
                                    if (oCD.MA == "TPTATC")
                                    {
                                        iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_GDTTT_TP.pdf";
                                    }
                                    else
                                    {
                                        if (pb == "2" || pb == "3" || pb == "4" || pb == "14" || pb == "15" || pb == "16")
                                        {
                                            iframe_pub.Src = "/UI/pdfjs/web/viewer.html?file=%2FHDSD/HDSD_GDTTT.pdf";
                                        }
                                        else if (pb == "1" || pb == "4"|| pb == "13")
                                        {
                                        }
                                    }
                                    break;
                                case "TDKT":
                                    break;
                                case "TCCB":
                                    break;
                                case "QTHT":
                                    break;
                            }
                        }
                    }
                }
            }
        }
        // DownLoad("~/HDSD/HDSD_GDTTT_TP.pdf");
        public void DownLoad(string Path)
        {
            string filePath = Server.MapPath(Path);
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            if (file.Exists)
            {
                FileStream fileStream = File.OpenRead(filePath);
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(file.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                Response.Clear();
                Response.ContentType = "application/pdf";
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name.Replace(" ", "_").Trim());
                Response.BinaryWrite(memStream.ToArray());
                Response.Flush();
                Response.Close();
                Response.End();
            }
        }
    }
}