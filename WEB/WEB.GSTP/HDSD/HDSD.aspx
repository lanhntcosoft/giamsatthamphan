﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="HDSD.aspx.cs" Inherits="WEB.GSTP.HDSD.HDSD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content_body">
        <iframe id="iframe_pub" runat="server"></iframe>
    </div>
    <style>
        iframe {
            border: 0 none;
            height: 900px;
            width: 98%;
        }
        .content_body {
            margin-bottom: 0px;
        }
        .dxsplPane
        {
            width:98% !important;
        }
        .dxsplS
        {
            display:none;
        }
    </style>
</asp:Content>
