﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.BaoCao.AHC
{
    public partial class rpt36HC : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt36HC()
        {
            InitializeComponent();
        }

        private void xrTableCell19_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell19.Text = xrTableCell19.Text.ToUpper();
        }

        private void xrLabel4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel4.Text = xrLabel4.Text.ToUpper();
        }

        private void Detail_NVLQ_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)Detail_NVLQ.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ttlNVLQ.Visible = false;
                xrTable_listNVLQ.Visible = false;
            }
        }

        private void Detail_ThamPhan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)Detail_ThamPhan.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ttlThamPhan.Visible = false;
                xrTable_listThamPhan.Visible = false;
            }
        }

        private void xrTable_ThamPhanDK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)GetCurrentRow();
            if (r == null || r.DataView.Count == 0 || r["THAMPHANDUKHUYET"] + "" == "")
            {
                xrTable_ThamPhanDK.Visible = false;
            }
        }

        private void Detail_KSVDuKhuyet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)GetCurrentRow();
            if (r == null || r.DataView.Count == 0 || r["KSVDUKHUYET"] + "" == "")
            {
                Detail_KSVDuKhuyet.Visible = false;
            }
        }

        private void Detail_KSV_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)Detail_KSV.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_listKSV.Visible = false;
            }
        }

        private void Detail_ttlKSV_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)Detail_KSV.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                Detail_ttlKSV.Visible = false;
            }
        }

        private void GroupHeader_ThamPhanDK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)GetCurrentRow();
            if (r == null || r.DataView.Count == 0 || r["THAMPHANDUKHUYET"] + "" == "")
            {
                GroupHeader_ThamPhanDK.Visible = false;
            }
        }

        private void Detail_NguoiTGTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)Detail_NguoiTGTT.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ttlNguoiTGTT.Visible = false;
                xrTable_listNguoiTGTT.Visible = false;
            }
        }

    }
}
