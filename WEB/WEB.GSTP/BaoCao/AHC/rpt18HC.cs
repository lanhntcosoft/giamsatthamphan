﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.BaoCao.AHC
{
    public partial class rpt18HC : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt18HC()
        {
            InitializeComponent();
        }

        private void xrTableCell19_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell19.Text = xrTableCell19.Text.ToUpper();
        }

        private void xrLabel4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel4.Text = xrLabel4.Text.ToUpper();
        }

        private void DetailReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable4.Visible = false;
                xrTable2.Visible = false;
            }
        }

        private void DetailReport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport1.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable10.Visible = false;
                xrTable11.Visible = false;
            }
        }

        private void DetailReport3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport3.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable6.Visible = false;
                xrTable13.Visible = false;
            }
        }

    }
}
