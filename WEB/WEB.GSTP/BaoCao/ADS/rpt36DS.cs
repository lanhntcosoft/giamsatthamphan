﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.BaoCao.ADS
{
    public partial class rpt36DS : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt36DS()
        {
            InitializeComponent();
        }

        private void xrTableCell46_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell46.Text = xrTableCell46.Text.Replace("Ông", "").Replace("Bà", "");
        }

        private void xrTableCell47_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell47.Text = xrTableCell47.Text.Replace("Ông", "").Replace("Bà", "");
        }

        private void xrTableCell19_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell19.Text = xrTableCell19.Text.ToUpper();
        }
    }
}
