﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.BaoCao.ADS
{
    public partial class rpt77DS : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt77DS()
        {
            InitializeComponent();
        }

        private void xrLabel4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel4.Text = xrLabel4.Text.ToUpper();
        }

        private void xrTableCell36_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell36.Text = xrTableCell36.Text.Replace("Ông", "").Replace("Bà", "");
        }

        private void xrTableCell4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell4.Text = xrTableCell4.Text.ToUpper();
        }

        private void Detail_ThamPhanDK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (string.IsNullOrEmpty(GetCurrentColumnValue("THAMPHANDUKHUYET") + ""))
            {
                Detail_ThamPhanDK.Visible = false;
            }
        }

        private void Detail_KSVDK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (string.IsNullOrEmpty(GetCurrentColumnValue("KIEMSATVIENDUKHUYET") + ""))
            {
                Detail_KSVDK.Visible = false;
            }
        }

        private void xrTable_ND_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NguyenDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ND.Visible = false;
            }
        }
        private void xrTable_ND_Ten_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NguyenDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ND_Ten.Visible = false;
            }
        }
        private void xrTable_ND_SDT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NguyenDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ND_SDT.Visible = false;
            }
            else
            {
                xrTable_ND_SDT.Visible = true;
                if (string.IsNullOrEmpty(r["DIENTHOAI"] + "") && string.IsNullOrEmpty(r["FAX"] + ""))
                {
                    xrTable_ND_SDT.Visible = false;
                }
            }
        }
        private void xrTable_ND_Email_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NguyenDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_ND_Email.Visible = false;
            }
            else
            {
                xrTable_ND_Email.Visible = true;
                if (string.IsNullOrEmpty(r["EMAIL"] + ""))
                {
                    xrTable_ND_Email.Visible = false;
                }
            }
        }

        private void xrTable_BD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_BiDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_BD.Visible = false;
            }
        }
        private void xrTable_BD_Ten_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_BiDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_BD_Ten.Visible = false;
            }
        }
        private void xrTable_BD_SDT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_BiDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_BD_SDT.Visible = false;
            }
            else
            {
                xrTable_BD_SDT.Visible = true;
                if (string.IsNullOrEmpty(r["DIENTHOAI"] + "") && string.IsNullOrEmpty(r["FAX"] + ""))
                {
                    xrTable_BD_SDT.Visible = false;
                }
            }
        }
        private void xrTable_BD_Email_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_BiDon.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_BD_Email.Visible = false;
            }
            else
            {
                xrTable_BD_Email.Visible = true;
                if (string.IsNullOrEmpty(r["EMAIL"] + ""))
                {
                    xrTable_BD_Email.Visible = false;
                }
            }
        }

        private void xrTable_NVLQ_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NVLQ.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_NVLQ.Visible = false;
            }
        }
        private void xrTable_NVLQ_Ten_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NVLQ.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_NVLQ_Ten.Visible = false;
            }
        }
        private void xrTable_NVLQ_SDT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NVLQ.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_NVLQ_SDT.Visible = false;
            }
            else
            {
                xrTable_NVLQ_SDT.Visible = true;
                if (string.IsNullOrEmpty(r["DIENTHOAI"] + "") && string.IsNullOrEmpty(r["FAX"] + ""))
                {
                    xrTable_NVLQ_SDT.Visible = false;
                }
            }
        }
        private void xrTable_NVLQ_Email_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            System.Data.DataRowView r = (System.Data.DataRowView)DetailReport_NVLQ.GetCurrentRow();
            if (r == null || r.DataView.Count == 0)
            {
                xrTable_NVLQ_Email.Visible = false;
            }
            else
            {
                xrTable_NVLQ_Email.Visible = true;
                if (string.IsNullOrEmpty(r["EMAIL"] + ""))
                {
                    xrTable_NVLQ_Email.Visible = false;
                }
            }
        }
    }
}
