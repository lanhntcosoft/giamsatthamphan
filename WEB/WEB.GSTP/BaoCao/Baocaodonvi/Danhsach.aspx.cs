﻿
using BL.GSTP;
using BL.GSTP.Danhmuc;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;

using System.Globalization;
using System.Web.UI.WebControls;

using BL.THONGKE;
using BL.THONGKE.Manager;
using BL.THONGKE.Info;
using BL.ThongKe;
using BL.GSTP.GDTTT;

using System.Reflection;

namespace WEB.GSTP.Baocao.Baocaodonvi
{
    public partial class Danhsach : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        private const int VIEWPOPUP = 1, EXPORTECXCEL = 2;
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.cmd_exels);

            //CONVERT SESSION
            ConvertSessions cs = new ConvertSessions();
            if (cs.convertSession2() == false)
            {
                return;
            }
            try
            {
                if (!IsPostBack)
                {
                    Drop_Levels_init();
                    Drop_courts_init();
                    Drop_courts_ext_init();
                    ddlGroupBaoCao_init();
                    ddlBaoCao_init();
                    ddlBaoCao_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                lstMsg.Text = ex.Message;
            }
        }
        protected void cmd_webs_Click(object sender, EventArgs e)
        {
            if (!CheckValidate())
            {
                return;
            }
            DateTime TuNgay = DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime DenNgay = DateTime.Parse(txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            ExportReport(VIEWPOPUP, TuNgay, DenNgay);
        }
        protected void cmd_exels_Click(object sender, EventArgs e)
        {
            if (!CheckValidate())
            {
                return;
            }
            DateTime TuNgay = DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime DenNgay = DateTime.Parse(txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            ExportReport(EXPORTECXCEL, TuNgay, DenNgay);
        }
        /// <summary>
        /// xuất bao cáo, type=1 (web), type=2 (excel)
        /// </summary>
        /// <param name="type"></param>
        private void ExportReport(int type, DateTime TuNgay, DateTime DenNgay)
        {
            //từ ngày
            int _REPORT_TIME_ID = int.Parse(TuNgay.ToString("yyyyMMdd"));
            //đến ngày
            int _REPORT_TIME_ID2 = int.Parse(DenNgay.ToString("yyyyMMdd"));
            //tòa án
            int _COURT_ID = 0, _ISDONVITRUCTHUOC = 0, _COURT_EXTID = 0;
            string _COURT_NAME = Drop_courts.SelectedItem.Text;
            if (Drop_courts.Items.Count > 0)
                _COURT_ID = int.Parse(Drop_courts.SelectedValue);
            if (Drop_courts_ext.Items.Count > 0)
                _COURT_EXTID = int.Parse(Drop_courts_ext.SelectedValue);
            if (Drop_courts_ext.Visible)
                _ISDONVITRUCTHUOC = 1;// Có tổng hợp đơn vị trực thuộc (đơn vị cấp dưới)
            //mẫu báo cáo
            int _ID_BAOCAO = int.Parse(ddlBaoCao.SelectedValue);

            //tội danh
            string _sCRIMINAL_ID = "";
            //loại vụ việc
            string _sCASE_ID = "";

            /* Xuất dữ liệu báo cáo */
            if (type == VIEWPOPUP)//web (xem trên của sổ popup)
            {
                ExportReport_Web(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (type == EXPORTECXCEL)//excel
            {
                ExportReport_Excel(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }

            /* Xuất toàn bộ các mẩu báo cáo (html)
            ExportReportAll_Html(_REPORT_TIME_ID, _REPORT_TIME_ID2, _COURT_ID, _COURT_NAME, _ID_BAOCAO, _sCRIMINAL_ID, _sCASE_ID, _COURT_ID_Ext);
            */
        }
        /// <summary>
        /// xuat bao cao ra excel _COURT_EXTID, _ISDONVITRUCTHUOC
        /// </summary>
        /// <param name="_REPORT_TIME_ID"></param>
        /// <param name="_REPORT_TIME_ID2"></param>
        /// <param name="_COURT_ID"></param>
        /// <param name="_COURT_NAME"></param>
        /// <param name="_ID_BAOCAO"></param>
        /// <param name="_sCRIMINAL_ID"></param>
        /// <param name="_sCASE_ID"></param>
        /// <param name="_COURT_EXTID"> Toa id</param>
        /// <param name="_ISDONVITRUCTHUOC"> don vi</param>
        private void ExportReport_Excel(int _COURT_ID, string _COURT_NAME, int _COURT_EXTID, int _ISDONVITRUCTHUOC, string _sCASE_ID, int _ID_BAOCAO, string _sCRIMINAL_ID, int _REPORT_TIME_ID, int _REPORT_TIME_ID2)
        {
            string reportText = getContentReport(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);

            //-------------------Export---------------------------
            Literal Table_Str_Totals = new Literal();
            Table_Str_Totals.Text = reportText;
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=BaoCao_" + _ID_BAOCAO + ".xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        private string getContentReport(int _COURT_ID, string _COURT_NAME, int _COURT_EXTID, int _ISDONVITRUCTHUOC, string _sCASE_ID, int _ID_BAOCAO, string _sCRIMINAL_ID, int _REPORT_TIME_ID, int _REPORT_TIME_ID2)
        {
            List<BL.THONGKE.Info.Judge_Report> li_sw = new List<BL.THONGKE.Info.Judge_Report>();
            String _COURT_ID_Ext = "";
            #region Hình sự
            if (_ID_BAOCAO == 19)
            {
                //Hình sự Sơ thẩm cá nhân 19
                M_Judge_Criminal M_Objecs = new M_Judge_Criminal();
                li_sw = M_Objecs.Judge_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 68)
            {
                //Hình sự Sơ thẩm pháp nhân 68
                M_Criminal_Instan_Com M_Objecs = new M_Criminal_Instan_Com();
                li_sw = M_Objecs.Judge_crimi_com_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 20)
            {
                //Hình sự Phúc thẩm cá nhân 20
                M_Criminal_Appeals M_Objecs = new M_Criminal_Appeals();
                li_sw = M_Objecs.Criminal_Appeals_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Appeals_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 71)
            {
                //Hình sự Phúc thẩm pháp nhân 71
                M_Criminal_Appea_Com M_Objecs = new M_Criminal_Appea_Com();
                li_sw = M_Objecs.Criminal_Appeals_Com_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Appeals_Com_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 21)
            {
                //Hình sự GĐT cá nhân 21
                M_Criminal_Cassation M_Objecs = new M_Criminal_Cassation();
                li_sw = M_Objecs.Criminal_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 87)
            {
                //Hình sự GĐT pháp nhân 87
                M_Criminal_Cassati_Com M_Objecs = new M_Criminal_Cassati_Com();
                li_sw = M_Objecs.Criminal_Cassation_Com_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Cassation_Com_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 22)
            {
                //Hình sự TT cá nhân 22
                M_Criminal_Retrials M_Objecs = new M_Criminal_Retrials();
                li_sw = M_Objecs.Criminal_Retrials_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Retrials_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 88)
            {
                //Hình sự TT pháp nhân 88
                M_Criminal_Retria_Com M_Objecs = new M_Criminal_Retria_Com();
                li_sw = M_Objecs.Criminal_Retrials_Com_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Retrials_Com_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 23)
            {
                //HS Theo thủ tục đặc biệt 23
                M_Criminal_Special M_Objecs = new M_Criminal_Special();
                li_sw = M_Objecs.Criminal_Special_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                //li_sw = M_Objecs.Criminal_Special_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 24)
            {
                //Bị cáo là người chưa TN 24
                M_Youth_Instance M_Objecs = new M_Youth_Instance();
                li_sw = M_Objecs.Youth_Instance_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 25)
            {
                //Kết quả thi hành án hình sự 25
                M_Criminal_Results M_Objecs = new M_Criminal_Results();
                li_sw = M_Objecs.Criminal_Results_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 66)
            {
                //Các bị cáo tòa án cấp sơ thẩm cho hưởng án treo và cải tạo không giam giữ bị kháng cáo, kháng nghị - 1H 66
                M_Probation1H M_Objecs = new M_Probation1H();
                li_sw = M_Objecs.Probation1H_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 67)
            {
                //Các bị cáo tòa án cấp phúc thẩm cho hưởng án treo và cải tạo không giam bị kháng nghị giám đốc thẩm - 1i 67
                M_Probation1I M_Objecs = new M_Probation1I();
                li_sw = M_Objecs.Probation1I_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Probation1I_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 74)
            {
                //Các bị cáo tòa án cấp sơ thẩm áp dụng tình tiết giảm nhẹ trách nhiệm hình sự và quyết định hình phạt nhẹ hơn quy định của bộ luật HS - 1K 74
                M_Probation1K M_Objecs = new M_Probation1K();
                li_sw = M_Objecs.Probation1K_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 69)
            {
                //UTTP về dân sự vào VN - 9A 69
                M_Delegation9A M_Objecs = new M_Delegation9A();
                li_sw = M_Objecs.Delegation9A_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 70)
            {
                //UTTP về dân sự Ra nước ngoài - 9B 70
                M_Delegation9B M_Objecs = new M_Delegation9B();
                li_sw = M_Objecs.Delegation9B_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Delegation9B_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 79)
            {
                //Trường hợp tòa án có vi phạm các quy định về tố tụng hình sự và thi hành án hình sự - 1L 79
                M_Violates1L M_Objecs = new M_Violates1L();
                li_sw = M_Objecs.Violates1L_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Violates1L_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 80)
            {
                //Vụ án về ma túy có liên quan đến việc giám định - 1M 80
                M_Drug_Inspection1M M_Objecs = new M_Drug_Inspection1M();
                li_sw = M_Objecs.Drug_Inspection1M_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Drug_Inspection1M_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 90)
            {
                //Thống kê về việc xử lý vi phạm hành chính thuộc thẩm quyền của tòa án - 9C 90
                M_Violate_9C M_Objecs = new M_Violate_9C();
                li_sw = M_Objecs.Violate_9C_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Violate_9C_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 75)
            {
                //Xét miễn giảm các khoản thu nộp ngân sách nhà nước - 9D 75
                M_Court_Fees9D M_Objecs = new M_Court_Fees9D();
                li_sw = M_Objecs.Fees9D_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Fees9D_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 89)
            {
                //Thống kê bản án, quyết định cung cấp cho sở tư pháp - 9i 89
                M_Of_Justice9i M_Objecs = new M_Of_Justice9i();
                li_sw = M_Objecs.Of_Justice9i_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Of_Justice9i_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 58)
            {
                //Xử lý hành chính tại tòa - 7A 58
                M_Sanctions_Instances7A M_Objecs = new M_Sanctions_Instances7A();
                li_sw = M_Objecs.Sanctions_Instances_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 59)
            {
                //Hoãn, miễn, TĐC - 7B 59
                M_Sanctions_Instances7B M_Objecs = new M_Sanctions_Instances7B();
                li_sw = M_Objecs.Sanctions_Instances7B_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 60)
            {
                //Khiếu nại, kiến nghị, KN - 7C 60
                M_Sanctions_Instances7C M_Objecs = new M_Sanctions_Instances7C();
                li_sw = M_Objecs.Sanctions_Instances7C_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 72)
            {
                //Danh sách các bị cáo tòa án sơ thẩm tuyên bị cáo không phạm tội - 1N 72
                M_Accused_1N M_Objecs = new M_Accused_1N();
                li_sw = M_Objecs.Accused_1N_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 73)
            {
                //Danh sách bị cáo tòa án phúc thẩm tuyên bị cáo không phạm tội - 1O 73
                M_Accused_1O M_Objecs = new M_Accused_1O();
                li_sw = M_Objecs.Accused_1O_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_1O_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 83)
            {
                //Danh sách các trường hợp tòa án cấp PT, GĐT hủy bản án để điều tra lại, sau đó có quyết định đình chỉ điều tra đối với bị can vì không thực hiện hành vi phạm tội - 1Q 83
                M_Accused_1Q M_Objecs = new M_Accused_1Q();
                li_sw = M_Objecs.Accused_1Q_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_1Q_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 81)
            {
                //Danh sách các bị cáo tòa án tuyên có tội nhưng sau đó bị kháng nghị PT, GĐT theo hướng không có tội - 1P 81
                M_Accused_1P M_Objecs = new M_Accused_1P();
                li_sw = M_Objecs.Accused_1P_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_1P_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 85)
            {
                //Danh sách xét xử sơ thẩm các vụ án điểm về tham nhũng, chức vụ - 1R 85
                M_Accused_1R M_Objecs = new M_Accused_1R();
                li_sw = M_Objecs.Accused_1R_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 86)
            {
                //Danh sách các trường hợp tòa án vi phạm thời hạn tạm giam bị cáo trong giai đoạn xét xử - 1S 86
                M_Accused_1S M_Objecs = new M_Accused_1S();
                li_sw = M_Objecs.Accused_1S_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_1S_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 84)
            {
                //Kháng nghị GĐT vụ án đã trả lời không có căn cứ kháng nghị - 8D 84
                M_Accused_8D M_Objecs = new M_Accused_8D();
                li_sw = M_Objecs.Accused_8D_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_8D_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 82)
            {
                //Danh sách giải quyết yêu cầu bồi thường theo luật bồi thường tại TAND - 9H 82
                M_Accused_9H M_Objecs = new M_Accused_9H();
                li_sw = M_Objecs.Accused_9H_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Accused_9H_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            #endregion
            #region Dân sự
            else if (_ID_BAOCAO == 27)
            {
                //Dân sự Sơ thẩm 27
                M_Judge_Civil M_Objecs = new M_Judge_Civil();
                li_sw = M_Objecs.Civil_Instances_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _COURT_EXTID, _ISDONVITRUCTHUOC, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 28)
            {
                //Dân sự Phúc thẩm 28
                M_Civil_Appeals M_Objecs = new M_Civil_Appeals();
                if (_ISDONVITRUCTHUOC == 0)
                    li_sw = M_Objecs.Civil_App_Export(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCASE_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                else
                    li_sw = M_Objecs.Civil_App_Export_Court_ExtID(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCASE_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 29)
            {
                //Dân sự GĐT 29
                M_Civil_Cassation M_Objecs = new M_Civil_Cassation();
                li_sw = M_Objecs.Civil_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Civil_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 30)
            {
                //Dân sự tái thẩm 30
                M_Civil_Retrials M_Objecs = new M_Civil_Retrials();
                li_sw = M_Objecs.Civil_Retrials_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Civil_Retrials_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 31)
            {
                //Dân sự theo thủ tục đặc biệt 31
                M_Civil_Special M_Objecs = new M_Civil_Special();
                li_sw = M_Objecs.Civil_Special_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Civil_Special_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            #endregion
            #region Hôn nhân
            else if (_ID_BAOCAO == 33)
            {
                //Hôn nhân Sơ thẩm 33
                M_Judge_Marriges M_Objecs = new M_Judge_Marriges();
                li_sw = M_Objecs.Marriges_Instances_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 34)
            {
                //Hôn nhân Phúc thẩm 34
                M_Marriges_Appeals M_Objecs = new M_Marriges_Appeals();
                if (_ISDONVITRUCTHUOC == 0)
                    li_sw = M_Objecs.Marriges_Appeals_Export(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCASE_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                else
                    li_sw = M_Objecs.Mar_App_Export_Court_ExtID(_COURT_EXTID, _COURT_NAME, _sCASE_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 35)
            {
                //Hôn nhân GĐT 35
                M_Marriges_Cassation M_Objecs = new M_Marriges_Cassation();
                li_sw = M_Objecs.Marriges_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Mar_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 36)
            {
                //Hôn nhân tái thẩm 36
                M_Marriges_Retrials M_Objecs = new M_Marriges_Retrials();
                li_sw = M_Objecs.Marriges_Retrial_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Mar_Retrial_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            #endregion
            #region Lao động
            else if (_ID_BAOCAO == 43)
            {
                //Lao động Sơ thẩm 43
                M_Judge_Labors M_Objecs = new M_Judge_Labors();
                li_sw = M_Objecs.Labor_Instances_Export(_COURT_ID, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 44)
            {
                //Lao động Phúc thẩm 44
                M_Labor_Appeals M_Objecs = new M_Labor_Appeals();
                if (_ISDONVITRUCTHUOC == 0)
                    li_sw = M_Objecs.Labor_Appeals_Export(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                else
                    li_sw = M_Objecs.labor_App_Export_Court_ExtID(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 46)
            {
                //Lao động GĐT 46
                M_Labor_Cassation M_Objecs = new M_Labor_Cassation();
                li_sw = M_Objecs.Labor_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.labor_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 45)
            {
                //Lao động tái thẩm 45
                M_Labor_Retrials M_Objecs = new M_Labor_Retrials();
                li_sw = M_Objecs.Labor_Retrials_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.labor_Retrials_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            #endregion
            #region Chưa xác định _ID_BAOCAO: 62,63,64,76
            else if (_ID_BAOCAO == 62)
            {
                //Đơn đề nghị GĐT,TT - 8A 62
                String TuNgay = txtNgay.Text.Trim().ToString();
                String DenNgay = txtDenNgay.Text.Trim().ToString();
                LoadReport_8A(_COURT_EXTID, _COURT_NAME,_ISDONVITRUCTHUOC, TuNgay, DenNgay);
                // M_Letters_twcw_8A M_Objecs = new M_Letters_twcw_8A();
                //li_sw = M_Objecs.Letters_twcw_8A_Export(_COURT_EXTID.ToString(), _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _ISDONVITRUCTHUOC.ToString(), TuNgay, DenNgay);
                //li_sw = M_Objecs.Letters_twcw_8A_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Letters_twcw_8A_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 63)
            {
                //Đơn khiếu nại tố cáo - 8B 63
                M_Letters_htcw_8B M_Objecs = new M_Letters_htcw_8B();
                li_sw = M_Objecs.Letters_htcw_8B_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Letters_htcw_8B_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 64)
            {
                //Kiến nghị GĐT,TT BA/QĐ - 8C 64
                M_Letters_htcwtw_8C M_Objecs = new M_Letters_htcwtw_8C();
                li_sw = M_Objecs.Letters_htcwtw_8C_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Letters_htcwtw_8C_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 76)
            {
                //Trưng cầu giám định trong quá trình giải quyết vụ án - 9E 76
                M_Verification9E M_Objecs = new M_Verification9E();
                li_sw = M_Objecs.Verification9E_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Verification9E_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            #endregion
            #region Kinh tế
            else if (_ID_BAOCAO == 38)
            {
                //Kinh tế Sơ thẩm 38
                M_Judge_Economics M_Objecs = new M_Judge_Economics();
                li_sw = M_Objecs.Economics_Instances_Export(_COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 39)
            {
                //Kinh tế Phúc thẩm 39
                M_Economic_Appeals M_Objecs = new M_Economic_Appeals();
                if (_ISDONVITRUCTHUOC == 0)
                    li_sw = M_Objecs.Economics_Appeals_Export(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                else
                    li_sw = M_Objecs.Economics_App_Export_Court_ExtID(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 40)
            {
                //Kinh tế GĐT 40
                M_Economic_Cassation M_Objecs = new M_Economic_Cassation();
                li_sw = M_Objecs.Economics_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Economics_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 41)
            {
                //Kinh tế tái thẩm 41
                M_Economic_Retrials M_Objecs = new M_Economic_Retrials();
                li_sw = M_Objecs.Economics_Retrials_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Economics_Retrials_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 54)
            {
                //Yêu cầu tuyên bố phá sản - 4E 54
                M_Bankruptcys_Instances4E M_Objecs = new M_Bankruptcys_Instances4E();
                li_sw = M_Objecs.Bankruptcys_Instances4E_Export(_COURT_NAME, _sCASE_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 55)
            {
                //Đơn đề nghị, kháng nghị - 4F 55
                M_Bankruptcys_Appeals4F M_Objecs = new M_Bankruptcys_Appeals4F();
                li_sw = M_Objecs.Bankruptcys_Appeals4F_Export(_COURT_ID_Ext, _COURT_NAME, _sCASE_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Bankruptcys_Appeals4F_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCASE_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
            }
            else if (_ID_BAOCAO == 56)
            {
                //PS Theo thủ tục đặc biệt - 4G 56
                M_Bankruptcys_Special4G M_Objecs = new M_Bankruptcys_Special4G();
                li_sw = M_Objecs.Bankruptcys_Special4G_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Bankruptcys_Special4G_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            #endregion
            #region Hành chính
            else if (_ID_BAOCAO == 48)
            {
                //Hành chính Sơ thẩm 48
                M_Judge_Admin M_Objecs = new M_Judge_Admin();
                li_sw = M_Objecs.Admin_Instances_Export(_COURT_ID, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 49)
            {
                //Hành chính Phúc thẩm 49
                M_Admin_Appeals M_Objecs = new M_Admin_Appeals();
                if (_ISDONVITRUCTHUOC == 0)
                    li_sw = M_Objecs.Admin_Appeals_Export(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
                else
                    li_sw = M_Objecs.Admin_App_Export_Court_ExtID(_COURT_ID, _COURT_NAME, _COURT_EXTID, _sCRIMINAL_ID, _ID_BAOCAO, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 50)
            {
                //Hành chính GĐT 50
                M_Admin_Cassation M_Objecs = new M_Admin_Cassation();
                li_sw = M_Objecs.Admin_Cassation_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Admin_Cassation_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 51)
            {
                //Hành chính tái thẩm 51
                M_Admin_Retrials M_Objecs = new M_Admin_Retrials();
                li_sw = M_Objecs.Admin_Retrials_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Admin_Retrials_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 52)
            {
                //HC Theo thủ tục đặc biệt 52
                M_Admin_Special M_Objecs = new M_Admin_Special();
                li_sw = M_Objecs.Admin_Specia_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Admin_Specia_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            #endregion
            #region Chưa xác định _ID_BAOCAO: 77,78,92,93,
            else if (_ID_BAOCAO == 77)
            {
                //Áp dụng thay đổi, hủy bỏ, QĐ áp dụng biện pháp khẩn cấp tạm thời - 9F 77
                M_Emergency9F M_Objecs = new M_Emergency9F();
                li_sw = M_Objecs.Emergency9F_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Emergency9F_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 78)
            {
                //Tòa án có vi phạm các quy định của pháp luật tố tụng dân sự, hành chính - 9G 78
                M_Violate_9G M_Objecs = new M_Violate_9G();
                li_sw = M_Objecs.Violate_9G_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                //li_sw = M_Objecs.Violate_9G_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            else if (_ID_BAOCAO == 92)
            {
                //Đơn khiếu nại - 8B-01 92
                M_Letters_htcw_8B_01 M_Objecs = new M_Letters_htcw_8B_01();
                //if (hi_value_courts_ext.Value == "0")
                if (_ID_BAOCAO > 0)//tạm
                {
                    li_sw = M_Objecs.Letters_htcw_8B_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                }
                else
                {
                    li_sw = M_Objecs.Letters_htcw_8B_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                }
            }
            else if (_ID_BAOCAO == 93)
            {
                //Đơn tố cáo - 8B-02 93
                M_Letters_htcw_8B_02 M_Objecs = new M_Letters_htcw_8B_02();
                //if (hi_value_courts_ext.Value == "0")
                if (_ID_BAOCAO > 0)//tạm
                {
                    li_sw = M_Objecs.Letters_htcw_8B_Export(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                }
                else
                {
                    li_sw = M_Objecs.Letters_htcw_8B_Export_Court_ExtID(_COURT_ID_Ext, _COURT_NAME, _sCRIMINAL_ID, _ID_BAOCAO, _COURT_ID.ToString(), _REPORT_TIME_ID.ToString(), _REPORT_TIME_ID2.ToString());
                }
            }
            #endregion
            string reportText = "";
            foreach (BL.THONGKE.Info.Judge_Report its in li_sw)
            {
                reportText += its.Text_Report;
            }

            return reportText;
        }
        #region "PHAN LOAI BAO CAO THEO TOA"
        //
        //############ PHAN LOAI TOA AN #############################################################################################
        //
        // Load Cấp tòa
        private void Drop_Levels_init()
        {
            string loaitoa = Session["LOAITOA"] + "";
            if (loaitoa == "TOICAO")
            {
                Drop_Levels.Items.Add(new ListItem("Tối cao", "TW"));
            }
            else if (loaitoa == "CAPCAO")
            {
                Drop_Levels.Items.Add(new ListItem("Cấp cao", "CW"));
            }
            else if (loaitoa == "CAPTINH")
            {
                Drop_Levels.Items.Add(new ListItem("Tỉnh/TP", "T"));
            }
            else if (loaitoa == "CAPHUYEN")
            {
                Drop_Levels.Items.Add(new ListItem("Quận/Huyện", "H"));
            }

        }
        // Load Tòa án Login
        private void Drop_courts_init()
        {
            M_Courts M_Objects = new M_Courts();
            List<Courts> items = null;

            Int32 courtId = Int32.Parse(Database.GetUserCourt(Session["Sesion_Manager_ID"].ToString()));
            string courtName = Database.GetUserCourtName(Session["Sesion_Manager_ID"].ToString());
            string loaitoa_selected = Drop_Levels.SelectedValue;

            if (loaitoa_selected == "TW")
            {
                //TW
                items = M_Objects.List_Couts_All_For_Type(loaitoa_selected);
                //trường hợp đặc biệt, map lại tòa án tối cao giữa 2 hệ thông
                foreach (Courts item in items)
                {
                    if (item.ID.ToString() == courtId.ToString()) item.COURT_NAME = courtName;
                }
            }
            else if (loaitoa_selected == "CW")
            {
                //CW
                items = M_Objects.List_Couts_All_For_Type(loaitoa_selected);
            }
            else if (loaitoa_selected == "T")
            {
                //T
                items = M_Objects.List_Couts_All_For_Type(loaitoa_selected);
            }
            else if (loaitoa_selected == "H")
            {
                //H
                items = M_Objects.List_Couts_All_For_Type(loaitoa_selected);
            }
            Drop_courts.DataSource = items;
            Drop_courts.DataBind();

            //Xác định Tòa án cho phép chọn
            foreach (ListItem item in Drop_courts.Items)
            {
                //chọn mặc định tòa đăng nhập
                if (item.Value == courtId.ToString()) item.Selected = true;
                //loại bỏ các tòa khác
                if (item.Value != courtId.ToString()) item.Enabled = false;
            }
        }
        // Load Tòa án trực thuộc Tòa án Login
        private void Drop_courts_ext_init()
        {
            string loaitoa_selected = Drop_Levels.SelectedValue;
            Int32 courtId_selected = Int32.Parse(Drop_courts.SelectedValue);

            M_Courts M_Objects = new M_Courts();
            List<Courts> items = null;

            if (loaitoa_selected == "TW")
            {
                //items = M_Objects.List_Couts_All_For_Type("CW");
                items = M_Objects.List_Couts_All_For_Type("TW");
                // lay ra don vi thuoc Tòa TC: Type= TW và user_Type = 3
            }
            else if (loaitoa_selected == "CW")
            {
                //items = M_Objects.List_Couts_All_For_Type("CW");
                items = M_Objects.Court_List_Id_Not_All_CW(courtId_selected);
            }
            else if (loaitoa_selected == "T")
            {
                //items = M_Objects.Court_List_Id_Not_All_CW(courtId_selected);
                items = M_Objects.Court_List_Id_Not_All(courtId_selected);
            }

            Drop_courts_ext.DataSource = items;
            Drop_courts_ext.DataBind();
            //Drop_courts_ext.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
            //Drop_courts_ext.SelectedIndex = 0;
        }
        // Load Loại án
        private void ddlGroupBaoCao_init()
        {
            //string id_courts = Drop_courts.SelectedValue;
            string id_courts = Drop_courts_ext.SelectedValue;
            M_Functions M_Objects = new M_Functions();
            List<Functions> List_Functions = M_Objects.Function_List_Courts(id_courts);

            int[] arsv = new int[11] { 18, 26,29, 32, 37, 42, 47, 53, 57, 61, 65 };
            //SELECT * FROM TC_FUNCTIONS TC WHERE TC.POSTION=1 and TC.ID_PARENT=0 and TC.ID <>1 ORDER BY TC.ID;

            ddlGroupBaoCao.Items.Clear();
            int vsi = 0;
            foreach (Functions its in List_Functions)
            {//its.ID=57 ap dung cac bien phap xu ly hanh chinh
                if ((its.ID_PARENT == 0) && (its.POSTION == 1) && (its.ID != 1) && (its.ID != 57))
                {
                    for (int i = 0; i <= 10; i++)
                    {
                        if (its.ID == arsv[i])
                        {
                            vsi = Convert.ToInt32(its.ID);
                            break;
                        }
                    }
                    ListItem ites = new ListItem(its.FUNCTION_NAME, vsi.ToString());
                    ddlGroupBaoCao.Items.Add(ites);
                }
            }
        }
        // Load Báo cáo
        private void ddlBaoCao_init()
        {
            ddlBaoCao.Items.Clear();
            string id_courts = Drop_courts_ext.SelectedValue,
                //is_DonViTrucThuoc = "0", 
                LoaiToa = Drop_Levels.SelectedValue + ",";
            //if (Drop_courts_ext.Visible) is_DonViTrucThuoc = "1";
            decimal LoaiAnID = Convert.ToDecimal(ddlGroupBaoCao.SelectedValue);

            M_Functions M_Objects = new M_Functions();
            List<Functions> List_Functions = M_Objects.Function_List_Courts(id_courts);

            foreach (Functions its in List_Functions)
            {//its.ID=57 ap dung cac bien phap xu ly hanh chinh
                if ((its.ID_PARENT == LoaiAnID) && (its.POSTION == 1) && (its.ID != 1) && (its.ID != 57))
                {
                    //if (is_DonViTrucThuoc == "1" && its.DESCRIPTION.Contains(LoaiToa))
                    //{
                    //    ListItem ites = new ListItem(its.FUNCTION_NAME, its.ID.ToString());
                    //    ddlBaoCao.Items.Add(ites);
                    //}
                    //if (is_DonViTrucThuoc == "0")
                    //{
                    //    ListItem ites = new ListItem(its.FUNCTION_NAME, its.ID.ToString());
                    //    ddlBaoCao.Items.Add(ites);
                    //}
                    ListItem ites = new ListItem(its.FUNCTION_NAME, its.ID.ToString());
                    ddlBaoCao.Items.Add(ites);
                }
            }
        }
        protected void ddl_courts_ext_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlGroupBaoCao_init();
            ddlBaoCao_init();
        }
        protected void Drop_Levels_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ẩn tòa án trực thuộc
            //lbToaTrucThuoc.Visible = false;
            //Drop_courts_ext.Visible = false;

            //string loaitoa_login = Database.GetUserType(Session["Sesion_Manager_ID"].ToString());
            //string loaitoa_selected = Drop_Levels.SelectedValue;

            //// hiện tòa án trực thuộc
            //if (loaitoa_login != loaitoa_selected)
            //{
            //    lbToaTrucThuoc.Visible = true;
            //    Drop_courts_ext.Visible = true;
            //}
            //Drop_courts_ext_init();
            //ddlBaoCao_init();
        }
        protected void ddlGroupBaoCao_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBaoCao_init();
            ddlBaoCao_SelectedIndexChanged(null, null);
        }
        //
        //############ KET THUC PHAN LOAI TOA AN #################################################################################################
        //
        #endregion
        #region "VIEW ALL 65 REPORT HTML (NOT USED)"
        //
        //############ BAT DAU VIEW 65 BAO CAO #######################################################################################
        //
        /// <summary>
        /// xuat bao cao ra web
        /// </summary>
        /// <param name="_REPORT_TIME_ID"></param>
        /// <param name="_REPORT_TIME_ID2"></param>
        /// <param name="_COURT_ID"></param>
        /// <param name="_COURT_NAME"></param>
        /// <param name="_ID_BAOCAO"></param>
        /// <param name="_sCRIMINAL_ID"></param>
        /// <param name="_sCASE_ID"></param>
        /// <param name="_COURT_ID_Ext"></param>
        private void ExportReport_Web(int _COURT_ID, string _COURT_NAME, int _COURT_EXTID, int _ISDONVITRUCTHUOC, string _sCASE_ID, int _ID_BAOCAO, string _sCRIMINAL_ID, int _REPORT_TIME_ID, int _REPORT_TIME_ID2)
        {
            string reportText = getContentReport(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);

            //Tạo đường kẻ sắc nét cho bảng
            reportText = reportText.Replace("<table cellpadding=\"0\" cellspacing=\"1\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center;\">", "<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center; border-collapse: collapse;\">");

            //Căn giữa nội dung
            reportText = reportText.Replace("<td style=\"border: 0.1pt solid Black; text-align: left; vertical-align: middle;\">", "<td style=\"border: 0.1pt solid Black; text-align: center; vertical-align: middle;\">");

            //Loại bỏ các ký tự đặc biệt
            reportText = reportText.Replace("\n", "");
            reportText = reportText.Replace("\t", "");

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Key:" + DateTime.Now.ToLongTimeString(), "showPopupReport('" + reportText + "')", true);
        }
        /// <summary>
        /// xuat toan bo bao cao ra trinh duyet web
        /// </summary>
        /// <param name="_REPORT_TIME_ID"></param>
        /// <param name="_REPORT_TIME_ID2"></param>
        /// <param name="_COURT_ID"></param>
        /// <param name="_COURT_NAME"></param>
        /// <param name="_ID_BAOCAO"></param>
        /// <param name="_sCRIMINAL_ID"></param>
        /// <param name="_sCASE_ID"></param>
        /// <param name="_COURT_ID_Ext"></param>
        private void ExportReportAll_Html(int _COURT_ID, string _COURT_NAME, int _COURT_EXTID, int _ISDONVITRUCTHUOC, string _sCASE_ID, int _ID_BAOCAO, string _sCRIMINAL_ID, int _REPORT_TIME_ID, int _REPORT_TIME_ID2)
        {
            string reportText = "";
            string reportInfo = "";
            reportInfo = reportInfo + "19:Hình sự Sơ thẩm cá nhân 19:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_ST_CN:PKG_JUDGE_CRIMINAL.TC_JUDGE_CRIMINAL_EXP;";
            reportInfo = reportInfo + "20:Hình sự Phúc thẩm cá nhân 20:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_PT_CN:PKG_CRIMINAL_APPEAL.TC_CRIMINAL_APP_EXP<br/>PKG_CRIMINAL_APPEAL.TC_CRIMINAL_APP_COURT_EXP;";
            reportInfo = reportInfo + "21:Hình sự GĐT cá nhân 21:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_GDT_CN:PKG_CRIMINAL_CASSATION.TC_CRIMINAL_CASS_EXP<br/>PKG_CRIMINAL_CASSATION.TC_CRIMINAL_CASS_COURT_EXP;";
            reportInfo = reportInfo + "22:Hình sự TT cá nhân 22:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_TT_CN:PKG_CRIMINAL_RETRIALS.TC_CRIMINAL_RETRI_EXP<br/>PKG_CRIMINAL_RETRIALS.TC_CRIMINAL_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "23:HS Theo thủ tục đặc biệt 23:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_TTDB:PKG_CRIMINAL_SPECIAL.TC_CRIMINAL_SPEC_EXP<br/>PKG_CRIMINAL_SPECIAL.TC_CRIMINAL_SPEC_COURT_EXP;";
            reportInfo = reportInfo + "24:Bị cáo là người chưa TN 24:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.BC_CTN:PKG_YOUTH_INSTANCE.TC_YOUTH_INS_EXP;";
            reportInfo = reportInfo + "25:Kết quả thi hành án hình sự 25:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.KQ_AHS:PKG_CRIMINAL_RESULTS.TC_CRIMINAL_RESULTS_EXP;";
            reportInfo = reportInfo + "66:Các bị cáo tòa án cấp sơ thẩm cho hưởng án treo và cải tạo không giam giữ bị kháng cáo, kháng nghị - 1H 66:Hình sự :PKG_TABLE_TC_SANCTIONS_INST.MAU_1H:PKG_PROBATION1H.TC_PROB_EXP;";
            reportInfo = reportInfo + "67:Các bị cáo tòa án cấp phúc thẩm cho hưởng án treo và cải tạo không giam bị kháng nghị giám đốc thẩm - 1i 67:Hình sự :PKG_TABLE_TC_SANCTIONS_INST.MAU_1I:PKG_PROBATION1I.TC_PROB1I_EXP<br/>PKG_PROBATION1I.TC_PROB1I_COURT_EXP;";
            reportInfo = reportInfo + "68:Hình sự Sơ thẩm pháp nhân 68:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_ST_PN:PKG_CRIMINAL_INSTAN_COM.TC_JUDGE_CRIMI_COM_EXP;";
            reportInfo = reportInfo + "71:Hình sự Phúc thẩm pháp nhân 71:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_PT_PN:PKG_CRIMINAL_APPEA_COM.TC_CRIMINAL_APP_COM_EXP<br/>PKG_CRIMINAL_APPEA_COM.TC_CRIMINAL_APP_COM_COURT_EXP;";
            reportInfo = reportInfo + "72:Danh sách các bị cáo tòa án sơ thẩm tuyên bị cáo không phạm tội - 1N 72:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1N:PKG_ACCUSED_1N.TC_ACC1N_EXP;";
            reportInfo = reportInfo + "73:Danh sách bị cáo tòa án phúc thẩm tuyên bị cáo không phạm tội - 1O 73:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1O:PKG_ACCUSED_1O.TC_ACC1O_EXP<br/>PKG_ACCUSED_1O.TC_ACC1O_COURT_EXP;";
            reportInfo = reportInfo + "74:Các bị cáo tòa án cấp sơ thẩm áp dụng tình tiết giảm nhẹ trách nhiệm hình sự và quyết định hình phạt nhẹ hơn quy định của bộ luật HS - 1K 74:Hình sự :PKG_TABLE_TC_SANCTIONS_INST.MAU_1K:PKG_PROBATION1K.TC_PROB1K_EXP;";
            reportInfo = reportInfo + "79:Trường hợp tòa án có vi phạm các quy định về tố tụng hình sự và thi hành án hình sự - 1L 79:Hình sự :PKG_TABLE_TC_SANCTIONS_INST.MAU_1L:PKG_VIOLATES1L.TC_VIOL_EXP<br/>PKG_VIOLATES1L.TC_VIOL_COURT_EXP;";
            reportInfo = reportInfo + "80:Vụ án về ma túy có liên quan đến việc giám định - 1M 80:Hình sự :PKG_TABLE_TC_SANCTIONS_INST.MAU_1M:PKG_DRUG_INSPECTION1M.TC_DRUG_EXP<br/>PKG_DRUG_INSPECTION1M.TC_DRUG_COURT_EXP;";
            reportInfo = reportInfo + "81:Danh sách các bị cáo tòa án tuyên có tội nhưng sau đó bị kháng nghị PT, GĐT theo hướng không có tội - 1P 81:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1P:PKG_ACCUSED_1P.TC_ACC1P_EXP<br/>PKG_ACCUSED_1P.TC_ACC1P_COURT_EXP;";
            reportInfo = reportInfo + "83:Danh sách các trường hợp tòa án cấp PT, GĐT hủy bản án để điều tra lại, sau đó có quyết định đình chỉ điều tra đối với bị can vì không thực hiện hành vi phạm tội - 1Q 83:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1Q:PKG_ACCUSED_1Q.TC_ACC1Q_EXP<br/>PKG_ACCUSED_1Q.TC_ACC1Q_COURT_EXP;";
            reportInfo = reportInfo + "85:Danh sách xét xử sơ thẩm các vụ án điểm về tham nhũng, chức vụ - 1R 85:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1R:PKG_ACCUSED_1R.TC_ACC1R_EXP;";
            reportInfo = reportInfo + "86:Danh sách các trường hợp tòa án vi phạm thời hạn tạm giam bị cáo trong giai đoạn xét xử - 1S 86:Hình sự :PKG_TABLE_TC_JUDGE_ACCUSED.MAU_1S:PKG_ACCUSED_1S.TC_ACC1S_EXP<br/>PKG_ACCUSED_1S.TC_ACC1S_COURT_EXP;";
            reportInfo = reportInfo + "87:Hình sự GĐT pháp nhân 87:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_GDT_PN:PKG_CRIMINAL_CASSATI_COM.TC_CRIMINAL_CASS_COM_EXP<br/>PKG_CRIMINAL_CASSATI_COM.TC_CRIMINAL_CASS_COM_COURT_EXP;";
            reportInfo = reportInfo + "88:Hình sự TT pháp nhân 88:Hình sự :PKG_TABLE_TC_JUDGE_CRIMINAL.HS_TT_PN:PKG_CRIMINAL_RETRIA_COM.TC_CRIMINAL_RET_COM_EXP<br/>PKG_CRIMINAL_RETRIA_COM.TC_CRIMINAL_RET_COM_COURT_EXP;";
            reportInfo = reportInfo + "27:Dân sự Sơ thẩm 27:Dân sự :PKG_TABLE_TC_JUDGE_CIVIL.DS_ST:PKG_JUDGE_CIVIL.TC_CIVIL_INSTANCE_EXP;";
            reportInfo = reportInfo + "28:Dân sự Phúc thẩm 28:Dân sự :PKG_TABLE_TC_JUDGE_CIVIL.DS_PT:PKG_CIVIL_APPEALS.TC_CIVIL_APP_EXP<br/>PKG_CIVIL_APPEALS.TC_CIVIL_APP_COURT_EXP;";
            reportInfo = reportInfo + "29:Dân sự GĐT 29:Dân sự :PKG_TABLE_TC_JUDGE_CIVIL.DS_GDT:PKG_CIVIL_CASSATION.TC_CIV_CASS_EXP<br/>PKG_CIVIL_CASSATION.TC_CIV_CASS_COURT_EXP;";
            reportInfo = reportInfo + "30:Dân sự tái thẩm 30:Dân sự :PKG_TABLE_TC_JUDGE_CIVIL.DS_TT:PKG_CIVIL_RETRIALS.TC_CIV_RETRI_EXP<br/>PKG_CIVIL_RETRIALS.TC_CIV_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "31:Dân sự theo thủ tục đặc biệt 31:Dân sự :PKG_TABLE_TC_JUDGE_CIVIL.DS_TTDB:PKG_CIVIL_SPECIAL.TC_CIV_SPEC_EXP<br/>PKG_CIVIL_SPECIAL.TC_CIV_SPEC_COURT_EXP;";
            reportInfo = reportInfo + "33:Hôn nhân Sơ thẩm 33:Hôn nhân :PKG_TABLE_TC_JUDGE_MARRIGES.HN_ST:PKG_JUDGE_MARRIGES.TC_MAR_EXP;";
            reportInfo = reportInfo + "34:Hôn nhân Phúc thẩm 34:Hôn nhân :PKG_TABLE_TC_JUDGE_MARRIGES.HN_PT:PKG_MARRIGES_APPEAL.TC_MARRIGES_APP_EXP<br/>PKG_MARRIGES_APPEAL.TC_MARRIGES_APP_COURT_EXP;";
            reportInfo = reportInfo + "35:Hôn nhân GĐT 35:Hôn nhân :PKG_TABLE_TC_JUDGE_MARRIGES.HN_GDT:PKG_MARRIGES_CASSATION.TC_MARRIGES_CASS_EXP<br/>PKG_MARRIGES_CASSATION.TC_MARRIGES_CASS_COURT_EXP;";
            reportInfo = reportInfo + "36:Hôn nhân tái thẩm 36:Hôn nhân :PKG_TABLE_TC_JUDGE_MARRIGES.HN_TT:PKG_MARRIGES_RETRIALS.TC_MARRIGES_RETRI_EXP<br/>PKG_MARRIGES_RETRIALS.TC_MARRIGES_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "38:Kinh tế Sơ thẩm 38:Kinh tế :PKG_TABLE_TC_JUDGE_ECONOMIC.KT_ST:PKG_JUDGE_ECONOMIC.TC_ECONO_INSTANCES_EXP;";
            reportInfo = reportInfo + "39:Kinh tế Phúc thẩm 39:Kinh tế :PKG_TABLE_TC_JUDGE_ECONOMIC.KT_PT:PKG_ECONOMIC_APPEAL.TC_ECONO_APP_EXP<br/>PKG_ECONOMIC_APPEAL.TC_ECONO_APP_COURT_EXP;";
            reportInfo = reportInfo + "40:Kinh tế GĐT 40:Kinh tế :PKG_TABLE_TC_JUDGE_ECONOMIC.KT_GDT:PKG_ECONOMIC_CASSATION.TC_ECONO_CASS_EXP<br/>PKG_ECONOMIC_CASSATION.TC_ECONO_CASS_COURT_EXP;";
            reportInfo = reportInfo + "41:Kinh tế tái thẩm 41:Kinh tế :PKG_TABLE_TC_JUDGE_ECONOMIC.KT_TT:PKG_ECONOMIC_RETRIALS.TC_ECONO_RETRI_EXP<br/>PKG_ECONOMIC_RETRIALS.TC_ECONO_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "43:Lao động Sơ thẩm 43:Lao động :PKG_TABLE_TC_JUDGE_LABOR.LD_ST:PKG_JUDGE_LABOR.TC_LABOR_INSTANCES_EXP;";
            reportInfo = reportInfo + "44:Lao động Phúc thẩm 44:Lao động :PKG_TABLE_TC_JUDGE_LABOR.LD_PT:PKG_LABOR_APPEAL.TC_LABOR_APP_EXP<br/>PKG_LABOR_APPEAL.TC_LABOR_APP_COURT_EXP;";
            reportInfo = reportInfo + "45:Lao động tái thẩm 45:Lao động :PKG_TABLE_TC_JUDGE_LABOR.LD_TT:PKG_LABOR_RETRIALS.TC_LABOR_RETRI_EXP<br/>PKG_LABOR_RETRIALS.TC_LABOR_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "46:Lao động GĐT 46:Lao động :PKG_TABLE_TC_JUDGE_LABOR.LD_GDT:PKG_LABOR_CASSATION.TC_LABOR_CASS_EXP<br/>PKG_LABOR_CASSATION.TC_LABOR_CASS_COURT_EXP;";
            reportInfo = reportInfo + "48:Hành chính Sơ thẩm 48:Hành chính :PKG_TABLE_TC_JUDGE_ADMIN.HC_ST:PKG_JUDGE_ADMIN.TC_ADMIN_INSTANCES_EXP;";
            reportInfo = reportInfo + "49:Hành chính Phúc thẩm 49:Hành chính :PKG_TABLE_TC_JUDGE_ADMIN.HC_PT:PKG_ADMIN_APPEALS.TC_ADMIN_APP_EXP<br/>PKG_ADMIN_APPEALS.TC_ADMIN_APP_COURT_EXP;";
            reportInfo = reportInfo + "50:Hành chính GĐT 50:Hành chính :PKG_TABLE_TC_JUDGE_ADMIN.HC_GDT:PKG_ADMIN_CASSATION.TC_ADMIN_CASS_EXP<br/>PKG_ADMIN_CASSATION.TC_ADMIN_CASS_COURT_EXP;";
            reportInfo = reportInfo + "51:Hành chính tái thẩm 51:Hành chính :PKG_TABLE_TC_JUDGE_ADMIN.HC_TT:PKG_ADMIN_RETRIALS.TC_ADMIN_RETRI_EXP<br/>PKG_ADMIN_RETRIALS.TC_ADMIN_RETRI_COURT_EXP;";
            reportInfo = reportInfo + "52:HC Theo thủ tục đặc biệt 52:Hành chính :PKG_TABLE_TC_JUDGE_ADMIN.HC_TTDB:PKG_ADMIN_SPECIAL.TC_ADMIN_SPEC_EXP<br/>PKG_ADMIN_SPECIAL.TC_ADMIN_SPEC_COURT_EXP;";
            reportInfo = reportInfo + "54:Yêu cầu tuyên bố phá sản - 4E 54:Tuyên bố phá sản:PKG_TABLE_TC_JUDGE_ECONOMIC.MAU_4E:PKG_BANKRUPTCYS_INSTANCES4E.TC_BANK_INSTANCES_EXP;";
            reportInfo = reportInfo + "55:Đơn đề nghị, kháng nghị - 4F 55:Tuyên bố phá sản:PKG_TABLE_TC_JUDGE_ECONOMIC.MAU_4F:PKG_BANKRUPTCYS_APPEALS4F.TC_BANK_APP_EXP<br/>PKG_BANKRUPTCYS_APPEALS4F.TC_BANK_APP_COURT_EXP;";
            reportInfo = reportInfo + "56:PS Theo thủ tục đặc biệt - 4G 56:Tuyên bố phá sản:PKG_TABLE_TC_JUDGE_ECONOMIC.MAU_4G:PKG_BANKRUPTCYS_SPECIAL4G.TC_BANK_SPEC_EXP<br/>PKG_BANKRUPTCYS_SPECIAL4G.TC_BANK_SPEC_COURT_EXP;";
            reportInfo = reportInfo + "58:Xử lý hành chính tại tòa - 7A 58:Áp dụng các biện pháp xử lý hành chính:PKG_TABLE_TC_SANCTIONS_INST.MAU_7A:PKG_SANCTIONS_INSTANCES7A.TC_SANCT_INSTANCES_EXP;";
            reportInfo = reportInfo + "59:Hoãn, miễn, TĐC - 7B 59:Áp dụng các biện pháp xử lý hành chính:PKG_TABLE_TC_SANCTIONS_INST.MAU_7B:PKG_SANCTIONS_INSTANCES7B.TC_SANCT7B_EXP;";
            reportInfo = reportInfo + "60:Khiếu nại, kiến nghị, KN - 7C 60:Áp dụng các biện pháp xử lý hành chính:PKG_TABLE_TC_SANCTIONS_INST.MAU_7C:PKG_SANCTIONS_INSTANCES7C.TC_SANCT7C_EXP;";
            reportInfo = reportInfo + "62:Đơn đề nghị GĐT,TT - 8A 62:Đơn tư pháp:PKG_TABLE_TC_JUDGE_LABOR.MAU_8A:PKG_LETTERS_TWCW_8A.TC_LETTERS_8A_EXP<br/>PKG_LETTERS_TWCW_8A.TC_LETTERS_8A_COURT_EXP;";
            reportInfo = reportInfo + "63:Đơn khiếu nại tố cáo - 8B 63:Đơn tư pháp:PKG_TABLE_TC_JUDGE_LABOR.MAU_8B:PKG_LETTERS_HTCW_8B.TC_LETTERS_8B_EXP<br/>PKG_LETTERS_HTCW_8B.TC_LETTERS_8B_COURT_EXP;";
            reportInfo = reportInfo + "64:Kiến nghị GĐT,TT BA/QĐ - 8C 64:Đơn tư pháp:PKG_TABLE_TC_JUDGE_LABOR.MAU_8C:PKG_LETTERS_HTCWTW_8C.TC_LETTERS_8C_EXP<br/>PKG_LETTERS_HTCWTW_8C.TC_LETTERS_8C_COURT_EXP;";
            reportInfo = reportInfo + "84:Kháng nghị GĐT vụ án đã trả lời không có căn cứ kháng nghị - 8D 84:Đơn tư pháp:PKG_TABLE_TC_JUDGE_ACCUSED.MAU_8D:PKG_ACCUSED_8D.TC_ACC8D_EXP<br/>PKG_ACCUSED_8D.TC_ACC8D_COURT_EXP;";
            reportInfo = reportInfo + "69:UTTP về dân sự vào VN - 9A 69:Mẫu thống kê khác:PKG_TABLE_TC_SANCTIONS_INST.MAU_9A:PKG_DELEGATION9A.TC_DELEG9A_EXP;";
            reportInfo = reportInfo + "70:UTTP về dân sự Ra nước ngoài - 9B 70:Mẫu thống kê khác:PKG_TABLE_TC_SANCTIONS_INST.MAU_9B:PKG_DELEGATION9B.TC_DELEG9B_EXP<br/>PKG_DELEGATION9B.TC_DELEG9B_COURT_EXP;";
            reportInfo = reportInfo + "75:Xét miễn giảm các khoản thu nộp ngân sách nhà nước - 9D 75:Mẫu thống kê khác:PKG_TABLE_TC_SANCTIONS_INST.MAU_9D:PKG_COURT_FEES9D.TC_FEES9D_EXP<br/>PKG_COURT_FEES9D.TC_FEES9D_COURT_EXP;";
            reportInfo = reportInfo + "76:Trưng cầu giám định trong quá trình giải quyết vụ án - 9E 76:Mẫu thống kê khác:PKG_TABLE_TC_JUDGE_LABOR.MAU_9E:PKG_VERIFICATION9E.TC_VERIF9E_EXP<br/>PKG_VERIFICATION9E.TC_VERIF9E_COURT_EXP;";
            reportInfo = reportInfo + "77:Áp dụng thay đổi, hủy bỏ, QĐ áp dụng biện pháp khẩn cấp tạm thời - 9F 77:Mẫu thống kê khác:PKG_TABLE_TC_JUDGE_ADMIN.MAU_9F:PKG_EMERGENCY9F.TC_EMER9F_EXP<br/>PKG_EMERGENCY9F.TC_EMER9F_COURT_EXP;";
            reportInfo = reportInfo + "78:Tòa án có vi phạm các quy định của pháp luật tố tụng dân sự, hành chính - 9G 78:Mẫu thống kê khác:PKG_TABLE_TC_JUDGE_ADMIN.MAU_9G:PKG_VIOLATES9G.TC_VIOL9G_EXP<br/>PKG_VIOLATES9G.TC_VIOL9G_COURT_EXP;";
            reportInfo = reportInfo + "82:Danh sách giải quyết yêu cầu bồi thường theo luật bồi thường tại TAND - 9H 82:Mẫu thống kê khác:PKG_TABLE_TC_JUDGE_ACCUSED.MAU_9H:PKG_ACCUSED_9H.TC_ACC9H_EXP<br/>PKG_ACCUSED_9H.TC_ACC9H_COURT_EXP;";
            reportInfo = reportInfo + "89:Thống kê bản án, quyết định cung cấp cho sở tư pháp - 9i 89:Mẫu thống kê khác:PKG_TABLE_TC_SANCTIONS_INST.MAU_9I:PKG_OF_JUSTICE9I.TC_JUSTICE9I_EXP<br/>PKG_OF_JUSTICE9I.TC_JUSTICE9I_COURT_EXP;";
            reportInfo = reportInfo + "90:Thống kê về việc xử lý vi phạm hành chính thuộc thẩm quyền của tòa án - 9C 90:Mẫu thống kê khác:PKG_TABLE_TC_SANCTIONS_INST.MAU_9C:PKG_VIOLATES9C.TC_VIOL9C_EXP<br/>PKG_VIOLATES9C.TC_VIOL9C_COURT_EXP;";
            reportInfo = reportInfo + "92:Đơn khiếu nại - 8B-01 92:Đơn tư pháp:PKG_TABLE_TC_JUDGE_LABOR.MAU_8B01:PKG_LETTERS_8B_01.TC_LETTERS_8B_EXP<br/>PKG_LETTERS_8B_01.TC_LETTERS_8B_COURT_EXP;";
            reportInfo = reportInfo + "93:Đơn tố cáo - 8B-02 93:Đơn tư pháp:PKG_TABLE_TC_JUDGE_LABOR.MAU_8B02:PKG_LETTERS_8B_02.TC_LETTERS_8B_EXP<br/>PKG_LETTERS_8B_02.TC_LETTERS_8B_COURT_EXP;";

            string[] arrReportInfo = reportInfo.Split(';');

            //sắp xếp lại theo thứ tự id báo cáo
            for (int i = 0; i < arrReportInfo.Length - 1; i++)
            {
                if (arrReportInfo[i] == "") continue;
                string[] arrTmp = arrReportInfo[i].Split(':');
                int minId = int.Parse(arrTmp[0]);
                int pos = i;
                //tìm phần tử nhỏ nhất
                for (int j = i + 1; j < arrReportInfo.Length; j++)
                {
                    if (arrReportInfo[j] == "") continue;
                    string[] arrTmp2 = arrReportInfo[j].Split(':');
                    int id = int.Parse(arrTmp2[0]);
                    if (minId >= id)
                    {
                        minId = id;
                        pos = j;
                    }
                }
                //hoán đồi id nhỏ nhất lên đầu
                if (pos > i)
                {
                    string tmpInfo = arrReportInfo[i];
                    arrReportInfo[i] = arrReportInfo[pos];
                    arrReportInfo[pos] = tmpInfo;
                }
            }

            for (int i = 0; i < arrReportInfo.Length; i++)
            {
                if (arrReportInfo[i] == "") continue;
                string[] arrInfo = arrReportInfo[i].Split(':');
                string id = arrInfo[0];
                _ID_BAOCAO = int.Parse(id);

                reportText += "<table width='100%'>";
                reportText += "	<tr>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "	</tr>";
                reportText += "	<tr>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "	</tr>";
                reportText += "	<tr style='text-align: left; background-color:#dddddd;'>";
                reportText += "		<th colspan='2'>";
                reportText += "			<b>" + arrInfo[0].Trim() + ". " + arrInfo[1].Trim() + "</b>";
                reportText += "		</th>";
                reportText += "	</tr>";
                reportText += "	<tr style='background-color:#dddddd;'>";
                reportText += "		<th style='text-align: right;'>Nhóm báo cáo: </th>";
                reportText += "		<th style='text-align: left;'>";
                reportText += "			<b> " + arrInfo[2].Trim() + "</b>";
                reportText += "		</th>";
                reportText += "	</tr>";
                reportText += "	<tr style='background-color:#dddddd;'>";
                reportText += "		<th style='text-align: right;'>Package đầu vào: </th>";
                reportText += "		<th style='text-align: left;'>";
                reportText += "			<b> " + arrInfo[3].Trim() + "</b>";
                reportText += "		</th>";
                reportText += "	</tr>";
                reportText += "	<tr style='background-color:#dddddd;'>";
                reportText += "		<th style='text-align: right;'>Package đầu ra: </th>";
                reportText += "		<th style='text-align: left;'>";
                reportText += "			<b> " + arrInfo[4].Trim() + "</b>";
                reportText += "		</th>";
                reportText += "	</tr>";
                reportText += "	<tr>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "		<th>&nbsp;</th>";
                reportText += "	</tr>";
                reportText += "</table>";

                reportText += getContentReport(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            reportText = reportText.Replace("<table cellpadding=\"0\" cellspacing=\"1\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center;\">", "<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center; border-collapse: collapse;\">");

            //-------------------Export---------------------------
            Literal Table_Str_Totals = new Literal();
            Table_Str_Totals.Text = reportText;
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ToaAn_65MauBaoCao.htm");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/HTML";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        /// <summary>
        /// xuat toan bo bao cao ra trinh duyet web
        /// </summary>
        /// <param name="_REPORT_TIME_ID"></param>
        /// <param name="_REPORT_TIME_ID2"></param>
        /// <param name="_COURT_ID"></param>
        /// <param name="_COURT_NAME"></param>
        /// <param name="_ID_BAOCAO"></param>
        /// <param name="_sCRIMINAL_ID"></param>
        /// <param name="_sCASE_ID"></param>
        /// <param name="_COURT_ID_Ext"></param>
        private void ExportReportAll_Html_old(int _COURT_ID, string _COURT_NAME, int _COURT_EXTID, int _ISDONVITRUCTHUOC, string _sCASE_ID, int _ID_BAOCAO, string _sCRIMINAL_ID, int _REPORT_TIME_ID, int _REPORT_TIME_ID2)
        {
            string reportText = "";
            string reportInfo = "PKG_TABLE_TC_JUDGE_CRIMINAL:HS_ST_CN:Hình sự Sơ thẩm cá nhân 19;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_PT_CN:Hình sự Phúc thẩm cá nhân 20;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_GDT_CN:Hình sự GĐT cá nhân 21;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_TT_CN:Hình sự TT cá nhân 22;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_TTDB:HS Theo thủ tục đặc biệt 23;PKG_TABLE_TC_JUDGE_CRIMINAL:BC_CTN:Bị cáo là người chưa TN 24;PKG_TABLE_TC_JUDGE_CRIMINAL:KQ_AHS:Kết quả thi hành án hình sự 25;PKG_TABLE_TC_SANCTIONS_INST:MAU_1H:Các bị cáo tòa án cấp sơ thẩm cho hưởng án treo và cải tạo không giam giữ bị kháng cáo, kháng nghị - 1H 66;PKG_TABLE_TC_SANCTIONS_INST:MAU_1I:Các bị cáo tòa án cấp phúc thẩm cho hưởng án treo và cải tạo không giam bị kháng nghị giám đốc thẩm - 1i 67;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_ST_PN:Hình sự Sơ thẩm pháp nhân 68;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_PT_PN:Hình sự Phúc thẩm pháp nhân 71;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1N:Danh sách các bị cáo tòa án sơ thẩm tuyên bị cáo không phạm tội - 1N 72;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1O:Danh sách bị cáo tòa án phúc thẩm tuyên bị cáo không phạm tội - 1O 73;PKG_TABLE_TC_SANCTIONS_INST:MAU_1K:Các bị cáo tòa án cấp sơ thẩm áp dụng tình tiết giảm nhẹ trách nhiệm hình sự và quyết định hình phạt nhẹ hơn quy định của bộ luật HS - 1K 74;PKG_TABLE_TC_SANCTIONS_INST:MAU_1L:Trường hợp tòa án có vi phạm các quy định về tố tụng hình sự và thi hành án hình sự - 1L 79;PKG_TABLE_TC_SANCTIONS_INST:MAU_1M:Vụ án về ma túy có liên quan đến việc giám định - 1M 80;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1P:Danh sách các bị cáo tòa án tuyên có tội nhưng sau đó bị kháng nghị PT, GĐT theo hướng không có tội - 1P 81;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1Q:Danh sách các trường hợp tòa án cấp PT, GĐT hủy bản án để điều tra lại, sau đó có quyết định đình chỉ điều tra đối với bị can vì không thực hiện hành vi phạm tội - 1Q 83;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1R:Danh sách xét xử sơ thẩm các vụ án điểm về tham nhũng, chức vụ - 1R 85;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_1S:Danh sách các trường hợp tòa án vi phạm thời hạn tạm giam bị cáo trong giai đoạn xét xử - 1S 86;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_GDT_PN:Hình sự GĐT pháp nhân 87;PKG_TABLE_TC_JUDGE_CRIMINAL:HS_TT_PN:Hình sự TT pháp nhân 88;PKG_TABLE_TC_JUDGE_CIVIL:DS_ST:Dân sự Sơ thẩm 27;PKG_TABLE_TC_JUDGE_CIVIL:DS_PT:Dân sự Phúc thẩm 28;PKG_TABLE_TC_JUDGE_CIVIL:DS_GDT:Dân sự GĐT 29;PKG_TABLE_TC_JUDGE_CIVIL:DS_TT:Dân sự tái thẩm 30;PKG_TABLE_TC_JUDGE_CIVIL:DS_TTDB:Dân sự theo thủ tục đặc biệt 31;PKG_TABLE_TC_JUDGE_MARRIGES:HN_ST:Hôn nhân Sơ thẩm 33;PKG_TABLE_TC_JUDGE_MARRIGES:HN_PT:Hôn nhân Phúc thẩm 34;PKG_TABLE_TC_JUDGE_MARRIGES:HN_GDT:Hôn nhân GĐT 35;PKG_TABLE_TC_JUDGE_MARRIGES:HN_TT:Hôn nhân tái thẩm 36;PKG_TABLE_TC_JUDGE_ECONOMIC:KT_ST:Kinh tế Sơ thẩm 38;PKG_TABLE_TC_JUDGE_ECONOMIC:KT_PT:Kinh tế Phúc thẩm 39;PKG_TABLE_TC_JUDGE_ECONOMIC:KT_GDT:Kinh tế GĐT 40;PKG_TABLE_TC_JUDGE_ECONOMIC:KT_TT:Kinh tế tái thẩm 41;PKG_TABLE_TC_JUDGE_LABOR:LD_ST:Lao động Sơ thẩm 43;PKG_TABLE_TC_JUDGE_LABOR:LD_PT:Lao động Phúc thẩm 44;PKG_TABLE_TC_JUDGE_LABOR:LD_TT:Lao động tái thẩm 45;PKG_TABLE_TC_JUDGE_LABOR:LD_GDT:Lao động GĐT 46;PKG_TABLE_TC_JUDGE_ADMIN:HC_ST:Hành chính Sơ thẩm 48;PKG_TABLE_TC_JUDGE_ADMIN:HC_PT:Hành chính Phúc thẩm 49;PKG_TABLE_TC_JUDGE_ADMIN:HC_GDT:Hành chính GĐT 50;PKG_TABLE_TC_JUDGE_ADMIN:HC_TT:Hành chính tái thẩm 51;PKG_TABLE_TC_JUDGE_ADMIN:HC_TTDB:HC Theo thủ tục đặc biệt 52;PKG_TABLE_TC_JUDGE_ECONOMIC:MAU_4E:Yêu cầu tuyên bố phá sản - 4E 54;PKG_TABLE_TC_JUDGE_ECONOMIC:MAU_4F:Đơn đề nghị, kháng nghị - 4F 55;PKG_TABLE_TC_JUDGE_ECONOMIC:MAU_4G:PS Theo thủ tục đặc biệt - 4G 56;PKG_TABLE_TC_SANCTIONS_INST:MAU_7A:Xử lý hành chính tại tòa - 7A 58;PKG_TABLE_TC_SANCTIONS_INST:MAU_7B:Hoãn, miễn, TĐC - 7B 59;PKG_TABLE_TC_SANCTIONS_INST:MAU_7C:Khiếu nại, kiến nghị, KN - 7C 60;PKG_TABLE_TC_JUDGE_LABOR:MAU_8A:Đơn đề nghị GĐT,TT - 8A 62;PKG_TABLE_TC_JUDGE_LABOR:MAU_8B:Đơn khiếu nại tố cáo - 8B 63;PKG_TABLE_TC_JUDGE_LABOR:MAU_8C:Kiến nghị GĐT,TT BA/QĐ - 8C 64;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_8D:Kháng nghị GĐT vụ án đã trả lời không có căn cứ kháng nghị - 8D 84;PKG_TABLE_TC_SANCTIONS_INST:MAU_9A:UTTP về dân sự vào VN - 9A 69;PKG_TABLE_TC_SANCTIONS_INST:MAU_9B:UTTP về dân sự Ra nước ngoài - 9B 70;PKG_TABLE_TC_SANCTIONS_INST:MAU_9D:Xét miễn giảm các khoản thu nộp ngân sách nhà nước - 9D 75;PKG_TABLE_TC_JUDGE_LABOR:MAU_9E:Trưng cầu giám định trong quá trình giải quyết vụ án - 9E 76;PKG_TABLE_TC_JUDGE_ADMIN:MAU_9F:Áp dụng thay đổi, hủy bỏ, QĐ áp dụng biện pháp khẩn cấp tạm thời - 9F 77;PKG_TABLE_TC_JUDGE_ADMIN:MAU_9G:Tòa án có vi phạm các quy định của pháp luật tố tụng dân sự, hành chính - 9G 78;PKG_TABLE_TC_JUDGE_ACCUSED:MAU_9H:Danh sách giải quyết yêu cầu bồi thường theo luật bồi thường tại TAND - 9H 82;PKG_TABLE_TC_SANCTIONS_INST:MAU_9I:Thống kê bản án, quyết định cung cấp cho sở tư pháp - 9i 89;PKG_TABLE_TC_SANCTIONS_INST:MAU_9C:Thống kê về việc xử lý vi phạm hành chính thuộc thẩm quyền của tòa án - 9C 90;PKG_TABLE_TC_JUDGE_LABOR:MAU_8B01:Đơn khiếu nại - 8B-01 92;PKG_TABLE_TC_JUDGE_LABOR:MAU_8B02:Đơn tố cáo - 8B-02 93";
            string reportId = "19,20,21,22,23,24,25,66,67,68,71,72,73,74,79,80,81,83,85,86,87,88,27,28,29,30,31,33,34,35,36,38,39,40,41,43,44,45,46,48,49,50,51,52,54,55,56,58,59,60,62,63,64,84,69,70,75,76,77,78,82,89,90,92,93";
            string[] arrReportInfo = reportInfo.Split(';');
            string[] arrReportId = reportId.Split(',');

            //sắp xếp lại theo thứ tự id báo cáo
            for (int i = 0; i < arrReportId.Length - 1; i++)
            {
                int minId = int.Parse(arrReportId[i]);
                int pos = i;
                //tìm phần tử nhỏ nhất
                for (int j = i + 1; j < arrReportId.Length; j++)
                {
                    int id = int.Parse(arrReportId[j]);
                    if (minId >= id)
                    {
                        minId = id;
                        pos = j;
                    }
                }
                //hoán đồi id nhỏ nhất lên đầu
                if (pos > i)
                {
                    string tmpId = arrReportId[i];
                    arrReportId[i] = arrReportId[pos];
                    arrReportId[pos] = tmpId;

                    string tmpInfo = arrReportInfo[i];
                    arrReportInfo[i] = arrReportInfo[pos];
                    arrReportInfo[pos] = tmpInfo;
                }
            }

            for (int i = 0; i < arrReportId.Length; i++)
            {
                string id = arrReportId[i];
                string info = arrReportInfo[i];
                //chuyển id từ cuối lên đầu
                info = info.Trim();
                int ipos = info.LastIndexOf(" ");
                info = info.Substring(ipos).Trim() + ". " + info.Substring(0, ipos).Trim();

                int _ID_BAOCAO_new = int.Parse(id);
                reportText += "<table width=\"100%\"><tr><th>&nbsp;</th></tr><tr><th>&nbsp;</th></tr><tr><th style=\"text-align: left; background-color:#dddddd;\"><b>" + info + "</b></th></tr><tr><th>&nbsp;</th></tr></table>";
                reportText += getContentReport(_COURT_ID, _COURT_NAME, _COURT_EXTID, _ISDONVITRUCTHUOC, _sCASE_ID, _ID_BAOCAO, _sCRIMINAL_ID, _REPORT_TIME_ID, _REPORT_TIME_ID2);
            }
            reportText = reportText.Replace("<table cellpadding=\"0\" cellspacing=\"1\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center;\">", "<table cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: times New Roman; font-size: 10pt; text-align: center; border-collapse: collapse;\">");

            //-------------------Export---------------------------
            Literal Table_Str_Totals = new Literal();
            Table_Str_Totals.Text = reportText;
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ToaAn_65MauBaoCao.htm");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/HTML";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        //
        //############ KET THUC VIEW 65 BAO CAO #####################################################################################################
        //
        #endregion
        protected void ddlBaoCao_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbToaTrucThuoc.Visible = true;
            Drop_courts_ext.Visible = true;
            string CapToaLogin = Session["LOAITOA"] + "", CapToaBaoCao = "";
            decimal BaoCaoID;
            if (ddlBaoCao.SelectedValue != null)
            {
                BaoCaoID = Convert.ToDecimal(ddlBaoCao.SelectedValue);
                M_Functions M_Objects = new M_Functions();
                List<Functions> List_Functions = M_Objects.Function_List_ByID(BaoCaoID);
                if (List_Functions.Count > 0)
                {
                    CapToaBaoCao = List_Functions[0].DESCRIPTION + "";
                }
                // xét ẩn hiện đơn vị trực thuộc
                if (CapToaLogin == "CAPTINH" && CapToaBaoCao.Contains("H,"))
                {
                    lbToaTrucThuoc.Visible = false;
                    Drop_courts_ext.Visible = false;
                }
                else if (CapToaLogin == "CAPHUYEN")
                {
                    lbToaTrucThuoc.Visible = false;
                    Drop_courts_ext.Visible = false;
                }
            }
        }
        private bool CheckValidate()
        {
            if (Cls_Comon.IsValidDate(txtNgay.Text) == false)
            {
                lstMsg.Text = "Chưa nhập ngày mở phiên tòa hoặc không hợp lệ !";
                txtNgay.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtDenNgay.Text) == false)
            {
                lstMsg.Text = "Chưa nhập ngày mở phiên tòa hoặc không hợp lệ !";
                txtDenNgay.Focus();
                return false;
            }
            DateTime TuNgay = DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime DenNgay = DateTime.Parse(txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (TuNgay > DenNgay)
            {
                lstMsg.Text = "Từ ngày không được lớn hơn đến ngày. Hãy xem lại!";
                txtNgay.Focus();
                return false;
            }
            return true;
        }

        private void LoadReport_8A(int _COURT_EXTID, string _COURT_NAME,int _ISDONVITRUCTHUOC, string _REPORT_TIME_ID, string _REPORT_TIME_ID2)
        {
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            Int32 PBID = strPBID == "" ? 0 : Convert.ToInt32(strPBID);
            object Result = new object();

            //DateTime? vNgayTu = txtNgay_Tu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgay_Tu.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            //DateTime? vNgayDen = txtNgay_Den.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgay_Den.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            Literal Table_Str_Totals = new Literal();
            GDTTT_BAOCAO_BL oBL = new GDTTT_BAOCAO_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();

            //-------------
            tbl = oBL.Letters_twcw_8A_Export(_COURT_EXTID.ToString(), _COURT_NAME, _ISDONVITRUCTHUOC.ToString(), _REPORT_TIME_ID, _REPORT_TIME_ID2);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }

            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=BaoCao_MAU_8A.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
            
        }

        private void LoadReport_2C(int _COURT_EXTID, string _COURT_NAME, int _ISDONVITRUCTHUOC, string _REPORT_TIME_ID, string _REPORT_TIME_ID2)
        {
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            Int32 PBID = strPBID == "" ? 0 : Convert.ToInt32(strPBID);
            object Result = new object();

            //DateTime? vNgayTu = txtNgay_Tu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgay_Tu.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            //DateTime? vNgayDen = txtNgay_Den.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgay_Den.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            Literal Table_Str_Totals = new Literal();
            GDTTT_BAOCAO_BL oBL = new GDTTT_BAOCAO_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();

            //-------------
            tbl = oBL.Letters_twcw_2C_Export(_COURT_EXTID.ToString(), _COURT_NAME, _ISDONVITRUCTHUOC.ToString(), _REPORT_TIME_ID, _REPORT_TIME_ID2);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }

            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=BaoCao_MAU_2C.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();

        }
    }
}
