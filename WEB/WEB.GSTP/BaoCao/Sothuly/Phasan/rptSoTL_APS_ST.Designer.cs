﻿namespace WEB.GSTP.BaoCao.Sothuly.Phasan
{
    partial class rptSoTL_APS_ST
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSoTL_APS_ST));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.TenToaAn = new DevExpress.XtraReports.Parameters.Parameter();
            this.TuNgay = new DevExpress.XtraReports.Parameters.Parameter();
            this.DenNgay = new DevExpress.XtraReports.Parameters.Parameter();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_NgayXemSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_TuNgay_DenNgay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_TenToaAn = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2797.527F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell145,
            this.xrTableCell60,
            this.xrTableCell22,
            this.xrTableCell169,
            this.xrTableCell30,
            this.xrTableCell170,
            this.xrTableCell36,
            this.xrTableCell38,
            this.xrTableCell42,
            this.xrTableCell44,
            this.xrTableCell171,
            this.xrTableCell47,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell52,
            this.xrTableCell174,
            this.xrTableCell54,
            this.xrTableCell175,
            this.xrTableCell56,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell103,
            this.xrTableCell112,
            this.xrTableCell114,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell176,
            this.xrTableCell131,
            this.xrTableCell177,
            this.xrTableCell137,
            this.xrTableCell179,
            this.xrTableCell180});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "[V_NGAYNHANDON_1]";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.69999951658660475D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "[V_NGUOINOPDON_2]";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.8503933161274817D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "[V_DN_HTX_BI_YCTBPS_3]";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell20.Weight = 0.85039323982582327D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Multiline = true;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell141.StylePriority.UsePadding = false;
            this.xrTableCell141.StylePriority.UseTextAlignment = false;
            this.xrTableCell141.Text = "[V_MANGANHCHINH_4]";
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell141.Weight = 0.28346398095060887D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Multiline = true;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell142.StylePriority.UsePadding = false;
            this.xrTableCell142.StylePriority.UseTextAlignment = false;
            this.xrTableCell142.Text = "[V_THAMPHAN_PCGQ_5]";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell142.Weight = 0.56692890079382185D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Multiline = true;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell145.StylePriority.UsePadding = false;
            this.xrTableCell145.StylePriority.UseTextAlignment = false;
            this.xrTableCell145.Text = "[V_XLD_CHUYENDONCHOTAKHAC_6]";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell145.Weight = 0.69999955774963651D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Multiline = true;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell60.StylePriority.UsePadding = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "[V_XLD_QDTRALAIDON_7]";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell60.Weight = 0.73944687680632826D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "[V_XXL_DENGHICUANGUOINOPDON_8]";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 0.69999954089080907D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Multiline = true;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell169.StylePriority.UsePadding = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.Text = "[V_XXL_KIENNGHICUAVKS_9]";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell169.Weight = 0.69999928383290067D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "[V_XXL_QDGQ_GIUNGUYENQD_10]";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.70000233559012248D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Multiline = true;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell170.StylePriority.UsePadding = false;
            this.xrTableCell170.StylePriority.UseTextAlignment = false;
            this.xrTableCell170.Text = "[V_XXL_QDGQ_HUYQDTRALAIDON_11]";
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell170.Weight = 0.69999806859803382D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell36.StylePriority.UsePadding = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "[V_TLDON_12]";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.70000051000381713D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Multiline = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "[V_TL_VABIHUYVAYCGQLAI_13]";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell38.Weight = 0.69999920833501239D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Multiline = true;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell42.StylePriority.UsePadding = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "[V_QD_KHONGMOTTPS_14]";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.70000042294789178D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Multiline = true;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell44.StylePriority.UsePadding = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "[V_MOTTPS_15]";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell44.Weight = 0.699999202866135D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Multiline = true;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell171.StylePriority.UsePadding = false;
            this.xrTableCell171.StylePriority.UseTextAlignment = false;
            this.xrTableCell171.Text = "[V_QUANTAIVIEN_16]";
            this.xrTableCell171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell171.Weight = 0.69999982932883054D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "[V_QD_ADBPKCTT_17]";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell47.Weight = 1.0999997164247466D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Multiline = true;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell172.StylePriority.UsePadding = false;
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.Text = "[V_DC_TTPS_SO_18]";
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell172.Weight = 0.699999797902997D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Multiline = true;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell173.StylePriority.UsePadding = false;
            this.xrTableCell173.StylePriority.UseTextAlignment = false;
            this.xrTableCell173.Text = "[V_DC_TTPS_XXL_DENGHI_19]";
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell173.Weight = 0.69999987051010293D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Multiline = true;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell48.StylePriority.UsePadding = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.Text = "[V_DC_TTPS_XXL_KIENNGHI_20]";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell48.Weight = 0.69999861121998763D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Multiline = true;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell49.StylePriority.UsePadding = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "[V_DC_TTPS_QDGQ_GIUNGUYEN_21]";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 0.70000102519290763D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Multiline = true;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = "[V_DC_TTPS_QDGQ_HUYQDDC_22]";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell52.Weight = 0.70000101954104688D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Multiline = true;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell174.StylePriority.UsePadding = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.Text = "[V_PHKD_QDCONGNHAN_23]";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell174.Weight = 0.69999860860072283D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Multiline = true;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell54.StylePriority.UsePadding = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = "[V_PHKD_QDSUADOI_24]";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell54.Weight = 0.69999982930363536D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Multiline = true;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell175.StylePriority.UsePadding = false;
            this.xrTableCell175.StylePriority.UseTextAlignment = false;
            this.xrTableCell175.Text = "[V_PHKD_QDDCTHUTUCPHKD_25]";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell175.Weight = 0.8504001619585535D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Multiline = true;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell56.StylePriority.UsePadding = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "[V_TBPS_THEOTTRG_26]";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell56.Weight = 0.699999799451662D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Multiline = true;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell92.StylePriority.UsePadding = false;
            this.xrTableCell92.StylePriority.UseTextAlignment = false;
            this.xrTableCell92.Text = "[V_TBPS_QD_HNCHUNO_KT_27]";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell92.Weight = 0.69999976579003076D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Multiline = true;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell93.StylePriority.UsePadding = false;
            this.xrTableCell93.StylePriority.UseTextAlignment = false;
            this.xrTableCell93.Text = "[V_TBPS_QD_HNCHUNO_28]";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell93.Weight = 0.69999977156907112D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Multiline = true;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell103.StylePriority.UsePadding = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "[V_TBPS_QD_KHONGXAYDUNG_29]";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell103.Weight = 0.69999860164140582D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Multiline = true;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell112.StylePriority.UsePadding = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "[V_TBPS_QD_KHONGTHONGQUA_30]";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.69999982234362146D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Multiline = true;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell114.StylePriority.UsePadding = false;
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.Text = "[V_TBPS_QD_KHONGTHUCHIEN_31]";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell114.Weight = 0.69999978567131027D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell123.StylePriority.UsePadding = false;
            this.xrTableCell123.StylePriority.UseTextAlignment = false;
            this.xrTableCell123.Text = "[V_TBPS_QD_TOCHUCTINDUNG_32]";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell123.Weight = 0.69999979444598459D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Multiline = true;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell124.StylePriority.UsePadding = false;
            this.xrTableCell124.StylePriority.UseTextAlignment = false;
            this.xrTableCell124.Text = "[V_NGUOI_CAMDAMNHIEM_33]";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell124.Weight = 0.8499973426091243D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Multiline = true;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell125.StylePriority.UsePadding = false;
            this.xrTableCell125.StylePriority.UseTextAlignment = false;
            this.xrTableCell125.Text = "[V_TONGGIATRINV_34]";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell125.Weight = 0.56689922402456638D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell176.StylePriority.UsePadding = false;
            this.xrTableCell176.StylePriority.UseTextAlignment = false;
            this.xrTableCell176.Text = "[V_TONGGIATRITHUHOI_35]";
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell176.Weight = 0.56690417473132626D;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell131.StylePriority.UsePadding = false;
            this.xrTableCell131.StylePriority.UseTextAlignment = false;
            this.xrTableCell131.Text = "[V_DNKN_LENTACAPTREN_36]";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell131.Weight = 0.69999733449981993D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Multiline = true;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell177.StylePriority.UsePadding = false;
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.Text = "[V_CHUYENHS_TA_CAPTREN_37]";
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell177.Weight = 0.69999980844699139D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell137.StylePriority.UsePadding = false;
            this.xrTableCell137.StylePriority.UseTextAlignment = false;
            this.xrTableCell137.Text = "[V_QDGQ_DNKN_TA_CAPTREN_38]";
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell137.Weight = 0.84999978605410242D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell179.StylePriority.UsePadding = false;
            this.xrTableCell179.StylePriority.UseTextAlignment = false;
            this.xrTableCell179.Text = "[V_QUAHANLUATDINH_39]";
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell179.Weight = 0.43351065269724165D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 3, 3, 100F);
            this.xrTableCell180.StylePriority.UsePadding = false;
            this.xrTableCell180.StylePriority.UseTextAlignment = false;
            this.xrTableCell180.Text = "[V_GHICHU_40]";
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell180.Weight = 0.56693256252998636D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 30F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TenToaAn
            // 
            this.TenToaAn.Description = "Tên tòa án";
            this.TenToaAn.Name = "TenToaAn";
            // 
            // TuNgay
            // 
            this.TuNgay.Description = "Từ ngày";
            this.TuNgay.Name = "TuNgay";
            // 
            // DenNgay
            // 
            this.DenNgay.Description = "Đến ngày";
            this.DenNgay.Name = "DenNgay";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrLabel1,
            this.xrLabel_TuNgay_DenNgay,
            this.xrLabel_TenToaAn});
            this.ReportHeader.HeightF = 100F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable4
            // 
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(1706.709F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10});
            this.xrTable4.SizeF = new System.Drawing.SizeF(551.0001F, 74.70837F);
            this.xrTable4.StylePriority.UsePadding = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.1712011175384138D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseFont = false;
            this.xrTableCell138.StylePriority.UseTextAlignment = false;
            this.xrTableCell138.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell138.Weight = 3.3849558216606015D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.91500044726409613D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseFont = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.Text = "Độc lập - Tự do - Hạnh phúc";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell178.Weight = 3.3849558216606015D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseTextAlignment = false;
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow9.Weight = 0.33956810398564718D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseTextAlignment = false;
            this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell181.Weight = 3.3849558216606015D;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(179.921F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(198F, 2.083333F);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_NgayXemSo});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.2200006905279972D;
            // 
            // xrTableCell_NgayXemSo
            // 
            this.xrTableCell_NgayXemSo.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Italic);
            this.xrTableCell_NgayXemSo.Name = "xrTableCell_NgayXemSo";
            this.xrTableCell_NgayXemSo.StylePriority.UseFont = false;
            this.xrTableCell_NgayXemSo.StylePriority.UseTextAlignment = false;
            this.xrTableCell_NgayXemSo.Text = "Ngày [] tháng [] năm []  ";
            this.xrTableCell_NgayXemSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell_NgayXemSo.Weight = 3.3849558216606015D;
            this.xrTableCell_NgayXemSo.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell_NgayXemSo_BeforePrint);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(707.7499F, 19.83334F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(980.2089F, 25F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "SỔ THỤ LÝ VÀ KẾT QUẢ GIẢI QUYẾT YÊU CẦU TUYÊN BỐ PHÁ SẢN";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_TuNgay_DenNgay
            // 
            this.xrLabel_TuNgay_DenNgay.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel_TuNgay_DenNgay.LocationFloat = new DevExpress.Utils.PointFloat(707.7503F, 49.70837F);
            this.xrLabel_TuNgay_DenNgay.Name = "xrLabel_TuNgay_DenNgay";
            this.xrLabel_TuNgay_DenNgay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_TuNgay_DenNgay.SizeF = new System.Drawing.SizeF(980.2089F, 25F);
            this.xrLabel_TuNgay_DenNgay.StylePriority.UseFont = false;
            this.xrLabel_TuNgay_DenNgay.StylePriority.UseTextAlignment = false;
            this.xrLabel_TuNgay_DenNgay.Text = "Từ ngày [] đến ngày []";
            this.xrLabel_TuNgay_DenNgay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrLabel_TuNgay_DenNgay.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel_TuNgay_DenNgay_BeforePrint);
            // 
            // xrLabel_TenToaAn
            // 
            this.xrLabel_TenToaAn.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel_TenToaAn.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_TenToaAn.Name = "xrLabel_TenToaAn";
            this.xrLabel_TenToaAn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_TenToaAn.SizeF = new System.Drawing.SizeF(687.5487F, 25F);
            this.xrLabel_TenToaAn.StylePriority.UseFont = false;
            this.xrLabel_TenToaAn.StylePriority.UseTextAlignment = false;
            this.xrLabel_TenToaAn.Text = "xrLabel_TenToaAn";
            this.xrLabel_TenToaAn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel_TenToaAn.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel_TenToaAn_BeforePrint);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader1.HeightF = 490.6251F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2797.527F, 490.6251F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell194,
            this.xrTableCell198,
            this.xrTableCell206,
            this.xrTableCell222,
            this.xrTableCell226,
            this.xrTableCell230,
            this.xrTableCell238,
            this.xrTableCell242,
            this.xrTableCell246,
            this.xrTableCell282,
            this.xrTableCell270,
            this.xrTableCell306,
            this.xrTableCell310,
            this.xrTableCell314,
            this.xrTableCell318,
            this.xrTableCell322,
            this.xrTableCell326,
            this.xrTableCell330,
            this.xrTableCell334,
            this.xrTableCell338});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 2.3750012416356996D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.RowSpan = 3;
            this.xrTableCell182.StylePriority.UseFont = false;
            this.xrTableCell182.StylePriority.UseTextAlignment = false;
            this.xrTableCell182.Text = "NGÀY NHẬN ĐƠN";
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell182.Weight = 0.76091626973467663D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell183.Multiline = true;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.RowSpan = 3;
            this.xrTableCell183.StylePriority.UseFont = false;
            this.xrTableCell183.StylePriority.UseTextAlignment = false;
            this.xrTableCell183.Text = "NGƯỜI NỘP ĐƠN \r\nHọ tên, địa chỉ, tư cách pháp lý (Điều 5)";
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell183.Weight = 0.92439773630708078D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell184.Multiline = true;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.RowSpan = 3;
            this.xrTableCell184.StylePriority.UseFont = false;
            this.xrTableCell184.StylePriority.UseTextAlignment = false;
            this.xrTableCell184.Text = resources.GetString("xrTableCell184.Text");
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell184.Weight = 0.92439811575463626D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.RowSpan = 3;
            this.xrTableCell194.StylePriority.UseFont = false;
            this.xrTableCell194.StylePriority.UseTextAlignment = false;
            this.xrTableCell194.Text = "MÃ NGÀNH CHÍNH";
            this.xrTableCell194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell194.Weight = 0.30813274227741738D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.RowSpan = 3;
            this.xrTableCell198.StylePriority.UseFont = false;
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.Text = "HỌ VÀ TÊN THẨM PHÁN ĐƯỢC PHÂN CÔNG GIẢI QUYẾT";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell198.Weight = 0.61626537235356871D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseFont = false;
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.Text = "XỬ LÝ ĐƠN YÊU CẦU MỞ THỦ TỤC PHÁ SẢN";
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell206.Weight = 1.5647126687485815D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseFont = false;
            this.xrTableCell222.StylePriority.UseTextAlignment = false;
            this.xrTableCell222.Text = "ĐỀ NGHỊ XEM XÉT LẠI, KIẾN NGHỊ VIỆC TRẢ LẠI ĐƠN YÊU CẦU MỞ THỦ TỤC PHÁ SẢN";
            this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell222.Weight = 3.0436667818898391D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell226.Multiline = true;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.RowSpan = 3;
            this.xrTableCell226.StylePriority.UseFont = false;
            this.xrTableCell226.StylePriority.UseTextAlignment = false;
            this.xrTableCell226.Text = "THỤ LÝ ĐƠN\r\n Số, ngày tháng năm";
            this.xrTableCell226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell226.Weight = 0.76091574969008557D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell230.Multiline = true;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.RowSpan = 3;
            this.xrTableCell230.StylePriority.UseFont = false;
            this.xrTableCell230.StylePriority.UseTextAlignment = false;
            this.xrTableCell230.Text = "THỤ LÝ \r\nVỤ ÁN BỊ HỦY VÀ YÊU CẦU  GIẢI QUYẾT LẠI\r\nSố, ngày, tháng, năm";
            this.xrTableCell230.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell230.Weight = 0.76091643498320394D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseFont = false;
            this.xrTableCell238.StylePriority.UseTextAlignment = false;
            this.xrTableCell238.Text = "QUYẾT ĐỊNH CỦATHẨM PHÁN";
            this.xrTableCell238.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell238.Weight = 1.5218321758128979D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell242.Multiline = true;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.RowSpan = 3;
            this.xrTableCell242.StylePriority.UseFont = false;
            this.xrTableCell242.StylePriority.UseTextAlignment = false;
            this.xrTableCell242.Text = "QUẢN TÀI VIÊN, DOANH NGHIỆP QUẢN LÝ, THANH LÝ TÀI SẢN\r\n Tên, số ngày tháng năm vă" +
    "n bản chỉ định";
            this.xrTableCell242.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell242.Weight = 0.76091640528144833D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell246.Multiline = true;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.RowSpan = 3;
            this.xrTableCell246.StylePriority.UseFont = false;
            this.xrTableCell246.StylePriority.UseTextAlignment = false;
            this.xrTableCell246.Text = "QUYẾT ĐỊNH ÁP DỤNG, THAY ĐỔI, HỦY BỎ QUYẾT ĐỊNH ÁP DỤNG BIỆN PHÁP KHẨN CẤP TẠM TH" +
    "ỜI \r\nTên loại quyết định; số, ngày, tháng, năm; lý do (nếu có)";
            this.xrTableCell246.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell246.Weight = 1.1957258575976397D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseFont = false;
            this.xrTableCell282.StylePriority.UseTextAlignment = false;
            this.xrTableCell282.Text = "ĐÌNH CHỈ TIẾN HÀNH THỦ TỤC PHÁ SẢN";
            this.xrTableCell282.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell282.Weight = 3.8045809585210808D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseFont = false;
            this.xrTableCell270.StylePriority.UseTextAlignment = false;
            this.xrTableCell270.Text = "PHỤC HỒI HOẠT ĐỘNG KINH DOANH";
            this.xrTableCell270.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell270.Weight = 2.4462382925721791D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseFont = false;
            this.xrTableCell306.StylePriority.UseTextAlignment = false;
            this.xrTableCell306.Text = "TUYÊN BỐ PHÁ SẢN";
            this.xrTableCell306.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell306.Weight = 5.32641422121843D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell310.Multiline = true;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.RowSpan = 3;
            this.xrTableCell310.StylePriority.UseFont = false;
            this.xrTableCell310.StylePriority.UseTextAlignment = false;
            this.xrTableCell310.Text = "NGƯỜI BỊ CẤM ĐẢM NHIỆM CHỨC VỤ\r\n Họ và tên, chức vụ, thời hạn bị cấm";
            this.xrTableCell310.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell310.Weight = 0.92396998836634148D;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.RowSpan = 3;
            this.xrTableCell314.StylePriority.UseFont = false;
            this.xrTableCell314.StylePriority.UseTextAlignment = false;
            this.xrTableCell314.Text = "TỔNG GIÁ TRỊ NGHĨA VỤ VỀ TÀI SẢN CỦA DOANH NGHIỆP (Điều 51)";
            this.xrTableCell314.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell314.Weight = 0.61623360270280514D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.RowSpan = 3;
            this.xrTableCell318.StylePriority.UseFont = false;
            this.xrTableCell318.StylePriority.UseTextAlignment = false;
            this.xrTableCell318.Text = "TỔNG GIÁ TRỊ TÀI SẢN THU HỒI ĐỂ PHÂN CHIA (Điều 54)";
            this.xrTableCell318.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell318.Weight = 0.6162336716265886D;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell322.Multiline = true;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.RowSpan = 3;
            this.xrTableCell322.StylePriority.UseFont = false;
            this.xrTableCell322.StylePriority.UseTextAlignment = false;
            this.xrTableCell322.Text = "ĐỀ NGHỊ, KHÁNG NGHỊ CÁC QUYẾT ĐỊNH LÊN TÒA ÁN CẤP TRÊN\r\nNgày, tháng, năm, loại qu" +
    "yết định";
            this.xrTableCell322.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell322.Weight = 0.76091652682338D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell326.Multiline = true;
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.RowSpan = 3;
            this.xrTableCell326.StylePriority.UseFont = false;
            this.xrTableCell326.StylePriority.UseTextAlignment = false;
            this.xrTableCell326.Text = "CHUYỂN HỒ SƠ CHO TÒA ÁN CẤP TRÊN\r\nNgày, tháng, năm";
            this.xrTableCell326.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell326.Weight = 0.76091647942010887D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell330.Multiline = true;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.RowSpan = 3;
            this.xrTableCell330.StylePriority.UseFont = false;
            this.xrTableCell330.StylePriority.UseTextAlignment = false;
            this.xrTableCell330.Text = "QUYẾT ĐỊNH GIẢI QUYẾT ĐỀ NGHỊ, KHÁNG NGHỊ CỦA TÒA ÁN CẤP TRÊN \r\nSố, ngày tháng nă" +
    "m; kết quả";
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell330.Weight = 0.92397000106487059D;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.RowSpan = 3;
            this.xrTableCell334.StylePriority.UseFont = false;
            this.xrTableCell334.StylePriority.UseTextAlignment = false;
            this.xrTableCell334.Text = "QUÁ HẠN LUẬT ĐỊNH";
            this.xrTableCell334.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell334.Weight = 0.47124413588975739D;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.RowSpan = 3;
            this.xrTableCell338.StylePriority.UseFont = false;
            this.xrTableCell338.StylePriority.UseTextAlignment = false;
            this.xrTableCell338.Text = "GHI CHÚ";
            this.xrTableCell338.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell338.Weight = 0.61626167965597345D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell195,
            this.xrTableCell199,
            this.xrTableCell203,
            this.xrTableCell207,
            this.xrTableCell211,
            this.xrTableCell215,
            this.xrTableCell223,
            this.xrTableCell227,
            this.xrTableCell231,
            this.xrTableCell235,
            this.xrTableCell239,
            this.xrTableCell243,
            this.xrTableCell247,
            this.xrTableCell251,
            this.xrTableCell259,
            this.xrTableCell283,
            this.xrTableCell279,
            this.xrTableCell267,
            this.xrTableCell271,
            this.xrTableCell275,
            this.xrTableCell287,
            this.xrTableCell291,
            this.xrTableCell303,
            this.xrTableCell307,
            this.xrTableCell311,
            this.xrTableCell315,
            this.xrTableCell319,
            this.xrTableCell323,
            this.xrTableCell327,
            this.xrTableCell331,
            this.xrTableCell335,
            this.xrTableCell339});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 3.5416681397192997D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.71655727267159586D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Weight = 0.87050841574962279D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.87050830243208988D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.29016953637747933D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.58033844892764941D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell203.Multiline = true;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.RowSpan = 2;
            this.xrTableCell203.StylePriority.UseFont = false;
            this.xrTableCell203.StylePriority.UseTextAlignment = false;
            this.xrTableCell203.Text = "CHUYỂN ĐƠN CHO TÒA ÁN KHÁC\r\n Số, ngày tháng năm";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell203.Weight = 0.71655731372854514D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell207.Multiline = true;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.RowSpan = 2;
            this.xrTableCell207.StylePriority.UseFont = false;
            this.xrTableCell207.StylePriority.UseTextAlignment = false;
            this.xrTableCell207.Text = "QUYẾT ĐỊNH TRẢ LẠI ĐƠN \r\nSố, ngày tháng năm; lý do";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell207.Weight = 0.75693807155124992D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.RowSpan = 2;
            this.xrTableCell211.StylePriority.UseFont = false;
            this.xrTableCell211.StylePriority.UseTextAlignment = false;
            this.xrTableCell211.Text = " ĐỀ NGHỊ CỦA NGƯỜI NỘP ĐƠN Họ và tên, ngày tháng năm đề nghị";
            this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell211.Weight = 0.71655731873888706D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.RowSpan = 2;
            this.xrTableCell215.StylePriority.UseFont = false;
            this.xrTableCell215.StylePriority.UseTextAlignment = false;
            this.xrTableCell215.Text = "KIẾN NGHỊ CỦA VKS Số, ngày tháng năm";
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell215.Weight = 0.7165573025649945D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseFont = false;
            this.xrTableCell223.StylePriority.UseTextAlignment = false;
            this.xrTableCell223.Text = "QUYẾT ĐỊNH GIẢI QUYẾT";
            this.xrTableCell223.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell223.Weight = 1.4331152925591408D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.71655731820336288D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 0.71655731767906039D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell235.Multiline = true;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.RowSpan = 2;
            this.xrTableCell235.StylePriority.UseFont = false;
            this.xrTableCell235.StylePriority.UseTextAlignment = false;
            this.xrTableCell235.Text = "KHÔNG MỞ THỦ TỤC PHÁ SẢN  \r\nSố, ngày tháng năm lý do";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell235.Weight = 0.71655728217428616D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell239.Multiline = true;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.RowSpan = 2;
            this.xrTableCell239.StylePriority.UseFont = false;
            this.xrTableCell239.StylePriority.UseTextAlignment = false;
            this.xrTableCell239.Text = "MỞ THỦ TỤC PHÁ SẢN \r\nSố, ngày, tháng, năm";
            this.xrTableCell239.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell239.Weight = 0.71655732595283717D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Weight = 0.71655732595289434D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Weight = 1.1260190923492353D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.RowSpan = 2;
            this.xrTableCell251.StylePriority.UseFont = false;
            this.xrTableCell251.StylePriority.UseTextAlignment = false;
            this.xrTableCell251.Text = "SỐ, NGÀY THÁNG NĂM; LÝ DO ĐÌNH CHỈ";
            this.xrTableCell251.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell251.Weight = 0.71655763834719788D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseFont = false;
            this.xrTableCell259.StylePriority.UseTextAlignment = false;
            this.xrTableCell259.Text = "ĐỀ NGHỊ XEM XÉT LẠI, KIẾN NGHỊ VIỆC ĐÌNH CHỈ TIẾN HÀNH THỦ TỤC PHÁ SẢN";
            this.xrTableCell259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell259.Weight = 1.4331152992694902D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell283.Multiline = true;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseFont = false;
            this.xrTableCell283.StylePriority.UseTextAlignment = false;
            this.xrTableCell283.Text = "QUYẾT ĐỊNH GIẢI QUYẾT\r\n ĐỀ NGHỊ, KIẾN NGHỊ";
            this.xrTableCell283.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell283.Weight = 1.4331152819112631D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell279.Multiline = true;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.RowSpan = 2;
            this.xrTableCell279.StylePriority.UseFont = false;
            this.xrTableCell279.StylePriority.UseTextAlignment = false;
            this.xrTableCell279.Text = "QUYẾT ĐỊNH CÔNG NHẬN NGHỊ QUYẾT CỦA HỘI NGHỊ CHỦ NỢ VỀ PHƯƠNG ÁN PHỤC HỒI KINH DO" +
    "ANH \r\nSố, ngày tháng năm, tóm tắt nội dung";
            this.xrTableCell279.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell279.Weight = 0.71655762675623369D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell267.Multiline = true;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.RowSpan = 2;
            this.xrTableCell267.StylePriority.UseFont = false;
            this.xrTableCell267.StylePriority.UseTextAlignment = false;
            this.xrTableCell267.Text = "QUYẾT ĐỊNH SỬA ĐỔI, BỔ SUNG PHƯƠNG ÁN PHỤC HỒI HOẠT ĐỘNG KINH DOANH\r\nSố, ngày, th" +
    "áng, năm, tóm tắt nội dung";
            this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell267.Weight = 0.71655760640432664D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell271.Multiline = true;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.RowSpan = 2;
            this.xrTableCell271.StylePriority.UseFont = false;
            this.xrTableCell271.StylePriority.UseTextAlignment = false;
            this.xrTableCell271.Text = "QUYẾT ĐỊNH ĐÌNH CHỈ THỦ TỤC PHỤC HỒI HOẠT ĐỘNG KINH DOANH \r\nSố, ngày tháng năm; t" +
    "óm tắt nội dung, lý do (Điều 95 LPS)";
            this.xrTableCell271.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell271.Weight = 0.870515171763327D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell275.Multiline = true;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.RowSpan = 2;
            this.xrTableCell275.StylePriority.UseFont = false;
            this.xrTableCell275.StylePriority.UseTextAlignment = false;
            this.xrTableCell275.Text = "THEO THỦ TỤC RÚT GỌN \r\nSố, ngày, tháng, năm";
            this.xrTableCell275.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell275.Weight = 0.71655759246665018D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell287.Multiline = true;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.RowSpan = 2;
            this.xrTableCell287.StylePriority.UseFont = false;
            this.xrTableCell287.StylePriority.UseTextAlignment = false;
            this.xrTableCell287.Text = "QUYẾT ĐỊNH KHI HỘI NGHỊ CHỦ NỢ KHÔNG THÀNH \r\nSố, ngày, tháng, năm";
            this.xrTableCell287.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell287.Weight = 0.7165576025928192D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell291.Multiline = true;
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.RowSpan = 2;
            this.xrTableCell291.StylePriority.UseFont = false;
            this.xrTableCell291.StylePriority.UseTextAlignment = false;
            this.xrTableCell291.Text = "QUYẾT ĐỊNH THEO NGHỊ QUYẾT CỦA HỘI NGHỊ CHỦ NỢ \r\nSố, ngày, tháng, năm";
            this.xrTableCell291.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell291.Weight = 0.71655760930992807D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseFont = false;
            this.xrTableCell303.StylePriority.UseTextAlignment = false;
            this.xrTableCell303.Text = "QUYẾT ĐỊNH SAU KHI CÓ NGHỊ QUYẾT CỦA HỘI NGHỊ CHỦ NỢ VỀ PHƯƠNG ÁN PHỤC HỒI HOẠT Đ" +
    "ỘNG KINH DOANH";
            this.xrTableCell303.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell303.Weight = 2.149671619506925D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell307.Multiline = true;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.RowSpan = 2;
            this.xrTableCell307.StylePriority.UseFont = false;
            this.xrTableCell307.StylePriority.UseTextAlignment = false;
            this.xrTableCell307.Text = "QUYẾT ĐỊNH ĐỐI VỚI TỔ CHỨC TÍN DỤNG \r\nSố, ngày tháng năm";
            this.xrTableCell307.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell307.Weight = 0.71655765976982488D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Weight = 0.87010569505476076D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.Weight = 0.58030928318415176D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.Weight = 0.58030931003439812D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.Weight = 0.71655760260610568D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Weight = 0.71655764692524815D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Weight = 0.87010572117282536D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.RowSpan = 0;
            this.xrTableCell335.Weight = 0.44377031281869617D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Weight = 0.58033929977554455D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell196,
            this.xrTableCell200,
            this.xrTableCell204,
            this.xrTableCell208,
            this.xrTableCell212,
            this.xrTableCell216,
            this.xrTableCell220,
            this.xrTableCell224,
            this.xrTableCell228,
            this.xrTableCell232,
            this.xrTableCell236,
            this.xrTableCell240,
            this.xrTableCell244,
            this.xrTableCell248,
            this.xrTableCell252,
            this.xrTableCell256,
            this.xrTableCell260,
            this.xrTableCell264,
            this.xrTableCell284,
            this.xrTableCell280,
            this.xrTableCell268,
            this.xrTableCell272,
            this.xrTableCell276,
            this.xrTableCell288,
            this.xrTableCell292,
            this.xrTableCell296,
            this.xrTableCell300,
            this.xrTableCell304,
            this.xrTableCell308,
            this.xrTableCell312,
            this.xrTableCell316,
            this.xrTableCell320,
            this.xrTableCell324,
            this.xrTableCell328,
            this.xrTableCell332,
            this.xrTableCell336,
            this.xrTableCell340});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 12.708332714845145D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Weight = 0.700000380335843D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.85039413451814561D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Weight = 0.85039409929916132D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Weight = 0.28346473691771257D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.56692943574723142D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 0.70000042139279239D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Weight = 0.73944834727328D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 0.70000042640313409D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.70000042740951507D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell220.Multiline = true;
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseFont = false;
            this.xrTableCell220.StylePriority.UseTextAlignment = false;
            this.xrTableCell220.Text = "GIỮ NGUYÊN QUYẾT ĐỊNH TRẢ LẠI ĐƠN YÊU CẦU MỞ THỦ TỤC PHÁ SẢN \r\nSố, ngày tháng năm" +
    "";
            this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell220.Weight = 0.70000043779211318D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell224.Multiline = true;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseFont = false;
            this.xrTableCell224.StylePriority.UseTextAlignment = false;
            this.xrTableCell224.Text = "HỦY QUYẾT ĐỊNH TRẢ LẠI ĐƠN\r\nSố, ngày tháng năm";
            this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell224.Weight = 0.70000042776203331D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Weight = 0.70000052092687448D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 0.700000519516488D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 0.70000049968184963D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 0.70000047902892437D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 0.70000047902892426D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 1.1000010949944454D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.70000070791098268D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell256.Multiline = true;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseFont = false;
            this.xrTableCell256.StylePriority.UseTextAlignment = false;
            this.xrTableCell256.Text = " ĐỀ NGHỊ \r\nNgày tháng năm, \r\nngười đề nghị";
            this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell256.Weight = 0.70000070789294244D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell260.Multiline = true;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseFont = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "KIẾN NGHỊ CỦA VKS\r\n Số, ngày tháng năm";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell260.Weight = 0.70000068075884658D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell264.Multiline = true;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseFont = false;
            this.xrTableCell264.StylePriority.UseTextAlignment = false;
            this.xrTableCell264.Text = "GIỮ NGUYÊN QUYẾT ĐỊNH ĐÌNH CHỈ TIẾN HÀNH THỦ TỤC PHÁ SẢN \r\nSố, ngày tháng năm";
            this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell264.Weight = 0.70000069204416882D;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell284.Multiline = true;
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseFont = false;
            this.xrTableCell284.StylePriority.UseTextAlignment = false;
            this.xrTableCell284.Text = "HỦY QUYẾT ĐỊNH ĐÌNH CHỈ TIẾN HÀNH THỦ TỤC PHÁ SẢN VÀ GIAO CHO THẨM PHÁN TIẾN HÀNH" +
    " GIẢI QUYẾT PHÁ SẢN\r\nSố, ngày tháng năm";
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell284.Weight = 0.70000071353133952D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Weight = 0.70000071353183368D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Weight = 0.70000071353278015D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Weight = 0.85040086988179586D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Weight = 0.70000065180399451D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Weight = 0.7000007038753564D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Weight = 0.70000070387645974D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell296.Multiline = true;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseFont = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.Text = "KHÔNG XÂY DỰNG ĐƯỢC PHƯƠNG ÁN PHỤC HỒI HOẠT ĐỘNG KINH DOANH \r\nSố, ngày, tháng, nă" +
    "m";
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell296.Weight = 0.70000067224083462D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell300.Multiline = true;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseFont = false;
            this.xrTableCell300.StylePriority.UseTextAlignment = false;
            this.xrTableCell300.Text = "KHÔNG THÔNG QUA ĐƯỢC PHƯƠNG ÁN PHỤC HỒI HOẠT ĐỘNG KINH DOANH \r\nSố, ngày, tháng, n" +
    "ăm";
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell300.Weight = 0.700000703816876D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell304.Multiline = true;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseFont = false;
            this.xrTableCell304.StylePriority.UseTextAlignment = false;
            this.xrTableCell304.Text = "KHÔNG THỰC HIỆN ĐƯỢC PHƯƠNG ÁN PHỤC HỒI HOẠT ĐỘNG KINH DOANH\r\nSố, ngày, tháng, nă" +
    "m";
            this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell304.Weight = 0.70000066972242048D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Weight = 0.70000070330637221D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.Weight = 0.85000079868216072D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.Weight = 0.56690056860278981D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Weight = 0.56690051297431143D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Weight = 0.70000068672475979D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Weight = 0.7000006849935605D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.Weight = 0.85000085635513378D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.RowSpan = 0;
            this.xrTableCell336.Weight = 0.43351236813554106D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.Weight = 0.5669317996735852D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell197,
            this.xrTableCell201,
            this.xrTableCell205,
            this.xrTableCell209,
            this.xrTableCell213,
            this.xrTableCell217,
            this.xrTableCell221,
            this.xrTableCell225,
            this.xrTableCell229,
            this.xrTableCell233,
            this.xrTableCell237,
            this.xrTableCell241,
            this.xrTableCell245,
            this.xrTableCell249,
            this.xrTableCell253,
            this.xrTableCell257,
            this.xrTableCell261,
            this.xrTableCell265,
            this.xrTableCell285,
            this.xrTableCell281,
            this.xrTableCell269,
            this.xrTableCell273,
            this.xrTableCell277,
            this.xrTableCell289,
            this.xrTableCell293,
            this.xrTableCell297,
            this.xrTableCell301,
            this.xrTableCell305,
            this.xrTableCell309,
            this.xrTableCell313,
            this.xrTableCell317,
            this.xrTableCell321,
            this.xrTableCell325,
            this.xrTableCell329,
            this.xrTableCell333,
            this.xrTableCell337,
            this.xrTableCell341});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.0000012207031794D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseFont = false;
            this.xrTableCell191.StylePriority.UseTextAlignment = false;
            this.xrTableCell191.Text = "1";
            this.xrTableCell191.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell191.Weight = 0.700000380335843D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseFont = false;
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.Text = "2";
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell192.Weight = 0.85039413451814561D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.Text = "3";
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell193.Weight = 0.85039409929916132D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseFont = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.Text = "4";
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell197.Weight = 0.28346473691771257D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Text = "5";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell201.Weight = 0.56692943574723142D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseTextAlignment = false;
            this.xrTableCell205.Text = "6";
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell205.Weight = 0.70000042139279239D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseFont = false;
            this.xrTableCell209.StylePriority.UseTextAlignment = false;
            this.xrTableCell209.Text = "7";
            this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell209.Weight = 0.73944834727328D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseFont = false;
            this.xrTableCell213.StylePriority.UseTextAlignment = false;
            this.xrTableCell213.Text = "8";
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell213.Weight = 0.70000042640313409D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseFont = false;
            this.xrTableCell217.StylePriority.UseTextAlignment = false;
            this.xrTableCell217.Text = "9";
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell217.Weight = 0.70000042740951507D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseFont = false;
            this.xrTableCell221.StylePriority.UseTextAlignment = false;
            this.xrTableCell221.Text = "10";
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell221.Weight = 0.70000043779211307D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseFont = false;
            this.xrTableCell225.StylePriority.UseTextAlignment = false;
            this.xrTableCell225.Text = "11";
            this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell225.Weight = 0.70000042776203353D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseFont = false;
            this.xrTableCell229.StylePriority.UseTextAlignment = false;
            this.xrTableCell229.Text = "12";
            this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell229.Weight = 0.70000052092687481D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseFont = false;
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "13";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell233.Weight = 0.70000051951648778D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseFont = false;
            this.xrTableCell237.StylePriority.UseTextAlignment = false;
            this.xrTableCell237.Text = "14";
            this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell237.Weight = 0.70000049968184974D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseFont = false;
            this.xrTableCell241.StylePriority.UseTextAlignment = false;
            this.xrTableCell241.Text = "15";
            this.xrTableCell241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell241.Weight = 0.70000047902892426D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseFont = false;
            this.xrTableCell245.StylePriority.UseTextAlignment = false;
            this.xrTableCell245.Text = "16";
            this.xrTableCell245.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell245.Weight = 0.70000047902892448D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseFont = false;
            this.xrTableCell249.StylePriority.UseTextAlignment = false;
            this.xrTableCell249.Text = "17";
            this.xrTableCell249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell249.Weight = 1.1000010941896026D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseFont = false;
            this.xrTableCell253.StylePriority.UseTextAlignment = false;
            this.xrTableCell253.Text = "18";
            this.xrTableCell253.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell253.Weight = 0.70000070791098357D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseFont = false;
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "19";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell257.Weight = 0.70000070789294333D;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseFont = false;
            this.xrTableCell261.StylePriority.UseTextAlignment = false;
            this.xrTableCell261.Text = "20";
            this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell261.Weight = 0.70000068075884692D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseFont = false;
            this.xrTableCell265.StylePriority.UseTextAlignment = false;
            this.xrTableCell265.Text = "21";
            this.xrTableCell265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell265.Weight = 0.70000069204416959D;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseFont = false;
            this.xrTableCell285.StylePriority.UseTextAlignment = false;
            this.xrTableCell285.Text = "22";
            this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell285.Weight = 0.70000071353133986D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseFont = false;
            this.xrTableCell281.StylePriority.UseTextAlignment = false;
            this.xrTableCell281.Text = "23";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell281.Weight = 0.70000071353183446D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseFont = false;
            this.xrTableCell269.StylePriority.UseTextAlignment = false;
            this.xrTableCell269.Text = "24";
            this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell269.Weight = 0.70000071353278071D;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseFont = false;
            this.xrTableCell273.StylePriority.UseTextAlignment = false;
            this.xrTableCell273.Text = "25";
            this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell273.Weight = 0.85040086988181385D;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseFont = false;
            this.xrTableCell277.StylePriority.UseTextAlignment = false;
            this.xrTableCell277.Text = "26";
            this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell277.Weight = 0.700000651803995D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseFont = false;
            this.xrTableCell289.StylePriority.UseTextAlignment = false;
            this.xrTableCell289.Text = "27";
            this.xrTableCell289.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell289.Weight = 0.70000070387535729D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseFont = false;
            this.xrTableCell293.StylePriority.UseTextAlignment = false;
            this.xrTableCell293.Text = "28";
            this.xrTableCell293.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell293.Weight = 0.70000070387646041D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseFont = false;
            this.xrTableCell297.StylePriority.UseTextAlignment = false;
            this.xrTableCell297.Text = "29";
            this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell297.Weight = 0.70000067224083573D;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseFont = false;
            this.xrTableCell301.StylePriority.UseTextAlignment = false;
            this.xrTableCell301.Text = "30";
            this.xrTableCell301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell301.Weight = 0.70000070381687685D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseFont = false;
            this.xrTableCell305.StylePriority.UseTextAlignment = false;
            this.xrTableCell305.Text = "31";
            this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell305.Weight = 0.700000669722421D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseFont = false;
            this.xrTableCell309.StylePriority.UseTextAlignment = false;
            this.xrTableCell309.Text = "32";
            this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell309.Weight = 0.700000703306373D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseFont = false;
            this.xrTableCell313.StylePriority.UseTextAlignment = false;
            this.xrTableCell313.Text = "33";
            this.xrTableCell313.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell313.Weight = 0.85000079868216094D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseFont = false;
            this.xrTableCell317.StylePriority.UseTextAlignment = false;
            this.xrTableCell317.Text = "34";
            this.xrTableCell317.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell317.Weight = 0.56690056860271187D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseFont = false;
            this.xrTableCell321.StylePriority.UseTextAlignment = false;
            this.xrTableCell321.Text = "35";
            this.xrTableCell321.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell321.Weight = 0.56690051297356192D;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseFont = false;
            this.xrTableCell325.StylePriority.UseTextAlignment = false;
            this.xrTableCell325.Text = "36";
            this.xrTableCell325.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell325.Weight = 0.7000006855306542D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseFont = false;
            this.xrTableCell329.StylePriority.UseTextAlignment = false;
            this.xrTableCell329.Text = "37";
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell329.Weight = 0.7000006853617784D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseFont = false;
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "38";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell333.Weight = 0.85000085634373557D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.RowSpan = 0;
            this.xrTableCell337.StylePriority.UseFont = false;
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "39";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell337.Weight = 0.43351236853682196D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.StylePriority.UseFont = false;
            this.xrTableCell341.StylePriority.UseTextAlignment = false;
            this.xrTableCell341.Text = "40";
            this.xrTableCell341.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell341.Weight = 0.56693187596762229D;
            // 
            // rptSoTL_APS_ST
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1});
            this.DisplayName = "Sổ thụ lý và kết quả giải quyết yêu cầu tuyên bố phá sản";
            this.ExportOptions.Docx.TableLayout = true;
            this.ExportOptions.Rtf.ExportMode = DevExpress.XtraPrinting.RtfExportMode.SingleFile;
            this.Margins = new System.Drawing.Printing.Margins(57, 27, 30, 30);
            this.PageHeight = 1654;
            this.PageWidth = 2940;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.TenToaAn,
            this.TuNgay,
            this.DenNgay});
            this.RequestParameters = false;
            this.Version = "17.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.Parameters.Parameter TenToaAn;
        private DevExpress.XtraReports.Parameters.Parameter TuNgay;
        private DevExpress.XtraReports.Parameters.Parameter DenNgay;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_NgayXemSo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_TuNgay_DenNgay;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_TenToaAn;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
    }
}
