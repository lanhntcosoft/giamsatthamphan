﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.BaoCao.AHS
{
    public partial class rpt21HS : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt21HS()
        {
            InitializeComponent();
        }

        private void SubBand_HDXX_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_HDXX"].Value + "" == "")
            {
                SubBand_HDXX.Visible = false;
            }
            else
            {
                SubBand_HDXX.Visible = true;
            }
        }

        private void SubBand_TP_DuKhuyet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_TP_DuKhuyet"].Value + "" == "")
            {
                SubBand_TP_DuKhuyet.Visible = false;
            }
            else
            {
                SubBand_TP_DuKhuyet.Visible = true;
                int Length = xrTableCell_TP_DK.Text.Length;
                xrTableCell_TP_DK.Text = xrTableCell_TP_DK.Text.Substring(0, Length - 2);
            }
        }

        private void SubBand_HTND_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_HTND"].Value + "" == "")
            {
                SubBand_HTND.Visible = false;
            }
            else
            {
                SubBand_HTND.Visible = true;
            }
        }

        private void SubBand_HTND_DuKhuyet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_HTND_DK"].Value + "" == "")
            {
                SubBand_HTND_DuKhuyet.Visible = false;
            }
            else
            {
                SubBand_HTND_DuKhuyet.Visible = true;
            }
        }

        private void SubBand_ThuKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_ThuKy"].Value + "" == "")
            {
                SubBand_ThuKy.Visible = false;
            }
            else
            {
                SubBand_ThuKy.Visible = true;
            }
        }

        private void SubBand_TK_DuKhuyet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_ThuKy_DK"].Value + "" == "")
            {
                SubBand_TK_DuKhuyet.Visible = false;
            }
            else
            {
                SubBand_TK_DuKhuyet.Visible = true;
            }
        }

        private void SubBand_KSV_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_KSV"].Value + "" == "")
            {
                SubBand_KSV.Visible = false;
            }
            else
            {
                SubBand_KSV.Visible = true;
            }
        }

        private void SubBand_KSV_DuKhuyet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Parameters["prm_KSV_DK"].Value + "" == "")
            {
                SubBand_KSV_DuKhuyet.Visible = false;
            }
            else
            {
                SubBand_KSV_DuKhuyet.Visible = true;
            }
        }
    }
}
