﻿using Module.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WEB.GSTP
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(Object sender, EventArgs e)
        {
            // Code that runs on application startup
            Application.Set("OnlineNow", 0);
            DevExpress.XtraReports.Web.ASPxWebDocumentViewer.StaticInitialize();
        }

        void Application_End(Object sender, EventArgs e)
        {
            // Code that runs on application shutdown
        }

        void Application_Error(Object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
        }

        void Session_Start(Object sender, EventArgs e)
        {

        }

        void Session_End(Object sender, EventArgs e)
        {
            // Xóa file js của bản đồ khi hết phiên làm việc
            string strFileName_js = "mapdata_" + Session[ENUM_SESSION.SESSION_DONVIID] + ".js";
            string path_js = Server.MapPath("~/GSTP/") + strFileName_js;
            FileInfo oF_js = new FileInfo(path_js);
            if (oF_js.Exists)// Nếu file đã tồn tại
            {
                File.Delete(path_js);
            }
            //
            int so = int.Parse(Application.Get("OnlineNow").ToString());
            if (so > 0) so--;
            else
                so = 0;
            Application.Set("OnlineNow", so);
        }

    }
}