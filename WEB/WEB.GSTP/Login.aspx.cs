﻿using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP
{
    public partial class Login : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();
                try
                {
                    List<DM_TRANGTINH> lstHT = dt.DM_TRANGTINH.Where(x => x.MATRANG == "hotrologingscm").ToList();
                    if (lstHT.Count > 0)
                    {
                        lsthotro.Text = lstHT[0].NOIDUNG;
                    }
                }catch(Exception ex) { }
            }
        }
        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() == "")
                {
                     lttMsg.Text = "Chưa nhập mã truy cập !";
                    //  Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Chưa nhập mã truy cập!");
                    txtUserName.Focus();
                    return;
                }
                string strPass = Cls_Comon.MD5Encrypt(txtPass.Text);
                decimal HieuLuc = 1, IsAccDoMain = 1, IsNotAccDoMain=0;
                List<QT_NGUOISUDUNG> lst = null;
                QT_NGUOISUDUNG oT = null;
                string user_name = txtUserName.Text.Trim().ToLower();
                if (chkIsDomain.Checked)//Sử dụng account domain
                {
                    lst = dt.QT_NGUOISUDUNG.Where(x => x.USERNAME.ToLower() == user_name
                                                && x.HIEULUC == HieuLuc
                                                && x.ISACCDOMAIN == IsAccDoMain).ToList<QT_NGUOISUDUNG>();
                    if (lst.Count == 0)
                    {
                        lttMsg.Text = "Mã truy cập hoặc mật khẩu không đúng !";
                        // Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Mã truy cập hoặc mật khẩu không đúng !");
                        txtUserName.Focus();
                        return;
                    }
                    bool status = IsAuthenticated(txtUserName.Text, txtPass.Text);
                    if (status)
                    {
                        oT = lst[0];
                    }
                    else
                    {
                        lttMsg.Text = "Mã truy cập hoặc mật khẩu không đúng !";
                        txtUserName.Focus();
                        //Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Mã truy cập hoặc mật khẩu không đúng !");
                        return;
                    }
                }
                else
                {
                    lst = dt.QT_NGUOISUDUNG.Where(x => x.USERNAME.ToLower() == user_name
                                                && x.PASSWORD == strPass
                                                && x.HIEULUC == HieuLuc
                                                && x.ISACCDOMAIN == IsNotAccDoMain).ToList<QT_NGUOISUDUNG>();
                    if (lst.Count == 0)
                    {
                        lttMsg.Text = "Mã truy cập hoặc mật khẩu không đúng !";
                        txtUserName.Focus();
                        //Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Mã truy cập hoặc mật khẩu không đúng !");
                        return;
                    }
                    else
                    {
                        oT = lst[0];
                    }
                }
                if (oT != null)
                {
                    Session[ENUM_SESSION.SESSION_USERID] = oT.ID;
                    Session[ENUM_SESSION.SESSION_NHOMNSDID] = oT.NHOMNSDID;
                    Session[ENUM_SESSION.SESSION_USERNAME] = oT.USERNAME;
                    Session[ENUM_SESSION.SESSION_DONVIID] = oT.DONVIID;
                    Session[ENUM_SESSION.SESSION_USERTEN] = oT.HOTEN;
                    Session[ENUM_SESSION.SESSION_LOAIUSER] = oT.LOAIUSER;
                    Session[ENUM_SESSION.SESSION_MACANBO] = oT.MACANBO;
                    Session[ENUM_SESSION.SESSION_CANBOID] = oT.CANBOID;
                    Session[ENUM_SESSION.SESSION_PHONGBANID] = oT.PHONGBANID;
                    Session[ENUM_LOAIAN.AN_HINHSU] = oT.IDAHINHSU;
                    Session[ENUM_LOAIAN.AN_DANSU] = oT.IDANDANSU;
                    Session[ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI] = oT.IDANKDTM;
                    Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] = oT.IDANHNGD;
                    Session[ENUM_LOAIAN.AN_LAODONG] = oT.IDANLAODONG;
                    Session[ENUM_LOAIAN.AN_HANHCHINH] = oT.IDANHANHCHINH;
                    Session[ENUM_LOAIAN.AN_PHASAN] = oT.IDANPHASAN;
                    Session[ENUM_LOAIAN.BPXLHC] = oT.IDBPXLHC;
                    Session[ENUM_LOAIAN.AN_GDTTT] = oT.IDGDTTT;
                    Session[ENUM_LOAIAN.AN_THA] = oT.IDTHA;
                    DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == oT.DONVIID).FirstOrDefault();
                    Session[ENUM_SESSION.SESSION_MADONVI] = oTA.MA;
                    Session[ENUM_SESSION.SESSION_TENDONVI] = oTA.TEN;
                    Session["CAP_XET_XU"] = oTA.LOAITOA;
                    Session[ENUM_SESSION.SESSION_ISPHANLOAIDON] =(String.IsNullOrEmpty(oT.ISPHANLOAIDON+""))?0:Convert.ToInt16( oT.ISPHANLOAIDON.ToString());
                    if(oTA.HANHCHINHID !=null)
                    {
                        DM_HANHCHINH oHC = dt.DM_HANHCHINH.Where(x => x.ID == oTA.HANHCHINHID).FirstOrDefault();
                        try
                        {
                            if (oHC.LOAI == 2)
                            {
                                Session[ENUM_SESSION.SESSION_TINH_ID] = oHC.CAPCHAID;
                                Session[ENUM_SESSION.SESSION_QUAN_ID] = oHC.ID;
                            }
                            else
                            {
                                Session[ENUM_SESSION.SESSION_TINH_ID] = oHC.ID;
                                Session[ENUM_SESSION.SESSION_QUAN_ID] = 0;
                            }
                        }
                        catch(Exception ex)
                        {
                            Session[ENUM_SESSION.SESSION_TINH_ID] = 0;
                            Session[ENUM_SESSION.SESSION_QUAN_ID] = 0;
                        }
                    }
                    else
                    {
                        Session[ENUM_SESSION.SESSION_TINH_ID] = 0;
                        Session[ENUM_SESSION.SESSION_QUAN_ID] = 0;
                    }
                    Session.Timeout = 240;
                    CultureInfo ci = new CultureInfo("vi-VN");
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;
                    try
                    {
                        //Số người online
                        int so = int.Parse(Application.Get("OnlineNow").ToString());
                        so++;
                        Application.Set("OnlineNow", so);
                        //Lượt truy cập
                        List<QT_THONGSO> lstLTC = dt.QT_THONGSO.ToList();
                        QT_THONGSO objLTC;
                        if (lstLTC.Count > 0)
                        {
                            objLTC = lstLTC[0];
                            if (objLTC.LUOTTRUYCAP != null) objLTC.LUOTTRUYCAP += 1;
                            else objLTC.LUOTTRUYCAP = 1;
                        }
                        else
                        {
                            objLTC = new QT_THONGSO();
                            objLTC.LUOTTRUYCAP = 1;
                            dt.QT_THONGSO.Add(objLTC);
                        }
                        dt.SaveChanges();
                    }catch(Exception ex) { }
                    Response.Redirect("Launcher.aspx");
                }            
            }
            catch(Exception ex)
            {
                lttMsg.Text = ex.ToString();
                //Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", ex.ToString());
            }
        }
        public bool IsAuthenticated(string usr, string pwd)
        {
            bool authenticated = false;
            string srvr = global::System.Configuration.ConfigurationManager.AppSettings["LDAPSVR"] +"";
            try
            {
                DirectoryEntry entry = new DirectoryEntry(srvr, usr, pwd);
                object nativeObject = entry.NativeObject;
                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                //not authenticated; reason why is in cex
            }
            catch (Exception ex)
            {
                //not authenticated due to some other exception [this is optional]
            }
            return authenticated;
        }
    }
}