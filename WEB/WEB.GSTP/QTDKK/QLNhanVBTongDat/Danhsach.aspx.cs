﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Module.Common;
using System.Globalization;
using System.Data;
using DAL.DKK;
using BL.DonKK;
using BL.DonKK.DanhMuc;

using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.Danhmuc;

namespace WEB.GSTP.QTDKK.QLNhanVBTongDat
{
    public partial class Danhsach : System.Web.UI.Page
    {
        DKKContextContainer dt = new DKKContextContainer();
        GSTPContext gsdt = new GSTPContext();
        public Decimal UserID = 0;
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            UserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (UserID > 0)
            {
                if (!IsPostBack)
                {
                    
                    LoadDrop();
                    LoadDanhSach();
                }
                dropTucachToTung.Attributes.Add("onchange", "change_control();");
                dropTrangThai.Attributes.Add("onchange", "change_control();");
            }
            else Response.Redirect("/login.aspx");
        }
        void LoadDrop()
        {
            String MaTuCachKoSD = "$TGTTDS_01$TGTTDS_02$TGTTDS_08$TGTTDS_09$";
            dropTucachToTung.Items.Clear();
            string temp = "";
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.TUCACHTGTTDS);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                dropTucachToTung.Items.Add(new ListItem("Nguyên đơn", "NGUYENDON"));
                dropTucachToTung.Items.Add(new ListItem("Bị đơn", "BIDON"));
                dropTucachToTung.Items.Add(new ListItem("Người có quyền và NVLQ", "QUYENNVLQ"));
                dropTucachToTung.Items.Add(new ListItem("Người được ủy quyền", "TGTTDS_01"));
                foreach (DataRow row in tbl.Rows)
                {
                    temp = "$" + row["MA"].ToString() + "$";
                    if (!MaTuCachKoSD.Contains(temp))
                        dropTucachToTung.Items.Add(new ListItem(row["Ten"].ToString(), row["MA"].ToString()));
                }
            }
            //--------------------
            dropTrangThai.Items.Clear();
            dropTrangThai.Items.Add(new ListItem("---Tất cả---", "4" ));
            dropTrangThai.Items.Add(new ListItem("Đang giao dịch", "1"));
            dropTrangThai.Items.Add(new ListItem("Đợi duyệt", "0"));
            dropTrangThai.Items.Add(new ListItem("Ngừng giao dịch", "2"));
            dropTrangThai.Items.Add(new ListItem("Tạm dừng", "3"));
        }
        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            LoadDanhSach();
        }
        //cmdIn_Click
        protected void cmdIn_Click(object sender, EventArgs e)
        {
            DOnKK_User_DKNhanVB_BL oBL = new DOnKK_User_DKNhanVB_BL();
            Literal Table_Str_Totals = new Literal();
            string Data = "<div style=\"mso-element: footer\"id=\"f1\"><w:sdt sdtdocpart=\"t\" docparttype=\"Page Numbers (Bottom of Page)\" docpartunique=\"t\" id=\"644013658\"><p class=MsoFooter align=right style=\"text-align:right\"><!--[if supportFields]><span style=\"mso-element:field-begin\" ></span><span style=\"mso-spacerun:yes\"></span>PAGE<span style=\"mso-spacerun:yes\"></span>\\* MERGEFORMAT<span style=\"mso-element:field-separator\"></span><![endif]--><span style=\"mso-no-proof:yes;display:none\">2</span><!--[if supportFields]><span style=\"mso-no-proof:yes\"><span style=\"mso-element:field-end\"></span></span><![endif]--><w:sdtPr></w:sdtPr></p></w:sdt><p class=\"MsoFooter\" align=\"right\" style=\"text-align: right;\"><o:p></o:p></p></div>";

            int countData = 0;
            foreach (RepeaterItem item in rpt.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl("chkChon");
                if (checkBox.Checked)
                {
                    countData++;
                }
            }
            int dem = 0;
            //lấy toàn bộ data để in
            foreach (RepeaterItem item in rpt.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl("chkChon");
                if (checkBox.Checked)
                {
                    Data+=LoadReport_DKNhanVBTongDat(checkBox.ToolTip);
                    dem++;
                    if (dem< countData)
                    {
                        Data += "<span style=\"font-size:12.0pt;font-family:''Times New Roman'',serif; mso-fareast-font-family:''Times New Roman''; mso-fareast-theme-font:minor-fareast; mso-ansi-language:EN-US;mso-fareast-language:EN-US; mso-bidi-language:AR-SA\"><br clear=all style = \"mso-special-character:line-break;page-break-before:always\"></span>";
                    }
                    
                }
            }
            string TenFile = "THONGBAONHANVBTONGDAT";
            string KieuFile = ".docx";
            var cacheKey = Guid.NewGuid().ToString("N");
            Context.Cache.Insert(key: cacheKey, value: Data, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadWord.aspx?cacheKey=" + cacheKey + "&FileName=" + TenFile + "&Extension=" + KieuFile + "';", true);
        }

        private string  LoadReport_DKNhanVBTongDat(string groupdk)
        {
            string Data = "";
            DOnKK_User_DKNhanVB_BL obj = new DOnKK_User_DKNhanVB_BL();
            DataTable tbl = obj.GetInfoDangKyNhanVB(groupdk);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                
                    Data += "<table cellpadding=\"1\" cellspacing=\"1\" style=\"font-family:times New Roman; font-size:14pt; text-align:center; border-collapse:collapse;\"><tr><th colspan=\"3\" style=\"text-align:center; vertical-align:top; font-size:14pt;\">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</th></tr><tr><th colspan=\"3\" style=\"width: 240px; text-align:center;\"><span>Độc lập - Tự do - Hạnh phúc</span></th></tr><tr>		<table><tr><td style=\"color:white\">11111</td><td style=\"border-top:1px solid black;width:200px\"></td><td style=\"color:white\">sads</td></tr></table></tr><tr style=\"\"><td></td><td></td></tr><tr style=\"height: 20px; \"></tr><tr><th style=\"width: 1250px\" colspan = \"2\"> ĐĂNG KÝ NHẬN VĂN BẢN, THÔNG BÁO TRỰC TUYẾN </th></tr></table>";

                
                DataRow root = tbl.Rows[0];
                string TenToaAn = root["TenToaAn"] + "";
                Data += "<p style=\"width: 1250px;text-align:center;font-size:16pt;\"><b>Kính gửi: " + TenToaAn+ "</b></p>";

                string TenDuongSu = root["UserHoTen"] + "";
                string CMND = root["UserCMND"] + "";
                string NgaySinh = root["UserNgaySinh"] + "" == "" ? "" : Convert.ToDateTime(root["UserNgaySinh"]).ToString("dd/MM/yyyy");
                string HKThuongTru = root["UserHKThuongTru"] + "" == "" ? root["HKThuongTru_TinhHuyen"] + "" : root["UserHKThuongTru"] + ", " + root["HKThuongTru_TinhHuyen"];
                string HKTamTru = root["UserHKTamTru"] + "" == "" ? root["HKTamTru_TinhHuyen"] + "" : root["UserHKTamTru"] + ", " + root["HKTamTru_TinhHuyen"];
                string DienThoai = root["UserDienThoai"] + "";
                string Email = root["UserEmail"] + "";
                string NgayDangKy = root["NgayTao"] + "" == "" ? "" : Convert.ToDateTime(root["NgayTao"]).ToString("dd/MM/yyyy");
                string MaDangKy = root["MaDangKy"] + "";
                Data+= "<table cellpadding=\"1\" cellspacing=\"1\" style=\"font-family:times New Roman; font-size:14pt; text-align:center; border-collapse:collapse;\">" +
                    "<tr>" +

                        "<td style=\"text-align:left;line-height:28px;\"> Tên tôi là: " + TenDuongSu + "</td>" +
                        "<td style=\"text-align:left;line-height:28px;\"> Ngày sinh: " + NgaySinh + " </td> </tr>" +
                    "<tr>" +
                        "<td style=\"text-align:left;line-height:28px;\" colspan=\"1\"> Số CMND / Thẻ căn cước/ Hộ chiếu: " + CMND + "</td>" +
                    "</tr>" +
                    "<tr>" +

                        "<td style=\"text-align:left;line-height:28px;\"> Điện thoại: " + DienThoai + " </td>" +
                        "<td style=\"text-align:left;line-height:28px;\">Email: " + Email + " </td>" +
                    "</tr>" +
                    "<tr>" +

                        "<td style=\"text-align:left;line-height:28px;\">Ngày đăng ký: " + NgayDangKy + "</td>" +
                        "<td style=\"text-align:left;line-height:28px;\"> Mã đăng ký: " + MaDangKy + "</td>" +
                    "</tr>" +
                    "</table>";
                Data += "<p style=\"font-size:15pt;\"><b>Yêu cầu tòa án gửi cho tôi các văn bản, thông báo mới của các vụ việc sau:</b></p>";
                // fill danh sách yêu cầu
                Data += "<table cellpadding=\"1\" cellspacing=\"1\" style=\"font-family:times New Roman; font-size:14pt; text-align:center; border-collapse:collapse;margin-top:20px\">" +
                       "<tr>" +
                           "<th  style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-bottom:10pt;padding-right:4pt;padding-left:4pt\"> STT</th>" +
                           "<th style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-bottom:10pt;padding-right:4pt;padding-left:4pt\"> Mã vụ việc</th>" +
                           "<th style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-bottom:10pt;padding-right:4pt;padding-left:4pt\">Tên vụ việc</th>" +
                           "<th style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-bottom:10pt;padding-right:4pt;padding-left:4pt\">Loại vụ việc</th>" +
                           "<th style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-bottom:10pt;padding-right:4pt;padding-left:4pt\">Tư cách tố tụng</th>" +
                       "</tr>";
                foreach (DataRow row in tbl.Rows)
                {
                    
                    string STT = row["stt"] + "";
                    string MaVuViec = row["MaVuViec"] + "";
                    string TenVuViec = row["TenVuViec"] + "";
                    string LoaiVuViec = row["LoaiVuAn"] + "";
                    string TuCachToTung = row["TenTuCachToTung"] + "";
                    string TrangThaiDuyet = row["TrangThaiDuyet"] + "";
                   Data+= "<tr style=\"font-size:13pt; padding:5pt\">" +
                           "<td style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-top:0pt;padding-bottom:20pt;padding-right:4pt;padding-left:4pt\"> " + STT + "</td>" +
                           "<td style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-top:0pt;padding-bottom:20pt;padding-right:4pt;padding-left:4pt\">" + MaVuViec + "</td>" +
                           "<td style=\"text-align: left; vertical-align: middle; border: 0.1pt solid #000000;padding-top:0pt;padding-bottom:20pt;padding-right:4pt;padding-left:4pt\">" + TenVuViec + "</td>" +
                           "<td style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-top:0pt;padding-bottom:20pt;padding-right:4pt;padding-left:4pt\">" + LoaiVuViec + "</td>" +
                           "<td style=\"text-align: center; vertical-align: middle; border: 0.1pt solid #000000;padding-top:0pt;padding-bottom:20pt;padding-right:4pt;padding-left:4pt\">" + TuCachToTung + "</td>" +
                       "</tr>";

                }
                Data += "</table>";
            }
            return Data;
        }

        protected void chkChonALl_CheckedChanged(Object sender, EventArgs args)
        {
            CheckBox checkBoxAll = (CheckBox)sender;
            if (checkBoxAll.Checked)
            {
                foreach (RepeaterItem item in rpt.Items)
                {
                    CheckBox checkBox = (CheckBox)item.FindControl("chkChon");
                    checkBox.Checked = true;
                }
            }
            else
            {
                foreach (RepeaterItem item in rpt.Items)
                {
                    CheckBox checkBox = (CheckBox)item.FindControl("chkChon");
                    checkBox.Checked = false;
                }
            }
        }


        //------------------------
        #region Load DS
        public void LoadDanhSach()
        {
            string tucachtt = dropTucachToTung.SelectedValue;
            string vuviec = txtVuViec.Text.Trim();
            decimal toa_an_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]); //(string.IsNullOrEmpty(hddToaAnID.Value)) ? 0 : Convert.ToInt32(hddToaAnID.Value);
            string textsearch = txtVuViec.Text.Trim();
            int trangthai =Convert.ToInt16( dropTrangThai.SelectedValue);
            int page_size = 20;
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            DateTime? dFrom = DateTime.Now;
            DateTime? dTo = DateTime.Now;
            dFrom = (String.IsNullOrEmpty(txtTuNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dTo = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            string tenduongsu = txtTenDuongSu.Text.Trim();
            string cmnd = txtCMND.Text.Trim();
            string madangky = txtMaDangKy.Text.Trim();
            string quanHePhapLuat = txtQuanHePhapLuat.Text;
            string email = txtEmail.Text;
            string sodienthoai = txtSoDienThoai.Text;
            
            DataTable tbl = null;
            DOnKK_User_DKNhanVB_BL oBL = new DOnKK_User_DKNhanVB_BL();

            tbl = oBL.GetListDKByToaAn(toa_an_id, tucachtt, tenduongsu, cmnd, madangky ,textsearch, trangthai, dFrom, dTo, quanHePhapLuat, email, sodienthoai, pageindex, page_size);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                int count_all = Convert.ToInt32(tbl.Rows[0]["CountAll"] + "");
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(20)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion

                rpt.DataSource = tbl;
                rpt.DataBind();
                pnPagingTop.Visible = pnPagingBottom.Visible = rpt.Visible = true;
            }
            else
                pnPagingTop.Visible = pnPagingBottom.Visible = rpt.Visible = false;
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rv = (DataRowView)e.Item.DataItem;

                int trangthai = Convert.ToInt16(rv["TrangThai"] + "");
                decimal Id = Convert.ToDecimal(rv["ID"] + "");
                LinkButton lbtDuyet = (LinkButton)e.Item.FindControl("lbtDuyet");
                LinkButton lbtMo = (LinkButton)e.Item.FindControl("lbtMo");
                LinkButton lbtDong = (LinkButton)e.Item.FindControl("lbtDong");
                //0:chưa dc duoc duyet, 1: da duyet
                switch (trangthai)
                {
                    //chờ duyệt
                    case 0:
                        lbtDuyet.Visible = true;
                        lbtMo.Visible = lbtDong.Visible = false;

                        break;
                        //Đang giao dịch
                    case 1:
                        lbtDong.Visible = true;
                        lbtMo.Visible = lbtDuyet.Visible = false;
                        break;
                        //Ngừng giao dịch
                    case 2:
                        DONKK_USER_DKNHANVB obj = dt.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.ID == Id);
                        
                        lbtMo.Visible = true;
                        lbtDong.Visible = lbtDuyet.Visible = false;
                        if (obj.LOAITACDONG.HasValue)
                        {
                            if (obj.LOAITACDONG.Value == 1)
                            {
                                //do người dân ddongs
                                lbtMo.Visible = false;
                                lbtDong.Visible = lbtDuyet.Visible = false;

                            }
                        }
                        break;
                    //Tạm dừng giao dịch
                    case 3:
                        lbtDuyet.Visible = false;
                        lbtMo.Visible = lbtDong.Visible = true;
                        break;
                }
            }
        }

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Decimal DangKyID = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "lbtDuyet":
                    Duyet(DangKyID);
                    break;
                case "lbtMo":
                    Mo(DangKyID);
                    break;
                case "lbtDong":
                    Dong(DangKyID);
                    break;
            }
        }
        void Dong(Decimal DangKyID)
        {
            Cls_Comon.CallFunctionJS(this, this.GetType(), "popup_LyDoVbTongDat('"+DangKyID+"');");
            //DONKK_USER_DKNHANVB obj = dt.DONKK_USER_DKNHANVB.Where(x => x.ID == DangKyID).Single<DONKK_USER_DKNHANVB>();
            //if (obj != null)
            //{
            //    obj.TRANGTHAI = 2;
            //    dt.SaveChanges();
            //    string msg = "Văn bản ngừng giao dịch thành công!";                
            //    string msg = "Văn bản ngừng giao dịch thành công!";                
            //    Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo",msg);
            //    hddPageIndex.Value = "1";
            //    LoadDanhSach();
            //}
        }

        public void Mo(Decimal DangKyID)
        {
            //chuyển sang trạng thái đang giao dịch
            DONKK_USER_DKNHANVB obj = dt.DONKK_USER_DKNHANVB.Where(x => x.ID == DangKyID).Single<DONKK_USER_DKNHANVB>();
            if (obj != null)
            {
                obj.TRANGTHAI = 1;
                dt.SaveChanges();
                string msg = "Văn bản được mở thành công!";
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", msg);
                hddPageIndex.Value = "1";
                LoadDanhSach();
            }
        }

        public void Duyet(Decimal DangKyID)
        {
            //chuyển sang trạng thái đang giao dịch
            DONKK_USER_DKNHANVB obj = dt.DONKK_USER_DKNHANVB.Where(x => x.ID == DangKyID).Single<DONKK_USER_DKNHANVB>();
            if (obj != null)
            {
                obj.TRANGTHAI = 1;
                dt.SaveChanges();
                string msg = "Văn bản được duyệt thành công!";
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", msg);
                hddPageIndex.Value = "1";
                LoadDanhSach();
            }
        }

        #region phan trang
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            LoadDanhSach();
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            LoadDanhSach();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            LoadDanhSach();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            LoadDanhSach();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            //dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            LoadDanhSach();
        }

        #endregion

        #endregion

    }
}