﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL.GSTP.ADS;
using BL.GSTP.DONGHEP;

namespace WEB.GSTP.QLAN.DONGHEP
{
    public partial class DonGhep : System.Web.UI.UserControl
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public decimal DONID;
        public Decimal LOAIANID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }
        public void hiddenbtnThemMoi()
        {
            lbtThemdonmoi.Visible = false;
        }
        private void LoadGrid()
        {
            decimal vDonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            string keyDonID = "DONGHEP.DONID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            string keyLoaiAnId = "DONGHEP.LOAIANID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            DONID = Convert.ToDecimal(Session[keyDonID]);
            LOAIANID = Convert.ToDecimal(Session[keyLoaiAnId]);
            int page_size = 10,
                pageindex = Convert.ToInt32(hddPageIndex.Value);
            DONGHEP_BL oBL = new DONGHEP_BL();
            DataTable oDT = oBL.GetDonGhep(vDonViID, DONID, LOAIANID);

            if (oDT != null && oDT.Rows.Count > 0)
            {
                var count_all = Convert.ToInt32(oDT.Rows.Count);
                if (dgList.CurrentPageIndex > (Convert.ToInt32(hddTotalPage.Value) - 1))
                {
                    dgList.CurrentPageIndex = 0;
                }
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, page_size).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                         lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                         lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }

            dgList.DataSource = oDT;
            dgList.DataBind();
        }
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rowView = (DataRowView)e.Item.DataItem;

                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                if (!string.IsNullOrEmpty(e.Item.Cells[2].Text) && e.Item.Cells[2].Text!="&nbsp;")
                {
                    //thêm bằng tay
                    lbtXoa.Text = "Hủy ghép";
                    lbtXoa.OnClientClick = "return confirm('Bạn thực sự muốn hủy ghép bản ghi này? ');";
                }
                else
                {
                    //thêm từ dkk
                    lbtXoa.Text = "Xóa";
                    lbtXoa.OnClientClick = "return confirm('Bạn thực sự muốn xóa bản ghi này? ');";
                }
            }
        }
        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            //string keyDonID = "DONGHEP.DONID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            string keyLoaiAnId = "DONGHEP.LOAIANID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            LOAIANID = Convert.ToDecimal(Session[keyLoaiAnId]);
            switch (e.CommandName)
            {
                case "Sua":
                    Cls_Comon.CallFunctionJS(this, this.GetType(), "popup_edit_DONGHEP(" + LOAIANID + "," + ND_id + ")");
                    break;
                case "Xoa":
                    DON_CHITIET obj = dt.DON_CHITIET.FirstOrDefault(s => s.ID == ND_id);
                    int count = 0;
                    if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_DANSU))
                    {

                        count = dt.ADS_DON_XULY.Count(s => s.DON_CHITIETID== obj.ID) ;
                    }
                    else if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_HONNHAN_GIADINH))
                    {
                        count = dt.AHN_DON_XULY.Count(s => s.DON_CHITIETID == obj.ID);
                    }
                    else if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_KINHDOANH_THUONGMAI))
                    {
                        count = dt.AKT_DON_XULY.Count(s => s.DON_CHITIETID == obj.ID);
                    }
                    else if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_LAODONG))
                    {
                        count = dt.ALD_DON_XULY.Count(s => s.DON_CHITIETID == obj.ID);
                    }
                    else if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_HANHCHINH))
                    {
                        count = dt.AHC_DON_XULY.Count(s => s.DON_CHITIETID == obj.ID);
                    }
                    else if (LOAIANID == int.Parse(ENUM_LOAIVUVIEC_NUMBER.AN_PHASAN))
                    {
                        count = dt.APS_DON_XULY.Count(s => s.DON_CHITIETID == obj.ID);
                    }

                    if (count>0)
                    {
                        LtrThongBao.Text = "Đơn đã được giải quyết, không thể xóa";
                    }


                    List<DON_CHITIET_DUONGSU> lstDonChiTietDuongSu = dt.DON_CHITIET_DUONGSU.Where(s => s.DONID == obj.ID).ToList();

                    dt.DON_CHITIET.Remove(obj);
                    dt.DON_CHITIET_DUONGSU.RemoveRange(lstDonChiTietDuongSu);
                    dt.SaveChanges();
                    LtrThongBao.Text = "Xóa thành công";
                    lbtimkiem_Click(null, null);
                    break;
            }

        }
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid();
        }

        protected void lbThemMoiDon_Click(object sender, EventArgs e)
        {
            //string keyDonID = "DONGHEP.DONID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            string keyLoaiAnId = "DONGHEP.LOAIANID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            LOAIANID = Convert.ToDecimal(Session[keyLoaiAnId]);
            Cls_Comon.CallFunctionJS(this, this.GetType(), "popup_edit_DONGHEP(" + LOAIANID + ")");
        } 
            #region "Phân trang"

            protected void lbTBack_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            LoadGrid();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            LoadGrid();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            LoadGrid();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            LoadGrid();
        }

        #endregion
    }
}