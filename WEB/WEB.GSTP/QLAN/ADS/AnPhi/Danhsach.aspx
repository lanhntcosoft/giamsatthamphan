﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master"
    AutoEventWireup="true" CodeBehind="Danhsach.aspx.cs" Inherits="WEB.GSTP.QLAN.ADS.AnPhi.Danhsach" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../UI/js/Common.js"></script>
    <style>
        .lable_td {
            width: 160px;
        }

        .checkbox {
            width: 100%;
        }

            .checkbox label {
                margin-left: 5px;
            }
    </style>
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddNgayGQYC" Value="" runat="server" />
    <asp:HiddenField ID="hddShowNopAnPhi" runat="server" Value="0" />
    <div class="box">
        <div class="box_nd">
            <asp:Panel ID="pnThongTinAnPhi" runat="server">
                <div class="boxchung">
                    <h4 class="tleboxchung">Thông tin án phí</h4>
                    <div class="boder" style="padding: 10px;">
                        <table class="table1">
                            <tr>
                                <td>Miễn án phí</td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkNopAnPhi" runat="server" Text="" AutoPostBack="True" OnCheckedChanged="chkNopAnPhi_CheckedChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td class="lable_td">Giá trị tranh chấp</td>
                                <td style="width: 175px;">
                                    <asp:TextBox ID="txtGiaTriTranhChap" runat="server" Enabled="false"
                                        CssClass="user align_right" Width="150px" onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="width: 120px;">Mức giảm án phí</td>
                                <td>
                                    <asp:TextBox ID="txtMucGiamAnPhi" runat="server" CssClass="user align_right" Width="185px" Enabled="false"
                                        onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Án phí</td>
                                <td>
                                    <asp:TextBox ID="txtAnPhi" runat="server" CssClass="user align_right" Width="150px" Enabled="false" onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td>Tạm ứng án phí</td>
                                <td>
                                    <asp:TextBox ID="txtTamUngAnPhi" runat="server" CssClass="user align_right" Enabled="false" Width="185px" onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Hạn nộp</td>
                                <td>
                                    <asp:TextBox ID="txtHanNopAnPhi" runat="server" CssClass="user" Enabled="false" Width="150px" MaxLength="10"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cl1" runat="server" TargetControlID="txtHanNopAnPhi" Format="dd/MM/yyyy" Enabled="true" />
                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtHanNopAnPhi" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                                <td>Số ngày gia hạn</td>
                                <td>
                                    <asp:TextBox ID="txtSoNgayGiaHan" runat="server" CssClass="user align_right" Width="185px" Enabled="false" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <div class="boxchung">
                <h4 class="tleboxchung">Thông tin biên lai án phí</h4>
                <div class="boder" style="padding: 10px;">
                    <div id="zone_nopanphi">
                        <table class="table1">
                            <tr>
                                <td class="lable_td">Ngày nộp tạm ứng án phí<span class="must_input">(*)</span></td>
                                <td style="width: 175px;">
                                    <asp:TextBox ID="txtNgayNopAnPhi" runat="server" CssClass="user" Width="150px" MaxLength="10" AutoPostBack="true" OnTextChanged="txtNgayNopAnPhi_TextChanged"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtNgayNopAnPhi" Format="dd/MM/yyyy" Enabled="true" />
                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server" TargetControlID="txtNgayNopAnPhi" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                                <td style="width: 120px;">Người nhận</td>
                                <td>
                                    <asp:DropDownList ID="ddlNguoiNhan" CssClass="chosen-select" runat="server" Width="193px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="lable_td">Số biên lai<span class="must_input">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtSoBienLai" runat="server" CssClass="user" Width="150px" MaxLength="10"></asp:TextBox>
                                </td>
                                <td>Ngày nộp biên lai<span class="must_input">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtNgayNopBL" runat="server" CssClass="user" Width="185px" MaxLength="10"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtNgayNopBL" Format="dd/MM/yyyy" Enabled="true" />
                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="txtNgayNopBL" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="lable_td">Số thông báo</td>
                                <td>
                                    <asp:TextBox ID="txtSoThongBao" runat="server" CssClass="user" Width="150px" MaxLength="10"></asp:TextBox>
                                </td>
                                <td>Ngày thông báo</td>
                                <td>
                                    <asp:TextBox ID="txtNgayThongBao" runat="server" CssClass="user" Width="185px" MaxLength="10"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtNgayThongBao" Format="dd/MM/yyyy" Enabled="true" />
                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtNgayThongBao" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="truong" style="text-align: center;">
                <asp:Button ID="cmdCapNhat" runat="server"
                    CssClass="buttoninput" Text="Lưu" OnClick="cmdCapNhat_Click" OnClientClick=" return Validate();" />
                <asp:Button ID="cmdxoa" runat="server"
                    CssClass="buttoninput" Text="Xóa" OnClick="cmdxoa_Click"/>
                <asp:Button ID="cmdThuLy" runat="server" Visible="false"
                    CssClass="buttoninput" Text="Thụ lý" OnClick="cmdThuLy_Click" />
            </div>
            <asp:Label runat="server" ID="lbtthongbao" ForeColor="Red" Style="margin-left: 15px;"></asp:Label>
        </div>
    </div>

    <script>
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }

        }

        function Validate() {
            //-------------------check zone nop an phi----------------------
            var chkNopAnPhi = document.getElementById('<%= chkNopAnPhi.ClientID %>');
            if (!chkNopAnPhi.checked) {
                var txtNgayNopAnPhi = document.getElementById('<%=txtNgayNopAnPhi.ClientID %>');
                if (!CheckDateTimeControl(txtNgayNopAnPhi, "Ngày nộp tạm ứng án phí"))
                    return false;
                var ddlNguoiNhan = document.getElementById('<%=ddlNguoiNhan.ClientID%>');
                var val = ddlNguoiNhan.options[ddlNguoiNhan.selectedIndex].value;
                if (val == 0) {
                    alert('Chưa chọn cán bộ nhận biên lai. Hãy chọn lại!');
                    ddlNguoiNhan.focus();
                    return false;
                }
                var txtSoBienLai = document.getElementById('<%=txtSoBienLai.ClientID %>');
                if (!Common_CheckTextBox(txtSoBienLai, "Số biên lai"))
                    return false;

                var txtNgayNopBL = document.getElementById('<%=txtNgayNopBL.ClientID %>');
                if (!CheckDateTimeControl(txtNgayNopBL, "Ngày nộp biên lai"))
                    return false;
                if (!SoSanhDate(txtNgayNopBL, txtNgayNopAnPhi)) {
                    alert('Ngày nộp biên lai phải lớn hơn hoặc bằng ngày nộp tạm ứng án phí. Hãy nhập lại!');
                    return false;
                }
            }
            return true;
        }
    </script>
</asp:Content>
