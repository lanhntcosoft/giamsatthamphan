﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.Danhmuc;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.DKK;
using BL.GSTP.ADS;

namespace WEB.GSTP.QLAN.ADS.Sotham
{
    public partial class BananSotham : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        DKKContextContainer dkk = new DKKContextContainer();
        CultureInfo cul = new CultureInfo("vi-VN");
        public Decimal DSID = 0;
        public bool GetNumber(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch { return false; }
        }
        public string GetTextDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return (Convert.ToDateTime(obj).ToString("dd/MM/yyyy", cul));
            }
            catch { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DSID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_DANSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_DANSU]);
                hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                LoadCombobox();
                LoadBoLuat();
                hddDonID.Value = Session[ENUM_LOAIAN.AN_DANSU] + "" == "" ? "0" : Session[ENUM_LOAIAN.AN_DANSU] + "";
                if (hddDonID.Value == "0") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Danhsach.aspx");
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                CheckQuyen(DONID);
                LoadBanAnInfo(DONID);
                LoadDieuLuat();
                LoadAnPhi();
                LoadTGTT();
                GetTrangThaiBanDauDONKK_USER_DKNHANVB(DONID);
            }
            LoadFile();
        }
        void CheckQuyen(decimal ID)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdLuatUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdAnphi, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdTGTT, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdHuyBanAn, oPer.CAPNHAT);

            Cls_Comon.SetLinkButton(lkChoiceDieuLuat, oPer.CAPNHAT);
            //-----------------------
            ADS_DON oT = dt.ADS_DON.Where(x => x.ID == ID).FirstOrDefault();
            if (oT != null)
            {
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    lstErr.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLuatUpdate, false);
                    Cls_Comon.SetButton(cmdAnphi, false);
                    Cls_Comon.SetButton(cmdTGTT, false);
                    Cls_Comon.SetButton(cmdHuyBanAn, false);
                    hddShowCommand.Value = "False";
                    //ko cho thêm điều luật
                    lkChoiceDieuLuat.Visible = false;
                    return;
                }
            }
            //-----------------------
            List<ADS_SOTHAM_THULY> lstCount = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == ID).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin thụ lý sơ thẩm !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                //ko cho thêm điều luật
                lkChoiceDieuLuat.Visible = false;
                return;
            }
            //Kiểm tra xem đã có quyết định đưa vụ việc ra xét xử hay không
            decimal IDLQD = 0;
            DM_QD_LOAI oLQD = dt.DM_QD_LOAI.Where(x => x.MA == "DVARXX").FirstOrDefault();
            if (oLQD != null) IDLQD = oLQD.ID;
            ADS_SOTHAM_QUYETDINH QDST = dt.ADS_SOTHAM_QUYETDINH.Where(x => x.DONID == ID && x.LOAIQDID == IDLQD).OrderByDescending(x => x.NGAYQD).FirstOrDefault();
            if (QDST == null)
            {
                lstErr.Text = "Chưa cập nhật quyết định đưa vụ án ra xét xử !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                //ko cho thêm điều luật
                lkChoiceDieuLuat.Visible = false;
                return;
            }
            else
            {
                hddNgayQDXX.Value = QDST.NGAYQD + "" == "" ? "" : ((DateTime)QDST.NGAYQD).ToString("dd/MM/yyyy");
            }
            //--Kiểm tra đã phân công thẩm phán chủ tọa
            List<ADS_SOTHAM_HDXX> lstTPCT = dt.ADS_SOTHAM_HDXX.Where(x => x.DONID == ID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin hội đồng xét xử !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }

            ADS_SOTHAM_KHANGCAO kc = dt.ADS_SOTHAM_KHANGCAO.Where(x => x.DONID == ID).FirstOrDefault();
            if (kc != null)
            {
                lstErr.Text = "Vụ việc đã có kháng cáo. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            ADS_SOTHAM_KHANGNGHI kn = dt.ADS_SOTHAM_KHANGNGHI.Where(x => x.DONID == ID).FirstOrDefault();
            if (kn != null)
            {
                lstErr.Text = "Vụ việc đã có kháng nghị. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new ADS_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
            if (Result != "")
            {
                lstErr.Text = Result;
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }

            //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            //int counttk = oCBDT.Rows.Count;
            //if (counttk > 0)
            //{
            //    //là thư k
            //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
            //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
            //    //int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
            //    int countItem = dt.ADS_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == ID && s.MAVAITRO== "VTTP_GIAIQUYETSOTHAM");
            //    if (countItem > 0)
            //    {
            //        //được gán 
            //    }
            //    else
            //    {
            //        //không được gán
            //        StrMsg = "Người dùng không được sửa đổi thông tin của vụ việc do không được phân công giải quyết.";
            //        lstErr.Text = StrMsg;
            //        Cls_Comon.SetButton(cmdUpdate, false);
            //        Cls_Comon.SetButton(cmdLuatUpdate, false);
            //        Cls_Comon.SetButton(cmdAnphi, false);
            //        Cls_Comon.SetButton(cmdTGTT, false);
            //        Cls_Comon.SetButton(cmdHuyBanAn, false);
            //        hddShowCommand.Value = "False";
            //        return;
            //    }
            //}
        }
        private void LoadAnPhi()
        {
            decimal DONID = Convert.ToDecimal(hddDonID.Value);
            ADS_SOTHAM_BL oBL = new ADS_SOTHAM_BL();
            dgAnPhi.DataSource = oBL.ADS_SOTHAM_BANAN_ANPHI_GET(DONID);
            dgAnPhi.DataBind();
            foreach (DataGridItem oItem in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                if (chkMien.Checked)
                {
                    txtAnphi.Text = "";
                    txtAnphi.Enabled = false;
                }
                else
                {
                    txtAnphi.Enabled = true;
                }
            }
        }
        private void LoadTGTT()
        {
            decimal DONID = Convert.ToDecimal(hddDonID.Value);
            ADS_SOTHAM_BL oBL = new ADS_SOTHAM_BL();
            DataTable dtTGTT = oBL.ADS_SOTHAM_BANAN_TGTT_GET(DONID);
            if (dtTGTT != null && dtTGTT.Rows.Count > 0)
            {
                hddTGTTRowLastIndex.Value = dtTGTT.Rows.Count + "";
            }
            dgTGTT.DataSource = dtTGTT;
            dgTGTT.DataBind();
        }
        protected void dgTGTT_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtNgayTGTT = (TextBox)e.Item.FindControl("txtNgayTGTT");
                if (hddTGTTRowLastIndex.Value == (dgTGTT.Items.Count + 1) + "")
                {
                    txtNgayTGTT.Attributes.Add("onfocus", "myFunctionFocus();");
                }
            }
        }
        private void LoadBoLuat()
        {
            ddlBoLuat.DataSource = dt.DM_BOLUAT.Where(x => x.LOAI == ENUM_LOAIVUVIEC.AN_DANSU && x.HIEULUC == 1 && x.LOAI != "01").OrderByDescending(y => y.NGAYBANHANH).ToList();
            ddlBoLuat.DataTextField = "TENBOLUAT";
            ddlBoLuat.DataValueField = "ID";
            ddlBoLuat.DataBind();
            LoadDieuKhoan();
        }
        private void LoadDieuKhoan()
        {
            //Load điều luật áp dụng gồm cả điều - khoản - điểm: tên tội danh
            if(ddlBoLuat.Items.Count > 0)
            {
                decimal IDBL = Convert.ToDecimal(ddlBoLuat.SelectedValue);
                ddlDieukhoan.DataSource = dt.DM_BOLUAT_TOIDANH
                    .Where(x => x.LUATID == IDBL && x.LOAI != 1)
                    .Select(x => new
                    {
                        ID = x.ID,
                        TENTOIDANH = x.DIEU + " - " + x.KHOAN + " - " + x.DIEM + ": " + x.TENTOIDANH
                    })
                    .ToList();

                ddlDieukhoan.DataTextField = "TENTOIDANH";
                ddlDieukhoan.DataValueField = "ID";
                ddlDieukhoan.DataBind();
            }
        }
        private void LoadFile()
        {
            decimal DONID = Convert.ToDecimal(hddDonID.Value);
            dgFile.DataSource = dt.ADS_SOTHAM_BANAN_FILE.Where(x => x.DONID == DONID).ToList();
            dgFile.DataBind();
            int ma_gd = 0;
            ADS_DON oT = dt.ADS_DON.Where(x => x.ID == DONID).FirstOrDefault();
            if (oT != null)
                ma_gd = (int)oT.MAGIAIDOAN;
            foreach (DataGridItem item in dgFile.Items)
            {
                LinkButton lbtXoa = (LinkButton)item.FindControl("lbtXoa");
                if (ma_gd == (int)ENUM_GIAIDOANVUAN.PHUCTHAM || ma_gd == (int)ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    Cls_Comon.SetLinkButton(lbtXoa, false);
                }
                if (hddShowCommand.Value == "False")
                {
                    lbtXoa.Visible = false;
                }
            }
        }
        private void LoadDieuLuat()
        {
            decimal DONID = Convert.ToDecimal(hddDonID.Value);
            ADS_SOTHAM_BL oBL = new ADS_SOTHAM_BL();
            dgDieuLuat.DataSource = oBL.ADS_SOTHAM_BANAN_DIEULUAT_GET(DONID);
            dgDieuLuat.DataBind();
        }
        private void LoadBanAnInfo(decimal DonID)
        {
            List<ADS_SOTHAM_BANAN> lst = dt.ADS_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (lst.Count > 0)
            {
                
                ADS_SOTHAM_BANAN oT = lst[0];hddBanAnID.Value = oT.ID.ToString();
                txtSobanan.Text = oT.SOBANAN;
                ddlLoaiQuanhe.SelectedValue = oT.LOAIQUANHE.ToString();
                txtQuanhephapluat_name(oT);
                if (oT.QHPLTKID != null)
                    ddlQHPLTK.SelectedValue = oT.QHPLTKID.ToString();
                if (oT.NGAYMOPHIENTOA != null)
                {
                    txtNgaymophientoa.Text = ((DateTime)oT.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                }
                else
                {
                    txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                }
                if (oT.NGAYTUYENAN != null) txtNgaytuyenan.Text = ((DateTime)oT.NGAYTUYENAN).ToString("dd/MM/yyyy", cul);
                if (oT.NGAYHIEULUC != null) txtNgayhieuluc.Text = ((DateTime)oT.NGAYHIEULUC).ToString("dd/MM/yyyy", cul);

                if (oT.ISVKSTHAMGIA != null)
                    rdbVKSThamgia.SelectedValue = oT.ISVKSTHAMGIA.ToString();

                rdbAnle.SelectedValue = oT.APDUNGANLE.ToString();
                ddlYeutonuocngoai.SelectedValue = oT.YEUTONUOCNGOAI.ToString();


                rdVuAnQuaHan.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISQUAHAN + "")) ? "0" : oT.TK_ISQUAHAN.ToString();
                rdNNChuQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_CHUQUAN + "")) ? "0" : oT.TK_QUAHAN_CHUQUAN.ToString();
                rdNNKhachQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_KHACHQUAN + "")) ? "0" : oT.TK_QUAHAN_KHACHQUAN.ToString();
                txtSoQDTraiPLBiHuy.Text = oT.TK_SOQDTRAIPLBIHUY + "";
                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;
                LoadFile();
            }
            else
            {
                ADS_DON oDon = dt.ADS_DON.Where(x => x.ID == DonID).FirstOrDefault();
                if (oDon != null)
                {
                    if (oDon.LOAIQUANHE != null)
                        ddlLoaiQuanhe.SelectedValue = oDon.LOAIQUANHE.ToString();
                    if (oDon.YEUTONUOCNGOAI != null)
                        ddlYeutonuocngoai.SelectedValue = oDon.YEUTONUOCNGOAI.ToString();
                }
                // QHPL Thống kê mặc định selected theo thụ lý
                ADS_SOTHAM_THULY tl = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == DonID).OrderByDescending(x => x.NGAYTHULY).FirstOrDefault();
                if (tl != null)
                {
                    if (tl.QHPLTKID != null)
                        ddlQHPLTK.SelectedValue = tl.QHPLTKID.ToString();
                    
                    txtQuanhephapluat_name(tl);
                }
                txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            }

        }
        private bool CheckValid()
        {
            if (txtQuanhephapluat.Text.Trim().Length >= 500)
            {
                lstErr.Text = "Quan hệ pháp luật nhập quá dài.";
                txtQuanhephapluat.Focus();
                return false;
            }
            if (txtQuanhephapluat.Text == null || txtQuanhephapluat.Text == "")
            {
                lstErr.Text = "Chưa nhập quan hệ pháp luật.";
                txtQuanhephapluat.Focus();
                return false;
            }

            if (ddlQHPLTK.SelectedValue == "0")
            {
                lstErr.Text = "Chưa chọn quan hệ pháp luật dùng cho thống kê!";
                return false;
            }
            decimal IDChitieuTK = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
            if (dt.DM_QHPL_TK.Where(x => x.PARENT_ID == IDChitieuTK).ToList().Count > 0)
            {
                lstErr.Text = "Quan hệ pháp luật dùng cho thống kê chỉ được chọn mã con, bạn hãy chọn lại !";
                return false;
            }
            if (txtSobanan.Text == "")
            {
                lstErr.Text = "Chưa nhập số bản án";
                txtSobanan.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaymophientoa.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày mở phiên tòa hoặc không hợp lệ !";
                txtNgaymophientoa.Focus();
                return false;
            }
            DateTime dNgayMoPhienToa = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayMoPhienToa > DateTime.Now)
            {
                lstErr.Text = "Ngày mở phiên tòa không được lớn hơn ngày hiện tại !";
                txtNgaymophientoa.Focus();
                return false;
            }
            DateTime NgayQDXX = hddNgayQDXX.Value == "" ? DateTime.MinValue : DateTime.Parse(hddNgayQDXX.Value, cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayMoPhienToa < NgayQDXX)
            {
                lstErr.Text = "Ngày mở phiên tòa không được nhỏ hơn ngày thụ lý " + hddNgayQDXX.Value;
                txtNgaymophientoa.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaytuyenan.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày tuyên án hoặc không hợp lệ !";
                return false;
            }
            DateTime dNgayTA = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayTA > DateTime.Now)
            {
                lstErr.Text = "Ngày tuyên án không được lớn hơn ngày hiện tại !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (dNgayTA < dNgayMoPhienToa)
            {
                lstErr.Text = "Ngày tuyên án phải lớn hơn ngày mở phiên tòa !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (txtNgayhieuluc.Text.Trim() != "")
            {
                if (Cls_Comon.IsValidDate(txtNgayhieuluc.Text) == false)
                {
                    lstErr.Text = "Chưa nhập ngày hiệu lực hoặc không hợp lệ !";
                    return false;
                }
                DateTime dNgayHieuLuc = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayHieuLuc < dNgayTA)
                {
                    lstErr.Text = "Ngày hiệu lực phải lớn hơn ngày tuyên án !";
                    txtNgayhieuluc.Focus();
                    return false;
                }
            }
            if (rdbAnle.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Áp dụng án lệ ?\"";
                return false;
            }
            if (rdbVKSThamgia.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Có VKS tham gia ?\"";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Vụ án quá hạn luật định ?\"";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "1")
            {
                if (rdNNChuQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn \"Nguyên nhân chủ quan ?\"";
                    return false;
                }
                if (rdNNKhachQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn \"Nguyên nhân khách quan ?\"";
                    return false;
                }
            }
            //----------------------------
            string so = txtSobanan.Text;
            if (!String.IsNullOrEmpty(txtNgaytuyenan.Text))
            {
                DateTime ngayBA = DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "ADS", so, ngayBA);
                if (CheckID > 0)
                {
                    Decimal CurrBanAnId = (string.IsNullOrEmpty(hddBanAnID.Value)) ? 0 : Convert.ToDecimal(hddBanAnID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "ADS", ngayBA).ToString();
                    if (CheckID != CurrBanAnId)
                    {
                        strMsg = "Số bản án " + txtSobanan.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSobanan.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSobanan.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        private void LoadCombobox()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBY2GROUPNAME(ENUM_DANHMUC.QUANHEPL_YEUCAU, ENUM_DANHMUC.QUANHEPL_TRANHCHAP);
            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();
            //Load QHPL Thống kê.
            ddlQHPLTK.DataSource = dt.DM_QHPL_TK.Where(x => x.STYLES == ENUM_QHPLTK.DANSU && x.ENABLE == 1).OrderBy(y => y.ARRTHUTU).ToList();
            ddlQHPLTK.DataTextField = "CASE_NAME";
            ddlQHPLTK.DataValueField = "ID";
            ddlQHPLTK.DataBind();
            ddlQHPLTK.Items.Insert(0, new ListItem("--Chọn QHPL dùng thống kê--", "0"));
        }
        protected void ddlQuanhephapluat_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal IDQHPL = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue);
            DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
            DM_DATAGROUP oGroup = dt.DM_DATAGROUP.Where(x => x.ID == obj.GROUPID).FirstOrDefault();
            if (oGroup.MA == ENUM_DANHMUC.QUANHEPL_TRANHCHAP)
                ddlLoaiQuanhe.SelectedValue = "1";
            else
                ddlLoaiQuanhe.SelectedValue = "2";
            Cls_Comon.SetFocus(this, this.GetType(), ddlQHPLTK.ClientID);
        }
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                List<ADS_SOTHAM_BANAN> lst = dt.ADS_SOTHAM_BANAN.Where(x => x.DONID == DONID).ToList();
                ADS_SOTHAM_BANAN oND;
                if (lst.Count == 0)
                    oND = new ADS_SOTHAM_BANAN();
                else
                    oND = lst[0];
                oND.DONID = DONID;
                oND.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                oND.LOAIQUANHE = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue);

                oND.QUANHEPHAPLUATID = null;
                oND.QUANHEPHAPLUAT_NAME = txtQuanhephapluat.Text;
                //renameTenvuviec(txtQuanhephapluat.Text, DONID);

                oND.QHPLTKID = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
                oND.SOBANAN = txtSobanan.Text;
                oND.NGAYMOPHIENTOA = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYTUYENAN = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYHIEULUC = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                oND.ISVKSTHAMGIA = rdbVKSThamgia.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbVKSThamgia.SelectedValue);
                oND.APDUNGANLE = rdbAnle.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbAnle.SelectedValue);
                oND.YEUTONUOCNGOAI = Convert.ToDecimal(ddlYeutonuocngoai.SelectedValue);

                oND.TK_ISQUAHAN = rdVuAnQuaHan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVuAnQuaHan.SelectedValue);
                oND.TK_QUAHAN_CHUQUAN = rdNNChuQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNChuQuan.SelectedValue);
                oND.TK_QUAHAN_KHACHQUAN = rdNNKhachQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNKhachQuan.SelectedValue);
                oND.TK_SOQDTRAIPLBIHUY = (String.IsNullOrEmpty(txtSoQDTraiPLBiHuy.Text + "")) ? 0 : Convert.ToDecimal(txtSoQDTraiPLBiHuy.Text);

                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;

                if (lst.Count == 0)
                {
                    oND.NGAYTAO = DateTime.Now;
                    oND.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ADS_SOTHAM_BANAN.Add(oND);
                    dt.SaveChanges();
                }
                else
                {
                    oND.NGAYSUA = DateTime.Now;
                    oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.SaveChanges();
                }
                TamNgungDONKK_USER_DKNHANVB(DONID);
                lstErr.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lstErr.Text = "Lỗi: " + ex.Message;
            }
        }

        private void GetTrangThaiBanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ADS_DON oDon = dt.ADS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_DANSU && s.TRANGTHAI == 1);
            if (obj != null)
            {

                ttBanDauDONKK_USER_DKNHANVB.Value = obj.TRANGTHAI.Value.ToString();
            }
        }
        private void SetTrangThaibanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ADS_DON oDon = dt.ADS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_DANSU && s.TRANGTHAI == 3);
            if (obj != null)
            {

                obj.TRANGTHAI = Convert.ToDecimal(ttBanDauDONKK_USER_DKNHANVB.Value);
                dkk.SaveChanges();
            }
        }
        private void TamNgungDONKK_USER_DKNHANVB(decimal DONID)
        {
            ADS_DON oDon = dt.ADS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_DANSU && s.TRANGTHAI == 1);
            if (obj != null)
            {

                obj.TRANGTHAI = 3;
                dkk.SaveChanges();
            }
        }


        protected void cmdHuyBanAn_Click(object sender, EventArgs e)
        {
            // Xóa thông tin bản án
            decimal DonID = Convert.ToDecimal(hddDonID.Value);
            //reset_TENVUVIEC(DonID);
            List<ADS_SOTHAM_BANAN> banans = dt.ADS_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (banans.Count > 0)
            {
                dt.ADS_SOTHAM_BANAN.RemoveRange(banans);
            }
            // Xóa thông tin file đính kèm
            List<ADS_SOTHAM_BANAN_FILE> files = dt.ADS_SOTHAM_BANAN_FILE.Where(x => x.DONID == DonID).ToList();
            if (files.Count > 0)
            {
                dt.ADS_SOTHAM_BANAN_FILE.RemoveRange(files);
            }
            // Xóa điều luật áp dụng, tội danh
            List<ADS_SOTHAM_BANAN_DIEULUAT> dieuLuats = dt.ADS_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DonID).ToList();
            if (dieuLuats.Count > 0)
            {
                dt.ADS_SOTHAM_BANAN_DIEULUAT.RemoveRange(dieuLuats);
            }
            // Xóa thông tin án phí
            List<ADS_SOTHAM_BANAN_ANPHI> anPhis = dt.ADS_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DonID).ToList();
            if (anPhis.Count > 0)
            {
                dt.ADS_SOTHAM_BANAN_ANPHI.RemoveRange(anPhis);
            }
            // Xóa thông tin người tham gia tố tụng
            List<ADS_SOTHAM_BANAN_TGTT> tGTTs = dt.ADS_SOTHAM_BANAN_TGTT.Where(x => x.DONID == DonID).ToList();
            if (tGTTs.Count > 0)
            {
                dt.ADS_SOTHAM_BANAN_TGTT.RemoveRange(tGTTs);
            }
            // Xóa file tống đạt bản án
            decimal BieuMauID = 0;
            DM_BIEUMAU bm = dt.DM_BIEUMAU.Where(x => x.MABM == "52-DS").FirstOrDefault();
            if (bm != null)
            {
                BieuMauID = bm.ID;
            }

            ADS_FILE file = dt.ADS_FILE.Where(x => x.DONID == DonID && x.BIEUMAUID == BieuMauID && x.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM).FirstOrDefault();
            if (file != null)
            {
                dt.ADS_FILE.Remove(file);
            }
            SetTrangThaibanDauDONKK_USER_DKNHANVB(DonID);
            dt.SaveChanges();
            ResetControl();
            lstErr.Text = "Xóa bản án thành công!";
        }
        private void ResetControl()
        {
            decimal DonID = Convert.ToDecimal(hddDonID.Value);
            txtQuanhephapluat.Text = getQHPL_NAME_THULY();
            ddlQuanhephapluat.SelectedIndex = 0;
            LoadBanAnInfo(DonID);
            txtSobanan.Text = "";
            txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtNgaytuyenan.Text = "";
            txtNgayhieuluc.Text = "";
            rdbAnle.ClearSelection();
            rdbVKSThamgia.ClearSelection();
            txtSoQDTraiPLBiHuy.Text = "";
            rdVuAnQuaHan.ClearSelection();
            rdNNChuQuan.ClearSelection();
            rdNNKhachQuan.ClearSelection();
            LoadFile();
            ddlBoLuat.SelectedIndex = 0;
            ddlDieukhoan.SelectedIndex = 0;
            //txtChuong.Text = "";
            //txtDiem.Text = "";
            //txtDieu.Text = "";
            //txtKhoan.Text = "";
            LoadDieuLuat();
            LoadAnPhi();
            LoadTGTT();
        }
        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (AsyncFileUpLoad.HasFile)
            {
                string strFileName = AsyncFileUpLoad.FileName;
                string path = Server.MapPath("~/TempUpload/") + strFileName;
                AsyncFileUpLoad.SaveAs(path);
                path = path.Replace("\\", "/");
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                ADS_SOTHAM_BANAN_FILE oTF = new ADS_SOTHAM_BANAN_FILE();
                // Lưu hồ sơ bản án
                string strFilePath = "";
                if (chkKySo.Checked)
                {
                    string[] arr = path.Split('/');
                    strFilePath = arr[arr.Length - 1];
                    strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                }
                else
                    strFilePath = path.Replace("/", "\\");
                byte[] buff = null;
                using (FileStream fs = File.OpenRead(strFilePath))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(strFilePath);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ADS_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                // Dùng cho tống đạt văn bản
                if (strFilePath.ToLower().Contains("52-ds"))
                {
                    ADS_DON don = dt.ADS_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    if (don != null)
                    {
                        UploadFileID(don, "52-DS", oTF);
                    }
                }
                File.Delete(strFilePath);
                //path = path.Replace("\\", "/");
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
                LoadFile();
            }
        }
        protected void cmdThemFileTL_Click(object sender, EventArgs e)
        {
            SaveFile_KySo();
            LoadFile();
        }
        void SaveFile_KySo()
        {
            string folder_upload = "/TempUpload/";
            string file_kyso = hddFilePath.Value;
            if (!String.IsNullOrEmpty(hddFilePath.Value))
            {
                String[] arr = file_kyso.Split('/');
                string file_name = arr[arr.Length - 1] + "";

                String file_path = Path.Combine(Server.MapPath(folder_upload), file_name);
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                ADS_SOTHAM_BANAN_FILE oTF = new ADS_SOTHAM_BANAN_FILE();

                byte[] buff = null;
                using (FileStream fs = File.OpenRead(file_path))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(file_path);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ADS_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                //xoa file
                File.Delete(file_path);
            }
        }
        protected void dgFile_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal DonID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_DANSU] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new ADS_CHUYEN_NHAN_AN_BL().Check_NhanAn(DonID, StrMsg);
                    if (Result != "")
                    {
                        lstErr.Text = Result;
                        return;
                    }
                    ADS_SOTHAM_BANAN_FILE oT = dt.ADS_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.ADS_SOTHAM_BANAN_FILE.Remove(oT);
                    dt.SaveChanges();
                    LoadFile();
                    break;
                case "Download":
                    ADS_SOTHAM_BANAN_FILE oND = dt.ADS_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                    }
                    break;
            }

        }
        protected void cmdLuatUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ////String Diem = txtDiem.Text.Trim();
                ////string Khoan = txtKhoan.Text.Trim();
                ////String Dieu = txtDieu.Text.Trim();
                ////string Chuong = txtChuong.Text.Trim();
                //decimal luatid = Convert.ToDecimal(ddlBoLuat.SelectedValue);

                //DM_BOLUAT_TOIDANH_BL objBL = new DM_BOLUAT_TOIDANH_BL();
                //int Loai_bo_luat = Convert.ToInt32(ENUM_LOAIVUVIEC.AN_DANSU);
                //DataTable tbl = objBL.GetByDK2(luatid, Loai_bo_luat, 1);
                //if (tbl != null && tbl.Rows.Count > 0)
                //{
                //    //if (tbl.Rows.Count == 1)
                //    //{
                //    //    DataRow row = tbl.Rows[0];
                //    //    SaveToiDanh(row);
                //    //}
                //    DataRow row = tbl.Rows[0];
                //    SaveToiDanh(row);
                //}
                decimal DonID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_DANSU] + "");
                decimal toidanhid = Convert.ToDecimal(ddlDieukhoan.SelectedValue);

                String[] arrToiDanh = null;
                DM_BOLUAT_TOIDANH objTD = dt.DM_BOLUAT_TOIDANH.Where(x => x.ID == toidanhid).Single();
                arrToiDanh = objTD.ARRSAPXEP.Split('/');
                if (arrToiDanh != null && arrToiDanh.Length > 0)
                {
                    decimal ChuongID = Convert.ToDecimal(arrToiDanh[0] + "");
                    foreach (String strToiDanhID in arrToiDanh)
                    {
                        if (strToiDanhID.Length > 0 && strToiDanhID != ChuongID.ToString())
                        {
                            toidanhid = Convert.ToDecimal(strToiDanhID);
                            InsertToiDanh(DonID, toidanhid);
                        }
                    }
                    dt.SaveChanges();
                    LoadDieuLuat();
                    lstMsgDieuluat.Text = "Lưu thành công!";
                }
            }
            catch (Exception ex)
            {
                lstMsgDieuluat.Text = "Lỗi: " + ex.Message;
            }
        }
        //thêm mới điều khoản
        void InsertToiDanh(Decimal DonID, Decimal toidanhid)
        {
            bool isupdate = false;
            DM_BOLUAT_TOIDANH objTD = null;
            ADS_SOTHAM_BANAN_DIEULUAT obj = new ADS_SOTHAM_BANAN_DIEULUAT();
            try
            {
                obj = dt.ADS_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DonID && x.DIEULUATID == toidanhid)
                        .Single<ADS_SOTHAM_BANAN_DIEULUAT>();
                if (obj != null)
                    isupdate = true;
                else
                    obj = new ADS_SOTHAM_BANAN_DIEULUAT();
            }
            catch (Exception ex) { obj = new ADS_SOTHAM_BANAN_DIEULUAT(); }
            if (!isupdate)
            {
                obj.DONID = DonID;
                //obj.DIEULUATID = Convert.ToDecimal(dropBoLuat.SelectedValue);
                obj.DIEULUATID = Convert.ToDecimal(toidanhid);

                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";

                objTD = dt.DM_BOLUAT_TOIDANH.Where(x => x.ID == toidanhid).Single();

                dt.ADS_SOTHAM_BANAN_DIEULUAT.Add(obj);
            }
        }

        void SaveToiDanh(DataRow rowToiDanh)
        {
            decimal boluatid = Convert.ToDecimal(ddlBoLuat.SelectedValue);
            //decimal toidanhid = Convert.ToDecimal(rowToiDanh["ID"] + "");
            decimal toidanhid = Convert.ToDecimal(ddlDieukhoan.SelectedValue);
            decimal DONID = Convert.ToDecimal(hddDonID.Value);

            Boolean isupdate = false;

            //kiem tra toi danh vua tim duoc da co trong DB chua
            if (dt.ADS_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DONID && x.DIEULUATID == toidanhid).ToList().Count > 0)
            {
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Đã tồn tại điều luật này trong danh sách !");
                return;
            }

            ADS_SOTHAM_BANAN_DIEULUAT obj = new ADS_SOTHAM_BANAN_DIEULUAT();
            //try
            //{
            //    obj = dt.ADS_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DONID && x.DIEULUATID== toidanhid).Single<ADS_SOTHAM_BANAN_DIEULUAT>();
            //    if (obj != null)
            //        isupdate = true;
            //    else
            //        obj = new ADS_SOTHAM_BANAN_DIEULUAT();
            //}
            //catch (Exception ex) { obj = new ADS_SOTHAM_BANAN_DIEULUAT(); }
            if (!isupdate)
            {
                obj.DONID = DONID;
                obj.DIEULUATID = toidanhid;
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.ADS_SOTHAM_BANAN_DIEULUAT.Add(obj);
                dt.SaveChanges();
            }
            LoadDieuLuat();
            lstMsgDieuluat.Text = "Lưu thành công!";
        }
        protected void ddlBoLuat_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDieuKhoan();
            Cls_Comon.SetFocus(this, this.GetType(), ddlDieukhoan.ClientID);
        }
        protected void dgDieuLuat_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false || cmdUpdate.Enabled == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal DonID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_DANSU] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new ADS_CHUYEN_NHAN_AN_BL().Check_NhanAn(DonID, StrMsg);
                    if (Result != "")
                    {
                        lstErr.Text = Result;
                        return;
                    }
                    ADS_SOTHAM_BANAN_DIEULUAT oT = dt.ADS_SOTHAM_BANAN_DIEULUAT.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.ADS_SOTHAM_BANAN_DIEULUAT.Remove(oT);
                    dt.SaveChanges();
                    LoadDieuLuat();
                    break;

            }

        }
        protected void cmdAnphi_Click(object sender, EventArgs e)
        {
            try
            {
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                bool IsNgayNhanBanAn = false;
                if (dgAnPhi.Items.Count > 0)
                {
                    foreach (DataGridItem oItem in dgAnPhi.Items)
                    {
                        TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                        if (txtNgaynhanbanan.Text != "")
                        {
                            if (!ValidateNgayNhanBanAn(txtNgaynhanbanan, false))
                            {
                                return;
                            }
                            IsNgayNhanBanAn = true;
                        }
                    }
                }
                //if (IsNgayNhanBanAn)// có nhập ngày nhận bản án
                //{
                //    if (!CheckIsHaveBanAnFile(DONID, false))
                //    {
                //        return;
                //    }
                //}
                string ListDuongsuTructuyenID = "", TenBieuMau = "", TenVuViec = "";
                decimal BieuMauID = 0, ToaAnID = 0, MaGiaiDoan = 0;
                ADS_DON adsDon = dt.ADS_DON.Where(x => x.ID == DONID).FirstOrDefault();
                if (adsDon != null)
                {
                    TenVuViec = adsDon.TENVUVIEC;
                    ToaAnID = (decimal)adsDon.TOAANID;
                    MaGiaiDoan = (decimal)adsDon.MAGIAIDOAN;
                }
                #region Phần phục vụ cho việc tống đạt
                if (IsNgayNhanBanAn)// có nhập ngày nhận bản án
                {
                    DM_BIEUMAU bm = dt.DM_BIEUMAU.Where(x => x.MABM.Trim().ToLower() == "52-ds").FirstOrDefault();
                    if (bm != null)
                    {
                        BieuMauID = bm.ID;
                        TenBieuMau = bm.TENBM;
                    }
                    // Kiểm tra đơn nhận theo hình thức Trực tuyến hay không
                    // Nếu là trực tuyến thì Gửi sang hệ thống ĐKK
                    bool IsTrucTuyen = false;
                    if (adsDon != null)
                    {
                        if (adsDon.HINHTHUCNHANDON == 3)//trực tuyến
                        {
                            IsTrucTuyen = true;
                        }
                    }
                    // Lấy danh sách các đương sự sẽ tống đạt trực tuyến
                    if (IsTrucTuyen)
                    {
                        ADS_DON_DUONGSU dsTrucTuyen = dt.ADS_DON_DUONGSU.Where(x => x.DONID == DONID && x.ISDAIDIEN == 1 && x.TUCACHTOTUNG_MA == ENUM_DANSU_TUCACHTOTUNG.NGUYENDON).FirstOrDefault();
                        if (dsTrucTuyen != null)
                        {
                            ListDuongsuTructuyenID = dsTrucTuyen.ID + "";
                        }
                    }

                    List<DONKK_USER_DKNHANVB> lstDKVB = dkk.DONKK_USER_DKNHANVB.Where(x => x.VUVIECID == DONID && x.MALOAIVUVIEC == ENUM_LOAIAN.AN_DANSU && x.TRANGTHAI == 1).ToList();
                    if (lstDKVB != null && lstDKVB.Count > 0)
                    {
                        foreach (DONKK_USER_DKNHANVB oDK in lstDKVB)
                        {
                            if (ListDuongsuTructuyenID == "")
                                ListDuongsuTructuyenID = oDK.DUONGSUID + "";
                            else
                                ListDuongsuTructuyenID += "," + oDK.DUONGSUID + "";
                        }
                    }
                }
                #endregion
                ListDuongsuTructuyenID = "," + ListDuongsuTructuyenID + ",";
                // Lưu thông tin
                foreach (DataGridItem oItem in dgAnPhi.Items)
                {
                    string strID = oItem.Cells[0].Text;
                    decimal DSID = Convert.ToDecimal(strID);
                    CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                    TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                    CheckBox chkThamgia = (CheckBox)oItem.FindControl("chkThamgia");
                    TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                    #region Lưu thông tin án phí
                    List<ADS_SOTHAM_BANAN_ANPHI> lst = dt.ADS_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DONID && x.DUONGSU == DSID).ToList();
                    if (lst.Count == 0)
                    {
                        ADS_SOTHAM_BANAN_ANPHI oT = new ADS_SOTHAM_BANAN_ANPHI();
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYTAO = DateTime.Now;
                        oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.ADS_SOTHAM_BANAN_ANPHI.Add(oT);
                    }
                    else
                    {
                        ADS_SOTHAM_BANAN_ANPHI oT = lst[0];
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYSUA = DateTime.Now;
                        oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    }
                    dt.SaveChanges();
                    #endregion
                    #region Tống đạt luôn tại đây
                    // có nhập ngày nhận bản án mới thực hiện
                    if (IsNgayNhanBanAn)// có nhập ngày nhận bản án
                    {
                        ADS_FILE objFile = dt.ADS_FILE.Where(x => x.DONID == DONID && x.TOAANID == ToaAnID && x.MAGIAIDOAN == MaGiaiDoan && x.BIEUMAUID == BieuMauID).FirstOrDefault();
                        decimal TongDatID = 0, FileBanAnID = 0;
                        ADS_FILE FileBA = dt.ADS_FILE.Where(x => x.TOAANID == ToaAnID && x.MAGIAIDOAN == MaGiaiDoan && x.DONID == DONID && x.BIEUMAUID == BieuMauID).FirstOrDefault();
                        if (FileBA != null)
                        {
                            FileBanAnID = FileBA.ID;
                        }
                        ADS_TONGDAT td = dt.ADS_TONGDAT.Where(x => x.DONID == DONID && x.BIEUMAUID == BieuMauID && x.TOAANID == ToaAnID && x.FILEID == FileBanAnID).FirstOrDefault();
                        if (td == null)
                        {
                            td = new ADS_TONGDAT();
                            td.DONID = DONID;
                            td.BIEUMAUID = BieuMauID;
                            td.TOAANID = ToaAnID;
                            td.NGAYTAO = DateTime.Now;
                            td.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                            td.TRANGTHAI = 0;// Chưa đăng lên Cổng thông tin điện tử
                            if (FileBA != null)
                            {
                                td.FILEID = FileBanAnID;
                                td.NOIDUNGFILE = FileBA.NOIDUNG;
                                td.TENFILE = FileBA.TENFILE;
                                td.KIEUFILE = FileBA.KIEUFILE;
                            }
                            dt.ADS_TONGDAT.Add(td);
                            dt.SaveChanges();
                        }
                        TongDatID = td.ID;
                        // Gửi sang hệ thống ĐKK
                        if (ListDuongsuTructuyenID.Contains("," + DSID + ","))
                        {
                            bool IsNew = false;
                            DONKK_DON_VBTONGDAT dkkvbtd = dkk.DONKK_DON_VBTONGDAT.Where(x => x.BIEUMAUID == BieuMauID && x.MALOAIVUVIEC == ENUM_LOAIAN.AN_DANSU && x.VUVIECID == DONID && x.DUONGSUID == DSID).FirstOrDefault();
                            if (dkkvbtd == null)
                            {
                                IsNew = true;
                                dkkvbtd = new DONKK_DON_VBTONGDAT();
                                dkkvbtd.NGAYGUI = DateTime.Now;
                                dkkvbtd.NGAYNHANTONGDAT = DateTime.Now;
                            }
                            dkkvbtd.TENVANBAN = TenBieuMau;
                            dkkvbtd.TENVUVIEC = TenVuViec;
                            dkkvbtd.TOAANID = ToaAnID;
                            dkkvbtd.VUVIECID = DONID;
                            dkkvbtd.DUONGSUID = DSID;
                            dkkvbtd.MALOAIVUVIEC = ENUM_LOAIAN.AN_DANSU;
                            dkkvbtd.BIEUMAUID = BieuMauID;
                            if (objFile != null)
                            {
                                dkkvbtd.NOIDUNGFILE = objFile.NOIDUNG;
                                dkkvbtd.TENFILE = objFile.TENFILE;
                                dkkvbtd.LOAIFILE = objFile.KIEUFILE;
                            }
                            if (IsNew)
                            {
                                dkk.DONKK_DON_VBTONGDAT.Add(dkkvbtd);
                            }
                            dkk.SaveChanges();
                            TongDatDoiTuong(TongDatID, DSID, 1, txtNgaynhanbanan.Text);
                        }
                        else // Tống đạt các đối tượng khác
                        {
                            TongDatDoiTuong(TongDatID, DSID, 0, txtNgaynhanbanan.Text);
                        }
                    }
                    #endregion
                }
                lstMsgAnphi.Text = "Lưu thành công !";
            }
            catch (Exception ex)
            {
                lstMsgAnphi.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void chkThamgia_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkThamgia = (CheckBox)Item.FindControl("chkThamgia");
                TextBox txtNgaynhanbanan = (TextBox)Item.FindControl("txtNgaynhanbanan");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtNgaynhanbanan.Text = txtNgaytuyenan.Text;
                    }
                }
            }
        }
        protected void chkMien_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)Item.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)Item.FindControl("txtAnphi");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtAnphi.Text = "";
                        txtAnphi.Enabled = false;
                    }
                    else
                    {
                        txtAnphi.Enabled = true;
                    }
                }
            }
        }
        protected void cmdTGTT_Click(object sender, EventArgs e)
        {
            try
            {
                decimal DONID = Convert.ToDecimal(hddDonID.Value);
                //bool IsNgayNhanBanAn = false;
                if (dgTGTT.Items.Count > 0)
                {
                    foreach (DataGridItem oItem in dgTGTT.Items)
                    {
                        TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgayTGTT");
                        if (txtNgaynhanbanan.Text != "")
                        {
                            if (!ValidateNgayNhanBanAn(txtNgaynhanbanan, true))
                            {
                                return;
                            }
                            //IsNgayNhanBanAn = true;
                        }
                    }
                }
                //if (IsNgayNhanBanAn)
                //{
                //    if (!CheckIsHaveBanAnFile(DONID, true))
                //    {
                //        return;
                //    }
                //}
                bool IsNew = false;
                foreach (DataGridItem oItem in dgTGTT.Items)
                {
                    string strID = oItem.Cells[0].Text;
                    decimal DSID = Convert.ToDecimal(strID);
                    IsNew = false;
                    CheckBox chkThamgiaTGTT = (CheckBox)oItem.FindControl("chkThamgiaTGTT");
                    TextBox txtNgayTGTT = (TextBox)oItem.FindControl("txtNgayTGTT");
                    ADS_SOTHAM_BANAN_TGTT oT = dt.ADS_SOTHAM_BANAN_TGTT.Where(x => x.DONID == DONID && x.THAMGIATOTUNGID == DSID).FirstOrDefault();
                    if (oT == null)
                    {
                        oT = new ADS_SOTHAM_BANAN_TGTT();
                        IsNew = true;
                    }
                    oT.DONID = DONID;
                    oT.THAMGIATOTUNGID = DSID;
                    oT.ISTHAMGIA = chkThamgiaTGTT.Checked == true ? 1 : 0;
                    oT.NGAYNHANBANAN = (String.IsNullOrEmpty(txtNgayTGTT.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgayTGTT.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (IsNew)
                    {
                        dt.ADS_SOTHAM_BANAN_TGTT.Add(oT);
                    }
                    dt.SaveChanges();
                }
                lblMsgTGTT.Text = "Lưu thành công !";
                cmdTGTT.Style.Add("margin-bottom", "0px");
            }
            catch (Exception ex)
            {
                lblMsgTGTT.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void chkThamgiaTGTT_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            foreach (DataGridItem Item in dgTGTT.Items)
            {
                CheckBox chkThamgiaTGTT = (CheckBox)Item.FindControl("chkThamgiaTGTT");
                TextBox txtNgayTGTT = (TextBox)Item.FindControl("txtNgayTGTT");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtNgayTGTT.Text = txtNgaytuyenan.Text;
                    }
                }
            }
        }
        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
                pnNguyenNhanQuaHan.Visible = true;
            else
                pnNguyenNhanQuaHan.Visible = false;
        }
        protected void txtNgaymophientoa_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgaymophientoa.Text))
            {
                if (String.IsNullOrEmpty(txtNgaytuyenan.Text))
                {
                    txtNgaytuyenan.Text = txtNgaymophientoa.Text;
                }
                if (String.IsNullOrEmpty(txtNgayhieuluc.Text))
                {
                    txtNgayhieuluc.Text = txtNgaymophientoa.Text;
                }
            }
        }
        private void UploadFileID(ADS_DON oDon, string strMaBieumau, ADS_SOTHAM_BANAN_FILE fileDinhKem)
        {
            ADS_DON_BL oBL = new ADS_DON_BL();
            decimal IDBM = 0;
            string strTenBM = "";
            bool isNew = false;
            List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
            if (lstBM.Count > 0)
            {
                IDBM = lstBM[0].ID;
                strTenBM = lstBM[0].TENBM;
            }
            ADS_FILE objFile = dt.ADS_FILE.Where(x => x.DONID == oDon.ID && x.TOAANID == oDon.TOAANID && x.MAGIAIDOAN == oDon.MAGIAIDOAN && x.BIEUMAUID == IDBM).FirstOrDefault();
            if (objFile == null)
            {
                isNew = true;
                objFile = new ADS_FILE();
            }
            objFile.DONID = oDon.ID;
            objFile.TOAANID = oDon.TOAANID;
            objFile.MAGIAIDOAN = oDon.MAGIAIDOAN;
            objFile.LOAIFILE = 1;
            objFile.BIEUMAUID = IDBM;
            objFile.NAM = DateTime.Now.Year;
            objFile.NOIDUNG = fileDinhKem.NOIDUNG;
            objFile.TENFILE = fileDinhKem.TENFILE;
            objFile.KIEUFILE = fileDinhKem.KIEUFILE;
            objFile.NGAYTAO = DateTime.Now;
            objFile.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            objFile.STT = 1;
            if (isNew)
                dt.ADS_FILE.Add(objFile);
            dt.SaveChanges();
        }
        private void TongDatDoiTuong(decimal TongDatID, decimal DuongSuID, decimal HinhThucGui, string NgayNhanBanAn)
        {
            string tucachtotung = "";
            ADS_DON_DUONGSU duongsu = dt.ADS_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
            if (duongsu != null)
            {
                tucachtotung = duongsu.TUCACHTOTUNG_MA + "";
            }
            bool IsNew = false;
            ADS_TONGDAT_DOITUONG tdDt = dt.ADS_TONGDAT_DOITUONG.Where(x => x.TONGDATID == TongDatID && x.DUONGSUID == DuongSuID).FirstOrDefault();
            if (tdDt == null)
            {
                IsNew = true;
                tdDt = new ADS_TONGDAT_DOITUONG();
            }
            tdDt.TONGDATID = TongDatID;
            tdDt.DUONGSUID = DuongSuID;
            tdDt.NGAYNHANTONGDAT = NgayNhanBanAn == "" ? (DateTime?)null : DateTime.Parse(NgayNhanBanAn, cul, DateTimeStyles.NoCurrentDateDefault);
            tdDt.TRANGTHAI = 1;
            tdDt.HINHTHUCGUI = HinhThucGui;
            if (HinhThucGui == 1)// gửi trực tuyến
                tdDt.NGAYGUI = tdDt.NGAYNHANTONGDAT;
            else
                tdDt.NGAYGUI = (DateTime?)null;
            tdDt.MATUCACH = tucachtotung;
            if (IsNew)
            {
                dt.ADS_TONGDAT_DOITUONG.Add(tdDt);
            }
            dt.SaveChanges();
        }
        private bool ValidateNgayNhanBanAn(TextBox txtNgaynhanbanan, bool IsThamGiaToTung)
        {
            DateTime NgayNhanBA;
            if (DateTime.TryParse(txtNgaynhanbanan.Text, cul, DateTimeStyles.NoCurrentDateDefault, out NgayNhanBA))
            {
                if (DateTime.Compare(NgayNhanBA, DateTime.Now) > 0)
                {
                    if (IsThamGiaToTung)
                        lblMsgTGTT.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                    else
                        lstMsgAnphi.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                    txtNgaynhanbanan.Focus();
                    return false;
                }
            }
            else
            {
                if (IsThamGiaToTung)
                    lblMsgTGTT.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                else
                    lstMsgAnphi.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                txtNgaynhanbanan.Focus();
                return false;
            }
            return true;
        }
        private bool CheckIsHaveBanAnFile(decimal DONID, bool IsThamGiaToTung)
        {
            // phải có file đính kèm và tên file phải có mã 52-ds
            bool IsBanAnFile = false;
            List<ADS_SOTHAM_BANAN_FILE> lstBAfile = dt.ADS_SOTHAM_BANAN_FILE.Where(x => x.DONID == DONID).ToList();
            if (lstBAfile == null || lstBAfile.Count == 0)
            {
                if (IsThamGiaToTung)
                    lblMsgTGTT.Text = "Hãy tải lên tệp Bản án dân sự sơ thẩm với tên tệp tin bao gồm mã biểu mẫu \" 52-DS \". Ví dụ: 52-DS. Bản án dân sự sơ thẩm.docx.";
                else
                    lstMsgAnphi.Text = "Hãy tải lên tệp Bản án dân sự sơ thẩm với tên tệp tin bao gồm mã biểu mẫu \" 52-DS \". Ví dụ: 52-DS. Bản án dân sự sơ thẩm.docx.";
                return false;
            }
            else
            {
                foreach (ADS_SOTHAM_BANAN_FILE obj in lstBAfile)
                {
                    if (obj.TENFILE.ToLower().Contains("52-ds"))
                    {
                        IsBanAnFile = true;
                    }
                }
            }
            if (!IsBanAnFile)
            {
                if (IsThamGiaToTung)
                    lblMsgTGTT.Text = "Hãy tải lên tệp Bản án dân sự sơ thẩm với tên tệp tin bao gồm mã biểu mẫu \" 52-DS \". Ví dụ: 52-DS. Bản án dân sự sơ thẩm.docx.";
                else
                    lstMsgAnphi.Text = "Hãy tải lên tệp Bản án dân sự sơ thẩm với tên tệp tin bao gồm mã biểu mẫu \" 52-DS \". Ví dụ: 52-DS. Bản án dân sự sơ thẩm.docx.";
                return false;
            }
            return true;
        }

        private decimal getcurrentid()
        {
            string current_id = Session[ENUM_LOAIAN.AN_DANSU] + "";
            if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Danhsach.aspx");
            return Convert.ToDecimal(current_id);
        }
        //private void renameTenvuviec(string obj, decimal DONID)
        //{
        //    ADS_DON oT = dt.ADS_DON.Where(x => x.ID == DONID).FirstOrDefault();
        //    ADS_DON_DUONGSU nguyendon = dt.ADS_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "NGUYENDON").FirstOrDefault();
        //    ADS_DON_DUONGSU bidon = dt.ADS_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "BIDON").FirstOrDefault();

        //    oT.TENVUVIEC = nguyendon.TENDUONGSU + " - " + bidon.TENDUONGSU + " - " + obj;
        //}
        //private void reset_TENVUVIEC(decimal DONID)
        //{
        //    ADS_SOTHAM_THULY oNT = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == DONID).FirstOrDefault();
        //    if (oNT.QUANHEPHAPLUAT_NAME != null)
        //    {
        //        renameTenvuviec(oNT.QUANHEPHAPLUAT_NAME, DONID);
        //    }
        //    else if (oNT.QUANHEPHAPLUATID != null)
        //    {
        //        decimal IDQHPL = Convert.ToDecimal(oNT.QUANHEPHAPLUATID.ToString());
        //        DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
        //        if (obj != null)
        //        {
        //            string quanhephapluat_name = obj.TEN.ToString();
        //            renameTenvuviec(quanhephapluat_name, DONID);
        //        }
        //    }
        //}
        private string getQHPL_NAME_THULY()
        {
            decimal ID = getcurrentid();
            ADS_SOTHAM_THULY oT = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == ID).FirstOrDefault();
            if(oT.QUANHEPHAPLUAT_NAME != null && oT.QUANHEPHAPLUAT_NAME != "")
            {
                return oT.QUANHEPHAPLUAT_NAME.ToString();
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) return obj.TEN.ToString();
                else return "";
            }
            else
            {
                return "";
            }
        }
        private void txtQuanhephapluat_name(ADS_SOTHAM_BANAN oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ADS_SOTHAM_BANAN oTT = dt.ADS_SOTHAM_BANAN.Where(x => x.DONID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }
        private void txtQuanhephapluat_name(ADS_SOTHAM_THULY oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ADS_DON oTT = dt.ADS_DON.Where(x => x.ID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }

        //hiển thị popup thêm điều luật
        protected void lkChoiceDieuLuat_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_Comon.CallFunctionJS(this, this.GetType(), "popupChonDieuKhoan()");
            }
            catch (Exception ex) { lstErr.Text = ex.Message; }
        }

        //load danh sách điều khoản mỗi khi tắt popup điều luật
        protected void cmdLoadDsToiDanh_Click(object sender, EventArgs e)
        {
            LoadDieuLuat();
        }
    }
}