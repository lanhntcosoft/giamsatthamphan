﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.ADS.Sotham
{
    public partial class vbtbtotung : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string current_id = Session[ENUM_LOAIAN.AN_DANSU] + "";
                hddDonID.Value = Session[ENUM_LOAIAN.AN_DANSU] + "" == "" ? "0" : Session[ENUM_LOAIAN.AN_DANSU] + "";
                if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Danhsach.aspx");
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                //Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                decimal DONID = Convert.ToDecimal(current_id);
                //CheckQuyen(ID);
                //SetTinhHuyenMacDinh();
                //LoadGrid();
                var data = dt.ADS_SOTHAM_THULY.FirstOrDefault(s=>s.DONID== DONID);
                if (data==null)
                {
                    btnTimKiem.Enabled = false;
                    cmdLammoi.Enabled = false;
                    btnThemmoi.Enabled = false;
                    lblThongbaoTimKiem.Text = "Vui lòng thụ lý vụ án";
                }
                else
                {
                    btnTimKiem.Enabled = true;
                    cmdLammoi.Enabled = true;
                    btnThemmoi.Enabled = true;
                }
                GetDoiTuongTrieuTap(DONID);
                LoadNguoiKyInfo(DONID);
                GetTenVBTB();
                //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
                //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
                //int counttk = oCBDT.Rows.Count;
                //if (counttk > 0)
                //{
                //    //là thư k
                //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
                //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
                //    //int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
                //    int countItem = dt.ADS_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == DONID && s.MAVAITRO == "VTTP_GIAIQUYETSOTHAM");
                //    if (countItem > 0)
                //    {
                //        //được gán 
                //    }
                //    else
                //    {
                //        //không được gán
                //        string StrMsg = "Người dùng không được sửa đổi thông tin của vụ việc do không được phân công giải quyết.";
                //        lblThongbaoTimKiem.Text = StrMsg;
                //        Cls_Comon.SetButton(btnTimKiem, false);
                //        Cls_Comon.SetButton(cmdLammoi, false);
                //        Cls_Comon.SetButton(btnThemmoi, false);
                //        return;
                //    }
                //}
                LoadGrid();
                
            }
        }
        //lấy tên văn bản thông báo
        private void GetTenVBTB()
        {
            //ddlTenVBTB.Items.Insert(0, new ListItem("--Các VB/TB tố tụng--", "0"));
            var d = dt.DM_DATAITEM.Where(s => s.MA == "VBTBTOTUNG").Select(s => new { s.ID, s.MA, s.TEN }).ToList();
            for (int i = 0; i < d.Count(); i++)
            {
                ddlTenVBTB.Items.Add(new ListItem(d[i].TEN, d[i].ID+""));
                ddllTenVBTB.Items.Insert(i, new ListItem(d[i].TEN, d[i].ID+""));

            }
            ddlTenVBTB.Items.Insert(0, new ListItem("---Chọn---","0"));
        }
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    DataRowView rowView = (DataRowView)e.Item.DataItem;
            //    LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
            //    Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);
            //    LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
            //    Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
            //    decimal DONID = Convert.ToDecimal(hddDonID.Value);
            //    ADS_DON oT = dt.ADS_DON.Where(x => x.ID == DONID).FirstOrDefault();
            //    if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
            //    {
            //        lblSua.Text = "Chi tiết";
            //        lbtXoa.Visible = false;
            //    }
            //    if (rowView["IsBanAnST"].ToString() != "0")
            //        lbtXoa.Visible = false;
            //    if (hddShowCommand.Value == "False")
            //    {
            //        lblSua.Text = "Chi tiết";
            //        lbtXoa.Visible = false;
            //    }
            //}
        }
        /// <summary>
        /// lấy ra đối tượng triệu tập
        /// </summary>
        private void GetDoiTuongTrieuTap(decimal DONID)
        {
            ADS_DON_DUONGSU_BL oBL = new ADS_DON_DUONGSU_BL();
            DM_DATAITEM_BL oDM = new DM_DATAITEM_BL();
            DataTable dtDuongsu = oBL.ADS_DON_DUONGSU_GETBY(DONID);
            //lấy đối tượng triệu tập
            var lstNguoiThamGiaTotung=(from tgtt in dt.ADS_DON_THAMGIATOTUNG.Where(s => s.DONID == DONID)
            join dm in dt.DM_DATAITEM on tgtt.TUCACHTGTTID equals dm.MA into dmm
            from dmmm in dmm.DefaultIfEmpty() select new 
            {
                ID= tgtt.ID,
                HOTEN=tgtt.HOTEN,
                TUCACHTOTUNG=dmmm.TEN
            }).ToList();
            DataTable dtDoiTuongTrieuTap = new DataTable();
            dtDoiTuongTrieuTap.Clear();
            dtDoiTuongTrieuTap.Columns.Add("ID");
            dtDoiTuongTrieuTap.Columns.Add("TEN");
            //thêm đối tượng đượng sự
            for (int i = 0; i < dtDuongsu.Rows.Count; i++)
            {
                DataRow _dtRow = dtDoiTuongTrieuTap.NewRow();
                _dtRow["ID"] = ENUM_LOAI_TRIEUTAP.DUONGSU+"_"+ dtDuongsu.Rows[i]["ID"];
                _dtRow["TEN"] = dtDuongsu.Rows[i]["TENDUONGSU"] + " - " + dtDuongsu.Rows[i]["TENTCTT"]; ;
                dtDoiTuongTrieuTap.Rows.Add(_dtRow);
            }
            //thêm ngươi tham gia tố tụng
            if (lstNguoiThamGiaTotung != null)
            {
                for (int i = 0; i < lstNguoiThamGiaTotung.Count; i++)
                {
                    DataRow _dtRow = dtDoiTuongTrieuTap.NewRow();
                    _dtRow["ID"] = ENUM_LOAI_TRIEUTAP.NGUOITHAMGIATOTUNG + "_" + lstNguoiThamGiaTotung[i].ID;
                    _dtRow["TEN"] = lstNguoiThamGiaTotung[i].HOTEN+" - "+ lstNguoiThamGiaTotung[i].TUCACHTOTUNG;
                    dtDoiTuongTrieuTap.Rows.Add(_dtRow);
                }
            }
            
            ddlDoiTuongTrieuTap.DataSource = dtDoiTuongTrieuTap;
            ddlDoiTuongTrieuTap.DataTextField = "TEN";
            ddlDoiTuongTrieuTap.DataValueField = "ID";
            ddlDoiTuongTrieuTap.DataBind();
            ddlDoiTuongTrieuTap.Items.Insert(0,new ListItem("---Chọn---", "0"));

            ddllDoiTuongTrieuTap.DataSource = dtDoiTuongTrieuTap;
            ddllDoiTuongTrieuTap.DataTextField = "TEN";
            ddllDoiTuongTrieuTap.DataValueField = "ID";
            ddllDoiTuongTrieuTap.DataBind();
        }
        private void LoadNguoiKyInfo(decimal DonID)
        {
            DM_CANBO_BL cb_BL = new DM_CANBO_BL();
            ADS_SOTHAM_HDXX oND = dt.ADS_SOTHAM_HDXX.Where(x => x.DONID == DonID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).FirstOrDefault<ADS_SOTHAM_HDXX>();
            if (oND != null)
            {
                decimal CanBoID = Convert.ToDecimal(oND.CANBOID.ToString());

                DataTable dtCanBo = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                if (dtCanBo.Rows.Count > 0)
                {
                    txttNguoiKy.Text = dtCanBo.Rows[0]["HOTEN"].ToString();
                    txttChucvu.Text = dtCanBo.Rows[0]["ChucVu"].ToString();
                    hddNguoiKyID.Value = dtCanBo.Rows[0]["ID"].ToString();
                }
            }
            else
            {
                ADS_DON_THAMPHAN oTP = dt.ADS_DON_THAMPHAN.Where(x => x.DONID == DonID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM).FirstOrDefault();
                if (oTP != null)
                {
                    decimal CanBoID = Convert.ToDecimal(oTP.CANBOID.ToString());
                    DataTable dtCanBo = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                    if (dtCanBo.Rows.Count > 0)
                    {
                        txttNguoiKy.Text = dtCanBo.Rows[0]["HOTEN"].ToString();
                        txttChucvu.Text = dtCanBo.Rows[0]["ChucVu"].ToString();
                        hddNguoiKyID.Value = dtCanBo.Rows[0]["ID"].ToString();
                    }
                }
                else
                    txtNguoiKy.Text = txtChucvu.Text = "";
            }
        }

        private bool CheckValid()
        {
            if (string.IsNullOrEmpty(txttSoVBTB.Text))
            {
                lbthongbao.Text = "Bạn chưa nhập số VB. Hãy nhập lại!";
                txttSoVBTB.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttNgay.Text))
            {
                lbthongbao.Text = "Bạn chưa nhập ngày. Hãy nhập lại!";
                txttNgay.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttNoiDung.Text))
            {
                lbthongbao.Text = "Bạn chưa nhập nội dung. Hãy nhập lại!";
                txttNoiDung.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttHieuLucTuNgay.Text))
            {
                lbthongbao.Text = "Bạn chưa nhập hiệu lực từ ngày. Hãy nhập lại!";
                txttHieuLucTuNgay.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txttHieuLucDenNgay.Text))
            {
                lbthongbao.Text = "Bạn chưa nhập hiệu lực đến ngày. Hãy nhập lại!";
                txttHieuLucDenNgay.Focus();
                return false;
            }
            
            return true;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                decimal DONID = Convert.ToDecimal(hddDonID.Value);

                VBTB_TOTUNG obj;
                if ((hddid.Value == "" || hddid.Value == "0"))
                {
                    obj = new VBTB_TOTUNG();
                }
                else
                {
                    decimal ID = Convert.ToDecimal(hddid.Value);
                    obj = dt.VBTB_TOTUNG.Where(x => x.ID == ID).FirstOrDefault();
                }
                obj.MAVBTB = Convert.ToDecimal(ddllTenVBTB.SelectedValue);
                obj.SOVBTB = txttSoVBTB.Text;
                //lấy thông tin đương sự
                string[] arrayData = ddllDoiTuongTrieuTap.SelectedValue.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrayData[0] == ENUM_LOAI_TRIEUTAP.DUONGSU)
                {
                    decimal idDuongSu = Convert.ToDecimal(arrayData[1]);
                    var data = dt.ADS_DON_DUONGSU.FirstOrDefault(s => s.ID == idDuongSu);
                    obj.LOAIDOITUONG = ENUM_LOAI_TRIEUTAP.DUONGSU;
                    obj.IDDOITUONG = data.ID;
                }
                else if (arrayData[0] == ENUM_LOAI_TRIEUTAP.NGUOITHAMGIATOTUNG)
                {
                    decimal idThamgiaTotung = Convert.ToDecimal(arrayData[1]);
                    var data = dt.ADS_DON_THAMGIATOTUNG.FirstOrDefault(s => s.ID == idThamgiaTotung);
                    obj.LOAIDOITUONG = ENUM_LOAI_TRIEUTAP.NGUOITHAMGIATOTUNG;
                    obj.IDDOITUONG = data.ID;
                }
                obj.NOIDUNG = txttNoiDung.Text.Trim();

                obj.DONID = DONID;

                obj.HIEULUCTUNGAY = (String.IsNullOrEmpty(txttHieuLucTuNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txttHieuLucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.HIEULUCDENNGAY = (String.IsNullOrEmpty(txttHieuLucDenNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txttHieuLucDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.NGUOIKY = string.IsNullOrEmpty(hddNguoiKyID.Value) ? (decimal?)null : Convert.ToDecimal(hddNguoiKyID.Value);
                obj.NGAY = (String.IsNullOrEmpty(txttNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txttNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.LOAIAN = ENUM_LOAIVUVIEC.AN_DANSU;
                obj.GIAIDOANVUAN = ENUM_GIAIDOANVUAN.SOTHAM;
                if (hddid.Value == "" || hddid.Value == "0")
                {
                    obj.NGAYTAO = DateTime.Now;
                    obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.VBTB_TOTUNG.Add(obj);
                    dt.SaveChanges();
                    ResetControlsThem();
                }
                else
                {
                    obj.NGAYSUA = DateTime.Now;
                    obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.SaveChanges();
                }
                dgList.CurrentPageIndex = 0;
                
                
                lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lbthongbao.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void btnUpdateAndQuayLai_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                decimal DONID = Convert.ToDecimal(hddDonID.Value);

                VBTB_TOTUNG obj;
                if ((hddid.Value == "" || hddid.Value == "0"))
                {
                    obj = new VBTB_TOTUNG();
                }
                else
                {
                    decimal ID = Convert.ToDecimal(hddid.Value);
                    obj = dt.VBTB_TOTUNG.Where(x => x.ID == ID).FirstOrDefault();
                }
                obj.MAVBTB = Convert.ToDecimal(ddllTenVBTB.SelectedValue);
                obj.SOVBTB = txttSoVBTB.Text;
                //lấy thông tin đương sự
                string[] arrayData = ddllDoiTuongTrieuTap.SelectedValue.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrayData[0] == ENUM_LOAI_TRIEUTAP.DUONGSU)
                {
                    decimal idDuongSu = Convert.ToDecimal(arrayData[1]);
                    var data = dt.ADS_DON_DUONGSU.FirstOrDefault(s => s.ID == idDuongSu);
                    obj.LOAIDOITUONG = ENUM_LOAI_TRIEUTAP.DUONGSU;
                    obj.IDDOITUONG = data.ID;
                }
                else if (arrayData[0] == ENUM_LOAI_TRIEUTAP.NGUOITHAMGIATOTUNG)
                {
                    decimal idThamgiaTotung = Convert.ToDecimal(arrayData[1]);
                    var data = dt.ADS_DON_THAMGIATOTUNG.FirstOrDefault(s => s.ID == idThamgiaTotung);
                    obj.LOAIDOITUONG = ENUM_LOAI_TRIEUTAP.NGUOITHAMGIATOTUNG;
                    obj.IDDOITUONG = data.ID;
                }
                obj.NOIDUNG = txttNoiDung.Text.Trim();

                obj.DONID = DONID;

                obj.HIEULUCTUNGAY = (String.IsNullOrEmpty(txttHieuLucTuNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txttHieuLucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.HIEULUCDENNGAY = (String.IsNullOrEmpty(txttHieuLucDenNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txttHieuLucDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.NGUOIKY = string.IsNullOrEmpty(hddNguoiKyID.Value) ? (decimal?)null : Convert.ToDecimal(hddNguoiKyID.Value);
                obj.NGAY = (String.IsNullOrEmpty(txttNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txttNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.LOAIAN = ENUM_LOAIVUVIEC.AN_DANSU;
                obj.GIAIDOANVUAN = ENUM_GIAIDOANVUAN.SOTHAM;
                if (hddid.Value == "" || hddid.Value == "0")
                {
                    obj.NGAYTAO = DateTime.Now;
                    obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.VBTB_TOTUNG.Add(obj);
                    dt.SaveChanges();
                    ResetControlsThem();
                }
                else
                {
                    obj.NGAYSUA = DateTime.Now;
                    obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.SaveChanges();
                }
                dgList.CurrentPageIndex = 0;

                
                LoadGrid();
                pnTimKiem.Visible = true;
                pnThemMoi.Visible = false;
                lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lbthongbao.Text = "Lỗi: " + ex.Message;
            }
        } 
        public void loadedit(decimal ID)
        {
            VBTB_TOTUNG oND = dt.VBTB_TOTUNG.Where(x => x.ID == ID).FirstOrDefault();
            hddid.Value = oND.ID.ToString();

            if (oND.MAVBTB.HasValue)
            {
                ddlTenVBTB.SelectedValue = oND.MAVBTB.Value + "";
            }
            else
            {
                ddlTenVBTB.SelectedIndex = 0;
            }
            txttSoVBTB.Text = oND.SOVBTB;
            txttNgay.Text = oND.NGAY.HasValue ? oND.NGAY.Value.ToString("dd/MM/yyyy") : "";
            ddllDoiTuongTrieuTap.SelectedValue = oND.LOAIDOITUONG + "_" + oND.IDDOITUONG;
            txttNoiDung.Text = oND.NOIDUNG;
            txttHieuLucTuNgay.Text = oND.HIEULUCTUNGAY.HasValue ? oND.HIEULUCTUNGAY.Value.ToString("dd/MM/yyyy") : "";
            txttHieuLucDenNgay.Text = oND.HIEULUCDENNGAY.HasValue ? oND.HIEULUCDENNGAY.Value.ToString("dd/MM/yyyy") : "";
            //ddlLoaiQD_SelectedIndexChanged(new object(), new EventArgs());
            //ddlTenVBTB.SelectedValue = oND.TENVBTB;
            pnTimKiem.Visible = false;
            pnThemMoi.Visible = true;
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            LoadGrid();
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid();
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            LoadGrid();
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            LoadGrid();
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            LoadGrid();
        }
        #endregion
        private void ResetControlsTimKiem()
        {
            txtNgay.Text = "";
            txtSoVBTB.Text = "";
            txtsNoiDung.Text = "";
            txtTuNgay.Text = "";
            txtDenNgay.Text = "";
            ddlDoiTuongTrieuTap.SelectedIndex = 0;
            ddlTenVBTB.SelectedIndex = 0;
            txtNguoiKy.Text = "";
            txtChucvu.Text = "";
            lbthongbao.Text = "";
            lblThongbaoTimKiem.Text = "";
        }
        private void ResetControlsThem()
        {
            txttNgay.Text = "";
            txttSoVBTB.Text = "";
            txttNoiDung.Text = "";
            txttHieuLucDenNgay.Text = "";
            txttHieuLucTuNgay.Text = "";
            ddllDoiTuongTrieuTap.SelectedIndex = 0;
            ddllTenVBTB.SelectedIndex = 0;
            hddid.Value = "";
            lbthongbao.Text = "";
        }
        public void xoa(decimal id)
        {
            VBTB_TOTUNG oND = dt.VBTB_TOTUNG.Where(x => x.ID == id).FirstOrDefault();

            dt.VBTB_TOTUNG.Remove(oND);
            dt.SaveChanges();
            dgList.CurrentPageIndex = 0;
            LoadGrid();
            lblThongbaoTimKiem.Text = "Xóa thành công!";
        }
        //
        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }
        protected void btnLammoi_Click(object sender, EventArgs e)
        {
            ResetControlsTimKiem();
        }
        //
        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            ResetControlsThem();
            pnTimKiem.Visible = false;
            pnThemMoi.Visible = true;
        }
        //btnQuayLai_Click
        protected void btnQuayLai_Click(object sender, EventArgs e)
        {
            hddid.Value = "";
            LoadGrid();
            pnTimKiem.Visible = true;
            pnThemMoi.Visible = false;
        }
        private string getToaAn(decimal ToaAnID)
        {
            try
            {
                string strDiadiem = "";
                DM_TOAAN oT = dt.DM_TOAAN.Where(x => x.ID == ToaAnID).FirstOrDefault();
                strDiadiem = oT.TEN.Replace("Tòa án nhân dân ", "");
                return strDiadiem;
            }
            catch { return ""; }
        }
        private string getDiaDiem(decimal ToaAnID)
        {
            try
            {
                string strDiadiem = "";
                DM_TOAAN oT = dt.DM_TOAAN.Where(x => x.ID == ToaAnID).FirstOrDefault();
                strDiadiem = oT.TEN.Replace("Tòa án nhân dân ", "");
                switch (oT.LOAITOA)
                {
                    case "CAPHUYEN":
                        DM_TOAAN opT = dt.DM_TOAAN.Where(x => x.ID == oT.CAPCHAID).FirstOrDefault();
                        strDiadiem = opT.TEN.Replace("Tòa án nhân dân ", "");
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                    case "CAPTINH":
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                    case "CAPCAO":
                        strDiadiem = strDiadiem.Replace("cấp cao", "");
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                }
                return strDiadiem;
            }
            catch { return ""; }
        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
        private void DowloadFile(decimal ID)
        {
            var Data = dt.VBTB_TOTUNG.FirstOrDefault(s => s.ID == ID);
            var oDon = dt.ADS_DON.FirstOrDefault(s => s.ID == Data.DONID);
            string strDiadiem = getDiaDiem((decimal)oDon.TOAANID);
            string strTinh = getToaAn((decimal)oDon.TOAANID);
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            GDTTT_DON_BL gDTTT_DON_BL = new GDTTT_DON_BL();
            tbl = gDTTT_DON_BL.GetGiayTrieuTap(ID, strDiadiem, strTinh);
            string strData = "";
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                strData = row["TEXT_REPORT"] + "";
            }
            string TenFile = "GIAYTRIEUTAP_ADS";
            string KieuFile = ".docx";
            var cacheKey = Guid.NewGuid().ToString("N");
            Context.Cache.Insert(key: cacheKey, value: strData, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadWord.aspx?cacheKey=" + cacheKey + "&FileName=" + TenFile + "&Extension=" + KieuFile + "';", true);

        }
        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Download":
                    DowloadFile(ND_id);
                    break;
                case "Sua":
                    lbthongbao.Text = "";
                    loadedit(ND_id);
                    hddid.Value = e.CommandArgument.ToString();
                    break;
                case "Xoa":
                    
                    xoa(ND_id);
                    break;
            }
        }
        

        public void LoadGrid()
        {
            VBTB_TOTUNG_BL oBL = new VBTB_TOTUNG_BL();
            decimal ID = Convert.ToDecimal(hddDonID.Value);
            decimal iddoituong = 0;
            string loaidoituong = "";
            if (ddlDoiTuongTrieuTap.SelectedValue!="0")
            {
                string[] arrayData = ddlDoiTuongTrieuTap.SelectedValue.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                iddoituong=Convert.ToDecimal(arrayData[1]);
                loaidoituong = arrayData[0];
            }
            DataTable oDT = oBL.VBTB_TOTUNG_GETLIST(ID, ENUM_LOAIVUVIEC.AN_DANSU,ENUM_GIAIDOANVUAN.SOTHAM, Convert.ToDecimal(ddlTenVBTB.SelectedValue), loaidoituong, iddoituong, txtSoVBTB.Text, txtNgay.Text, txtsNoiDung.Text,Convert.ToDecimal(ddlHieuLuc.SelectedValue), txtTuNgay.Text, txtDenNgay.Text, txtNguoiKy.Text, txtChucvu.Text);

            if (oDT != null && oDT.Rows.Count > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(Convert.ToInt32(oDT.Rows.Count), Convert.ToInt32(20)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + oDT.Rows.Count.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion

                dgList.DataSource = oDT;
                dgList.DataBind();
                pndata.Visible = true;
            }
            else
            {
                pndata.Visible = false;
            }
        }
    }
}