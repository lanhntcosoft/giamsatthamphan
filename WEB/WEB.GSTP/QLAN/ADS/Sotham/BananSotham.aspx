﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="BananSotham.aspx.cs" Inherits="WEB.GSTP.QLAN.ADS.Sotham.BananSotham" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/UI/js/base64.js"></script>
    <script type="text/javascript" src="/UI/js/vgcaplugin.js"></script>
    <script type="text/javascript" src="../../../UI/js/Common.js"></script>

    <style type="text/css">
        .tleboxchung {
            text-transform: uppercase;
        }

        .ajax__calendar_container {
            width: 180px;
        }

        .ajax__calendar_body {
            width: 100%;
            height: 145px;
        }
    </style>
    <asp:HiddenField ID="ttBanDauDONKK_USER_DKNHANVB" runat="server" Value="0" />
    <asp:HiddenField ID="hddBanAnID" runat="server" Value="0" />
    <asp:HiddenField ID="hddDonID" runat="server" Value="0" />
    <asp:HiddenField ID="hddNgayQDXX" runat="server" Value="" />
    <asp:HiddenField ID="hddTGTTRowLastIndex" runat="server" Value="0" />
    <asp:HiddenField ID="hddShowCommand" runat="server" Value="True" />
    <div class="boxchung">
        <h4 class="tleboxchung">THÔNG TIN BẢN ÁN</h4>   
        <div class="boder" style="padding: 10px;">
            <table class="table1">
                <tr>
                    <td>Quan hệ pháp luật<span class="batbuoc">(*)</span></td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlQuanhephapluat" CssClass="chosen-select" runat="server" Width="450px"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlQuanhephapluat_SelectedIndexChanged" Visible="false">
                        </asp:DropDownList>
                       <asp:TextBox ID="txtQuanhephapluat" CssClass="user" placeholder="" runat="server" Width="440px" MaxLength="500" TextMode="MultiLine"></asp:TextBox>

                        <asp:DropDownList ID="ddlLoaiQuanhe" Visible="false" CssClass="chosen-select" runat="server" Width="300px">
                            <asp:ListItem Value="1" Text="Tranh chấp"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Yêu cầu"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>QHPL dùng cho thống kê<span class="batbuoc">(*)</span></td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlQHPLTK" CssClass="chosen-select" runat="server" Width="450px"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 158px;">Số bản án<span class="batbuoc">(*)</span></td>
                    <td style="width: 208px;">
                        <asp:TextBox ID="txtSobanan" CssClass="user" runat="server" Width="90px" MaxLength="250"></asp:TextBox>
                    </td>
                    <td style="width: 125px;">Ngày mở phiên tòa<span class="batbuoc">(*)</span></td>
                    <td>
                        <asp:TextBox ID="txtNgaymophientoa" AutoPostBack="true" OnTextChanged="txtNgaymophientoa_TextChanged" runat="server" CssClass="user"
                            Width="95px" MaxLength="10"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtNgaymophientoa" Format="dd/MM/yyyy" BehaviorID="_content_CalendarExtender2" />
                        <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtNgaymophientoa" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="True" BehaviorID="_content_MaskedEditExtender3" Century="2000" CultureAMPMPlaceholder="SA;CH" CultureCurrencySymbolPlaceholder="₫" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="," CultureThousandsPlaceholder="." CultureTimePlaceholder=":" />
                    </td>
                </tr>
                <tr>
                    <td>Ngày tuyên án<span class="batbuoc">(*)</span></td>
                    <td>
                        <asp:TextBox ID="txtNgaytuyenan" runat="server" CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtNgaytuyenan" Format="dd/MM/yyyy" BehaviorID="_content_CalendarExtender1" />
                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtNgaytuyenan" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="True" BehaviorID="_content_MaskedEditExtender1" Century="2000" CultureAMPMPlaceholder="SA;CH" CultureCurrencySymbolPlaceholder="₫" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="," CultureThousandsPlaceholder="." CultureTimePlaceholder=":" />
                    </td>
                    <td>Ngày hiệu lực</td>
                    <td>
                        <asp:TextBox ID="txtNgayhieuluc" runat="server" CssClass="user" Width="95px" MaxLength="10"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtNgayhieuluc" Format="dd/MM/yyyy" BehaviorID="_content_CalendarExtender3" />
                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtNgayhieuluc" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="True" BehaviorID="_content_MaskedEditExtender2" Century="2000" CultureAMPMPlaceholder="SA;CH" CultureCurrencySymbolPlaceholder="₫" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="," CultureThousandsPlaceholder="." CultureTimePlaceholder=":" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="boxchung">
                            <h4 class="tleboxchung">Các chỉ tiêu hỗ trợ thống kê</h4>
                            <div class="boder" style="padding: 10px;">
                                <table class="table1">
                                    <tr>
                                        <td style="width: 158px;">Áp dụng án lệ ?<span class="batbuoc">(*)</span></td>
                                        <td style="width: 150px;">
                                            <asp:RadioButtonList ID="rdbAnle" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="0" Text="Không"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td style="width: 168px;">Có VKS tham gia ?<span class="batbuoc">(*)</span></td>
                                        <td>
                                            <asp:RadioButtonList ID="rdbVKSThamgia" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="0" Text="Không"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                    </tr>
                                    <tr>
                                        <td>Yếu tố nước ngoài ?</td>
                                        <td>
                                            <asp:DropDownList ID="ddlYeutonuocngoai" CssClass="chosen-select" runat="server" Width="110px">
                                                <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Không"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Chưa xác định"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>Số quyết định cá biệt trái pháp luật của cơ quan, tổ chức bị Tòa án hủy ?</td>
                                        <td>
                                            <asp:TextBox ID="txtSoQDTraiPLBiHuy" CssClass="user align_right"
                                                runat="server" Width="100px" onkeypress="return isNumber(event)"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Vụ án quá hạn luật định ?<span class="batbuoc">(*)</span></td>
                                        <td>
                                            <asp:RadioButtonList ID="rdVuAnQuaHan" AutoPostBack="true"
                                                runat="server" RepeatDirection="Horizontal"
                                                OnSelectedIndexChanged="rdVuAnQuaHan_SelectedIndexChanged">
                                                <asp:ListItem Value="0">Không</asp:ListItem>
                                                <asp:ListItem Value="1">Có</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <asp:Panel ID="pnNguyenNhanQuaHan" runat="server" Visible="false">
                                        <tr>
                                            <td>Nguyên nhân chủ quan ?<span class="batbuoc">(*)</span></td>
                                            <td>
                                                <asp:RadioButtonList ID="rdNNChuQuan" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="0">Không</asp:ListItem>
                                                    <asp:ListItem Value="1">Có</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>Nguyên nhân khách quan ?<span class="batbuoc">(*)</span></td>
                                            <td>
                                                <asp:RadioButtonList ID="rdNNKhachQuan" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="0">Không</asp:ListItem>
                                                    <asp:ListItem Value="1">Có</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <tr>
                                        <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Tệp đính kèm</td>
                    <td colspan="3">
                        <asp:HiddenField ID="hddFilePath" runat="server" />

                        <asp:CheckBox ID="chkKySo" Checked="true" runat="server" onclick="CheckKyso();" Text="Sử dụng ký số file đính kèm" />
                        <br />
                        <asp:HiddenField ID="hddFileKySo" runat="server" Value="" />
                        <asp:HiddenField ID="hddSessionID" runat="server" />
                        <asp:HiddenField ID="hddURLKS" runat="server" />
                        <div id="zonekyso" style="margin-bottom: 5px; margin-top: 10px;">
                            <button type="button" class="buttonkyso" id="TruongPhongKyNhay" onclick="exc_sign_file1();">Chọn file đính kèm và ký số</button>
                            <button type="button" class="buttonkyso" id="_Config" onclick="vgca_show_config();">Cấu hình CKS</button><br />
                            <ul id="file_name" style="list-style: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; line-height: 18px;">
                            </ul>
                        </div>
                        <div id="zonekythuong" style="display: block; margin-top: 10px; width: 80%;">
                            <cc1:AsyncFileUpload ID="AsyncFileUpLoad" runat="server" CompleteBackColor="Lime" UploaderStyle="Modern" OnUploadedComplete="AsyncFileUpLoad_UploadedComplete"
                                ErrorBackColor="Red" ThrobberID="Throbber" UploadingBackColor="#66CCFF" OnClientUploadComplete="uploadComplete" />
                            <asp:Image ID="Throbber" runat="server" ImageUrl="~/UI/img/loading-gear.gif" />
                        </div>
                        <div style="display: none">
                            <asp:Button ID="cmdThemFileTL" runat="server"
                                Text="Them tai lieu" OnClick="cmdThemFileTL_Click" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <asp:DataGrid ID="dgFile" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            PageSize="20" AllowPaging="false" GridLines="None" PagerStyle-Mode="NumericPages"
                            CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                            ItemStyle-CssClass="chan" Width="100%" OnItemCommand="dgFile_ItemCommand">
                            <Columns>
                                <asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        TT
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Tên tệp
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("TENFILE") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px">
                                    <HeaderTemplate>
                                        Tệp đính kèm
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblDownload" runat="server" Text="Xem" CausesValidation="false" CommandName="Download" ForeColor="#0e7eee"
                                            CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Thao tác
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" CausesValidation="false" Text="Xóa file" ForeColor="#0e7eee"
                                            CommandName="Xoa" CommandArgument='<%#Eval("ID") %>' ToolTip="Xóa" OnClientClick="return confirm('Bạn thực sự muốn xóa file này? ');"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="header"></HeaderStyle>
                            <ItemStyle CssClass="chan"></ItemStyle>
                            <PagerStyle Visible="false"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lstErr" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput"
                            Text="Lưu thông tin bản án" OnClick="cmdUpdate_Click"
                            OnClientClick="return validate();" />
                        <asp:Button ID="cmdHuyBanAn" runat="server" CssClass="buttoninput"
                            Text="Xóa bản án" OnClick="cmdHuyBanAn_Click"
                            OnClientClick="return confirm('Bạn thực sự muốn xóa bản án này? ');" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="boxchung">
        <h4 class="tleboxchung">ĐIỀU LUẬT ÁP DỤNG</h4>
        <div class="boder" style="padding: 10px;">
            <table class="table1">
                <%--Chọn điều luật áp dụng--%>
                <tr>
                    <td></td>
                    <td>
                        <div style="float: left;">  
                            <asp:LinkButton ID="lkChoiceDieuLuat" runat="server"
                                OnClick="lkChoiceDieuLuat_Click"
                                CssClass="buttonpopup choice_list">Chọn điều luật</asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 135px;">Bộ Luật áp dụng <span class="batbuoc">(*)</span></td>
                    <td>
                        <asp:DropDownList ID="ddlBoLuat" CssClass="chosen-select" runat="server" Width="420px" AutoPostBack="True" OnSelectedIndexChanged="ddlBoLuat_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Điều khoản áp dụng <span class="batbuoc">(*)</span></td>
                    <td>
                        <asp:DropDownList ID="ddlDieukhoan" CssClass="chosen-select" runat="server" Width="420px"></asp:DropDownList>
                    </td>
                </tr>
                <%--<tr>
                    <td><b>Chương</b></td>
                    <td>
                        <asp:TextBox ID="txtChuong" runat="server" CssClass="user" Width="60px" ReadOnly="true"></asp:TextBox>
                        <b style="width: 50px;">Điểm</b>
                        <asp:TextBox ID="txtDiem" runat="server" CssClass="user" Width="60px" ReadOnly="true"></asp:TextBox>
                        <b style="width: 50px;">Khoản</b>
                        <asp:TextBox ID="txtKhoan" runat="server" CssClass="user" Width="60px" ReadOnly="true"></asp:TextBox>
                        <b style="width: 50px;">Điều</b>
                        <asp:TextBox ID="txtDieu" runat="server" CssClass="user" Width="60px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="cmdLuatUpdate" runat="server" CssClass="buttoninput"
                            Text="Lưu điều khoản" OnClick="cmdLuatUpdate_Click" OnClientClick="return validate_them_dieuluat();" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="lstMsgDieuluat" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="dgDieuLuat" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            PageSize="20" AllowPaging="false" GridLines="None" PagerStyle-Mode="NumericPages"
                            CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                            ItemStyle-CssClass="chan" Width="100%" OnItemCommand="dgDieuLuat_ItemCommand">
                            <Columns>
                                <asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        TT
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Bộ luật
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("TENBOLUAT") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                                    <HeaderTemplate>
                                        Tên điều khoản
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("TENTOIDANH") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Điểm
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("DIEM") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Khoản
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("KHOAN") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Điều
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("DIEU") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Thao tác
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtXoa" runat="server" CausesValidation="false" Text="Xóa" ForeColor="#0e7eee"
                                            CommandName="Xoa" CommandArgument='<%#Eval("ID") %>' ToolTip="Xóa" OnClientClick="return confirm('Bạn thực sự muốn xóa điều luật này? ');"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="header"></HeaderStyle>
                            <ItemStyle CssClass="chan"></ItemStyle>
                            <PagerStyle Visible="false"></PagerStyle>
                        </asp:DataGrid>

                    </td>

                </tr>

            </table>
        </div>
    </div>
    <div class="boxchung">
        <h4 class="tleboxchung">THÔNG TIN ÁN PHÍ</h4>
        <div class="boder" style="padding: 10px;">
            <table class="table1">
                <tr>
                    <td>
                        <asp:DataGrid ID="dgAnPhi" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            PageSize="20" AllowPaging="false" GridLines="None" PagerStyle-Mode="NumericPages"
                            CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                            ItemStyle-CssClass="chan" Width="100%">
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        TT
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Tên đương sự
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("TENDUONGSU") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Tham gia phiên tòa
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkThamgia" runat="server" Checked='<%# GetNumber(Eval("ISTHAMGIA"))%>' AutoPostBack="true" ToolTip='<%#Eval("ID")%>' OnCheckedChanged="chkThamgia_CheckChange" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
                                    <HeaderTemplate>
                                        Ngày nhận bản án
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNgaynhanbanan" runat="server" Text='<%# GetTextDate(Eval("NGAYNHANAN"))%>' CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtNgaynhanbanan_CalendarExtender" runat="server" TargetControlID="txtNgaynhanbanan" Format="dd/MM/yyyy" />
                                        <cc1:MaskedEditExtender ID="txtNgaynhanbanan_MaskedEditExtender3" runat="server" TargetControlID="txtNgaynhanbanan" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Miễn án phí
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkMien" runat="server" Checked='<%# GetNumber(Eval("MIENANPHI"))%>' ToolTip='<%#Eval("ID")%>' AutoPostBack="true" OnCheckedChanged="chkMien_CheckChange" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Án phí
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtAnphi" runat="server" Style="text-align: right; padding-right: 5px;" Text='<%#Eval("ANPHI")+""==""?"":Convert.ToDouble(Eval("ANPHI")).ToString("#,0.###", new System.Globalization.CultureInfo("vi-VN")) %>' CssClass="user" Width="90%"
                                            onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="header"></HeaderStyle>
                            <ItemStyle CssClass="chan"></ItemStyle>
                            <PagerStyle Visible="false"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lstMsgAnphi" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="cmdAnphi" runat="server" CssClass="buttoninput"
                            Text="Lưu án phí" OnClick="cmdAnphi_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="boxchung">
        <h4 class="tleboxchung">V. THÔNG TIN NGƯỜI THAM GIA TỐ TỤNG</h4>
        <div class="boder" style="padding: 10px;">
            <table class="table1">
                <tr>
                    <td>
                        <asp:DataGrid ID="dgTGTT" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            PageSize="20" AllowPaging="false" GridLines="None" PagerStyle-Mode="NumericPages"
                            CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                            ItemStyle-CssClass="chan" Width="100%" OnItemDataBound="dgTGTT_ItemDataBound">
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        TT
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Tên người tham gia tố tụng
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("HOTEN") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px">
                                    <HeaderTemplate>
                                        Tham gia phiên tòa
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkThamgiaTGTT" runat="server" Checked='<%# GetNumber(Eval("ISTHAMGIA"))%>' AutoPostBack="true" ToolTip='<%#Eval("ID")%>' OnCheckedChanged="chkThamgiaTGTT_CheckChange" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
                                    <HeaderTemplate>
                                        Ngày nhận bản án
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNgayTGTT" runat="server" Text='<%# GetTextDate(Eval("NGAYNHANBANAN"))%>' CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtNgayTGTT_CalendarExtender" runat="server" TargetControlID="txtNgayTGTT" Format="dd/MM/yyyy" />
                                        <cc1:MaskedEditExtender ID="txtNgayTGTT_MaskedEditExtender3" runat="server" TargetControlID="txtNgayTGTT" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="True" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="₫" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="," CultureThousandsPlaceholder="." CultureTimePlaceholder=":" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Tư cách tham gia tố tụng
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("TENTUCACH") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="header"></HeaderStyle>
                            <ItemStyle CssClass="chan"></ItemStyle>
                            <PagerStyle Visible="false"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMsgTGTT" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="cmdTGTT" runat="server" CssClass="buttoninput"
                            Text="Lưu thông tin" OnClick="cmdTGTT_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--Sự kiện load lại danh sách điều khoản khi tắt popup-->
    <div style="display: none">
        <asp:Button ID="cmdLoadDsToiDanh" runat="server"
            Text="Load ds toidanh" OnClick="cmdLoadDsToiDanh_Click" />
    </div>
    <script type="text/javascript">
        <!--Sự kiện load lại danh sách điều khoản khi tắt popup-->
        function LoadDsToiDanh() {
            $("#<%= cmdLoadDsToiDanh.ClientID %>").click();
        }

        function uploadComplete(sender) {
            //__doPostBack('tctl00$UpdatePanel1', '');
            __doPostBack('<%= dgFile.UniqueID %>', '');
        }

    </script>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
            CheckKyso();
        }
    </script>
    <script type="text/javascript">
        var count_file = 0;
        function CheckKyso() {
            var chkKySo = document.getElementById('<%=chkKySo.ClientID%>');

            if (chkKySo.checked) {
                document.getElementById('zonekyso').style.display = "block";
                document.getElementById('zonekythuong').style.display = "none";
            }
            else {
                document.getElementById('zonekyso').style.display = "none";
                document.getElementById('zonekythuong').style.display = "block";
            }
        }
        function VerifyPDFCallBack(rv) {

        }

        function exc_verify_pdf1() {
            var prms = {};
            var hddSession = document.getElementById('<%=hddSessionID.ClientID%>');
            prms["SessionId"] = "";
            prms["FileName"] = document.getElementById("file1").value;

            var json_prms = JSON.stringify(prms);

            vgca_verify_pdf(json_prms, VerifyPDFCallBack);
        }

        function SignFileCallBack1(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                var hddFilePath = document.getElementById('<%=hddFilePath.ClientID%>');
                var new_item = document.createElement("li");
                new_item.innerHTML = received_msg.FileName;
                hddFilePath.value = received_msg.FileServer;
                //-------------Them icon xoa file------------------
                var del_item = document.createElement("img");
                del_item.src = '/UI/img/xoa.gif';
                del_item.style.width = "15px";
                del_item.style.margin = "5px 0 0 5px";
                del_item.onclick = function () {
                    if (!confirm('Bạn muốn xóa file này?')) return false;
                    document.getElementById("file_name").removeChild(new_item);
                }
                del_item.style.cursor = 'pointer';
                new_item.appendChild(del_item);

                document.getElementById("file_name").appendChild(new_item);
                $("#<%= cmdThemFileTL.ClientID %>").click();
            } else {
                document.getElementById("_signature").value = received_msg.Message;
            }
        }

        //metadata có kiểu List<KeyValue> 
        //KeyValue là class { string Key; string Value; }
        function exc_sign_file1() {
            var prms = {};
            var scv = [{ "Key": "abc", "Value": "abc" }];
            var hddURLKS = document.getElementById('<%=hddURLKS.ClientID%>');
            prms["FileUploadHandler"] = hddURLKS.value.replace(/^http:\/\//i, window.location.protocol + '//');
            prms["SessionId"] = "";
            prms["FileName"] = "";
            prms["MetaData"] = scv;
            var json_prms = JSON.stringify(prms);
            vgca_sign_file(json_prms, SignFileCallBack1);
        }
        function RequestLicenseCallBack(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                document.getElementById("_signature").value = received_msg.LicenseRequest;
            } else {
                alert("Ký số không thành công:" + received_msg.Status + ":" + received_msg.Error);
            }
        }

    </script>
    <script type="text/javascript">
        function myFunctionFocus() {
            var cmdTGTT = document.getElementById('<%= cmdTGTT.ClientID%>');
            cmdTGTT.style.marginBottom = "120px";
            window.scrollTo(0, document.body.scrollHeight);
        }

        function validate() {
            var msg = "";
            var ddlQHPLTK = document.getElementById('<%= ddlQHPLTK.ClientID%>');
            var value_change = ddlQHPLTK.options[ddlQHPLTK.selectedIndex].value;
            if (value_change == "0") {
                alert('Bạn chưa chọn mục "QHPL dùng cho thống kê". Hãy kiểm tra lại!');
                ddlQHPLTK.focus();
                return false;
            }
            //----------------------
            var txtSobanan = document.getElementById('<%=txtSobanan.ClientID%>');
            if (!Common_CheckTextBox(txtSobanan, "Số bản án")) {
                // alert('Bạn chưa nhập "Số bản án".Hãy kiểm tra lại!');
                //txtSobanan.focus();
                return false;
            }

            var txtNgaymophientoa = document.getElementById('<%=txtNgaymophientoa.ClientID%>');
            if (!CheckDateTimeControl(txtNgaymophientoa, 'Ngày mở phiên tòa'))
                return false;

            var hddNgayQDXX = document.getElementById('<%=hddNgayQDXX.ClientID%>');
            if (hddNgayQDXX.value != "") {
                if (!SoSanh2Date(txtNgaymophientoa, "Ngày mở phiên tòa", hddNgayQDXX.value, 'ngày Quyết định đưa vụ án ra xét xử (' + hddNgayQDXX.value + ')'))
                    return false;
            }

            //--------------------
            var txtNgaytuyenan = document.getElementById('<%=txtNgaytuyenan.ClientID%>');
            if (!CheckDateTimeControl(txtNgaytuyenan, 'Ngày tuyên án'))
                return false;

            if (!SoSanh2Date(txtNgaytuyenan, 'Ngày tuyên án', txtNgaymophientoa.value, "Ngày mở phiên tòa"))
                return false;
            //---------------------

            var txtNgayhieuluc = document.getElementById('<%=txtNgayhieuluc.ClientID%>');
            if (Common_CheckEmpty(txtNgayhieuluc.value)) {
                if (!CheckDateTimeControl(txtNgayhieuluc, 'Ngày hiệu lực'))
                    return false;

                if (!SoSanh2Date(txtNgayhieuluc, 'Ngày hiệu lực', txtNgaytuyenan.value, "Ngày tuyên án"))
                    return false;
            }

            if (!validate_CheckRadio())
                return false;
            //-----------------------------
            return true;
        }
        function validate_CheckRadio() {
            //----------------------
            var rdbAnle = document.getElementById('<%=rdbAnle.ClientID%>');
            msg = 'Mục "Có áp dụng Án lệ" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdbAnle, msg))
                return false;
            //-------------------------
            var rdbVKSThamgia = document.getElementById('<%=rdbVKSThamgia.ClientID%>');
            msg = 'Mục "Có VKS tham gia" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdbVKSThamgia, msg))
                return false;
            //-----------------------------
            var rdVuAnQuaHan = document.getElementById('<%=rdVuAnQuaHan.ClientID%>');
            msg = 'Mục "Vụ án quá hạn luật định" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdVuAnQuaHan, msg))
                return false;
            var inputs = rdVuAnQuaHan.getElementsByTagName('input');
            var selected = 0;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    selected = inputs[i].value;
                    break;
                }
            }
            if (selected == 1) {
                var rdNNChuQuan = document.getElementById('<%=rdNNChuQuan.ClientID%>');
                msg = 'Mục "Nguyên nhân chủ quan" bắt buộc phải chọn. Hãy kiểm tra lại!';
                if (!CheckChangeRadioButtonList(rdNNChuQuan, msg))
                    return false;
                //-----------------------------
                var rdNNKhachQuan = document.getElementById('<%=rdNNKhachQuan.ClientID%>');
                msg = 'Mục "Nguyên nhân khách quan" bắt buộc phải chọn. Hãy kiểm tra lại!';
                if (!CheckChangeRadioButtonList(rdNNKhachQuan, msg))
                    return false;
            }
            return true;
        }
        function validate_them_dieuluat() {
            var ddlBoLuat = document.getElementById('<%=ddlBoLuat.ClientID%>');
            value_change = ddlBoLuat.options[ddlBoLuat.selectedIndex].value;
            if (value_change == "0") {
                alert('Bạn chưa chọn bộ luật. Hãy kiểm tra lại!');
                ddlBoLuat.focus();
                return false;
            }

            <%--var txtKhoan = document.getElementById('<%=txtKhoan.ClientID%>');
            if (!Common_CheckTextBox(txtKhoan, "Khoản")) {
                //alert('Bạn chưa nhập mục "Khoản". Hãy kiểm tra lại!');
                //txtKhoan.focus();
                return false;
            }

            var txtDieu = document.getElementById('<%=txtDieu.ClientID%>');
            if (!Common_CheckTextBox(txtDieu, "Điều")) {
                //alert('Bạn chưa nhập mục "Điều". Hãy kiểm tra lại!');
                //txtDieu.focus();
                return false;
            }--%>
            return true;
        }
        //hiển thị popup chọn điều khoản
        function popupChonDieuKhoan() {
            var width = 950;
            var height = 750;
            var link = "/QLAN/ADS/Sotham/Popup/pChonDieuKhoan.aspx?hsID=<%=DSID%>";
            PopupCenter(link, "Chọn điều khoản áp dụng", width, height);
        }
    </script>
</asp:Content>

