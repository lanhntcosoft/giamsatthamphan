﻿using BL.GSTP;
using BL.GSTP.ADS;
using BL.GSTP.AHS;
using BL.GSTP.Danhmuc;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.AHS.SoTham
{
    public partial class Quyetdinhvuan : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public Decimal VuAnID = 0;
        public string NgaySoSanh;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VuAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
                if (!IsPostBack)
                {
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                    if (VuAnID == 0)
                        Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");
                    txtNgayQD.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    //LoadCombobox();
                    LoadQD();
                    LoadNguoiKyInfo();
                    CheckQuyen();
                    hddPageIndex.Value = "1";
                    dgList.CurrentPageIndex = 0;
                    LoadGrid();
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        void CheckQuyen()
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdLammoi, oPer.CAPNHAT);

            //Check quyết định sửa chữa, bổ sung bản án
            if (ddlQuyetdinh.SelectedItem.Text.Contains("29-HS"))
            {
                lbthongbao.Text = "";
            }
            else
            {
                AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    hddIsSuaDoi.Value = "0";
                    lbthongbao.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLammoi, false);
                    hddShowCommand.Value = "False";
                    return;
                }
                else
                    hddIsSuaDoi.Value = "1";
                try
                {
                    AHS_SOTHAM_THULY objTL = dt.AHS_SOTHAM_THULY.Where(x => x.VUANID == VuAnID).OrderByDescending(y => y.NGAYTHULY).FirstOrDefault<AHS_SOTHAM_THULY>();
                    if (objTL != null)
                        NgaySoSanh = ((DateTime)objTL.NGAYTHULY).ToString("dd/MM/yyyy", cul);
                }
                catch { }
                if (!Check_Phancongthamphan())
                {
                    lbthongbao.Text = "Vụ án chưa được phân công thẩm phán. Đề nghị cập nhật thông tin 'Phân công thẩm phán giải quyết' !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLammoi, false);
                }
                AHS_SOTHAM_KHANGCAO kc = dt.AHS_SOTHAM_KHANGCAO.Where(x => x.VUANID == VuAnID).FirstOrDefault();
                if (kc != null)
                {
                    lbthongbao.Text = "Vụ án đã có kháng cáo. Không được sửa đổi.";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLammoi, false);
                    hddShowCommand.Value = "False";
                    return;
                }
                AHS_SOTHAM_KHANGNGHI kn = dt.AHS_SOTHAM_KHANGNGHI.Where(x => x.VUANID == VuAnID).FirstOrDefault();
                if (kn != null)
                {
                    lbthongbao.Text = "Vụ án đã có kháng nghị. Không được sửa đổi.";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLammoi, false);
                    hddShowCommand.Value = "False";
                    return;
                }
                string StrMsg = "Không được sửa đổi thông tin.";
                string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
                if (Result != "")
                {
                    lbthongbao.Text = Result;
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdLammoi, false);
                    hddShowCommand.Value = "False";
                    return;
                }
            }
        }

        Boolean Check_Phancongthamphan()
        {
            try
            {
                /* Trong BanAn can check co ChuToaPhien toa, trong cac quyet dinh chi can check ThamphanGiaiQuyet*/
                List<AHS_THAMPHANGIAIQUYET> lst = dt.AHS_THAMPHANGIAIQUYET.Where(x => x.VUANID == VuAnID
                                && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM).ToList<AHS_THAMPHANGIAIQUYET>();
                if (lst != null && lst.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception exx) { return false; }
        }

        //private void LoadCombobox()
        //{
        //    ddlLoaiQD.Items.Clear();
        //    decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);

        //    // Load loại quyết định và quyết định
        //    ddlLoaiQD.DataSource = dt.DM_QD_LOAI.Where(x => x.HIEULUC == 1 && x.ISHINHSU == 1).OrderBy(y => y.THUTU).ToList();
        //    ddlLoaiQD.DataTextField = "TEN";
        //    ddlLoaiQD.DataValueField = "ID";
        //    ddlLoaiQD.DataBind();
        //    ddlLoaiQD.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
        //    LoadQD();
        //}

        private void LoadQD()
        {
            // Quyết định bắt tạm giam không hiển thị ở quyết định vụ án (ISDUONGSUYEUCAU=1)
            decimal LoaiQD = 0;
            DM_QD_LOAI objLoaiQD = dt.DM_QD_LOAI.Where(x => x.HIEULUC == 1 && x.ISHINHSU == 1 && x.ISDUONGSUYEUCAU == 1).FirstOrDefault();
            if (objLoaiQD != null)
            {
                LoaiQD = objLoaiQD.ID;
            }
            ddlQuyetdinh.Items.Clear();
            List<DM_QD_QUYETDINH> lst = dt.DM_QD_QUYETDINH.Where(x => x.LOAIID != LoaiQD && x.ISHINHSU == 1 && x.ISSOTHAM == 1).OrderBy(y => y.TEN).ToList();
            if (lst != null && lst.Count > 0)
            {
                ddlQuyetdinh.DataSource = lst;
                ddlQuyetdinh.DataTextField = "TEN";
                ddlQuyetdinh.DataValueField = "ID";
                ddlQuyetdinh.DataBind();
            }
            ddlQuyetdinh.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            ddlQuyetdinh.SelectedIndex = 0;
            LoadLydo();
        }

        private void LoadLydo()
        {
            ddlLydo.Items.Clear(); ddlLydo_BM03.Items.Clear();
            decimal ID = Convert.ToDecimal(ddlQuyetdinh.SelectedValue);
            List<DM_QD_QUYETDINH_LYDO> lst = dt.DM_QD_QUYETDINH_LYDO.Where(x => x.QDID == ID & x.HIEULUC == 1).OrderBy(y => y.THUTU).ToList();

            if (ddlQuyetdinh.SelectedItem.Text.Contains("03-HS"))
            {
                LoadNguoiDuocPhanCong();
                LoadNguoiBiThayDoi();
                pnLyDo_BM03.Visible = true;
                pnLyDo.Visible = false;
                pntxtLydo.Visible = false;
            }
            else if (ddlQuyetdinh.Text == "201" || ddlQuyetdinh.Text == "202")
            {
                pnLyDo.Visible = false;
                pntxtLydo.Visible = true;
                pnLyDo_BM03.Visible = false;
                lbtxtLydo.InnerText = "Lý do";
            }
            else if (lst != null && lst.Count > 0)
            {
                pnLyDo.Visible = true;
                pnLyDo_BM03.Visible = false;
                pntxtLydo.Visible = false;
            }
            else
            {
                pnLyDo_BM03.Visible = false;
                pnLyDo.Visible = false;
                pntxtLydo.Visible = false;
            }

            ddlLydo.DataSource = ddlLydo_BM03.DataSource = lst;
            ddlLydo.DataTextField = ddlLydo_BM03.DataTextField = "TEN";
            ddlLydo.DataValueField = ddlLydo_BM03.DataValueField = "ID";
            ddlLydo.DataBind(); ddlLydo_BM03.DataBind();

            ddlLydo.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            ddlLydo.SelectedIndex = 0;
            ddlLydo_BM03.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            ddlLydo_BM03.SelectedIndex = 0;

        }

        private void LoadNguoiDuocPhanCong()
        {
            ddlNguoiDuocPC.Items.Clear();
            DM_CANBO_BL objBL = new DM_CANBO_BL();
            string tucach = ddlThayDoi.SelectedValue == "1" ? "TP" : (ddlThayDoi.SelectedValue == "2" ? "HTND" : "THUKY");
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DataTable tbl = objBL.DMCANBO_DUOCPC_BM03HS_ST(ToaAnID, VuAnID, tucach);
            if (tbl.Rows.Count > 0)
            {
                ddlNguoiDuocPC.DataSource = tbl;
                ddlNguoiDuocPC.DataTextField = "MA_TEN";
                ddlNguoiDuocPC.DataValueField = "ID";
                ddlNguoiDuocPC.DataBind();
            }
            else
            {
                ddlNguoiDuocPC.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            }
        }

        private void LoadNguoiBiThayDoi()
        {
            ddlNguoiBiThayDoi.Items.Clear();

            DM_CANBO_BL objBL = new DM_CANBO_BL();
            string tucach = ddlThayDoi.SelectedValue == "1" ? "TP" : (ddlThayDoi.SelectedValue == "2" ? "HTND" : "THUKY");
            DataTable tbl = objBL.DMCANBO_NGUOIBITHAYDOI_TCTT_ST(VuAnID, tucach);
            if (tbl.Rows.Count > 0)
            {
                ddlNguoiBiThayDoi.DataSource = tbl;
                ddlNguoiBiThayDoi.DataTextField = "HOTEN";
                ddlNguoiBiThayDoi.DataValueField = "CANBOID";
                ddlNguoiBiThayDoi.DataBind();
            }
            else
            {
                ddlNguoiBiThayDoi.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            }
        }

        private void LoadNguoiKyInfo()
        {
            decimal DonID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            DM_CANBO_BL cb_BL = new DM_CANBO_BL();
            AHS_SOTHAM_HDXX oND = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == DonID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).FirstOrDefault<AHS_SOTHAM_HDXX>();
            if (oND != null)
            {
                decimal CanBoID = Convert.ToDecimal(oND.CANBOID.ToString());
                DataTable dtCanBo = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                if (dtCanBo.Rows.Count > 0)
                {
                    txtNguoiKy.Text = dtCanBo.Rows[0]["HOTEN"].ToString();
                    txtChucvu.Text = dtCanBo.Rows[0]["ChucVu"].ToString();
                    hddNguoiKyID.Value = dtCanBo.Rows[0]["ID"].ToString();
                }
            }
            else
            {
                AHS_THAMPHANGIAIQUYET oTP = dt.AHS_THAMPHANGIAIQUYET.Where(x => x.VUANID == DonID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM).FirstOrDefault();
                if (oTP != null)
                {
                    decimal CanBoID = Convert.ToDecimal(oTP.CANBOID.ToString());
                    DataTable dtCanBo = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                    if (dtCanBo.Rows.Count > 0)
                    {
                        txtNguoiKy.Text = dtCanBo.Rows[0]["HOTEN"].ToString();
                        txtChucvu.Text = dtCanBo.Rows[0]["ChucVu"].ToString();
                        hddNguoiKyID.Value = dtCanBo.Rows[0]["ID"].ToString();
                    }
                }
                else
                    txtNguoiKy.Text = txtChucvu.Text = "";
            }
        }

        private void ResetControls()
        {
            txtLydo.Text = null;
            txtNgayQD.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //ddlLoaiQD.SelectedIndex = 0;
            ddlQuyetdinh.SelectedIndex = 0;
            ddlLydo.SelectedIndex = 0;
            ddlLydo_BM03.SelectedIndex = 0;
            pnLyDo.Visible = false;
            pnLyDo_BM03.Visible = false;
            pntxtLydo.Visible = false;

            lbthongbao.Text = txtSoQD.Text = txtHieulucTuNgay.Text = txtHieuLucDenNgay.Text = "";
            //txtHanTheoLuatThang.Text = txtHanTheoLuatNgay.Text = "";
            //txtTheoLuatNgayKetThuc.Text = txtThucTeThang.Text = txtThucTeNgay.Text = txtThucTeNgayKetThuc.Text = "";
            hddid.Value = "0";
            hddFilePath.Value = "";
            lbtDownload.Visible = false;
        }

        private bool CheckValid()
        {
            //if (txtNguoiKy.Text.Trim() == "")
            //{
            //    lbthongbao.Text = "Vụ án chưa phân công thẩm phán giải quyết!";
            //    return false;
            //}
            if (ddlQuyetdinh.SelectedValue == "0")
            {
                lbthongbao.Text = "Bạn chưa chọn quyết định!";
                ddlQuyetdinh.Focus();
                return false;
            }

            //----------------------------
            if (!String.IsNullOrEmpty(txtSoQD.Text) && !String.IsNullOrEmpty(txtNgayQD.Text))
            {
                string so = txtSoQD.Text;
            
                DateTime ngay = DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal LoaiQD = Convert.ToDecimal(ddlQuyetdinh.SelectedValue);
                Decimal CheckID = oSTBL.CHECK_SQDTheoLoaiAn(DonViID, "AHS", so, ngay, LoaiQD);

                if (CheckID > 0)
                {
                    String strMsg = "";
                    String STTNew = oSTBL.GET_SQD_NEW(DonViID, "AHS", ngay, LoaiQD).ToString();
                    Decimal CurrID = (string.IsNullOrEmpty(hddid.Value)) ? 0 : Convert.ToDecimal(hddid.Value);
                    if (CheckID != CurrID)
                    {
                        strMsg = "Số Quyết định " + txtSoQD.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSoQD.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSoQD.Focus();
                        return false;
                    }
                }
            }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                AHS_SOTHAM_QUYETDINH_VUAN oND;
                if (hddid.Value == "" || hddid.Value == "0")
                    oND = new AHS_SOTHAM_QUYETDINH_VUAN();
                else
                {
                    decimal ID = Convert.ToDecimal(hddid.Value);
                    oND = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.ID == ID).FirstOrDefault();
                    if (oND.DONVIID.ToString() != Session[ENUM_SESSION.SESSION_DONVIID].ToString())
                    {
                        lbthongbao.Text = "Quyết định đang chọn thuộc thẩm quyền của tòa án khác, không được phép thay đổi !";
                        return;
                    }
                }
                oND.VUANID = VuAnID;
                oND.NGAYMOPT = (String.IsNullOrEmpty(txtNgayMoPhienToa.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayMoPhienToa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.DIADIEMMOPT = txtDiaDiem.Text.Trim();
                //--------------------------------
                try
                {
                    oND.QUYETDINHID = Convert.ToDecimal(ddlQuyetdinh.SelectedValue);
                    DM_QD_QUYETDINH objQD = dt.DM_QD_QUYETDINH.Where(x => x.ID == oND.QUYETDINHID).SingleOrDefault();
                    if (objQD != null)
                        oND.LOAIQDID = objQD.LOAIID;
                }
                catch (Exception exx) { }

                set_valueLydo(oND);

                if (pnLyDo_BM03.Visible)
                {
                    oND.LYDOID = Convert.ToDecimal(ddlLydo_BM03.SelectedValue);
                    // Update Người tiến hành tố tụng
                    decimal CanBoBiThay = Convert.ToDecimal(ddlNguoiBiThayDoi.SelectedValue),
                        CanBoDuocPC = Convert.ToDecimal(ddlNguoiDuocPC.SelectedValue);
                    if (CanBoBiThay > 0)
                    {
                        AHS_SOTHAM_HDXX hdxx = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == VuAnID && x.CANBOID == CanBoBiThay).FirstOrDefault();
                        if (hdxx != null)
                        {
                            hdxx.ISTHAYDOI = 1;
                            bool isNew = false;
                            AHS_SOTHAM_HDXX obj = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == VuAnID && x.CANBOID == CanBoDuocPC).FirstOrDefault();
                            if (obj == null)
                            {
                                isNew = true;
                                obj = new AHS_SOTHAM_HDXX();
                            }
                            obj.CANBOID = CanBoDuocPC;
                            obj.DUKHUYET = hdxx.DUKHUYET;
                            obj.FILEID = hdxx.FILEID;
                            obj.MAVAITRO = hdxx.MAVAITRO;
                            obj.NGAYQD = (String.IsNullOrEmpty(txtNgayQD.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                            obj.NGAYPHANCONG = obj.NGAYQD;
                            obj.NGUOIPHANCONGID = hdxx.NGUOIPHANCONGID;
                            obj.SOQD = txtSoQD.Text;
                            obj.VUANID = VuAnID;
                            obj.NGAYTAO = DateTime.Now;
                            obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                            if (isNew)
                            {
                                dt.AHS_SOTHAM_HDXX.Add(obj);
                            }
                            dt.SaveChanges();
                        }
                    }
                    oND.THAYDOITCTT = Convert.ToDecimal(ddlThayDoi.SelectedValue);
                    oND.NGUOIDUOCPHANCONG = Convert.ToDecimal(ddlNguoiDuocPC.SelectedValue);
                    oND.NGUOIBITHAY = Convert.ToDecimal(ddlNguoiBiThayDoi.SelectedValue);
                }
                oND.LOAIDONVI = 0;
                oND.DONVIID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                oND.SOQUYETDINH = txtSoQD.Text;
                oND.NGAYQD = (String.IsNullOrEmpty(txtNgayQD.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.HIEULUCTU = (String.IsNullOrEmpty(txtHieulucTuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtHieulucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.HIEULUCDEN = (String.IsNullOrEmpty(txtHieuLucDenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtHieuLucDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                //oND.THEOLUAT_THANG = Cls_Comon.GetNumber(txtHanTheoLuatThang.Text);
                //oND.THEOLUAT_NGAY = Cls_Comon.GetNumber(txtHanTheoLuatNgay.Text);
                //oND.THEOLUAT_NGAYKETTHUC = (String.IsNullOrEmpty(txtTheoLuatNgayKetThuc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTheoLuatNgayKetThuc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                //oND.THUCTE_THANG = Cls_Comon.GetNumber(txtThucTeThang.Text);
                //oND.THUCTE_NGAY = Cls_Comon.GetNumber(txtThucTeNgay.Text);
                //oND.THUCTE_NGAYKETTHUC = (String.IsNullOrEmpty(txtThucTeNgayKetThuc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtThucTeNgayKetThuc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGUOIKYID = Convert.ToDecimal(hddNguoiKyID.Value);
                DM_QD_QUYETDINH oQDT = dt.DM_QD_QUYETDINH.Where(x => x.ID == oND.QUYETDINHID).FirstOrDefault();
                if (oQDT != null)
                {
                    oND.FILEID = UploadFileID(VuAnID, oQDT.MA);
                }
                oND.CHUCVU = txtChucvu.Text;
                if (hddid.Value == "" || hddid.Value == "0")
                {
                    oND.NGAYTAO = DateTime.Now;
                    oND.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.AHS_SOTHAM_QUYETDINH_VUAN.Add(oND);
                }
                else
                {
                    oND.NGAYSUA = DateTime.Now;
                    oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                }

                if (oND.LOAIQDID == 3)
                {
                    // update giai đoạn vụ án = sotham
                    AHS_VUAN objAn = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault<AHS_VUAN>();
                    if (objAn != null)
                    {
                       // objAn.MAGIAIDOAN = ENUM_GIAIDOANVUAN.DINHCHI;
                        objAn.NGAYSUA = DateTime.Now;
                        objAn.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    }
                }

                dt.SaveChanges();
                dgList.CurrentPageIndex = 0;
                LoadGrid();
                ResetControls();
                lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lbthongbao.Text = ex.Message;
            }
        }

        public void LoadGrid()
        {
            AHS_SOTHAM_BL oBL = new AHS_SOTHAM_BL();
            decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            DataTable oDT = oBL.AHS_ST_QD_VUAN_GETLIST(ID);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                #region "Xác định số lượng trang"
                int Total = oDT.Rows.Count;
                hddTotalPage.Value = Cls_Comon.GetTotalPage(Total, dgList.PageSize).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + Total.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
                dgList.DataSource = oDT;
                dgList.DataBind();
                pndata.Visible = true;
            }
            else
            {
                pndata.Visible = false;
            }
        }

        protected void btnLammoi_Click(object sender, EventArgs e)
        {
            try
            {
                ResetControls();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        public void xoa(decimal id)
        {
            AHS_SOTHAM_QUYETDINH_VUAN oND = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.ID == id).FirstOrDefault();
            if (oND != null)
            {
                if (oND.DONVIID.ToString() != Session[ENUM_SESSION.SESSION_DONVIID].ToString())
                {
                    lbthongbao.Text = "Quyết định đang chọn thuộc thẩm quyền của tòa án khác, không được phép xóa !";
                    return;
                }
                if (oND.LOAIQDID == 3)
                {
                    AHS_VUAN objAn = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault<AHS_VUAN>();
                    if (objAn != null)
                    {
                        objAn.MAGIAIDOAN = ENUM_GIAIDOANVUAN.SOTHAM;
                        objAn.NGAYSUA = DateTime.Now;
                        objAn.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.SaveChanges();
                    }
                }
                AHS_FILE file = dt.AHS_FILE.Where(x => x.ID == oND.FILEID).FirstOrDefault();
                if (file != null)
                {
                    dt.AHS_FILE.Remove(file);
                }
                dt.AHS_SOTHAM_QUYETDINH_VUAN.Remove(oND);
                dt.SaveChanges();
            }
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid();
            ResetControls();
            lbthongbao.Text = "Xóa thành công!";
        }

        public void loadedit(decimal ID)
        {
            lbthongbao.Text = "";
            AHS_SOTHAM_QUYETDINH_VUAN oND = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.ID == ID).FirstOrDefault();
            hddid.Value = oND.ID.ToString();
            txtDiaDiem.Text = oND.DIADIEMMOPT + "";
            if (oND.NGAYMOPT != null) txtNgayMoPhienToa.Text = ((DateTime)oND.NGAYMOPT).ToString("dd/MM/yyyy", cul);
            // if (oND.LOAIQDID != null) ddlLoaiQD.SelectedValue = oND.LOAIQDID.ToString();
            //LoadQD();
            if (oND.QUYETDINHID != null)
                ddlQuyetdinh.SelectedValue = oND.QUYETDINHID.ToString();
            LoadLydo();
            if (ddlQuyetdinh.SelectedItem.Text.Contains("03-HS"))
            {
                if (oND.LYDOID + "" != "")
                    ddlLydo_BM03.SelectedValue = oND.LYDOID.ToString();
            }
            else
            {
                //if (oND.LYDOID + "" != "")
                //ddlLydo.SelectedValue = oND.LYDOID.ToString();
                get_valueLydo(ID);
            }
            txtSoQD.Text = oND.SOQUYETDINH;
            txtNgayQD.Text = string.IsNullOrEmpty(oND.NGAYQD + "") ? "" : ((DateTime)oND.NGAYQD).ToString("dd/MM/yyyy", cul);
            txtHieulucTuNgay.Text = string.IsNullOrEmpty(oND.HIEULUCTU + "") ? "" : ((DateTime)oND.HIEULUCTU).ToString("dd/MM/yyyy", cul);
            txtHieuLucDenNgay.Text = string.IsNullOrEmpty(oND.HIEULUCDEN + "") ? "" : ((DateTime)oND.HIEULUCDEN).ToString("dd/MM/yyyy", cul);
            if (txtHieuLucDenNgay.Text == "")
            {
                decimal QDID = oND.QUYETDINHID + "" == "" ? 0 : (decimal)oND.QUYETDINHID;
                DM_QD_QUYETDINH oT = dt.DM_QD_QUYETDINH.Where(x => x.ID == QDID).FirstOrDefault();
                if (oT != null)
                {
                    hddThoiHanThang.Value = oT.THOIHAN_THANG == null ? "0" : oT.THOIHAN_THANG.ToString();
                    hddThoiHanNgay.Value = oT.THOIHAN_NGAY == null ? "0" : oT.THOIHAN_NGAY.ToString();
                    //ddlLoaiQD.SelectedValue = oT.LOAIID + "";
                }
            }
            DM_CANBO cbo = dt.DM_CANBO.Where(x => x.ID == oND.NGUOIKYID).FirstOrDefault<DM_CANBO>();
            if (cbo != null)
            {
                txtNguoiKy.Text = cbo.HOTEN;
            }
            txtChucvu.Text = oND.CHUCVU;
            AHS_FILE file = dt.AHS_FILE.Where(x => x.ID == oND.FILEID).FirstOrDefault();
            if (file != null)
            {
                if (file.TENFILE + "" != "")
                {
                    lbtDownload.Visible = true;
                    lbtDownload.Text = "Tải tệp đính kèm: " + file.TENFILE;
                }
            }
            else
                lbtDownload.Visible = false;

            if (ddlQuyetdinh.SelectedItem.Text.Contains("03-HS"))
            {
                ddlThayDoi.SelectedValue = oND.THAYDOITCTT.ToString();
                LoadNguoiDuocPhanCong();
                LoadNguoiBiThayDoi();
                pnLyDo_BM03.Visible = true;
                pnLyDo.Visible = false;
                pntxtLydo.Visible = false;
                ddlNguoiDuocPC.SelectedValue = oND.NGUOIDUOCPHANCONG.ToString();
                ddlNguoiBiThayDoi.SelectedValue = oND.NGUOIBITHAY.ToString();
            }
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
                switch (e.CommandName)
                {
                    case "Download":
                        AHS_FILE oND = dt.AHS_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                        if (oND.TENFILE != "")
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        break;
                    case "Sua":
                        loadedit(ND_id);
                        hddid.Value = e.CommandArgument.ToString();
                        break;
                    case "Xoa":
                        MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                        if (oPer.XOA == false)
                        {
                            lbthongbao.Text = "Bạn không có quyền xóa!";
                            return;
                        }
                        if (hddIsSuaDoi.Value == "0")
                        {
                            lbthongbao.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi!";
                            return;
                        }
                        string StrMsg = "Không được sửa đổi thông tin.";
                        string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
                        if (Result != "")
                        {
                            lbthongbao.Text = Result;
                            return;
                        }
                        xoa(ND_id);
                        break;
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        #region #Phân trang
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
                hddPageIndex.Value = lbCurrent.Text;
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        #endregion

        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AsyncFileUpLoad.HasFile)
                {
                    string strFileName = AsyncFileUpLoad.FileName;
                    string path = Server.MapPath("~/TempUpload/") + strFileName;
                    AsyncFileUpLoad.SaveAs(path);
                    path = path.Replace("\\", "/");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void lbtDownload_Click(object sender, EventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(hddid.Value);
                AHS_SOTHAM_QUYETDINH_VUAN oND = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND != null)
                {
                    AHS_FILE file = dt.AHS_FILE.Where(x => x.ID == oND.FILEID).FirstOrDefault();
                    if (file != null)
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: file.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + file.TENFILE + "&Extension=" + file.KIEUFILE + "';", true);
                    }
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        //protected void ddlLoaiQD_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LoadQD();
        //        ddlQuyetdinh_SelectedIndexChanged(sender, e);

        //    }
        //    catch (Exception ex) { lbthongbao.Text = ex.Message; }
        //}

        protected void ddlQuyetdinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(ddlQuyetdinh.SelectedValue);
                //DM_QD_QUYETDINH oT = dt.DM_QD_QUYETDINH.Where(x => x.ID == ID).FirstOrDefault();
                //if (oT != null)
                //{
                //    ddlLoaiQD.SelectedValue = oT.LOAIID + "";
                //}
                //Load số quyêt định với các loại QD Hinh Su sau
                if (ID == 77 || ID == 78 || ID == 79 || ID == 80 || ID == 128|| ID ==206 || ID == 4 || ID == 61)
                {
                    // lấy số mới nhất 
                    Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                    DateTime ngayQD;
                    if (txtNgayQD.Text != "")
                        ngayQD = DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    else
                        ngayQD = DateTime.Now;
                    ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                    String STTNew = oSTBL.GET_SQD_NEW(DonViID, "AHS", ngayQD, ID).ToString();
                    txtSoQD.Text = STTNew;
                }
                LoadLydo();

                //Check quyết định sửa chữa, bổ sung bản án
                CheckQuyen();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void txtHieulucTuNgay_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtHieulucTuNgay.Text != "")
                {
                    DateTime dFrom = DateTime.Parse(this.txtHieulucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (dFrom != DateTime.MinValue)
                    {
                        int SoThangTheoLuat = Convert.ToInt32(hddThoiHanThang.Value), SoNgayTheoLuat = Convert.ToInt32(hddThoiHanNgay.Value);
                        dFrom = dFrom.AddMonths(SoThangTheoLuat);
                        dFrom = dFrom.AddDays(SoNgayTheoLuat);
                        txtHieuLucDenNgay.Text = dFrom.ToString("dd/MM/yyyy", cul);
                    }
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void ddlThayDoi_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadNguoiDuocPhanCong();
                LoadNguoiBiThayDoi();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void ddlNguoiDuocPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { LoadNguoiBiThayDoi(); }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        //protected void txtThucTeThang_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtHieulucTuNgay.Text != "")
        //    {
        //        DateTime dFrom = DateTime.Parse(this.txtHieulucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
        //        if (dFrom != DateTime.MinValue)
        //        {
        //            int SoThang = 0, SoNgay = 0;
        //            if (txtThucTeThang.Text != "") SoThang = Convert.ToInt32(txtThucTeThang.Text);
        //            if (txtThucTeNgay.Text != "") SoNgay = Convert.ToInt32(txtThucTeNgay.Text);
        //            dFrom = dFrom.AddMonths(SoThang);
        //            dFrom = dFrom.AddDays(SoNgay);
        //            txtThucTeNgayKetThuc.Text = dFrom.ToString("dd/MM/yyyy", cul);
        //        }
        //    }
        //}
        //protected void txtThucTeNgay_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtHieulucTuNgay.Text != "")
        //    {
        //        DateTime dFrom = DateTime.Parse(this.txtHieulucTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
        //        if (dFrom != DateTime.MinValue)
        //        {
        //            int SoThang = 0, SoNgay = 0;
        //            if (txtThucTeThang.Text != "") SoThang = Convert.ToInt32(txtThucTeThang.Text);
        //            if (txtThucTeNgay.Text != "") SoNgay = Convert.ToInt32(txtThucTeNgay.Text);
        //            dFrom = dFrom.AddMonths(SoThang);
        //            dFrom = dFrom.AddDays(SoNgay);
        //            txtThucTeNgayKetThuc.Text = dFrom.ToString("dd/MM/yyyy", cul);
        //        }
        //    }
        //}

        private decimal UploadFileID(decimal VuAnID, string strMaBieumau)
        {
            decimal IDFIle = 0, IDBM = 0;
            AHS_VUAN oVuAn = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oVuAn != null)
            {
                List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
                if (lstBM.Count > 0)
                {
                    IDBM = lstBM[0].ID;
                }
                bool isNew = false;
                AHS_FILE objFile = dt.AHS_FILE.Where(x => x.VUANID == VuAnID && x.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM && x.BIEUMAUID == IDBM).FirstOrDefault();
                if (objFile == null)
                {
                    isNew = true;
                    objFile = new AHS_FILE();
                }
                objFile.VUANID = VuAnID;
                objFile.TOAANID = oVuAn.TOAANID;
                objFile.MAGIAIDOAN = ENUM_GIAIDOANVUAN.SOTHAM;
                objFile.LOAIFILE = 0;
                objFile.BIEUMAUID = IDBM;
                objFile.NAM = DateTime.Now.Year;
                objFile.NGAYTAO = DateTime.Now;
                objFile.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                if (hddFilePath.Value != "")
                {
                    try
                    {
                        string strFilePath = "";
                        if (chkKySo.Checked)
                        {
                            string[] arr = hddFilePath.Value.Split('/');
                            strFilePath = arr[arr.Length - 1];
                            strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                        }
                        else
                            strFilePath = hddFilePath.Value.Replace("/", "\\");

                        byte[] buff = null;
                        using (FileStream fs = File.OpenRead(strFilePath))
                        {
                            BinaryReader br = new BinaryReader(fs);
                            FileInfo oF = new FileInfo(strFilePath);
                            long numBytes = oF.Length;
                            buff = br.ReadBytes((int)numBytes);
                            objFile.NOIDUNG = buff;
                            objFile.TENFILE = Cls_Comon.ChuyenTVKhongDau(oF.Name);
                            objFile.KIEUFILE = oF.Extension;
                        }
                        File.Delete(strFilePath);
                    }
                    catch (Exception ex) { lbthongbao.Text = ex.Message; }
                }
                if (isNew)
                {
                    dt.AHS_FILE.Add(objFile);
                }
                dt.SaveChanges();
                IDFIle = objFile.ID;
            }
            return IDFIle;
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rowView = (DataRowView)e.Item.DataItem;
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                if (hddShowCommand.Value == "False")
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
                ImageButton lblDownload = (ImageButton)e.Item.FindControl("lblDownload");
                if (rowView["TENFILE"] + "" == "")
                {
                    lblDownload.Visible = false;
                }
                else
                {
                    lblDownload.Visible = true;
                }
            }
        }

        protected void set_valueLydo(AHS_SOTHAM_QUYETDINH_VUAN oND)
        {
            if (ddlQuyetdinh.SelectedValue == "201")
            {
                oND.QUYETDINHID = 201;
                oND.LYDO_NAME = txtLydo.Text;
            }
            else if (ddlQuyetdinh.SelectedValue == "202")
            {
                oND.QUYETDINHID = 202;
                oND.LYDO_NAME = txtLydo.Text;
            }
            else if (pnLyDo.Visible)
            {
                oND.LYDOID = Convert.ToDecimal(ddlLydo.SelectedValue);
            }
        }

        protected void get_valueLydo(decimal ID)
        {
            AHS_SOTHAM_QUYETDINH_VUAN oND = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.ID == ID).FirstOrDefault();

            if (oND.QUYETDINHID == 201 || oND.QUYETDINHID == 202)
            {
                lbtxtLydo.InnerText = "Lý do";
                pntxtLydo.Visible = true;

                if (oND.LYDO_NAME != null)
                {
                    txtLydo.Text = oND.LYDO_NAME;
                }
                else if (oND.LYDOID != null)
                {
                    ddlLydo.SelectedValue = oND.LYDOID.ToString();
                    txtLydo.Text = ddlLydo.SelectedItem.Text;
                }
            }
            else if (oND.LYDOID != null && pnLyDo.Visible)
            {
                lbtxtLydo.InnerText = "";
                ddlLydo.SelectedValue = oND.LYDOID.ToString();
            }
        }
    }
}