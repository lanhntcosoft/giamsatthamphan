﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WEB.GSTP.QLAN.AHS.SoTham.BanAnST
{
    public partial class BanAnSoTham : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");

        public string NgaySoSanh;
        public decimal VuAnID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            rdNNQuaHan.Visible = true;
            if (Session[ENUM_LOAIAN.AN_HINHSU] != null)
            {
                VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");

                if (!IsPostBack)
                {
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                    LoadNguoiKyInfo();
                    LoadThongTinBanAnSoTham();
                    LoadDsBiCao_AnPhi();
                    CheckQuyen();
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        void CheckQuyen()
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdateBanAnST, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdUpdateAnPhi, oPer.CAPNHAT);

            //Cls_Comon.SetButton(cmdUpdateThongKe2, oPer.CAPNHAT);
            //Cls_Comon.SetButton(cmdUpdateThongKe, oPer.CAPNHAT);

            //-----------------------------
            Decimal VuAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");

            if (VuAnID == 0)
                Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");

            AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
            {
                lttMsgBanAn.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                pnAnPhi.Enabled = false;
                return;
            }
            else if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.DINHCHI)
            {
                lttMsgBanAn.Text = "Vụ việc đã được đình chỉ, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                pnAnPhi.Enabled = false;
                return;
            }
            //-----------------------------
            if (!Check_GanToiDanhChoBiCao(VuAnID))
                return;

            if (!Check_QDXetXu(VuAnID))
                return;

            //-----------------------------
            if (!Check_Phancongthamphan(VuAnID))
            {
                lttMsgBanAn.Text = "Vụ việc chưa được phân công thẩm phán. Đề nghị cập nhật thông tin 'Phân công thẩm phán giải quyết' !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                pnAnPhi.Enabled = false;
                return;
            }
            //-----------------------------
            try
            {
                List<AHS_SOTHAM_QUYETDINH_VUAN> lstQDST = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.VUANID == VuAnID).OrderByDescending(xy => xy.NGAYQD).ToList<AHS_SOTHAM_QUYETDINH_VUAN>();
                AHS_SOTHAM_QUYETDINH_VUAN objTL = lstQDST[0];
                NgaySoSanh = ((DateTime)objTL.NGAYQD).ToString("dd/MM/yyyy", cul);
            }
            catch
            {
                lttMsgBanAn.Text = "Quyết định vụ án chưa được cập nhật. ";
                lttMsgBanAn.Text += "Đề nghị cập nhật thông tin mục '3.3 Quyết định vụ án'!";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                pnAnPhi.Enabled = false;
                return;
            }
            AHS_SOTHAM_KHANGCAO kc = dt.AHS_SOTHAM_KHANGCAO.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            if (kc != null)
            {
                lttMsgBanAn.Text = "Vụ án đã có kháng cáo. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
            }
            AHS_SOTHAM_KHANGNGHI kn = dt.AHS_SOTHAM_KHANGNGHI.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            if (kn != null)
            {
                lttMsgBanAn.Text = "Vụ án đã có kháng nghị. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
            if (Result != "")
            {
                lttMsgBanAn.Text = Result;
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                return;
            }
        }
        bool Check_QDXetXu(Decimal VuAnID)
        {
            //Kiểm tra xem đã có quyết định đưa vụ việc ra xét xử hay không
            decimal IDLQD = 0;
            DM_QD_LOAI oLQD = dt.DM_QD_LOAI.Where(x => x.MA == "DVARXX").FirstOrDefault();
            if (oLQD != null)
                IDLQD = oLQD.ID;

            List<AHS_SOTHAM_QUYETDINH_VUAN> lst = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.VUANID == VuAnID && x.LOAIQDID == IDLQD).ToList();
            if (lst == null || lst.Count == 0)
            {
                lttMsgBanAn.Text = "Chưa cập nhật quyết định đưa vụ án ra xét xử !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                return false;
            }
            else
                return true;
        }
        bool Check_GanToiDanhChoBiCao(Decimal VuAnID)
        {
            List<AHS_SOTHAM_CAOTRANG_DIEULUAT> lst = dt.AHS_SOTHAM_CAOTRANG_DIEULUAT.Where(x => x.VUANID == VuAnID).ToList();
            if (lst == null || lst.Count == 0)
            {
                lttMsgBanAn.Text = "Các bị cáo trong vụ án chưa được gán tội danh. Đề nghị cập nhật thông tin này !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdUpdateAnPhi, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                return false;
            }
            else
                return true;
        }
        Boolean Check_Phancongthamphan(Decimal VuAnID)
        {
            try
            {
                /* Trong BanAn can check co ChuToaPhien toa, trong cac quyet dinh chi can check ThamphanGiaiQuyet*/
                //List<AHS_THAMPHANGIAIQUYET> lst = dt.AHS_THAMPHANGIAIQUYET.Where(x => x.VUANID == VuAnID
                //                && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_CHUTOASOTHAM ).ToList<AHS_THAMPHANGIAIQUYET>();

                List<AHS_SOTHAM_HDXX> lst = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == VuAnID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).ToList<AHS_SOTHAM_HDXX>();
                if (lst != null && lst.Count > 0)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
        private void LoadNguoiKyInfo()
        {
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            DM_CANBO_BL cb_BL = new DM_CANBO_BL();
            AHS_SOTHAM_HDXX oND = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == VuAnID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).FirstOrDefault<AHS_SOTHAM_HDXX>();
            if (oND != null)
            {
                decimal CanBoID = Convert.ToDecimal(oND.CANBOID.ToString());
                DataTable tbl = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    txtNguoiKy.Text = tbl.Rows[0]["HOTEN"] + "";
                    hddNguoiKyID.Value = tbl.Rows[0]["ID"] + "";
                }
            }
            else
                txtNguoiKy.Text = "";
        }
        //void SetNewSoBanAn()
        //{
        //    decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
        //    decimal ToaAnID = (Decimal)dt.AHS_VUAN.Where(x => x.ID == VuAnID).Single<AHS_VUAN>().TOAANID;
        //    AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
        //    decimal thutu = objBL.GetNewTT(ToaAnID, VuAnID);
        //    txtSoBanAn.Text = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();
        //}
        private void LoadThongTinBanAnSoTham()
        {
            MSG_file.Text = string.Empty;
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            decimal ToaAnID = (Decimal)dt.AHS_VUAN.Where(x => x.ID == VuAnID).Single<AHS_VUAN>().TOAANID;
            hddToaAnID.Value = ToaAnID.ToString();

            try
            {
                DM_TOAAN objTA = dt.DM_TOAAN.Where(x => x.ID == ToaAnID && x.HIEULUC == 1).Single<DM_TOAAN>();
                txtToaAn.Text = objTA.TEN;
                txtDiaDiem.Text = objTA.DIACHI;
            }
            catch { }
            //-----------------------------------------------------------
            txtNgayBanAn.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);

            //-------------------------------------------
            int tucach_phapnhan_tm = 0;
            AHS_BICANBICAO oBC = dt.AHS_BICANBICAO.Where(x => x.VUANID == VuAnID && x.BICANDAUVU == 1).FirstOrDefault();
            if (oBC != null)
            {
                tucach_phapnhan_tm = (int)oBC.LOAIDOITUONG;
                hddTuCachPN.Value = tucach_phapnhan_tm.ToString();
            }
            AHS_SOTHAM_BANAN obj = null;
            try
            {
                obj = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault();
                if (obj != null)
                {
                    hddID.Value = obj.ID.ToString();
                    LoadDsBiCao_AnPhi();
                    hddID.Value = obj.ID + "";

                    rdAnLe.SelectedValue = (string.IsNullOrEmpty(obj.ISANLE + "")) ? "0" : obj.ISANLE.ToString();
                    rdAnRutGon.SelectedValue = (string.IsNullOrEmpty(obj.ISANRUTGON + "")) ? "0" : obj.ISANRUTGON.ToString();
                    rdBaoLucGD.SelectedValue = (string.IsNullOrEmpty(obj.ISBAOLUCGIADINH + "")) ? "0" : obj.ISBAOLUCGIADINH.ToString();
                    rdXetXuLuuDong.SelectedValue = (string.IsNullOrEmpty(obj.ISXXLUUDONG + "")) ? "0" : obj.ISXXLUUDONG.ToString();

                    //-----------------------------
                    txtNgayBanAn.Text = ((DateTime)obj.NGAYBANAN).ToString("dd/MM/yyyy", cul);
                    txtSoBanAn.Text = obj.SOBANAN + "";

                    txtNgayMoPhienToa.Text = obj.NGAYMOPHIENTOA + "" == "" ? DateTime.Now.ToString("dd/MM/yyyy", cul) : ((DateTime)obj.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                    txtDiaDiem.Text = obj.DIADIEM + "";

                    //---------------------------------------
                    string temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_TOIDANH_NANGHON + "")) && obj.TK_XXKHACVKS_TOIDANH_NANGHON > 0) ? obj.TK_XXKHACVKS_TOIDANH_NANGHON.ToString() : "";
                    txt_XXToiDanh_NangHon.Text = temp;
                    temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_TOIDANH_NHEHON + "")) && obj.TK_XXKHACVKS_TOIDANH_NHEHON > 0) ? obj.TK_XXKHACVKS_TOIDANH_NHEHON.ToString() : "";
                    txt_XXToiDanh_NheHon.Text = temp;

                    temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_KHOAN_NANGHON + "")) && obj.TK_XXKHACVKS_KHOAN_NANGHON > 0) ? obj.TK_XXKHACVKS_KHOAN_NANGHON.ToString() : "";
                    txt_XXKhoan_NangHon.Text = temp;
                    temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_KHOAN_NHEHON + "")) && obj.TK_XXKHACVKS_KHOAN_NHEHON > 0) ? obj.TK_XXKHACVKS_KHOAN_NHEHON.ToString() : "";
                    txt_XXKhoan_NheHon.Text = temp;

                    temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_HINHPHAT_NANGHON + "")) && obj.TK_XXKHACVKS_HINHPHAT_NANGHON > 0) ? obj.TK_XXKHACVKS_HINHPHAT_NANGHON.ToString() : "";
                    txt_XXHinhPhat_NangHon.Text = temp;
                    temp = ((!string.IsNullOrEmpty(obj.TK_XXKHACVKS_HINHPHAT_NHEHON + "")) && obj.TK_XXKHACVKS_HINHPHAT_NHEHON > 0) ? obj.TK_XXKHACVKS_HINHPHAT_NHEHON.ToString() : "";
                    txt_XXHinhPhat_NheHon.Text = temp;

                    //---------------------------------------
                    txtTSChiemDoat.Text = ((!string.IsNullOrEmpty(obj.TK_TSCHIEMDOAT + "")) && obj.TK_TSCHIEMDOAT > 0) ? obj.TK_TSCHIEMDOAT.ToString() : "";
                    txtTSThietHai.Text = ((!string.IsNullOrEmpty(obj.TK_TSTHIETHAI + "")) && obj.TK_TSTHIETHAI > 0) ? obj.TK_TSTHIETHAI.ToString() : "";

                    //---------------------------------------
                    if (tucach_phapnhan_tm == 0)
                        row_tucach_phapnhan_tm.Visible = true;
                    else
                        row_tucach_phapnhan_tm.Visible = false;
                    rdPNTM_NuocNgoai.SelectedValue = (string.IsNullOrEmpty(obj.TK_PNTM_NUOCNGOAI + "")) ? "0" : obj.TK_PNTM_NUOCNGOAI.ToString();
                    rdPNTM_NhaNuoc.SelectedValue = (string.IsNullOrEmpty(obj.TK_PNTM_NHANUOC + "")) ? "0" : obj.TK_PNTM_NHANUOC.ToString();
                    //---------------------------------------
                    rdToaAnApDungBaoVe.SelectedValue = (string.IsNullOrEmpty(obj.TK_APDUNGBAOVE + "")) ? "0" : obj.TK_APDUNGBAOVE.ToString();
                    //---------------------------------------
                    rdViPhamHanTamGiam.SelectedValue = (string.IsNullOrEmpty(obj.TK_ISVIPHAMHANTAMGIAM + "")) ? "0" : obj.TK_ISVIPHAMHANTAMGIAM.ToString();
                    txtViPham_SoBiCao.Text = ((!string.IsNullOrEmpty(obj.TK_VIPHAMTG_BICAO + "")) && obj.TK_VIPHAMTG_BICAO > 0) ? obj.TK_VIPHAMTG_BICAO.ToString() : "";

                    rdIsPhucHoi.SelectedValue = (string.IsNullOrEmpty(obj.TK_PHUCHOIAN_VUAN + "")) ? "0" : obj.TK_PHUCHOIAN_VUAN.ToString();
                    txtPhucHoi_SoBiCao.Text = ((!string.IsNullOrEmpty(obj.TK_PHUCHOIAN_BICAO + "")) && obj.TK_PHUCHOIAN_BICAO > 0) ? obj.TK_PHUCHOIAN_BICAO.ToString() : "";

                    rdKhoiTo.Text = obj.TK_KHOITO_VUAN + "";
                    txtKhoiTo_SoBiCao.Text = ((!string.IsNullOrEmpty(obj.TK_KHOITO_BICAO + "")) && obj.TK_KHOITO_BICAO > 0) ? obj.TK_KHOITO_BICAO.ToString() : "";

                    //---------------------------------------
                    rdToaAn_XacMinh.SelectedValue = (string.IsNullOrEmpty(obj.TK_TOAAN_SOVUXM + "")) ? "0" : obj.TK_TOAAN_SOVUXM.ToString();
                    rdToaAn_KoXacMinh.SelectedValue = (string.IsNullOrEmpty(obj.TK_TOAAN_KOXM + "")) ? "0" : obj.TK_TOAAN_KOXM.ToString();

                    rdViPham_CtacQuanLy.SelectedValue = (string.IsNullOrEmpty(obj.TK_VIPHAMCONGTACQL + "")) ? "0" : obj.TK_VIPHAMCONGTACQL.ToString();
                    rdTraAn_VKSKoNhan.SelectedValue = (string.IsNullOrEmpty(obj.TK_ISTRAHS_VKSKHONGNHAN + "")) ? "0" : obj.TK_ISTRAHS_VKSKHONGNHAN.ToString();


                    if ((!string.IsNullOrEmpty(obj.TK_QUAHAN_CHUQUAN)) || (!String.IsNullOrEmpty(obj.TK_QUAHAN_KHACHQUAN)))
                    {
                        if ((obj.TK_QUAHAN_CHUQUAN == "0") || (obj.TK_QUAHAN_KHACHQUAN == "0"))
                        {
                            rdVuAnQuaHan.SelectedValue = "0";
                            pnNNQuanHan.Visible = false;
                            rdNNQuaHan.SelectedIndex = -1;
                        }
                        else
                        {
                            rdVuAnQuaHan.SelectedValue = "1";
                            pnNNQuanHan.Visible = true;

                            string nn_chuquan = (string.IsNullOrEmpty(obj.TK_QUAHAN_CHUQUAN + "")) ? "0" : obj.TK_QUAHAN_CHUQUAN;
                            string nn_khachquan = (string.IsNullOrEmpty(obj.TK_QUAHAN_KHACHQUAN + "")) ? "0" : obj.TK_QUAHAN_KHACHQUAN;
                            if (nn_chuquan == "1")
                                rdNNQuaHan.SelectedValue = "0";
                            else
                                rdNNQuaHan.SelectedValue = "1";
                        }
                    }
                    else
                    {
                        rdVuAnQuaHan.SelectedValue = "0";
                        pnNNQuanHan.Visible = false;
                        rdNNQuaHan.SelectedIndex = -1;
                    }
                    rdToaAn_BSTaiLieu.SelectedValue = (string.IsNullOrEmpty(obj.TK_YEUCAUVKSBOSUNGTL + "")) ? "0" : obj.TK_YEUCAUVKSBOSUNGTL.ToString();
                    //--------------------------------
                    if ((obj.TENFILE + "") != "")
                    {
                        lbtDownload.Visible = true;
                        lbtXoaFile.Visible = true;
                    }
                    else
                    {
                        lbtDownload.Visible = false;
                        lbtXoaFile.Visible = false;
                    }
                }
                else
                {
                    txtNgayMoPhienToa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                    pnAnPhi.Enabled = false;
                }
            }
            catch
            {
                obj = null;
                txtNgayMoPhienToa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                pnAnPhi.Enabled = false;
            }
        }

        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AsyncFileUpLoad.HasFile)
                {
                    string strFileName = AsyncFileUpLoad.FileName;
                    string path = Server.MapPath("~/TempUpload/") + strFileName;
                    AsyncFileUpLoad.SaveAs(path);
                    path = path.Replace("\\", "/");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }
        protected void lbtDownload_Click(object sender, EventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(hddID.Value);
                AHS_SOTHAM_BANAN oND = dt.AHS_SOTHAM_BANAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND.TENFILE != "")
                {
                    var cacheKey = Guid.NewGuid().ToString("N");
                    Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNGFILE, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }
        protected void lbtXoaFile_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(hddID.Value);
                AHS_SOTHAM_BANAN oND = dt.AHS_SOTHAM_BANAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND.TENFILE != "")
                {
                    oND.NOIDUNGFILE = null;
                    oND.TENFILE = null;
                    oND.KIEUFILE = null;
                    dt.SaveChanges();
                    MSG_file.Text = "File bản án đã được xóa thành công";
                    lbtDownload.Visible = false;
                    lbtXoaFile.Visible = false;
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }
        #region Update thong tin AnSoTham
        protected void cmdUpdateBanAnST_Click(object sender, EventArgs e)
        {
            lttMsgBanAn.Text = lttMsgAnPhi.Text = "";
            MSG_file.Text = string.Empty;
            string so = txtSoBanAn.Text;
            DateTime ngayBA = DateTime.Parse(this.txtNgayBanAn.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
            Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "AHS", so, ngayBA);
            if (CheckID > 0)
            {
                Decimal CurrBanAnId = (string.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
                String strMsg = "";
                String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "AHS", ngayBA).ToString();
                if (CheckID != CurrBanAnId)
                {
                    strMsg = "Số bản án " + txtSoBanAn.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                    txtSoBanAn.Text = STTNew;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    txtSoBanAn.Focus();
                    return;
                }
            }
            UpdateBanAn();
            Update_ThongTinThongKe();
            try
            {
                try { Update_BanAn_ToiDanh(); } catch { }
                hddPageIndex.Value = "1";
                LoadDsBiCao_AnPhi();
            }
            catch { }
            lttMsgBanAn.Text = "Lưu dữ liệu thành công!";
        }
        void Update_BanAn_ToiDanh()
        {
            Decimal BanAnID = String.IsNullOrEmpty(hddID.Value) ? 0 : Convert.ToDecimal(hddID.Value);
            List<AHS_SOTHAM_BANAN_DIEU_CHITIET> lst = null;
            lst = dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.Where(x => x.VUANID == VuAnID
                                                           && x.BANANID == 0
                                                        ).ToList();
            if (lst != null && lst.Count > 0)
            {
                foreach (AHS_SOTHAM_BANAN_DIEU_CHITIET item in lst)
                {
                    item.BANANID = BanAnID;
                }
                dt.SaveChanges();
            }
        }
        void UpdateBanAn()
        {
            Boolean IsUpdate = false;
            DateTime date_temp;
            Decimal VuAnId = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);

            AHS_SOTHAM_BANAN obj = new AHS_SOTHAM_BANAN();
            if (VuAnId > 0)
            {
                obj = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnId).SingleOrDefault<AHS_SOTHAM_BANAN>();
                if (obj != null)
                    IsUpdate = true;
                else
                    obj = new AHS_SOTHAM_BANAN();
            }
            else
                obj = new AHS_SOTHAM_BANAN();

            obj.VUANID = VuAnId;
            //-----------------------------------------   
            date_temp = (String.IsNullOrEmpty(txtNgayBanAn.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayBanAn.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYBANAN = date_temp;

            //-----------------------------------------           
            date_temp = (String.IsNullOrEmpty(txtNgayMoPhienToa.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayMoPhienToa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYMOPHIENTOA = date_temp;
            obj.DIADIEM = txtDiaDiem.Text.Trim();
            //----------------------------------------- 
            if (hddFilePath.Value != "")
            {
                try
                {
                    string strFilePath = "";
                    if (chkKySo.Checked)
                    {
                        string[] arr = hddFilePath.Value.Split('/');
                        strFilePath = arr[arr.Length - 1];
                        strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                    }
                    else
                        strFilePath = hddFilePath.Value.Replace("/", "\\");
                    byte[] buff = null;
                    using (FileStream fs = File.OpenRead(strFilePath))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        FileInfo oF = new FileInfo(strFilePath);
                        long numBytes = oF.Length;
                        buff = br.ReadBytes((int)numBytes);
                        obj.NOIDUNGFILE = buff;
                        obj.TENFILE = oF.Name;
                        obj.KIEUFILE = oF.Extension;
                    }
                    File.Delete(strFilePath);
                    lbtDownload.Visible = true;
                    lbtXoaFile.Visible = true;
                    MSG_file.Text = string.Empty;
                }
                catch { }
            }

            //-----------------------------------------
            obj.NGUOIKY = (!String.IsNullOrEmpty(hddNguoiKyID.Value)) ? 0 : Convert.ToDecimal(hddNguoiKyID.Value);
            Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            obj.TOAANID = ToaAnID;
            obj.SOBANAN = txtSoBanAn.Text.Trim();
            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";

                //AHS_VUAN objVuAn = dt.AHS_VUAN.Where(x => x.ID == VuAnId).Single<AHS_VUAN>();
                //obj.TOAANID = objVuAn.TOAANID;

                //AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
                //decimal thutu = objBL.GetNewTT(ToaAnID, VuAnId);
                //obj.SOBANAN = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();

                dt.AHS_SOTHAM_BANAN.Add(obj);
                dt.SaveChanges();
                InsertToiDanhTuCaoTrangSangBanAnSoTham(obj.ID);
                hddID.Value = obj.ID + "";
            }
            lttMsgBanAn.Text = "Lưu dữ liệu thành công!";
        }
        void InsertToiDanhTuCaoTrangSangBanAnSoTham(decimal BanAnID)
        {
            Boolean IsUpdate = false;
            AHS_SOTHAM_BANAN_DIEU_CHITIET BaDieuCT;
            AHS_SOTHAM_BANAN_BICAO Ba_BC;
            Decimal BiCaoID = 0;
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");

            AHS_SOTHAM_CAOTRANG_DIEULUAT_BL objBL = new AHS_SOTHAM_CAOTRANG_DIEULUAT_BL();
            DataTable tbl = objBL.GetByVuAnID(VuAnID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                Decimal DieuLuatID = 0, HinhPhatID = 0, ToiDanhId = 0, LoaiHinhPhat = 0;
                foreach (DataRow row in tbl.Rows)
                {
                    BiCaoID = (string.IsNullOrEmpty(row["BiCanID"] + "")) ? 0 : Convert.ToDecimal(row["BiCanID"] + "");
                    DieuLuatID = (string.IsNullOrEmpty(row["BoLuatID"] + "")) ? 0 : Convert.ToDecimal(row["BoLuatID"] + "");
                    ToiDanhId = (string.IsNullOrEmpty(row["ToiDanhID"] + "")) ? 0 : Convert.ToDecimal(row["ToiDanhID"] + "");
                    //-----------------------------------------------
                    #region Update Vao BanAn_BiCao
                    IsUpdate = false;

                    Ba_BC = dt.AHS_SOTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID && x.BICAOID == BiCaoID).SingleOrDefault<AHS_SOTHAM_BANAN_BICAO>();
                    if (Ba_BC != null)
                        IsUpdate = true;
                    else
                        Ba_BC = new AHS_SOTHAM_BANAN_BICAO();

                    Ba_BC.BANANID = BanAnID;
                    Ba_BC.BICAOID = BiCaoID;

                    if (!IsUpdate)
                    {
                        dt.AHS_SOTHAM_BANAN_BICAO.Add(Ba_BC);
                        dt.SaveChanges();
                    }

                    #endregion
                    //------------------------------------------------
                    #region Update BAnAn_Dieu_ChiTiet  
                    IsUpdate = false;
                    BaDieuCT = dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.Where(x => x.BANANID == BanAnID
                                                                       && x.BICANID == BiCaoID
                                                                       && x.DIEULUATID == DieuLuatID
                                                                       && x.TOIDANHID == ToiDanhId
                                                                    ).FirstOrDefault<AHS_SOTHAM_BANAN_DIEU_CHITIET>();
                    if (BaDieuCT != null)
                        IsUpdate = true;
                    else
                        BaDieuCT = new AHS_SOTHAM_BANAN_DIEU_CHITIET();

                    BaDieuCT.BANANID = BanAnID;
                    BaDieuCT.ISCHANGE = 0;
                    BaDieuCT.BICANID = BiCaoID;
                    BaDieuCT.DIEULUATID = DieuLuatID;
                    BaDieuCT.HINHPHATID = HinhPhatID;
                    BaDieuCT.LOAIHINHPHAT = LoaiHinhPhat;
                    BaDieuCT.TOIDANHID = ToiDanhId;
                    BaDieuCT.TENTOIDANH = row["TenToiDanh"] + "";
                    BaDieuCT.ISMAIN = String.IsNullOrEmpty(row["IsMain"] + "") ? 0 : Convert.ToInt16(row["IsMain"] + "");
                    BaDieuCT.VUANID = VuAnID;
                    switch (Convert.ToInt16(BaDieuCT.LOAIHINHPHAT))
                    {
                        case ENUM_LOAIHINHPHAT.DANG_TRUE_FALSE_VALUE:
                            BaDieuCT.TF_VALUE = 0;
                            break;
                        case ENUM_LOAIHINHPHAT.DANG_SO_HOC_VALUE:
                            BaDieuCT.SH_VALUE = 0;
                            break;
                        case ENUM_LOAIHINHPHAT.DANG_THOI_GIAN_VALUE:
                            BaDieuCT.TG_NGAY = 0;
                            BaDieuCT.TG_THANG = 0;
                            BaDieuCT.TG_NAM = 0;
                            break;
                        case ENUM_LOAIHINHPHAT.DANG_KHAC_VALUE:
                            BaDieuCT.K_VALUE1 = 0;
                            BaDieuCT.K_VALUE2 = "";
                            break;
                    }
                    if (!IsUpdate)
                    {
                        dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.Add(BaDieuCT);
                        dt.SaveChanges();
                    }
                    #endregion
                }
            }
        }
        #endregion

        #region Update thong tin thong ke
        void Update_ThongTinThongKe()
        {
            Boolean IsUpdate = false;
            Decimal VuAnId = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);

            AHS_SOTHAM_BANAN obj = new AHS_SOTHAM_BANAN();
            if (VuAnId > 0)
            {
                obj = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnId).SingleOrDefault<AHS_SOTHAM_BANAN>();
                if (obj != null)
                    IsUpdate = true;
            }
            else
                obj = new AHS_SOTHAM_BANAN();

            obj.VUANID = VuAnId;

            obj.ISANLE = Convert.ToDecimal(rdAnLe.SelectedValue);
            obj.ISANRUTGON = Convert.ToDecimal(rdAnRutGon.SelectedValue);
            obj.ISXXLUUDONG = Convert.ToDecimal(rdXetXuLuuDong.SelectedValue);
            obj.ISBAOLUCGIADINH = Convert.ToDecimal(rdBaoLucGD.SelectedValue);

            obj.TK_XXKHACVKS_TOIDANH_NANGHON = (string.IsNullOrEmpty(txt_XXToiDanh_NangHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXToiDanh_NangHon.Text.Replace(".", ""));
            obj.TK_XXKHACVKS_TOIDANH_NHEHON = (string.IsNullOrEmpty(txt_XXToiDanh_NheHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXToiDanh_NheHon.Text.Replace(".", ""));
            obj.TK_XXKHACVKS_KHOAN_NANGHON = (string.IsNullOrEmpty(txt_XXKhoan_NangHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXKhoan_NangHon.Text.Replace(".", ""));
            obj.TK_XXKHACVKS_KHOAN_NHEHON = (string.IsNullOrEmpty(txt_XXKhoan_NheHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXKhoan_NheHon.Text.Replace(".", ""));
            obj.TK_XXKHACVKS_HINHPHAT_NANGHON = (string.IsNullOrEmpty(txt_XXHinhPhat_NangHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXHinhPhat_NangHon.Text.Replace(".", ""));
            obj.TK_XXKHACVKS_HINHPHAT_NHEHON = (string.IsNullOrEmpty(txt_XXHinhPhat_NheHon.Text.Trim())) ? 0 : Convert.ToDecimal(txt_XXHinhPhat_NheHon.Text.Replace(".", ""));

            //---------------------------------------
            obj.TK_TSCHIEMDOAT = (string.IsNullOrEmpty(txtTSChiemDoat.Text)) ? 0 : Convert.ToDecimal(txtTSChiemDoat.Text.Replace(".", ""));
            obj.TK_TSTHIETHAI = (string.IsNullOrEmpty(txtTSThietHai.Text)) ? 0 : Convert.ToDecimal(txtTSThietHai.Text.Replace(".", ""));

            //---------------------------------------
            obj.TK_PNTM_NUOCNGOAI = obj.TK_PNTM_NHANUOC = 0;
            if (hddTuCachPN.Value == "0")
            {
                obj.TK_PNTM_NUOCNGOAI = Convert.ToDecimal(rdPNTM_NuocNgoai.SelectedValue);
                obj.TK_PNTM_NHANUOC = Convert.ToDecimal(rdPNTM_NhaNuoc.SelectedValue);
            }
            obj.TK_APDUNGBAOVE = Convert.ToDecimal(rdToaAnApDungBaoVe.SelectedValue);
            //---------------------------------------
            obj.TK_ISVIPHAMHANTAMGIAM = Convert.ToDecimal(rdViPhamHanTamGiam.SelectedValue);
            //obj.TK_VIPHAMTG_VUAN = (string.IsNullOrEmpty(txtViPham_SoVuAn.Text)) ? 0 : Convert.ToDecimal(txtViPham_SoVuAn.Text) ;
            obj.TK_VIPHAMTG_BICAO = (string.IsNullOrEmpty(txtViPham_SoBiCao.Text)) ? 0 : Convert.ToDecimal(txtViPham_SoBiCao.Text);

            obj.TK_PHUCHOIAN_VUAN = Convert.ToDecimal(rdIsPhucHoi.SelectedValue);
            obj.TK_PHUCHOIAN_BICAO = (string.IsNullOrEmpty(txtPhucHoi_SoBiCao.Text)) ? 0 : Convert.ToDecimal(txtPhucHoi_SoBiCao.Text);

            obj.TK_TOAAN_SOVUXM = Convert.ToDecimal(rdToaAn_XacMinh.SelectedValue);
            obj.TK_TOAAN_KOXM = Convert.ToDecimal(rdToaAn_KoXacMinh.SelectedValue);

            obj.TK_KHOITO_VUAN = Convert.ToDecimal(rdKhoiTo.SelectedValue);
            obj.TK_KHOITO_BICAO = (string.IsNullOrEmpty(txtKhoiTo_SoBiCao.Text)) ? 0 : Convert.ToDecimal(txtKhoiTo_SoBiCao.Text);

            obj.TK_ISTRAHS_VKSKHONGNHAN = Convert.ToDecimal(rdTraAn_VKSKoNhan.SelectedValue);
            obj.TK_YEUCAUVKSBOSUNGTL = Convert.ToDecimal(rdToaAn_BSTaiLieu.SelectedValue);

            if (rdVuAnQuaHan.SelectedValue == "1")
            {
                if (rdNNQuaHan.SelectedValue == "0")
                    obj.TK_QUAHAN_CHUQUAN = "1";
                else obj.TK_QUAHAN_KHACHQUAN = "0";
            }
            else
                obj.TK_QUAHAN_KHACHQUAN = obj.TK_QUAHAN_CHUQUAN = "0";

            obj.TK_VIPHAMCONGTACQL = Convert.ToDecimal(rdViPham_CtacQuanLy.SelectedValue);
            Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            obj.TOAANID = ToaAnID;

            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";

                AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
                 if (!String.IsNullOrEmpty(txtSoBanAn.Text))
                    obj.SOBANAN = txtSoBanAn.Text.Trim();
                else
                {
                    try {
                       decimal thutu = objBL.GetNewTT(ToaAnID, VuAnId);
                       obj.SOBANAN = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();
                    }
                    catch (Exception ex) { }
                }
                dt.AHS_SOTHAM_BANAN.Add(obj);
                dt.SaveChanges();
                hddID.Value = obj.ID + "";
            }
            //lttMsgThongKe.Text = "Lưu dữ liệu thống kê thành công!";
        }
        #endregion

        #region TongHopHP- chua xong
        DataTable CreateTable()
        {
            DataTable tblTemp = new DataTable();
            tblTemp.Columns.Add("BiCanID", typeof(Decimal));
            tblTemp.Columns.Add("MaNhomHinhPhat", typeof(string));
            tblTemp.Columns.Add("HinhPhatID", typeof(Decimal));
            tblTemp.Columns.Add("MaHinhPhat", typeof(string));

            tblTemp.Columns.Add("IsAnTreo", typeof(Decimal));

            tblTemp.Columns.Add("TF_Value", typeof(Decimal));
            tblTemp.Columns.Add("SH_Value", typeof(Decimal));
            tblTemp.Columns.Add("TG_Nam", typeof(Decimal));
            tblTemp.Columns.Add("TG_Thang", typeof(Decimal));
            tblTemp.Columns.Add("TG_Ngay", typeof(Decimal));
            tblTemp.Columns.Add("K_Value1", typeof(Decimal));
            tblTemp.Columns.Add("K_Value2", typeof(string));
            return tblTemp;
        }
        void TongHopHinhPhat()
        {
            AHS_SOTHAM_BANAN_DIEU_CHITIET_BL objBL = new AHS_SOTHAM_BANAN_DIEU_CHITIET_BL();
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");

            AHS_SOTHAM_BANAN objST = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).Single<AHS_SOTHAM_BANAN>();
            Decimal BanAnID = objST.ID;

            DataTable tblRetval = CreateTable();
            DataTable tblTD = null;
            //-------lay ds cac bi cao theo bananid-----------
            Decimal BiCanID = 0;
            List<AHS_SOTHAM_BANAN_BICAO> lstBC = dt.AHS_SOTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID).ToList<AHS_SOTHAM_BANAN_BICAO>();

            //lay ds toidanh+hinhphat cua tat ca bi can theo bananid
            DataTable tbl = objBL.GetByBanAnID(BanAnID, 0);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                DataView view = new DataView(tbl);
                tblTD = view.ToTable(true, "ToiDanhID");

                foreach (AHS_SOTHAM_BANAN_BICAO objBC in lstBC)
                {
                    BiCanID = (decimal)objBC.BICAOID;
                    foreach (DataRow rowTD in tblTD.Rows)
                    {
                        //TongHopTheoHinhPhat(tbl, tblRetval, BiCanID);
                        LayDLTheoMaNhomHP(tbl, tblRetval, BiCanID, ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG);
                        LayDLTheoMaNhomHP(tbl, tblRetval, BiCanID, ENUM_NHOMHINHPHAT.NHOM_HPCHINH);
                        LayDLTheoMaNhomHP(tbl, tblRetval, BiCanID, ENUM_NHOMHINHPHAT.NHOM_QDKHAC);
                    }
                }
            }

            if (tblRetval != null && tblRetval.Rows.Count > 0)
            {
                string MaNhomHinhPhat = "";
                DataTable tblData = tblRetval.Clone();

                //1. Lay ds cac hinh phat thuoc nhom HPBoSUng --> inser vào bang temp
                MaNhomHinhPhat = ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG;
                DataRow[] arr = tblRetval.Select("MaNhomHinhPhat='" + MaNhomHinhPhat + "'");
                if (arr != null && arr.Length > 0)
                {
                    foreach (DataRow row in arr)
                    {
                        // newrow = tblTemp.NewRow();
                        tblData.ImportRow(row);
                    }
                }
                //---------------------------
                //2. Lay ds theo BiCan Va NhomHP <> NhomHPBoSung
                DataTable tbltemp = tblRetval.Clone();
                foreach (AHS_SOTHAM_BANAN_BICAO objBC in lstBC)
                {
                    BiCanID = (decimal)objBC.BICAOID;
                    arr = tblRetval.Select("BiCanID =" + BiCanID + " and MaNhomHinhPhat<>'" + MaNhomHinhPhat + "'", "MaNhomHinhPhat");
                    if (arr != null && arr.Length > 0)
                    {
                        foreach (DataRow row in arr)
                            tbltemp.ImportRow(row);
                        //--------------------
                        //3. Lay DS cac NhomHP theo BiAn
                        DataView view = new DataView(tbltemp);
                        DataTable tblNhomHP = view.ToTable(true, "MaNhomHinhPhat");

                        //KT: Neu BiAn co ca 2 nhom HP = HPCHinh + QD khac --> chi lay dl cua nhom HPChinh
                        //Neu biAn chi co HP = QD khac --> lay Dl cua nhom nay ra
                        if (tblNhomHP != null && tblNhomHP.Rows.Count > 0)
                        {
                            int count_nhomhp = tblNhomHP.Rows.Count;
                            if (count_nhomhp == 1)
                            {
                                foreach (DataRow row in tbltemp.Rows)
                                    tblData.ImportRow(row);
                            }
                            else
                            {
                                MaNhomHinhPhat = ENUM_NHOMHINHPHAT.NHOM_HPCHINH;
                                arr = tbltemp.Select("MaNhomHinhPhat='" + MaNhomHinhPhat + "'");
                                foreach (DataRow row in arr)
                                    tblData.ImportRow(row);
                            }
                        }
                    }
                }
            }
        }

        void TongHopTheoHinhPhat(DataTable tblData, DataTable tblRetval, Decimal BiCanID)
        {
            /*
           * Quy tac tong hop hinh phat nhu sau:
           * - Tinh tong hinh phat cac toi danh 
           * - voi chung HinhPhatID : neu dl = true/false, default_true --> chọn true
           * - neu dl = so --> cong tong
           * - dl co antreo --> chọn IsAnTreo = 1
           */
            Decimal TF_Value = 0, SH_Value = 0;
            Decimal TG_Nam = 0, TG_Thang = 0, TG_Ngay = 0;
            Decimal K_Value1 = 0;
            String K_Value2 = "";
            Decimal IsAnTreo = 0;

            decimal temp_value = 0;
            DataRow newrow = null;
            DataTable tblHP = null;
            DataRow[] arr = null;
            Decimal HinhPhatID = 0;
            DataTable tblNhomHP = null;
            DataTable tblTemp = tblData.Clone();

            //lay ds hinh phat tu bang tblData
            DataView view = new DataView(tblData);
            tblHP = view.ToTable(true, "HinhPhatID, MaHinhPhat, TenHinhPhat");
            string MaHinhPhat = "";
            foreach (DataRow rowHP in tblHP.Rows)
            {
                TF_Value = SH_Value = TG_Nam = TG_Thang = TG_Ngay = K_Value1 = 0;
                K_Value2 = "";
                tblTemp.Clear();
                tblNhomHP.Clear();

                HinhPhatID = Convert.ToDecimal(rowHP["HinhPhatID"] + "");
                MaHinhPhat = rowHP["MaHinhPhat"] + "";

                //----------lay ds theo bi can, hinhphat (cac toi danh khac nhau)--------
                arr = tblData.Select("BiCanID=" + BiCanID + " and HinhPhatID=" + HinhPhatID, "MaNhomHinhPhat, MucDo desc");
                if (arr != null && arr.Length > 0)
                {
                    //foreach (DataRow row in arr)
                    //    tblTemp.ImportRow(row);

                    //view = new DataView(tblTemp);
                    //tblNhomHP = view.ToTable(true, "MaNhomHinhPhat");

                    //--------------------------------
                    newrow = tblRetval.NewRow();
                    newrow["BiCanID"] = BiCanID;
                    newrow["HinhPhatID"] = HinhPhatID;
                    newrow["MaNhomHinhPhat"] = arr[0]["MaNhomHinhPhat"] + "";
                    newrow["MaHinhPhat"] = MaHinhPhat;

                    #region Cong don hinh phat
                    foreach (DataRow rowData in arr)
                    {
                        SH_Value += Convert.ToDecimal(rowData["SH_Value"] + "");

                        TG_Nam += Convert.ToDecimal(rowData["TG_Nam"] + "");
                        TG_Thang += Convert.ToDecimal(rowData["TG_Thang"] + "");
                        TG_Ngay += Convert.ToDecimal(rowData["TG_Ngay"] + "");

                        K_Value1 += Convert.ToDecimal(rowData["K_Value1"] + "");

                        K_Value2 += (string.IsNullOrEmpty(K_Value2)) ? "" : ",";
                        K_Value2 += rowData["K_Value2"].ToString();

                        temp_value = Convert.ToDecimal(rowData["TF_Value"] + "");
                        if (temp_value > 0)
                            TF_Value = temp_value;

                        temp_value = Convert.ToDecimal(rowData["IsAnTreo"] + "");
                        if (temp_value > 0)
                            IsAnTreo = temp_value;
                    }
                    #endregion

                    newrow["IsAnTreo"] = IsAnTreo;
                    newrow["TF_Value"] = TF_Value;
                    newrow["SH_Value"] = SH_Value;
                    newrow["TG_Nam"] = TG_Nam;
                    newrow["TG_Thang"] = TG_Thang;
                    newrow["TG_Ngay"] = TG_Ngay;
                    newrow["K_Value1"] = K_Value1;
                    newrow["K_Value2"] = K_Value2;
                    tblRetval.Rows.Add(newrow);
                }
            }
        }
        void LayDLTheoMaNhomHP(DataTable tblData, DataTable tblRetval, Decimal BiCanID, String MaNhomHinhPhat)
        {
            String MaHinhPhat = "";
            DataRow[] arr;
            DataTable tblHP = null;
            DataTable tblTemp = tblData.Clone();
            DataView view = null;
            //-----------------------------
            arr = tblData.Select("BiCanID=" + BiCanID + " and MaNhomHinhPhat='" + MaNhomHinhPhat + "'", "MucDo DESC");
            if (arr != null && arr.Length > 0)
            {
                foreach (DataRow row in arr)
                    tblTemp.ImportRow(row);

                view = new DataView(tblTemp);
                tblHP = view.ToTable(true, "HINHPHATID,MAHINHPHAT,TENHINHPHAT");

                //---------------------------
                switch (MaNhomHinhPhat)
                {
                    case ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG:
                        //Lay toan bo hinh phat bo sung
                        break;
                    case ENUM_NHOMHINHPHAT.NHOM_HPCHINH:
                        foreach (DataRow rowHP in tblHP.Rows)
                        {
                            MaHinhPhat = rowHP["MaHinhPhat"].ToString();
                            //if (MaHinhPhat== ENUM_HINHPHAT.TU_HINH  || MaHinhPhat == ENUM_HINHPHAT.TU_CHUNGTHAN)
                            //    AddHinhPhat_Default(tblData, BiCanID, HinhPhatID, MaHinhPhat, MaNhomHinhPhat);
                            //else
                            //{

                            //}
                        }
                        break;
                    case ENUM_NHOMHINHPHAT.NHOM_QDKHAC:
                        break;
                }


                //foreach (DataRow rowHP in tblHP.Rows)
                //{
                //    TF_Value = SH_Value = TG_Nam = TG_Thang = TG_Ngay = K_Value1 = 0;
                //    K_Value2 = "";

                //    HinhPhatID = Convert.ToDecimal(rowHP["HinhPhatID"] + "");
                //    MaHinhPhat = rowHP["MaHinhPhat"] + "";

                //    //--------------------------------
                //    newrow = tblRetval.NewRow();
                //    newrow["BiCanID"] = BiCanID;
                //    newrow["HinhPhatID"] = HinhPhatID;
                //    newrow["MaHinhPhat"] = MaHinhPhat;
                //    newrow["MaNhomHinhPhat"] = MaNhomHinhPhat;

                //    #region Cong don hinh phat
                //    foreach (DataRow rowData in arr)
                //    {
                //        SH_Value += Convert.ToDecimal(rowData["SH_Value"] + "");

                //        TG_Nam += Convert.ToDecimal(rowData["TG_Nam"] + "");
                //        TG_Thang += Convert.ToDecimal(rowData["TG_Thang"] + "");
                //        TG_Ngay += Convert.ToDecimal(rowData["TG_Ngay"] + "");

                //        K_Value1 += Convert.ToDecimal(rowData["K_Value1"] + "");

                //        K_Value2 += (string.IsNullOrEmpty(K_Value2)) ? "" : ",";
                //        K_Value2 += rowData["K_Value2"].ToString();

                //        temp_value = Convert.ToDecimal(rowData["TF_Value"] + "");
                //        if (temp_value > 0)
                //            TF_Value = temp_value;

                //        temp_value = Convert.ToDecimal(rowData["IsAnTreo"] + "");
                //        if (temp_value > 0)
                //            IsAnTreo = temp_value;
                //    }
                //    #endregion

                //    newrow["IsAnTreo"] = IsAnTreo;
                //    newrow["TF_Value"] = TF_Value;
                //    newrow["SH_Value"] = SH_Value;
                //    newrow["TG_Nam"] = TG_Nam;
                //    newrow["TG_Thang"] = TG_Thang;
                //    newrow["TG_Ngay"] = TG_Ngay;
                //    newrow["K_Value1"] = K_Value1;
                //    newrow["K_Value2"] = K_Value2;
                //    tblRetval.Rows.Add(newrow);
                //}
            }
        }
        void AddHinhPhat_Default(DataTable tblData, Decimal BiCanId, Decimal HinhPhatID, string MaHinhPhat, string MaNhomHinhPhat)
        {
            DataRow newrow = tblData.NewRow();
            newrow["BiCanID"] = BiCanId;
            newrow["HinhPhatID"] = HinhPhatID;
            newrow["MaHinhPhat"] = MaHinhPhat;
            newrow["MaNhomHinhPhat"] = MaNhomHinhPhat;

            newrow["TF_Value"] = 1;

            newrow["IsAnTreo"] = 0;
            newrow["SH_Value"] = 0;
            newrow["TG_Nam"] = 0;
            newrow["TG_Thang"] = 0;
            newrow["TG_Ngay"] = 0;
            newrow["K_Value1"] = 0;
            newrow["K_Value2"] = "";
            tblData.Rows.Add(newrow);
        }
        #endregion

        #region Update thong tin an phi & Load DSBiCao_AnPhi
        protected void cmdUpdateAnPhi_Click(object sender, EventArgs e)
        {
            MSG_file.Text = string.Empty;
            lttMsgBanAn.Text = lttMsgAnPhi.Text = "";
            AHS_SOTHAM_BANAN_BICAO obj = null;
            Boolean IsNew = false;
            Decimal BanAnID = Convert.ToDecimal(hddID.Value);
            if (rpt.Items.Count > 0)
            {
                foreach (RepeaterItem oItem in rpt.Items)
                {
                    TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                    if (txtNgaynhanbanan.Text != "")
                    {
                        DateTime NgayNhanBA;
                        if (DateTime.TryParse(txtNgaynhanbanan.Text, cul, DateTimeStyles.NoCurrentDateDefault, out NgayNhanBA))
                        {
                            if (DateTime.Compare(NgayNhanBA, DateTime.Now) > 0)
                            {
                                lttMsgAnPhi.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                                txtNgaynhanbanan.Focus();
                                return;
                            }
                        }
                        else
                        {
                            lttMsgAnPhi.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                            txtNgaynhanbanan.Focus();
                            return;
                        }
                    }
                }
            }
            foreach (RepeaterItem item in rpt.Items)
            {
                IsNew = false;
                HiddenField hddBiCao = (HiddenField)item.FindControl("hddBiCao");
                CheckBox chkThamGiaPhienToa = (CheckBox)item.FindControl("chkThamGiaPhienToa");
                TextBox txtAnPhi = (TextBox)item.FindControl("txtAnPhi");
                //CheckBox chkDinhChi = (CheckBox)item.FindControl("chkDinhChi");
                TextBox txtNgaynhanbanan = (TextBox)item.FindControl("txtNgaynhanbanan");
                decimal BiCaoId = Convert.ToDecimal(hddBiCao.Value);
                obj = dt.AHS_SOTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID && x.BICAOID == BiCaoId).SingleOrDefault<AHS_SOTHAM_BANAN_BICAO>();
                if (obj == null)
                {
                    IsNew = true;
                    obj = new AHS_SOTHAM_BANAN_BICAO();
                }

                obj.BANANID = BanAnID;
                obj.ISTHAMGIAPHIENTOA = (chkThamGiaPhienToa.Checked) ? 1 : 0;
                //obj.ISDINHCHI = (chkDinhChi.Checked) ? 1 : 0;

                obj.NGAYNHANBANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.BICAOID = BiCaoId;
                obj.ANPHI = (string.IsNullOrEmpty(txtAnPhi.Text + "")) ? 0 : Convert.ToDecimal(txtAnPhi.Text.Replace(".", ""));
                if (IsNew)
                    dt.AHS_SOTHAM_BANAN_BICAO.Add(obj);
                dt.SaveChanges();

            }
            lttMsgAnPhi.Text = "Lưu dữ liệu thành công!";
        }
        #region Load DS bi cao
        public void cmdReloadParent_Click(object sender, EventArgs e)
        {
            LoadDsBiCao_AnPhi();
        }
        public void LoadDsBiCao_AnPhi()
        {
            int vu_an_id = Convert.ToInt32(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            int page_size = 20;
            int pageindex = Convert.ToInt32(hddPageIndex.Value);

            AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
            DataTable tbl = objBL.GetDsBiCaoByVuAn(vu_an_id, pageindex, page_size);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                DataRow[] arr = tbl.Select("BICANDAUVU=1", "BICANDAUVU desc,NgayThamGia desc");
                if (arr != null && arr.Length>0)
                {
                    row_tucach_phapnhan_tm.Visible = false;
                    rdPNTM_NhaNuoc.SelectedIndex = rdPNTM_NuocNgoai.SelectedIndex = 0;
                }

                int count_all = Convert.ToInt32(tbl.Rows[0]["CountAll"] + "");
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, page_size).ToString();
                // lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion

                rpt.DataSource = tbl;
                rpt.DataBind();
                pndata.Visible = true;
                //-----------------------------
                int IsShow = (string.IsNullOrEmpty(tbl.Rows[0]["IsShow"] + "")) ? 0 : Convert.ToInt16(tbl.Rows[0]["IsShow"] + "");
                if (IsShow > 0)
                {
                    cmdUpdateAnPhi.Enabled = true;
                    pnAnPhi.Enabled = true;
                }
                else
                {
                    // lttMsgBanAn.Text = "Chưa có bản án xét xử sơ thẩm!";
                    cmdUpdateAnPhi.Enabled = pnAnPhi.Enabled = false;
                }
            }
            else
            {
                lttMsgAnPhi.Text = "Chưa có bị cáo được xét xử sơ thẩm. Đề nghị kiểm tra lại!";
                cmdUpdateAnPhi.Enabled = pnAnPhi.Enabled = pndata.Visible = false;
            }
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dv = (DataRowView)e.Item.DataItem;
                int IsShow = Convert.ToInt16(dv["IsShow"] + "");
                if (IsShow == 0)
                {
                    Panel lnLinkToiDanh = (Panel)e.Item.FindControl("lnLinkToiDanh");
                    lnLinkToiDanh.Visible = false;
                }

                TextBox txtNgaynhanbanan = (TextBox)e.Item.FindControl("txtNgaynhanbanan");
                DateTime ngaynhan = String.IsNullOrEmpty(dv["NgayNhanBanAn"] + "") ? DateTime.MinValue : Convert.ToDateTime(dv["NgayNhanBanAn"] + "");
                if (ngaynhan == DateTime.MinValue)
                    txtNgaynhanbanan.Text = "";
                else
                    txtNgaynhanbanan.Text = ngaynhan.ToString("dd/MM/yyyy", cul);
                // Tong hop toi danh
                //lblTHtoidanh
                HiddenField hddBiCao = (HiddenField)e.Item.FindControl("hddBiCao");
                decimal vBicaoID = Convert.ToDecimal(hddBiCao.Value);

                try
                { //-- Hình phạt tổng hợp
                    string THHinhPhat = Tonghophinhphat_bicao(VuAnID, vBicaoID);
                    Label lblTHtoidanh = (Label)e.Item.FindControl("lblTHtoidanh");
                    lblTHtoidanh.Text = THHinhPhat;
                }
                catch (Exception ex) { }
            }
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal curr_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "ToiDanh":
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
                    if (Result != "")
                    {
                        lttMsgAnPhi.Text = Result;
                        return;
                    }
                    Cls_Comon.CallFunctionJS(this, this.GetType(), "popupChonToiDanh(" + curr_id + ")");
                    break;
            }
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadDsBiCao_AnPhi();
            }
            catch (Exception ex) { lttMsgAnPhi.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadDsBiCao_AnPhi();
            }
            catch (Exception ex) { lttMsgAnPhi.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadDsBiCao_AnPhi();
            }
            catch (Exception ex) { lttMsgAnPhi.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadDsBiCao_AnPhi();
            }
            catch (Exception ex) { lttMsgAnPhi.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                LoadDsBiCao_AnPhi();
            }
            catch (Exception ex) { lttMsgAnPhi.Text = ex.Message; }
        }
        #endregion

        #endregion
        #endregion
        
        protected void rdViPhamHanTamGiam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdViPhamHanTamGiam.SelectedValue == "1")
            {
                pnViPham.Enabled = true;
                Cls_Comon.SetFocus(this, this.GetType(), txtViPham_SoBiCao.ClientID);
            }
            else
            {
                pnViPham.Enabled = false;
                txtViPham_SoBiCao.Text = "0";
                Cls_Comon.SetFocus(this, this.GetType(), rdIsPhucHoi.ClientID);
            }
        }
        protected void rdIsPhucHoi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdIsPhucHoi.SelectedValue == "1")
            {
                pnPhucHoi.Enabled = true;
                Cls_Comon.SetFocus(this, this.GetType(), rdKhoiTo.ClientID);
            }
            else
            {
                pnPhucHoi.Enabled = false;
                txtPhucHoi_SoBiCao.Text = "0";
                Cls_Comon.SetFocus(this, this.GetType(), txtViPham_SoBiCao.ClientID);
            }
        }
        protected void rdKhoiTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdKhoiTo.SelectedValue == "1")
            {
                pnKhoiTo.Enabled = true;
                Cls_Comon.SetFocus(this, this.GetType(), txtKhoiTo_SoBiCao.ClientID);
            }
            else
            {
                pnKhoiTo.Enabled = false;
                txtKhoiTo_SoBiCao.Text = "0";
                Cls_Comon.SetFocus(this, this.GetType(), rdTraAn_VKSKoNhan.ClientID);
            }
        }
        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
            {
                pnNNQuanHan.Visible = true;
                Cls_Comon.SetFocus(this, this.GetType(), rdNNQuaHan.ClientID);
            }
            else
            {
                pnNNQuanHan.Visible = false;
                rdNNQuaHan.ClearSelection();
                Cls_Comon.SetFocus(this, this.GetType(), cmdUpdateBanAnST.ClientID);
            }
        }
        protected void txtNgaymophientoa_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgayMoPhienToa.Text))
            {
                if (String.IsNullOrEmpty(txtNgayBanAn.Text))
                    txtNgayBanAn.Text = txtNgayMoPhienToa.Text;
            }
        }

        protected void cmdHuyBanAn_Click(object sender, EventArgs e)
        {
            // Xóa thông tin bản án
            Decimal BanAnID = 0;
            decimal VuAnID = Session[ENUM_LOAIAN.AN_HINHSU] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            AHS_SOTHAM_BANAN banan = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            if (banan != null)
                BanAnID = banan.ID;
            
            //------Xoa toi danh---------------------
            List<AHS_SOTHAM_BANAN_DIEU_CHITIET> toiDanhs = dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.Where(x => x.BANANID == BanAnID 
                                                                                                    && x.VUANID== VuAnID).ToList();
            if (toiDanhs.Count > 0)
                dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.RemoveRange(toiDanhs);
            
            //-- Xóa thông tin bản án bị cáo------------------
            List<AHS_SOTHAM_BANAN_BICAO> biCaos = dt.AHS_SOTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID).ToList();
            if (biCaos.Count > 0)
                dt.AHS_SOTHAM_BANAN_BICAO.RemoveRange(biCaos);
            //---------------------------
            dt.AHS_SOTHAM_BANAN.Remove(banan);

            dt.SaveChanges();
            ResetControl();
            lttMsgBanAn.Text = "Xóa bản án thành công!";
        }
        private void ResetControl()
        {
            txtNgayMoPhienToa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtDiaDiem.Text = "";
            txtNgayBanAn.Text = "";
            txtSoBanAn.Text = "";
            rdAnLe.ClearSelection();
            rdAnRutGon.ClearSelection();
            rdBaoLucGD.ClearSelection();
            rdXetXuLuuDong.ClearSelection();
            txt_XXToiDanh_NangHon.Text = "";
            txt_XXToiDanh_NheHon.Text = "";
            txt_XXKhoan_NangHon.Text = "";
            txt_XXKhoan_NheHon.Text = "";
            txt_XXHinhPhat_NangHon.Text = "";
            txt_XXHinhPhat_NheHon.Text = "";
            txtTSChiemDoat.Text = "";
            txtTSThietHai.Text = "";
            rdPNTM_NhaNuoc.ClearSelection();
            rdPNTM_NuocNgoai.ClearSelection();
            rdViPhamHanTamGiam.ClearSelection();
            txtViPham_SoBiCao.Text = "";
            rdIsPhucHoi.ClearSelection();
            txtPhucHoi_SoBiCao.Text = "";
            rdKhoiTo.ClearSelection();
            txtKhoiTo_SoBiCao.Text = "";
            rdTraAn_VKSKoNhan.ClearSelection();
            rdViPham_CtacQuanLy.ClearSelection();
            rdToaAn_XacMinh.ClearSelection();
            rdToaAn_KoXacMinh.ClearSelection();
            rdToaAn_BSTaiLieu.ClearSelection();
            rdToaAnApDungBaoVe.ClearSelection();
            rdVuAnQuaHan.ClearSelection();
            rdNNQuaHan.ClearSelection();
            LoadDsBiCao_AnPhi();
            lbtDownload.Visible = false;
            lbtXoaFile.Visible = false;
            MSG_file.Text = string.Empty;
            hddID.Value = "0";
        }

        string Tonghophinhphat_bicao(Decimal vuanid, Decimal BicaoID)
        {
            AHS_SOTHAM_BANAN_DIEU_CHITIET_BL ahs_tonghop = new AHS_SOTHAM_BANAN_DIEU_CHITIET_BL();
            string tbla = ahs_tonghop.Tonghophinhphat_ST(vuanid, BicaoID);
            return tbla;
        }
    }
}

