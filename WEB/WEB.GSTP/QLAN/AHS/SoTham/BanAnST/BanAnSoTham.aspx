﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true"
    CodeBehind="BanAnSoTham.aspx.cs" Inherits="WEB.GSTP.QLAN.AHS.SoTham.BanAnST.BanAnSoTham" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/UI/js/base64.js"></script>
    <script type="text/javascript" src="/UI/js/vgcaplugin.js"></script>
    <script src="../../../../UI/js/Common.js"></script>
    <asp:HiddenField ID="hddID" runat="server" Value="0" />
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddTuCachPN" Value="1" runat="server" />
    <asp:HiddenField ID="hddBanAnID" runat="server" Value="0" />
    <style>
        .align_right {
            text-align: right;
        }

        .phantrang_bottom {
            display: block;
            line-height: 20px;
            margin-bottom: 2px;
            margin-top: 5px;
            overflow: hidden;
            width: 100%;
        }

        .col1 {
            width: 130px;
        }

        .col2 {
            width: 230px;
        }

        .col3 {
            width: 100px;
        }

        .ajax__calendar_container {
            width: 180px;
        }

        .ajax__calendar_body {
            width: 100%;
            height: 145px;
        }
    </style>
    <div class="box">
        <div class="box_nd">
            <div class="truong">
                <div class="boxchung">
                    <h4 class="tleboxchung">Thông tin bản án sơ thẩm</h4>
                    <div class="boder" style="padding: 10px;">
                        <asp:HiddenField ID="hddToaAnID" runat="server" Value="0" />
                        <asp:TextBox ID="txtToaAn" CssClass="user" runat="server"
                            Width="200px" ReadOnly="true" Visible="false"></asp:TextBox>
                        <table class="table1">
                            <tr>
                                <td style="width: 122px;">Ngày mở phiên tòa<span class="batbuoc">(*)</span></td>
                                <td style="width: 120px;">
                                    <asp:TextBox ID="txtNgayMoPhienToa" AutoPostBack="true" OnTextChanged="txtNgaymophientoa_TextChanged" runat="server" CssClass="user"
                                        Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtNgayMoPhienToa"
                                        Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtNgayMoPhienToa"
                                        Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                                <td style="width: 90px;">Địa điểm<span class="batbuoc">(*)</span></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtDiaDiem" CssClass="user" runat="server" Width="79%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Ngày bản án<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtNgayBanAn" runat="server" CssClass="user"
                                        Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtNgayBanAn"
                                        Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtNgayBanAn"
                                        Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                                <td>Số bản án<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtSoBanAn" CssClass="user align_right"
                                        runat="server" Width="100px"
                                        onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>

                                <td style="width: 70px;">Người ký<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:HiddenField ID="hddNguoiKyID" runat="server" />
                                    <asp:TextBox ID="txtNguoiKy" CssClass="user" runat="server" Width="200px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>Đính kèm tệp</td>
                                <td colspan="5">
                                    <asp:HiddenField ID="hddFilePath" runat="server" />
                                    <asp:CheckBox ID="chkKySo" Visible="false" Checked="true" runat="server" onclick="CheckKyso();" Text="Sử dụng ký số file đính kèm" />
                                    <br />
                                    <asp:HiddenField ID="hddFileKySo" runat="server" Value="" />
                                    <asp:HiddenField ID="hddSessionID" runat="server" />
                                    <asp:HiddenField ID="hddURLKS" runat="server" />
                                    <div id="zonekyso" style="display: none; margin-bottom: 5px; margin-top: 10px;">
                                        <button type="button" class="buttonkyso" id="TruongPhongKyNhay" onclick="exc_sign_file1();">Chọn file đính kèm và ký số</button>
                                        <button type="button" class="buttonkyso" id="_Config" onclick="vgca_show_config();">Cấu hình</button><br />
                                        <ul id="file_name" style="list-style: none; margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; line-height: 18px;">
                                        </ul>
                                    </div>
                                    <div id="zonekythuong" style="margin-top: 10px; width: 80%;">
                                        <cc1:AsyncFileUpload ID="AsyncFileUpLoad" runat="server" CompleteBackColor="Lime" UploaderStyle="Modern" OnUploadedComplete="AsyncFileUpLoad_UploadedComplete"
                                            ErrorBackColor="Red" ThrobberID="Throbber" UploadingBackColor="#66CCFF" />
                                        <asp:Image ID="Throbber" runat="server"
                                            ImageUrl="~/UI/img/loading-gear.gif" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="5">
                                    <div style="float: left;">
                                        <div style="float: left;">
                                            <asp:LinkButton ID="lbtDownload" Visible="false"
                                                runat="server" Text="Tải file đính kèm" OnClick="lbtDownload_Click"></asp:LinkButton>
                                        </div>
                                        <div style="float: left; margin-left: 15px; font-weight: bold;">
                                            <asp:ImageButton Visible="false" ID="lbtXoaFile" runat="server" ToolTip="Xóa"
                                                ImageUrl="/UI/img/xoa.gif" Width="17px" OnClick="lbtXoaFile_Click"
                                                OnClientClick="return confirm('Bạn thực sự muốn xóa? ');" />
                                        </div>
                                        <div style="float: left; width: 100%; color: red">
                                            <asp:Literal ID="MSG_file" runat="server" Text=""></asp:Literal>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6"></td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div class="boxchung">
                                        <h4 class="tleboxchung">Thông tin vụ án theo thống kê</h4>
                                        <div class="boder" style="padding: 10px;">
                                            <table class="table1">

                                                <tr>
                                                    <td colspan="2">
                                                        <div style="float: left; line-height: 28px; width: 115px;">Có áp dụng Án lệ<span class="batbuoc">(*)</span></div>
                                                        <asp:RadioButtonList ID="rdAnLe" runat="server"
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td colspan="2">
                                                        <div style="float: left; line-height: 28px; width: 105px;">Án rút gọn<span class="batbuoc">(*)</span></div>
                                                        <asp:RadioButtonList ID="rdAnRutGon" runat="server"
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div style="float: left; line-height: 28px; width: 115px;">Bạo lực gia đình<span class="batbuoc">(*)</span> </div>
                                                        <asp:RadioButtonList ID="rdBaoLucGD" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td colspan="2">
                                                        <div style="float: left; line-height: 28px; width: 105px;">Xét xử lưu động<span class="batbuoc">(*)</span></div>

                                                        <asp:RadioButtonList ID="rdXetXuLuuDong" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div style="float: left; width: 100%; margin-bottom: 5px;">Tòa án áp dụng tội danh khác VKS truy tố<span class="batbuoc">(*)</span></div>

                                                        <div style="float: left;">
                                                            Nặng hơn
                                                            <asp:TextBox ID="txt_XXToiDanh_NangHon" runat="server"
                                                                CssClass="user align_right" onkeypress="return isNumber(event)"
                                                                Width="50px"></asp:TextBox>
                                                            (bị cáo)
                                                        </div>
                                                        <div style="float: left; margin-left: 10px;">
                                                            Nhẹ hơn
                                                            <asp:TextBox ID="txt_XXToiDanh_NheHon" runat="server"
                                                                CssClass="user align_right" onkeypress="return isNumber(event)"
                                                                Width="50px"></asp:TextBox>
                                                            (bị cáo)
                                                        </div>
                                                    </td>

                                                    <td colspan="2">
                                                        <div style="float: left; width: 100%; margin-bottom: 5px;">
                                                            Tòa án xét xử theo khoản khác VKS truy tố<span class="batbuoc">(*)</span>
                                                        </div>
                                                        <div style="float: left;">
                                                            Nặng hơn
                                                            <asp:TextBox ID="txt_XXKhoan_NangHon" runat="server"
                                                                CssClass="user align_right" onkeypress="return isNumber(event)"
                                                                Width="50px"></asp:TextBox>
                                                            (bị cáo)
                                                        </div>
                                                        <div style="float: left; margin-left: 10px;">
                                                            Nhẹ hơn
                                                            <asp:TextBox ID="txt_XXKhoan_NheHon" runat="server"
                                                                CssClass="user align_right" onkeypress="return isNumber(event)"
                                                                Width="50px"></asp:TextBox>
                                                            (bị cáo)
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div style="float: left; width: 100%; margin-bottom: 5px;">
                                                            Tòa án áp dụng hình phạt khác theo đề nghị của VKS
                                                        <span class="batbuoc">(*)</span>
                                                        </div>
                                                        <div style="float: left;">
                                                            Nặng hơn
                                                            <asp:TextBox ID="txt_XXHinhPhat_NangHon" runat="server"
                                                                CssClass="user align_right" onkeypress="return isNumber(event)"
                                                                Width="50px"></asp:TextBox>(bị cáo)
                                                        </div>
                                                        <div style="float: left; margin-left: 10px;">
                                                            Nhẹ hơn
                                                        <asp:TextBox ID="txt_XXHinhPhat_NheHon" runat="server"
                                                            CssClass="user align_right" Width="50px"
                                                            onkeypress="return isNumber(event)"></asp:TextBox>(bị cáo)
                                                        </div>
                                                    </td>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Tài sản chiếm đoạt  <span class="batbuoc">(*)</span>

                                                        <asp:TextBox ID="txtTSChiemDoat" runat="server"
                                                            onkeyup="javascript:this.value=Comma(this.value);"
                                                            onkeypress="return isNumber(event)" CssClass="user align_right"
                                                            Width="160px"></asp:TextBox>(VNĐ)</td>
                                                    <td colspan="2">Tài sản thiệt hại  <span class="batbuoc">(*)</span>
                                                        <asp:TextBox ID="txtTSThietHai" runat="server" onkeyup="javascript:this.value=Comma(this.value);"
                                                            onkeypress="return isNumber(event)" CssClass="user align_right"
                                                            Width="160px"></asp:TextBox>(VNĐ)</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr id="row_tucach_phapnhan_tm" runat="server">
                                                    <td style="width: 200px;">Pháp nhân thương mại có vốn góp nhà nước<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdPNTM_NhaNuoc"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <td style="width: 200px;">Pháp nhân thương mại có vốn đầu tư nước ngoài<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdPNTM_NuocNgoai"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 200px;">Vi phạm hạn tạm giam trong giai đoạn xét xử<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdViPhamHanTamGiam"
                                                            runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="true" OnSelectedIndexChanged="rdViPhamHanTamGiam_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <asp:Panel ID="pnViPham" runat="server" Enabled="false">
                                                        <td colspan="2">
                                                            <div style="float: left; width: 58px; line-height: 22px;">Số bị cáo</div>
                                                            <asp:TextBox ID="txtViPham_SoBiCao" runat="server"
                                                                onkeypress="return isNumber(event)" CssClass="user align_right"
                                                                Width="102px"></asp:TextBox>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="col1">Tòa án quyết định phục hồi vụ án<span class="batbuoc">(*)</span></td>
                                                    <td class="col2">
                                                        <asp:RadioButtonList ID="rdIsPhucHoi"
                                                            runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="true" OnSelectedIndexChanged="rdIsPhucHoi_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <asp:Panel ID="pnPhucHoi" runat="server" Enabled="false">
                                                        <td colspan="2">
                                                            <div style="float: left; width: 58px; line-height: 22px;">Số bị cáo</div>
                                                            <asp:TextBox ID="txtPhucHoi_SoBiCao" runat="server"
                                                                onkeypress="return isNumber(event)" CssClass="user align_right"
                                                                Width="102px"></asp:TextBox>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="col1">Khởi tố vụ án tại phiên tòa<span class="batbuoc">(*)</span></td>
                                                    <td class="col2">
                                                        <asp:RadioButtonList ID="rdKhoiTo"
                                                            runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="true" OnSelectedIndexChanged="rdKhoiTo_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <asp:Panel ID="pnKhoiTo" runat="server" Enabled="false">
                                                        <td colspan="2">
                                                            <div style="float: left; width: 58px; line-height: 22px;">Số bị cáo</div>
                                                            <asp:TextBox ID="txtKhoiTo_SoBiCao" runat="server"
                                                                onkeypress="return isNumber(event)" CssClass="user align_right"
                                                                Width="102px"></asp:TextBox>
                                                        </td>
                                                    </asp:Panel>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>Vụ án trả hồ sơ nhưng VKS không chấp nhận yêu cầu của Tòa án<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdTraAn_VKSKoNhan"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td style="width: 280px;">Kiến nghị sửa chữa thiếu sót, vi phạm trong công tác quản lý<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdViPham_CtacQuanLy"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <!----------------------------------------->
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>Tòa án tiến hành xác minh, thu thập, bổ sung chứng cứ<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdToaAn_XacMinh"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>Tòa án tiến hành xác minh, thu thập chứng cứ khi đã yêu cầu mà VKS không bổ sung được
                                                        <span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdToaAn_KoXacMinh"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>
                                                <!----------------------------------------->
                                                <tr>
                                                    <td>Tòa án yêu cầu VKS bổ sung tài liệu, chứng cứ<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdToaAn_BSTaiLieu"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>Tòa án đề nghị các cơ quan áp dụng các biện pháp bảo vệ<span class="batbuoc">(*)</span></td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rdToaAnApDungBaoVe"
                                                            runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                </tr>
                                                <!----------------------------------------->
                                                <tr>
                                                    <td colspan="4" style="border-bottom: dotted 1px #dcdcdc; padding-bottom: 2px;"></td>
                                                </tr>

                                                <tr>
                                                    <td rowspan="2">Vụ án quá hạn luật định<span class="batbuoc">(*)</span></td>
                                                    <td rowspan="2">
                                                        <asp:RadioButtonList ID="rdVuAnQuaHan" AutoPostBack="true"
                                                            runat="server" RepeatDirection="Horizontal"
                                                            OnSelectedIndexChanged="rdVuAnQuaHan_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>

                                                    <td colspan="2">
                                                        <asp:Panel ID="pnNNQuanHan" runat="server" Visible="false">
                                                            <asp:RadioButtonList ID="rdNNQuaHan" runat="server"
                                                                RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">Nguyên nhân chủ quan </asp:ListItem>
                                                                <asp:ListItem Value="1">Nguyên nhân khách quan </asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>


                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" style="text-align: center">
                                    <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                                        <asp:Literal ID="lttMsgBanAn" runat="server"></asp:Literal>
                                    </div>
                                    <asp:Button ID="cmdUpdateBanAnST" runat="server" CssClass="buttoninput"
                                        Text="Lưu bản án sơ thẩm" OnClick="cmdUpdateBanAnST_Click"
                                        OnClientClick="return validate();" />
                                    <asp:Button ID="cmdHuyBanAn" runat="server" CssClass="buttoninput"
                                        Text="Xóa bản án" OnClick="cmdHuyBanAn_Click"
                                        OnClientClick="return confirm('Bạn thực sự muốn xóa bản án này? ');" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <asp:Panel ID="pnAnPhi" runat="server">
                    <div class="boxchung">
                        <h4 class="tleboxchung">Quyết định và hình phạt</h4>
                        <div class="boder" style="padding: 10px;">
                            <div class="zone_ghichu" style="display: none;">
                                <b>Ghi chú</b><br />
                                <b>1. Lưu án phí cho bị cáo</b><br />
                                1.1. Nhập mức án phí cho bị cáo<br />
                                1.2. Ấn chọn "Lưu" để cập nhật thông tin<br />
                                <b>2. Cập nhật quyết định & điều luật áp dụng cho bị cáo</b><br />
                                2.1. Chọn bị cáo<br />
                                2.2. Chọn "Điều luật áp dụng" của bị cáo được chọn<br />
                            </div>
                            <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                                <asp:Literal ID="lttMsgAnPhi" runat="server"></asp:Literal>
                            </div>
                            <div style="">
                                <asp:Panel runat="server" ID="pndata">
                                    <div class="phantrang">
                                        <div class="sobanghi">
                                        </div>
                                        <div class="sotrang">
                                            <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                                OnClick="lbTBack_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                                Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                            <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                            <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                                Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                                Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                                Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                                Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                            <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                                Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                                OnClick="lbTNext_Click"></asp:LinkButton>
                                        </div>
                                    </div>

                                    <asp:Repeater ID="rpt" runat="server"
                                        OnItemCommand="rpt_ItemCommand"
                                        OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <table class="table2" width="100%" border="1">
                                                <tr class="header">
                                                    <td style="width: 42px;">
                                                        <div align="center"><strong>TT</strong></div>
                                                    </td>
                                                    <td width="20%">
                                                        <div align="center"><strong>Bị cáo</strong></div>
                                                    </td>
                                                    <td width="80px">
                                                        <div align="center"><strong>Tham gia phiên tòa</strong></div>
                                                    </td>
                                                    <td width="60px">
                                                        <div align="center"><strong>Án phí (Đơn vị là đồng)</strong></div>
                                                    </td>
                                                    
                                                    <td width="100px">
                                                        <div align="center"><strong>Ngày nhận bản án</strong></div>
                                                    </td>
                                                    <td width="90px">
                                                        <div align="center"><strong>Hình phạt tổng hợp</strong></div>
                                                    </td>
                                                    <td>
                                                        <div align="center"><strong>Điều luật áp dụng</strong></div>
                                                    </td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hddBiCao" runat="server" Value='<%#Eval("ID") %>' />
                                                    <%# Eval("STT") %></td>
                                                <td><%#Eval("HoTen") %></td>
                                                <td>
                                                    <div style="text-align: center;">
                                                        <asp:CheckBox ID="chkThamGiaPhienToa" runat="server"
                                                            Checked='<%# (Convert.ToInt16(Eval("IsThamGiaPhienToa"))>0)? true:false %>' />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAnPhi" runat="server" Text='<%#Eval("AnPhi") %>'
                                                        onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"
                                                        CssClass="user align_right"></asp:TextBox></div>
                                                </td>
                                               
                                                <td>
                                                    <div style="text-align: center;">
                                                        <asp:TextBox ID="txtNgaynhanbanan" runat="server"
                                                            Text='<%# string.Format("{0:dd/MM/yyyy}",Eval("NgayNhanBanAn")) %>' CssClass="user" Width="70px"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtNgaynhanbanan_CalendarExtender" runat="server" TargetControlID="txtNgaynhanbanan" Format="dd/MM/yyyy" />
                                                        <cc1:MaskedEditExtender ID="txtNgaynhanbanan_MaskedEditExtender3" runat="server" TargetControlID="txtNgaynhanbanan" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="True" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="₫" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="," CultureThousandsPlaceholder="." CultureTimePlaceholder=":" />
                                                    </div>
                                                </td>
                                                 <td>
                                                    <div style="text-align: center;">
                                                        <asp:Label ID ="lblTHtoidanh" runat="server" Text="" ></asp:Label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Panel ID="lnLinkToiDanh" runat="server">
                                                        <div align="center">
                                                            <a href="javascript:;" onclick="popupChonToiDanh(<%#Eval("ID") %>)" class="link_ds">Điều luật áp dụng</a>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>
                                    </asp:Repeater>

                                    <div class="phantrang_bottom">
                                        <div class="sobanghi">
                                            <asp:Button ID="cmdUpdateAnPhi" runat="server" CssClass="buttoninput"
                                                Text="Lưu thông tin" OnClick="cmdUpdateAnPhi_Click" />
                                            <asp:Button ID="cmdReloadParent" runat="server" CssClass="buttoninput" style="display:none" OnClick="cmdReloadParent_Click" />
                                        </div>
                                        <div class="sotrang">
                                            <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                                OnClick="lbTBack_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                                Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                            <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                            <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                                Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                                Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                                Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                                Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                            <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                            <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                                Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                                OnClick="lbTNext_Click"></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

    <script>
        function validate() {
            var NgaySoSanh = '<%= NgaySoSanh%>';
            var txtNgayMoPhienToa = document.getElementById('<%=txtNgayMoPhienToa.ClientID%>');
            if (!CheckDateTimeControl(txtNgayMoPhienToa, 'Ngày mở phiên tòa'))
                return false;
            //if (NgaySoSanh != '') {
            //    if (!SoSanh2Date(txtNgayMoPhienToa, 'Ngày mở phiên tòa', NgaySoSanh, 'Ngày ra quyết định vụ án'))
            //        return false;
            //}


            //--------------
            var txtDiaDiem = document.getElementById('<%=txtDiaDiem.ClientID%>');
            if (!Common_CheckEmpty(txtDiaDiem.value)) {
                alert('Bạn chưa chọn "Địa điểm".Hãy kiểm tra lại!');
                txtDiaDiem.focus();
                return false;
            }
            //--------------
            var txtSoBanAn = document.getElementById('<%=txtSoBanAn.ClientID%>');
            if (!Common_CheckEmpty(txtSoBanAn.value)) {
                alert('Bạn chưa nhập số bản án.Hãy kiểm tra lại!');
                txtSoBanAn.focus();
                return false;
            }

            var txtNgayBanAn = document.getElementById('<%=txtNgayBanAn.ClientID%>');
            if (!CheckDateTimeControl(txtNgayBanAn, 'Ngày bản án'))
                return false;
            //if (NgaySoSanh != '') {
            //    if (!SoSanh2Date(txtNgayBanAn, 'Ngày bản án', NgaySoSanh, 'Ngày ra quyết định vụ án'))
            //        return false;
            //}
            //if (!SoSanh2Date(txtNgayMoPhienToa, 'Ngày mở phiên tòa', txtNgayBanAn.value, 'Ngày bản án'))
            //    return false;

            //--------------
            var hddNguoiKyID = document.getElementById('<%=hddNguoiKyID.ClientID%>');
            var txtNguoiKy = document.getElementById('<%=txtNguoiKy.ClientID%>');
            if (!Common_CheckEmpty(hddNguoiKyID.value)) {
                alert('Bạn chưa chọn mục "Người ký". Hãy kiểm tra lại!');
                txtNguoiKy.focus();
                return false;
            }
            if (!validate_chitieu_tk())
                return false;

            return true;
        }

        function validate_chitieu_tk() {
            if (!validate_CheckRadio())
                return false;

            var hddTuCachPN = document.getElementById('<%=hddTuCachPN.ClientID%>');
            if (hddTuCachPN.value == "0") {
                var rdPNTM_NhaNuoc = document.getElementById('<%=rdPNTM_NhaNuoc.ClientID%>');
                msg = 'Mục "Pháp nhân thương mại có vốn góp nhà nước" bắt buộc phải chọn. Hãy kiểm tra lại!';
                if (!CheckChangeRadioButtonList(rdPNTM_NhaNuoc, msg))
                    return false;

                var rdPNTM_NuocNgoai = document.getElementById('<%=rdPNTM_NuocNgoai.ClientID%>');
                msg = 'Mục "Pháp nhân thương mại có vốn đầu tư nước ngoài" bắt buộc phải chọn. Hãy kiểm tra lại!';
                if (!CheckChangeRadioButtonList(rdPNTM_NuocNgoai, msg))
                    return false;
            }
            var rdToaAnApDungBaoVe = document.getElementById('<%=rdToaAnApDungBaoVe.ClientID%>');
            msg = 'Mục "Tòa án đề nghị các cơ quan áp dụng các biện pháp bảo vệ" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdToaAnApDungBaoVe, msg))
                return false;

            //---------------------------
            var rdViPhamHanTamGiam = document.getElementById('<%=rdViPhamHanTamGiam.ClientID%>');
            msg = 'Mục "Vi phạm hạn tạm giam trong giai đoạn xét xử" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdViPhamHanTamGiam, msg))
                return false;
            else {
                var selected_value = GetStatusRadioButtonList(rdViPhamHanTamGiam);
                if (selected_value == 1) {
                    var txtViPham_SoBiCao = document.getElementById('<%=txtViPham_SoBiCao.ClientID%>');
                    if (!Common_CheckEmpty(txtTStxtViPham_SoBiCaoThietHai.value)) {
                        alert('Bạn chưa nhập "Số bị cáo"!');
                        txtViPham_SoBiCao.focus();
                        return false;
                    }
                }
            }
            //-----------------------------
            var rdIsPhucHoi = document.getElementById('<%=rdIsPhucHoi.ClientID%>');
            msg = 'Mục "Tòa án quyết định phục hồi vụ án" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdIsPhucHoi, msg))
                return false;
            else {
                var selected_value = GetStatusRadioButtonList(rdIsPhucHoi);
                if (selected_value == 1) {
                    var txtPhucHoi_SoBiCao = document.getElementById('<%=txtPhucHoi_SoBiCao.ClientID%>');
                    if (!Common_CheckEmpty(txtPhucHoi_SoBiCao.value)) {
                        alert('Bạn chưa nhập "Số bị cáo"!');
                        txtPhucHoi_SoBiCao.focus();
                        return false;
                    }
                }
            }
            //-----------------------------
            var rdKhoiTo = document.getElementById('<%=rdKhoiTo.ClientID%>');
            msg = 'Mục "Khởi tố vụ án tại phiên tòa" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdKhoiTo, msg))
                return false;
            else {
                var selected_value = GetStatusRadioButtonList(rdKhoiTo);
                if (selected_value == 1) {
                    var txtKhoiTo_SoBiCao = document.getElementById('<%=txtKhoiTo_SoBiCao.ClientID%>');
                    if (!Common_CheckEmpty(txtKhoiTo_SoBiCao.value)) {
                        alert('Bạn chưa nhập "Số bị cáo"!');
                        txtKhoiTo_SoBiCao.focus();
                        return false;
                    }
                }
            }
            //-----------------------------
            var rdTraAn_VKSKoNhan = document.getElementById('<%=rdTraAn_VKSKoNhan.ClientID%>');
            msg = 'Mục "Vụ án trả hồ sơ nhưng VKS không chấp nhận yêu cầu của Tòa án" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdTraAn_VKSKoNhan, msg))
                return false;
            //-----------------------------
            var rdToaAn_BSTaiLieu = document.getElementById('<%=rdToaAn_BSTaiLieu.ClientID%>');
            msg = 'Mục "Tòa án yêu cầu VKS bổ sung tài liêu, chứng cứ" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdToaAn_BSTaiLieu, msg))
                return false;

            //-----------------------------
            var rdToaAn_XacMinh = document.getElementById('<%=rdToaAn_XacMinh.ClientID%>');
            msg = 'Mục "Kiến nghị sửa chữa thiếu sót, vi phạm trong công tác quản lý" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdToaAn_XacMinh, msg))
                return false;
            //-----------------------------
            var rdToaAn_KoXacMinh = document.getElementById('<%=rdToaAn_KoXacMinh.ClientID%>');
            msg = 'Mục "Tòa án tiến hành xác minh, thu thập chứng cứ khi đã yêu cầu mà VKS không bổ sung được" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdToaAn_KoXacMinh, msg))
                return false;

            //-----------------------------
            var rdVuAnQuaHan = document.getElementById('<%=rdVuAnQuaHan.ClientID%>');
            msg = 'Mục "Vụ án quá hạn" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdVuAnQuaHan, msg))
                return false;
            else {
                var selected_value = GetStatusRadioButtonList(rdVuAnQuaHan);
                if (selected_value == 1) {
                    var rdNNQuaHan = document.getElementById('<%=rdNNQuaHan.ClientID%>');
                    msg = 'Mục "Nguyên nhân quá hạn" bắt buộc phải chọn. Hãy kiểm tra lại!';
                    if (!CheckChangeRadioButtonList(rdNNQuaHan, msg))
                        return false;
                }
            }

            return true;
        }
        function validate_CheckRadio() {
            var msg = "";
            var rdAnLe = document.getElementById('<%=rdAnLe.ClientID%>');
            msg = 'Mục "Có áp dụng Án lệ" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdAnLe, msg))
                return false;

            //-----------------------------
            var rdAnRutGon = document.getElementById('<%=rdAnRutGon.ClientID%>');
            msg = 'Mục "Án rút gọn" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdAnRutGon, msg))
                return false;
            //-----------------------------
            var rdBaoLucGD = document.getElementById('<%=rdBaoLucGD.ClientID%>');
            msg = 'Mục "Bạo lực gia đình" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdBaoLucGD, msg))
                return false;
            //----------------------
            var rdXetXuLuuDong = document.getElementById('<%=rdXetXuLuuDong.ClientID%>');
            msg = 'Mục "Xét xử lưu động" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdXetXuLuuDong, msg))
                return false;
            //-----------------------------
            return true;
        }
    </script>
    <script>
        function popupChonToiDanh(BiCanID) {
            var BanAnID = document.getElementById('<%=hddID.ClientID%>').value;
            var link = "/QLAN/AHS/SoTham/BanAnST/popup/pToiDanh.aspx?aID=" + BanAnID + "&bID=" + BiCanID +"&vID=<%=VuAnID%>";
            var width = 900;
            var height = 850;
            PopupCenter(link, "Cập nhật điều luật áp dụng cho bị cáo", width, height);
        }
        function uploadStart(sender, args) {
            var fileName = args.get_fileName();

            var fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            var validFilesTypes = ["exe", "dll", "msi", "bat"];
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (fileExt == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }
            if (isValidFile) {
                var err = new Error();
                err.name = "Lỗi tải lên";
                err.message = "Hệ thống không lưu trữ file định dạng (exe,dll,msi,bat). Hãy chọn lại!";
                throw (err);
                return false;
            }
            else
                return true;
        }
    </script>

    <script type="text/javascript">
        var count_file = 0;
        function CheckKyso() {
            var chkKySo = document.getElementById('<%=chkKySo.ClientID%>');

            if (chkKySo.checked) {
                document.getElementById("zonekyso").style.display = "";
                document.getElementById("zonekythuong").style.display = "none";
            }
            else {
                document.getElementById("zonekyso").style.display = "none";
                document.getElementById("zonekythuong").style.display = "";
            }
        }
        function VerifyPDFCallBack(rv) {

        }

        function exc_verify_pdf1() {
            var prms = {};
            var hddSession = document.getElementById('<%=hddSessionID.ClientID%>');
            prms["SessionId"] = "";
            prms["FileName"] = document.getElementById("file1").value;

            var json_prms = JSON.stringify(prms);

            vgca_verify_pdf(json_prms, VerifyPDFCallBack);
        }

        function SignFileCallBack1(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                var hddFilePath = document.getElementById('<%=hddFilePath.ClientID%>');
                var new_item = document.createElement("li");
                new_item.innerHTML = received_msg.FileName;
                hddFilePath.value = received_msg.FileServer;
                //-------------Them icon xoa file------------------
                var del_item = document.createElement("img");
                del_item.src = '/UI/img/xoa.gif';
                del_item.style.width = "15px";
                del_item.style.margin = "5px 0 0 5px";
                del_item.onclick = function () {
                    if (!confirm('Bạn muốn xóa file này?')) return false;
                    document.getElementById("file_name").removeChild(new_item);
                }
                del_item.style.cursor = 'pointer';
                new_item.appendChild(del_item);

                document.getElementById("file_name").appendChild(new_item);
            } else {
                document.getElementById("_signature").value = received_msg.Message;
            }
        }

        //metadata có kiểu List<KeyValue> 
        //KeyValue là class { string Key; string Value; }
        function exc_sign_file1() {
            var prms = {};
            var scv = [{ "Key": "abc", "Value": "abc" }];
            var hddURLKS = document.getElementById('<%=hddURLKS.ClientID%>');
            prms["FileUploadHandler"] = hddURLKS.value.replace(/^http:\/\//i, window.location.protocol + '//');
            prms["SessionId"] = "";
            prms["FileName"] = "";
            prms["MetaData"] = scv;
            var json_prms = JSON.stringify(prms);
            vgca_sign_file(json_prms, SignFileCallBack1);
        }
        function RequestLicenseCallBack(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                document.getElementById("_signature").value = received_msg.LicenseRequest;
            } else {
                alert("Ký số không thành công:" + received_msg.Status + ":" + received_msg.Error);
            }
        }
        function pageLoad(sender, args) {
            $(function () {

                var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
                for (var selector in config) { $(selector).chosen(config[selector]); }
            });
        }
        function Loadds_bicao() {
                $("#<%= cmdReloadParent.ClientID %>").click();           
        }
    </script>
</asp:Content>


