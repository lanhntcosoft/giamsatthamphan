﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.AHS.SoTham
{
    public partial class ThuLy : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public String NgayHoSo;
        public Decimal VuAnID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {               
                if (Session[ENUM_LOAIAN.AN_HINHSU] != null)
                {
                    VuAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);

                    lstMsgB.Text = "";
                    if (!IsPostBack)
                    {
                        LoadDrop();                       
                        CheckQuyen();
                        LoadGrid();
                        LoadTHThuyLy(VuAnID);
                        if (rpt.Items.Count == 0)
                        {
                            SetNew_SoThuLy();
                        }
                    }
                }
                else
                    Response.Redirect("/Login.aspx");
            }
            catch (Exception ex) { lstMsgB.Text = ex.Message; }
        }
        private void LoadTHThuyLy(decimal vid)
        {
            //Load Truong hop thu lý
            AHS_VUAN obj = dt.AHS_VUAN.Where(x => x.ID == vid).FirstOrDefault();
            if (obj.TRUONGHOPGIAONHAN == 270)
            {   // Phúc tham huy
                ddTruongHopTL.SelectedValue = "236";
                ddTruongHopTL.Enabled = false;
            }
            else if (obj.TRUONGHOPGIAONHAN == 1758)
            {
                //GDT huy
                ddTruongHopTL.SelectedValue = "1777";
                ddTruongHopTL.Enabled = false;
            }
            else
            {
                ddTruongHopTL.Enabled = true;
            }
        }
        void CheckQuyen()
        {
            int GiaiDoan = 0;
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdThemmoi, oPer.CAPNHAT);
            Decimal VuAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            if (VuAnID == 0)
                Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");
            AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oT != null)
            {
                GiaiDoan = (int)oT.MAGIAIDOAN;
                NgayHoSo = oT.NGAYBANCAOTRANG + "" == "" ? "" : ((DateTime)oT.NGAYBANCAOTRANG).ToString("dd/MM/yyyy", cul);
                //NgayHoSo = ((DateTime)oT.NGAYBANCAOTRANG).ToString("dd/MM/yyyy", cul); //Ngay thu ly >= ngay quyet dinh (oT.NgayBanCaoTrang)
            }
            if (GiaiDoan == ENUM_GIAIDOANVUAN.PHUCTHAM || GiaiDoan == ENUM_GIAIDOANVUAN.THULYGDT)
            {
                hddGiaiDoanVuAn.Value = oT.MAGIAIDOAN+"";
                lstMsgB.Text = "Vụ án đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdThemmoi, false);
                hddIsShowCommand.Value = "False";
                return;
            }
            AHS_SOTHAM_BANAN ba = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            if (ba != null)
            {
                lstMsgB.Text = "Vụ án đã có bản án, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdThemmoi, false);
                hddIsShowCommand.Value = "False";
                return;
            }
            List<AHS_SOTHAM_CAOTRANG_DIEULUAT> lst = dt.AHS_SOTHAM_CAOTRANG_DIEULUAT.Where(x => x.VUANID == VuAnID).ToList();
            if (lst == null || lst.Count == 0)
            {
                lstMsgB.Text = "Các bị can trong vụ án chưa được gán tội danh. Đề nghị cập nhật thông tin này !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdThemmoi, false);
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
            if (Result != "")
            {
                lstMsgB.Text = Result;
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdThemmoi, false);
                hddIsShowCommand.Value = "False";
                return;
            }
        }
        void SetNew_SoThuLy()
        {
            //decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            //DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? date : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //decimal ToaID = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] +"");
            //AHS_SOTHAM_THULY_BL obj = new AHS_SOTHAM_THULY_BL();
            //try
            //{
            //    decimal STT = obj.GETNEWTT(ToaID, NgayThuLy);
            //    txtSoThuLy.Text = STT.ToString();
            //}
            //catch (Exception ex) { txtSoThuLy.Text = "1"; }
            Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
            txtNgayThuLy.Text = DateTime.Now.ToString("dd/MM/yyyy");
            if (!String.IsNullOrEmpty(txtNgayThuLy.Text))
            {
                DateTime ngaythuly = DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                //Số thụ lý mới
                txtSoThuLy.Text = oSTBL.GET_STL_NEW(DonViID, "AHS", ngaythuly).ToString();   
            }
        }
        private void LoadInfo(decimal ThuLyID)
        {
            lttCanhBao.Text = lstMsgB.Text = "";
            AHS_SOTHAM_THULY obj = dt.AHS_SOTHAM_THULY.Where(x => x.ID == ThuLyID).FirstOrDefault<AHS_SOTHAM_THULY>();
            if (obj != null)
            {
                txtNgayThuLy.Text = (DateTime)obj.NGAYTHULY == DateTime.MinValue ? "" : ((DateTime)obj.NGAYTHULY).ToString("dd/MM/yyyy", cul);
               
                txtSoThuLy.Text = obj.SOTHULY + "";
                txtTuNgay.Text = (DateTime)obj.THOIHANTUNGAY == DateTime.MinValue ? "" : ((DateTime)obj.THOIHANTUNGAY).ToString("dd/MM/yyyy", cul);
                txtDenNgay.Text = (DateTime)obj.THOIHANDENNGAY == DateTime.MinValue ? "" : ((DateTime)obj.THOIHANDENNGAY).ToString("dd/MM/yyyy", cul);
                //txtSoNgay.Text =  obj.THOIHAN_NGAY.ToString();
                //txtSoThang.Text = obj.THOIHAN_THANG.ToString();
                ddTruongHopTL.SelectedValue = obj.TRUONGHOPTHULY + "";
                if (ddTruongHopTL.SelectedValue == "233")// Tòa án trả hồ sơ - VKS chấp nhận điều tra bổ sung
                {
                    pnTTCaoTrang.Visible = true;
                    txtSoBanCaoTrang.Text = obj.SOBANCAOTRANG;
                    txtNgayBanCaoTrang.Text = obj.NGAYBANCAOTRANG + "" == "" ? "" : ((DateTime)obj.NGAYBANCAOTRANG).ToString("dd/MM/yyyy");
                }
                else
                {
                    pnTTCaoTrang.Visible = false;
                    txtSoBanCaoTrang.Text = "";
                    txtNgayBanCaoTrang.Text = "";
                }
                if (obj.UTTPDI == 1)
                    cbUTTP.Checked = true;
                CanhBao_TamGiam_KC_KN();
            }
        }
        void LoadDrop()
        {
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.TRUONGHOPTHULYAN);

            ddTruongHopTL.Items.Clear();
            //  ddTruongHopTL.Items.Add(new ListItem("--------Chọn--------", "0"));
            if (tbl != null && tbl.Rows.Count > 0)
            {
                foreach (DataRow row in tbl.Rows)
                    ddTruongHopTL.Items.Add(new ListItem(row["Ten"] + "", row["ID"] + ""));
            }
        }
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckValidate())
                return;
            //------------------------------
            //Decimal ThuLyID = (String.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
            //DateTime NGAYTHULY = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //String sothuly = txtSoThuLy.Text.Trim();
            //AHS_SOTHAM_THULY_BL objBL = new AHS_SOTHAM_THULY_BL();
            //Boolean IsExist = objBL.CheckExistSoThuLy(ThuLyID, sothuly, NGAYTHULY);
            //if (IsExist)
            //{
            //    lstMsgB.Text = "Đã có số thụ lý này. Đề nghị kiểm tra lại";
            //    return;
            //}
            //else
            //{
                Save();
                ResetForm();
                hddPageIndex.Value = "1";
                LoadGrid();
           //}
        }
        void Save()
        {
            Boolean IsNew = false;
            AHS_SOTHAM_THULY obj = null;
            String sothuly = txtSoThuLy.Text.Trim();
            Decimal VuAnId = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            DateTime NGAYTHULY = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.Now : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            Decimal ThuLyID = (String.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
            decimal ToaID = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            AHS_SOTHAM_THULY_BL objBL = new AHS_SOTHAM_THULY_BL();
            //------------------------------
            if (ThuLyID >0)
            {
                obj = dt.AHS_SOTHAM_THULY.Where(x => x.ID == ThuLyID).FirstOrDefault<AHS_SOTHAM_THULY>();
                IsNew = false;
            }
            else
            {                
                obj = new AHS_SOTHAM_THULY();
                IsNew = true;
            }
           
            obj.VUANID = VuAnId;
            obj.SOTHULY = txtSoThuLy.Text;
            obj.NGAYTHULY = NGAYTHULY;
            obj.THOIHANTUNGAY = NGAYTHULY;
            obj.THOIHANDENNGAY = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? DateTime.Now.AddDays(15) : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            
            //-----------------------------------------
            obj.TRUONGHOPTHULY = Convert.ToDecimal(ddTruongHopTL.SelectedValue);
            if (cbUTTP.Checked)
                obj.UTTPDI = 1;
            else
                obj.UTTPDI = 0;

            if (ddTruongHopTL.SelectedValue == "233")// Tòa án trả hồ sơ - VKS chấp nhận điều tra bổ sung
            {
                obj.SOBANCAOTRANG = txtSoBanCaoTrang.Text;
                obj.NGAYBANCAOTRANG = txtNgayBanCaoTrang.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBanCaoTrang.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            }
            if (IsNew)
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                               
                decimal STT = objBL.GETNEWTT(ToaID, NGAYTHULY);
                obj.TT = STT;
                dt.AHS_SOTHAM_THULY.Add(obj);
            }
            else
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            }
            dt.SaveChanges();

            // update giai đoạn vụ án = sotham
            AHS_VUAN objAn = dt.AHS_VUAN.Where(x => x.ID == VuAnId).FirstOrDefault<AHS_VUAN>();
            if (objAn != null)
            {
                objAn.MAGIAIDOAN = ENUM_GIAIDOANVUAN.SOTHAM;
                objAn.NGAYSUA = DateTime.Now;
                objAn.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                //anhvh add 26/06/2020
                GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                GD.GAIDOAN_INSERT_UPDATE("1", VuAnId, 2, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), 0, 0, 0, 0);
                //------------
            }
            dt.SaveChanges();
            lstMsgB.Text = "Lưu dữ liệu thành công!";
        }
        private void LoadGrid()
        {
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            AHS_SOTHAM_THULY_BL obj = new AHS_SOTHAM_THULY_BL();
            DataTable tbl = obj.GetByVuAnID(VuAnID);
            rpt.DataSource = tbl;
            rpt.DataBind();
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                DataRowView rowView = (DataRowView)e.Item.DataItem;

                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);
                Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);

                int MaGiaiDoanVuAn = (string.IsNullOrEmpty(hddGiaiDoanVuAn.Value)) ? 0 : Convert.ToInt32(hddGiaiDoanVuAn.Value);
                if (MaGiaiDoanVuAn == ENUM_GIAIDOANVUAN.PHUCTHAM || MaGiaiDoanVuAn == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    lblSua.Text = "Chi tiết";
                    Cls_Comon.SetLinkButton(lbtXoa, false);
                }
                int CheckDelete = (string.IsNullOrEmpty(rowView["CheckDelete"] + "")) ? 0 : Convert.ToInt16(rowView["CheckDelete"] + "");
                if (CheckDelete > 0)
                    lbtXoa.Visible = false;
                if (!Convert.ToBoolean(hddIsShowCommand.Value))
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
            }
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal ThuLyID = Convert.ToDecimal(e.CommandArgument.ToString());
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    hddID.Value = ThuLyID + ""; ;
                    LoadInfo(ThuLyID);
                    break;
                case "Xoa":
                    if (oPer.XOA == false)
                    {
                        lstMsgB.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
                    if (Result != "")
                    {
                        lstMsgB.Text = Result;
                        return;
                    }
                    xoa(ThuLyID);
                    break;
            }
        }
        public void xoa(decimal id)
        {
            decimal VuanID = 0;
            AHS_SOTHAM_THULY oND = dt.AHS_SOTHAM_THULY.Where(x => x.ID == id).FirstOrDefault();
            if (oND != null)
            {
                VuanID = oND.VUANID + "" == "" ? 0 : (decimal)oND.VUANID;
                #region Kiểm tra dữ liệu liên quan trước khi xóa
                // Kiểm tra bản án sơ thẩm
                AHS_SOTHAM_BANAN ba = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuanID).FirstOrDefault<AHS_SOTHAM_BANAN>();
                if (ba != null)
                {
                    lstMsgB.Text = "Vụ việc đã có bản sơ thẩm. Không được xóa.";
                    return;
                }
                // Kiểm tra quyết định vụ án
                AHS_SOTHAM_QUYETDINH_VUAN qd = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.VUANID == VuanID).FirstOrDefault<AHS_SOTHAM_QUYETDINH_VUAN>();
                if (qd != null)
                {
                    lstMsgB.Text = "Vụ việc đã có quyết định vụ án. Không được xóa.";
                    return;
                }
                // Kiểm tra quyết định bị can, bị cáo
                AHS_SOTHAM_QUYETDINH_BICAN qdbc = dt.AHS_SOTHAM_QUYETDINH_BICAN.Where(x => x.VUANID == VuanID).FirstOrDefault<AHS_SOTHAM_QUYETDINH_BICAN>();
                if (qdbc != null)
                {
                    lstMsgB.Text = "Vụ việc đã có quyết định bị can, bị cáo. Không được xóa.";
                    return;
                }
                // Kiểm tra người tiến hành tố tụng
                AHS_SOTHAM_HDXX hdxx = dt.AHS_SOTHAM_HDXX.Where(x => x.VUANID == VuanID).FirstOrDefault<AHS_SOTHAM_HDXX>();
                if (hdxx != null)
                {
                    lstMsgB.Text = "Vụ việc đã có thông tin người tiến hành tố tụng. Không được xóa.";
                    return;
                }
                // Kiểm tra phân công thẩm phán giải quyết
                AHS_THAMPHANGIAIQUYET gqst = dt.AHS_THAMPHANGIAIQUYET.Where(x => x.VUANID == VuanID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM).FirstOrDefault<AHS_THAMPHANGIAIQUYET>();
                if (gqst != null)
                {
                    lstMsgB.Text = "Vụ việc đã có thông tin phân công thẩm phán giải quyết. Không được xóa.";
                    return;
                }
                #endregion
                // Chuyển giai đoạn của vụ án từ sơ thẩm về hồ sơ
                AHS_VUAN objDon = dt.AHS_VUAN.Where(x => x.ID == VuanID).FirstOrDefault<AHS_VUAN>();
                //if (objDon != null)
                //{
                //    objDon.MAGIAIDOAN = ENUM_GIAIDOANVUAN.HOSO;
                //}
                dt.AHS_SOTHAM_THULY.Remove(oND);
                dt.SaveChanges();
                LoadGrid();
            }
            lstMsgB.Text = "Xóa thành công!";
        }
        void ResetForm()
        {
            hddID.Value = "0";
            txtDenNgay.Text = "";
            txtTuNgay.Text = txtNgayThuLy.Text = "";
            txtSoThuLy.Text = "";
            txtSoBanCaoTrang.Text = txtNgayBanCaoTrang.Text = "";
            txtNgayThuLy.Text = "";
            lttCanhBao.Text = "";
            ddTruongHopTL.SelectedIndex = 0;
            pnTTCaoTrang.Visible = false;
            cbUTTP.Checked = false;
        }
        protected void cmdThemmoi_Click(object sender, EventArgs e)
        {
            ResetForm();
            SetNew_SoThuLy();
        }
        protected void txtNgayThuLy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTuNgay.Text = txtNgayThuLy.Text;
                decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]), LoaiToiPhamID = 0;
                DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.Now : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                // số thụ lý
                // SetNew_SoThuLy(NgayThuLy);
                //----------------------------------
                AHS_VUAN vuan = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault<AHS_VUAN>();
                if (vuan != null)
                    LoaiToiPhamID = vuan.LOAITOIPHAMID + "" == "" ? 0 : (decimal)vuan.LOAITOIPHAMID;
               
                DM_DATAITEM dmLoaiToiPham = dt.DM_DATAITEM.Where(x => x.ID == LoaiToiPhamID && x.HIEULUC == 1).FirstOrDefault<DM_DATAITEM>();
                if (dmLoaiToiPham != null)
                {                    
                    if (NgayThuLy != DateTime.MinValue)
                    {
                        txtDenNgay.Enabled = false;
                        switch (dmLoaiToiPham.MA)
                        {
                            case ENUM_AHS_LOAITOIPHAM.IT_NGHIEMTRONG:
                                txtDenNgay.Text = (NgayThuLy.AddDays(30)).ToString("dd/MM/yyyy", cul);
                                break;
                            case ENUM_AHS_LOAITOIPHAM.NGHIEMTRONG:
                                txtDenNgay.Text = (NgayThuLy.AddDays(45)).ToString("dd/MM/yyyy", cul);
                                break;
                            case ENUM_AHS_LOAITOIPHAM.RAT_NGHIEMTRONG:
                                 txtDenNgay.Text = (NgayThuLy.AddMonths(2)).ToString("dd/MM/yyyy", cul);
                                break;
                            case ENUM_AHS_LOAITOIPHAM.DACBIET_NGHIEMTRONG:
                               txtDenNgay.Text = (NgayThuLy.AddMonths(3)).ToString("dd/MM/yyyy", cul);
                                break;
                            default:
                                //chưa xac dinh--> cho phep thay doi
                                txtDenNgay.Enabled = true;
                                break;
                        }
                    }
                    else
                    {
                        //chưa xac dinh--> cho phep thay doi
                        txtDenNgay.Enabled = true;
                    }
                }

                CanhBao_TamGiam_KC_KN();
                Cls_Comon.SetFocus(this, this.GetType(), txtSoThuLy.ClientID);
            }
            catch (Exception ex)
            {
                lstMsgB.Text = ex.Message;
            }
        }
        void CanhBao_TamGiam_KC_KN()
        {
            DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            int songaycanhbao = 3;
            AHS_SOTHAM_BIENPHAPNGANCHAN_BL objBL = new AHS_SOTHAM_BIENPHAPNGANCHAN_BL();
            DataTable tbl = objBL.GetAllBiCanBiGiamGiu(VuAnID);
            if (tbl != null)
            {
                String StrDisplay = "";
                String temp = "";
                String canhbao = "";
                DateTime ngaykt ;
                foreach (DataRow row in tbl.Rows)
                {
                    canhbao = "";
                    temp = "";                    
                    if (!String.IsNullOrEmpty(row["NgayKetThuc"] + "") )
                    {
                        ngaykt = Convert.ToDateTime(row["NgayKetThuc"] + "");
                        if (ngaykt != DateTime.MinValue)
                        {
                            temp = " Đến ngày: " + ngaykt.ToString("dd/MM/yyyy", cul);
                            if (ngaykt < NgayThuLy)
                                canhbao = "<span class='canhbao_thuly'>(Quá hạn)</span>";
                            else
                            {
                                if (NgayThuLy.AddDays(songaycanhbao) >= ngaykt)
                                    canhbao = "<span class='canhbao_thuly'>(Sắp hết hạn)</span>";
                            }
                        }
                    }
                    if (canhbao.Length > 0)
                    {
                        StrDisplay += "<li>Bị can: " + row["TenBiCan"].ToString() 
                                        + " - " + row["TenBienPhapNganChan"].ToString()
                                                + ". Từ ngày: " + (Convert.ToDateTime(row["NgayBatDau"] + "")).ToString("dd/MM/yyyy", cul)
                                                + temp
                                                + canhbao
                                     + "</li>";
                    }
                }
                lttCanhBao.Text = (StrDisplay.Length == 0) ? "" : ("<div id='canhbao_form'>" + "<ul>" + StrDisplay + "</ul></div>");
            }
        }
        protected void ddTruongHopTL_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddTruongHopTL.SelectedValue == "233")// Tòa án trả hồ sơ - VKS chấp nhận điều tra bổ sung
            {
                pnTTCaoTrang.Visible = true;
            }
            else
            {
                pnTTCaoTrang.Visible = false;
            }
        }
        private bool CheckValidate()
        {
            if (ddTruongHopTL.SelectedValue == "")
            {
                lstMsgB.Text = "Bạn chưa chọn trường hợp thụ lý. Hãy kiểm tra lại!";
                ddTruongHopTL.Focus();
                return false;
            }
            else if (ddTruongHopTL.SelectedValue == "233")
            {
                int lenghtSoBanCaoTrang = txtSoBanCaoTrang.Text.Trim().Length;
                if (lenghtSoBanCaoTrang == 0)
                {
                    lstMsgB.Text = "Bạn chưa nhập số bản cáo trạng. Hãy kiểm tra lại!";
                    txtSoBanCaoTrang.Focus();
                    return false;
                }
                else if (lenghtSoBanCaoTrang > 250)
                {
                    lstMsgB.Text = "Số bản cáo trạng không quá 250 ký tự. Hãy kiểm tra lại!";
                    txtSoBanCaoTrang.Focus();
                    return false;
                }
            }
            DateTime NgayThuLy = DateTime.Parse(txtNgayThuLy.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            if (DateTime.Compare(DateTime.Now, NgayThuLy) < 0)
            {
                lstMsgB.Text = "Ngày thụ lý phải nhỏ hơn ngày hiện tại. Hãy kiểm tra lại!";
                txtNgayThuLy.Focus();
                return false;
            }
            AHS_VUAN vuan = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (vuan != null)
            {
                if (vuan.NGAYXAYRA + "" != "")
                {
                    if (DateTime.Compare(NgayThuLy, (DateTime)vuan.NGAYXAYRA) < 0)
                    {
                        lstMsgB.Text = "Ngày thụ lý phải lớn hơn ngày xảy ra vụ án ("+((DateTime)vuan.NGAYXAYRA).ToString("dd/MM/yyyy")+ "). Hãy kiểm tra lại!";
                        txtNgayThuLy.Focus();
                        return false;
                    }
                }
            }

            //----------------------------
            string sothuly = txtSoThuLy.Text;
            if (!String.IsNullOrEmpty(txtNgayThuLy.Text))
            {
                DateTime ngaythuly = DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoTLTheoLoaiAn(DonViID, "AHS", sothuly, ngaythuly);
                if (CheckID > 0)
                {
                    Decimal CurrThuLyID = (string.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GET_STL_NEW(DonViID, "AHS", ngaythuly).ToString();
                    if (CheckID != CurrThuLyID)
                    {
                        //lbthongbao.Text = "Số thụ lý này đã có!";
                        strMsg = "Số thụ lý " + txtSoThuLy.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSoThuLy.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSoThuLy.Focus();
                        return false;
                    }
                }
            }
            //----------------------------
            return true;
        }
    }
}

