﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true"
    CodeBehind="ThuLy.aspx.cs" Inherits="WEB.GSTP.QLAN.AHS.SoTham.ThuLy" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../UI/js/Common.js"></script><asp:HiddenField ID="hddID" runat="server" Value="0" />
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddGiaiDoanVuAn" Value="" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddIsShowCommand" Value="True" runat="server" />
    <style>
        .align_right {
            text-align: right;
        }

        #canhbao_form {
            background-color: #fff478;
            border: 1px solid red;
            border-radius: 4px;
            box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
            width:90%;
            min-height: 30px;
            line-height: 30px;
            opacity: 0.9;
            z-index: 10;
            float:left;
            font-weight: bold;
            padding: 5px;
        }
        #canhbao_form ul li{float:left; width:100%; margin:0; padding:0;margin-left:3%;}
        .canhbao_thuly{ color: red;margin-left:3px;}
    </style>
    <div class="box">
        <div class="box_nd">
            <div class="truong">
                <div class="boxchung">
                    <h4 class="tleboxchung">Thông tin thụ lý</h4>
                    <div class="boder" style="padding: 10px;">
                        <table class="table1">
                            <tr>
                                <td style="width: 115px;">Trường hợp thụ lý<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:DropDownList ID="ddTruongHopTL" CssClass="chosen-select"
                                        runat="server" Width="377px" AutoPostBack="true" OnSelectedIndexChanged="ddTruongHopTL_SelectedIndexChanged">
                                    </asp:DropDownList>
                               </td>
                               <td colspan="2"><asp:CheckBox ID="cbUTTP" Checked="false" runat="server" Text="Ủy thác tư pháp đi" /></td>
                            </tr>
                            <tr>                                                            
                                <td style="width: 80px;">Ngày thụ lý<span class="batbuoc">(*)</span></td>
                                <td  style="width: 125px;">
                                    <asp:TextBox ID="txtNgayThuLy" runat="server" CssClass="user" Width="100px" MaxLength="10"
                                        onkeypress="return isNumber(event)"
                                        AutoPostBack="True" OnTextChanged="txtNgayThuLy_TextChanged"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtNgayThuLy"
                                        Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtNgayThuLy"
                                        Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td> 
                                <td style="width:80px;">Số thụ lý<span class="batbuoc">(*)</span></td>
                                <td >
                                    <asp:TextBox ID="txtSoThuLy" CssClass="user"                                       
                                        runat="server" Width="150px" MaxLength="250"></asp:TextBox></td>   
                            </tr>
                            <asp:Panel ID="pnTTCaoTrang" runat="server" Visible="false">
                                <tr>
                                   
                                    <td>Ngày bản cáo trạng<span class="batbuoc">(*)</span></td>
                                    <td>
                                        <asp:TextBox ID="txtNgayBanCaoTrang" runat="server"
                                             onkeypress="return isNumber(event)"
                                            CssClass="user" Width="100px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtNgayBanCaoTrang" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtNgayBanCaoTrang" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </td>
                                    <td>Số bản cáo trạng<span class="batbuoc">(*)</span></td>
                                    <td>
                                        <asp:TextBox ID="txtSoBanCaoTrang" runat="server"
                                            CssClass="user align_right"
                                            Width="162px" MaxLength="10" onkeypress="return isNumber(event)"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <tr style="display: none;">
                                <td>Từ ngày</td>
                                <td>
                                    <asp:TextBox ID="txtTuNgay" runat="server" CssClass="user" Width="100px"
                                         onkeypress="return isNumber(event)"
                                        MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtTuNgay_CalendarExtender" runat="server"
                                        TargetControlID="txtTuNgay" 
                                        Format="dd/MM/yyyy" Enabled="false" />
                                    <cc1:MaskedEditExtender ID="txtTuNgay_MaskedEditExtender" runat="server" TargetControlID="txtTuNgay"
                                        Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                                <td>Đến ngày</td>
                                <td>
                                    <asp:TextBox ID="txtDenNgay" runat="server" CssClass="user"
                                         onkeypress="return isNumber(event)"
                                        Width="100px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server"
                                        TargetControlID="txtDenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                                        TargetControlID="txtDenNgay" Mask="99/99/9999" MaskType="Date"
                                        CultureName="vi-VN"
                                        ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                             <tr>
                                <td colspan="4">
                                    <asp:Literal ID="lttCanhBao" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2">
                                    <div style="margin: 5px; text-align: center; width: 95%">
                                        <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput"
                                            Text="Lưu" OnClick="cmdUpdate_Click" OnClientClick="return validate();" />
                                        <asp:Button ID="cmdThemmoi" runat="server" CssClass="buttoninput"
                                            Text="Làm mới" OnClick="cmdThemmoi_Click" />
                                    </div>

                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="width: 95%; color: red;">
                                        <asp:Literal ID="lstMsgB" runat="server"></asp:Literal>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="danhsach">
                    
                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand" OnItemDataBound="rpt_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table2" width="100%" border="1">
                                <tr class="header">
                                    <td width="42">
                                        <div align="center"><strong>TT</strong></div>
                                    </td>
                                     <td width="150px">
                                        <div align="center"><strong>Số thụ lý</strong></div>
                                    </td>
                                    <td>
                                        <div align="center"><strong>Trường hợp thụ lý</strong></div>
                                    </td>
                                    <td width="10%">
                                        <div align="center"><strong>Ngày thụ lý</strong></div>
                                    </td>
                                    <td width="70px">
                                        <div align="center"><strong>Thao tác</strong></div>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Container.ItemIndex + 1 %></td>
                                <td><%#Eval("SoThuLy") %></td>
                                <td><%#Eval("TruongHopThuLy") %></td>
                                <td><%# string.Format("{0:dd/MM/yyyy}",Eval("NgayThuLy")) %></td>
                                <td>
                                    <asp:LinkButton ID="lblSua" runat="server" Text="Sửa" CausesValidation="false" CommandName="Sua" ForeColor="#0e7eee"
                                        CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lbtXoa" runat="server" 
                                        CausesValidation="false" Text="Xóa" ForeColor="#0e7eee"
                                        CommandName="Xoa" CommandArgument='<%#Eval("ID") %>' ToolTip="Xóa"
                                        OnClientClick="return confirm('Bạn thực sự muốn xóa bản ghi này? ');"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate></table></FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    <script>
        function pageLoad(sender, args) {
            $(function () {
                var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
                for (var selector in config) { $(selector).chosen(config[selector]); }
            });
        }
        function validate() {
            <%--var NgayHoSo = '<%= NgayHoSo%>';--%>

            var ddTruongHopTL = document.getElementById('<%=ddTruongHopTL.ClientID%>');
            value_change = ddTruongHopTL.options[ddTruongHopTL.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn trường hợp thụ lý. Hãy kiểm tra lại!');
                ddTruongHopTL.focus();
                return false;
            }        
            //------------------------------------
            var txtSoThuLy = document.getElementById('<%=txtSoThuLy.ClientID%>');
            if(!Common_CheckTextBox(txtSoThuLy, "Số thụ lý")){
                return false;
            }
            //------------------------------------
            if (value_change == "233")
            {
                var txtSoBanCaoTrang = document.getElementById('<%=txtSoBanCaoTrang.ClientID%>');
                if (!Common_CheckEmpty(txtSoBanCaoTrang.value)) {
                    alert('Bạn chưa nhập số bản cáo trạng. Hãy kiểm tra lại!');
                    txtSoBanCaoTrang.focus();
                    return false;
                }
                else {
                    var lengthSoBanCaoTrang = txtSoBanCaoTrang.value.length;
                    if (lengthSoBanCaoTrang > 250) {
                        alert('Số bản cáo trạng không được quá 250 ký tự. Hãy kiểm tra lại!');
                        txtSoBanCaoTrang.focus();
                        return false;
                    }
                }

                var txtNgayBanCaoTrang = document.getElementById('<%=txtNgayBanCaoTrang.ClientID%>');
                if (!CheckDateTimeControl(txtNgayBanCaoTrang, 'Ngày bản cáo trạng'))
                    return false;
            }
           
           
            return true;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    </script>
     <%--<script>
         function hide_zone_message() {
             var today = new Date();
             var zone = document.getElementById('canhbao_form');
             if (zone.style.display != "") {
                 zone.style.display = "none";
                 zone.innerText = "";
             }
             var t = setTimeout(hide_zone_message, 5000);
         }
         hide_zone_message();
        </script>--%>
</asp:Content>


