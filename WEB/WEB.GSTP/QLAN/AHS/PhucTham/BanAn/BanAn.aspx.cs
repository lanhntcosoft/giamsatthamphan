﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BL.GSTP.Danhmuc;

namespace WEB.GSTP.QLAN.AHS.PhucTham.BanAn
{
    public partial class BanAn : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        Decimal ToaAnID = 0, CurrUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            if (CurrUserID > 0)
            {
                ToaAnID = Session[ENUM_SESSION.SESSION_DONVIID] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
                if (!IsPostBack)
                {
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                    hddVuAnID.Value = Session[ENUM_LOAIAN.AN_HINHSU] + "" == "" ? "" : Session[ENUM_LOAIAN.AN_HINHSU] + "";
                    LoadCombobox();
                    LoadNguoiKyInfo();
                    LoaThongTinBanAn();
                    LoadDsBiCao();
                    CheckQuyen();
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        void CheckQuyen()
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdateBanAnST, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdSave, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdSave2, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdSaveThongKe, oPer.CAPNHAT);
            decimal VUANID = Convert.ToDecimal(hddVuAnID.Value);
            bool IsUpdateThuLyPT = true;
            AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VUANID).FirstOrDefault();
            if (oT != null)
            {
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.HOSO || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM)
                {
                    IsUpdateThuLyPT = false;
                }
            }
            List<AHS_PHUCTHAM_THULY> lstCount = dt.AHS_PHUCTHAM_THULY.Where(x => x.VUANID == VUANID).ToList();
            if (lstCount.Count == 0)
            {
                IsUpdateThuLyPT = false;
            }
            if (!IsUpdateThuLyPT)
            {
                lttMsgBanAn.Text = "Chưa cập nhật thông tin thụ lý phúc thẩm!";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdSave, false);
                Cls_Comon.SetButton(cmdSave2, false);
                Cls_Comon.SetButton(cmdSaveThongKe, false);
                return;
            }

            if (!Check_Phancongthamphan())
            {
                lttMsgBanAn.Text = "Vụ việc chưa được phân công thẩm phán. Đề nghị cập nhật thông tin 'Phân công thẩm phán giải quyết' !";
                Cls_Comon.SetButton(cmdUpdateBanAnST, false);
                Cls_Comon.SetButton(cmdSave, false);
                Cls_Comon.SetButton(cmdSave2, false);
                Cls_Comon.SetButton(cmdSaveThongKe, false);
            }
        }
        Boolean Check_Phancongthamphan()
        {
            try
            {
                decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
                List<AHS_PHUCTHAM_HDXX> lst = dt.AHS_PHUCTHAM_HDXX.Where(x => x.VUANID == VuAnID
                                                                           && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).ToList<AHS_PHUCTHAM_HDXX>();

                if (lst != null && lst.Count > 0)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
        private void LoadNguoiKyInfo()
        {
            DM_CANBO_BL cb_BL = new DM_CANBO_BL();
            decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            AHS_PHUCTHAM_HDXX oND = dt.AHS_PHUCTHAM_HDXX.Where(x => x.VUANID == VuAnID
                                                                 && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).FirstOrDefault<AHS_PHUCTHAM_HDXX>();
            if (oND != null)
            {
                decimal CanBoID = Convert.ToDecimal(oND.CANBOID.ToString());
                DataTable dtCanBo = cb_BL.DM_CANBO_GETINFOBYID(CanBoID);
                if (dtCanBo != null && dtCanBo.Rows.Count > 0)
                {
                    txtNguoiKy.Text = dtCanBo.Rows[0]["HOTEN"].ToString();
                    hddNguoiKyID.Value = dtCanBo.Rows[0]["ID"].ToString();
                }
            }
            else
            {
                txtNguoiKy.Text = "";
            }
        }
        //private void SetNewSoBanAn()
        //{
        //    decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
        //    AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
        //    decimal thutu = objBL.GetNewTT(ToaAnID, VuAnID);
        //    txtSoBanAn.Text = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();
        //}
        private void LoaThongTinBanAn()
        {
            hddToaAnID.Value = ToaAnID.ToString();
            DM_TOAAN objTA = dt.DM_TOAAN.Where(x => x.ID == ToaAnID && x.HIEULUC == 1).SingleOrDefault<DM_TOAAN>();
            if (objTA != null)
            {
                txtToaAn.Text = objTA.TEN;
                txtDiaDiem.Text = objTA.DIACHI;
                txtNgayBanAn.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            }
            //-------------------------------------------
            decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            AHS_PHUCTHAM_BANAN obj = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault();
            if (obj != null)
            {
                hddID.Value = obj.ID.ToString();
                LoadDsBiCao();
                //----------------------------------------------------------
                txtSBC_GIUNGUYEN_AN_ST.Text = obj.SBC_GIUNGUYEN_AN_ST + "";
                txtSBC_SUA_AN_ST.Text = obj.SBC_SUA_AN_ST + "";
                txtSBC_CHAPNHAN_TOANBO.Text = obj.SBC_CHAPNHAN_TOANBO + "";
                txtSBC_CHAPNHAN_MOTPHAN.Text = obj.SBC_CHAPNHAN_MOTPHAN + "";

                ddlKetQuaPhucTham.SelectedValue = obj.KETQUAPHUCTHAMID.ToString();
                LoadDropLyDoBanAn();
                ddlLyDoBanAn.SelectedValue = obj.LYDOBANANID.ToString();

                rdAnLe.SelectedValue = obj.ISANLE + "";
                rdAnRutGon.SelectedValue = obj.ISANRUTGON + "";
                rdBaoLucGD.SelectedValue = obj.ISBAOLUCGIADINH + "";
                rdXetXuLuuDong.SelectedValue = obj.ISXXLUUDONG + "";
                //-----------------------------
                txtNgayBanAn.Text = obj.NGAYBANAN + "" == "" ? DateTime.Now.ToString("dd/MM/yyyy", cul) : ((DateTime)obj.NGAYBANAN).ToString("dd/MM/yyyy", cul);
                //((DateTime)obj.NGAYBANAN).ToString("dd/MM/yyyy", cul);
                txtSoBanAn.Text = obj.SOBANAN + "";

                txtNgayMoPhienToa.Text = obj.NGAYMOPHIENTOA + "" == "" ? DateTime.Now.ToString("dd/MM/yyyy", cul) : ((DateTime)obj.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                txtDiaDiem.Text = obj.DIADIEM + "";
                //----------------------------
                rdSuaHinhPhatBS.SelectedValue = obj.TK_SUAHPBOSUNG + "";
                rdSuaBoiThuongTH.SelectedValue = obj.TK_BOITHUONGTH + "";
                rdSuaKhac.SelectedValue = obj.TK_SUAKHAC + "";
                rdSuaQDAnSoTham.SelectedValue = obj.TK_SUAQD_ANST + "";
                rdKhoiToTaiToa.SelectedValue = obj.TK_KHOITOTAITOA + "";
                rdKoRutKhangCao.SelectedValue = obj.TK_NONERUTKHANGCAO + "";
                rdDuyetKhangNghiVKS.SelectedValue = obj.TK_DUYETKHANGNGHI_VKS + "";
                if (obj.TK_DUYETKHANGNGHI_VKS > 0)
                {
                    txtDuyetKN_VKS_BiCao.Enabled = true;
                    txtDuyetKN_VKS_BiCao.Text = obj.TK_DUYETKHANGNGHI_SOBICAO + "";
                }
                //--------------------------------
                if ((obj.TENFILE + "") != "")
                {
                    lbtDownload.Visible = true;
                    lbtXoaFile.Visible = true;
                }
                else
                {
                    lbtDownload.Visible = false;
                    lbtXoaFile.Visible = false;
                }
            }
            else
            {
                pnAnPhi.Enabled = false;
                txtNgayMoPhienToa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                // SetNewSoBanAn();
            }
        }

        //-----------------lUU THONG TIN AN SOTHAM--------------------------------
        protected void cmdUpdateBanAnST_Click(object sender, EventArgs e)
        {
            string so =  txtSoBanAn.Text;
            DateTime ngayBA = DateTime.Parse(this.txtNgayBanAn.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
            Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "AHS_PT", so, ngayBA);
            if (CheckID > 0)
            {
                Decimal CurrBanAnId = (string.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
                String strMsg = "";
                String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "AHS_PT", ngayBA).ToString();
                if (CheckID != CurrBanAnId)
                {
                    strMsg = "Số bản án " + txtSoBanAn.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                    txtSoBanAn.Text = STTNew;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    txtSoBanAn.Focus();
                    return;
                }
            }
            UpdateBanAn();
            hddPageIndex.Value = "1";
            LoadDsBiCao();
            lttMsgBanAn.Text = "Cập nhật thông tin bản án thành công!.";
            lttMsgAnPhi.Text = lttMsgThongKe.Text = "";
        }
        void UpdateBanAn()
        {
            Boolean IsUpdate = false;
            DateTime date_temp;
            decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            AHS_PHUCTHAM_BANAN obj = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault<AHS_PHUCTHAM_BANAN>();
            if (obj != null)
                IsUpdate = true;
            else
            {
                obj = new AHS_PHUCTHAM_BANAN();
            }
            obj.VUANID = VuAnID;
            obj.KETQUAPHUCTHAMID = Convert.ToDecimal(ddlKetQuaPhucTham.SelectedValue);
            obj.LYDOBANANID = Convert.ToDecimal(ddlLyDoBanAn.SelectedValue);
            if (hddFilePath.Value != "")
            {
                try
                {
                    string strFilePath = "";
                    if (chkKySo.Checked)
                    {
                        string[] arr = hddFilePath.Value.Split('/');
                        strFilePath = arr[arr.Length - 1];
                        strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                    }
                    else
                        strFilePath = hddFilePath.Value.Replace("/", "\\");
                    byte[] buff = null;
                    using (FileStream fs = File.OpenRead(strFilePath))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        FileInfo oF = new FileInfo(strFilePath);
                        long numBytes = oF.Length;
                        buff = br.ReadBytes((int)numBytes);
                        obj.NOIDUNGFILE = buff;
                        obj.TENFILE = oF.Name;
                        obj.KIEUFILE = oF.Extension;
                    }
                    File.Delete(strFilePath);
                    lbtDownload.Visible = true;
                    lbtXoaFile.Visible = true;
                    MSG_file.Text = string.Empty;
                }
                catch { }/* lbthongbao.Text = ex.Message; }*/
            }

            //-----------------------------------------   
            date_temp = (String.IsNullOrEmpty(txtNgayBanAn.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayBanAn.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYBANAN = date_temp;

            //-----------------------------------------           
            date_temp = (String.IsNullOrEmpty(txtNgayMoPhienToa.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayMoPhienToa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYMOPHIENTOA = date_temp;
            obj.DIADIEM = txtDiaDiem.Text.Trim();

            //-----------------------------------------
            obj.TOAANID = ToaAnID;

            obj.NGUOIKY = (!String.IsNullOrEmpty(hddNguoiKyID.Value)) ? 0 : Convert.ToDecimal(hddNguoiKyID.Value);
            obj.SOBANAN = txtSoBanAn.Text.Trim();
            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                //AHS_PHUCTHAM_BANAN_BL objBL = new AHS_PHUCTHAM_BANAN_BL();
                //decimal thutu = objBL.GetNewTT(ToaAnID, VuAnID);
                //obj.SOBANAN = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();

                dt.AHS_PHUCTHAM_BANAN.Add(obj);
                dt.SaveChanges();
                InsertToiDanhTuCaoTrangSangBanAnPT(obj.ID);
                hddID.Value = obj.ID + "";
            }
            lttMsgBanAn.Text = "Lưu dữ liệu thành công!";
        }
        void InsertToiDanhTuCaoTrangSangBanAnPT(decimal BanAnID)
        {
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            Decimal BiCaoID = 0;
            Boolean isupdate = false;
            AHS_PHUCTHAM_BANAN_BICAO objBC = new AHS_PHUCTHAM_BANAN_BICAO();
            List<AHS_PHUCTHAM_BICANBICAO> lst = dt.AHS_PHUCTHAM_BICANBICAO.Where(x => x.VUANID == VuAnID).ToList<AHS_PHUCTHAM_BICANBICAO>();
            if (lst != null && lst.Count > 0)
            {
                foreach (AHS_PHUCTHAM_BICANBICAO item in lst)
                {
                    BiCaoID = (Decimal)item.BICANID;
                    #region Update_BiCan_phuc tham
                    isupdate = false;
                    objBC = dt.AHS_PHUCTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID
                                                                && x.BICAOID == BiCaoID).SingleOrDefault<AHS_PHUCTHAM_BANAN_BICAO>();
                    if (objBC != null)
                        isupdate = true;
                    else
                        objBC = new AHS_PHUCTHAM_BANAN_BICAO();

                    objBC.BANANID = BanAnID;
                    objBC.BICAOID = BiCaoID;

                    if (!isupdate)
                    {
                        dt.AHS_PHUCTHAM_BANAN_BICAO.Add(objBC);
                        dt.SaveChanges();
                    }
                    #endregion               
                    ThemHinhPhat(BiCaoID, BanAnID);
                }
            }
        }
        void ThemHinhPhat(Decimal BiCaoID,decimal BanAnID)
        {
            decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            Decimal BanAnST_ID = 0;
            //------------------------------
            AHS_SOTHAM_BANAN st = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault<AHS_SOTHAM_BANAN>();
            if (st != null) BanAnST_ID = st.ID;
            //------------------------------
            Boolean IsUpdate = false;
            AHS_PHUCTHAM_BANAN_DIEU_CT Ba_PT_CT = new AHS_PHUCTHAM_BANAN_DIEU_CT();
            List<AHS_SOTHAM_BANAN_DIEU_CHITIET> lst = dt.AHS_SOTHAM_BANAN_DIEU_CHITIET.Where(x => x.BANANID == BanAnST_ID 
                                                                                            && x.BICANID == BiCaoID
                                                                                            ).ToList<AHS_SOTHAM_BANAN_DIEU_CHITIET>();

            //decimal kckn = Ahs_kiemtra_khangcaokhangnghi(VuAnID, BiCaoID);

            //if(kckn > 0)
            foreach (AHS_SOTHAM_BANAN_DIEU_CHITIET Ba_ST_CT in lst)
            {
                IsUpdate = false;
                Ba_PT_CT = dt.AHS_PHUCTHAM_BANAN_DIEU_CT.Where(x => 
                                                                x.BANANID == BanAnID
                                                              && x.BICANID == Ba_ST_CT.BICANID
                                                              && x.DIEULUATID == Ba_ST_CT.DIEULUATID
                                                              && x.TOIDANHID == Ba_ST_CT.TOIDANHID
                                                              && x.HINHPHATID == Ba_ST_CT.HINHPHATID
                                                         ).SingleOrDefault<AHS_PHUCTHAM_BANAN_DIEU_CT>();
                // Kiểm tra đã có điều đó ở PT_CT chưa
                if (Ba_PT_CT == null)
                    Ba_PT_CT = new AHS_PHUCTHAM_BANAN_DIEU_CT();
                else
                    IsUpdate = true;

                Ba_PT_CT.BANANID = BanAnID;
                Ba_PT_CT.BICANID = BiCaoID;
                Ba_PT_CT.DIEULUATID = Ba_ST_CT.DIEULUATID;
                Ba_PT_CT.TOIDANHID = Ba_ST_CT.TOIDANHID;
                
                //Nếu KQXXPT là "Giữ nguyên bản án, quyết định sơ thẩm" thì copy các hình phạt từ sơ thẩm lên phúc thẩm
                AHS_PHUCTHAM_BANAN ocheck_KQXXPTbj = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault<AHS_PHUCTHAM_BANAN>();
                if (ocheck_KQXXPTbj.KETQUAPHUCTHAMID == 1)
                {
                    Ba_PT_CT.HINHPHATID = Ba_ST_CT.HINHPHATID;
                    Ba_PT_CT.LOAIHINHPHAT = Ba_ST_CT.LOAIHINHPHAT;
                    Ba_PT_CT.ISANTREO = Ba_ST_CT.ISANTREO;
                    Ba_PT_CT.ISCHANGE = Ba_ST_CT.ISCHANGE;
                    Ba_PT_CT.TF_VALUE = Ba_ST_CT.TF_VALUE;
                    Ba_PT_CT.SH_VALUE = Ba_ST_CT.SH_VALUE;
                    Ba_PT_CT.TG_NAM = Ba_ST_CT.TG_NAM;
                    Ba_PT_CT.TG_THANG = Ba_ST_CT.TG_THANG;
                    Ba_PT_CT.TG_NGAY = Ba_ST_CT.TG_NGAY;
                    Ba_PT_CT.K_VALUE1 = Ba_ST_CT.K_VALUE1;
                    Ba_PT_CT.K_VALUE2 = Ba_ST_CT.K_VALUE2;
                    Ba_PT_CT.TGTT_NAM = Ba_ST_CT.TGTT_NAM;
                    Ba_PT_CT.TGTT_THANG = Ba_ST_CT.TGTT_THANG;
                    Ba_PT_CT.TGTT_NGAY = Ba_ST_CT.TGTT_NGAY;
                }
                else
                    {
                        Ba_PT_CT.HINHPHATID = 0;
                        Ba_PT_CT.LOAIHINHPHAT = 0;
                        Ba_PT_CT.ISCHANGE = 0;
                    }
                // Kiểm tra tên tội danh là null hay ko
                if (String.IsNullOrEmpty(Ba_ST_CT.TENTOIDANH))
                {
                    if (Ba_ST_CT.TOIDANHID > 0)
                        Ba_PT_CT.TENTOIDANH = "";
                }
                else
                    Ba_PT_CT.TENTOIDANH = Ba_ST_CT.TENTOIDANH;
                Ba_PT_CT.ISMAIN = (string.IsNullOrEmpty(Ba_ST_CT.ISMAIN + "")) ? 0 : Ba_ST_CT.ISMAIN;

                if (!IsUpdate)
                    dt.AHS_PHUCTHAM_BANAN_DIEU_CT.Add(Ba_PT_CT);
                dt.SaveChanges();
            }
        }

        #region Load DS bi cao
        public void LoadDsBiCao()
        {
            int vu_an_id = Convert.ToInt32(Session[ENUM_LOAIAN.AN_HINHSU] + "");

            AHS_PHUCTHAM_BANAN_BL objBL = new AHS_PHUCTHAM_BANAN_BL();
            DataTable tbl = objBL.GetDsBiCaoByVuAn(vu_an_id);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                rpt.DataSource = tbl;
                rpt.DataBind();

                if (Convert.ToInt16(tbl.Rows[0]["IsShow"] + "") > 0)
                    cmdSave.Enabled = cmdSave2.Enabled = pnAnPhi.Enabled = true;
                else
                {
                    lttMsgAnPhi.Text = "Chưa có bản án xét xử phúc thẩm!";
                    cmdSave.Enabled = cmdSave2.Enabled = pnAnPhi.Enabled = false;
                }
            }
            else
            {
                //  pnAnPhi.Enabled = false;
                lttMsgAnPhi.Text = "Chưa có bị cáo được xét xử phúc thẩm. Đề nghị kiểm tra lại!";
                cmdSave.Enabled = cmdSave2.Enabled = pnAnPhi.Enabled = false;
            }
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dv = (DataRowView)e.Item.DataItem;

                decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
                //HiddenField hddBiCao = (HiddenField)item.FindControl("hddBiCao");
                decimal BiCaoId = Convert.ToDecimal(dv["BiCanID"] + "");
                //decimal kckn = 0;
                //try
                //{
                //    kckn = Ahs_kiemtra_khangcaokhangnghi(VuAnID, BiCaoId);
                //}
                //catch (Exception ex) { }

                TextBox txtNgaynhanbanan = (TextBox)e.Item.FindControl("txtNgaynhanbanan");
                DateTime ngaynhan = String.IsNullOrEmpty(dv["NgayNhanBanAn"] + "") ? DateTime.MinValue : Convert.ToDateTime(dv["NgayNhanBanAn"] + "");
                if (ngaynhan == DateTime.MinValue)
                    txtNgaynhanbanan.Text = "";
                else
                    txtNgaynhanbanan.Text = ngaynhan.ToString("dd/MM/yyyy", cul);

                int IsShow = Convert.ToInt16(dv["IsShow"] + "");
                //if (IsShow == 0 || kckn == 0)
                //{
                //    Panel lnLinkToiDanh = (Panel)e.Item.FindControl("lnLinkToiDanh");
                //    lnLinkToiDanh.Visible = false;
                //}

                try
                { //-- Hình phạt tổng hợp

                    string THHinhPhat = Tonghophinhphat_ST(VuAnID, BiCaoId);
                    Label lblTHtoidanh = (Label)e.Item.FindControl("lblTHtoidanh");
                    lblTHtoidanh.Text = THHinhPhat;
                }
                catch (Exception ex) { }
                
                try
                { //-- Hình phạt tổng hợp
                    string THHinhPhat = Tonghophinhphat_PT(VuAnID, BiCaoId);
                    Label lblTHtoidanhPT = (Label)e.Item.FindControl("lblTHtoidanhPT");
                    lblTHtoidanhPT.Text = THHinhPhat;
                }
                catch (Exception ex) { }
            }
        }
        #endregion
        //-------------------------------------------------
        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AsyncFileUpLoad.HasFile)
                {
                    string strFileName = AsyncFileUpLoad.FileName;
                    string path = Server.MapPath("~/TempUpload/") + strFileName;
                    AsyncFileUpLoad.SaveAs(path);
                    path = path.Replace("\\", "/");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }
        protected void lbtDownload_Click(object sender, EventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(hddID.Value);
                AHS_PHUCTHAM_BANAN oND = dt.AHS_PHUCTHAM_BANAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND.TENFILE != "")
                {
                    var cacheKey = Guid.NewGuid().ToString("N");
                    Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNGFILE, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }
        protected void lbtXoaFile_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(hddID.Value);
                AHS_PHUCTHAM_BANAN oND = dt.AHS_PHUCTHAM_BANAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND.TENFILE != "")
                {
                    oND.NOIDUNGFILE = null;
                    oND.TENFILE = null;
                    oND.KIEUFILE = null;
                    dt.SaveChanges();
                    MSG_file.Text = "File bản án đã được xóa thành công";
                    lbtDownload.Visible = false;
                    lbtXoaFile.Visible = false;
                }
            }
            catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }

        //------------LUU THONG TIN CHI TIEU HO TRO THONG KE---------------------    
        protected void cmdSaveThongKe_Click(object sender, EventArgs e)
        {
            Update_ChiTieuTK();
            lttMsgAnPhi.Text = lttMsgBanAn.Text = "";
        }
        void Update_ChiTieuTK()
        {
            Boolean IsUpdate = false;
            Decimal VuAnId = Convert.ToDecimal(hddVuAnID.Value);
            AHS_PHUCTHAM_BANAN obj = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == VuAnId).SingleOrDefault<AHS_PHUCTHAM_BANAN>();
            if (obj != null)
                IsUpdate = true;
            else
                obj = new AHS_PHUCTHAM_BANAN();

            obj.VUANID = VuAnId;
            obj.ISANLE = Convert.ToDecimal(rdAnLe.SelectedValue);
            obj.ISANRUTGON = Convert.ToDecimal(rdAnRutGon.SelectedValue);
            obj.ISBAOLUCGIADINH = Convert.ToDecimal(rdBaoLucGD.SelectedValue);
            obj.ISXXLUUDONG = Convert.ToDecimal(rdXetXuLuuDong.SelectedValue);

            obj.SBC_GIUNGUYEN_AN_ST = (string.IsNullOrEmpty(txtSBC_GIUNGUYEN_AN_ST.Text.Trim())) ? 0 : Convert.ToDecimal(txtSBC_GIUNGUYEN_AN_ST.Text.Trim());
            obj.SBC_SUA_AN_ST = (string.IsNullOrEmpty(txtSBC_SUA_AN_ST.Text.Trim())) ? 0 : Convert.ToDecimal(txtSBC_SUA_AN_ST.Text.Trim());
            obj.SBC_CHAPNHAN_TOANBO = (string.IsNullOrEmpty(txtSBC_CHAPNHAN_TOANBO.Text.Trim())) ? 0 : Convert.ToDecimal(txtSBC_CHAPNHAN_TOANBO.Text.Trim());
            obj.SBC_CHAPNHAN_MOTPHAN = (string.IsNullOrEmpty(txtSBC_CHAPNHAN_MOTPHAN.Text.Trim())) ? 0 : Convert.ToDecimal(txtSBC_CHAPNHAN_MOTPHAN.Text.Trim());

            obj.TK_SUAHPBOSUNG = Convert.ToDecimal(rdSuaHinhPhatBS.SelectedValue);
            obj.TK_BOITHUONGTH = Convert.ToDecimal(rdSuaBoiThuongTH.SelectedValue);
            obj.TK_SUAKHAC = Convert.ToDecimal(rdSuaKhac.SelectedValue);
            obj.TK_SUAQD_ANST = Convert.ToDecimal(rdSuaQDAnSoTham.SelectedValue);
            obj.TK_KHOITOTAITOA = Convert.ToDecimal(rdKhoiToTaiToa.SelectedValue);
            obj.TK_NONERUTKHANGCAO = Convert.ToDecimal(rdKoRutKhangCao.SelectedValue);
            obj.TK_DUYETKHANGNGHI_VKS = Convert.ToDecimal(rdDuyetKhangNghiVKS.SelectedValue);
            if (obj.TK_DUYETKHANGNGHI_VKS > 0)
                obj.TK_DUYETKHANGNGHI_SOBICAO = Convert.ToDecimal(txtDuyetKN_VKS_BiCao.Text);

            Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            obj.TOAANID = ToaAnID;

            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";

                AHS_PHUCTHAM_BANAN_BL objBL = new AHS_PHUCTHAM_BANAN_BL();
                decimal thutu = objBL.GetNewTT(ToaAnID, VuAnId);
                obj.SOBANAN = ENUM_LOAIVUVIEC.AN_HINHSU + Session[ENUM_SESSION.SESSION_MADONVI] + thutu.ToString();

                dt.AHS_PHUCTHAM_BANAN.Add(obj);
                dt.SaveChanges();
                hddID.Value = obj.ID + "";
            }

            lttMsgBanAn.Text = lttMsgAnPhi.Text = "";
            lttMsgThongKe.Text = "Lưu dữ liệu thành công!";
        }

        //----------Update an phi--------------------------------------
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            AHS_PHUCTHAM_BANAN_BICAO obj = null;
            Boolean IsNew = false;
            Decimal BanAnID = Convert.ToDecimal(hddID.Value);

            foreach (RepeaterItem item in rpt.Items)
            {
                IsNew = false;
                HiddenField hddBiCao = (HiddenField)item.FindControl("hddBiCao");
                CheckBox chkThamGiaPhienToa = (CheckBox)item.FindControl("chkThamGiaPhienToa");
                TextBox txtAnPhi = (TextBox)item.FindControl("txtAnPhi");
                //CheckBox chkDinhChi = (CheckBox)item.FindControl("chkDinhChi");
                TextBox txtNgaynhanbanan = (TextBox)item.FindControl("txtNgaynhanbanan");
                decimal BiCaoId = Convert.ToDecimal(hddBiCao.Value);

                obj = dt.AHS_PHUCTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID && x.BICAOID == BiCaoId).SingleOrDefault<AHS_PHUCTHAM_BANAN_BICAO>();
                if (obj == null)
                {
                    IsNew = true;
                    obj = new AHS_PHUCTHAM_BANAN_BICAO();
                }

                obj.BANANID = BanAnID;
                obj.ISTHAMGIAPHIENTOA = (chkThamGiaPhienToa.Checked) ? 1 : 0;
                //obj.ISDINHCHI = (chkDinhChi.Checked) ? 1 : 0;

                DateTime date_temp = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.NGAYNHANBANAN = date_temp;
                obj.BICAOID = Convert.ToDecimal(hddBiCao.Value);
                obj.ANPHI = (string.IsNullOrEmpty(txtAnPhi.Text + "")) ? 0 : Convert.ToDecimal(txtAnPhi.Text.Replace(".", ""));
                if (IsNew)
                    dt.AHS_PHUCTHAM_BANAN_BICAO.Add(obj);
                dt.SaveChanges();

            }
            lttMsgThongKe.Text = "Lưu dữ liệu thành công!";
            lttMsgBanAn.Text = lttMsgThongKe.Text = "";
            lttMsgAnPhi.Text = "Lưu dữ liệu thành công!";
        }
        //-------------------------------
        void LoadCombobox()
        {
            LoadDropKetQuaPhucTham();
            LoadDropLyDoBanAn();
        }
        private void LoadDropKetQuaPhucTham()
        {
            ddlKetQuaPhucTham.Items.Clear();
            ddlKetQuaPhucTham.DataSource = dt.DM_KETQUA_PHUCTHAM.Where(x => x.ISAHS == 1).OrderBy(y => y.THUTU).ToList();
            ddlKetQuaPhucTham.DataTextField = "TEN";
            ddlKetQuaPhucTham.DataValueField = "ID";
            ddlKetQuaPhucTham.DataBind();
            ddlKetQuaPhucTham.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        private void LoadDropLyDoBanAn()
        {
            ddlLyDoBanAn.Items.Clear();
            decimal KetQuaID = Convert.ToDecimal(ddlKetQuaPhucTham.SelectedValue);
            DM_KETQUA_PHUCTHAM_LYDO_BL kqptLyDoBL = new DM_KETQUA_PHUCTHAM_LYDO_BL();
            DataTable dtTable = kqptLyDoBL.DM_KETQUA_PT_LYDO_GETLIST(KetQuaID);
            if (dtTable != null && dtTable.Rows.Count > 0)
            {
                ddlLyDoBanAn.DataSource = dtTable;
                ddlLyDoBanAn.DataTextField = "TEN";
                ddlLyDoBanAn.DataValueField = "ID";
                ddlLyDoBanAn.DataBind();
            }
            ddlLyDoBanAn.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        protected void ddlKetQuaPhucTham_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { LoadDropLyDoBanAn(); } catch (Exception ex) { lttMsgBanAn.Text = ex.Message; }
        }

        protected void cmdHuyBanAn_Click(object sender, EventArgs e)
        {
            decimal BanAnID = Convert.ToDecimal(hddID.Value);
            // Xóa thông tin bản án
            AHS_PHUCTHAM_BANAN banan = dt.AHS_PHUCTHAM_BANAN.Where(x => x.ID == BanAnID).FirstOrDefault();
            if (banan != null)
            {
                dt.AHS_PHUCTHAM_BANAN.Remove(banan);
            }
            // Xóa thông tin file đính kèm
            //List<AHS_PHUCTHAM_BANAN_FILE> files = dt.AHS_PHUCTHAM_BANAN_FILE.Where(x => x.BANANID == BanAnID).ToList();
            //if (files.Count > 0)
            //{
            //    dt.AHS_PHUCTHAM_BANAN_FILE.RemoveRange(files);
            //}
            // Xóa thông tin bị can tham gia phiên tòa
            List<AHS_PHUCTHAM_BANAN_BICAO> tBABCs = dt.AHS_PHUCTHAM_BANAN_BICAO.Where(x => x.BANANID == BanAnID).ToList();
            if (tBABCs.Count > 0)
            {
                dt.AHS_PHUCTHAM_BANAN_BICAO.RemoveRange(tBABCs);
            }
            // Xóa thông tin hình phạt
            List<AHS_PHUCTHAM_BANAN_DIEU_CT> tGTTs = dt.AHS_PHUCTHAM_BANAN_DIEU_CT.Where(x => x.BANANID == BanAnID).ToList();
            if (tGTTs.Count > 0)
            {
                dt.AHS_PHUCTHAM_BANAN_DIEU_CT.RemoveRange(tGTTs);
            }
            dt.SaveChanges();
            ResetControl();
            lttMsgBanAn.Text = "Xóa bản án thành công!";
        }
        private void ResetControl()
        {
            //SetNewSoBanAn();
            txtNgayMoPhienToa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtNgayBanAn.Text = "";
            ddlKetQuaPhucTham.SelectedIndex = 0;
            ddlLyDoBanAn.SelectedIndex = 0;
            LoadDsBiCao();
            rdAnLe.ClearSelection();
            rdAnRutGon.ClearSelection();
            rdBaoLucGD.ClearSelection();
            rdXetXuLuuDong.ClearSelection();
            rdSuaHinhPhatBS.ClearSelection();
            rdSuaBoiThuongTH.ClearSelection();
            rdSuaKhac.ClearSelection();
            rdSuaQDAnSoTham.ClearSelection();
            rdKhoiToTaiToa.ClearSelection();
            rdKoRutKhangCao.ClearSelection();
            rdDuyetKhangNghiVKS.ClearSelection();
            txtDuyetKN_VKS_BiCao.Text = "";
            txtSBC_GIUNGUYEN_AN_ST.Text = "";
            txtSBC_SUA_AN_ST.Text = "";
            txtSBC_CHAPNHAN_TOANBO.Text = "";
            txtSBC_CHAPNHAN_MOTPHAN.Text = "";
            lttMsgBanAn.Text = "";
            lttMsgAnPhi.Text = "";
            lttMsgThongKe.Text = "";
            hddID.Value = "0";
        }

        protected void rdDuyetKhangNghiVKS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdDuyetKhangNghiVKS.SelectedValue == "1")
                txtDuyetKN_VKS_BiCao.Enabled = true;
            else
                txtDuyetKN_VKS_BiCao.Enabled = false;
        }
        protected void txtNgaymophientoa_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgayMoPhienToa.Text))
            {
                if (String.IsNullOrEmpty(txtNgayBanAn.Text))
                    txtNgayBanAn.Text = txtNgayMoPhienToa.Text;
            }
        }
        string Tonghophinhphat_ST(Decimal vuanid, Decimal BicaoID)
        {
            AHS_SOTHAM_BANAN_DIEU_CHITIET_BL ahs_tonghop = new AHS_SOTHAM_BANAN_DIEU_CHITIET_BL();
            string tbla = ahs_tonghop.Tonghophinhphat_ST(vuanid, BicaoID);
            return tbla;
        }
        string Tonghophinhphat_PT(Decimal bananid, Decimal BicaoID)
        {
            AHS_PHUCTHAM_BANAN_DIEU_CT_BL ahs_tonghop = new AHS_PHUCTHAM_BANAN_DIEU_CT_BL();
            string tbla = ahs_tonghop.Tonghophinhphat_PT(bananid, BicaoID);
            return tbla;
        }
        decimal Ahs_kiemtra_khangcaokhangnghi(Decimal vuanid, Decimal BicaoID)
        {
            AHS_PHUCTHAM_BANAN_DIEU_CT_BL ahs_tonghop = new AHS_PHUCTHAM_BANAN_DIEU_CT_BL();
            decimal tbla = ahs_tonghop.Ahs_kiemtra_khangcaokhangnghi(vuanid, BicaoID);
            return tbla;
        }
        public void cmdReloadParent_Click(object sender, EventArgs e)
        {
            LoadDsBiCao();
        }
    }
}

