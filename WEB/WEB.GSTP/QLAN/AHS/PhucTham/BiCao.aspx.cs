﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.AHS.PhucTham
{
    public partial class BiCao : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public Decimal VuAnID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            VuAnID =(string.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU]+""))?0: Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            if (!IsPostBack)
            {
                if (VuAnID > 0)
                   LoadGrid();

                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                Cls_Comon.SetButton(cmdUpdateBottom, oPer.CAPNHAT);
            }
        }
        public void LoadGrid()
        {
            VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            // AHS_SOTHAM_BANAN_BL objBL = new AHS_SOTHAM_BANAN_BL();
            //DataTable tbl = objBL.GetAllBiCaoKetAnST(VuAnID);
            AHS_BICANBICAO_BL objBL = new AHS_BICANBICAO_BL();
            DataTable tbl = objBL.GetAllByVuAnID(VuAnID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                int count_all = Convert.ToInt32(tbl.Rows.Count);
                rpt.DataSource = tbl;
                rpt.DataBind();
                rpt.Visible = true;
                
                foreach (DataRow row in tbl.Rows)
                {
                    if (Convert.ToInt16(row["IsPhucTham"]+"")>0)
                        hddBiCanPhucTham.Value  += row["BiCaoID"].ToString() + ",";
                }
            }
            else
                rpt.Visible = false;
        }
       
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            Decimal bicanid = 0;
            Boolean isnew = true;
            string curr_user = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            AHS_PHUCTHAM_BICANBICAO obj = null;
            string BiCanPhucTham_Cu = ","+hddBiCanPhucTham.Value;
            string temp = ",";
            foreach (RepeaterItem item in rpt.Items)
            {
                isnew = true;
                temp = "";
                HiddenField hdd = (HiddenField)item.FindControl("hdd");
                bicanid = Convert.ToDecimal(hdd.Value);
                CheckBox chkChange = (CheckBox)item.FindControl("chkChange");
                if (chkChange.Checked)
                {
                    temp = "," + bicanid + ",";
                    if (BiCanPhucTham_Cu.Contains(temp))
                        BiCanPhucTham_Cu = BiCanPhucTham_Cu.Replace(temp, ",");
                    //----------------------------------------
                    try
                    {
                        obj = dt.AHS_PHUCTHAM_BICANBICAO.Where(x => x.VUANID == VuAnID 
                                                                 && x.BICANID == bicanid
                                                               ).Single<AHS_PHUCTHAM_BICANBICAO>();
                        if (obj != null)
                            isnew = false;
                        else
                            obj = new AHS_PHUCTHAM_BICANBICAO();
                    }
                    catch (Exception ex) { obj = new AHS_PHUCTHAM_BICANBICAO(); }
                    obj.BICANID = bicanid;
                    obj.VUANID = VuAnID;
                  
                    if (isnew )
                    {
                        obj.NGUOITAO = curr_user;
                        obj.NGAYTAO = DateTime.Now;
                        dt.AHS_PHUCTHAM_BICANBICAO.Add(obj);
                        dt.SaveChanges();
                    }
                }
            }

            if (BiCanPhucTham_Cu !=",")
            {
                String[] arr = BiCanPhucTham_Cu.Split(',');
                foreach(string item in arr)
                {
                    if (!string.IsNullOrEmpty(item +""))
                    {
                        bicanid = Convert.ToDecimal(item);
                        obj = dt.AHS_PHUCTHAM_BICANBICAO.Where(x => x.VUANID == VuAnID && x.BICANID == bicanid).Single<AHS_PHUCTHAM_BICANBICAO>();
                        dt.AHS_PHUCTHAM_BICANBICAO.Remove(obj);
                    }
                }dt.SaveChanges();
            }
            lstMsgT.Text= "Cập nhật dữ liệu thành công!";
        }
    }
}