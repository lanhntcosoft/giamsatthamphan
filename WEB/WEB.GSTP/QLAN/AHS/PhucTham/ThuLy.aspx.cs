﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.AHS.PhucTham
{
    public partial class ThuLy : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public decimal VuAnID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lstMsgB.Text = "";
                txtTuNgay.Enabled = false;
                if (Session[ENUM_LOAIAN.AN_HINHSU] != null)
                {
                    VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
                    if (!IsPostBack)
                    {
                        MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                        Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                        Cls_Comon.SetButton(cmdThemmoi, oPer.CAPNHAT);
                        LoadThongTin_XetXuSoTham();
                        CheckQuyen();
                        LoadGrid();
                        if (rpt.Items.Count == 0)
                        {
                            SetNew_SoThuLy();
                        }
                    }
                }
                else
                    Response.Redirect("/Login.aspx");
            }
            catch (Exception ex) { lstMsgB.Text = ex.Message; }
        }
        void CheckQuyen()
        {
            //Kiểm tra có kháng cáo, kháng nghị hay không?
            AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oT != null)
            {
                AHS_SOTHAM_BL objST = new AHS_SOTHAM_BL();
                DataTable tbl = objST.AHS_SOTHAM_KCaoKNghi_GETLIST(VuAnID);
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM && tbl.Rows.Count == 0)
                {
                    lstMsgB.Text = "Chưa có kháng cáo/ kháng nghị !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdThemmoi, false);
                    hddIsShowCommand.Value = "False";
                    return;
                }

                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    lstMsgB.Text = "Vụ án đã được chuyển lên tòa án cấp trên. Không được sửa đổi !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdThemmoi, false);
                    hddIsShowCommand.Value = "False";
                    return;
                }
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM)
                {
                    lstMsgB.Text = "Vụ án đã được chuyển xét xử lại cấp sơ thẩm. Không được sửa đổi !";
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdThemmoi, false);
                    hddIsShowCommand.Value = "False";
                    return;
                }
            }
            AHS_PHUCTHAM_BANAN ba = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            if (ba != null)
            {
                lstMsgB.Text = "Vụ án đã có bản án phúc thẩm. Không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdThemmoi, false);
                hddIsShowCommand.Value = "False";
                return;
            }
        }
        //------------------------------
        void LoadThongTin_XetXuSoTham()
        {
            LoadToaSoTham();
            LoadGrid_KhangCao();
            LoadGrid_KhangNghi();

            ddTruongHopTL.Items.Clear();
            if (pnKC.Visible == true && pnKN.Visible == true)
                ddTruongHopTL.Items.Add(new ListItem("Do có kháng cáo và kháng nghị phúc thẩm", "AHS_PT_ThuLyKCKN"));
            else
            {
                if (pnKC.Visible == true)
                    ddTruongHopTL.Items.Add(new ListItem("Do có kháng cáo phúc thẩm", "AHS_PT_ThuLyKC"));
                else if (pnKN.Visible == true)
                    ddTruongHopTL.Items.Add(new ListItem("Do có kháng nghị phúc thẩm", "AHS_PT_ThuLyKN"));
            }
        }
        void LoadToaSoTham()
        {
            AHS_VUAN obj = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if(obj!= null)
            {
                AHS_SOTHAM_BANAN oBA = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).SingleOrDefault<AHS_SOTHAM_BANAN>();
                if (oBA != null)
                {

                    lblBAQD.Text = "Số BA:<b>" + oBA.SOBANAN + "</b>";
                    string ngayBA = (((DateTime)oBA.NGAYBANAN) == DateTime.MinValue) ? "" : ((DateTime)oBA.NGAYBANAN).ToString("dd/MM/yyyy", cul);
                    lblNgayBAQD.Text = "Ngày BA:<b>" + ngayBA +"</b>";
                }
                else
                {
                    AHS_SOTHAM_QUYETDINH_VUAN objQD = dt.AHS_SOTHAM_QUYETDINH_VUAN.Where(x => x.VUANID == VuAnID).FirstOrDefault();
                    if (objQD != null)
                    {
                        lblBAQD.Text = "Số QĐ:<b>"+ objQD.SOQUYETDINH + "</b>";
                        string ngayba = (((DateTime)objQD.NGAYQD) == DateTime.MinValue) ? "" : ((DateTime)objQD.NGAYQD).ToString("dd/MM/yyyy", cul);
                        lblNgayBAQD.Text = " Ngày QĐ:<b>" + ngayba + "</b>";
                    }

                }
                if (obj.TOAANID > 0)
                {
                    DM_TOAAN oT = dt.DM_TOAAN.Where(x => x.ID == obj.TOAANID).First();
                    lblToaxx.Text = oT.TEN;
                }

            }

            AHS_SOTHAM_BL oBL = new AHS_SOTHAM_BL();
            //DataTable tbl = oBL.AHS_SOTHAM_HDXX_GETLIST(VuAnID);
            //if (tbl != null && tbl.Rows.Count > 0)
            //{
            //    rptToaSoTham.DataSource = tbl;
            //    rptToaSoTham.DataBind();
            //    rptToaSoTham.Visible = true;
            //}
            //else rptToaSoTham.Visible = false;
            AHS_BICANBICAO_BL objBL = new AHS_BICANBICAO_BL();
            DataTable tbl = objBL.GetAllPaging(VuAnID, null, 1, 100);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                rptToaSoTham.DataSource = tbl;
                rptToaSoTham.DataBind();
                rptToaSoTham.Visible = true;
            }else
                rptToaSoTham.Visible = false;
        }
        public void LoadGrid_KhangCao()
        {
            AHS_SOTHAM_BL oBL = new AHS_SOTHAM_BL();
            decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            DataTable oDT = oBL.AHS_SoTham_GetAllKCByVuAn(ID);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                rptKC.DataSource = oDT;
                rptKC.DataBind();
                pnKC.Visible = true;
            }
            else pnKC.Visible = false;
        }
        public void LoadGrid_KhangNghi()
        {
            AHS_SOTHAM_BL oBL = new AHS_SOTHAM_BL();
            decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            DataTable oDT = oBL.AHS_SOTHAM_GetAllKNByVuAn(ID);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                rptKN.DataSource = oDT;
                rptKN.DataBind();
                pnKN.Visible = true;
            }
            else pnKN.Visible = false;
        }
        //------------------------------
        private void LoadInfo(decimal ThuLyID)
        {
            lstMsgB.Text = "";
            AHS_PHUCTHAM_THULY obj = dt.AHS_PHUCTHAM_THULY.Where(x => x.ID == ThuLyID).FirstOrDefault<AHS_PHUCTHAM_THULY>();
            if (obj != null)
            {
                txtNgayThuLy.Text = ((DateTime)obj.NGAYTHULY).ToString("dd/MM/yyyy", cul);
                txtSoThuLy.Text = obj.SOTHULY + "";
                txtTuNgay.Text = (DateTime)obj.THOIHANTUNGAY == DateTime.MinValue ? "" : ((DateTime)obj.THOIHANTUNGAY).ToString("dd/MM/yyyy", cul);
                txtDenNgay.Text = (DateTime)obj.THOIHANDENNGAY == DateTime.MinValue ? "" : ((DateTime)obj.THOIHANDENNGAY).ToString("dd/MM/yyyy", cul);
                //txtSoNgay.Text =  obj.THOIHAN_NGAY.ToString();
                //txtSoThang.Text = obj.THOIHAN_THANG.ToString();
                if (obj.UTTPDI == 1)
                    cbUTTP.Checked = true;
                else
                    cbUTTP.Checked = false;

                DM_DATAITEM objDM = dt.DM_DATAITEM.Where(x => x.ID == obj.TRUONGHOPTHULY).FirstOrDefault();
                ddTruongHopTL.SelectedValue = objDM.MA.ToString();
            }
        }
        //void LoadDrop()
        //{
        //    DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
        //    DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.LOAITHULY_AHS_PT);

        //    ddTruongHopTL.Items.Clear();
        //    if (tbl != null && tbl.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in tbl.Rows)
        //            ddTruongHopTL.Items.Add(new ListItem(row["Ten"] + "", row["ID"] + ""));
        //    }
        //}
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckValidate())
                return;
            //------------------------------
            //Decimal ThuLyID = (String.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
            //DateTime NGAYTHULY = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //String sothuly = txtSoThuLy.Text.Trim();
            //AHS_PHUCTHAM_THULY_BL objBL = new AHS_PHUCTHAM_THULY_BL();
            //Boolean IsExist = objBL.CheckExistSoThuLy(ThuLyID,sothuly, NGAYTHULY);
            //if (IsExist)
            //{
            //    lstMsgB.Text = "Đã có số thụ lý này. Đề nghị kiểm tra lại";
            //    return;
            //}
            //else
            //{
            Save();
            ResetForm();
            hddPageIndex.Value = "1";
            LoadGrid();
            //}
        }

        void Save()
        {
            Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            Decimal VuAnId = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.Now : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            Boolean IsNew = false;
            AHS_PHUCTHAM_THULY obj = dt.AHS_PHUCTHAM_THULY.Where(x => x.VUANID == VuAnId).FirstOrDefault<AHS_PHUCTHAM_THULY>();
            if (obj == null)
            {
                IsNew = true;
                obj = new AHS_PHUCTHAM_THULY();
            }
            obj.VUANID = VuAnId;
            obj.TOAANID = ToaAnID;

            String MaTHThuLy = ddTruongHopTL.SelectedValue;
            DM_DATAITEM objDM = dt.DM_DATAITEM.Where(x => x.MA == MaTHThuLy).FirstOrDefault();
            obj.TRUONGHOPTHULY = objDM.ID;

            obj.SOTHULY = txtSoThuLy.Text.Trim();
            //-----------------------------------------
            DateTime date_temp;
            date_temp = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.Now : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYTHULY = date_temp;
            obj.THOIHANTUNGAY = obj.NGAYTHULY;
            //-------------------------------
            date_temp = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? DateTime.Now.AddDays(15) : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.THOIHANDENNGAY = date_temp;
            if (cbUTTP.Checked)
                obj.UTTPDI = 1;
            else
                obj.UTTPDI = 0;


            if (IsNew)
            {
                AHS_PHUCTHAM_THULY_BL objBL = new AHS_PHUCTHAM_THULY_BL();
                decimal ToaID = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
                decimal STT = objBL.GETNEWTT(ToaID, NgayThuLy);
                obj.TT = STT;

                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.AHS_PHUCTHAM_THULY.Add(obj);

                // update giai đoạn vụ án
                AHS_VUAN objAn = dt.AHS_VUAN.Where(x => x.ID == VuAnId).FirstOrDefault<AHS_VUAN>();
                if (objAn != null)
                    objAn.MAGIAIDOAN = ENUM_GIAIDOANVUAN.PHUCTHAM;
                //anhvh add 26/06/2020
                GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                GD.GAIDOAN_INSERT_UPDATE("1", VuAnId, 3, 0, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), 0, 0, 0);
            }
            else
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            }
            dt.SaveChanges();

            //--------------------
            GanBiCaoThamGiaPT();

            lstMsgB.Text = "Lưu dữ liệu thành công!";
        }
        void GanBiCaoThamGiaPT()
        {
            string curr_user = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            List<AHS_BICANBICAO> lst = dt.AHS_BICANBICAO.Where(x => x.VUANID == VuAnID).ToList();
            List<AHS_NGUOITHAMGIATOTUNG> lstTGTT = dt.AHS_NGUOITHAMGIATOTUNG.Where(x => x.VUANID == VuAnID).ToList();
            AHS_SOTHAM_BANAN bananidST = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).FirstOrDefault();
            List<AHS_SOTHAM_KHANGNGHI> lstKN = dt.AHS_SOTHAM_KHANGNGHI.Where(x => x.VUANID == VuAnID && x.BANANID == bananidST.ID).ToList();

            bool checkKCKN = false;
            try
            {


                if (lstKN != null && lstKN.Count > 0)
                {
                    checkKCKN = true;
                }
                else
                {
                    if (lstTGTT != null && lstTGTT.Count > 0)
                    {
                        foreach (AHS_NGUOITHAMGIATOTUNG item in lstTGTT)
                        {
                            AHS_SOTHAM_KHANGCAO tgttKC = dt.AHS_SOTHAM_KHANGCAO.Where(x => x.VUANID == VuAnID && x.NGUOIKCID == item.ID).FirstOrDefault();
                            if (tgttKC != null)
                            {
                                checkKCKN = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) {}

            if (checkKCKN)
            {
                if (lst != null && lst.Count > 0)
                {
                    AHS_PHUCTHAM_BICANBICAO obj = null;
                    Boolean isnew = true;
                    foreach (AHS_BICANBICAO item in lst)
                    {
                        try
                        {
                            try
                            {
                                obj = dt.AHS_PHUCTHAM_BICANBICAO.Where(x => x.VUANID == VuAnID && x.BICANID == item.ID).Single<AHS_PHUCTHAM_BICANBICAO>();
                                if (obj != null)
                                    isnew = false;
                                else
                                    obj = new AHS_PHUCTHAM_BICANBICAO();
                            }
                            catch (Exception ex) { obj = new AHS_PHUCTHAM_BICANBICAO(); }
                            obj.BICANID = item.ID;
                            obj.VUANID = VuAnID;

                            if (isnew)
                            {
                                obj.NGUOITAO = curr_user;
                                obj.NGAYTAO = DateTime.Now;
                                dt.AHS_PHUCTHAM_BICANBICAO.Add(obj);
                                dt.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            else
            {
                if (lst != null && lst.Count > 0)
                {
                    AHS_PHUCTHAM_BICANBICAO obj = null;
                    Boolean isnew = true;
                    foreach (AHS_BICANBICAO item in lst)
                    {
                        try
                        {
                            try
                            {
                                obj = dt.AHS_PHUCTHAM_BICANBICAO.Where(x => x.VUANID == VuAnID && x.BICANID == item.ID).Single<AHS_PHUCTHAM_BICANBICAO>();
                                if (obj != null)
                                    isnew = false;
                                else
                                    obj = new AHS_PHUCTHAM_BICANBICAO();
                            }
                            catch (Exception ex) { obj = new AHS_PHUCTHAM_BICANBICAO(); }

                            AHS_SOTHAM_KHANGCAO bcKC = dt.AHS_SOTHAM_KHANGCAO.Where(x => x.VUANID == VuAnID && x.NGUOIKCID == item.ID).FirstOrDefault();
                            if (bcKC != null)
                            {
                                obj.BICANID = item.ID;
                                obj.VUANID = VuAnID;

                                if (isnew)
                                {
                                    obj.NGUOITAO = curr_user;
                                    obj.NGAYTAO = DateTime.Now;
                                    dt.AHS_PHUCTHAM_BICANBICAO.Add(obj);
                                    dt.SaveChanges();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
        }
        protected void txtNgayThuLy_TextChanged(object sender, EventArgs e)
        {
            txtTuNgay.Text = txtNgayThuLy.Text;
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]), LoaiToiPhamID = 0;
            DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? DateTime.Now : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            // số thụ lý
            //SetNew_SoThuLy(NgayThuLy);

            AHS_VUAN vuan = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault<AHS_VUAN>();
            if (vuan != null)
            {
                LoaiToiPhamID = vuan.LOAITOIPHAMID + "" == "" ? 0 : (decimal)vuan.LOAITOIPHAMID;
            }
            DM_DATAITEM dmLoaiToiPham = dt.DM_DATAITEM.Where(x => x.ID == LoaiToiPhamID && x.HIEULUC == 1).FirstOrDefault<DM_DATAITEM>();
            if (dmLoaiToiPham != null)
            {
                string MaLoaiTP = dmLoaiToiPham.MA;
                txtDenNgay.Enabled = false;
                if (NgayThuLy != DateTime.MinValue)
                {
                    switch (MaLoaiTP)
                    {
                        case ENUM_AHS_LOAITOIPHAM.IT_NGHIEMTRONG:
                            txtDenNgay.Text = (NgayThuLy.AddDays(30)).ToString("dd/MM/yyyy", cul);
                            break;
                        case ENUM_AHS_LOAITOIPHAM.NGHIEMTRONG:
                            txtDenNgay.Text = (NgayThuLy.AddDays(45)).ToString("dd/MM/yyyy", cul);
                            break;
                        case ENUM_AHS_LOAITOIPHAM.RAT_NGHIEMTRONG:
                            txtDenNgay.Text = (NgayThuLy.AddMonths(2)).ToString("dd/MM/yyyy", cul);
                            break;
                        case ENUM_AHS_LOAITOIPHAM.DACBIET_NGHIEMTRONG:
                            txtDenNgay.Text = (NgayThuLy.AddMonths(3)).ToString("dd/MM/yyyy", cul);
                            break;
                        default:
                            txtDenNgay.Enabled = true;
                            break;
                    }
                }
                else
                    txtDenNgay.Enabled = true;
            }
            // số thụ lý
            // SetNew_SoThuLy(NgayThuLy);
            Cls_Comon.SetFocus(this, this.GetType(), txtSoThuLy.ClientID);
        }
        void SetNew_SoThuLy()
        {
            //decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            //DateTime NgayThuLy = (String.IsNullOrEmpty(txtNgayThuLy.Text.Trim())) ? date : DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //decimal ToaID = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            //try
            //{
            //    AHS_PHUCTHAM_THULY_BL obj = new AHS_PHUCTHAM_THULY_BL();
            //    decimal STT = obj.GETNEWTT(ToaID, NgayThuLy);
            //    txtSoThuLy.Text = STT.ToString();
            //}
            //catch( Exception ex) { txtSoThuLy.Text = "1"; }
            Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
            txtNgayThuLy.Text = DateTime.Now.ToString("dd/MM/yyyy");
            if (!String.IsNullOrEmpty(txtNgayThuLy.Text))
            {
                DateTime ngaythuly = DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                //Số thụ lý mới
                txtSoThuLy.Text = oSTBL.GET_STL_NEW(DonViID, "AHS_PT", ngaythuly).ToString();
            }
        }
        private void LoadGrid()
        {
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            AHS_PHUCTHAM_THULY_BL obj = new AHS_PHUCTHAM_THULY_BL();
            DataTable tbl = obj.GetByVuAnID(VuAnID);
            rpt.DataSource = tbl;
            rpt.DataBind();
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);
                Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
                string current_id = Session[ENUM_LOAIAN.AN_HINHSU] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == DONID).FirstOrDefault();
                if (oT != null)
                {
                    if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM)
                    {
                        lblSua.Text = "Chi tiết";
                        lbtXoa.Visible = false;
                    }
                }
                if (!Convert.ToBoolean(hddIsShowCommand.Value))
                {
                    lbtXoa.Visible = lblSua.Visible = false;
                }
            }
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal ThuLyID = Convert.ToDecimal(e.CommandArgument.ToString());
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    hddID.Value = ThuLyID + ""; ;
                    LoadInfo(ThuLyID);
                    break;
                case "Xoa":
                    if (oPer.XOA == false)
                    {
                        lstMsgB.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    AHS_PHUCTHAM_THULY oT = dt.AHS_PHUCTHAM_THULY.Where(x => x.ID == ThuLyID).FirstOrDefault();
                    dt.AHS_PHUCTHAM_THULY.Remove(oT);
                    dt.SaveChanges();
                    LoadGrid();
                    break;
            }
        }
        void ResetForm()
        {
            txtTuNgay.Text = txtDenNgay.Text = "";
            //txtTuNgay.Text = txtNgayThuLy.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);

            hddID.Value = "0";
            ddTruongHopTL.SelectedIndex = 0;
            txtSoThuLy.Text = "";
            txtNgayThuLy.Text = "";
            //AHS_PHUCTHAM_THULY oT = new AHS_PHUCTHAM_THULY();
            //Decimal VuAnId = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
            //AHS_PHUCTHAM_THULY_BL dsBL = new AHS_PHUCTHAM_THULY_BL();

            //SetNew_SoThuLy(DateTime.Now);
            cbUTTP.Checked = false;

        }
        protected void cmdThemmoi_Click(object sender, EventArgs e)
        {
            ResetForm();
            CheckQuyen();
        }
        private bool CheckValidate()
        {
            if (ddTruongHopTL.SelectedValue == "")
            {
                lstMsgB.Text = "Bạn chưa chọn trường hợp thụ lý. Hãy kiểm tra lại!";
                ddTruongHopTL.Focus();
                return false;
            }

            if (Cls_Comon.IsValidDate(txtNgayThuLy.Text) == false)
            {
                lstMsgB.Text = "Bạn phải nhập ngày thụ lý theo định dạng (dd/MM/yyyy)!";
                txtNgayThuLy.Focus();
                return false;
            }
            //----------------------------
            string sothuly = txtSoThuLy.Text;
            if (!String.IsNullOrEmpty(txtNgayThuLy.Text))
            {
                DateTime ngaythuly = DateTime.Parse(this.txtNgayThuLy.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoTLTheoLoaiAn(DonViID, "AHS_PT", sothuly, ngaythuly);
                if (CheckID > 0)
                {
                    Decimal CurrThuLyID = (string.IsNullOrEmpty(hddID.Value)) ? 0 : Convert.ToDecimal(hddID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GET_STL_NEW(DonViID, "AHS_PT", ngaythuly).ToString();
                    if (CheckID != CurrThuLyID)
                    {
                        //lbthongbao.Text = "Số thụ lý này đã có!";
                        strMsg = "Số thụ lý " + txtSoThuLy.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSoThuLy.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSoThuLy.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
    }
}