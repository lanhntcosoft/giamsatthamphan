﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.AHS.Hoso
{
    public partial class NguoiThamGiaToTung : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public String NgayXayRaVuAn;
        public int CurrentYear = 0;
        String TuCachTT_BiHai_Ma = ENUM_AHS_TUCACHTHAMGIATT.BIHAI;
        Decimal TuCachTT_BiHai_Id = 0;
        public Decimal VuAnID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VuAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_HINHSU] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
                CurrentYear = DateTime.Now.Year;
                if (!IsPostBack)
                {
                    string current_id = Session[ENUM_LOAIAN.AN_HINHSU] + "";
                    if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");
                    LoadCombobox();
                    ddlTuCachTGTT_SelectedIndexChanged(sender, e);
                    LoadGrid();
                    CheckQuyen();
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        void CheckQuyen()
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.TAOMOI);
            Cls_Comon.SetButton(cmdLammoi, oPer.CAPNHAT);

            AHS_VUAN oT = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            hddGiaiDoanVuAn.Value = oT.MAGIAIDOAN.ToString();

            int ma_gd = (int)oT.MAGIAIDOAN;
            NgayXayRaVuAn = ((DateTime)oT.NGAYXAYRA).ToString("dd/MM/yyyy", cul);
            if (ma_gd == (int)ENUM_GIAIDOANVUAN.DINHCHI)
            {
                lbthongbao.Text = "Vụ việc đã bị đình chỉ, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLammoi, false);
                HideButtonDS();
                return;
            }
            else if (ma_gd == (int)ENUM_GIAIDOANVUAN.PHUCTHAM || ma_gd == (int)ENUM_GIAIDOANVUAN.THULYGDT)
            {
                lbthongbao.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLammoi, false);
                HideButtonDS();
                return;
            }
            //----------------------
            List<AHS_SOTHAM_BANAN> lstBA = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == VuAnID).ToList();
            if (lstBA != null && lstBA.Count() > 0)
                cmdLammoi.Visible = false;
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
            if (Result != "")
            {
                lbthongbao.Text = Result;
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLammoi, false);
                HideButtonDS();
                return;
            }
        }
        void HideButtonDS()
        {
            foreach (DataGridItem item in dgList.Items)
            {
                LinkButton lblSua = (LinkButton)item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)item.FindControl("lbtXoa");
                lblSua.Text = "Chi tiết";
                Cls_Comon.SetLinkButton(lbtXoa, false);
            }
        }
        private void LoadCombobox()
        {
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlNgheNghiep.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.NGHENGHIEP);
            ddlNgheNghiep.DataTextField = "TEN";
            ddlNgheNghiep.DataValueField = "ID";
            ddlNgheNghiep.DataBind();
            ddlNgheNghiep.Items.Insert(0, new ListItem("--- Chọn ---", "0"));

            //---------------------------------
            ddlTuCachTGTT.Items.Clear();
            //ddlTuCachTGTT.Items.Add(new ListItem("--- Chọn ---", "0"));
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.TUCACHTGTTHS);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                string TuCach_13_HS = ENUM_AHS_TUCACHTHAMGIATT.BAOCHUAKHAC + ";" + ENUM_AHS_TUCACHTHAMGIATT.BAOVEQUYENLOIDUONGSU + ";" + ENUM_AHS_TUCACHTHAMGIATT.LUATSU;
                foreach (DataRow row in tbl.Rows)
                {
                    ddlTuCachTGTT.Items.Add(new ListItem(row["Ten"] + "", row["ID"].ToString()));
                    if (row["Ma"] + "" == TuCachTT_BiHai_Ma)
                    {
                        TuCachTT_BiHai_Id = Convert.ToDecimal(row["ID"] + "");
                        hddTuCachTT_BiHai_ID.Value = row["ID"] + "";
                    }
                    else if (TuCach_13_HS.Contains(row["Ma"] + ""))
                    {
                        hddTuCach_13HS_ID.Value += hddTuCach_13HS_ID.Value + ";" + row["ID"] + "";
                    }
                }

                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                    pnTreViThanhNien.Visible = true;
                else
                    pnTreViThanhNien.Visible = false;
            }
            else
                ddlTuCachTGTT.Items.Add(new ListItem("--- Chọn ---", "0"));
        }
        private void ResetControls()
        {
            dropLoaiDoiTuong.SelectedIndex = 0;
            ddlTuCachTGTT.SelectedIndex = 0;

            int loaidoituong = Convert.ToInt16(dropLoaiDoiTuong.SelectedValue);
            if (loaidoituong == 0)
            {
                pnNguoiDD.Visible = false;
                pnCaNhan.Visible = true;
                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                    pnTreViThanhNien.Visible = true;
                else
                    pnTreViThanhNien.Visible = false;
                rdTreViThanhNien.SelectedIndex = -1;
                ddlPLTuoi.SelectedIndex = 0;
                pnTreVTNCo.Enabled = false;
            }

            txtHoten.Text = txtNamsinh.Text = txtNgaysinh.Text = "";
            txtDiaChiChitiet.Text = txtNgaythamgia.Text = "";
            ddlGioitinh.SelectedIndex = 0;
            ddlNgheNghiep.SelectedValue = "0";

            txtNDD_ChucVu.Text = txtNDD_CMND.Text = "";
            txtNDD_Email.Text = txtNDD_HoTen.Text = txtNDD_Mobile.Text = "";

            lbthongbao.Text = "";
            hddid.Value = "0";
            Cls_Comon.SetFocus(this, this.GetType(), dropLoaiDoiTuong.ClientID);
        }
        private bool CheckValid()
        {
            int loaidoituong = Convert.ToInt16(dropLoaiDoiTuong.SelectedValue);
            if (loaidoituong == 0)
            {
                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                {
                    if (rdTreViThanhNien.SelectedValue == "")
                    {
                        lbthongbao.Text = "Mục 'Trẻ vị thành niên' bắt buộc phải chọn. Hãy kiểm tra lại.";
                        return false;
                    }
                }
            }
            return true;
        }

        protected void txtNgaysinh_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgaysinh.Text))
            {
                DateTime d;
                d = (String.IsNullOrEmpty(txtNgaysinh.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaysinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                String str_now = DateTime.Now.ToString("dd/MM/yyyy", cul);
                DateTime now = DateTime.Parse(str_now, cul, DateTimeStyles.NoCurrentDateDefault);
                if (d != DateTime.MinValue)
                {
                    if (d > now)
                    {
                        Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Ngày sinh không thể lớn hơn ngày hiện tại. Hãy kiểm tra lại!");
                        Cls_Comon.SetFocus(this, this.GetType(), txtNgaysinh.ClientID);
                    }
                    else
                        txtNamsinh.Text = d.Year.ToString();
                }
            }
            Cls_Comon.SetFocus(this, this.GetType(), txtNamsinh.ClientID);
        }
        protected void txtNamSinh_TextChanged(object sender, EventArgs e)
        {
            int namsinh = 0;
            if (!String.IsNullOrEmpty(txtNgaysinh.Text))
            {
                if (!String.IsNullOrEmpty(txtNamsinh.Text))
                {
                    namsinh = Convert.ToInt32(txtNamsinh.Text);
                    DateTime date_temp;
                    date_temp = (String.IsNullOrEmpty(txtNgaysinh.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaysinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (date_temp != DateTime.MinValue)
                    {
                        String ngaysinh = txtNgaysinh.Text.Trim();
                        String[] arr = ngaysinh.Split('/');

                        txtNgaysinh.Text = arr[0] + "/" + arr[1] + "/" + namsinh.ToString();
                    }
                }
            }
            if (!String.IsNullOrEmpty(txtNamsinh.Text))
            {
                namsinh = Convert.ToInt32(txtNamsinh.Text);
                if (namsinh > DateTime.Now.Year)
                {
                    Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Năm sinh không thể lớn hơn năm hiện tại. Hãy kiểm tra lại!");

                }
            }
            Cls_Comon.SetFocus(this, this.GetType(), ddlNgheNghiep.ClientID);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;

                SaveData();
                dgList.CurrentPageIndex = 0;
                LoadGrid();
                ResetControls();
                lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lbthongbao.Text = ex.Message;
            }
        }
        void SaveData()
        {
            Decimal NguoiTGTTID = (String.IsNullOrEmpty(hddid.Value)) ? 0 : Convert.ToDecimal(hddid.Value);

            bool isNew = false;
            AHS_NGUOITHAMGIATOTUNG oND = null;
            if (NguoiTGTTID > 0)
            {
                oND = dt.AHS_NGUOITHAMGIATOTUNG.Where(x => x.ID == NguoiTGTTID).FirstOrDefault();
                if (oND != null)
                    isNew = false;
                else
                {
                    isNew = true;
                    oND = new AHS_NGUOITHAMGIATOTUNG();
                }
            }
            else
            {
                isNew = true;
                oND = new AHS_NGUOITHAMGIATOTUNG();
            }

            oND.VUANID = VuAnID;
            oND.HOTEN = txtHoten.Text.Trim();
            oND.DIACHICHITIET = txtDiaChiChitiet.Text.Trim();

            //------
            oND.NGAYSINH = oND.NGAYTHAMGIA = (DateTime?)null;
            oND.NAMSINH = oND.NGHENGHIEPID = 0;
            oND.GIOITINH = (Decimal?)null;
            oND.NDD_HOTEN = oND.NDD_CMND = oND.NDD_MOBILE = oND.NDD_CHUCVU = oND.NDD_EMAIL = "";
            oND.ISTREVITHANHNIEN = oND.LOAITREVITHANHNIEN = 0;

            oND.LOAIDT = Convert.ToDecimal(dropLoaiDoiTuong.SelectedValue);
            if (oND.LOAIDT == 0)
            {
                //ca nhan---
                oND.NGAYSINH = (String.IsNullOrEmpty(txtNgaysinh.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaysinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NAMSINH = txtNamsinh.Text == "" ? 0 : Convert.ToDecimal(txtNamsinh.Text);
                oND.GIOITINH = Convert.ToDecimal(ddlGioitinh.SelectedValue);
                oND.NGHENGHIEPID = Convert.ToDecimal(ddlNgheNghiep.SelectedValue);
                oND.NGAYTHAMGIA = (String.IsNullOrEmpty(txtNgaythamgia.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaythamgia.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                //TuCach TGTT =  Bị hại 
                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                {
                    oND.ISTREVITHANHNIEN = Convert.ToDecimal(rdTreViThanhNien.SelectedValue);
                    string isTreViThanhNien = rdTreViThanhNien.SelectedValue;
                    if (isTreViThanhNien == "1")
                        oND.LOAITREVITHANHNIEN = Convert.ToDecimal(ddlPLTuoi.SelectedValue);
                }
            }
            else
            {
                //-------co quan, to chuc
                oND.NDD_HOTEN = txtNDD_HoTen.Text.Trim();
                oND.NDD_CHUCVU = txtNDD_ChucVu.Text.Trim();
                oND.NDD_CMND = txtNDD_CMND.Text.Trim();
                oND.NDD_MOBILE = txtNDD_Mobile.Text.Trim();
                oND.NDD_EMAIL = txtNDD_Email.Text.Trim();
            }

            if (hddTuCach_13HS_ID.Value.Contains(ddlTuCachTGTT.SelectedValue))
                oND.FILEID = UploadFileID(VuAnID, "13-HS");
            oND.NGAYSUA = DateTime.Now;
            oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
           
            // Là giai đoạn lập hồ sơ vụ án nên ISHOSO= 1
            oND.ISHOSO = 1;

            if (isNew)
                dt.AHS_NGUOITHAMGIATOTUNG.Add(oND);
            dt.SaveChanges();
            hddid.Value = oND.ID.ToString();
            try
            {
                UpdateTuCach(oND.ID);
            }
            catch (Exception ex) { }
        }
        private void UpdateTuCach(Decimal NguoiThamGiaID)
        {
            bool isNew = false;
            AHS_NGUOITHAMGIATOTUNG_TUCACH obj = dt.AHS_NGUOITHAMGIATOTUNG_TUCACH.Where(x => x.NGUOIID == NguoiThamGiaID).FirstOrDefault<AHS_NGUOITHAMGIATOTUNG_TUCACH>();
            if (obj == null)
            {
                obj = new AHS_NGUOITHAMGIATOTUNG_TUCACH();
                isNew = true;
            }
            obj.NGUOIID = NguoiThamGiaID;
            obj.TUCACHID = Convert.ToDecimal(ddlTuCachTGTT.SelectedValue);
            if (isNew)
            {
                dt.AHS_NGUOITHAMGIATOTUNG_TUCACH.Add(obj);
            }
            dt.SaveChanges();
        }
        public void LoadGrid()
        {
            lbthongbao.Text = "";
            decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU] + "");
            AHS_SOTHAM_BL objBL = new AHS_SOTHAM_BL();
            int pageindex = Convert.ToInt32(hddPageIndex.Value), page_size = Convert.ToInt32(hddPageSize.Value);
            DataTable tbl = objBL.AHS_NTGTT_GetByVuAnID(VuAnID, "HOSO", pageindex, page_size);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                int Total = Convert.ToInt32(tbl.Rows[0]["CountAll"].ToString());
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(Total, page_size).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + Total + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
                dgList.PageSize = page_size;
                dgList.DataSource = tbl;
                dgList.DataBind();
                pndata.Visible = true;
            }
            else
            {
                pndata.Visible = false;
            }
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
                switch (e.CommandName)
                {
                    case "Sua":
                        lbthongbao.Text = "";
                        loadedit(ND_id);
                        hddid.Value = e.CommandArgument.ToString();
                        break;
                    case "Xoa":
                        MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                        if (oPer.XOA == false)
                        {
                            lbthongbao.Text = "Bạn không có quyền xóa!";
                            return;
                        }
                        string StrMsg = "Không được sửa đổi thông tin.";
                        string Result = new AHS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, StrMsg);
                        if (Result != "")
                        {
                            lbthongbao.Text = Result;
                            return;
                        }
                        xoa(ND_id);
                        break;
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        #endregion
        public void xoa(decimal id)
        {
            AHS_NGUOITHAMGIATOTUNG oND = dt.AHS_NGUOITHAMGIATOTUNG.Where(x => x.ID == id).FirstOrDefault();
            if (oND != null)
            {
                dt.AHS_NGUOITHAMGIATOTUNG.Remove(oND);
                List<AHS_NGUOITHAMGIATOTUNG_TUCACH> lst = dt.AHS_NGUOITHAMGIATOTUNG_TUCACH.Where(x => x.NGUOIID == id).ToList<AHS_NGUOITHAMGIATOTUNG_TUCACH>();
                if (lst != null && lst.Count > 0)
                { dt.AHS_NGUOITHAMGIATOTUNG_TUCACH.RemoveRange(lst); }
                dt.SaveChanges();
                dgList.CurrentPageIndex = 0;
                LoadGrid();
                ResetControls();
                lbthongbao.Text = "Xóa thành công!";
            }
        }
        public void loadedit(decimal ID)
        {
            hddid.Value = ID.ToString();
            DM_HANHCHINH_BL oHCBL = new DM_HANHCHINH_BL();
            AHS_NGUOITHAMGIATOTUNG oND = dt.AHS_NGUOITHAMGIATOTUNG.Where(x => x.ID == ID).FirstOrDefault();
            if (oND != null)
            {
                int loaidoituong = (String.IsNullOrEmpty(oND.LOAIDT + "")) ? 0 : Convert.ToInt16(oND.LOAIDT);
                Cls_Comon.SetValueComboBox(dropLoaiDoiTuong, loaidoituong);
                if (loaidoituong == 0)
                {
                    //ca nhan
                    lblHoTen.Text = "Họ tên";
                    pnCaNhan.Visible = true;
                    rdTreViThanhNien.SelectedIndex = -1;
                    pnTreViThanhNien.Visible = false;
                    pnNguoiDD.Visible = false;
                }
                else
                {
                    //coquan, to chuc
                    lblHoTen.Text = "Tên " + ((loaidoituong == 1) ? "cơ quan" : "tổ chức");
                    pnCaNhan.Visible = false;
                    rdTreViThanhNien.SelectedIndex = -1;
                    pnTreViThanhNien.Visible = false;
                    pnNguoiDD.Visible = true;
                }

                //--------------------
                txtHoten.Text = oND.HOTEN;
                txtDiaChiChitiet.Text = oND.DIACHICHITIET;
                int gioitinh = (String.IsNullOrEmpty(oND.GIOITINH + "")) ? 0 : Convert.ToInt16(oND.GIOITINH);
                if (gioitinh > 1)
                    ddlGioitinh.SelectedIndex = -1;
                else
                    ddlGioitinh.SelectedValue = gioitinh.ToString();
                txtNgaysinh.Text = string.IsNullOrEmpty(oND.NGAYSINH + "") ? "" : ((DateTime)oND.NGAYSINH).ToString("dd/MM/yyyy", cul);
                txtNamsinh.Text = oND.NAMSINH == 0 ? "" : oND.NAMSINH.ToString();
                ddlNgheNghiep.SelectedValue = oND.NGHENGHIEPID.ToString();
                txtNgaythamgia.Text = string.IsNullOrEmpty(oND.NGAYTHAMGIA + "") ? "" : ((DateTime)oND.NGAYTHAMGIA).ToString("dd/MM/yyyy", cul);

                //--------------------
                txtNDD_ChucVu.Text = oND.NDD_CHUCVU + "";
                txtNDD_CMND.Text = oND.NDD_CMND + "";
                txtNDD_Email.Text = oND.NDD_EMAIL + "";
                txtNDD_HoTen.Text = oND.NDD_HOTEN + "";
                txtNDD_Mobile.Text = oND.NDD_MOBILE + "";

                //--------------------
                AHS_NGUOITHAMGIATOTUNG_TUCACH tuCach = dt.AHS_NGUOITHAMGIATOTUNG_TUCACH.Where(x => x.NGUOIID == oND.ID).FirstOrDefault<AHS_NGUOITHAMGIATOTUNG_TUCACH>();
                if (tuCach != null)
                {
                    TuCachTT_BiHai_Id = Convert.ToDecimal(hddTuCachTT_BiHai_ID.Value);
                    Decimal tucachid = (Decimal)tuCach.TUCACHID;
                    ddlTuCachTGTT.SelectedValue = tucachid + "";
                    if (tucachid == TuCachTT_BiHai_Id && loaidoituong == 0)
                    {
                        pnTreViThanhNien.Visible = true;
                        string isTreVTN = oND.ISTREVITHANHNIEN + "";
                        if (isTreVTN != "")
                        {
                            rdTreViThanhNien.SelectedValue = oND.ISTREVITHANHNIEN.ToString();
                            string isTreViThanhNien = rdTreViThanhNien.SelectedValue;
                            if (isTreViThanhNien == "1")
                            {
                                pnTreVTNCo.Enabled = true;
                                pnTreVTNKhong.Visible = false;
                                ddlPLTuoi.SelectedValue = oND.LOAITREVITHANHNIEN.ToString();
                            }
                            else
                            {
                                pnTreVTNCo.Enabled = false;
                                pnTreVTNKhong.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        rdTreViThanhNien.SelectedValue = "0";
                        pnTreVTNCo.Enabled = false;
                        pnTreVTNKhong.Visible = true;
                    }
                }
                else
                {
                    pnTreViThanhNien.Visible = false;
                }
            }
        }

        protected void btnLammoi_Click(object sender, EventArgs e)
        {
            ResetControls();
        }
        protected void ddlTuCachTGTT_SelectedIndexChanged(object sender, EventArgs e)
        {
            int loaidoituong = Convert.ToInt16(dropLoaiDoiTuong.SelectedValue);
            if (loaidoituong == 0)
            {
                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                    pnTreViThanhNien.Visible = true;
                else
                    pnTreViThanhNien.Visible = false;
            }
            Cls_Comon.SetFocus(this, this.GetType(), txtHoten.ClientID);
        }
        protected void dropLoaiDoiTuong_SelectedIndexChanged(object sender, EventArgs e)
        {
            int loaidoituong = Convert.ToInt16(dropLoaiDoiTuong.SelectedValue);
            if (loaidoituong == 0)
            {
                lblHoTen.Text = "Họ tên";
                //ca nhan
                pnCaNhan.Visible = true;
                pnNguoiDD.Visible = false;
                if (ddlTuCachTGTT.SelectedValue == hddTuCachTT_BiHai_ID.Value)
                {
                    rdTreViThanhNien.SelectedIndex = -1;
                    pnTreViThanhNien.Visible = true;
                }
                else
                    pnTreViThanhNien.Visible = false;
            }
            else
            {
                lblHoTen.Text = "Tên " + ((loaidoituong == 1) ? "cơ quan" : "tổ chức");
                //coquan, to chuc
                pnCaNhan.Visible = false;
                pnNguoiDD.Visible = true;
                rdTreViThanhNien.SelectedIndex = -1;
                pnTreViThanhNien.Visible = false;
            }

            Cls_Comon.SetFocus(this, this.GetType(), txtHoten.ClientID);
        }
        protected void rdTreViThanhNien_SelectedIndexChanged(object sender, EventArgs e)
        {
            string isTreViThanhNien = rdTreViThanhNien.SelectedValue;
            if (isTreViThanhNien == "1")
            {
                pnTreVTNCo.Enabled = true;
                Cls_Comon.SetFocus(this, this.GetType(), ddlPLTuoi.ClientID);
            }
            else
            {
                pnTreVTNCo.Enabled = false;
                Cls_Comon.SetFocus(this, this.GetType(), txtDiaChiChitiet.ClientID);
            }
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rv = (DataRowView)e.Item.DataItem;
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);

                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);

                //int ma_gd = (String.IsNullOrEmpty(hddGiaiDoanVuAn.Value)) ? 0 : Convert.ToInt16(hddGiaiDoanVuAn.Value);
                //if (ma_gd == (int)ENUM_GIAIDOANVUAN.PHUCTHAM || ma_gd == (int)ENUM_GIAIDOANVUAN.THULYGDT)
                //{
                //    lblSua.Text = "Chi tiết";
                //    Cls_Comon.SetLinkButton(lbtXoa, false);
                //}
            }
        }
        private decimal UploadFileID(decimal VuAnID, string strMaBieumau)
        {
            decimal IDFIle = 0, IDBM = 0;
            AHS_VUAN oVuAn = dt.AHS_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oVuAn != null)
            {                
                List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
                if (lstBM.Count > 0)
                {
                    IDBM = lstBM[0].ID;
                }
                bool isNew = false;
                AHS_FILE objFile = dt.AHS_FILE.Where(x => x.VUANID == VuAnID 
                                                    && x.MAGIAIDOAN == ENUM_GIAIDOANVUAN.HOSO 
                                                    && x.BIEUMAUID == IDBM).FirstOrDefault();
                if (objFile == null)
                {
                    isNew = true;
                    objFile = new AHS_FILE();
                }
                objFile.VUANID = VuAnID;
                objFile.TOAANID = oVuAn.TOAANID;
                objFile.MAGIAIDOAN = ENUM_GIAIDOANVUAN.SOTHAM;//ENUM_GIAIDOANVUAN.HOSO
                objFile.LOAIFILE = 0;
                objFile.BIEUMAUID = IDBM;
                objFile.NAM = DateTime.Now.Year;
                objFile.NGAYTAO = DateTime.Now;
                objFile.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                if (isNew)                   
                    dt.AHS_FILE.Add(objFile);
                dt.SaveChanges();
                IDFIle = objFile.ID;
            }
            return IDFIle;
        }
    }
}
