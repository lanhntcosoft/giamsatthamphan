﻿<%@ Page Language="C#" Title="" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="vbtbtotung.aspx.cs" Inherits="WEB.GSTP.QLAN.AKT.Phuctham.vbtbtotung" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .QDVACol1 {
            width: 107px;
        }

        .QDVACol2 {
            width: 270px;
        }

        .QDVACol3 {
            width: 116px;
        }
    </style>
    <script src="../../../UI/js/Common.js"></script>
    <script type="text/javascript" src="/UI/js/base64.js"></script>
    <script type="text/javascript" src="/UI/js/vgcaplugin.js"></script>
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddDonID" Value="0" runat="server" />
    <asp:Panel ID="pnTimKiem" runat="server" Visible="true">
        <div class="box">
        <div class="box_nd">
            <div class="boxchung">
                <h4 class="tleboxchung">Tìm kiếm vb/tb tố tụng</h4>
                <div class="boder" style="padding: 10px;">
                    <table class="table1">
                        <tr style="width:125px">
                            <td style="width:125px">Các VB/TB tố tụng</td>
                            <td>
                                <asp:DropDownList ID="ddlTenVBTB" CssClass="chosen-select" runat="server" Width="208px">
                                    
                                </asp:DropDownList>
                            </td>

                            <td>Đối tượng triệu tập</td>
                            <td>
                                <asp:DropDownList ID="ddlDoiTuongTrieuTap" CssClass="chosen-select" runat="server" Width="208px">
                                    
                                </asp:DropDownList>
                            </td>

                            <td>Người ký</td>
                            <td>
                                <asp:TextBox ID="txtNguoiKy" CssClass="user" runat="server" Width="200px" MaxLength="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="QDVACol1">Số</td>
                            <td class="QDVACol2">
                                <asp:TextBox ID="txtSoVBTB" runat="server" CssClass="user" Width="200px" MaxLength="20"></asp:TextBox>
                            </td>
                            <td class="QDVACol3">Ngày</td>
                            <td>
                                <asp:TextBox ID="txtNgay" runat="server" CssClass="user" Width="200px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="txtNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                            <td>Chức vụ</td>
                            <td>
                                <asp:TextBox ID="txtChucvu" CssClass="user" runat="server" Width="200px" MaxLength="250"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Nội dung</td>
                            <td style="width:max-content" colspan="6">
                                <asp:TextBox ID="txtsNoiDung" CssClass="user" TextMode="MultiLine" Width="99%" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Tìm kiếm theo</td>
                            <td>
                                <asp:DropDownList ID="ddlHieuLuc" CssClass="chosen-select" runat="server" Width="208px">
                                    <asp:ListItem Selected Value="1" Text="Hiệu lực từ ngày" />
                                    <asp:ListItem Value="2" Text="Hiệu lực đến ngày" />
                                </asp:DropDownList>
                            </td>
                            <td class="QDVACol1">Từ ngày</td>
                            <td class="QDVACol2">
                                <asp:TextBox ID="txtTuNgay" runat="server" CssClass="user" Width="200px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTuNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtTuNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                            <td class="QDVACol3">Đến ngày</td>
                            <td>
                                <asp:TextBox ID="txtDenNgay" runat="server" CssClass="user" Width="200px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDenNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                        </tr>
                        
                    </table>
                    <div style="margin-top:10px;margin-left:20px"><asp:Label runat="server" ID="lblThongbaoTimKiem" Text="" ForeColor="Red"></asp:Label></div>
                    
                    <div style="text-align:center;margin-top:10px">
                        
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="buttoninput" Text="Tìm kiếm" OnClick="btnTimKiem_Click" />
                        
                        <asp:Button ID="cmdLammoi" runat="server" CssClass="buttoninput" Text="Làm mới" OnClick="btnLammoi_Click" />
                        <asp:Button ID="btnThemmoi" runat="server" CssClass="buttoninput" Text="Thêm mới" OnClick="btnThemmoi_Click"/>
                    </div>
                </div>
            </div>
             <div class="truong">
                <table class="table1">
                    
                    <tr>
                        <td colspan="2">
                            <asp:Panel runat="server" ID="pndata" Visible="false">
                                <div class="phantrang">
                                    <div class="sobanghi">
                                        <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click"></asp:LinkButton>
                                    </div>
                                </div>
                                <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    PageSize="20" AllowPaging="True" GridLines="None" PagerStyle-Mode="NumericPages"
                                    CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                                    ItemStyle-CssClass="chan" Width="100%"
                                    OnItemCommand="dgList_ItemCommand" OnItemDataBound="dgList_ItemDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                TT
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                VB/TB tố tụng
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("MAVBTB") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Số
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("SOVBTB") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="NGAY" HeaderText="Ngày" HeaderStyle-Width="75px" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Đối tượng triệu tập
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("DOITUONG") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Nội dung
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("NOIDUNG") %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="HIEULUCTUNGAY" HeaderText="Hiệu lực từ ngày" HeaderStyle-Width="75px" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="HIEULUCDENNGAY" HeaderText="Hiệu lực đến ngày" HeaderStyle-Width="75px" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NGUOITAO" HeaderText="Người tạo" HeaderStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NGAYTAO" HeaderText="Ngày tạo" HeaderStyle-Width="65px" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Thao tác
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                               <asp:LinkButton ID="lblDownload" runat="server" Text='Tải' CausesValidation="false" CommandName="Download"
                                            CommandArgument='<%#Eval("ID") %>' ></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lblSua" runat="server" Text="Sửa" CausesValidation="false" CommandName="Sua" ForeColor="#0e7eee"
                                                    CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lbtXoa" runat="server" CausesValidation="false" Text="Xóa" ForeColor="#0e7eee"
                                                    CommandName="Xoa" CommandArgument='<%#Eval("ID") %>'
                                                    ToolTip="Xóa" OnClientClick="return confirm('Bạn thực sự muốn xóa bản ghi này? ');"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="header"></HeaderStyle>
                                    <ItemStyle CssClass="chan"></ItemStyle>
                                    <PagerStyle Visible="false"></PagerStyle>
                                </asp:DataGrid>
                                <div class="phantrang">
                                    <div class="sobanghi">
                                        <asp:HiddenField ID="hdicha" runat="server" />
                                        <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click"></asp:LinkButton>
                                    </div>
                                </div>

                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
        </asp:Panel>
    
    <asp:Panel ID="pnThemMoi" runat="server" Visible="false">
        <asp:HiddenField ID="hddNguoiKyID" runat="server" Value="0" />
        <asp:HiddenField ID="hddid" runat="server" Value="0" />
        
        <div class="box">
        <div class="box_nd">
            <div class="boxchung">
                <h4 class="tleboxchung">Thông tin quyết định</h4>
                <div class="boder" style="padding: 10px;">
                    <table class="table1">
                        <tr style="width:125px">
                            <td style="width:125px">Các VB/TB tố tụng<span class="batbuoc">(*)</span></td>
                            <td >
                                <asp:DropDownList ID="ddllTenVBTB" CssClass="chosen-select" runat="server" Width="251px">
                                </asp:DropDownList>
                            </td>
                            <td>Đối tượng triệu tập<span class="batbuoc">(*)</span></td>
                            <td >
                                <asp:DropDownList ID="ddllDoiTuongTrieuTap" CssClass="chosen-select" runat="server" Width="251px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="QDVACol1">Số<span class="batbuoc">(*)</span></td>
                            <td class="QDVACol2">
                                <asp:TextBox ID="txttSoVBTB" runat="server" CssClass="user" Width="242" MaxLength="20"></asp:TextBox>
                            </td>
                            <td class="QDVACol3">Ngày<span class="must_input">(*)</span></td>
                            <td>
                                <asp:TextBox ID="txttNgay" runat="server" CssClass="user" Width="244px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txttNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txttNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>Nội dung<span class="must_input">(*)</span></td>
                            <td style="width:max-content" colspan="4">
                                <CKEditor:CKEditorControl  ID="txttNoiDung"
                                    BasePath="/UI/Tools/ckeditor/"
                                    runat="server" Height="200px"></CKEditor:CKEditorControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="QDVACol1">Hiệu lực từ ngày<span class="must_input">(*)</span></td>
                            <td class="QDVACol2">
                                <asp:TextBox ID="txttHieuLucTuNgay" runat="server" CssClass="user" Width="200px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txttHieuLucTuNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" TargetControlID="txttHieuLucTuNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                            <td class="QDVACol3">Đến ngày<span class="must_input">(*)</span></td>
                            <td>
                                <asp:TextBox ID="txttHieuLucDenNgay" runat="server" CssClass="user" Width="200px" MaxLength="10"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txttHieuLucDenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" TargetControlID="txttHieuLucDenNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>Người ký</td>
                            <td>
                                <asp:TextBox ID="txttNguoiKy" CssClass="user" Enabled="false" runat="server" Width="200px" MaxLength="250"></asp:TextBox>
                            </td>
                            <td>Chức vụ</td>
                            <td>
                                <asp:TextBox ID="txttChucvu" CssClass="user" 
                                    Enabled="false" runat="server" Width="200px" MaxLength="250"></asp:TextBox></td>
                        </tr>
                    </table>
                    <div style="margin-top:10px;margin-left:10px">
                        <asp:Label runat="server" ID="lbthongbao" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-top:10px;text-align:center">
                        
                        <asp:Button ID="Button1" runat="server" CssClass="buttoninput" Text="Lưu" OnClick="btnUpdate_Click"/>
                        
                        <asp:Button ID="Button2" runat="server" CssClass="buttoninput" Text="Lưu & Quay lại" OnClick="btnUpdateAndQuayLai_Click" />
                        <asp:Button ID="btnQuayLai" runat="server" CssClass="buttoninput" Text="Quay lại" OnClick="btnQuayLai_Click"/>
                    </div>
                </div>
            </div>
             
        </div>
    </div>
        </asp:Panel>
    
    <script>
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
        function validate() {

            var ddlTenVBTB = $('#dvSpliter_ContentPlaceHolder1_ddlTenVBTB').val();
            if (ddlTenVBTB=='0') {
                alert('Bạn chưa chọn VB/TB tố tụng, vui lòng chọn!');
                return false;
            }

            var txtSoVBTB = document.getElementById('<%=txtSoVBTB.ClientID%>');
            val = txtSoVBTB.options[txtSoVBTB.selectedIndex].value;
            if (val == 0) {
                alert('Bạn chưa nhập số VB, vui lòng nhập!');
                txtSoVBTB.focus();
                return false;
            }

            var ngayvbtb = $('#dvSpliter_ContentPlaceHolder1_txtNgay').val();
            if (ngayvbtb = '') {
                alert('Bạn chưa nhập ngày, vui lòng nhập!');
                $('#dvSpliter_ContentPlaceHolder1_txtNgay').focus();
                return false;
            }
            var noidung = $('#cke_contents_dvSpliter_ContentPlaceHolder1_txtNoiDung').val();
            if (noidung == '') {
                alert('Bạn chưa nhập nội dung, vui lòng nhập!');
                $('#cke_contents_dvSpliter_ContentPlaceHolder1_txtNoiDung').focus();
                return false;
            }

            var hieuluctungay = $('#ctl00$dvSpliter$ContentPlaceHolder1$txtHieuLucTuNgay').val();
            if (hieuluctungay == '') {
                alert('Bạn chưa nhập hiệu lực từ ngày, vui lòng nhập!');
                $('#ctl00$dvSpliter$ContentPlaceHolder1$txtHieuLucTuNgay').focus();
                return false;
            }

            var hieulucdenngay = $('#ctl00$dvSpliter$ContentPlaceHolder1$txtHieuLucDenNgay').val();
            if (hieulucdenngay == '') {
                alert('Bạn chưa nhập hiệu lực đến ngày, vui lòng nhập!');
                $('#ctl00$dvSpliter$ContentPlaceHolder1$txtHieuLucDenNgay').focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>