﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using BL.GSTP.Danhmuc;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BL.GSTP.THA;


namespace WEB.GSTP.QLAN.THA
{
    public partial class NhanUyThacTHA_Edit : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        THA_UYTHAC_DETAIL obj = new THA_UYTHAC_DETAIL();
        private const decimal ROOT = 0;
        Decimal CurrUserID = 0, VuAnID = 0, BiAnID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");

                if (!IsPostBack)
                {
                    LoadInfoUyThac();
                    LoadDropNguoiKy();
                    //txtNgayUyThac.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        //----------------------------------
        void LoadInfoUyThac()
        {
            if (Request["uID"] != null)
            {
                Decimal DetailUyThacID = Convert.ToDecimal(Request["uID"] + "");
                THA_UYTHAC_DETAIL_BL obj = new THA_UYTHAC_DETAIL_BL();
                DataTable tbl = obj.GetInfo(DetailUyThacID);
                if (tbl != null && tbl.Rows.Count>0)
                {
                    DataRow row = tbl.Rows[0];
                   
                    lttMaVuAn.Text = row["BA_MaVuAn"] + "";
                    lttTenVuAn.Text = row["BA_TenVuAn"] + "";

                    lttMaBiAn.Text = row["MaBiCan"] + "";
                    lttTenBiAn.Text = row["TenBiCan"] + "";

                    lttQDUyThac.Text = row["SoQD"].ToString() +" - "+ row["TenQD"].ToString();
                    lttToaUyThac.Text = row["TenToaAnUyThac"] + "";
                    lttNguoiNhapUyThac.Text = row["TenNguoiNhap"] + "";
                    //
                    lttNgayUyThac.Text =String.IsNullOrEmpty( row["NgayUyThac"] + "")? "": (Convert.ToDateTime(row["NgayUyThac"].ToString())).ToString("dd/MM/yyyy", cul);
                }
            }
        }
        //---------------------
        void LoadDropNguoiKy()
        {
            DM_CANBO_BL obj = new DM_CANBO_BL();
            Decimal donvi = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DataTable tbl = obj.GetAllChanhAn_PhoCA(donvi);
            dropNguoiKy.DataSource = tbl;
            dropNguoiKy.DataTextField = "HOTEN";
            dropNguoiKy.DataValueField = "ID";
            dropNguoiKy.DataBind();
            dropNguoiKy.Items.Insert(0, new ListItem("--Chọn--", "0"));
        }
        void LoadDropByGroupName(DropDownList drop, string GroupName, Boolean ShowChangeAll)
        {
            drop.Items.Clear();
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(GroupName);
            if (ShowChangeAll)
                drop.Items.Add(new ListItem("---Chọn---", "0"));
            foreach (DataRow row in tbl.Rows)
            {
                drop.Items.Add(new ListItem(row["TEN"] + "", row["ID"] + ""));
            }
        }

        //----------------------------------
        protected void rdTruongHopUT_SelectedIndexChanged(object sender, EventArgs e)
        {
            //pnLyDo.Visible = pnKhac.Visible = false;
            //if (rdTruongHopUT.SelectedValue == "0")
            //{
            //    pnLyDo.Visible = true;
            //    dropLyDo.SelectedValue = "0";
            //    hddLoaiUyThac.Value = "0";
            //}
            //else
            //{ pnLyDo.Visible = false; hddLoaiUyThac.Value = "1"; }
        }
        //-----------------------------------------
        protected void cmdResert_Click(object sender, EventArgs e)
        {
            ResetControls();
        }
        public void ResetControls()
        {
            //txtToaAnUyThac.Text = txtUy_thac.Text = "";
            //txtQD_SoQD.Text = txtQD_NgayQD.Text = "";

            //rdTruongHopUT.SelectedValue = "0";
            //hddLoaiUyThac.Value = "0";
            //pnLyDo.Visible = true;
            //dropLyDo.SelectedValue = "0";

            //txtNgayNhan.Text = txtNgayUyThac.Text = "";
            //txtGhichu.Text = "";

            //dropLyDo.SelectedValue = dropNguoiNhap.SelectedValue = "0";

            //HddID.Value = "0";
            //lbthongbao.Text = "";
        }

        //--------------------------------------
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                //Decimal QuyetDinhTHA_ID = Convert.ToDecimal(dropQuyetDinhUyThacTHA.SelectedValue);
                //THA_UYTHAC_DETAIL obj = dt.THA_UYTHAC_DETAIL.Where(x => x.BIANID == BiAnID && x.QD_UYTHACTHA_ID == QuyetDinhTHA_ID).FirstOrDefault();
                //if (obj != null)
                //{
                //    LayDuLieuUpdate(obj);
                //    obj.NGAYSUA = DateTime.Now;
                //    obj.NGUOISUA = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                //}
                //else
                //{
                //    obj = new THA_UYTHAC_DETAIL();
                //    LayDuLieuUpdate(obj);
                //    obj.NGAYTAO = DateTime.Now;
                //    obj.NGUOITAO = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                //    dt.THA_UYTHAC_DETAIL.Add(obj);
                //}
                //dt.SaveChanges();
                //ResetControls();
                //lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception exc)
            {
                lbthongbao.Text = exc.Message;
            }
        }
        public void LayDuLieuUpdate(THA_UYTHAC_DETAIL obj)
        {
            //obj.BIANID = BiAnID;
            //obj.QD_UYTHACTHA_ID = Convert.ToDecimal(dropQuyetDinhUyThacTHA.SelectedValue);

            //obj.LOAIUYTHAC = Convert.ToDecimal(rdTruongHopUT.SelectedValue);
            //if (obj.LOAIUYTHAC == 0)
            //    obj.LYDOID = Convert.ToDecimal(dropLyDo.SelectedValue);
            //else
            //    obj.LYDOID = 0;

            ////------------------  
            //obj.NGAYUYTHAC = (String.IsNullOrEmpty(txtNgayUyThac.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayUyThac.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //obj.NGAYNHANUYTHAC = (String.IsNullOrEmpty(txtNgayNhan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayNhan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            ////-------------------
            //obj.NGUOINHAP = String.IsNullOrEmpty(dropNguoiNhap.SelectedValue) ? 0 : Convert.ToDecimal(dropNguoiNhap.SelectedValue);
        }
    }
}