﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master"
    AutoEventWireup="true" CodeBehind="ThongTinVA.aspx.cs" Inherits="WEB.GSTP.QLAN.THA.HoSo.ThongTinVA" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../../../UI/js/Common.js"></script>
    <asp:HiddenField ID="hddVuAnID" runat="server" Value="0" />
    <asp:HiddenField ID="hddBiAnID" runat="server" Value="0" />
    <style>
        .boxchung {
            float: left;
            width: 99%;
            margin-left: 0;
        }
    </style>
    <div class="box">
        <div class="box_nd">
            <div class="content_form">
                <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                    <asp:Literal ID="lstMsgTop" runat="server"></asp:Literal>
                </div>

                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">Thông tin vụ án</h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <table class="table1">
                            <tr>
                                <td style="width: 80px;">Loại</td>
                                <td style="width: 250px;">
                                    <asp:DropDownList ID="dropLoaiLuaChon" CssClass="chosen-select"
                                        Width="248px"
                                        runat="server">
                                    </asp:DropDownList></td>



                                <td style="width: 90px;"><span class="batbuoc"></span></td>
                                <td>
                                    <asp:TextBox ID="txtMaVuAn" CssClass="user"
                                        runat="server" Width="260px" Visible="false"></asp:TextBox></td>
                                <%--<td style="width: 90px;">Mã vụ án<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtMaVuAn" CssClass="user"
                                        runat="server" Width="260px"></asp:TextBox></td>--%>



                            </tr>
                            <tr>
                                <td>Tên vụ án<span class="batbuoc">(*)</span></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtTenVuAn" CssClass="user"
                                        TextMode="MultiLine" Rows="2"
                                        runat="server" Width="618px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Ngày xảy ra vụ án<span class="batbuoc">(*)</span></td>
                                <td>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNgayXayra" runat="server" CssClass="user"
                                            Width="70px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server"
                                            TargetControlID="txtNgayXayra" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                                            TargetControlID="txtNgayXayra" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN"
                                            ErrorTooltipEnabled="true" />
                                    </div>

                                    <div style="float: left; line-height:20px; margin-left: 5px;">Ngày bản án<span class="batbuoc">(*)</span></div>
                                    <div style="float: right;">
                                        <asp:TextBox ID="txtNgayBanAn" runat="server" CssClass="user"
                                            Width="70px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                            TargetControlID="txtNgayBanAn" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server"
                                            TargetControlID="txtNgayBanAn" Mask="99/99/9999"
                                            MaskType="Date" CultureName="vi-VN"
                                            ErrorTooltipEnabled="true" />
                                    </div>
                                </td>
                                <td>Số bản án</td>
                                <td>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtSoBanAn" CssClass="user align_right"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="100px" MaxLength="50"></asp:TextBox>
                                    </div>
                                    <div style="float: left; margin-left: 5px; margin-right: 3px; width:75px">Ngày hiệu lực bản án<span class="batbuoc">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNgayAnCoHieuLuc" runat="server" CssClass="user"
                                            Width="70px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server"
                                            TargetControlID="txtNgayAnCoHieuLuc" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server"
                                            TargetControlID="txtNgayAnCoHieuLuc" Mask="99/99/9999"
                                            MaskType="Date" CultureName="vi-VN"
                                            ErrorTooltipEnabled="true" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Tòa án ra bản án<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:HiddenField ID="hddToaAnID" runat="server" />
                                    <a alt="Nhập tên để chọn Tòa án ra bản án" class="tooltipright">
                                        <asp:TextBox ID="txtToaAn" CssClass="user"
                                            runat="server" Width="240px" MaxLength="250"></asp:TextBox></a>
                                </td>
                                <td>Tính chất bản án</td>
                                <td>
                                    <asp:DropDownList ID="dropTinhChatVuAn" CssClass="chosen-select"
                                        runat="server" Width="270px">
                                        <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Không"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Chưa xác định"></asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>

                        </table>
                    </div>
                </div>
                <asp:Panel ID="pnBiAn" runat="server">
                    <div class="boxchung">
                        <h4 class="tleboxchung bg_title_group bg_yellow">Thông tin bị án</h4>
                        <div class="boder" style="padding: 20px 10px;">
                            <table class="table1">
                                <tr>


                                    <td ></td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtMaBiCan" CssClass="user"
                                            runat="server" Width="240px" Visible="false"></asp:TextBox></td>

                                    <%--<td >Mã bị án</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtMaBiCan" CssClass="user"
                                            runat="server" Width="240px"></asp:TextBox></td>--%>




                                </tr>
                                <tr>
                                    <td style="width: 120px;">Tên bị án<span class="batbuoc">(*)</span></td>
                                    <td style="width: 250px;">
                                        <asp:TextBox ID="txtTenBiCan" CssClass="user"
                                            runat="server" Width="240px"></asp:TextBox>
                                    </td>
                                    <td style="width: 105px;">Tên khác</td>
                                    <td>
                                        <asp:TextBox ID="txtTenKhac" CssClass="user"
                                            runat="server" Width="242px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CMND</td>
                                    <td>
                                        <asp:TextBox ID="txtCMND" CssClass="user"
                                            runat="server" Width="240px" MaxLength="250"></asp:TextBox></td>
                                    <td>Trình độ văn hóa<span class="batbuoc">(*)</span></td>
                                    <td>
                                        <asp:DropDownList ID="dropTrinhDoVH" CssClass="chosen-select"
                                            runat="server" Width="250px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="3"><i>(CMND/Thẻ căn cước/Hộ chiếu</i>)</td>
                                </tr>
                                <tr>
                                    <td>Giới tính</td>
                                    <td>
                                        <asp:DropDownList ID="ddlGioitinh"
                                            CssClass="chosen-select" runat="server" Width="120">
                                            <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>Ngày sinh</td>
                                    <td>
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtNgaysinh" runat="server" CssClass="user" Width="100px" MaxLength="10" AutoPostBack="true" OnTextChanged="txtNgaysinh_TextChanged"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtNgaysinh" Format="dd/MM/yyyy" Enabled="true" />
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="txtNgaysinh" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                        </div>
                                        <div style="float: left; line-height: 25px; margin-right: 5px; margin-left: 10px;">Năm sinh<span class="batbuoc">(*)</span></div>
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtNamSinh" runat="server" CssClass="user"
                                                Width="53px" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Quốc tịch<span class="batbuoc">(*)</span></td>
                                    <td>
                                        <asp:DropDownList ID="dropQuocTich" CssClass="chosen-select"
                                            runat="server" Width="120px" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td>Dân tộc</td>
                                    <td>
                                        <asp:DropDownList ID="dropDanToc" CssClass="chosen-select"
                                            runat="server" Width="250px">
                                        </asp:DropDownList></td>
                                </tr>
                                <asp:Panel ID="pnHoKhau" runat="server">
                                    <tr>
                                        <%--  <td>ĐKHKTT (Mã HC)<span class="batbuoc">(*)</span></td>
                                        <td>
                                            <asp:HiddenField ID="hddHKTTID" runat="server" />
                                            <a alt="Nhập tên để chọn địa chỉ ĐKHKTT (Mã HC)" class="tooltipright">
                                                <asp:TextBox ID="txtHKTT" CssClass="user" runat="server" Width="200px" MaxLength="250"></asp:TextBox></a></td>
                                        --%>
                                        <td>Thường trú<span class="batbuoc">(*)</span></td>
                                        <td>
                                            <asp:DropDownList ID="ddlHKTT_Tinh" CssClass="chosen-select" runat="server" Width="48%"
                                                AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlHKTT_Tinh_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlHKTT_Huyen" CssClass="chosen-select"
                                                runat="server" Width="125px">
                                            </asp:DropDownList>
                                        </td>

                                        <td>Nơi ĐKHKTT chi tiết</td>
                                        <td>
                                            <asp:TextBox ID="txtHKTT_Chitiet" CssClass="user"
                                                runat="server" Width="242px" MaxLength="250"></asp:TextBox></td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td>Nơi sinh sống<span id="zoneTamtru" class="batbuoc">(*)</span></td>
                                    <td>
                                        <asp:DropDownList ID="ddlTamTru_Tinh" CssClass="chosen-select" runat="server" Width="48%"
                                            AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlTamTru_Tinh_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlTamTru_Huyen" CssClass="chosen-select"
                                            runat="server" Width="125px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>Nơi tạm trú chi tiết</td>
                                    <td>
                                        <asp:TextBox ID="txtTamtru_Chitiet" CssClass="user"
                                            runat="server" Width="242px" MaxLength="250"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Tôn giáo</td>
                                    <td>
                                        <asp:DropDownList ID="dropTonGiao" CssClass="chosen-select"
                                            runat="server" Width="248px">
                                        </asp:DropDownList></td>
                                    <td>Nghề nghiệp</td>
                                    <td>
                                        <asp:DropDownList ID="dropNgheNghiep" CssClass="chosen-select"
                                            runat="server" Width="250px">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td>Chức vụ đảng</td>
                                    <td>
                                        <asp:DropDownList ID="dropChucVuDang" CssClass="chosen-select"
                                            Width="248px" runat="server">
                                        </asp:DropDownList></td>
                                    <td>Công chức, viên chức</td>
                                    <td>
                                        <asp:DropDownList ID="dropChucVuCQ" CssClass="chosen-select"
                                            Width="250px" runat="server">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr id="row_thongtinbo">
                                    <td>Họ tên bố</td>
                                    <td>
                                        <asp:TextBox ID="txtHoTenBo" CssClass="user"
                                            runat="server" Width="240px" MaxLength="250"></asp:TextBox></td>
                                    <td>Năm sinh</td>
                                    <td>
                                        <asp:TextBox ID="txtNamSinhBo" CssClass="user align_right"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="100px" MaxLength="50"></asp:TextBox></td>
                                </tr>
                                <tr id="row_thongtinme">
                                    <td>Họ tên mẹ</td>
                                    <td>
                                        <asp:TextBox ID="txtHotenMe" CssClass="user"
                                            runat="server" Width="240px" MaxLength="250"></asp:TextBox></td>
                                    <td>Năm sinh</td>
                                    <td>
                                        <asp:TextBox ID="txtNamSinhMe" CssClass="user align_right"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="100px" MaxLength="50"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td>Nghiện hút</td>
                                    <td>
                                        <asp:RadioButtonList ID="rdNGhienHut" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>Tái phạm</td>
                                    <td>
                                        <asp:RadioButtonList ID="rdTinhTrangTaiPham" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tiền án</td>
                                    <td>
                                        <asp:TextBox ID="txtTienAn" CssClass="user align_right"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="113px" MaxLength="50" Text="0"></asp:TextBox></td>
                                    <td>Tiền sự</td>
                                    <td>
                                        <asp:TextBox ID="txtTienSu" CssClass="user align_right"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="100px" MaxLength="50" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Tình trạng giam giữ</td>
                                    <td>
                                        <asp:DropDownList ID="dropTinhTrangGiamGiu" CssClass="chosen-select"
                                            runat="server" Width="248px">
                                            <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Không"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Chưa xác định"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td><%--Nơi tạm giam--%></td>
                                    <td>
                                        <%--<asp:TextBox ID="txtNoiTamGiam" CssClass="user"
                                            runat="server" Width="200px" MaxLength="250"></asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">Tội danh<span class="batbuoc">(*)</span></td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtToiDanh" CssClass="user"
                                            runat="server" Width="735px" TextMode="MultiLine"
                                            Rows="2" Height="40px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>
                <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                    <asp:Literal ID="lstMsgBottom" runat="server"></asp:Literal>
                </div>
                <div style="margin: 5px; text-align: center; width: 95%">
                    <asp:Button ID="cmdSave" runat="server" CssClass="buttoninput" Text="Lưu"
                        OnClientClick="return validate();" OnClick="cmdSave_Click" />
                    <asp:Button ID="cmdThemBiAn" runat="server" CssClass="buttoninput"
                        Text="Thêm mới bị án" OnClick="cmdThemBiAn_Click" />
                    <asp:Button ID="cmdQuaylaiB" runat="server" CssClass="buttoninput"
                        Text="Quay lại" OnClick="cmdQuaylai_Click" />
                </div>

                <asp:Panel ID="pnListBiAn" runat="server">
                    <div style="float: left; width: 100%; margin-top: 20px;">
                        <div style="float: left; width: 100%; text-transform: uppercase; font-weight: bold;">Danh sách bị án thuộc vụ án</div>
                        <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
                        <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
                        <div class="phantrang">
                            <div class="sobanghi">
                                <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                            </div>
                            <div class="sotrang">
                                <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back" Visible="false"
                                    OnClick="lbTBack_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active" Visible="false"
                                    Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                <asp:Label ID="lbTStep1" runat="server" Text="..." Visible="false"></asp:Label>
                                <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:Label ID="lbTStep6" runat="server" Text="..." Visible="false"></asp:Label>
                                <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next" Visible="false"
                                    OnClick="lbTNext_Click"></asp:LinkButton>
                            </div>
                        </div>

                        <!--------------------------------------->
                        <table class="table2" width="100%" border="1">
                            <tr class="header">
                                <td width="42">
                                    <div align="center"><strong>TT</strong></div>
                                </td>
                                <td style="width: 85px;" align="center">Chọn bị án
                                </td>
<%--                                <td width="10%">
                                    <div align="center"><strong>Mã bị án</strong></div>
                                </td>--%>
                                <td>
                                    <div align="center"><strong>Bị án</strong></div>
                                </td>

                                <td width="60px">
                                    <div align="center"><strong>Năm sinh</strong></div>
                                </td>

                                <td width="15%">
                                    <div align="center"><strong>Địa chỉ tạm trú</strong></div>
                                </td>

                                <td width="70">
                                    <div align="center"><strong>Thao tác</strong></div>
                                </td>
                            </tr>
                            <asp:Repeater ID="rpt" runat="server"
                                OnItemCommand="rpt_ItemCommand" OnItemDataBound="rpt_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("STT") %></td>
                                        <td>
                                            <asp:LinkButton runat="server" ID="lkChange"
                                                CommandArgument='<%#Eval("ID   ") %>'
                                                CommandName="Choice"
                                                CssClass="buttonchitiet" Text="Chọn bị án" ToolTip="Chọn bị án"
                                                OnClientClick="return confirm('Bạn chắc chắn muốn chọn bị án này để cập nhật các thông tin về việc thi hành án?');"></asp:LinkButton>
                                        </td>
                                        <%--<td><%# Eval("MaBiCan") %></td>--%>
                                        <td><%# Eval("HoTen") %></td>

                                        <td><%# Eval("NamSinh") %></td>

                                        <td><%# Eval("TamTruChiTiet") %></td>

                                        <td>
                                            <div class="align_center">
                                                <asp:LinkButton ID="lblSua" runat="server" Text="Sửa" CausesValidation="false"
                                                    CommandName="sua" ForeColor="#0e7eee" ToolTip="Sửa"
                                                    CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lbtXoa" runat="server"
                                                    CausesValidation="false" Text="Xóa" ForeColor="#0e7eee"
                                                    CommandName="Xoa"
                                                    CommandArgument='<%#Eval("ID") %>'
                                                    ToolTip="Xóa"
                                                    OnClientClick="return confirm('Bạn thực sự muốn xóa bản ghi này? ');"></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>

                        <!--------------------------------------->
                        <div class="phantrang">
                            <div class="sobanghi">
                                <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                            </div>
                            <div class="sotrang">
                                <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back" Visible="false"
                                    OnClick="lbTBack_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active" Visible="false"
                                    Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                <asp:Label ID="lbBStep1" runat="server" Text="..." Visible="false"></asp:Label>
                                <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:Label ID="lbBStep6" runat="server" Text="..." Visible="false"></asp:Label>
                                <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                    Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next" Visible="false"
                                    OnClick="lbTNext_Click"></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </div>
        </div>
    </div>
    <script>

        function pageLoad(sender, args) {
            var urldm_toaan = '<%=ResolveUrl("~/Ajax/SearchDanhmuc.aspx/GetDMToaAn") %>';
            $("[id$=txtToaAn]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: urldm_toaan, data: "{ 'q': '" + request.term + "'}", dataType: "json", type: "POST", contentType: "application/json; charset=utf-8",
                        success: function (data) { response($.map(data.d, function (item) { return { label: item.split('_')[1], val: item.split('_')[0] } })) }, error: function (response) { }, failure: function (response) { }
                    });
                },
                select: function (e, i) { $("[id$=hddToaAnID]").val(i.item.val); }, minLength: 1
            });

            //-----------------------------------------------
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>

    <script>
        function validate() {
            if (!Validate_VuAn())
                return false;

            if (!Validate_BiAn())
                return false;
            return true;
        }
        function Validate_VuAn() {
<%--            var txtMaVuAn = document.getElementById('<%=txtMaVuAn.ClientID%>');
            if (!Common_CheckTextBox(txtMaVuAn,"mã vụ án"))
                return false;--%>

            var txtTenVuAn = document.getElementById('<%=txtTenVuAn.ClientID%>');
            if (!Common_CheckTextBox(txtTenVuAn, "tên vụ án"))
                return false;

            //-------------------------------------------
            var txtNgayXayra = document.getElementById('<%=txtNgayXayra.ClientID%>');
            if (!CheckDateTimeControl(txtNgayXayra, 'Ngày xảy ra vụ án'))
                return false;

            var txtNgayBanAn = document.getElementById('<%=txtNgayBanAn.ClientID%>');
            if (!CheckDateTimeControl(txtNgayBanAn, 'Ngày bản án'))
                return false;

            if (!SoSanhDate(txtNgayBanAn,  txtNgayXayra)) {
                alert('Xin vui lòng kiểm tra lại. "Ngày bản án" không thể nhỏ hơn "Ngày xảy ra vụ án" !');
                txtNgayBanAn.focus();
                return false;
            }
            //----------------------
            var txtNgayAnCoHieuLuc = document.getElementById('<%=txtNgayAnCoHieuLuc.ClientID%>');
            if (!CheckDateTimeControl(txtNgayAnCoHieuLuc, 'Ngày bản án có hiệu lực'))
                return false;
            if (!SoSanhDate(txtNgayAnCoHieuLuc, txtNgayXayra)) {
                alert('Xin vui lòng kiểm tra lại. "Ngày bản án có hiệu lực" không thể nhỏ hơn "Ngày xảy ra vụ án" !');
                txtNgayAnCoHieuLuc.focus();
                return false;
            }
            if (!SoSanhDate(txtNgayAnCoHieuLuc, txtNgayBanAn)) {
                alert('Xin vui lòng kiểm tra lại. "Ngày bản án có hiệu lực" không thể nhỏ hơn "Ngày bản án" !');
                txtNgayAnCoHieuLuc.focus();
                return false;
            }

            //-------------------------------------------
            var hddToaAn = document.getElementById('<%=hddToaAnID.ClientID%>');
            var txtToaAn = document.getElementById('<%=txtToaAn.ClientID%>');
            if (!Common_CheckEmpty(hddToaAn.value)) {
                alert('Bạn chưa chọn tòa án ra bản án. Hãy kiểm tra lại!');
                txtToaAn.focus();
                return false;
            }
            return true;
        }
        function Validate_BiAn() {
           <%-- var txtMaBiCan = document.getElementById('<%=txtMaBiCan.ClientID%>');
            if (!Common_CheckTextBox(txtMaBiCan, "mã bị án"))
                return false;--%>
            //-----------------------------
            var txtTen = document.getElementById('<%=txtTenBiCan.ClientID%>');
            if (!Common_CheckTextBox(txtTen, "tên bị án"))
                return false;
            //-----------------------------
            var txtNgaysinh = document.getElementById('<%=txtNgaysinh.ClientID%>');
            if (Common_CheckEmpty(txtNgaysinh.value)) {
                if (!CheckDateTimeControl(txtNgaysinh, 'Ngày sinh của bị án'))
                    return false;
            }

            var txtNamSinh = document.getElementById('<%=txtNamSinh.ClientID%>');
            if (!Common_CheckEmpty(txtNamSinh.value)) {
                alert('Bạn chưa nhập năm sinh của bị án.Hãy kiểm tra lại!');
                txtNamSinh.focus();
                return false;
            }
            else {
                var CurrentYear = '<%=CurrentYear%>';
                var namsinh = parseInt(txtNamSinh.value);
                if (namsinh > CurrentYear) {
                    alert('Năm sinh phải nhỏ hơn hoặc bằng năm hiện tại. Hãy kiểm tra lại!');
                    txtNamSinh.focus();
                    return false;
                }
            }

            //-----------------------------
            var dropQuocTich = document.getElementById('<%=dropQuocTich.ClientID%>');
            var value_change = dropQuocTich.options[dropQuocTich.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn mục "quốc tịch". Hãy kiểm tra lại!');
                dropQuocTich.focus();
                return false;
            }

            //-----------------------------
            var dropDanToc = document.getElementById('<%=dropDanToc.ClientID%>');
            value_change = dropDanToc.options[dropDanToc.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn mục "Dân tộc". Hãy kiểm tra lại!');
                dropDanToc.focus();
                return false;
            }

            //-----------------------------------------
            var QuocTichVN = '<%=QuocTichVN%>';
            var dropQuocTich = document.getElementById('<%=dropQuocTich.ClientID%>');
            value_change = dropQuocTich.options[dropQuocTich.selectedIndex].value;

            if (value_change == QuocTichVN) {
                document.getElementById('zoneTamtru').innerHTML = "<span class='batbuoc'>(*)</span>";

                //--------check nhap ho khau thuong tru---------
                var ddlHKTT_Tinh = document.getElementById('<%=ddlHKTT_Tinh.ClientID%>');
                var ddlHKTT_Huyen = document.getElementById('<%=ddlHKTT_Huyen.ClientID%>');

                value_change = ddlHKTT_Tinh.options[ddlHKTT_Tinh.selectedIndex].value;
                if (value_change == "0") {
                    alert('Bạn chưa chọn mục "Tỉnh/TP của nơi Thường trú". Hãy kiểm tra lại!');
                    ddlHKTT_Tinh.focus();
                    return false;
                }
                value_change = ddlHKTT_Huyen.options[ddlHKTT_Huyen.selectedIndex].value;
                if (value_change == "0") {
                    alert('Bạn chưa chọn mục "Quận/Huyện của nơi Thường trú". Hãy kiểm tra lại!');
                    ddlHKTT_Huyen.focus();
                    return false;
                }

                //--------check nhap ho khau tam tru---------
                var ddlTamTru_Tinh = document.getElementById('<%=ddlTamTru_Tinh.ClientID%>');
                var ddlTamTru_Huyen = document.getElementById('<%=ddlTamTru_Huyen.ClientID%>');
                value_change = ddlTamTru_Tinh.options[ddlTamTru_Tinh.selectedIndex].value;
                if (value_change == "0") {
                    alert('Bạn chưa chọn mục "Tỉnh/TP của nơi Tạm trú". Hãy kiểm tra lại!');
                    ddlTamTru_Tinh.focus();
                    return false;
                }
                value_change = ddlTamTru_Huyen.options[ddlTamTru_Huyen.selectedIndex].value;
                if (value_change == "0") {
                    alert('Bạn chưa chọn mục "Quận/Huyện của nơi Tạm trú". Hãy kiểm tra lại!');
                    ddlTamTru_Huyen.focus();
                    return false;
                }
            }
            else
                document.getElementById('zoneTamtru').innerHTML = "";
            //----------------------------
            var rdNGhienHut = document.getElementById('<%=rdNGhienHut.ClientID%>');
            msg = 'Mục "Nghiện hút"  bắt buộc phải chọn.';
            msg += 'Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdNGhienHut, msg))
                return false;
            //----------------------------
            var rdTinhTrangTaiPham = document.getElementById('<%=rdTinhTrangTaiPham.ClientID%>');
            msg = 'Mục "Tái phạm"  bắt buộc phải chọn.';
            msg += 'Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdTinhTrangTaiPham, msg))
                return false;

            //-------------------------------------------------
            var dropTrinhDoVH = document.getElementById('<%=dropTrinhDoVH.ClientID%>');
            value_change = dropTrinhDoVH.options[dropTrinhDoVH.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn mục "trình độ văn hóa". Hãy kiểm tra lại!');
                dropTrinhDoVH.focus();
                return false;
            }
            //--------------------
            var txtToiDanh = document.getElementById('<%=txtToiDanh.ClientID%>');
            if (!Common_CheckTextBox(txtToiDanh, "tội danh của bị án"))
                return false;
            return true;
        }
    </script>
</asp:Content>

