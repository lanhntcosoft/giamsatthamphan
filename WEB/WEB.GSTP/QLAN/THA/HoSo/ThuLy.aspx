﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="ThuLy.aspx.cs" Inherits="WEB.GSTP.QLAN.THA.HoSo.ThuLy" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../UI/js/Common.js"></script>
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddBiAnID" Value="0" runat="server" />
    <asp:HiddenField ID="hddVuAnID" Value="0" runat="server" />

    <style>
        .boxchung {
            float: left;
            width: 99%;
            margin-left: 0;
        }
    </style>

    <div class="box">
        <div class="box_nd">
            <asp:Panel ID="pn" runat="server">
                <div class="boxchung">
                    <h4 class="tleboxchung">Thông tin thụ lý</h4>
                    <div class="boder" style="padding: 10px;">
                        <table class="table1">
                            <tr>
                                <td style="width: 75px;">Mã thụ lý</td>
                                <td style="width: 200px;">
                                    <asp:TextBox ID="txtMaThuLy"
                                        CssClass="user" runat="server" Width="170px" placeholder="(Mã sinh tự động)"></asp:TextBox>
                                </td>
                                <td style="width: 100px;">Bị án</td>
                                <td>
                                    <asp:DropDownList ID="dropBiAn" CssClass="chosen-select"
                                        runat="server" Width="277px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>Địa chỉ</td>
                                <td>
                                    <asp:TextBox ID="txtDiaChi" CssClass="user" runat="server" Width="170px"></asp:TextBox>
                                </td>
                                <td>Trường hợp thụ lý</td>
                                <td>
                                    <asp:DropDownList ID="dropTruongHopThuLy"
                                        CssClass="chosen-select" runat="server" Width="277px">
                                        <asp:ListItem Value="1" Text="Khiếu kiện"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Ngày bản án có hiệu lực</td>
                                <td>
                                    <asp:TextBox ID="txtNgayBanAnCoHieuLuc" CssClass="user align_right"
                                        Enabled="false" runat="server" Width="170px"></asp:TextBox>
                                </td>
                                <td>Số thụ lý<span class="batbuoc">(*)</span></td>
                                <td>
                                    <div style="float: left">
                                        <asp:TextBox ID="txtSoThuly" CssClass="user align_right" runat="server"
                                            Width="90px"></asp:TextBox>
                                    </div>
                                    <div style="float: left; margin-left: 3px; margin-right: 3px; line-height: 20px;">Ngày thụ lý<span class="batbuoc">(*)</span></div>
                                    <asp:TextBox ID="txtNgaythuly" runat="server" CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtNgaythuly" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtNgaythuly" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                            </tr>

                            <tr>
                                <td>Thời hạn</td>
                                <td>

                                    <asp:TextBox ID="txtHanThang" CssClass="user" Style="text-align: center" onkeypress="return isNumber(event)" runat="server" Width="40px" MaxLength="2"></asp:TextBox>
                                    Tháng
                            <asp:TextBox ID="txtHanNgay" CssClass="user" Style="text-align: center" onkeypress="return isNumber(event)" runat="server" Width="40px" MaxLength="2"></asp:TextBox>
                                    Ngày
                                </td>

                                <td>Từ ngày</td>
                                <td>
                                    <div style="float: left">
                                        <asp:TextBox ID="txtTuNgay" runat="server" CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTuNgay" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtTuNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </div>
                                    <div style="float: left; width: 77px; margin-left: 3px; margin-right: 3px; line-height: 20px;">Đến ngày</div>
                                    <asp:TextBox ID="txtDenNgay" runat="server" CssClass="user" Width="90px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtDenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDenNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3"><i>(Thời hạn ra QĐ thi hành án)</i></td>
                            </tr>
                            <tr>
                                <td>Ghi chú</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtGhichu" CssClass="user" runat="server" Width="590px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">
                                    <asp:Button ID="cmdUpdate" runat="server"
                                        CssClass="buttoninput" Text="Lưu" OnClick="btnUpdate_Click" OnClientClick="return validate();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>

            <div style="width: 100%; float: left;">

                <asp:Label runat="server" ID="lbthongbao" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }

        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function validate() {
            var NgaySoSanh = '<%= NgaySoSanh%>';

            var txtSoThuly = document.getElementById('<%=txtSoThuly.ClientID%>');
            if (!Common_CheckTextBox(txtSoThuly, "Số thụ lý"))
                return false;
            //-------------------------------------
            var txtNgaythuly = document.getElementById('<%=txtNgaythuly.ClientID%>');
            if (!CheckDateTimeControl(txtNgaythuly, 'Ngày thụ lý'))
                return false;
            if (!SoSanh2Date(txtNgaythuly, 'Ngày thụ lý', NgaySoSanh, 'Ngày bản án có hiệu lực'))
                return false;

            //-------------------------------------
            var txtTuNgay = document.getElementById('<%=txtTuNgay.ClientID%>');
            if (Common_CheckEmpty(txtTuNgay.value)) {
                if (!Common_IsTrueDate(txtTuNgay.value)) {
                    txtDenNgay.focus();
                    return false;
                }
            }
            var txtDenNgay = document.getElementById('<%=txtDenNgay.ClientID%>');
             if (Common_CheckEmpty(txtDenNgay.value)) {
                 if (!Common_IsTrueDate(txtDenNgay.value)) {
                     txtDenNgay.focus();
                     return false;
                 }
                 //------------------------
                 if (!SoSanh2Date(txtDenNgay, 'Mục "Đến ngày"', txtTuNgay.value, 'mục "Từ ngày"', )) {
                     txtDenNgay.focus();
                     return false;
                 }
            }
            return true;
        }
    </script>
</asp:Content>
