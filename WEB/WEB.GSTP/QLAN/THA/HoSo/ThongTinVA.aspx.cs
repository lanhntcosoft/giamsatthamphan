﻿using BL.GSTP;
using DAL.GSTP;

//using BL.GSTP.AHS;
using BL.GSTP.THA;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.THA.HoSo
{
    public partial class ThongTinVA : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        GSTPContext dt = new GSTPContext();
        Decimal CurrUserID = 0, VuAnID =0, BiAnID =0;
        public decimal QuocTichVN = 0;

        public int CurrentYear = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentYear = DateTime.Now.Year;
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                QuocTichVN = new DM_DATAITEM_BL().GetQuocTichID_VN();
                if (!IsPostBack)
                {                   
                    LoadDrop();
                    if (Request["aID"] != null)
                    {
                        VuAnID = Convert.ToDecimal(Request["aID"] + "");
                        hddVuAnID.Value = VuAnID.ToString();
                    }
                    if (Request["bID"] != null)
                    {
                        BiAnID = Convert.ToDecimal(Request["bID"] + "");
                        hddBiAnID.Value = BiAnID.ToString();
                    }
                    dropLoaiLuaChon.SelectedValue = "0";

                    //------------------------------
                    BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                    if(Request["bID"]!= null)
                        BiAnID = Convert.ToDecimal(Request["bID"] + "");
                    hddBiAnID.Value = BiAnID.ToString();
                    if (BiAnID>0)
                        LoadInfo();
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
     
        //---------------------------------
        void LoadDrop()
        {
            LoadDropTinh_Huyen();
            LoadDropLoai();
            LoadDropByGroupName(dropQuocTich, ENUM_DANHMUC.QUOCTICH, false);
            LoadDropByGroupName(dropTrinhDoVH, ENUM_DANHMUC.TRINHDOVANHOA, true);
            LoadDropByGroupName(dropDanToc, ENUM_DANHMUC.DANTOC, true);
            LoadDropByGroupName(dropNgheNghiep, ENUM_DANHMUC.NGHENGHIEP, true);
            LoadDropByGroupName(dropTonGiao, ENUM_DANHMUC.TONGIAO, true);
            LoadDropByGroupName(dropTinhTrangGiamGiu, ENUM_DANHMUC.TINHTRANGGIAMGIU, false);
            LoadDropByGroupName(dropChucVuDang, ENUM_DANHMUC.CHUCVUDANG, true);
            LoadDropByGroupName(dropChucVuCQ, ENUM_DANHMUC.CHUCVU, true);
            //--------------
            dropTinhChatVuAn.Items.Clear();
            dropTinhChatVuAn.Items.Add(new ListItem("Ít nghiêm trọng", "1"));
            dropTinhChatVuAn.Items.Add(new ListItem("Nghiêm trọng", "2"));
            dropTinhChatVuAn.Items.Add(new ListItem("Rất nghiêm trọng", "3"));
            dropTinhChatVuAn.Items.Add(new ListItem("Đặc biệt nghiêm trọng", "4"));
        }
        void LoadDropByGroupName(DropDownList drop, string GroupName, Boolean ShowChangeAll)
        {
            drop.Items.Clear();
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(GroupName);
            if (ShowChangeAll)
                drop.Items.Add(new ListItem("------- Chọn --------", "0"));
            foreach (DataRow row in tbl.Rows)
            {
                drop.Items.Add(new ListItem(row["TEN"] + "", row["ID"] + ""));
            }
        }
        void LoadDropLoai()
        {

            if (Request["type"] == "list")
            {
                dropLoaiLuaChon.Enabled = false;
            }
            else
            {
                dropLoaiLuaChon.Enabled = true;
            }
            dropLoaiLuaChon.Items.Clear();
            dropLoaiLuaChon.Items.Add(new ListItem("Thuộc hệ thống quản lý án", "1"));
            dropLoaiLuaChon.Items.Add(new ListItem("Ngoài hệ thống quản lý án", "0"));
            //if (Request["type"] == "list")
            //{
            //    dropLoaiLuaChon.Enabled = false;
            //}
            //else
            //{
            //    dropLoaiLuaChon.Enabled = true;
            //}
        }
        //------------------------------
        private const decimal ROOT = 0;
        private void LoadDropTinh_Huyen()
        {
            ddlHKTT_Tinh.Items.Clear();
            ddlTamTru_Tinh.Items.Clear();
            ddlHKTT_Tinh.Items.Add(new ListItem("Chọn Tỉnh/TP", "0"));
            ddlTamTru_Tinh.Items.Add(new ListItem("Chọn Tỉnh/TP", "0"));
            List<DM_HANHCHINH> lstTinh = dt.DM_HANHCHINH.Where(x => x.CAPCHAID == ROOT).OrderBy(x => x.THUTU).ToList<DM_HANHCHINH>();
            if (lstTinh != null && lstTinh.Count > 0)
            {
                foreach (DM_HANHCHINH objTinh in lstTinh)
                {
                    ListItem item = new ListItem(objTinh.TEN, objTinh.ID.ToString());
                    ddlHKTT_Tinh.Items.Add(item);
                    ddlTamTru_Tinh.Items.Add(item);
                }
            }

            //----------------------------
            ddlHKTT_Huyen.Items.Clear();
            ddlTamTru_Huyen.Items.Clear();

            ddlHKTT_Huyen.Items.Add(new ListItem("---Chọn---", "0"));
            ddlTamTru_Huyen.Items.Add(new ListItem("---Chọn---", "0"));
            Decimal TinhID = Convert.ToDecimal(ddlHKTT_Tinh.SelectedValue);
            if (TinhID > 0)
            {
                List<DM_HANHCHINH> lstHuyen = dt.DM_HANHCHINH.Where(x => x.CAPCHAID == TinhID).OrderBy(x => x.THUTU).ToList<DM_HANHCHINH>();
                if (lstHuyen != null && lstHuyen.Count > 0)
                {
                    foreach (DM_HANHCHINH obj in lstHuyen)
                    {
                        ListItem item = new ListItem(obj.TEN, obj.ID.ToString());
                        ddlHKTT_Huyen.Items.Add(item);
                        ddlTamTru_Huyen.Items.Add(item);
                    }
                }
            }
        }
        private void LoadDropHuyenByTinh(DropDownList drop, Decimal TinhID)
        {
            drop.Items.Clear();
            List<DM_HANHCHINH> lstHuyen = dt.DM_HANHCHINH.Where(x => x.CAPCHAID == TinhID).OrderBy(x => x.THUTU).ToList<DM_HANHCHINH>();
            if (lstHuyen != null && lstHuyen.Count > 0)
            {
                drop.DataSource = lstHuyen;
                drop.DataTextField = "TEN";
                drop.DataValueField = "ID";
                drop.DataBind();
            }
            else
                drop.Items.Add(new ListItem("---Chọn---", "0"));
        }
        protected void ddlHKTT_Tinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDropHuyenByTinh(ddlHKTT_Huyen, Convert.ToDecimal(ddlHKTT_Tinh.SelectedValue));
            }
            catch (Exception ex) { lstMsgTop.Text = lstMsgBottom.Text = ex.Message; }
        }
        protected void ddlTamTru_Tinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDropHuyenByTinh(ddlTamTru_Huyen, Convert.ToDecimal(ddlTamTru_Tinh.SelectedValue));
            }
            catch (Exception ex) { lstMsgTop.Text = lstMsgBottom.Text = ex.Message; }
        }
        //---------------------------------
        void LoadInfo()
        {
            //--------Load Ds Bi An----------------
            BiAnID = Convert.ToDecimal(hddBiAnID.Value);
            try
            {
                THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
                VuAnID = (decimal)obj.VUANID;
                hddVuAnID.Value = VuAnID.ToString();
                if (VuAnID > 0)
                {
                    pnBiAn.Visible = false;
                    hddPageIndex.Value = "1";
                    LoadDSBiAnTheoVuAn();

                    THA_VUAN objAn = dt.THA_VUAN.Where(x => x.ID == VuAnID).Single<THA_VUAN>();
                    if (objAn != null)
                    {
                        txtMaVuAn.Text = objAn.BA_MAVUAN;
                        txtTenVuAn.Text = objAn.BA_TENVUAN;
                        txtNgayXayra.Text = objAn.BA_NGAYVUAN != null ? ((DateTime)objAn.BA_NGAYVUAN).ToString("dd/MM/yyyy", cul) : "";
                        dropTinhChatVuAn.SelectedValue = objAn.BA_TINHCHAT +"";
                        dropLoaiLuaChon.SelectedValue = objAn.ISHETHONG +"";
                        dropLoaiLuaChon.Enabled = false;

                        txtNgayBanAn.Text = objAn.BA_ST_NGAYBANAN != null ? ((DateTime)objAn.BA_ST_NGAYBANAN).ToString("dd/MM/yyyy", cul) : "";
                        txtNgayAnCoHieuLuc.Text = objAn.BA_ST_NGAYHIEULUC != null ? ((DateTime)objAn.BA_ST_NGAYHIEULUC).ToString("dd/MM/yyyy", cul) : "";
                        Decimal ToaAnId =(Decimal) objAn.TOAANID;
                        try
                        {
                            String ToaAn = dt.DM_TOAAN.Where(x => x.ID == ToaAnId).Single<DM_TOAAN>().TEN;
                            txtToaAn.Text = ToaAn;
                            hddToaAnID.Value = ToaAnId.ToString();
                        }
                        catch(Exception ex) { }
                    }
                }
            }  catch   (Exception EX) { }
        }

        //-------------------------------------
        protected void txtNgaysinh_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgaysinh.Text))
            {
                DateTime date_temp;
                date_temp = (String.IsNullOrEmpty(txtNgaysinh.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaysinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (date_temp != DateTime.MinValue)
                {
                    txtNamSinh.Text = date_temp.Year + "";
                }
            }
        }

        //--------------------------------------
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            Decimal THA_VuAnId = (String.IsNullOrEmpty(hddVuAnID.Value))? 0: Convert.ToDecimal(hddVuAnID.Value);
            //Lưu thong tin vu an--> Luu ID vao hddVuAnID
            AddVuAn();

            //----------------------------
            UpdateBiAn();

            //-------------------------
            
            hddPageIndex.Value = "1";
            LoadDSBiAnTheoVuAn();
            lstMsgTop.Text = lstMsgBottom.Text = "Đã cập nhật xong dữ liệu!" ;
        }
        void AddVuAn()
        {
            DateTime date_temp;
            string tenvuan = txtTenVuAn.Text.Trim();
            string mavuan = txtMaVuAn.Text.Trim();
                        
            Decimal THA_VuAnId = (String.IsNullOrEmpty(hddVuAnID.Value)) ? 0 : Convert.ToDecimal(hddVuAnID.Value);
            THA_VUAN obj = new THA_VUAN();
            if (THA_VuAnId > 0)
                obj = dt.THA_VUAN.Where(x => x.ID == THA_VuAnId).Single<THA_VUAN>();
            else
            {
                //Check 
                obj = new THA_VUAN();
            }

            obj.BA_MAVUAN = mavuan;
            obj.BA_TENVUAN = tenvuan;
            obj.BA_TINHCHAT =Convert.ToDecimal( dropTinhChatVuAn.SelectedValue);
            obj.ISHETHONG = Convert.ToDecimal(dropLoaiLuaChon.SelectedValue);
            
            //------------------------------------------
            date_temp = (String.IsNullOrEmpty(txtNgayXayra.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayXayra.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.BA_NGAYVUAN = date_temp;
            obj.BA_THANG = Convert.ToDecimal(date_temp.Month);
            obj.BA_NAM = Convert.ToDecimal(date_temp.Year);

            obj.TOAANID = (hddToaAnID.Value == "") ? 0 : Convert.ToDecimal(hddToaAnID.Value);

            obj.BA_PT_SO = txtSoBanAn.Text.Trim();
            obj.BA_ST_TOAANID = (hddToaAnID.Value == "") ? 0 : Convert.ToDecimal(hddToaAnID.Value);

            date_temp = (String.IsNullOrEmpty(txtNgayBanAn.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayBanAn.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.BA_ST_NGAYBANAN = date_temp;
            date_temp = (String.IsNullOrEmpty(txtNgayAnCoHieuLuc.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayAnCoHieuLuc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.BA_ST_NGAYHIEULUC = date_temp;

            //------------------------
            if (obj.ISHETHONG == 0)
            {
                //Thuoc he thong quan ly an  
                obj.IDVUANHETHONG = 0;
            }
            //--------------------------------
            if (THA_VuAnId > 0)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_VUAN.Add(obj);
                dt.SaveChanges();
                THA_VuAnId = obj.ID;
            }
            hddVuAnID.Value = THA_VuAnId.ToString();
        }
       
        void UpdateBiAn()
        {
            DateTime date_temp;
            Decimal BiAnId = (String.IsNullOrEmpty(hddBiAnID.Value)) ? 0 : Convert.ToDecimal(hddBiAnID.Value);

            THA_BIAN objBA = new THA_BIAN();
            if (BiAnId > 0)
                objBA = dt.THA_BIAN.Where(x => x.ID == BiAnId).Single<THA_BIAN>();
            else
                objBA = new THA_BIAN();

            Decimal THA_VuAnId = (String.IsNullOrEmpty(hddVuAnID.Value)) ? 0 : Convert.ToDecimal(hddVuAnID.Value);
            objBA.VUANID = THA_VuAnId;
            objBA.MABICAN = txtMaBiCan.Text.Trim();
            objBA.HOTEN = txtTenBiCan.Text.Trim();
            objBA.TENKHAC = txtTenKhac.Text.Trim();
            objBA.SOCMND = txtCMND.Text.Trim();

            objBA.TRINHDOVANHOAID = Convert.ToDecimal(dropTrinhDoVH.SelectedValue);
            objBA.QUOCTICHID = Convert.ToDecimal(dropQuocTich.SelectedValue);
            objBA.DANTOCID = Convert.ToDecimal(dropDanToc.SelectedValue);
            objBA.TONGIAOID = Convert.ToDecimal(dropTonGiao.SelectedValue);
            objBA.GIOITINH = Convert.ToDecimal(ddlGioitinh.SelectedValue);
            objBA.TINHTRANGGIAMGIUID = Convert.ToDecimal(dropTinhTrangGiamGiu.SelectedValue);
            objBA.NGHENGHIEPID = Convert.ToInt32(dropNgheNghiep.SelectedValue);

            objBA.TOIDANH = txtToiDanh.Text.Trim();  

            //-------------------------------------
            date_temp = (String.IsNullOrEmpty(txtNgaysinh.Text.Trim()) ) ? DateTime.MinValue : DateTime.Parse(this.txtNgaysinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            objBA.NGAYSINH = date_temp;
            if (date_temp != DateTime.MinValue)
                objBA.THANGSINH = Convert.ToDecimal(date_temp.Month);
            objBA.NAMSINH = Convert.ToDecimal(txtNamSinh.Text);

            objBA.CHUCVUCHINHQUYENID = Convert.ToDecimal(dropChucVuCQ.SelectedValue);
            objBA.CHUCVUDANGID = Convert.ToDecimal(dropChucVuDang.SelectedValue);

            //--------------------------------
            objBA.TAMTRU = Convert.ToDecimal(ddlTamTru_Tinh.SelectedValue);
            objBA.TAMTRU_HUYEN = Convert.ToDecimal(ddlTamTru_Tinh.SelectedValue);
            objBA.TAMTRUCHITIET = txtTamtru_Chitiet.Text;

            objBA.HKTT = Convert.ToDecimal(ddlHKTT_Tinh.SelectedValue);
            objBA.HKTT_HUYEN = Convert.ToDecimal(ddlHKTT_Huyen.SelectedValue);
            objBA.KHTTCHITIET = txtHKTT_Chitiet.Text;

            //--------------------------------
            objBA.HOTENBO = txtHoTenBo.Text.Trim();
            objBA.NAMSINHBO = (String.IsNullOrEmpty(txtNamSinhBo.Text + "")) ? 0 : Convert.ToDecimal(txtNamSinhBo.Text.Trim());
            objBA.HOTENME = txtHotenMe.Text.Trim();
            objBA.NAMSINHME = (String.IsNullOrEmpty(txtNamSinhMe.Text + "")) ? 0 : Convert.ToDecimal(txtNamSinhMe.Text.Trim());

            //--------------------------------
            objBA.NGHIENHUT = Convert.ToInt16(rdNGhienHut.SelectedValue);
            objBA.TAIPHAM = Convert.ToInt16(rdTinhTrangTaiPham.SelectedValue);

            //--------------------------------
            objBA.TIENAN = (string.IsNullOrEmpty(objBA.TIENAN + "")) ? 0 : Convert.ToInt32(objBA.TIENAN);
            objBA.TIENSU = (string.IsNullOrEmpty(objBA.TIENSU + "")) ? 0 : Convert.ToInt32(objBA.TIENSU);

            //--------------------------------
            if (BiAnId > 0)
            {
                objBA.NGAYSUA = DateTime.Now;
                objBA.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                objBA.NGAYTAO = DateTime.Now;
                objBA.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_BIAN.Add(objBA);
                dt.SaveChanges();
                BiAnId = objBA.ID;
            }
            hddBiAnID.Value = BiAnId.ToString();
        }
        protected void cmdThemBiAn_Click(object sender, EventArgs e)
        {
            //txtTenVuAn.Text = "";
            //txtNgayAnCoHieuLuc.Text = txtNgayBanAn.Text = txtNgayXayra.Text = "";
            //txtSoBanAn.Text = "";
            //hddToaAnID.Value = "";
            //txtToaAn.Text = "";
            pnBiAn.Visible = true;
            //------xoa form thong tin nhap thong tin bi an------------
            txtMaBiCan.Text = txtTenBiCan.Text = txtTenKhac.Text = "";
            txtCMND.Text = txtNgaysinh.Text = txtNamSinh.Text = "";
            //txtHKTT.Text = txtHKTT_Chitiet.Text = "";
            //hddHKTTID.Value = "";

            //txtTamtru.Text = txtTamtru_Chitiet.Text = "";
            //hddTamtruID.Value = "";
            txtHoTenBo.Text = txtNamSinhBo.Text = "";
            txtHotenMe.Text = txtNamSinhMe.Text = "";
            txtTienAn.Text = txtTienAn.Text = "0";
            //txtNoiTamGiam.Text = "";
            txtToiDanh.Text = "";

            dropDanToc.SelectedIndex = dropNgheNghiep.SelectedIndex = -1;
            dropQuocTich.SelectedIndex = dropTinhChatVuAn.SelectedIndex = -1;
            dropTonGiao.SelectedIndex = dropTrinhDoVH.SelectedIndex = -1;
            dropTinhTrangGiamGiu.SelectedIndex = -1;
        }

        #region DS bi an theo vu an
        void LoadDSBiAnTheoVuAn()
        {
            Decimal THA_VuAnID = (string.IsNullOrEmpty(hddVuAnID.Value))?0:Convert.ToDecimal(hddVuAnID.Value);
            if (THA_VuAnID > 0)
            {
                int page_size = 20;
                int pageindex = Convert.ToInt32(hddPageIndex.Value);

                string textsearch = "";
                THA_BIAN_BL objBL = new THA_BIAN_BL();
                DataTable tbl = objBL.GetByVuAn_Paging(textsearch, THA_VuAnID, pageindex, page_size);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    int count_all = Convert.ToInt32(tbl.Rows[0]["CountAll"] + "");
                    #region "Xác định số lượng trang"
                    hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, page_size).ToString();
                    lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                    Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                                 lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                    #endregion

                    rpt.DataSource = tbl;
                    rpt.DataBind();
                    pnBiAn.Visible = false;
                    pnListBiAn.Visible = true;
                }
                else
                {
                    pnBiAn.Visible = true;
                    pnListBiAn.Visible = false;
                    //pndata.Visible = false;
                    //lbthongbao.Text = "Không tìm thấy dữ liệu phù hợp điều kiện!";
                }
            }
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                // rpt.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadDSBiAnTheoVuAn();
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = ex.Message; 
            }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {

                hddPageIndex.Value = "1";
                LoadDSBiAnTheoVuAn();
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = ex.Message; 
            }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                // rpt.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadDSBiAnTheoVuAn();
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = ex.Message; 
            }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                //  rpt.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadDSBiAnTheoVuAn();
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = ex.Message; 
            }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                // rpt.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
                hddPageIndex.Value = lbCurrent.Text;
                LoadDSBiAnTheoVuAn();
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = ex.Message; 
            }
        }
        #endregion
        void LoadThongTinBiAn(Decimal BiAnID)
        {
            pnBiAn.Visible = true;
            THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (obj != null )
            {
                txtMaBiCan.Text = obj.MABICAN;
                txtTenBiCan.Text = obj.HOTEN;
                txtTenKhac.Text = obj.TENKHAC;
                txtNgaysinh.Text =( obj.NGAYSINH!= null && obj.NGAYSINH != DateTime.MinValue) ? ((DateTime)obj.NGAYSINH).ToString("dd/MM/yyyy", cul) : "";
                txtNamSinh.Text = obj.NAMSINH + "";

                txtCMND.Text = obj.SOCMND;
                dropTrinhDoVH.SelectedValue = obj.TRINHDOVANHOAID + "";

                ddlGioitinh.SelectedValue = obj.GIOITINH + "";

                dropTonGiao.SelectedValue = obj.TONGIAOID + "";
                dropQuocTich.SelectedValue = obj.QUOCTICHID + "";

                //hddHKTTID.Value = obj.HKTT.ToString();
                //DM_HANHCHINH objHC = dt.DM_HANHCHINH.Where(x => x.ID == obj.HKTT).Single<DM_HANHCHINH>() ;
                //txtHKTT.Text = objHC.TEN;
                //txtHKTT_Chitiet.Text = obj.KHTTCHITIET + "";

                //hddTamtruID.Value = obj.TAMTRU.ToString();
                //objHC = dt.DM_HANHCHINH.Where(x => x.ID == obj.TAMTRU).Single<DM_HANHCHINH>(); ;
                //txtTamtru.Text = objHC.TEN;
                //txtTamtru_Chitiet.Text = obj.TAMTRUCHITIET + "";
                //-------------------------------------
                
                try
                {
                    if (obj.HKTT != null)
                        ddlHKTT_Tinh.SelectedValue = obj.HKTT + "";
                }catch(Exception ex) { }
                try
                {
                    LoadDropHuyenByTinh(ddlHKTT_Huyen, (decimal)obj.HKTT);
                    if (obj.HKTT_HUYEN != null)
                        ddlHKTT_Huyen.SelectedValue = obj.HKTT_HUYEN + "";
                    
                }catch (Exception ex) { }
                txtHKTT_Chitiet.Text = obj.KHTTCHITIET;
                //-------------------------------------
                try
                {
                    if (obj.TAMTRU != null)
                        ddlTamTru_Tinh.SelectedValue = obj.TAMTRU + "";
                }catch (Exception ex) { }
                try
                {
                    LoadDropHuyenByTinh(ddlTamTru_Huyen, (decimal)obj.TAMTRU);
                    if (obj.TAMTRU_HUYEN != null)
                        ddlTamTru_Huyen.SelectedValue = obj.TAMTRU_HUYEN + "";
                }catch (Exception ex) { }
                txtTamtru_Chitiet.Text = obj.TAMTRUCHITIET;

                //-------------------------------------
                try{dropDanToc.SelectedValue = obj.DANTOCID + ""; } catch (Exception ex) { }
                try{dropNgheNghiep.SelectedValue = obj.NGHENGHIEPID + ""; } catch (Exception ex) { }

                try{dropChucVuDang.SelectedValue = obj.CHUCVUDANGID + ""; } catch (Exception ex) { }
                try{dropChucVuCQ.SelectedValue = obj.CHUCVUCHINHQUYENID + ""; } catch (Exception ex) { }

                txtHoTenBo.Text = obj.HOTENBO + "";
                txtNamSinhBo.Text = obj.NAMSINHBO + "";
                txtHotenMe.Text = obj.HOTENME + "";
                txtNamSinhMe.Text = obj.NAMSINHME + "";

                rdNGhienHut.SelectedValue = obj.NGHIENHUT + "";
                rdTinhTrangTaiPham.SelectedValue = obj.TAIPHAM + "";
                txtTienAn.Text = obj.TIENAN + "";
                txtTienSu.Text = obj.TIENSU + "";

                try{ dropTinhTrangGiamGiu.SelectedValue = obj.TINHTRANGGIAMGIUID + ""; } catch (Exception ex) { }
                txtToiDanh.Text = obj.TOIDANH + "";
            }
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal curr_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "sua":
                    LoadThongTinBiAn(curr_id);
                    break;
                case "Choice":
                    //----------Chon bi an can cap nhat thong tin----------
                    //Lưu vào người dùng
                    decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
                    oNSD.IDTHA = curr_id;//luu thong tin id bi an vao
                    dt.SaveChanges();

                    Session[ENUM_LOAIAN.AN_THA] = curr_id;
                    Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    break;
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false)
                    {
                        lstMsgTop.Text = lstMsgBottom.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    xoa(curr_id);
                    break;
            }
        }
        public void xoa(decimal id)
        {
            THA_BIAN oT = dt.THA_BIAN.Where(x => x.ID == id).FirstOrDefault();
            dt.THA_BIAN.Remove(oT);
            dt.SaveChanges();

            hddPageIndex.Value = "1";
            LoadDSBiAnTheoVuAn();
            lstMsgTop.Text = lstMsgBottom.Text = "Xóa thành công!";
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);

                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
            }
        }
        #endregion
        
        //-------------------------------------
        protected void cmdQuaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("/QLAN/THA/Danhsach.aspx");
        }
    }
}