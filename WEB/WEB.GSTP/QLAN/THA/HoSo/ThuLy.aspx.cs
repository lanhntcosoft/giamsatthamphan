﻿using BL.GSTP;
using BL.GSTP.AHC;
using BL.GSTP.THA;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.THA.HoSo
{
    public partial class ThuLy : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        Decimal CurrUserID = 0, VuAnID = 0, BiAnID = 0;
        public string NgaySoSanh = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                txtMaThuLy.Enabled = txtDiaChi.Enabled = false;
                //dropBiAn.Enabled = false;
                if (!IsPostBack)
                {
                    BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                    hddBiAnID.Value = BiAnID.ToString();
                    CheckQuyen(BiAnID);
                    if (BiAnID > 0)
                    {
                        pn.Visible = true;
                        LoadCombobox();
                        LoadThongTinBiAn();
                        LoadInfoThuLy();
                    }
                    else
                    {
                        pn.Visible = false;
                        lbthongbao.Text = "Bạn cần chọn bị án để xử lý!";
                    }
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        void CheckQuyen(Decimal BiAnID)
        {
            try
            {
                THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
                if (obj != null)
                {
                    Decimal THA_VuAnID = (decimal)obj.VUANID;
                    THA_VUAN objVA = dt.THA_VUAN.Where(x => x.ID == THA_VuAnID).Single<THA_VUAN>();
                    NgaySoSanh = (objVA.BA_ST_NGAYHIEULUC == null || objVA.BA_ST_NGAYHIEULUC == DateTime.MinValue) ? "" : ((DateTime)objVA.BA_ST_NGAYHIEULUC).ToString("dd/MM/yyyy", cul);
                    // ((DateTime)objVA.BA_ST_NGAYHIEULUC).ToString("dd/MM/yyyy", cul);
                    txtNgayBanAnCoHieuLuc.Text = NgaySoSanh;
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message.ToString(); }
        }
        private void LoadCombobox()
        {
            dropTruongHopThuLy.Items.Clear();
            dropTruongHopThuLy.Items.Add(new ListItem("Thụ lý mới", "0"));
            dropTruongHopThuLy.Items.Add(new ListItem("Thụ lý mới do có ủy thác Thi hành án", "1"));
        }
        void LoadThongTinBiAn()
        {
            dropBiAn.Items.Clear();
            BiAnID = Convert.ToDecimal(hddBiAnID.Value);
            THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (obj != null)
            {
                VuAnID = (decimal)obj.VUANID;
                hddVuAnID.Value = VuAnID.ToString();
                dropBiAn.Items.Add(new ListItem(obj.HOTEN, obj.ID.ToString()));

                DM_HANHCHINH objHC = dt.DM_HANHCHINH.Where(x => x.ID == obj.HKTT).Single<DM_HANHCHINH>(); ;
                txtDiaChi.Text = objHC.TEN;
            }
        }

        //-----------------------------
        void LoadInfoThuLy()
        {
            BiAnID = Convert.ToDecimal(hddBiAnID.Value);
            try
            {
                THA_THULY obj = dt.THA_THULY.Where(x => x.BIANID == BiAnID).Single<THA_THULY>();
                if (obj != null)
                {
                    hddVuAnID.Value = obj.VUANID + "";
                    hddBiAnID.Value = obj.BIANID + "";
                    txtMaThuLy.Text = obj.MATHULY;
                    txtNgaythuly.Text = obj.NGAYTHULY != null ? ((DateTime)obj.NGAYTHULY).ToString("dd/MM/yyyy", cul) : "";
                    txtSoThuly.Text = obj.SOTHULY;

                    txtHanThang.Text = (string.IsNullOrEmpty(obj.THOIHAN_THANG + "")) ? "0" : obj.THOIHAN_THANG.ToString();
                    txtHanNgay.Text = (string.IsNullOrEmpty(obj.THOIHAN_NGAY + "")) ? "0" : obj.THOIHAN_NGAY.ToString();

                    String date_temp = (obj.THOIHANTUNGAY == null || obj.THOIHANTUNGAY == DateTime.MinValue) ? "" : ((DateTime)obj.THOIHANTUNGAY).ToString("dd/MM/yyyy", cul);
                    txtTuNgay.Text = date_temp;

                    date_temp = (obj.THOIHANDENNGAY == null || obj.THOIHANDENNGAY == DateTime.MinValue) ? "" : ((DateTime)obj.THOIHANDENNGAY).ToString("dd/MM/yyyy", cul);
                    txtDenNgay.Text = date_temp;

                    txtGhichu.Text = obj.GHICHU;
                }
            }
            catch (Exception ex) { }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            DateTime date_temp;
            Boolean IsUpdate = false;
            Decimal VuAnId = (String.IsNullOrEmpty(hddVuAnID.Value + "")) ? 0 : Convert.ToDecimal(hddVuAnID.Value);
            BiAnID = Convert.ToDecimal(dropBiAn.SelectedValue);// Convert.ToDecimal(hddBiAnID.Value);
            THA_THULY obj = new THA_THULY();
            try
            {
                if (BiAnID > 0)
                {
                    obj = dt.THA_THULY.Where(x => x.BIANID == BiAnID && x.VUANID == VuAnId).Single<THA_THULY>();
                    IsUpdate = true;
                }
                else
                    obj = new THA_THULY();
            }
            catch (Exception ex) { obj = new THA_THULY(); }

            obj.BIANID = BiAnID;
            obj.VUANID = VuAnId;

            obj.TRUONGHOPTHULY = Convert.ToDecimal(dropTruongHopThuLy.SelectedValue);
            obj.SOTHULY = txtSoThuly.Text.Trim();

            //------------------------------------------
            date_temp = (String.IsNullOrEmpty(txtNgaythuly.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaythuly.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYTHULY = date_temp;
            //------------------------
            obj.THOIHAN_NGAY = (String.IsNullOrEmpty(txtHanNgay.Text.Trim())) ? 0 : Convert.ToDecimal(txtHanNgay.Text.Trim());
            obj.THOIHAN_THANG = (String.IsNullOrEmpty(txtHanThang.Text.Trim())) ? 0 : Convert.ToDecimal(txtHanThang.Text.Trim());

            date_temp = (String.IsNullOrEmpty(txtTuNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.THOIHANTUNGAY = date_temp;
            date_temp = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.THOIHANDENNGAY = date_temp;
            //--------------------------------
            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");

                THA_THULY_BL objBL = new THA_THULY_BL();
                obj.MATHULY = objBL.GetNewThuTu(ToaAnID) + "";
                obj.TOAANID = ToaAnID;

                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_THULY.Add(obj);
                dt.SaveChanges();
                BiAnID = obj.ID;
            }
            hddBiAnID.Value = BiAnID.ToString();
            lbthongbao.Text = "Đã cập nhật xong thụ lý cho bị án " + dropBiAn.SelectedItem.Text;
        }

        protected void btnLammoi_Click(object sender, EventArgs e)
        {
            txtMaThuLy.Text = txtNgaythuly.Text = txtSoThuly.Text = "";
            txtDiaChi.Text = txtGhichu.Text = "";
            txtHanNgay.Text = txtHanThang.Text = "";
            txtDenNgay.Text = txtTuNgay.Text = "";
        }
    }
}
