﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master"
    AutoEventWireup="true" CodeBehind="NhanUyThacTHA_Edit.aspx.cs"
    Inherits="WEB.GSTP.QLAN.THA.NhanUyThacTHA_Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../UI/js/Common.js"></script>

    <!----------------------------------------------------->
    <asp:HiddenField ID="HddID" runat="server" Value="0" />
    <asp:HiddenField ID="Hddindex" runat="server" Value="1" />
    <asp:HiddenField ID="HddPage" runat="server" Value="20" />
    <asp:HiddenField ID="hddLoaiUyThac" runat="server" Value="0" />
    <!----------------------------------------------------->
    <div style="margin-left: 1%; width: 98%; float: left;">
        <div style="margin: 15px 1%; text-align: center; width: 98%; color: red;">
            <asp:Literal ID="lstMsgT" runat="server"></asp:Literal>
        </div>
        <div class="boxchung">
            <h4 class="tleboxchung">Thông tin ủy thác thi hành án</h4>
            <div class="boder" style="padding: 5px 10px;">
                <table class="table1">
                    <tr>
                        <td class="cell_label">Mã vụ án</td>
                        <td>
                            <asp:Literal ID="lttMaVuAn" runat="server"></asp:Literal>
                        </td>
                        <td class="cell_label">Tên vụ án</td>
                        <td>
                            <asp:Literal ID="lttTenVuAn" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="cell_label">Mã bị án</td>
                        <td>
                            <asp:Literal ID="lttMaBiAn" runat="server"></asp:Literal>
                        </td>
                        <td class="cell_label">Tên bị án</td>
                        <td>
                            <asp:Literal ID="lttTenBiAn" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="cell_label">Quyết định ủy thác THA</td>
                        <td colspan="3">
                            <asp:Literal ID="lttQDUyThac" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="cell_label">Tòa án ủy thác<span class="batbuoc">(*)</span></td>
                        <td>
                            <asp:Literal ID="lttToaUyThac" runat="server"></asp:Literal>
                        </td>
                        <td class="cell_label">Người nhập<span class="batbuoc">(*)</span> </td>
                        <td>
                            <asp:Literal ID="lttNguoiNhapUyThac" runat="server"></asp:Literal>
                        </td>
                    </tr>

                    <!---------------------------------------------->
                    <tr>
                        <td class="cell_label">Ngày ủy thác<span class="batbuoc">(*)</span>
                        </td>
                        <td colspan="3">
                            <asp:Literal ID="lttNgayUyThac" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
            <h4 class="tleboxchung" style="margin-top:10px;">Xử lý ủy thác thi hành án</h4>
            <div class="boder" style="padding: 5px 10px;">
                <table class="table1">
                    <tr>
                        <td style="width: 120px">Ngày nhận<span class="batbuoc">(*)</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNgayNhan" runat="server" Width="222px" Enabled="false"
                                CssClass="user"></asp:TextBox>
                            <%-- <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtNgayNhan" Format="dd/MM/yyyy" Enabled="true" />
                            <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtNgayNhan" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                            <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="txtNgayNhan" InvalidValueMessage="dd/MM/yyyy" Style="color: red; margin-left: 15px;"></cc1:MaskedEditValidator>
                            --%>  </td>
                        <td>Người ký <span class="batbuoc">(*)</span> </td>
                        <td>
                            <asp:DropDownList ID="dropNguoiKy" runat="server" Enabled="false"
                                CssClass="chosen-select" Width="230px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Ghi chú</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtGhichu" runat="server"
                                Width="611px" CssClass="user"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding-top: 10px; text-align: center;">
                            <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput"
                                Text="Lưu" OnClientClick="return validate();" OnClick="cmdUpdate_Click" />
                            <asp:Button ID="cmdResert" runat="server" CssClass="buttoninput"
                                Text="Làm mới" OnClick="cmdResert_Click" /></td>
                    </tr>
                </table>
                <div style="padding-top: 10px; text-align: center; width: 95%; float: left; margin-left: 2%; color: red; font-weight: bold;">
                    <asp:Literal ID="lbthongbao" runat="server"></asp:Literal>
                </div>

            </div>
        </div>

    </div>
    <script>
        function validate() {
           <%-- var value_changed = "";
            var dropQuyetDinhUyThacTHA = document.getElementById('<%=dropQuyetDinhUyThacTHA.ClientID%>');
            value_changed = dropQuyetDinhUyThacTHA.options[dropQuyetDinhUyThacTHA.selectedIndex].value;
            if (value_changed == "0") {
                alert('Bạn chưa chọn Quyết định ủy thác thi hành án. Hãy kiểm tra lại!');
                dropQuyetDinhUyThacTHA.focus();
                return false;
            }
            //-------------------------
            var hddLoaiUyThac = document.getElementById('<%=hddLoaiUyThac.ClientID%>');
            if (hddLoaiUyThac.value == "0") {
                var dropLyDo = document.getElementById('<%=dropLyDo.ClientID%>');
                value_changed = dropLyDo.options[dropLyDo.selectedIndex].value;
                if (value_changed == "0") {
                    alert('Bạn chưa chọn lý đo. Hãy kiểm tra lại!');
                    dropLyDo.focus();
                    return false;
                }
            }
            //---------------------------------
            var txtNgayUyThac = document.getElementById('<%=txtNgayUyThac.ClientID%>')
            if (!CheckDateTimeControl(txtNgayUyThac, 'Ngày ủy thác'))
                return false;
            //----------
            var txtNgayNhan = document.getElementById('<%=txtNgayNhan.ClientID%>')
            if (Common_CheckEmpty(txtNgayNhan.value)) {
                if (!CheckDateTimeControl(txtNgayNhan, 'Ngày nhận'))
                    return false;

                if (!SoSanhDate(txtNgayNhan, txtNgayUyThac)) {
                    alert('Xin vui lòng kiểm tra lại. "Ngày nhận" không thể nhỏ hơn "Ngày ủy thác" !');
                    txtNgayNhan.focus();
                    return false;
                }
            }
            //---------------------------------           
            var dropNguoiNhap = document.getElementById('<%=dropNguoiNhap.ClientID%>');
            var value_change = dropNguoiNhap.options[dropNguoiNhap.selectedIndex].value;
            if (value_change == "0") {
                alert('Bạn chưa chọn người nhập. Hãy kiểm tra lại!');
                dropNguoiNhap.focus();
                return false;
            }

            var txtGhichu = document.getElementById('<%=txtGhichu.ClientID%>')
            if (Common_CheckEmpty(txtGhichu.value)) {
                var soluongkytu = 2000;
                if (!Common_CheckLengthString(txtGhichu, soluongkytu)) {
                    alert("Mục 'Ghi chú' không thể nhập quá " + soluong + " ký tự. Hãy kiểm tra lại!");
                    txtGhichu.focus();
                    return false;
                }
            }--%>

            return true;
        }
    </script>
    <script>
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>

