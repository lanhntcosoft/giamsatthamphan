﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="QuyetDinh.aspx.cs" Inherits="WEB.GSTP.QLAN.THA.CongVanTHA.QuyetDinh" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../UI/js/Common.js"></script>
    <style>
        .boxchung {
            float: left;
            width: 99%;
            margin-left: 0;
        }

        .title_ds {
            float: left;
            width: 100%;
            background: #fff none repeat scroll 0 0;
            white-space: nowrap;
            margin-bottom: 5px;
            text-transform: uppercase;
        }
    </style>
    <asp:HiddenField ID="hddCurrThuLyID" runat="server" />
    <div class="box">
        <div class="box_nd">
            <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                <asp:Literal ID="lstMsgTop" runat="server"></asp:Literal>
            </div>
            <div class="content_form">

                <!---------------------------------------->
                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">
                        <asp:Literal ID="lttGroup" runat="server" Text="Quyết định"></asp:Literal></h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <table class="table1">

                            <tr>
                                <td style="width: 120px;">Yêu cầu</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtYeuCau" CssClass="user" Enabled="false"
                                        runat="server" Width="568px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 100px;">Lý do</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtLyDo" CssClass="user" Enabled="false"
                                        runat="server" Width="568px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 120px;">QĐ/TB</td>
                                <td colspan="3">
                                    <asp:DropDownList ID="dropQuyetDinh_TB" runat="server" CssClass="chosen-select"
                                        Width="577px">
                                    </asp:DropDownList>
                                    <%--  <asp:TextBox ID="txtQD" CssClass="user" Enabled="false"
                                        runat="server" Width="98%"></asp:TextBox>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>Số QĐ/TB<span class="batbuoc">(*)</span></td>
                                <td style="width: 310px;">
                                    <asp:TextBox ID="txtSoQD" CssClass="user"
                                        runat="server" Width="200px"></asp:TextBox>

                                </td>
                                <td style="width: 100px;">Ngày ra QĐ/TB<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtNgayQD" runat="server" CssClass="user"
                                        placeholder="ngày/tháng/năm" AutoPostBack="True" OnTextChanged="txtNgayQD_TextChanged"
                                        Width="140px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                        TargetControlID="txtNgayQD" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server"
                                        TargetControlID="txtNgayQD" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN"
                                        ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-bottom: dotted 1px #dcdcdc;"></td>
                            </tr>
                            <tr>
                                <td>Hiệu lực từ ngày<span class="batbuoc">(*)</span>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtHieuLuc_TuNgay" runat="server" Width="200px" CssClass="user" 
                                        placeholder="ngày/tháng/năm" 
                                        AutoPostBack="True" OnTextChanged="txtHieuLuc_TuNgay_TextChanged"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtHieuLuc_TuNgay" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtHieuLuc_TuNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="txtHieuLuc_TuNgay" InvalidValueMessage="dd/MM/yyyy" Style="color: red; margin-left: 15px;"></cc1:MaskedEditValidator>
                                </td>
                            </tr>
                            <%------%>
                            <tr>
                                <td>Thời hạn theo luật định                          
                                </td>
                                <td>
                                    <asp:TextBox ID="txtThang_TheoLuat" runat="server"
                                        Width="50px" CssClass="user align_right"
                                        onkeypress=" return isNumber(event)"
                                        AutoPostBack="true" OnTextChanged="txtThang_TheoLuat_TextChanged"></asp:TextBox>
                                    Tháng
                            &nbsp;
                            <asp:TextBox ID="txtNgay_TheoLuat" runat="server" Width="50px" CssClass="user align_right" Text="15" AutoPostBack="true" onkeypress="return isNumber(event)" OnTextChanged="txtNgay_TheoLuat_TextChanged"></asp:TextBox>
                                    Ngày
                                </td>
                                <td>Ngày kết thúc theo luật định<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtKetThucTheoLuat" runat="server"
                                        Width="140px" CssClass="user" placeholder="ngày/tháng/năm"
                                        AutoPostBack="true" OnTextChanged="txtKetThucTheoLuat_TextChanged"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtNgayQuyetDinh_CalendarExtender" runat="server" TargetControlID="txtKetThucTheoLuat" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtKetThucTheoLuat" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="txtKetThucTheoLuat" InvalidValueMessage="dd/MM/yyyy" Style="color: red; margin-left: 15px;"></cc1:MaskedEditValidator>
                                </td>
                            </tr>
                            <%------%>
                            <tr>
                                <td>Thời hạn thực tế                           
                                </td>
                                <td>
                                    <asp:TextBox ID="txtThangThucTe" runat="server" Width="50px" CssClass="user align_right" AutoPostBack="true"
                                        onkeypress="return isNumber(event)" MaxLength="2" OnTextChanged="txtThangThucTe_TextChanged"></asp:TextBox>
                                    Tháng
                            &nbsp;
                            <asp:TextBox ID="txtNgayThucTe" runat="server" Width="50px" CssClass="user align_right" Text="15" AutoPostBack=" true"
                                onkeypress="return isNumber(event)" MaxLength="2" OnTextChanged="txtNgayThucTe_TextChanged"></asp:TextBox>
                                    Ngày
                                </td>
                                <td>Hiệu lực đến ngày<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtNgayHieuLuc_DenNgay" runat="server" Width="140px"
                                        placeholder="ngày/tháng/năm" CssClass="user"
                                        AutoPostBack="true" OnTextChanged="txtNgayHieuLuc_DenNgay_TextChanged"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtNgayHieuLuc_DenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="txtNgayHieuLuc_DenNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="txtNgayHieuLuc_DenNgay" InvalidValueMessage="dd/MM/yyyy" Style="color: red; margin-left: 15px;"></cc1:MaskedEditValidator>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="4" style="border-bottom: dotted 1px #dcdcdc;"></td>
                            </tr>

                            <tr>
                                <td>Người kí</td>
                                <td>
                                    <asp:HiddenField ID="hddLoadID" runat="server" />
                                    <asp:DropDownList ID="ddlNguoiki" runat="server" CssClass="chosen-select"
                                        Width="200px" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlNguoiki_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>Chức vụ</td>
                                <td>
                                    <asp:TextBox ID="txtChucVu" runat="server" Width="140px" CssClass="user" disabled="disabled"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Kết quả<span class="batbuoc">(*)</span></td>
                                <td colspan="3">
                                    <asp:RadioButtonList ID="rdKetQua" runat="server"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="0">Không chấp nhận</asp:ListItem>
                                        <asp:ListItem Value="1">Chấp nhận</asp:ListItem>
                                        <asp:ListItem Value="2">Chấp nhận một phần</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>

                            <tr>
                                <td>Ghi chú</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtGhiChu" CssClass="user"
                                        runat="server" Width="568px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Đính kèm tệp</td>
                                <td colspan="3">
                                    <asp:FileUpload ID="file" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3">
                                    <div style="margin: 5px; text-align: center; width: 95%">
                                        <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput" Text="Lưu"
                                            OnClientClick="return validate();" OnClick="cmdUpdate_Click" />
                                        <asp:Button ID="cmdBack" runat="server" CssClass="buttoninput"
                                            Text="Quay lại" OnClick="cmdBack_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
        function validate() {
            var NgaySoSanh;
            var dropQuyetDinh_TB = document.getElementById('<%=dropQuyetDinh_TB.ClientID%>');
            value_change = dropQuyetDinh_TB.options[dropQuyetDinh_TB.selectedIndex].value;
            if (value_change == "0") {
                alert('Bạn chưa chọn quyết định/thông báo. Hãy kiểm tra lại!');
                dropQuyetDinh_TB.focus();
                return false;
            }
            //-----------------------
            var txtSoQD = document.getElementById('<%=txtSoQD.ClientID%>');
            if (!Common_CheckTextBox(txtSoQD, "số QĐ/TB"))
                return false;

            var txtNgayQD = document.getElementById('<%=txtNgayQD.ClientID%>');
            if (!CheckDateTimeControl(txtNgayQD, 'Ngày ra QĐ/TB'))
                return false;

            //--------------
            var txtHieuLuc_TuNgay = document.getElementById('<%=txtHieuLuc_TuNgay.ClientID%>');
            if (!CheckDateTimeControl(txtHieuLuc_TuNgay, 'mục "Hiệu lực từ ngày"'))
                return false;
            var NgaySoSanh = txtNgayRaQD.value;
            if (!SoSanh2Date(txtHieuLuc_TuNgay, 'mục "Hiệu lực từ ngày"', NgaySoSanh, 'mục "Ngày ra quyết đinh/TB"')) {
                txtHieuLuc_TuNgay.focus();
                return false;
            }

            //--------------
            var txtKetThucTheoLuat = document.getElementById('<%=txtKetThucTheoLuat.ClientID%>');
            if (!Common_CheckEmpty(txtKetThucTheoLuat.value)) {
                alert('Mục "Ngày kết thúc theo luật định" không được bỏ trống');
                txtKetThucTheoLuat.focus();
                return false;
            }

            NgaySoSanh = txtHieuLuc_TuNgay.value;
            if (!SoSanh2Date(txtKetThucTheoLuat, 'Ngày kết thúc theo luật định', NgaySoSanh, 'mục "Hiệu lực từ ngày"')) {
                txtKetThucTheoLuat.focus();
                return false;
            }
            //---------------------------
            var txtNgayHieuLuc_DenNgay = document.getElementById('<%=txtNgayHieuLuc_DenNgay.ClientID%>');
            if (!Common_CheckEmpty(txtNgayHieuLuc_DenNgay.value)) {
                alert('Mục "Hiệu lực đến ngày" không được bỏ trống');
                txtNgayHieuLuc_DenNgay.focus();
                return false;
            }

            NgaySoSanh = txtHieuLuc_TuNgay.value;
            if (!SoSanh2Date(txtNgayHieuLuc_DenNgay, 'Mục "Hiệu lực đến ngày"', NgaySoSanh, 'mục "Hiệu lực từ ngày"')) {
                txtNgayHieuLuc_DenNgay.focus();
                return false;
            }
            //----------------------------------
            var ddlNguoiki = document.getElementById('<%=ddlNguoiki.ClientID%>');
            value_change = ddlNguoiki.options[ddlNguoiki.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn người ký. Hãy kiểm tra lại!');
                ddlNguoiki.focus();
                return false;
            }
            return true;
        }
        function set_ngayhieuluc() {

        }
    </script>
</asp:Content>
