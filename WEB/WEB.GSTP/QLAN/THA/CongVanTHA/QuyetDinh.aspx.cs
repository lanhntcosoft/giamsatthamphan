﻿using BL.GSTP;
using BL.GSTP.AHC;
using BL.GSTP.THA;
using BL.GSTP.Danhmuc;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BL.GSTP.AHS;

namespace WEB.GSTP.QLAN.THA.CongVanTHA
{
    public partial class QuyetDinh : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        Decimal CurrUserID = 0, VuAnID = 0, BiAnID = 0;
        public String NgaySoSanh = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                if (!IsPostBack)
                {
                    THA_BIAN objBA = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
                    if (objBA != null)
                        VuAnID = (decimal)objBA.VUANID;
                    LoadDrop();
                    Decimal ThuLyCV_ID = String.IsNullOrEmpty(Request["tID"] + "") ? 0 : Convert.ToDecimal(Request["tID"] + "");
                    if (ThuLyCV_ID == 0)
                    {
                        lstMsgTop.Text = "Bạn chưa thực hiện thụ lý công văn/đơn. Đề nghị thực hiện 'Thụ lý công văn/đơn' trước khi thực hiện việc này";
                        cmdUpdate.Visible = false;
                        return;
                    }

                    hddCurrThuLyID.Value = ThuLyCV_ID.ToString();
                    if (ThuLyCV_ID > 0)                       
                        LoadInfo(ThuLyCV_ID);
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
       
        void LoadDrop()
        {
            LoadDropByGroupName(dropQuyetDinh_TB, ENUM_DANHMUC.QUYETDINH_TB_THA, true);

            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            Decimal donvi = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DataTable oCBDT = oDMCBBL.GetAllChanhAn_PhoCA(donvi);

            ddlNguoiki.DataSource = oCBDT;
            ddlNguoiki.DataTextField = "HOTEN";
            ddlNguoiki.DataValueField = "ID";
            ddlNguoiki.DataBind();
            ddlNguoiki.Items.Insert(0, new ListItem("-----Chọn-----", "0"));
        }
        void LoadDropByGroupName(DropDownList drop, string GroupName, Boolean ShowChangeAll)
        {
            drop.Items.Clear();
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(GroupName);
            if (ShowChangeAll)
                drop.Items.Add(new ListItem("------- Chọn --------", "0"));
            foreach (DataRow row in tbl.Rows)
            {
                drop.Items.Add(new ListItem(row["TEN"] + "", row["ID"] + ""));
            }
        }

        protected void ddlNguoiki_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlNguoiki.SelectedValue != "0")
            {
                try
                {
                    Decimal CanBoID = Convert.ToDecimal(ddlNguoiki.SelectedValue);
                    Decimal ChucVuID = (Decimal)dt.DM_CANBO.Where(x => x.ID == CanBoID).SingleOrDefault().CHUCVUID;
                 
                    txtChucVu.Text = dt.DM_DATAITEM.Where(x => x.ID == ChucVuID).SingleOrDefault().TEN;
                }
                catch (Exception ex)
                {
                    //lstMsgTop.Text = ex.Message;
                }
            }
        }
        private void ResertControll()
        {           
            txtSoQD.Text = txtNgayQD.Text = "";
            txtNgayThucTe.Text =   txtThangThucTe.Text = "";
            txtNgay_TheoLuat.Text =  txtThang_TheoLuat.Text = "";
            txtHieuLuc_TuNgay.Text = txtKetThucTheoLuat.Text = txtNgayHieuLuc_DenNgay.Text = "";

            ddlNguoiki.SelectedValue = "0";
            txtChucVu.Text = "";
            rdKetQua.SelectedValue = "0";
            txtGhiChu.Text = "";
        }
        protected void cmdResert_Click(object sender, EventArgs e)
        {
            ResertControll();
        }
        //--------------------------------------
        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetDinh_DS.aspx");
        }
        #region Update data
        private void GetDataToUpdate(THA_CVDON_QD obj)
        {
            Decimal ThuLyCV_ID = String.IsNullOrEmpty(hddCurrThuLyID.Value) ? 0 : Convert.ToDecimal(hddCurrThuLyID.Value);

            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            if (VuAnID == 0)
            {
                THA_BIAN oT = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
                if (oT != null)
                    VuAnID = (decimal)oT.VUANID;
            }
            
            //--------------------------------           
            obj.BIANID = BiAnID;
            obj.VUANID = VuAnID;
            obj.CVDONID = ThuLyCV_ID;
            obj.QUYETDINHID= Convert.ToDecimal(dropQuyetDinh_TB.SelectedValue);

            //-------------------
            obj.SOQD = txtSoQD.Text;
            obj.NGAYQD = (String.IsNullOrEmpty(txtNgayQD.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //----------Ngày quyết đinh/hiệu lực từ ngày/den ngay-------------
            obj.HIEULUC_TUNGAY = (String.IsNullOrEmpty(txtHieuLuc_TuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //---------Tinh theo luật-------
            obj.THEOLUAT_SONGAYHIEULUC = String.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim()) ? 0 : Convert.ToDecimal(txtNgay_TheoLuat.Text);
            obj.THEOLUAT_SOTHANGHIEULUC = String.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim()) ? 0 : Convert.ToDecimal(txtThang_TheoLuat.Text);
            obj.THEOLUAT_DENNGAY = (String.IsNullOrEmpty(txtKetThucTheoLuat.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtKetThucTheoLuat.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //------Tinh theo thuc te------- 
            obj.THUCTE_SONGAYHIEULUC = String.IsNullOrEmpty(txtNgayThucTe.Text.Trim()) ? 0 : Convert.ToDecimal(txtNgayThucTe.Text);
            obj.THUCTE_SOTHANGHIEULUC = String.IsNullOrEmpty(txtThangThucTe.Text.Trim()) ? 0 : Convert.ToDecimal(txtThangThucTe.Text);

            obj.HIEULUC_DENNGAY = (String.IsNullOrEmpty(txtNgayHieuLuc_DenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayHieuLuc_DenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //-----------------------------
            obj.NGUOIKY = ddlNguoiki.SelectedValue + "";
            obj.CHUCVU = txtChucVu.Text.Trim();

            obj.KETQUA = Convert.ToDecimal(rdKetQua.SelectedValue);
            obj.GHICHU = txtGhiChu.Text;
        }

        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            Decimal ThuLyCV_ID = String.IsNullOrEmpty(hddCurrThuLyID.Value) ? 0 : Convert.ToDecimal(hddCurrThuLyID.Value);

            if (ThuLyCV_ID==0)
            {
                lstMsgTop.Text = "Bạn chưa thực hiện thụ lý công văn/đơn. Đề nghị thực hiện 'Thụ lý công văn/đơn' trước khi thực hiện việc này";
                cmdUpdate.Visible = false;
                return;
            }

            THA_CVDON_QD obj = dt.THA_CVDON_QD.Where(x => x.BIANID == BiAnID 
                                                       && x.CVDONID ==ThuLyCV_ID ).FirstOrDefault();
            if (obj != null)
            {
                GetDataToUpdate(obj);
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            }
            else
            {
                obj = new THA_CVDON_QD();
                GetDataToUpdate(obj);
                obj.NGAYTAO = DateTime.Now;                
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_CVDON_QD.Add(obj);
            }
            dt.SaveChanges();
            lstMsgTop.Text = "Lưu thành công!";
        }

        #endregion
        
        protected void txtNgayQD_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgayQD.Text))
            {
                txtHieuLuc_TuNgay.Text = txtNgayQD.Text;
                DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                
                //-------------------
                int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
                int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
                DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
                txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
                //-------------------
                int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
                int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
                DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
                txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
            }
        }

        protected void txtHieuLuc_TuNgay_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------

            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }

        //------------------------
        protected void txtThang_TheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgay_TheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
        }
        protected void txtKetThucTheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime NgayKetThuc_TheoLuat = DateTime.Parse(this.txtKetThucTheoLuat.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            double songay = (NgayKetThuc_TheoLuat - NgayHieuLuc_BD).TotalDays;
            txtThang_TheoLuat.Text = "";
            txtNgay_TheoLuat.Text = songay.ToString();
        }

        //-------------------------
        protected void txtThangThucTe_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgayThucTe_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgayHieuLuc_DenNgay_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime NgayKetThuc_ThucTe = DateTime.Parse(this.txtNgayHieuLuc_DenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            double songay = (NgayKetThuc_ThucTe - NgayHieuLuc_BD).TotalDays;
            txtThangThucTe.Text = "";
            txtNgayThucTe.Text = songay.ToString();
        }

        //--------------------------------------
        void LoadInfo(Decimal ThuLyCV_ID)
        {
            THA_CVDON_THULY objThuLy = dt.THA_CVDON_THULY.Where(x => x.ID == ThuLyCV_ID).Single<THA_CVDON_THULY>();
            if (objThuLy != null)
            {
                Decimal YeuCauID = (Decimal)objThuLy.YEUCAUID;
                Decimal LyDoId = (Decimal)objThuLy.LYDOID;
                
                try
                {
                    DM_DATAITEM objItem = dt.DM_DATAITEM.Where(x => x.ID == YeuCauID).Single<DM_DATAITEM>();
                    txtYeuCau.Text = objItem.TEN;

                    objItem = dt.DM_DATAITEM.Where(x => x.ID == LyDoId).Single<DM_DATAITEM>();
                    txtLyDo.Text = objItem.TEN;

                    //objItem.
                  
                }
                catch (Exception ex) { }
                //if (objThuLy.LOAIDON == 0)
                //    NgaySoSanh = (String.IsNullOrEmpty(objThuLy.CV_NGAY + "")) ? "" : ((DateTime)objThuLy.CV_NGAY).ToString("dd/MM/yyyy", cul);
                //else
                //    NgaySoSanh = (String.IsNullOrEmpty(objThuLy.NGAYTHULY + "")) ? "" : ((DateTime)objThuLy.NGAYTHULY).ToString("dd/MM/yyyy", cul);
            }
            //----------------------
            THA_CVDON_QD obj = dt.THA_CVDON_QD.Where(x => x.CVDONID == ThuLyCV_ID && x.BIANID == BiAnID).SingleOrDefault<THA_CVDON_QD>();
            if (obj != null)
            {               
                txtSoQD.Text = obj.SOQD;
                txtNgayQD.Text = (((DateTime)obj.NGAYQD) == DateTime.MinValue) ? "" : ((DateTime)obj.NGAYQD).ToString("dd/MM/yyyy", cul);
                dropQuyetDinh_TB.SelectedValue  = obj.QUYETDINHID.ToString();
                dropQuyetDinh_TB.Enabled = false;
                //-------------------------------------
                txtHieuLuc_TuNgay.Text = (((DateTime)obj.HIEULUC_TUNGAY) == DateTime.MinValue) ? "" : ((DateTime)obj.HIEULUC_TUNGAY).ToString("dd/MM/yyyy", cul);

                txtNgay_TheoLuat.Text = (string.IsNullOrEmpty(obj.THEOLUAT_SONGAYHIEULUC+"") || obj.THEOLUAT_SONGAYHIEULUC == 0) ? "" : obj.THEOLUAT_SONGAYHIEULUC.ToString();
                txtThang_TheoLuat.Text = (string.IsNullOrEmpty(obj.THEOLUAT_SOTHANGHIEULUC+"") || obj.THEOLUAT_SOTHANGHIEULUC == 0) ? "" : obj.THEOLUAT_SOTHANGHIEULUC.ToString();
                txtKetThucTheoLuat.Text = (((DateTime)obj.THEOLUAT_DENNGAY) == DateTime.MinValue) ? "" : ((DateTime)obj.THEOLUAT_DENNGAY).ToString("dd/MM/yyyy", cul);

                txtNgayThucTe.Text = (string.IsNullOrEmpty(obj.THUCTE_SONGAYHIEULUC + "") || obj.THUCTE_SONGAYHIEULUC == 0) ? "" : obj.THUCTE_SONGAYHIEULUC.ToString();
                txtThangThucTe.Text = (string.IsNullOrEmpty(obj.THUCTE_SOTHANGHIEULUC + "") || obj.THUCTE_SOTHANGHIEULUC == 0) ? "" : obj.THUCTE_SOTHANGHIEULUC.ToString();
                txtNgayHieuLuc_DenNgay.Text = (((DateTime)obj.HIEULUC_DENNGAY) == DateTime.MinValue) ? "" : ((DateTime)obj.HIEULUC_DENNGAY).ToString("dd/MM/yyyy", cul);
                
                //-------------------------------------
                rdKetQua.SelectedValue = obj.KETQUA.ToString();

                try { ddlNguoiki.SelectedValue = obj.NGUOIKY.ToString(); } catch (Exception ex) { }
                txtChucVu.Text = obj.CHUCVU;                
                txtGhiChu.Text = obj.GHICHU;
            }
        }
   
       
    }
}