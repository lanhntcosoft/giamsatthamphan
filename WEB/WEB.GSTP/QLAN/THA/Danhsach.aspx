﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master"
    AutoEventWireup="true" CodeBehind="Danhsach.aspx.cs" Inherits="WEB.GSTP.QLAN.THA.Danhsach" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <div class="box">
        <div class="box_nd">
            <div class="truong">
                <table class="table1">
                    <tr>
                        <td colspan="2">
                            <div class="boxchung">
                                <h4 class="tleboxchung">Tìm kiếm</h4>
                                <div class="boder" style="padding: 10px;">
                                    <table class="table1">
                                        <tr>
                                            <td style="width: 75px;">Lựa chọn</td>
                                            <td style="width: 200px;">
                                                <asp:DropDownList ID="dropLoaiLuaChon" CssClass="chosen-select"
                                                    Width="180px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropLoaiLuaChon_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Mã vụ án</td>
                                            <td>
                                                <asp:TextBox ID="txtMaVuAn" CssClass="user"
                                                    runat="server" Width="170px" MaxLength="50"></asp:TextBox></td>
                                            <td style="width: 80px;">Tên vụ án</td>
                                            <td>
                                                <asp:TextBox ID="txtTenVuAn" CssClass="user" runat="server" Width="170px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mã bị án</td>
                                            <td>
                                                <asp:TextBox ID="txtMaBiAn" CssClass="user"
                                                    runat="server" Width="170px" MaxLength="50"></asp:TextBox></td>
                                            <td>Tên bị án</td>
                                            <td>
                                                <div style="float: left;">
                                                    <asp:TextBox ID="txtTenBiAn" CssClass="user" runat="server" Width="170px"></asp:TextBox>
                                                </div>
                                                <div style="float: left; margin-left: 10px;">
                                                    <span style="float: left; line-height: 25px; margin-right: 5px;">Số CMND</span>
                                                    <asp:TextBox ID="txtCMND" CssClass="user"
                                                        runat="server" Width="170px" MaxLength="50"></asp:TextBox>
                                                </div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Số bản án</td>
                                            <td>
                                                <asp:TextBox ID="txtSoBanAn" CssClass="user" runat="server"
                                                    Width="170px" MaxLength="50"></asp:TextBox></td>
                                            <td>Ngày bản án</td>
                                            <td>
                                                <asp:TextBox ID="txtNgayBanAn" runat="server" CssClass="user" Width="100px" MaxLength="10"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server"
                                                    TargetControlID="txtNgayBanAn" Format="dd/MM/yyyy" Enabled="true" />
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                                                    TargetControlID="txtNgayBanAn" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN"
                                                    ErrorTooltipEnabled="true" />
                                               <%-- <div style="float: left; margin-left: 10px;">
                                                    <span style="float: left; line-height: 25px; margin-right: 5px;">Trạng thái thụ lý</span>
                                                   <asp:DropDownList ID="dropTrangThaiThuLyTHA" CssClass="chosen-select"
                                                    Width="180px" runat="server" >
                                                       <asp:ListItem Text="Tất cả" Value="2"></asp:ListItem>
                                                       <asp:ListItem Text="Chưa thụ lý" Value="0"></asp:ListItem>
                                                       <asp:ListItem Text="Đã thụ lý" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                                </div>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px;" align="left"></td>
                        <td align="left">
                            <asp:Button ID="cmdTimkiem" runat="server" CssClass="buttoninput" Text="Tìm kiếm" OnClick="lbtimkiem_Click" />
                            <asp:Button ID="cmdThemmoi" runat="server" CssClass="buttoninput" Text="Thêm mới" OnClick="btnThemmoi_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Label runat="server" ID="lbtthongbao" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <div class="phantrang">
                                <div class="sobanghi">
                                    <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                                </div>
                                <div class="sotrang">
                                    <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back" Visible="false"
                                        OnClick="lbTBack_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active" Visible="false"
                                        Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                    <asp:Label ID="lbTStep1" runat="server" Text="..." Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:Label ID="lbTStep6" runat="server" Text="..." Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next" Visible="false"
                                        OnClick="lbTNext_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <!--------------------------------------->
                            <asp:HiddenField ID="hddBiAnID" runat="server" />
                            <table class="table2" style="width: 100%;" border="1">
                                <tr class="header">
                                    <td style="width: 15px; text-align: center">STT</td>
                                    <td style="width: 85px; text-align: center">Chọn bị án</td>
                                    <td style="width: 70px; text-align: center">Mã bị án</td>
                                    <td style="text-align: center">Bị án</td>
                                    <td style="width: 70px; text-align: center">Mã vụ án</td>
                                    <td style="text-align: center">Tên vụ án</td>
                                    <td style="width: 85px; text-align: center">Số bản án</td>
                                    <td style="width: 85px; text-align: center">Ngày bản án</td>
                                    <td style="width: 80px; text-align: center">Thao tác</td>
                                </tr>
                                <asp:Repeater ID="rpt" runat="server" 
                                    OnItemCommand="rpt_ItemCommand" OnItemDataBound="rpt_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("STT") %></td>
                                            <td><asp:Button ID="cmdChitiet" runat="server" Text="Chọn bị án"
                                                    CssClass="buttonchitiet" CausesValidation="false"
                                                    CommandArgument='<%# Eval("BiAnID") +"$"+ Eval("IDVuAnHeThong")%>'
                                                    CommandName="ThuLyAn"/>
                                            </td>
                                            <td><%# Eval("MaBiAn") %></td>
                                            <td><%# Eval("TenBiAn") %></td>
                                            <td><%# Eval("MaVuAn") %></td>
                                            <td><%# Eval("TenVuAn") %></td>
                                            <td><%# Eval("SoBanAn") %></td>
                                            <td><%# string.Format("{0:dd/MM/yyyy}",Eval("NGAYBANAN")) %></td>
                                            <td>
                                                <div class="align_center">
                                                    <asp:LinkButton ID="lblSua" runat="server" Text="Sửa" CausesValidation="false"
                                                        CommandName="sua" ForeColor="#0e7eee" ToolTip="Sửa"
                                                        CommandArgument='<%#Eval("BiAnID") %>'></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                    <asp:LinkButton ID="lbtXoa" runat="server" ToolTip="Xóa"
                                                        CausesValidation="false" Text="Xóa" ForeColor="#0e7eee"
                                                        CommandName="Xoa" CommandArgument='<%#Eval("BiAnID") %>'></asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <!--------------------------------------->
                            <div class="phantrang">
                                <div class="sobanghi">
                                    <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                                </div>
                                <div class="sotrang">
                                    <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back" Visible="false"
                                        OnClick="lbTBack_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active" Visible="false"
                                        Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                    <asp:Label ID="lbBStep1" runat="server" Text="..." Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:Label ID="lbBStep6" runat="server" Text="..." Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so" Visible="false"
                                        Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next" Visible="false"
                                        OnClick="lbTNext_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script>
        function pageLoad(sender, args) {

            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }

    </script>
</asp:Content>
