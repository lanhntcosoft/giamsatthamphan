﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.THA;
using Module.Common;
using BL.GSTP.Danhmuc;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB.GSTP.QLAN.THA.DacXa
{
    public partial class ThuLyDon : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        GSTPContext dt = new GSTPContext();
        Decimal VuAnID = 0, BiAnID = 0;
        Decimal ToaAnID = 0;
        THA_DACXA obj = new THA_DACXA();
        Decimal CurrUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                if (!IsPostBack)
                {
                    BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                    ToaAnID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_MADONVI] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_MADONVI] + "");
                    txtTentoa.Text = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    Load_BiAn();

                    loadinfor();
                }
            }
            else Response.Redirect("/login.aspx");
        }
        private void loadinfor()
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_DACXA TM = dt.THA_DACXA.Where(x => x.BIANID == BiAnID).FirstOrDefault();
            if (TM != null)
            {
                txtSothuly.Text = TM.SOTHULY;
                txtNhanxetXa_Phuong.Text = TM.GHICHU;
                rdYeucau_XoaAnTich.SelectedValue = TM.LOAIXOAANTICH + "";
                if (TM.NGAYTHULY != DateTime.MinValue) txtNgay_thuly.Text = ((DateTime)TM.NGAYTHULY).ToString("dd/MM/yyyy", cul);              
                if (TM.NGAYTAODON != DateTime.MinValue) txtNgay_lamdon.Text = ((DateTime)TM.NGAYTAODON).ToString("dd/MM/yyyy", cul);               
            }
        }
        void Load_BiAn()
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (obj != null)
            {
                VuAnID = (decimal)obj.VUANID;
                hddcurID.Value = VuAnID.ToString();
                txtToidanh.Text = obj.TOIDANH;

            }
        }

        private void Save_Infor(THA_DACXA obj)
        {

            ToaAnID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_MADONVI] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_MADONVI] + "");
            //load date

            THA_BIAN objBA = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (objBA != null)
            {
                VuAnID = (decimal)objBA.VUANID;
                hddcurID.Value = VuAnID.ToString();
            }
            try
            {
                obj.NGAYTAODON = (String.IsNullOrEmpty(txtNgay_lamdon.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgay_lamdon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.NGAYTHULY = (String.IsNullOrEmpty(txtNgay_thuly.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgay_thuly.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
            //lấy ID theo bang
            obj.VUANID = Convert.ToDecimal(hddcurID.Value);
            obj.BIANID = BiAnID;
            obj.TOAANID = ToaAnID;

            //lấy thông tin theo bảng
            obj.SOTHULY = txtSothuly.Text.Trim();
            obj.LOAIXOAANTICH = Convert.ToDecimal(rdYeucau_XoaAnTich.SelectedValue);
            obj.GHICHU = txtNhanxetXa_Phuong.Text;
        }


        protected void cmdUpdateVuAn_Click(object sender, EventArgs e)
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_DACXA obj = dt.THA_DACXA.Where(x => x.BIANID == BiAnID).FirstOrDefault();
            if (obj != null)
            {
                Save_Infor(obj);
                dt.SaveChanges();
                lbthongbao.Text = "Cập nhật thành công!";
            }
            else
            {
                obj = new THA_DACXA();
                Save_Infor(obj);
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_DACXA.Add(obj);
                dt.SaveChanges();
                lbthongbao.Text = "Lưu thành công!";
            }

        }

    }
}