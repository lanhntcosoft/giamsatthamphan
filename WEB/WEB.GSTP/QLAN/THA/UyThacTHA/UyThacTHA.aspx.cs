﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using BL.GSTP.Danhmuc;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BL.GSTP.THA;


namespace WEB.GSTP.QLAN.THA.UyThacTHA
{
    public partial class UyThacTHA : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        THA_UYTHAC_DETAIL obj = new THA_UYTHAC_DETAIL();
        private const decimal ROOT = 0;
        Decimal CurrUserID = 0,  VuAnID = 0,BiAnID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");

                if (!IsPostBack)
                {
                    LoadDrop();
                    LoadGrid();
                    txtNgayUyThac.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        //----------------------------------
        void LoadDrop()
        {
            LoadDrop_QDUyThacTHA();
            LoadDropNguoiNhan();
            LoadDropByGroupName(dropLyDo, ENUM_DANHMUC.UYTHACTHA_LyDo, true);
        }
        //-----------------
        void LoadDrop_QDUyThacTHA()
        {
            dropQuyetDinhUyThacTHA.Items.Clear();
            dropQuyetDinhUyThacTHA.Items.Add(new ListItem("--- Chọn ---", "0"));
            THA_UYTHAC_QUYETDINH_BL oT = new THA_UYTHAC_QUYETDINH_BL();
            DataTable tbl = oT.GetAllByBiAnID(BiAnID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                ListItem item = null;               
                foreach(DataRow row in tbl.Rows)
                {
                    item = new ListItem();
                    item.Value = row["ID"] + "";
                    item.Text = row["MAQD"].ToString() + " - "+ row["TENQD"].ToString();
                    dropQuyetDinhUyThacTHA.Items.Add(item);
                }
            }
        }
        protected void dropQuyetDinhUyThacTHA_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropQuyetDinhUyThacTHA.SelectedValue != "0")
            {
                decimal QuyetDinhID = Convert.ToDecimal(dropQuyetDinhUyThacTHA.SelectedValue);
                LoadZoneQuyetDinhTHAByID(QuyetDinhID);
                THA_UYTHAC_DETAIL obj = dt.THA_UYTHAC_DETAIL.Where(x => x.QD_UYTHACTHA_ID == QuyetDinhID && x.BIANID == BiAnID).FirstOrDefault();
                if (obj != null)
                    LoadUyThacDetail(obj);
            }
            else
            {
                txtToaAnUyThac.Text = txtUy_thac.Text = "";
                txtQD_SoQD.Text = txtQD_NgayQD.Text=  "";
            }
        }
        void LoadZoneQuyetDinhTHAByID(decimal QuyetDinhID)
        {
            THA_UYTHAC_QUYETDINH obj = dt.THA_UYTHAC_QUYETDINH.Where(x => x.ID == QuyetDinhID).SingleOrDefault();
            if (obj != null)
            {
                Decimal toaanID = (Decimal)obj.TOAANNHANUYTHACID;
                txtToaAnUyThac.Text = dt.DM_TOAAN.Where(x => x.ID == toaanID).SingleOrDefault().TEN;

                toaanID = (Decimal)obj.TOAANUYTHACID;
                txtUy_thac.Text = dt.DM_TOAAN.Where(x => x.ID == toaanID).SingleOrDefault().TEN;

                txtQD_NgayQD.Text = String.IsNullOrEmpty(obj.NGAYQD + "") ? "" : ((DateTime)obj.NGAYQD).ToString("dd/MM/yyyy", cul);
                txtQD_SoQD.Text = obj.SOQD + "";
                txtQD_NgayQD.Enabled = txtQD_SoQD.Enabled = txtToaAnUyThac.Enabled = txtUy_thac.Enabled = false;
            }
        }
        //---------------------
        void LoadDropNguoiNhan()
        {
            DM_CANBO_BL obj = new DM_CANBO_BL();
            Decimal donvi = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            //  DataTable oCBDT = oDMCBBL.GetAllChanhAn_PhoCA(donvi);
            DataTable tbl = obj.DM_CANBO_GETBYDONVI(donvi);
            dropNguoiNhap.DataSource = tbl;
            dropNguoiNhap.DataTextField = "HOTEN";
            dropNguoiNhap.DataValueField = "ID";
            dropNguoiNhap.DataBind();
            dropNguoiNhap.Items.Insert(0, new ListItem("--Chọn--", "0"));
        }
        void LoadDropByGroupName(DropDownList drop, string GroupName, Boolean ShowChangeAll)
        {
            drop.Items.Clear();
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            DataTable tbl = oBL.DM_DATAITEM_GETBYGROUPNAME(GroupName);
            if (ShowChangeAll)
                drop.Items.Add(new ListItem("---Chọn---", "0"));
            foreach (DataRow row in tbl.Rows)
            {
                drop.Items.Add(new ListItem(row["TEN"] + "", row["ID"] + ""));
            }
        }
       
        //----------------------------------
        protected void rdTruongHopUT_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnLyDo.Visible = pnKhac.Visible = false;
            if (rdTruongHopUT.SelectedValue == "0")
            {
                pnLyDo.Visible = true;
                dropLyDo.SelectedValue = "0";
                hddLoaiUyThac.Value = "0";
            }
            else
            { pnLyDo.Visible = false; hddLoaiUyThac.Value = "1"; }
        }

        private void loadEdit(decimal ID)
        {
            THA_UYTHAC_DETAIL obj = dt.THA_UYTHAC_DETAIL.Where(x => x.ID == ID).FirstOrDefault();
            if (obj != null)
                LoadUyThacDetail(obj);
        }
        void LoadUyThacDetail(THA_UYTHAC_DETAIL obj)
        {
            HddID.Value = ID.ToString();
            try
            {
                dropQuyetDinhUyThacTHA.SelectedValue = obj.QD_UYTHACTHA_ID + "";
                LoadZoneQuyetDinhTHAByID(Convert.ToDecimal(obj.QD_UYTHACTHA_ID));
            }
            catch (Exception ex) { }

            //------------------------------
            rdTruongHopUT.SelectedValue = (string.IsNullOrEmpty(obj.LOAIUYTHAC + "")) ? "0" : obj.LOAIUYTHAC.ToString();
            if (rdTruongHopUT.SelectedValue == "0")
            {
                pnLyDo.Visible = true;
                dropLyDo.SelectedValue = "0";
                hddLoaiUyThac.Value = "0";
            }
            else
            {
                pnLyDo.Visible = false;
                hddLoaiUyThac.Value = "1";
            }
            //------------------------------
            dropLyDo.SelectedValue = (String.IsNullOrEmpty(obj.LYDOID + "")) ? "0" : obj.LYDOID.ToString();

            txtNgayUyThac.Text = String.IsNullOrEmpty(obj.NGAYUYTHAC + "") ? "" : ((DateTime)obj.NGAYUYTHAC).ToString("dd/MM/yyyy", cul);
            txtNgayNhan.Text = String.IsNullOrEmpty(obj.NGAYNHANUYTHAC + "") ? "" : ((DateTime)obj.NGAYNHANUYTHAC).ToString("dd/MM/yyyy", cul);

            txtGhichu.Text = obj.GHICHU + "";

            try
            {
                dropNguoiNhap.SelectedValue = (String.IsNullOrEmpty(obj.NGUOINHAP + "")) ? "0" : obj.NGUOINHAP.ToString();
            }
            catch (Exception ex) { }
        }
        //-----------------------------------------
        private void LoadGrid()
        {
            lbthongbao.Text = "";
            decimal BiAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_UYTHAC_DETAIL_BL objBL = new THA_UYTHAC_DETAIL_BL();
            DataTable tbl = objBL.GetAllByBiAnID(BiAnID);
            if (tbl != null)
            {
                rpt.DataSource = tbl;
                rpt.DataBind();
                pndata.Visible = true;
            }
            else
                pndata.Visible = false;
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal ID = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Sua":
                    lbthongbao.Text = "";
                    loadEdit(ID);
                    HddID.Value = e.CommandArgument.ToString();
                    break;
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false)
                    {
                        lbthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    xoa(ID);
                    break;
            }
        }
        public void xoa(decimal ID)
        {
            //THA_UYTHAC_DETAIL oGA = dt.THA_UYTHAC_DETAIL.Where(x => x.ID == ID).FirstOrDefault();
            //if (oGA != null)
            //{
            //    dt.THA_UYTHAC_DETAIL.Remove(oGA);
            //    dt.SaveChanges();
            //    LoadGrid();
            //    ResetControls();
            //    dgList.CurrentPageIndex = 0;
            //    lbthongbao.Text = "Xóa thành công!";
            //}
        }

        //-----------------------------------------
        protected void cmdResert_Click(object sender, EventArgs e)
        {
            ResetControls();
        }
        public void ResetControls()
        {
            txtToaAnUyThac.Text = txtUy_thac.Text = "";
            txtQD_SoQD.Text = txtQD_NgayQD.Text = "";

            rdTruongHopUT.SelectedValue = "0";
            hddLoaiUyThac.Value = "0";
            pnLyDo.Visible = true;
            dropLyDo.SelectedValue = "0";

            txtNgayNhan.Text = txtNgayUyThac.Text = "";
            txtGhichu.Text = "";

            dropLyDo.SelectedValue = dropNguoiNhap.SelectedValue = "0";

            HddID.Value = "0";
            lbthongbao.Text = "";
        }

        //--------------------------------------
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                Decimal QuyetDinhTHA_ID = Convert.ToDecimal(dropQuyetDinhUyThacTHA.SelectedValue);
                THA_UYTHAC_DETAIL obj = dt.THA_UYTHAC_DETAIL.Where(x => x.BIANID == BiAnID && x.QD_UYTHACTHA_ID== QuyetDinhTHA_ID).FirstOrDefault();
                if (obj != null)
                {
                    LayDuLieuUpdate(obj);
                    obj.NGAYSUA = DateTime.Now;
                    obj.NGUOISUA =Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] );
                }
                else
                {
                    obj = new THA_UYTHAC_DETAIL();
                    LayDuLieuUpdate(obj);
                    obj.NGAYTAO = DateTime.Now;
                    obj.NGUOITAO = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    dt.THA_UYTHAC_DETAIL.Add(obj);
                }
                dt.SaveChanges();
                LoadGrid();
                ResetControls();
                lbthongbao.Text = "Lưu thành công!";
            }
            catch (Exception exc)
            {
                lbthongbao.Text = exc.Message;
            }
        }
        public void LayDuLieuUpdate(THA_UYTHAC_DETAIL obj)
        {
            obj.BIANID = BiAnID;
            obj.QD_UYTHACTHA_ID = Convert.ToDecimal(dropQuyetDinhUyThacTHA.SelectedValue);

            obj.LOAIUYTHAC = Convert.ToDecimal(rdTruongHopUT.SelectedValue);
            if (obj.LOAIUYTHAC == 0)
                obj.LYDOID = Convert.ToDecimal(dropLyDo.SelectedValue);
            else
                obj.LYDOID = 0;

            //------------------  
            obj.NGAYUYTHAC = (String.IsNullOrEmpty(txtNgayUyThac.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayUyThac.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYNHANUYTHAC = (String.IsNullOrEmpty(txtNgayNhan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayNhan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //-------------------
            obj.NGUOINHAP = String.IsNullOrEmpty(dropNguoiNhap.SelectedValue) ? 0 : Convert.ToDecimal(dropNguoiNhap.SelectedValue);
        }
    }
}