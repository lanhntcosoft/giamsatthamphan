﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.AHS;
using Module.Common;
using BL.GSTP.Danhmuc;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BL.GSTP.THA;

namespace WEB.GSTP.QLAN.THA.UyThacTHA
{
    public partial class QuyetDinh : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        GSTPContext dt = new GSTPContext();
        THA_UYTHAC_QUYETDINH obj = new THA_UYTHAC_QUYETDINH();
        decimal BiAnID = 0, VuAnID = 0, ToaAnUyThacID = 0;
        Decimal CurrUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
           
            if (CurrUserID > 0)
            {
                BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");                
                if (!IsPostBack)
                {
                    load_ddlNguoiki();

                    txtNgayRaQD.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                    txtUy_thac.Text = Convert.ToString(Session[ENUM_SESSION.SESSION_TENDONVI]);
                     ToaAnUyThacID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID].ToString())) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                    
                    LoadGrid();
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }
        
        #region LoadNguoiKi- chuc vu 
        public void load_ddlNguoiki()
        {
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            Decimal donvi = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DataTable oCBDT = oDMCBBL.GetAllChanhAn_PhoCA(donvi);

            ddlNguoiki.DataSource = oCBDT;
            ddlNguoiki.DataTextField = "HOTEN";
            ddlNguoiki.DataValueField = "ID";
            ddlNguoiki.DataBind();
            ddlNguoiki.Items.Insert(0, new ListItem("--Chọn--", "0"));
        }
        protected void ddlNguoiki_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlNguoiki.SelectedValue != "0")
            {
                try
                {
                    Decimal CanBoID = Convert.ToDecimal(ddlNguoiki.SelectedValue);
                    Decimal ChucVuID = (Decimal)dt.DM_CANBO.Where(x => x.ID == CanBoID).SingleOrDefault().CHUCVUID;
                    hddcurID.Value = ChucVuID.ToString();

                    txtChucVu.Text = dt.DM_DATAITEM.Where(x => x.ID == ChucVuID).SingleOrDefault().TEN;
                }
                catch (Exception ex)
                {
                    lbthongbao.Text = ex.Message;
                }
            }
        }
        #endregion
        
        private void ResertControll()
        {
            //chuc vu-----------
            txtChucVu.Text = "";
            ddlNguoiki.SelectedValue = "0";
            txtGhichu.Text = "";

            //ngay thang--------

            txtHieuLuc_TuNgay.Text = "";
            txtNgay_TheoLuat.Text = "";
            txtNgayHieuLuc_DenNgay.Text = "";
            txtNgayRaQD.Text = "";
            txtNgayThucTe.Text = "";
            txtThangThucTe.Text = "";
            txtThang_TheoLuat.Text = "";

            //------------------
            txtKetThucTheoLuat.Text = "";
            txtQDvaTB.Text = "";
            txtSoQuyDinh.Text = "";
            txtToaAnUyThac.Text = "";
            txtUy_thac.Text = "";

        }
        protected void cmdResert_Click(object sender, EventArgs e)
        {
            ResertControll();
        }
        #region load /Time
        protected void txtNgayRaQD_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgayRaQD.Text))
            {
                txtHieuLuc_TuNgay.Text = txtNgayRaQD.Text;
                DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                //-------------------
                int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
                int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
                DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
                txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
                //-------------------
                int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
                int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
                DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
                txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
            }
        }
        protected void txtHieuLuc_TuNgay_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------

            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }

        //------------------------
        protected void txtThang_TheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgay_TheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_TheoLuat = (string.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtThang_TheoLuat.Text);
            int Ngay_TheoLuat = (string.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim())) ? 0 : Convert.ToInt16(txtNgay_TheoLuat.Text);
            DateTime NgayKetThuc_TheoLuat = NgayHieuLuc_BD.AddMonths(Thang_TheoLuat).AddDays(Ngay_TheoLuat);
            txtKetThucTheoLuat.Text = NgayKetThuc_TheoLuat.ToString("dd/MM/yyyy", cul);
        }
        protected void txtKetThucTheoLuat_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime NgayKetThuc_TheoLuat = DateTime.Parse(this.txtKetThucTheoLuat.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            double songay = (NgayKetThuc_TheoLuat - NgayHieuLuc_BD).TotalDays;
            txtThang_TheoLuat.Text = "";
            txtNgay_TheoLuat.Text = songay.ToString();
        }

        //-------------------------
        protected void txtThangThucTe_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgayThucTe_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            int Thang_ThucTe = (string.IsNullOrEmpty(txtThangThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtThangThucTe.Text);
            int Ngay_ThucTe = (string.IsNullOrEmpty(txtNgayThucTe.Text.Trim())) ? 0 : Convert.ToInt16(txtNgayThucTe.Text);
            DateTime NgayKetThuc_ThucTe = NgayHieuLuc_BD.AddMonths(Thang_ThucTe).AddDays(Ngay_ThucTe);
            txtNgayHieuLuc_DenNgay.Text = NgayKetThuc_ThucTe.ToString("dd/MM/yyyy", cul);
        }
        protected void txtNgayHieuLuc_DenNgay_TextChanged(object sender, EventArgs e)
        {
            DateTime NgayHieuLuc_BD = DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime NgayKetThuc_ThucTe = DateTime.Parse(this.txtNgayHieuLuc_DenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //-------------------
            double songay = (NgayKetThuc_ThucTe - NgayHieuLuc_BD).TotalDays;
            txtThangThucTe.Text = "";
            txtNgayThucTe.Text = songay.ToString();
        }
        #endregion
        //--------------------------------------
        #region lay thông tin/luu--
        private void GetDataToUpdate(THA_UYTHAC_QUYETDINH obj)
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_BIAN oT = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (oT != null)
            {
                VuAnID = (decimal)oT.VUANID;
            }
            //-----ID------
            obj.BIANID = BiAnID;
            obj.VUANID = VuAnID;

            //-------ma QD / QS/QB.---
            obj.MAQD = txtMaQD.Text;
            obj.TENQD = txtQDvaTB.Text;

            //-------toa an uy thac---
            ToaAnUyThacID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID].ToString())) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            obj.TOAANUYTHACID = ToaAnUyThacID;//Session DonViID cua nguoi dung dang login
            obj.TOAANNHANUYTHACID = (string.IsNullOrEmpty(hddToaAnUyThacID.Value)) ? 0 : Convert.ToDecimal(hddToaAnUyThacID.Value);

            //------SoQD/Ngay raQD---
            obj.SOQD = txtSoQuyDinh.Text.Trim();
            obj.NGAYQD = String.IsNullOrEmpty(txtNgayRaQD.Text.Trim()) ? (DateTime?)null : DateTime.Parse(this.txtNgayRaQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //----------Hieu luc tu ngay---
            obj.HIEULUC_TUNGAY = (String.IsNullOrEmpty(txtHieuLuc_TuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtHieuLuc_TuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //------thoi han theo luat dinh/Ngay ket thuc theo luat---
            obj.THEOLUAT_SONGAYHIEULUC = String.IsNullOrEmpty(txtNgay_TheoLuat.Text.Trim()) ? 0 : Convert.ToDecimal(txtNgay_TheoLuat.Text);
            obj.THEOLUAT_SOTHANGHIEULUC = String.IsNullOrEmpty(txtThang_TheoLuat.Text.Trim()) ? 0 : Convert.ToDecimal(txtThang_TheoLuat.Text);

            obj.HIEULUCTHEOLUAT_DENNGAY = String.IsNullOrEmpty(txtKetThucTheoLuat.Text.Trim()) ? (DateTime?)null : DateTime.Parse(this.txtKetThucTheoLuat.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //------Thời hạn thực tế/Ngay hieu luc day--- 
            obj.THUCTE_SONGAYHIEULUC = String.IsNullOrEmpty(txtNgayThucTe.Text.Trim()) ? 0 : Convert.ToDecimal(txtNgayThucTe.Text);
            obj.THUCTE_SOTHANGHIEULUC = String.IsNullOrEmpty(txtThangThucTe.Text.Trim()) ? 0 : Convert.ToDecimal(txtThangThucTe.Text);

            obj.HIEULUC_DENNGAY = (String.IsNullOrEmpty(txtNgayHieuLuc_DenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayHieuLuc_DenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

            //---------Nguoi ki------
            obj.NGUOIKY = ddlNguoiki.SelectedValue + "";
            obj.CHUCVU = txtChucVu.Text;
            //---------Ghi chú-----------
            obj.GHICHU = txtGhichu.Text;
        }


        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            Decimal CurrID = (String.IsNullOrEmpty(hddcurID.Value)) ? 0 : Convert.ToDecimal(hddcurID.Value);
            Boolean IsUpdate = false;
            THA_UYTHAC_QUYETDINH obj = null;
            if (CurrID > 0)
            {
                obj = dt.THA_UYTHAC_QUYETDINH.Where(x => x.ID == CurrID).FirstOrDefault();
                if (obj != null)
                    IsUpdate = true;
            }
            GetDataToUpdate(obj);
            if (!IsUpdate)
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGAYKY = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_UYTHAC_QUYETDINH.Add(obj);
            }
            dt.SaveChanges();
            LoadGrid();
            ResertControll();
            lbthongbao.Text = "Lưu thành công!";
        }

        #endregion
        
        #region danh sach 
        public void LoadGrid()
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");           
            THA_UYTHAC_QUYETDINH_BL oT = new THA_UYTHAC_QUYETDINH_BL();
            DataTable tbl = oT.GetAllByBiAnID(BiAnID);
            if (tbl != null && tbl.Rows.Count>0)
            {
                rpt.DataSource = tbl;
                rpt.DataBind();
                pndata.Visible = true;
            }
            else
            {
                pndata.Visible = false;
            }
        }
  

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal ID = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Sua":
                    lbthongbao.Text = "";
                    loadEdit(ID);
                    hddcurID.Value = e.CommandArgument.ToString();
                    break;
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false)
                    {
                        lbthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    xoa(ID);
                    break;
            }
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        private void loadEdit(decimal ID)
        {
            hddcurID.Value = ID.ToString();
            THA_UYTHAC_QUYETDINH oGA = dt.THA_UYTHAC_QUYETDINH.Where(x => x.ID == ID).FirstOrDefault();
            if (oGA != null)
            {          
                //--------Ma va QD---------
                txtMaQD.Text = oGA.MAQD;
                txtQDvaTB.Text = oGA.TENQD;
                try
                {
                    ddlNguoiki.SelectedValue = oGA.NGUOIKY + "";
                }catch(Exception ex) { }
                txtChucVu.Text = oGA.CHUCVU;
                //--------ToaAn----------
                try
                {                                                                          
                    Decimal toaanID = (Decimal)oGA.TOAANNHANUYTHACID;                    
                    txtToaAnUyThac.Text = dt.DM_TOAAN.Where(x => x.ID == toaanID).SingleOrDefault().MA_TEN;
                    hddToaAnID.Value = toaanID.ToString();

                    Decimal toaanuythacID = (Decimal)oGA.TOAANUYTHACID;
                    hddToaAnUyThacID.Value = toaanuythacID.ToString();
                    txtUy_thac.Text = dt.DM_TOAAN.Where(x => x.ID == ToaAnUyThacID).SingleOrDefault().TEN;
                }
                catch (Exception ext) { }                
               
                //------SoQD/Ngay raQD---
                txtSoQuyDinh.Text = oGA.SOQD;
                txtNgayRaQD.Text = String.IsNullOrEmpty(oGA.NGAYQD + "") ? "" : ((DateTime)oGA.NGAYQD).ToString("dd/MM/yyyy", cul);

                //----------Hieu luc tu ngay---
                txtHieuLuc_TuNgay.Text = String.IsNullOrEmpty(oGA.HIEULUC_TUNGAY + "") ? "" : ((DateTime)oGA.HIEULUC_TUNGAY).ToString("dd/MM/yyyy", cul);

                //------thoi han theo luat dinh/Ngay ket thuc theo luat---
                txtKetThucTheoLuat.Text = string.IsNullOrEmpty(oGA.HIEULUCTHEOLUAT_DENNGAY + "") ? "" : ((DateTime)oGA.HIEULUCTHEOLUAT_DENNGAY).ToString("dd/MM/yyyy", cul);
                txtNgay_TheoLuat.Text = (String.IsNullOrEmpty(oGA.THEOLUAT_SONGAYHIEULUC + "") || (oGA.THEOLUAT_SONGAYHIEULUC == 0)) ? "" : oGA.THEOLUAT_SONGAYHIEULUC + "";
                txtThang_TheoLuat.Text = (String.IsNullOrEmpty(oGA.THEOLUAT_SOTHANGHIEULUC + "") || (oGA.THEOLUAT_SOTHANGHIEULUC == 0)) ? "" : oGA.THEOLUAT_SOTHANGHIEULUC + "";

                //------Thời hạn thực tế/Ngay hieu luc day---          
                txtNgayThucTe.Text = (String.IsNullOrEmpty(oGA.THUCTE_SONGAYHIEULUC+"") ||(oGA.THUCTE_SONGAYHIEULUC==0)) ? "" : oGA.THUCTE_SONGAYHIEULUC + "";
                txtThangThucTe.Text = (String.IsNullOrEmpty(oGA.THUCTE_SOTHANGHIEULUC + "") || (oGA.THUCTE_SOTHANGHIEULUC == 0)) ? "" : oGA.THUCTE_SOTHANGHIEULUC + "";
                txtNgayHieuLuc_DenNgay.Text = String.IsNullOrEmpty(oGA.HIEULUC_DENNGAY + "") ? "" : ((DateTime)oGA.HIEULUC_DENNGAY).ToString("dd/MM/yyyy", cul);

                //---------Ghi chú-----------
                txtGhichu.Text = oGA.GHICHU;
            }
        }
        private void xoa(decimal ID)
        {
            THA_UYTHAC_QUYETDINH oGA = dt.THA_UYTHAC_QUYETDINH.Where(x => x.ID == ID).FirstOrDefault();
            if (oGA != null)
            {
                dt.THA_UYTHAC_QUYETDINH.Remove(oGA);
                dt.SaveChanges();
                ResertControll();
                LoadGrid();
                lbthongbao.Text = "Xóa thành công!";
            }
        }
        #endregion
    }
}