﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.THA;
using Module.Common;
using BL.GSTP.Danhmuc;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB.GSTP.QLAN.THA.XoaAnTich
{
    public partial class KetQua : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        GSTPContext dt = new GSTPContext();
        Decimal BiAnID = 0;
       
        THA_ANTICH_DON obj = new THA_ANTICH_DON();
        Decimal CurrUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                if (!IsPostBack)
                {
                    BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
                    load_infor();
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }      
        void load_infor()
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_ANTICH_DON TM = dt.THA_ANTICH_DON.Where(x => x.BIANID == BiAnID).FirstOrDefault();
            if (TM != null)
            {
                rdYeuCau_XoaAn.SelectedValue = TM.LOAIXOAANTICH + "";
                if (!String.IsNullOrEmpty(TM.KETQUA + ""))
                    rdKetQua.SelectedValue = TM.KETQUA+"";
            }
        }

        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");
            THA_ANTICH_DON obj = dt.THA_ANTICH_DON.Where(x => x.BIANID == BiAnID).FirstOrDefault();
            if (obj != null)
            {
                obj.KETQUA = Convert.ToDecimal(rdKetQua.SelectedValue);
                dt.SaveChanges();
                lbthongbao.Text = "Cập nhật thành công!";
            }
        }  
    }
}