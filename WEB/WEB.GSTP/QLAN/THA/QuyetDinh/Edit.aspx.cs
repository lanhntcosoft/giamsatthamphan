﻿using BL.GSTP;
using BL.GSTP.AHC;
using BL.GSTP.THA;
using BL.GSTP.Danhmuc;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BL.GSTP.AHS;

namespace WEB.GSTP.QLAN.THA.QuyetDinh
{
    public partial class Edit : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        Decimal CurrUserID = 0, VuAnID = 0, BiAnID = 0;
        Decimal HeThong_VuAnID = 0, HeThong_BiAnID = 0;
        public string NgaySoSanh = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");

            if (CurrUserID > 0)
            {
                if (!IsPostBack)
                {
                    LoadDsNguoiKy();
                    lkFile.Visible = cmdXoa.Visible = false;

                    hddBiAnID.Value = BiAnID.ToString();
                    CheckQuyen();
                    LoadInfo();
                }
                //-----------them doan dk cho su kien upload file ------------------
                txtNgayQD.Attributes.Add("onchange", "return SetNgayTHA();");
            }
            else
                Response.Redirect("/Login.aspx");
        }
        void CheckQuyen()
        {
            try
            {
                THA_THULY obj = dt.THA_THULY.Where(x => x.BIANID == BiAnID).FirstOrDefault<THA_THULY>();
                if (obj != null)
                    NgaySoSanh = ((DateTime)obj.NGAYTHULY).ToString("dd/MM/yyyy", cul);
                else
                {
                    lttNhomHPBoSung.Text = lttNhomHPChinh.Text = lttNhomQDKhac.Text = "Bị án chưa có thông tin thụ lý. Bạn hãy kiểm tra lại!";
                    cmdSave.Visible = cmdSave2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                cmdSave.Visible = cmdSave2.Visible = false;
                lttNhomHPBoSung.Text = lttNhomHPChinh.Text = lttNhomQDKhac.Text = "Bị án chưa có thông tin thụ lý. Bạn hãy kiểm tra lại!";
            }
        }
        void LoadInfo()
        {
            BiAnID = (String.IsNullOrEmpty(Session[ENUM_LOAIAN.AN_THA] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA] + "");

            THA_BIAN obj = dt.THA_BIAN.Where(x => x.ID == BiAnID).Single<THA_BIAN>();
            if (obj != null)
            {
                VuAnID = (decimal)obj.VUANID;
                hddVuAnID.Value = VuAnID.ToString();
                // lttBiAn.Text = obj.HOTEN;
                dropBiAn.Items.Clear();
                dropBiAn.Items.Add(new ListItem(obj.HOTEN, obj.ID.ToString()));

                //HeThong_BiAnID = (decimal)obj.IDBICANHETHONG;
                //HeThong_VuAnID = (decimal)obj.IDVUANHETHONG;

                HeThong_BiAnID = (obj.IDBICANHETHONG == null || obj.IDBICANHETHONG == Decimal.MinValue) ? 0 : (Decimal)obj.IDBICANHETHONG;
                HeThong_VuAnID = (obj.IDVUANHETHONG == null || obj.IDVUANHETHONG == Decimal.MinValue) ? 0 : (Decimal)obj.IDVUANHETHONG;
            }

            Load_DsHinhPhat();
            LoadQuyetDinhTHA_DaCo(HeThong_VuAnID, HeThong_BiAnID);
        }
        #region NGuoi Ky, chuc vu
        void LoadDsNguoiKy()
        {
            Decimal donvi = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.GetAllChanhAn_PhoCA(donvi);
            dropNguoiKy.DataSource = oCBDT;
            dropNguoiKy.DataTextField = "HOTEN";
            dropNguoiKy.DataValueField = "ID";
            dropNguoiKy.DataBind();
            dropNguoiKy.Items.Insert(0, new ListItem("--Chọn--", "0"));
        }
        protected void dropNguoiKy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropNguoiKy.SelectedValue != "0")
            {
                try
                {
                    Decimal CanBoID = Convert.ToDecimal(dropNguoiKy.SelectedValue);
                    decimal ChucVuId = (Decimal)dt.DM_CANBO.Where(x => x.ID == CanBoID).Single<DM_CANBO>().CHUCVUID;

                    txtChucVu.Text = dt.DM_DATAITEM.Where(x => x.ID == ChucVuId).Single<DM_DATAITEM>().TEN;
                }
                catch (Exception ex) { }
            }
        }
        #endregion

        //su kien xay ra sau khi thuc hien xong sk gay postback
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Decimal row_hinhphat = 0;
            //-------------------------------
            decimal CurrHinhPhatID = Convert.ToDecimal(hddHinhPhatChange.Value);
            if (CurrHinhPhatID > 0)
            {
                foreach (RepeaterItem item in rptHPChinh.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chk");
                    HiddenField hddHinhPhatID = (HiddenField)item.FindControl("hddHinhPhatID");
                    row_hinhphat = Convert.ToDecimal(hddHinhPhatID.Value);
                    HiddenField hddLoai = (HiddenField)item.FindControl("hddLoai");
                    RadioButtonList rdDefaultTrue = (RadioButtonList)item.FindControl("rdDefaultTrue");
                    rdDefaultTrue.Visible = false;
                    if (chk.Checked)
                    {
                        if (row_hinhphat != CurrHinhPhatID)
                            chk.Checked = false;
                        else
                        {
                            if (hddLoai.Value == ENUM_LOAIHINHPHAT.DEFAULT_TRUE.ToString())
                                rdDefaultTrue.SelectedValue = "1";
                        }
                    }
                }
                foreach (RepeaterItem item in rptQDKhac.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chk");
                    HiddenField hddHinhPhatID = (HiddenField)item.FindControl("hddHinhPhatID");
                    row_hinhphat = Convert.ToDecimal(hddHinhPhatID.Value);
                    HiddenField hddLoai = (HiddenField)item.FindControl("hddLoai");
                    RadioButtonList rdDefaultTrue = (RadioButtonList)item.FindControl("rdDefaultTrue");
                    rdDefaultTrue.Visible = false;
                    if (chk.Checked)
                    {
                        if (row_hinhphat != CurrHinhPhatID)
                            chk.Checked = false;
                        else
                        {
                            if (hddLoai.Value == ENUM_LOAIHINHPHAT.DEFAULT_TRUE.ToString())
                                rdDefaultTrue.SelectedValue = "1";
                        }
                    }
                }
            }
        }

        //----------------------------------
        void Load_DsHinhPhat()
        {
            try
            {
                //---------Load 3 nhom dhp----------------------------
                DM_DATAGROUP objG = dt.DM_DATAGROUP.Where(x => x.MA == ENUM_DANHMUC.NHOMHINHPHAT).Single<DM_DATAGROUP>();
                List<DM_DATAITEM> lst = dt.DM_DATAITEM.Where(x => x.GROUPID == objG.ID).ToList<DM_DATAITEM>();
                if (lst != null && lst.Count > 0)
                {
                    foreach (DM_DATAITEM data in lst)
                    {
                        switch (data.MA + "")
                        {
                            case ENUM_NHOMHINHPHAT.NHOM_HPCHINH:
                                lttNhomHPChinh.Text = data.TEN;
                                LoadHinhPhatTheoNhomHP(data.ID, rptHPChinh);
                                break;
                            case ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG:
                                lttNhomHPBoSung.Text = data.TEN;
                                LoadHinhPhatTheoNhomHP(data.ID, rptHPBoSung);
                                break;
                            case ENUM_NHOMHINHPHAT.NHOM_QDKHAC:
                                lttNhomQDKhac.Text = data.TEN;
                                LoadHinhPhatTheoNhomHP(data.ID, rptQDKhac);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }
        void LoadHinhPhatTheoNhomHP(decimal GroupID, Repeater rptControl)
        {
            DM_BOLUAT_TOIDANH_HINHPHAT_BL obj = new DM_BOLUAT_TOIDANH_HINHPHAT_BL();
            DM_HINHPHAT_BL objHP = new DM_HINHPHAT_BL();
            DataTable tbl = objHP.GetByNhomHP(GroupID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                rptControl.DataSource = tbl;
                rptControl.DataBind();
            }
        }

        protected void rptHP_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rv = (DataRowView)e.Item.DataItem;

                HiddenField hddHinhPhatID = (HiddenField)e.Item.FindControl("hddHinhPhatID");
                decimal hinhphat = Convert.ToDecimal(hddHinhPhatID.Value);
                CheckBox chkAnTreo = (CheckBox)e.Item.FindControl("chkAnTreo");

                if (Convert.ToInt16(rv["ISANTREO"] + "") > 0)
                    chkAnTreo.Visible = true;
                else chkAnTreo.Visible = false;

                //-------------------------------
                decimal group_id = Convert.ToDecimal(rv["NHOMHINHPHAT"] + "");
                String MaNhom = rv["MaNhomHinhPhat"] + "";

                CheckBox chk = (CheckBox)e.Item.FindControl("chk");
                chk.Attributes.Add("onchange", "ChangeHP(" + hinhphat + "," + group_id + ")");

                Decimal VuAnId = Convert.ToDecimal(hddVuAnID.Value);
                THA_BIAN_QUYETDINH_BS objCTBS = null;
                THA_BIAN_QUYETDINH objCT = null;
                if (MaNhom == ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG)
                {
                    chkAnTreo.Checked = false;
                    chk.Visible = false;
                    try
                    {

                        objCTBS = dt.THA_BIAN_QUYETDINH_BS.Where(x => x.BIANID == BiAnID
                                                                   && x.VUANID == VuAnId
                                                                   && x.HINHPHATID == hinhphat
                                                               ).Single<THA_BIAN_QUYETDINH_BS>();
                    }
                    catch (Exception ex) { }
                    if (objCTBS != null)
                    {
                        hddGroupChange.Value = group_id.ToString();
                        if (MaNhom != ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG)
                        {
                            chkAnTreo.Checked = true;
                            chk.Checked = true;
                            hddHinhPhatChange.Value = hinhphat.ToString();
                        }
                    }
                    else
                    {
                        chkAnTreo.Checked = false;
                        chk.Checked = false;
                    }
                }
                else
                {
                    chk.Visible = true;
                    try
                    {
                        objCT = dt.THA_BIAN_QUYETDINH.Where(x => x.BIANID == BiAnID
                                                                            && x.VUANID == VuAnId
                                                                            && x.HPC_HINHPHATID == hinhphat

                                                                        ).Single<THA_BIAN_QUYETDINH>();
                        if (objCT != null)
                        {
                            hddGroupChange.Value = group_id.ToString();
                            if (MaNhom != ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG)
                            {
                                chkAnTreo.Checked = true;
                                chk.Checked = true;
                                hddHinhPhatChange.Value = hinhphat.ToString();
                            }
                        }
                        else
                        {
                            chkAnTreo.Checked = false;
                            chk.Checked = false;
                        }
                    }
                    catch (Exception ex) { }
                }

                //----------------------
                HiddenField hddLoai = (HiddenField)e.Item.FindControl("hddLoai");
                int LoaiHinhPhat = Convert.ToInt16(rv["LOAIHINHPHAT"] + "");
                switch (LoaiHinhPhat)
                {
                    case ENUM_LOAIHINHPHAT.DANG_TRUE_FALSE_VALUE:
                        RadioButtonList rdTrueFalse = (RadioButtonList)e.Item.FindControl("rdTrueFalse");
                        rdTrueFalse.Visible = true;
                        if (objCTBS != null)
                            rdTrueFalse.SelectedValue = (String.IsNullOrEmpty(objCTBS.TF_VALUE + "")) ? "0" : objCTBS.TF_VALUE.ToString();
                        if (objCT != null)
                            rdTrueFalse.SelectedValue = (String.IsNullOrEmpty(objCT.HPC_TF_VALUE + "")) ? "0" : objCT.HPC_TF_VALUE.ToString();
                        break;
                    case ENUM_LOAIHINHPHAT.DANG_SO_HOC_VALUE:
                        TextBox txtSohoc = (TextBox)e.Item.FindControl("txtSohoc");
                        txtSohoc.Visible = true;
                        if (objCTBS != null)
                            txtSohoc.Text = (String.IsNullOrEmpty(objCTBS.SH_VALUE + "")) ? "0" : objCTBS.SH_VALUE.ToString();
                        if (objCT != null)
                            txtSohoc.Text = (String.IsNullOrEmpty(objCT.HPC_SH_VALUE + "")) ? "0" : objCT.HPC_SH_VALUE.ToString();

                        break;
                    case ENUM_LOAIHINHPHAT.DANG_THOI_GIAN_VALUE:
                        Panel pnThoiGian = (Panel)e.Item.FindControl("pnThoiGian");
                        pnThoiGian.Visible = true;
                        TextBox txtNam = (TextBox)e.Item.FindControl("txtNam");
                        TextBox txtThang = (TextBox)e.Item.FindControl("txtThang");
                        TextBox txtNgay = (TextBox)e.Item.FindControl("txtNgay");
                        if (objCTBS != null)
                        {
                            txtNam.Text = (String.IsNullOrEmpty(objCTBS.TG_NAM + "")) ? "0" : objCTBS.TG_NAM.ToString();
                            txtThang.Text = (String.IsNullOrEmpty(objCTBS.TG_THANG + "")) ? "0" : objCTBS.TG_THANG.ToString();
                            txtNgay.Text = (String.IsNullOrEmpty(objCTBS.TG_NGAY + "")) ? "0" : objCTBS.TG_NGAY.ToString();
                        }
                        if (objCT != null)
                        {
                            txtNam.Text = (String.IsNullOrEmpty(objCT.HPC_TG_NAM + "")) ? "0" : objCT.HPC_TG_NAM.ToString();
                            txtThang.Text = (String.IsNullOrEmpty(objCT.HPC_TG_THANG + "")) ? "0" : objCT.HPC_TG_THANG.ToString();
                            txtNgay.Text = (String.IsNullOrEmpty(objCT.HPC_TG_NGAY + "")) ? "0" : objCT.HPC_TG_NGAY.ToString();
                        }
                        break;
                    case ENUM_LOAIHINHPHAT.DANG_KHAC_VALUE:
                        Panel pnKhac = (Panel)e.Item.FindControl("pnKhac");
                        pnKhac.Visible = true;
                        TextBox txtKhac1 = (TextBox)e.Item.FindControl("txtKhac1");
                        TextBox txtKhac2 = (TextBox)e.Item.FindControl("txtKhac2");
                        if (objCT != null)
                        {
                            txtKhac1.Text = (String.IsNullOrEmpty(objCTBS.K_VALUE1 + "")) ? "0" : objCTBS.K_VALUE1.ToString();
                            txtKhac2.Text = (String.IsNullOrEmpty(objCTBS.K_VALUE2 + "")) ? "0" : objCTBS.K_VALUE2.ToString();
                        }
                        if (objCT != null)
                        {
                            txtKhac1.Text = (String.IsNullOrEmpty(objCT.HPC_K_VALUE1 + "")) ? "0" : objCT.HPC_K_VALUE1.ToString();
                            txtKhac2.Text = (String.IsNullOrEmpty(objCT.HPC_K_VALUE2 + "")) ? "0" : objCT.HPC_K_VALUE2.ToString();
                        }
                        break;
                    case ENUM_LOAIHINHPHAT.DEFAULT_TRUE:
                        RadioButtonList rdDefaultTrue = (RadioButtonList)e.Item.FindControl("rdDefaultTrue");
                        rdDefaultTrue.Visible = false;
                        if (objCTBS != null)
                            rdDefaultTrue.SelectedValue = "1";
                        if (objCT != null)
                            rdDefaultTrue.SelectedValue = "1";
                        break;
                }
            }
        }

        //----------------------------------
        void LoadQuyetDinhTHA_DaCo(Decimal HeThong_VuAnID, Decimal HeThong_BiAnID)
        {
            // BiAnID = Convert.ToDecimal(hddBiAnID.Value);
            //VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            Decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");
            THA_BIAN_QUYETDINH obj = null;
            try
            {
                obj = dt.THA_BIAN_QUYETDINH.Where(x => x.BIANID == BiAnID
                                                                       && x.VUANID == VuAnID
                                                                       && x.TOAANID == ToaAnID
                                                                    ).Single<THA_BIAN_QUYETDINH>();
            }
            catch (Exception ex) { }
            if (obj != null)
            {
                txtNgayQD.Text = obj.QD_NGAY != null ? ((DateTime)obj.QD_NGAY).ToString("dd/MM/yyyy", cul) : "";
                txtNgayTHA.Text = obj.NGAYTHIHANH != null ? ((DateTime)obj.NGAYTHIHANH).ToString("dd/MM/yyyy", cul) : "";

                txtSoQD.Text = obj.QD_SO;
                txtNoiChapHanhAn.Text = obj.NOICHAPHANHAN;

                hddFilePath.Value = obj.TENFILE;
                lkFile.Text = obj.TENFILE;
                txtChucVu.Text = obj.CHUCVU + "";
                try
                {
                    dropNguoiKy.SelectedValue = (String.IsNullOrEmpty(obj.NGUOIKY + "")) ? "0" : obj.NGUOIKY.ToString();
                }
                catch (Exception ex) { }
            }
            else
            {
                GetTongHopHinhPhat_CuaBiCanHeThong(HeThong_VuAnID, HeThong_BiAnID);
            }
        }
        void GetTongHopHinhPhat_CuaBiCanHeThong(Decimal HeThong_VuAnID, Decimal HeThong_BiAnID)
        {
            String ThongBao = "";
            ThongBao = "Chưa có quyết định thi hành án cho bị án này.";
            ThongBao += "<br/>Tổng hợp hình phạt cho bị án hiện được lấy từ kết quả xét xử ";
            try
            {
                AHS_VUAN objVA = dt.AHS_VUAN.Where(x => x.ID == HeThong_VuAnID).Single<AHS_VUAN>();
            
                if (objVA != null)
                {
                    int ma_giai_doan = (int)objVA.MAGIAIDOAN;
                    switch (ma_giai_doan)
                    {
                        case ENUM_GIAIDOANVUAN.SOTHAM:
                            ThongBao += "Sơ thẩm";
                            TongHopHinhPhat_SoTham(HeThong_VuAnID, HeThong_BiAnID);
                            break;
                        case ENUM_GIAIDOANVUAN.PHUCTHAM:
                            ThongBao += "Phúc thẩm";
                            TongHopHinhPhat_PhucTham(HeThong_VuAnID, HeThong_BiAnID);
                            break;
                    }
                }
            }
            catch
            {
                ThongBao = "Không tìm thấy hình phạt cho bị án!";
            }
            //lstMsgTop.Text = lstMsgBottom.Text = ThongBao;
        }
        void TongHopHinhPhat_SoTham(Decimal HeThong_VuAnID, Decimal HeThong_BiAnID)
        {
            DateTime Now = DateTime.Now;
            List<AHS_SOTHAM_BANAN> lstBA = dt.AHS_SOTHAM_BANAN.Where(x => x.VUANID == HeThong_VuAnID).ToList<AHS_SOTHAM_BANAN>();
            if (lstBA != null && lstBA.Count > 0)
            {
                AHS_SOTHAM_BANAN objBA = lstBA[0];
                Decimal BanAnID = (Decimal)objBA.ID;
                DateTime ngaybanan = (DateTime)objBA.NGAYBANAN;
                if (Now > ngaybanan.AddDays(14))
                {
                    //lay tong hop hinh phat so tham va bind vao ds cac hinh phat
                    AHS_SOTHAM_BANAN_DIEU_CHITIET_BL obj = new AHS_SOTHAM_BANAN_DIEU_CHITIET_BL();
                    DataTable tbl = obj.GetTHHinhPhatCoHieuLucTHA(HeThong_VuAnID, HeThong_BiAnID);
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        DataView view = new DataView(tbl);
                        DataTable tblNhomHP = view.ToTable(true, "NhomHinhPhat", "MaNhomHinhPhat");
                        if (tblNhomHP != null && tblNhomHP.Rows.Count > 0)
                        {
                            DataTable tblData = tbl.Clone();
                            DataRow[] arr = null;
                            String MaNhom = "";
                            string NhomID = "";
                            foreach (DataRow row in tbl.Rows)
                            {
                                MaNhom = row["MaNhomHinhPhat"] + "";
                                NhomID = row["NhomHinhPhat"] + "";
                                arr = tbl.Select("NhomHinhPhat=" + NhomID, "MucDo DESC");
                                foreach (DataRow rowData in arr)
                                {
                                    tblData.ImportRow(rowData);
                                }
                            }
                        }
                    }
                }
            }
        }
        void CongDonDL(DataTable tbl, String MaNhomHinhPhat)
        {
            if (MaNhomHinhPhat == ENUM_NHOMHINHPHAT.NHOM_HPBOSUNG)
            {
                foreach (DataRow row in tbl.Rows)
                {

                }
            }
        }
        void TongHopHinhPhat_PhucTham(Decimal HeThong_VuAnID, Decimal HeThong_BiAnID)
        {
            List<AHS_PHUCTHAM_BANAN> lstBA = dt.AHS_PHUCTHAM_BANAN.Where(x => x.VUANID == HeThong_VuAnID).ToList<AHS_PHUCTHAM_BANAN>();
            if (lstBA != null && lstBA.Count > 0)
            {
                AHS_PHUCTHAM_BANAN objBA = lstBA[0];
                Decimal BanAnID = (Decimal)objBA.ID;
                DateTime ngaybanan = (DateTime)objBA.NGAYBANAN;

                //lay tong hop hinh phat phuc tham va bind vao ds cac hinh phat
                //List<AHS_PHUCTHAM_BANAN_DIEU_CT> lstHP = dt.AHS_PHUCTHAM_BANAN_DIEU_CT.Where(x => x.BANANID == BanAnID
                //                                                                                     && x.BICANID == HeThong_BiAnID
                //                                                                                   ).ToList<AHS_PHUCTHAM_BANAN_DIEU_CT>();
                //if (lstHP != null && lstHP.Count>0)
                //{

                //}
            }
        }
        //----------------------------------------------------------
        #region File
        protected void lkFile_Click(object sender, EventArgs e)
        {
            DowloadFile();
        }
        void DowloadFile()
        {
            try
            {
                Decimal CurrID = Convert.ToDecimal(hddCurrentID.Value);
                THA_BIAN_QUYETDINH oND = dt.THA_BIAN_QUYETDINH.Where(x => x.ID == CurrID).FirstOrDefault();
                if (oND.TENFILE != "")
                {
                    var cacheKey = Guid.NewGuid().ToString("N");
                    Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                }
            }
            catch (Exception ex)
            {
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", ex.Message);
            }
        }
        void UploadFile(THA_BIAN_QUYETDINH obj)
        {
            string folder_upload = "/TempUpload";
            string strPath = Server.MapPath(folder_upload);
            if (!System.IO.Directory.Exists(strPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(strPath);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            string file_path = "";
            string filename = "";
            byte[] buff = null;
            if (fileupload.HasFiles)
            {
                //-----upload file len server
                filename = Path.GetFileName(fileupload.PostedFile.FileName);
                file_path = strPath + "\\" + filename;
                fileupload.PostedFile.SaveAs(file_path);

                //----------doc file vua duoc upload len server de lay noi dung-------------
                using (FileStream fs = File.OpenRead(file_path))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(file_path);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);

                    obj.TENFILE = oF.Name;
                    obj.NOIDUNG = buff;
                    obj.KIEUFILE = oF.Extension;
                }

                //xoa file
                File.Delete(file_path);
            }
        }
        protected void cmdXoa_Click(object sender, ImageClickEventArgs e)
        {
            Decimal CurrID = Convert.ToDecimal(hddCurrentID.Value);
            THA_BIAN_QUYETDINH oND = dt.THA_BIAN_QUYETDINH.Where(x => x.ID == CurrID).FirstOrDefault();
            if (oND.TENFILE != "")
            {
                oND.TENFILE = "";
                oND.NOIDUNG = null;
                oND.KIEUFILE = "";
                dt.SaveChanges();
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Tệp đính kèm được xóa thành công!");
            }
        }
        #endregion

        //----------------------------------------------------------
        protected void cmdQuaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsach.aspx");
        }

        //----------------------------------
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                UPdate_QuyetDinh();
                Update_HinhPhat();
                Cls_Comon.ShowMessage(this, this.GetType(), "Thông báo", "Cập nhật quyết định thi hành án cho bị án thành công!");
            }
            catch (Exception ex) { }
        }
        void UPdate_QuyetDinh()
        {
            DateTime date_temp;
            Boolean IsUpdate = false;
            BiAnID = Convert.ToDecimal(hddBiAnID.Value);
            Decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            THA_BIAN_QUYETDINH obj = new THA_BIAN_QUYETDINH();

            if (BiAnID > 0)
            {
                try
                {
                    obj = dt.THA_BIAN_QUYETDINH.Where(x => x.BIANID == BiAnID && x.VUANID == VuAnID).Single<THA_BIAN_QUYETDINH>();
                    IsUpdate = true;
                }
                catch (Exception ex)
                {
                    obj = new THA_BIAN_QUYETDINH();
                }
            }
            else
                obj = new THA_BIAN_QUYETDINH();

            obj.BIANID = BiAnID;
            obj.VUANID = VuAnID;
            obj.QD_SO = txtSoQD.Text.Trim();

            date_temp = (String.IsNullOrEmpty(txtNgayQD.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.QD_NGAY = date_temp;
            obj.NGUOIKY = dropNguoiKy.SelectedValue;
            obj.CHUCVU = txtChucVu.Text.Trim();

            date_temp = (String.IsNullOrEmpty(txtNgayTHA.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayTHA.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYTHIHANH = date_temp;
            obj.NOICHAPHANHAN = txtNoiChapHanhAn.Text.Trim();

            UploadFile(obj);
            obj.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID] + "");

            //--------------------------------
            if (IsUpdate)
            {
                obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.SaveChanges();
            }
            else
            {
                obj.NGAYTAO = DateTime.Now;
                obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.THA_BIAN_QUYETDINH.Add(obj);
                dt.SaveChanges();
            }

            hddCurrentID.Value = obj.ID.ToString();
            //lbthongbao.Text = "Đã cập nhật xong thụ lý cho bị án " + dropBiAn.SelectedItem.Text;

            //------------------------
            if (!String.IsNullOrEmpty(obj.TENFILE))
                lkFile.Visible = cmdXoa.Visible = true;
            else
                lkFile.Visible = cmdXoa.Visible = false;
        }
        void Update_HinhPhat()
        {
            Decimal BiCanID = BiAnID;
            Decimal NhomHinhPhatChange = String.IsNullOrEmpty(hddGroupChange.Value) ? 0 : Convert.ToDecimal(hddGroupChange.Value);
            if (NhomHinhPhatChange > 0)
            {
                string manhom = dt.DM_DATAITEM.Where(x => x.ID == NhomHinhPhatChange).Single<DM_DATAITEM>().MA;
                if (manhom == ENUM_NHOMHINHPHAT.NHOM_HPCHINH)
                    Update_HinhPhatChinh(rptHPChinh, BiCanID);
                else
                    Update_HinhPhatChinh(rptQDKhac, BiCanID);
            }
            else
            {
                Update_HinhPhatChinh(rptHPChinh, BiCanID);
                Update_HinhPhatChinh(rptQDKhac, BiCanID);
            }
            Update_HinhPhatBS(rptHPBoSung, BiCanID);
        }
        void Update_HinhPhatChinh(Repeater rpt, Decimal BiCanID)
        {
            Decimal QuyetDinhID = String.IsNullOrEmpty(hddCurrentID.Value) ? 0 : Convert.ToDecimal(hddCurrentID.Value);
            if (QuyetDinhID > 0)
            {
                Decimal HinhPhatChinh = Convert.ToDecimal(hddHinhPhatChange.Value);
                THA_BIAN_QUYETDINH obj;
                Boolean IsUpdate = false;
                foreach (RepeaterItem itemHP in rpt.Items)
                {
                    try
                    {
                        HiddenField hddGroup = (HiddenField)itemHP.FindControl("hddGroup");
                        decimal CurrGroupID = Convert.ToDecimal(hddGroup.Value);

                        HiddenField hddHinhPhatID = (HiddenField)itemHP.FindControl("hddHinhPhatID");
                        decimal hinhphatid = Convert.ToDecimal(hddHinhPhatID.Value);
                        CheckBox chk = (CheckBox)itemHP.FindControl("chk");
                        if (hinhphatid == HinhPhatChinh && chk.Checked == true)
                        {
                            try
                            {
                                obj = dt.THA_BIAN_QUYETDINH.Where(x => x.ID == QuyetDinhID).Single<THA_BIAN_QUYETDINH>();
                                if (obj != null)
                                    IsUpdate = true;
                                else
                                    obj = new THA_BIAN_QUYETDINH();
                            }
                            catch (Exception ex) { obj = new THA_BIAN_QUYETDINH(); }

                            //--------------------------
                            CheckBox chkAnTreo = (CheckBox)itemHP.FindControl("chkAnTreo");
                            obj.ISANTREO = (chkAnTreo.Checked) ? 1 : 0;

                            GetValue(obj, itemHP);
                            if (!IsUpdate)
                                dt.THA_BIAN_QUYETDINH.Add(obj);
                            dt.SaveChanges();
                        }
                    }
                    catch (Exception ex) { }
                }
            }
        }
        void Update_HinhPhatBS(Repeater rpt, Decimal BiCanID)
        {
            Boolean IsUpdate = false;
            Decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            Decimal QuyetDinhID = String.IsNullOrEmpty(hddCurrentID.Value) ? 0 : Convert.ToDecimal(hddCurrentID.Value);
            THA_BIAN_QUYETDINH_BS obj;

            foreach (RepeaterItem itemHP in rpt.Items)
            {
                try
                {
                    IsUpdate = false;

                    HiddenField hddGroup = (HiddenField)itemHP.FindControl("hddGroup");
                    decimal CurrGroupID = Convert.ToDecimal(hddGroup.Value);

                    HiddenField hddHinhPhatID = (HiddenField)itemHP.FindControl("hddHinhPhatID");
                    decimal hinhphatid = Convert.ToDecimal(hddHinhPhatID.Value);
                    //KT da co hinhphat bo sung nay trong DB?? 
                    try
                    {
                        obj = dt.THA_BIAN_QUYETDINH_BS.Where(x => x.BIANID == BiCanID
                                                               && x.VUANID == VuAnID
                                                            ).Single<THA_BIAN_QUYETDINH_BS>();
                        if (obj != null)
                            IsUpdate = true;
                        else
                            obj = new THA_BIAN_QUYETDINH_BS();
                    }
                    catch (Exception ex) { obj = new THA_BIAN_QUYETDINH_BS(); }
                    GetValue_BS(obj, itemHP);
                    if (!IsUpdate)
                        dt.THA_BIAN_QUYETDINH_BS.Add(obj);
                    dt.SaveChanges();
                }
                catch (Exception ex) { }
            }
        }

        void GetValue(THA_BIAN_QUYETDINH obj, RepeaterItem itemHP)
        {
            Decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            HiddenField hddLoai = (HiddenField)itemHP.FindControl("hddLoai");
            decimal loai_hp = Convert.ToDecimal(hddLoai.Value);
            obj.HPC_LOAIID = loai_hp;

            HiddenField hddHinhPhatID = (HiddenField)itemHP.FindControl("hddHinhPhatID");
            decimal hinhphatid = Convert.ToDecimal(hddHinhPhatID.Value);
            obj.HPC_HINHPHATID = hinhphatid;

            obj.VUANID = VuAnID;
            obj.BIANID = BiAnID;

            switch (Convert.ToInt16(loai_hp))
            {
                case ENUM_LOAIHINHPHAT.DEFAULT_TRUE:
                    obj.HPC_TF_VALUE = 1;
                    break;
                case ENUM_LOAIHINHPHAT.DANG_TRUE_FALSE_VALUE:
                    RadioButtonList rdTrueFalse = (RadioButtonList)itemHP.FindControl("rdTrueFalse");
                    obj.HPC_TF_VALUE = Convert.ToDecimal(rdTrueFalse.SelectedValue);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_SO_HOC_VALUE:
                    TextBox txtSohoc = (TextBox)itemHP.FindControl("txtSohoc");
                    obj.HPC_SH_VALUE = String.IsNullOrEmpty(txtSohoc.Text) ? 0 : Convert.ToDecimal(txtSohoc.Text);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_THOI_GIAN_VALUE:
                    TextBox txtNam = (TextBox)itemHP.FindControl("txtNam");
                    TextBox txtThang = (TextBox)itemHP.FindControl("txtThang");
                    TextBox txtNgay = (TextBox)itemHP.FindControl("txtNgay");
                    obj.HPC_TG_NGAY = String.IsNullOrEmpty(txtNgay.Text) ? 0 : Convert.ToDecimal(txtNgay.Text);
                    obj.HPC_TG_THANG = String.IsNullOrEmpty(txtThang.Text) ? 0 : Convert.ToDecimal(txtThang.Text);
                    obj.HPC_TG_NAM = String.IsNullOrEmpty(txtNam.Text) ? 0 : Convert.ToDecimal(txtNam.Text);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_KHAC_VALUE:
                    TextBox txtKhac1 = (TextBox)itemHP.FindControl("txtKhac1");
                    TextBox txtKhac2 = (TextBox)itemHP.FindControl("txtKhac2");
                    obj.HPC_K_VALUE1 = String.IsNullOrEmpty(txtKhac1.Text) ? 0 : Convert.ToDecimal(txtKhac1.Text);
                    obj.HPC_K_VALUE2 = txtKhac2.Text.Trim();
                    break;
            }
        }
        void GetValue_BS(THA_BIAN_QUYETDINH_BS obj, RepeaterItem itemHP)
        {
            Decimal VuAnID = Convert.ToDecimal(hddVuAnID.Value);
            HiddenField hddLoai = (HiddenField)itemHP.FindControl("hddLoai");
            decimal loai_hp = Convert.ToDecimal(hddLoai.Value);
            obj.LOAIHINHPHAT = loai_hp;

            HiddenField hddHinhPhatID = (HiddenField)itemHP.FindControl("hddHinhPhatID");
            decimal hinhphatid = Convert.ToDecimal(hddHinhPhatID.Value);
            obj.HINHPHATID = hinhphatid;

            obj.VUANID = VuAnID;
            obj.BIANID = BiAnID;

            switch (Convert.ToInt16(loai_hp))
            {
                case ENUM_LOAIHINHPHAT.DEFAULT_TRUE:
                    obj.TF_VALUE = 1;
                    break;
                case ENUM_LOAIHINHPHAT.DANG_TRUE_FALSE_VALUE:
                    RadioButtonList rdTrueFalse = (RadioButtonList)itemHP.FindControl("rdTrueFalse");
                    obj.TF_VALUE = Convert.ToDecimal(rdTrueFalse.SelectedValue);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_SO_HOC_VALUE:
                    TextBox txtSohoc = (TextBox)itemHP.FindControl("txtSohoc");
                    obj.SH_VALUE = String.IsNullOrEmpty(txtSohoc.Text) ? 0 : Convert.ToDecimal(txtSohoc.Text);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_THOI_GIAN_VALUE:
                    TextBox txtNam = (TextBox)itemHP.FindControl("txtNam");
                    TextBox txtThang = (TextBox)itemHP.FindControl("txtThang");
                    TextBox txtNgay = (TextBox)itemHP.FindControl("txtNgay");
                    obj.TG_NGAY = String.IsNullOrEmpty(txtNgay.Text) ? 0 : Convert.ToDecimal(txtNgay.Text);
                    obj.TG_THANG = String.IsNullOrEmpty(txtThang.Text) ? 0 : Convert.ToDecimal(txtThang.Text);
                    obj.TG_NAM = String.IsNullOrEmpty(txtNam.Text) ? 0 : Convert.ToDecimal(txtNam.Text);
                    break;
                case ENUM_LOAIHINHPHAT.DANG_KHAC_VALUE:
                    TextBox txtKhac1 = (TextBox)itemHP.FindControl("txtKhac1");
                    TextBox txtKhac2 = (TextBox)itemHP.FindControl("txtKhac2");
                    obj.K_VALUE1 = String.IsNullOrEmpty(txtKhac1.Text) ? 0 : Convert.ToDecimal(txtKhac1.Text);
                    obj.K_VALUE2 = txtKhac2.Text.Trim();
                    break;
            }
        }
    }
}