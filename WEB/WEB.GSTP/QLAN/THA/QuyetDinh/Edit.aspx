﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/GSTP.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="WEB.GSTP.QLAN.THA.QuyetDinh.Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <script type="text/javascript" src="/UI/js/base64.js"></script>
    <script type="text/javascript" src="/UI/js/vgcaplugin.js"></script>--%>
    <script src="../../../UI/js/Common.js"></script>
    <style>
        .boxchung {
            float: left;
            width: 99%;
            margin-left: 0;
        }
    </style>
    <script src="../../../UI/js/Common.js"></script>
    <asp:HiddenField ID="hddCurrentID" Value="0" runat="server" />
    <asp:HiddenField ID="hddBiAnID" Value="0" runat="server" />
    <asp:HiddenField ID="hddVuAnID" Value="0" runat="server" />
    <asp:HiddenField ID="hddHinhPhatChange" runat="server" Value="0" />
    <asp:HiddenField ID="hddGroupChange" runat="server" Value="0" />
    <div class="box">
        <div class="box_nd">
            <div class="truong">
              
                <div style="margin: 5px; text-align: center; width: 95%; color: red;" >
                    <asp:Literal ID="lstMsgTop" runat="server"></asp:Literal>
                </div>
                <div style="margin: 5px; text-align: center; width: 95%">
                    <asp:Button ID="cmdSave" runat="server" CssClass="buttoninput" Text="Lưu"
                        OnClientClick="return validate();"   OnClick="cmdSave_Click"/>
                    <asp:Button ID="cmdQuaylaiB" runat="server" CssClass="buttoninput"
                        Text="Quay lại" OnClick="cmdQuaylai_Click" />
                </div>
                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">Quyết định thi hành án</h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <table class="table1">
                            <tr>
                                <td style="width: 95px;">Bị án<span class="batbuoc">(*)</span></td>
                                <td style="width: 200px;">
                                    <asp:DropDownList ID="dropBiAn" CssClass="chosen-select"
                                        runat="server" Width="200px">
                                    </asp:DropDownList>

                                </td>
                                <td style="width: 100px;">Số quyết định<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtSoQD" CssClass="user"
                                        runat="server" Width="90%"></asp:TextBox></td>
                                <td style="width: 110px;">
                                    <div class="float_right">Ngày quyết định<span class="batbuoc">(*)</span></div>
                                </td>
                                <td style="width: 120px;">
                                    <asp:TextBox ID="txtNgayQD" runat="server" CssClass="user float_right"
                                        Width="110px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server"
                                        TargetControlID="txtNgayQD" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                                        TargetControlID="txtNgayQD" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN"
                                        ErrorTooltipEnabled="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>Ngày thi hành<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:TextBox ID="txtNgayTHA" runat="server" CssClass="user"
                                        Width="192px" MaxLength="10"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                        TargetControlID="txtNgayTHA" Format="dd/MM/yyyy" Enabled="true" />
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server"
                                        TargetControlID="txtNgayTHA" Mask="99/99/9999"
                                        MaskType="Date" CultureName="vi-VN"
                                        ErrorTooltipEnabled="true" />
                                </td>
                                <td>Nơi chấp hành án</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtNoiChapHanhAn" CssClass="user"
                                        runat="server" Width="98%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Người ký<span class="batbuoc">(*)</span></td>
                                <td>
                                    <asp:DropDownList ID="dropNguoiKy" CssClass="chosen-select"
                                        runat="server" Width="200px" AutoPostBack="true"
                                        OnSelectedIndexChanged="dropNguoiKy_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>Chức vụ<span class="batbuoc">(*)</span></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtChucVu" CssClass="user" Enabled="false"
                                        runat="server" Width="98%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Tệp đính kèm</td>
                                <td colspan="5">
                                    <asp:HiddenField ID="hddFilePath" runat="server" />
                                    <asp:HiddenField ID="hddFileKySo" runat="server" Value="" />
                                    <asp:HiddenField ID="hddSessionID" runat="server" />
                                    <asp:HiddenField ID="hddURLKS" runat="server" />
                                    <!------------------------>
                                    <div id="zonekyso" style="margin-bottom: 5px; margin-top: 10px;">
                                        <asp:FileUpload ID="fileupload" runat="server" />
                                        <asp:LinkButton ID="lkFile" runat="server"
                                            ToolTip="Bấm vào để tải file" OnClick="lkFile_Click"></asp:LinkButton>
                                        <asp:ImageButton ID="cmdXoa" runat="server"
                                            ImageUrl="../../../UI/img/delete.png" ToolTip="Xóa"
                                            OnClientClick="return confirm('Bạn có thực sự muốn xóa tệp đính kèm này?');"
                                            Width="20px" OnClick="cmdXoa_Click"></asp:ImageButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!----------------------------------------->
                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">
                        <asp:Literal ID="lttNhomHPChinh" runat="server"></asp:Literal></h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <asp:Repeater ID="rptHPChinh" runat="server"
                            OnItemDataBound="rptHP_ItemDataBound">
                            <HeaderTemplate>
                                <table style="width: 100%;" id="tblHP">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 50px;">
                                        <div style="float: left; width: 100%; text-align: center;">
                                            <asp:CheckBox ID="chk" ToolTip='<%#Eval("HinhPhatID") %>' runat="server" AutoPostBack="true" />
                                        </div>
                                        <asp:HiddenField ID="hddGroup" runat="server" Value='<%#Eval("NHOMHINHPHAT") %>' />
                                        <asp:HiddenField ID="hddLoai" runat="server" Value='<%#Eval("LoaiHinhPHat") %>' />
                                        <asp:HiddenField ID="hddHinhPhatID" runat="server" Value='<%#Eval("HinhPhatID") %>' />
                                    </td>
                                    <td style="width: 50%;"><%# Eval("TenHinhPhat") %></td>
                                    <td>
                                        <asp:RadioButtonList ID="rdDefaultTrue" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:RadioButtonList ID="rdTrueFalse" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:TextBox ID="txtSohoc" CssClass="user" Width="100px"
                                            runat="server" onkeypress="return isNumber(event)" Visible="false"></asp:TextBox>
                                        <!-------------------------->
                                        <asp:Panel ID="pnThoiGian" runat="server" Visible="false">
                                            <asp:TextBox ID="txtNam" CssClass="user align_right" Width="40px"
                                                runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>năm&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtThang" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>tháng&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtNgay" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>ngày</span>
                                        </asp:Panel>
                                        <!-------------------------->
                                        <asp:Panel ID="pnKhac" runat="server" Visible="false">
                                            <asp:TextBox ID="txtKhac1" CssClass="user" Width="100px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            hoặc
                                                                <asp:TextBox ID="txtKhac2" runat="server" CssClass="user"
                                                                    Width="100px" Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                        </asp:Panel>

                                        <!-------------------------->
                                        <asp:CheckBox ID="chkAnTreo" runat="server"
                                            Text="Hưởng án treo" CssClass="margin_top" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!---------------------------------------->
                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">
                        <asp:Literal ID="lttNhomHPBoSung" runat="server"></asp:Literal></h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <asp:Repeater ID="rptHPBoSung" runat="server" OnItemDataBound="rptHP_ItemDataBound">
                            <HeaderTemplate>
                                <table width="100%" id="tblHP">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 50px;">
                                        <div style="float: left; width: 100%; text-align: center;">
                                            <asp:CheckBox ID="chk" ToolTip='<%#Eval("HinhPhatID") %>' runat="server" Visible="false" />
                                            <%-- <img src="../../../UI/img/arrow.png" />--%>
                                        </div>
                                        <asp:HiddenField ID="hddGroup" runat="server" Value='<%#Eval("NHOMHINHPHAT") %>' />
                                        <asp:HiddenField ID="hddLoai" runat="server" Value='<%#Eval("LoaiHinhPHat") %>' />
                                        <asp:HiddenField ID="hddHinhPhatID" runat="server" Value='<%#Eval("HinhPhatID") %>' />
                                    </td>
                                    <td style="width: 50%;"><%# Eval("TenHinhPhat") %></td>
                                    <td>
                                        <asp:RadioButtonList ID="rdDefaultTrue" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:RadioButtonList ID="rdTrueFalse" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:TextBox ID="txtSohoc" CssClass="user" Width="100px"
                                            runat="server" onkeypress="return isNumber(event)" Visible="false"></asp:TextBox>
                                        <!-------------------------->
                                        <asp:Panel ID="pnThoiGian" runat="server" Visible="false">
                                            <asp:TextBox ID="txtNam" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>năm&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtThang" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>tháng&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtNgay" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>ngày</span>
                                        </asp:Panel>
                                        <!-------------------------->
                                        <asp:Panel ID="pnKhac" runat="server" Visible="false">
                                            <asp:TextBox ID="txtKhac1" CssClass="user" Width="100px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            hoặc
                                                                <asp:TextBox ID="txtKhac2" runat="server" CssClass="user"
                                                                    Width="100px" Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                        </asp:Panel>
                                        <!-------------------------->

                                        <asp:CheckBox ID="chkAnTreo" runat="server" Text="Hưởng án treo" CssClass="margin_top" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!------------------------------------------->
                <div class="boxchung">
                    <h4 class="tleboxchung bg_title_group bg_green">
                        <asp:Literal ID="lttNhomQDKhac" runat="server"></asp:Literal></h4>
                    <div class="boder" style="padding: 20px 10px;">
                        <asp:Repeater ID="rptQDKhac" runat="server" OnItemDataBound="rptHP_ItemDataBound">
                            <HeaderTemplate>
                                <table width="100%" id="tblHP">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 50px;">
                                        <div style="float: left; width: 100%; text-align: center;">
                                            <asp:CheckBox ID="chk" ToolTip='<%#Eval("HinhPhatID") %>' runat="server" AutoPostBack="true" />
                                        </div>
                                        <asp:HiddenField ID="hddGroup" runat="server" Value='<%#Eval("NHOMHINHPHAT") %>' />
                                        <asp:HiddenField ID="hddLoai" runat="server" Value='<%#Eval("LoaiHinhPHat") %>' />
                                        <asp:HiddenField ID="hddHinhPhatID" runat="server" Value='<%#Eval("HinhPhatID") %>' />
                                    </td>
                                    <td style="width: 50%;"><%# Eval("TenHinhPhat") %></td>
                                    <td>
                                        <asp:RadioButtonList ID="rdDefaultTrue" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:RadioButtonList ID="rdTrueFalse" runat="server"
                                            RepeatDirection="Horizontal" Visible="false">
                                            <asp:ListItem Value="0">Không</asp:ListItem>
                                            <asp:ListItem Value="1">Có</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <!-------------------------->
                                        <asp:TextBox ID="txtSohoc" CssClass="user" Width="100px"
                                            runat="server" onkeypress="return isNumber(event)" Visible="false"></asp:TextBox>
                                        <!-------------------------->
                                        <asp:Panel ID="pnThoiGian" runat="server" Visible="false">
                                            <asp:TextBox ID="txtNam" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>năm&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtThang" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>tháng&nbsp;&nbsp;</span>
                                            <asp:TextBox ID="txtNgay" CssClass="user align_right" Width="40px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            <span class='span_date'>ngày</span>
                                        </asp:Panel>
                                        <!-------------------------->
                                        <asp:Panel ID="pnKhac" runat="server" Visible="false">
                                            <asp:TextBox ID="txtKhac1" CssClass="user" Width="100px" runat="server"
                                                Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                            hoặc
                                                                <asp:TextBox ID="txtKhac2" runat="server" CssClass="user"
                                                                    Width="100px" Text="0" onkeypress="return isNumber(event)"></asp:TextBox>
                                        </asp:Panel>
                                        <!-------------------------->

                                        <asp:CheckBox ID="chkAnTreo" runat="server" Text="Hưởng án treo" CssClass="margin_top" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!------------------------------------------>
                <div style="margin: 5px; text-align: center; width: 95%; color: red;">
                    <asp:Literal ID="lstMsgBottom" runat="server"></asp:Literal>
                </div>

                <div style="margin: 5px; text-align: center; width: 95%">
                    <asp:Button ID="cmdSave2" runat="server" CssClass="buttoninput" Text="Lưu"
                        OnClientClick="return validate();" OnClick="cmdSave_Click" />
                    <asp:Button ID="cmdQuaylai2" runat="server" CssClass="buttoninput"
                        Text="Quay lại" OnClick="cmdQuaylai_Click" />
                </div>
            </div>
        </div>
    </div>
    
    <script>

          function pageLoad(sender, args) {
              var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
              for (var selector in config) { $(selector).chosen(config[selector]); }
          }
          //-------------------------------------
          function validate() {
              var NgaySoSanh = '<%= NgaySoSanh%>';

            var txtSoQD = document.getElementById('<%=txtSoQD.ClientID%>');
            if (!Common_CheckTextBox(txtSoQD, "Số quyết định"))
                return false;
           
            //-----------------------------
            var txtNgayQD = document.getElementById('<%=txtNgayQD.ClientID%>');
            if (!CheckDateTimeControl(txtNgayQD, 'Ngày quyết định'))
                return false;

            if (!SoSanh2Date(txtNgayQD,'Ngày quyết định thi hành án', NgaySoSanh, 'Ngày thụ lý'))
                return false;
            //-----------------------------
            var txtNgayTHA = document.getElementById('<%=txtNgayTHA.ClientID%>');
            if (!CheckDateTimeControl(txtNgayTHA, 'Ngày thi hành án'))
                return false;
            if (!SoSanh2Date(txtNgayTHA, 'Ngày thi hành án', NgaySoSanh, 'Ngày thụ lý'))
                return false;

            var ngay_qd = txtNgayQD.value;
            if (!SoSanh2Date(txtNgayTHA, 'Ngày thi hành án', ngay_qd, 'Ngày quyết định thi hành án'))
                return false;

            //----------------------------- 
            var dropNguoiKy = document.getElementById('<%=dropNguoiKy.ClientID%>');
            value_change = dropNguoiKy.options[dropNguoiKy.selectedIndex].value;
            if (value_change == "") {
                alert('Bạn chưa chọn Người ký văn bản Quyết định thi hành án . Hãy kiểm tra lại!');
                dropNguoiKy.focus();
                return false;
            }

            var txtChucVu = document.getElementById('<%=txtChucVu.ClientID%>');
            if (!Common_CheckTextBox(txtChucVu, "chức vụ của người ký"))
                return false;
            return true;
          }
          function ChangeHP(hinhphatid, grouphp) {
              var hddGroupChange = document.getElementById('<%=hddGroupChange.ClientID%>');
            var hddHinhPhatChange = document.getElementById('<%=hddHinhPhatChange.ClientID%>');

            var hdd_value = hddHinhPhatChange.value;
            if (hdd_value == hinhphatid) {
                hddHinhPhatChange.value = "0";
                hddGroupChange.value = "0";
            }
            else {
                hddHinhPhatChange.value = hinhphatid;
                hddGroupChange.value = grouphp;
            }
        }
    </script>

    <script type="text/javascript">
        var count_file = 0;
        function VerifyPDFCallBack(rv) {

        }

        function exc_verify_pdf1() {
            var prms = {};
            var hddSession = document.getElementById('<%=hddSessionID.ClientID%>');
            prms["SessionId"] = "";
            prms["FileName"] = document.getElementById("file1").value;

            var json_prms = JSON.stringify(prms);

            vgca_verify_pdf(json_prms, VerifyPDFCallBack);
        }

        function SignFileCallBack1(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                var hddFilePath = document.getElementById('<%=hddFilePath.ClientID%>');
                var new_item = document.createElement("li");
                new_item.innerHTML = received_msg.FileName;
                hddFilePath.value = received_msg.FileServer;
                //-------------Them icon xoa file------------------
                var del_item = document.createElement("img");
                del_item.src = '/UI/img/xoa.gif';
                del_item.style.width = "15px";
                del_item.style.margin = "5px 0 0 5px";
                del_item.onclick = function () {
                    if (!confirm('Bạn muốn xóa file này?')) return false;
                    document.getElementById("file_name").removeChild(new_item);
                }
                del_item.style.cursor = 'pointer';
                new_item.appendChild(del_item);

                document.getElementById("file_name").appendChild(new_item);
            } else {
                document.getElementById("_signature").value = received_msg.Message;
            }
        }

        //metadata có kiểu List<KeyValue> 
        //KeyValue là class { string Key; string Value; }
        function exc_sign_file1() {
            var prms = {};
            var scv = [{ "Key": "abc", "Value": "abc" }];
            var hddURLKS = document.getElementById('<%=hddURLKS.ClientID%>');
            prms["FileUploadHandler"] = hddURLKS.value.replace(/^http:\/\//i, window.location.protocol + '//');
            prms["SessionId"] = "";
            prms["FileName"] = "";
            prms["MetaData"] = scv;
            var json_prms = JSON.stringify(prms);
            vgca_sign_file(json_prms, SignFileCallBack1);
        }
        function RequestLicenseCallBack(rv) {
            var received_msg = JSON.parse(rv);
            if (received_msg.Status == 0) {
                document.getElementById("_signature").value = received_msg.LicenseRequest;
            } else {
                alert("Ký số không thành công:" + received_msg.Status + ":" + received_msg.Error);
            }
        }
       function SetNgayTHA() {
            var NgaySoSanh = '<%= NgaySoSanh%>';
          
            var txtNgayQD = document.getElementById('<%=txtNgayQD.ClientID%>');
            var txtNgayTHA = document.getElementById('<%=txtNgayTHA.ClientID%>');

            if (!CheckDateTimeControl(txtNgayQD, 'Ngày quyết định thi hành án'))
                return false;
            if (!SoSanh2Date(txtNgayQD, 'Ngày quyết định thi hành án', NgaySoSanh, 'Ngày thụ lý'))
                return false;

            if (Common_CheckEmpty(txtNgayQD.value)) 
                txtNgayTHA.value = txtNgayQD.value;
            return true;
        }
</script>
</asp:Content>
