﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.GSTP.QLAN.GDTTT.In;
using BL.GSTP.GDTTT;

namespace WEB.GSTP.QLAN.GDTTT.Hoso
{
    public partial class Danhsachdon_cc : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        Decimal PhongBanID = 0, CurrDonViID = 0;
        private const decimal ROOT = 0;
        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            { return false; }
        }
        public string GetDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return Convert.ToDateTime(obj).ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            PhongBanID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_PHONGBANID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_PHONGBANID]);
            CurrDonViID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_DONVIID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnNBInTBC);
            scriptManager.RegisterPostBackControl(this.btnNBInTotrinh);
            scriptManager.RegisterPostBackControl(this.btnNBInDSTP);
            scriptManager.RegisterPostBackControl(this.btnNBInDS);
            scriptManager.RegisterPostBackControl(this.btnNBDSChuaDDK);
            scriptManager.RegisterPostBackControl(this.btnNBInDSTrung);
            scriptManager.RegisterPostBackControl(this.btnTKDanhsach);
            scriptManager.RegisterPostBackControl(this.btnNTADanhsach);
            scriptManager.RegisterPostBackControl(this.btnNBIn_KQ_Quoc_Hoi);
            scriptManager.RegisterPostBackControl(this.btnTKCoquan);
            scriptManager.RegisterPostBackControl(this.btnGXN);
            scriptManager.RegisterPostBackControl(this.btnNBInThongbao);
            scriptManager.RegisterPostBackControl(this.btnNBTieuhoso);
            scriptManager.RegisterPostBackControl(this.btnIndanhsach);

            //-----------------
            if (!IsPostBack)
            {

                if ((Session["TTBCVISIBLE"] + "") == "1")
                {
                    lbtTTBC.Text = "[ Đóng ]";
                    pnTTBC.Visible = true;
                }
                if ((Session["TTTKVISIBLE"] + "") == "0")
                {
                    lbtTTTK.Text = "[ Thu gọn ]";
                    pnTTTK.Visible = true;
                }
                //---------Văn thư - văn bản đến
                if ((Session["VT_VBD_VISIBLE"] + "") == "1")
                {
                    lbt_vt_vbd.Text = "[ Đóng ]";
                    pn_VT_VBD.Visible = true;
                }
                if ((Session["VT_VBD_VISIBLE"] + "") == "0")
                {
                    lbt_vt_vbd.Text = "[ Thu gọn ]";
                    pn_VT_VBD.Visible = true;
                }
                Load_Noi_NhanSearch();
                LoadDropTinh();
                LoadLoaiAn(Convert.ToDecimal(ddlPhongban.SelectedValue));
                SetGetSessionTK(false);

                if (Session[SS_TK.SOBAQD] != null)
                    Load_Data();
                //if (ddlTrangthaichuyen.SelectedValue == "0")
                //    Cls_Comon.SetButton(cmdSua, true);
                //else
                //    Cls_Comon.SetButton(cmdSua, false);
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
                Cls_Comon.SetButton(btnChuyendon, oPer.CAPNHAT);
                Cls_Comon.SetButton(btnThuHoi, oPer.CAPNHAT);
                Cls_Comon.SetButton(cmdSua, oPer.CAPNHAT);
                Cls_Comon.SetButton(cmdGopdon, oPer.CAPNHAT);
                //--------
                if (Session[ENUM_SESSION.SESSION_USERNAME] + "" == "tc.vanphonga")
                {
                    kq_QuocHoi.Visible = true;
                }
                //ẩn nút chuyển đơn
                btnChuyendon.Enabled = false;
                btnChuyendon.CssClass = "buttonprintdisable";
                //khi được phân quyền chức năng nhận văn bản thì mới được hiển thì group tìm kiếm văn thư - và được gán vào quyền xem
                MenuPermission oPer_vanthuden = Cls_Comon.GetMenuPer("/QLAN/GDTTT/VT_DEN/Van_ban_den_form.aspx", Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                pn_vanthu.Visible = oPer_vanthuden.XEM;
            }

        }
        protected void Load_Noi_NhanSearch()
        {
            Drop_NOI_NHAN_SEARCH.Items.Clear();
            Drop_NOI_NHAN_SEARCH.Items.Add(new ListItem("Văn Thư", Session[ENUM_SESSION.SESSION_DONVIID] + ""));//giá trị = 1 chỉ là để khác null để check
            //---------------
            Drop_NOI_NHAN_SEARCH.Items.Insert(0, new ListItem("---Tất cả---", ""));
        }
        protected void cmd_xuly_vt_vbd_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img = (ImageButton)sender;
            String[] ND_id_arr = img.CommandArgument.ToString().Split(';');
            String _Id = ND_id_arr[0] + "";
            String _VANBANDEN_ID = ND_id_arr[1] + "";
            Session[SS_TK.ISHOME] = 1;
            Session["DONID_CC"] = _Id;
            Session["VUVIECID_CC"] = null;
            Response.Redirect("Thongtindon_cc.aspx?ID=" + _Id + "&vt_id=" + _VANBANDEN_ID);
        }
        private void SetGetSessionTK(bool isSet)
        {
            try
            {
                if (isSet)
                {
                    decimal isDonGoc = 1;
                    if (txtSohieudon.Text != "" || txtThuly_So.Text != ""
                        || //txtNgaynhapTu.Text !="" || txtNgaynhapDen.Text !="" ||
                       ddlPhanloaiDdon.SelectedValue == "2")
                        isDonGoc = 0;
                    Session[SS_TK.PHANLOAIDON] = ddlPhanloaiDdon.SelectedValue;
                    Session[SS_TK.ISDONGOC] = isDonGoc;
                    Session[SS_TK.ISTUHINH] = ddlAnTuHinh.SelectedValue;
                    Session[SS_TK.TOAANXX] = ddlToaXetXu.SelectedValue;
                    Session[SS_TK.SOBAQD] = txtSoQDBA.Text;
                    Session[SS_TK.NGAYBAQD] = txtNgayBAQD.Text;
                    Session[SS_TK.NGUOIGUI] = txtNguoigui.Text;
                    Session[SS_TK.SOCMND] = txtSoCMND.Text;
                    Session[SS_TK.NGAYNHANTU] = txtNgayNhanTu.Text;
                    Session[SS_TK.NGAYNHANDEN] = txtNgayNhanDen.Text;
                    Session[SS_TK.HINHTHUCDON] = ddlHinhthucdon.SelectedValue;
                    Session[SS_TK.MADON] = txtSohieudon.Text;
                    if (ddlHuyen.SelectedValue == "0")
                    {
                        Session[SS_TK.TINHID] = 0;
                        Session[SS_TK.HUYENID] = 0;
                    }
                    else
                    {
                        decimal TinhHuyenID = Convert.ToDecimal(ddlHuyen.SelectedValue);
                        DM_HANHCHINH oDMHC = dt.DM_HANHCHINH.Where(x => x.ID == TinhHuyenID).FirstOrDefault();
                        if (oDMHC.LOAI == 1)//TỈnh
                        {
                            Session[SS_TK.TINHID] = oDMHC.ID.ToString();
                            Session[SS_TK.HUYENID] = 0;
                        }
                        else
                        {
                            Session[SS_TK.TINHID] = oDMHC.CAPCHAID;
                            Session[SS_TK.HUYENID] = oDMHC.ID;
                        }
                    }
                    Session[SS_TK.DIACHICHITIET] = txtDiachi.Text;
                    Session[SS_TK.SOCV] = txtCV_So.Text;
                    Session[SS_TK.NGAYCV] = txtCV_Ngay.Text;
                    Session[SS_TK.TRALOIDON] = ddlTraloi.SelectedValue;
                    Session[SS_TK.LOAICHUYEN] = ddlNoichuyenden.SelectedValue;
                    Session[SS_TK.TRANGTHAICHUYEN] = ddlTrangthaichuyen.SelectedValue;
                    Session[SS_TK.PHONGBANCHUYEN] = ddlPhongban.SelectedValue;
                    Session[SS_TK.LOAIAN] = ddlLoaiAn.SelectedValue;
                    Session[SS_TK.DIEUKIENCHUYEN] = ddlTrangthaidon.SelectedValue;
                    Session[SS_TK.NGAYCHUYENTU] = txtNgaychuyenTu.Text;
                    Session[SS_TK.NGAYCHUYENDEN] = txtNgaychuyenDen.Text;
                    Session[SS_TK.TOAKHACID] = ddlToaKhac.SelectedValue;
                    Session[SS_TK.TENNGOAITOAAN] = txtNgoaitoaan.Text;
                    Session[SS_TK.THULYDON] = ddlThuLy.SelectedValue;
                    Session[SS_TK.CHANHANCHIDAO] = ddlChidao.SelectedValue;
                    Session[SS_TK.TRAIGIAM] = ddlTraigiam.SelectedValue;
                    Session[SS_TK.THAMPHAN] = ddlThamphan.SelectedValue;
                    Session[SS_TK.LOAICV] = ddlLoaiCV.SelectedValue;
                    Session[SS_TK.BC_SOCV] = txtBC_SoCV.Text;
                    Session[SS_TK.BC_NGAYCV] = txtBC_Ngaydk.Text;
                    Session[SS_TK.BC_NGUOIKY] = txtBC_Nguoiky.Text;
                    Session[SS_TK.NGAYNHAPTU] = txtNgaynhapTu.Text;
                    Session[SS_TK.NGAYNHAPDEN] = txtNgaynhapDen.Text;
                    Session[SS_TK.THULY_TU] = txtThuly_Tu.Text;
                    Session[SS_TK.THULY_DEN] = txtThuly_Den.Text;
                    Session[SS_TK.SOTHULY] = txtThuly_So.Text;
                    string vArrSelectID = "";
                    foreach (DataGridItem Item in dgList.Items)
                    {
                        CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                        if (chkChon.Checked)
                        {
                            if (vArrSelectID == "") vArrSelectID = chkChon.ToolTip;
                            else vArrSelectID = vArrSelectID + "," + chkChon.ToolTip;
                        }
                    }
                    if (vArrSelectID != "") vArrSelectID = "," + vArrSelectID + ",";
                    //Người nhập
                    string strNguoiNhap = "";
                    foreach (ListItem i in chkNguoinhap.Items)
                    {
                        if (i.Selected)
                        {
                            if (strNguoiNhap == "")
                                strNguoiNhap = i.Value;
                            else
                                strNguoiNhap = strNguoiNhap + "," + i.Value;
                        }
                    }
                    if (strNguoiNhap != "") strNguoiNhap = "," + strNguoiNhap + ",";
                    Session[SS_TK.ARRSELECTID] = vArrSelectID;
                    Session[SS_TK.NGUOINHAP] = strNguoiNhap;

                    Session[SS_TK.CVPC_SO] = txtCVPC_So.Text;
                    Session[SS_TK.CVPC_NGAY] = txtCVPC_Ngay.Text;
                    Session[SS_TK.CVPC_TenCQ] = txtCVPC_TenCQ.Text;
                    Session[SS_TK.GUITOI_CA_TA] = ddlChuyentoi.SelectedValue;
                    //--------------------------------
                    Session[SS_TK.NOI_NHAN_SEARCH] = Drop_NOI_NHAN_SEARCH.SelectedValue;
                    Session[SS_TK.TRANG_THAI_XLY_VT] = Drop_TRANGTHAICHUYEN.SelectedValue;
                    Session[SS_TK.LOAI_VB] = Drop_LOAI_VB_Search.SelectedValue;
                    Session[SS_TK.SODEN] = txt_SODEN_SEARCH.Text.Trim();
                    Session[SS_TK.SODEN_DEN] = txt_SODEN_SEARCH_DEN.Text.Trim();
                    Session[SS_TK.NGAY_FROM] = txt_NGAY_FROM.Text.Trim();
                    Session[SS_TK.NGAY_FROM_DEN] = txt_NGAY_TO.Text.Trim();
                    Session[SS_TK.NGUOI_GUI_BT] = txt_NGUOI_GUI_BT_SEARCH.Text.Trim();
                }
                else
                {
                    int IsHome = (string.IsNullOrEmpty(Session[SS_TK.ISHOME] + "")) ? 0 : Convert.ToInt16(Session[SS_TK.ISHOME] + "");
                    if (IsHome == 1)
                    {
                        ddlPhanloaiDdon.SelectedValue = Session[SS_TK.PHANLOAIDON] + "";
                        txtNguoigui.Text = Session[SS_TK.NGUOIGUI] + "";
                        txtSoQDBA.Text = Session[SS_TK.SOBAQD] + "";
                        txtNgayBAQD.Text = Session[SS_TK.NGAYBAQD] + "";
                        if (Session[SS_TK.TOAANXX] != null) ddlToaXetXu.SelectedValue = Session[SS_TK.TOAANXX] + "";
                        txtNgayNhanTu.Text = Session[SS_TK.NGAYNHANTU] + "";
                        txtNgayNhanDen.Text = Session[SS_TK.NGAYNHANDEN] + "";
                        if (Session[SS_TK.LOAICHUYEN] != null) ddlNoichuyenden.SelectedValue = Session[SS_TK.LOAICHUYEN] + "";
                        ddlNoichuyenden_SelectedIndexChanged(null, null);
                        if (Session[SS_TK.PHONGBANCHUYEN] != null) ddlPhongban.SelectedValue = Session[SS_TK.PHONGBANCHUYEN] + "";
                        if (Session[SS_TK.LOAIAN] != null) ddlLoaiAn.SelectedValue = Session[SS_TK.LOAIAN] + "";

                        if (Session[SS_TK.DIEUKIENCHUYEN] != null) ddlTrangthaidon.SelectedValue = Session[SS_TK.DIEUKIENCHUYEN] + "";
                        txtNgaychuyenTu.Text = Session[SS_TK.NGAYCHUYENTU] + "";
                        txtNgaychuyenDen.Text = Session[SS_TK.NGAYCHUYENDEN] + "";
                        if (Session[SS_TK.TOAKHACID] != null) ddlToaKhac.SelectedValue = Session[SS_TK.TOAKHACID] + "";
                        txtNgoaitoaan.Text = Session[SS_TK.TENNGOAITOAAN] + "";
                        if (Session[SS_TK.THULYDON] != null) ddlThuLy.SelectedValue = Session[SS_TK.THULYDON] + "";
                        if (Session[SS_TK.HINHTHUCDON] != null) ddlHinhthucdon.SelectedValue = Session[SS_TK.HINHTHUCDON] + "";
                        txtSoCMND.Text = Session[SS_TK.SOCMND] + "";
                        txtSohieudon.Text = Session[SS_TK.MADON] + "";
                        string strTinhID = Session[SS_TK.TINHID] + "";
                        string strHuyenID = Session[SS_TK.HUYENID] + "";
                        if (strHuyenID != "" && strHuyenID != "0")
                            ddlHuyen.SelectedValue = strHuyenID;
                        else
                        {
                            if (strTinhID != "" && strTinhID != "0")
                                ddlHuyen.SelectedValue = strTinhID;
                        }
                        txtDiachi.Text = Session[SS_TK.DIACHICHITIET] + "";
                        if (Session[SS_TK.TRALOIDON] != null) ddlTraloi.SelectedValue = Session[SS_TK.TRALOIDON] + "";

                        txtCV_So.Text = Session[SS_TK.SOCV] + "";
                        txtCV_Ngay.Text = Session[SS_TK.NGAYCV] + "";
                        if (Session[SS_TK.TRANGTHAICHUYEN] != null) ddlTrangthaichuyen.SelectedValue = Session[SS_TK.TRANGTHAICHUYEN] + "";

                        if (Session[SS_TK.CHANHANCHIDAO] != null) ddlChidao.SelectedValue = Session[SS_TK.CHANHANCHIDAO] + "";
                        if (Session[SS_TK.TRAIGIAM] != null) ddlTraigiam.SelectedValue = Session[SS_TK.TRAIGIAM] + "";

                        txtBC_SoCV.Text = Session[SS_TK.BC_SOCV] + "";
                        txtBC_Ngaydk.Text = Session[SS_TK.BC_NGAYCV] + "";
                        txtBC_Nguoiky.Text = Session[SS_TK.BC_NGUOIKY] + "";

                        txtNgaynhapTu.Text = Session[SS_TK.NGAYNHAPTU] + "";
                        txtNgaynhapDen.Text = Session[SS_TK.NGAYNHAPDEN] + "";
                        if (Session[SS_TK.THAMPHAN] != null) ddlThamphan.SelectedValue = Session[SS_TK.THAMPHAN] + "";
                        if (Session[SS_TK.LOAICV] != null) ddlLoaiCV.SelectedValue = Session[SS_TK.LOAICV] + "";
                        if (Session[SS_TK.ISTUHINH] != null) ddlAnTuHinh.SelectedValue = Session[SS_TK.ISTUHINH] + "";

                        txtThuly_Tu.Text = Session[SS_TK.THULY_TU] + "";
                        txtThuly_Den.Text = Session[SS_TK.THULY_DEN] + "";
                        txtThuly_So.Text = Session[SS_TK.SOTHULY] + "";
                        string strNguoiNhap = Session[SS_TK.NGUOINHAP] + "";
                        foreach (ListItem i in chkNguoinhap.Items)
                        {
                            if (strNguoiNhap.Contains("," + i.Value + ",")) i.Selected = true;
                        }
                        txtCVPC_So.Text = Session[SS_TK.CVPC_SO] + "";
                        txtCVPC_Ngay.Text = Session[SS_TK.CVPC_NGAY] + "";
                        txtCVPC_TenCQ.Text = Session[SS_TK.CVPC_TenCQ] + "";
                        if (Session[SS_TK.GUITOI_CA_TA] != null) ddlChuyentoi.SelectedValue = Session[SS_TK.GUITOI_CA_TA] + "";

                        Drop_NOI_NHAN_SEARCH.SelectedValue = Session[SS_TK.NOI_NHAN_SEARCH] + "";
                        Drop_TRANGTHAICHUYEN.SelectedValue = Session[SS_TK.TRANG_THAI_XLY_VT] + "";
                        Drop_LOAI_VB_Search.SelectedValue = Session[SS_TK.LOAI_VB] + "";

                        txt_SODEN_SEARCH.Text = Session[SS_TK.SODEN] + "";
                        txt_SODEN_SEARCH_DEN.Text = Session[SS_TK.SODEN_DEN] + "";
                        txt_NGAY_FROM.Text = Session[SS_TK.NGAY_FROM] + "";
                        txt_NGAY_TO.Text = Session[SS_TK.NGAY_FROM_DEN] + "";
                        txt_NGUOI_GUI_BT_SEARCH.Text = Session[SS_TK.NGUOI_GUI_BT] + "";
                        //--------------------------------
                        ShowButtonPrint();
                    }
                }
            }
            catch (Exception ex) { }
        }
        private DataTable getDS(bool isCV, bool isOnPrint, bool isTraigiam, bool isChiDao, bool isTBQuahan)
        {
            int IsHome = (string.IsNullOrEmpty(Session[SS_TK.ISHOME] + "")) ? 0 : Convert.ToInt16(Session[SS_TK.ISHOME] + "");
            decimal isDonGoc = 1;
            if (txtSohieudon.Text != "" || txtThuly_So.Text != ""
                ||// txtNgaynhapTu.Text != "" || txtNgaynhapDen.Text != "" ||
                ddlPhanloaiDdon.SelectedValue == "2")
                isDonGoc = 0;
            Session[SS_TK.ISDONGOC] = isDonGoc;
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            decimal ToaRaBAQD = Convert.ToDecimal(ddlToaXetXu.SelectedValue),
                HinhThucDon = Convert.ToDecimal(ddlHinhthucdon.SelectedValue), DiaChiTinh = 0,
                DiaChiHuyen = 0, TraLoi = Convert.ToDecimal(ddlTraloi.SelectedValue);
            string SoBAQD = txtSoQDBA.Text.Trim(), NgayBAQD = txtNgayBAQD.Text, NguoiGui = txtNguoigui.Text.Trim(),
                SoCMND = txtSoCMND.Text.Trim(), SoHieuDon = txtSohieudon.Text.Trim(), DiaChiCT = txtDiachi.Text.Trim(),
                SoCongVan = txtCV_So.Text.Trim(), NgayCongVan = txtCV_Ngay.Text,
                strNguoiNhap = "";
            DateTime? TuNgay = txtNgayNhanTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayNhanTu.Text, cul, DateTimeStyles.NoCurrentDateDefault),
                DenNgay = txtNgayNhanDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayNhanDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            if (ddlHuyen.SelectedValue == "0")
            {
                DiaChiTinh = 0;
                DiaChiHuyen = 0;
            }
            else
            {
                decimal TinhHuyenID = Convert.ToDecimal(ddlHuyen.SelectedValue);
                DM_HANHCHINH oDMHC = dt.DM_HANHCHINH.Where(x => x.ID == TinhHuyenID).FirstOrDefault();
                if (oDMHC.LOAI == 1)//TỈnh
                {
                    DiaChiTinh = (decimal)oDMHC.ID;
                    DiaChiHuyen = 0;
                }
                else
                {
                    DiaChiTinh = (decimal)oDMHC.CAPCHAID;
                    DiaChiHuyen = oDMHC.ID;
                }
            }
            if (isCV) HinhThucDon = 3;
            string vArrSelectID = "";
            if (isOnPrint)
            {
                foreach (DataGridItem Item in dgList.Items)
                {
                    CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                    if (chkChon.Checked)
                    {
                        if (vArrSelectID == "") vArrSelectID = chkChon.ToolTip;
                        else vArrSelectID = vArrSelectID + "," + chkChon.ToolTip;
                    }
                }
            }
            if (vArrSelectID != "") vArrSelectID = "," + vArrSelectID + ",";
            //Người nhập
            foreach (ListItem i in chkNguoinhap.Items)
            {
                if (i.Selected)
                {
                    if (strNguoiNhap == "")
                        strNguoiNhap = i.Value;
                    else
                        strNguoiNhap = strNguoiNhap + "," + i.Value;
                }
            }
            if (strNguoiNhap != "") strNguoiNhap = "," + strNguoiNhap + ",";
            decimal vNoichuyen = Convert.ToDecimal(ddlNoichuyenden.SelectedValue);
            decimal vTrangthai = Convert.ToDecimal(ddlTrangthaichuyen.SelectedValue);
            decimal vCD_DONVIID = 0, vCD_TA_TRANGTHAI = -1;
            if (ddlNoichuyenden.SelectedValue == "0")
            {
                vCD_DONVIID = Convert.ToDecimal(ddlPhongban.SelectedValue);
                vCD_TA_TRANGTHAI = Convert.ToDecimal(ddlTrangthaidon.SelectedValue);
            }
            else if (ddlNoichuyenden.SelectedValue == "1")
            {
                vCD_DONVIID = Convert.ToDecimal(ddlToaKhac.SelectedValue);
            }
            string vCD_TENDONVI = txtNgoaitoaan.Text;
            DateTime? vNgaychuyenTu = txtNgaychuyenTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaychuyenTu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
                , vNgaychuyenDen = txtNgaychuyenDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaychuyenDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            decimal vIsThuLy = Convert.ToDecimal(ddlThuLy.SelectedValue);
            decimal vPhanloaixuly = 0;// Convert.ToDecimal(ddlPhanloaiDdon.SelectedValue);
            if (ddlThuLy.Visible == false) vIsThuLy = -1;
            if (IsHome == 1)//khi người dùng nhấn vào số liệu từ bảng thống kê sau login
            {
                vIsThuLy = Convert.ToDecimal(ddlThuLy.SelectedValue);
            }
            DateTime? vNgayThulyTu = txtThuly_Tu.Text == "" ? (DateTime?)null : DateTime.Parse(txtThuly_Tu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
                , vNgayThulyDen = txtThuly_Den.Text == "" ? (DateTime?)null : DateTime.Parse(txtThuly_Den.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime? vNgayNhapTu = txtNgaynhapTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaynhapTu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
               , vNgayNhapDen = txtNgaynhapDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaynhapDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            //if (vNgayNhapDen != null)
            //{
            //    vNgayNhapDen = ((DateTime)vNgayNhapDen).AddHours(23);
            //}
            string vSoThuly = txtThuly_So.Text;
            decimal vChidao = Convert.ToDecimal(ddlChidao.SelectedValue);
            if (isChiDao) vChidao = 0;
            decimal vTraigiam = Convert.ToDecimal(ddlTraigiam.SelectedValue);
            decimal vTBQuahan = 0, vThamphanID = 0, vThamtravienID = 0, vLoaiCVID = 0, vIsTuHinh = Convert.ToDecimal(ddlAnTuHinh.SelectedValue);
            DateTime vNgayQuahan = DateTime.Now;
            if (isTBQuahan)
            {
                vTBQuahan = 1;
                if (txtBC_Ngaydk.Text != "")
                {
                    vNgayQuahan = DateTime.Parse(txtBC_Ngaydk.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                    if (vNgayQuahan == null) vNgayQuahan = DateTime.Now;
                }
            }
            if (isTraigiam) vTraigiam = 1;
            int page_size = Convert.ToInt32(ddlPageCount.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            vThamphanID = Convert.ToDecimal(ddlThamphan.SelectedValue);
            vLoaiCVID = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
            if (isOnPrint)
            {
                pageindex = 1;
                page_size = 10000;
            }
            decimal vLoaiAn = Convert.ToDecimal(ddlLoaiAn.SelectedValue);
            decimal vGuitoiCA_TA = Convert.ToDecimal(ddlChuyentoi.SelectedValue);
            string vCVPC_So = txtCVPC_So.Text.Trim(), vCVPC_Ngay = txtCVPC_Ngay.Text, vCVPC_TenCQ = txtCVPC_TenCQ.Text.Trim();
            //Dùng kết hợp trong trường hợp chọn số tờ trình
            if ((vNoichuyen == 0 || vNoichuyen == -1) && ddlLOAICVPC.SelectedValue == "1")
            {
                vCD_TENDONVI = "TTR";
            }
            else if (vNoichuyen == 2)
            {
                vCD_TENDONVI = txtNgoaitoaan.Text;
            }
            else
            {
                if (vCD_TENDONVI == "" && ddlLOAICVPC.SelectedValue == "0")
                    vCD_TENDONVI = "CVPC";
            }

            //ddl_LOAI_GDTTT 
            decimal vLOAI_GDTTT = Convert.ToDecimal(ddl_LOAI_GDTTT.SelectedValue);

            DataTable oDT = oBL.GDTTT_DON_SEARCH(Drop_NOI_NHAN_SEARCH.SelectedValue, Drop_TRANGTHAICHUYEN.SelectedValue, Drop_LOAI_VB_Search.SelectedValue, txt_SODEN_SEARCH.Text.Trim(), txt_SODEN_SEARCH_DEN.Text.Trim()
                   , txt_NGAY_FROM.Text.Trim(), txt_NGAY_TO.Text.Trim(), txt_NGUOI_GUI_BT_SEARCH.Text.Trim()
                   , Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui
                   , SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen
                   , DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen
                   , vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                   , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                   vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, vLOAI_GDTTT, pageindex, page_size);
            return oDT;
        }
        private DataTable getDS_BC(Decimal mau_bc, bool isCV, bool isOnPrint, bool isTraigiam, bool isChiDao, bool isTBQuahan)
        {
            int IsHome = (string.IsNullOrEmpty(Session[SS_TK.ISHOME] + "")) ? 0 : Convert.ToInt16(Session[SS_TK.ISHOME] + "");
            decimal isDonGoc = 1;
            if (txtSohieudon.Text != "" || txtThuly_So.Text != ""
                ||// txtNgaynhapTu.Text != "" || txtNgaynhapDen.Text != "" ||
                ddlPhanloaiDdon.SelectedValue == "2")
                isDonGoc = 0;
            Session[SS_TK.ISDONGOC] = isDonGoc;
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            decimal ToaRaBAQD = Convert.ToDecimal(ddlToaXetXu.SelectedValue),
                HinhThucDon = Convert.ToDecimal(ddlHinhthucdon.SelectedValue), DiaChiTinh = 0,
                DiaChiHuyen = 0, TraLoi = Convert.ToDecimal(ddlTraloi.SelectedValue);
            string SoBAQD = txtSoQDBA.Text.Trim(), NgayBAQD = txtNgayBAQD.Text, NguoiGui = txtNguoigui.Text.Trim(),
                SoCMND = txtSoCMND.Text.Trim(), SoHieuDon = txtSohieudon.Text.Trim(), DiaChiCT = txtDiachi.Text.Trim(),
                SoCongVan = txtCV_So.Text.Trim(), NgayCongVan = txtCV_Ngay.Text,
                strNguoiNhap = "";
            DateTime? TuNgay = txtNgayNhanTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayNhanTu.Text, cul, DateTimeStyles.NoCurrentDateDefault),
                DenNgay = txtNgayNhanDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayNhanDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            if (ddlHuyen.SelectedValue == "0")
            {
                DiaChiTinh = 0;
                DiaChiHuyen = 0;
            }
            else
            {
                decimal TinhHuyenID = Convert.ToDecimal(ddlHuyen.SelectedValue);
                DM_HANHCHINH oDMHC = dt.DM_HANHCHINH.Where(x => x.ID == TinhHuyenID).FirstOrDefault();
                if (oDMHC.LOAI == 1)//TỈnh
                {
                    DiaChiTinh = (decimal)oDMHC.ID;
                    DiaChiHuyen = 0;
                }
                else
                {
                    DiaChiTinh = (decimal)oDMHC.CAPCHAID;
                    DiaChiHuyen = oDMHC.ID;
                }
            }
            if (isCV) HinhThucDon = 3;
            string vArrSelectID = "";
            if (isOnPrint)
            {
                //int countSelectedID = 0;
                foreach (DataGridItem Item in dgList.Items)
                {
                    CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                    if (chkChon.Checked)
                    {
                        //countSelectedID = countSelectedID + 1;
                        if (vArrSelectID == "")
                            vArrSelectID = chkChon.ToolTip;
                        else
                            vArrSelectID = vArrSelectID + "," + chkChon.ToolTip;
                    }
                }
                //if (countSelectedID == 0 && (mau_bc == 14 || mau_bc == 4))
                //{
                //    foreach (DataGridItem Item in dgList.Items)
                //    {
                //        CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                //        vArrSelectID = vArrSelectID + "," + chkChon.ToolTip;
                //    }
                //}
                //countSelectedID = 0;
            }
            if (vArrSelectID != "") vArrSelectID = "," + vArrSelectID + ",";
            //Người nhập
            foreach (ListItem i in chkNguoinhap.Items)
            {
                if (i.Selected)
                {
                    if (strNguoiNhap == "")
                        strNguoiNhap = i.Value;
                    else
                        strNguoiNhap = strNguoiNhap + "," + i.Value;
                }
            }
            if (strNguoiNhap != "") strNguoiNhap = "," + strNguoiNhap + ",";
            decimal vNoichuyen = Convert.ToDecimal(ddlNoichuyenden.SelectedValue);
            decimal vTrangthai = Convert.ToDecimal(ddlTrangthaichuyen.SelectedValue);
            decimal vCD_DONVIID = 0, vCD_TA_TRANGTHAI = -1;
            if (ddlNoichuyenden.SelectedValue == "0")
            {
                vCD_DONVIID = Convert.ToDecimal(ddlPhongban.SelectedValue);
                vCD_TA_TRANGTHAI = Convert.ToDecimal(ddlTrangthaidon.SelectedValue);
            }
            else if (ddlNoichuyenden.SelectedValue == "1")
            {
                vCD_DONVIID = Convert.ToDecimal(ddlToaKhac.SelectedValue);
            }
            string vCD_TENDONVI = txtNgoaitoaan.Text;
            DateTime? vNgaychuyenTu = txtNgaychuyenTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaychuyenTu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
                , vNgaychuyenDen = txtNgaychuyenDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaychuyenDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            decimal vIsThuLy = Convert.ToDecimal(ddlThuLy.SelectedValue);
            decimal vPhanloaixuly = 0;// Convert.ToDecimal(ddlPhanloaiDdon.SelectedValue);
            if (ddlThuLy.Visible == false) vIsThuLy = -1;
            if (IsHome == 1)//khi người dùng nhấn vào số liệu từ bảng thống kê sau login
            {
                vIsThuLy = Convert.ToDecimal(ddlThuLy.SelectedValue);
            }
            DateTime? vNgayThulyTu = txtThuly_Tu.Text == "" ? (DateTime?)null : DateTime.Parse(txtThuly_Tu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
                , vNgayThulyDen = txtThuly_Den.Text == "" ? (DateTime?)null : DateTime.Parse(txtThuly_Den.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime? vNgayNhapTu = txtNgaynhapTu.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaynhapTu.Text, cul, DateTimeStyles.NoCurrentDateDefault)
               , vNgayNhapDen = txtNgaynhapDen.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaynhapDen.Text + " 23:59:59", cul, DateTimeStyles.NoCurrentDateDefault);
            //if (vNgayNhapDen != null)
            //{
            //    vNgayNhapDen = ((DateTime)vNgayNhapDen).AddHours(23);
            //}
            string vSoThuly = txtThuly_So.Text;
            decimal vChidao = Convert.ToDecimal(ddlChidao.SelectedValue);
            if (isChiDao) vChidao = 0;
            decimal vTraigiam = Convert.ToDecimal(ddlTraigiam.SelectedValue);
            decimal vTBQuahan = 0, vThamphanID = 0, vThamtravienID = 0, vLoaiCVID = 0, vIsTuHinh = Convert.ToDecimal(ddlAnTuHinh.SelectedValue);
            DateTime vNgayQuahan = DateTime.Now;
            if (isTBQuahan)
            {
                vTBQuahan = 1;
                if (txtBC_Ngaydk.Text != "")
                {
                    vNgayQuahan = DateTime.Parse(txtBC_Ngaydk.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                    if (vNgayQuahan == null) vNgayQuahan = DateTime.Now;
                }
            }
            if (isTraigiam) vTraigiam = 1;
            int page_size = Convert.ToInt32(ddlPageCount.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            vThamphanID = Convert.ToDecimal(ddlThamphan.SelectedValue);
            vLoaiCVID = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
            if (isOnPrint)
            {
                pageindex = 1;
                page_size = 10000;
            }
            decimal vLoaiAn = Convert.ToDecimal(ddlLoaiAn.SelectedValue);
            decimal vGuitoiCA_TA = Convert.ToDecimal(ddlChuyentoi.SelectedValue);
            string vCVPC_So = txtCVPC_So.Text.Trim(), vCVPC_Ngay = txtCVPC_Ngay.Text, vCVPC_TenCQ = txtCVPC_TenCQ.Text.Trim();
            //Dùng kết hợp trong trường hợp chọn số tờ trình
            if ((vNoichuyen == 0 || vNoichuyen == -1) && ddlLOAICVPC.SelectedValue == "1")
            {
                vCD_TENDONVI = "TTR";
            }
            else if (vNoichuyen == 2)
            {
                vCD_TENDONVI = txtNgoaitoaan.Text;
            }
            else
            {
                if (vCD_TENDONVI == "" && ddlLOAICVPC.SelectedValue == "0")
                    vCD_TENDONVI = "CVPC";
            }
            DataTable oDT = new DataTable();
            if (mau_bc == 1)//Phiếu chuyển thụ lý mới
            {
                oDT = oBL.GDTTT_DON_SEARCH_THU_LY_MOI(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                       SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                       DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                       vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                       , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                       vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 2)//Tờ trình phân công
            {
                oDT = oBL.GDTTT_DON_SEARCH_TTRINH_PHANCONG(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                      SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                      DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                      vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                      , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                      vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 3)//DS đơn & TP giải quyết
            {
                oDT = oBL.GDTTT_DON_TP_GIAI_QUYET(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                      SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                      DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                      vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                      , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                      vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 4)//Danh sách thụ lý mới
            {
                //oDT = oBL.GDTTT_DON_DS_TL_MOI(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                //      SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                //      DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                //      vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                //      , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                //      vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);

                //oDT = oBL.GDTTT_DON_DS_TL_MOI(Session[ENUM_SESSION.SESSION_USERID] + "", vArrSelectID);

                oDT = oBL.GDTTT_DON_DS_TL_MOI_CC(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                      SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                      DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                      vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                      , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                      vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 5)//Danh sách đơn chưa đủ điều kiện
            {
                oDT = oBL.GDTTT_DON_DS_DON_CHUA_DU_DK(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                 SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                 DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                 vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                 , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                 vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 6)//Danh sách đơn trùng
            {
                oDT = oBL.GDTTT_DON_TRUNG(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 7)//Danh sách chuyển tòa án khác
            {
                oDT = oBL.GDTTT_DON_CHUYEN_TOA_AN_KHAC(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 8)//Danh sách chuyển ngoài tòa
            {
                oDT = oBL.GDTTT_DON_CHUYEN_NGOAI_TOA(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 9)//Danh sách Kết quả do cơ quan của quốc hội
            {
                oDT = oBL.GDTTT_DON_CHUYEN_CQ_QUOC_HOI(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 10)//In phiếu gửi cơ quan chuyển đơn
            {
                oDT = oBL.GDTTT_GUI_COQUAN_CHUYENDON(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 11)//Giấy xác nhận
            {
                oDT = oBL.GDTTT_DON_SEARCH_GIAY_XAC_NHAN(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 12)//Thong bao yeu cau bo sung don khong du điều kiện
            {
                oDT = oBL.GDTTT_DON_SEARCH_THONG_BAO_YCBSCC(txtBC_Ngaydk.Text, txtBC_Nguoiky.Text, txtBC_SoCV.Text, Session[ENUM_SESSION.SESSION_USERID] + "", ToaAnID, ToaRaBAQD, SoBAQD, NgayBAQD, NguoiGui,
                SoCMND, TuNgay, DenNgay, HinhThucDon, SoHieuDon, DiaChiTinh, DiaChiHuyen,
                DiaChiCT, SoCongVan, NgayCongVan, TraLoi, strNguoiNhap, vNoichuyen,
                vTrangthai, vCD_DONVIID, vCD_TA_TRANGTHAI, vCD_TENDONVI, vNgaychuyenTu, vNgaychuyenDen, vArrSelectID, vIsThuLy, vPhanloaixuly
                , vNgayThulyTu, vNgayThulyDen, vSoThuly, vChidao, vTraigiam, vTBQuahan, vNgayQuahan, vThamphanID,
                vThamtravienID, vLoaiCVID, vNgayNhapTu, vNgayNhapDen, isDonGoc, vIsTuHinh, vLoaiAn, vCVPC_So, vCVPC_Ngay, vCVPC_TenCQ, vGuitoiCA_TA, pageindex, page_size);
            }
            else if (mau_bc == 13)//Tiểu hồ sơ
            {
                oDT = oBL.GDTTT_REPORT_TIEU_HO_SO_CC(vArrSelectID, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            }
            else if (mau_bc == 14)//Danh sách Văn bản hành chính, tài liệu chung
            {
                oDT = oBL.GDTTT_DON_DS_VB_CC(vArrSelectID, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            }
            return oDT;
        }
        private void Load_Data()
        {
            cmdSua.Visible = false;
            cmdGopdon.Visible = false;
            //lbtthongbao.Text = "";
            lbTFirst.Visible = ddlPageCount.Visible = lbBFirst.Visible = ddlPageCount2.Visible = true;
            DataTable oDT = getDS(false, false, false, false, false);
            int count_all = 0;
            if (oDT.Rows.Count > 0)
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
            if (oDT != null && count_all > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(ddlPageCount.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString("#,#", cul) + " </b> đơn trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
                cmdSua.Visible = true;
                cmdGopdon.Visible = true;
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }
            dgList.PageSize = Convert.ToInt32(ddlPageCount.SelectedValue);
            dgList.DataSource = oDT;
            dgList.DataBind();

        }
        //-----
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
            SetGetSessionTK(true);

            Decimal SoCV = String.IsNullOrEmpty(txtCV_So.Text) ? 0 : Convert.ToDecimal(txtCV_So.Text);
            if (SoCV > 0)
            {
                btnChuyendon.Enabled = true;
                btnChuyendon.CssClass = "buttoninput";
            }
            else
            {
                btnChuyendon.Enabled = false;
                btnChuyendon.CssClass = "buttonprintdisable";
            }
        }
        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            ///------------
            GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            Session["DONID_CC"] = oGDTBL.GDTTT_DON_CC_REIDS();
            Session["VUVIECID_CC"] = null;
            //---------
            Response.Redirect("Thongtindon_cc.aspx?type=new");


        }
        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    Session[SS_TK.ISHOME] = 1;
                    Session["DONID_CC"] = e.CommandArgument.ToString();

                    Decimal _Id_d = Convert.ToDecimal(e.CommandArgument.ToString());
                    GDTTT_DON obj = dt.GDTTT_DON.Where(x => x.ID == _Id_d).FirstOrDefault();
                    if (obj.VUVIECID != null && obj.VUVIECID != 0)
                    {
                        Session["VUVIECID_CC"] = obj.VUVIECID;
                    }
                    else
                        Session["VUVIECID_CC"] = null;
                    Response.Redirect("Thongtindon_cc.aspx?ID=" + e.CommandArgument.ToString());
                    break;
                case "Xoa":
                    if (oPer.XOA == false)
                    {
                        lbtthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    String ND_id = e.CommandArgument.ToString();
                    String[] ND_id_arr = ND_id.Split(';');
                    decimal ID = Convert.ToDecimal(ND_id_arr[0]);
                    decimal SODON = Convert.ToDecimal(ND_id_arr[1]);
                    String VANBANDEN_ID = ND_id_arr[2] + "";
                    if (SODON > 1)
                    {
                        lbtthongbao.Text = "Bạn không thể xóa được nhóm đơn này, bạn phải hủy ghép đơn trước khi xóa!";
                        return;
                    }
                    else
                    {
                        GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                        decimal V_IS_DONTRUNG = 1;
                        oBL.CHECK_DONTRUNGID_RETURN(ID, ref V_IS_DONTRUNG);
                        if (V_IS_DONTRUNG == 1)
                        {
                            lbtthongbao.Text = "Bạn không thể xóa được đơn này, bạn phải hủy ghép đơn trước khi xóa!";
                            return;
                        }
                        else if (V_IS_DONTRUNG == 0)
                        {
                            GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                            if (oT.THAMPHANID > 0)
                            {
                                lbtthongbao.Text = "Đơn đã phân công thẩm phán giải quyết, không được phép xóa !";
                                return;
                            }
                            if (oT.ISCHUYENXULY == 1)
                            {
                                lbtthongbao.Text = "Đơn đã được chuyển xử lý, không được phép xóa !";
                                return;
                            }
                            if (dt.GDTTT_DON_CHUYEN.Where(x => x.DONID == ID).ToList().Count > 0)
                            {
                                lbtthongbao.Text = "Đơn đã có dữ liệu liên quan, không được phép xóa !";
                                return;
                            }
                            //Nếu đơn của người khác tạo thì không được xóa
                            string strUserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                            if (oT.NGUOITAO.ToLower() != strUserName.ToLower() && strUserName.ToLower() != "tc.vanphong")
                            {
                                lbtthongbao.Text = "Đơn do cán bộ khác thêm mới, không được phép xóa !";
                                return;
                            }
                            foreach (GDTTT_DON t in dt.GDTTT_DON.Where(x => x.DONTRUNGID == oT.ID).ToList())
                            {
                                t.DONTRUNGID = 0;
                                t.SOLUONGDON = 1;
                            }
                            //----------
                            VT_CHUYEN_NHAN_BL M_Object = new VT_CHUYEN_NHAN_BL();
                            if (VANBANDEN_ID != "")
                            {
                                if (M_Object.VT_HUY_NHAN_IN_DELETE_HCTP(VANBANDEN_ID) == true)
                                    lbtthongbao.Text = "Bạn đã xóa đơn và hủy nhận ở Văn Thư Đến thành công.";
                            }
                            else
                            {
                                //---anhvh add xoa ND,BD,NKN, DAN SU, HINH SU
                                XOA_ND_BD_NKN(ID);
                                //--------------
                                dt.GDTTT_DON.Remove(oT);
                                dt.SaveChanges();
                            }
                            dgList.CurrentPageIndex = 0;
                            hddPageIndex.Value = "1";
                            Load_Data();
                        }
                    }
                    break;
                case "SoDonTrung":
                    string StrMsgArr = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Lichsudon.aspx?arrid=" + e.CommandArgument + "','Danh sách đơn trùng',1000,500);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsgArr, true);
                    break;
                case "Lichsu":
                    string StrMsg = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Lichsudon.aspx?vid=" + e.CommandArgument + "','Lịch sử quá trình chuyển đơn',1000,500);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
                    break;
                case "NhieuDonTrung":
                    string strThemDonTrung = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Themdontrung.aspx?vid=" + e.CommandArgument + "','Thêm đơn trùng',950,650);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), strThemDonTrung, true);
                    break;
                case "DonTrung":
                    Response.Redirect("Thongtindon_cc.aspx?type=dontrung&ID=" + e.CommandArgument.ToString());
                    break;
                case "Xemthem":
                    string Strxt = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Noidungdon.aspx?vid=" + e.CommandArgument + "','Nội dung đơn',800,500);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), Strxt, true);
                    break;
                case "KQGQ":
                    string Strkqgqt = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Ketquagiaiquyet.aspx?vid=" + e.CommandArgument + "','Nội dung đơn',600,350);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), Strkqgqt, true);
                    break;
                case "KINHTRINH":
                    string StrKinhtrinh = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Kinhtrinh.aspx?vid=" + e.CommandArgument + "','Nội dung đơn',600,350);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrKinhtrinh, true);
                    break;
                case "BOSUNGTL":
                    string StrBSTL = "PopupReport('/QLAN/GDTTT/Hoso/Popup/BoSungTL.aspx?vid=" + e.CommandArgument + "','Bổ sung tài liệu',800,450);";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrBSTL, true);
                    break;
            }

        }
        protected void XOA_ND_BD_NKN(Decimal ID)
        {
            List<GDTTT_DON_DUONGSU_TOIDANH_CC> lst = dt.GDTTT_DON_DUONGSU_TOIDANH_CC.Where(x => x.DONID == ID).ToList();
            if (lst != null && lst.Count > 0)
            {
                foreach (GDTTT_DON_DUONGSU_TOIDANH_CC item in lst)
                    dt.GDTTT_DON_DUONGSU_TOIDANH_CC.Remove(item);
            }
            List<GDTTT_DON_DS_KN_CC> lkn = dt.GDTTT_DON_DS_KN_CC.Where(x => x.DONID == ID).ToList();
            if (lkn != null && lkn.Count > 0)
            {
                foreach (GDTTT_DON_DS_KN_CC item in lkn)
                    dt.GDTTT_DON_DS_KN_CC.Remove(item);
            }
            List<GDTTT_DON_DUONGSU_CC> lds = dt.GDTTT_DON_DUONGSU_CC.Where(x => x.DONID == ID).ToList();
            if (lds != null && lds.Count > 0)
            {
                foreach (GDTTT_DON_DUONGSU_CC item in lds)
                    dt.GDTTT_DON_DUONGSU_CC.Remove(item);
            }
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            Load_Data();
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            // dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            Load_Data();
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            // dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            Load_Data();
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            //dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            Load_Data();
        }
        protected void ddlPageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount2.SelectedValue = ddlPageCount.SelectedValue;
            //  dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void ddlPageCount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount.SelectedValue = ddlPageCount2.SelectedValue;
            // dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        #endregion
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Decimal ChucDanh_TPTATC = 0;
            try { ChucDanh_TPTATC = dt.DM_DATAITEM.Where(x => x.MA == "TPTATC").FirstOrDefault().ID; } catch (Exception ex) { }
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault();
            //----------------
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkChon = (CheckBox)e.Item.FindControl("chkChon");
                ImageButton lblSua = (ImageButton)e.Item.FindControl("cmdEdit");
                ImageButton lbtXoa = (ImageButton)e.Item.FindControl("cmdXoa");
                LinkButton cmdKQGQ = (LinkButton)e.Item.FindControl("cmdKQGQ");
                LinkButton cmdNhieuDonTrung = (LinkButton)e.Item.FindControl("cmdNhieuDonTrung");
                LinkButton cmdDonTrung = (LinkButton)e.Item.FindControl("cmdDonTrung");
                LinkButton cmdLichsu = (LinkButton)e.Item.FindControl("cmdLichsu");
                LinkButton cmdSoDonTrung = (LinkButton)e.Item.FindControl("cmdSoDonTrung");
                Panel pn_TTGQ = (Panel)e.Item.FindControl("pn_TTGQ");
                Panel pn_TTGQ_VT = (Panel)e.Item.FindControl("pn_TTGQ_VT");
                Panel pn_TTNGDVG = (Panel)e.Item.FindControl("pn_TTNGDVG");
                Panel pn_TTNGDVG_VT = (Panel)e.Item.FindControl("pn_TTNGDVG_VT");
                Panel pn_TTD = (Panel)e.Item.FindControl("pn_TTD");
                Panel pn_TTD_VT = (Panel)e.Item.FindControl("pn_TTD_VT");
                Panel pn_THAOTAC = (Panel)e.Item.FindControl("pn_THAOTAC");
                Panel pn_THAOTAC_VT = (Panel)e.Item.FindControl("pn_THAOTAC_VT");
                Panel pn_TTD_HCTP = (Panel)e.Item.FindControl("pn_TTD_HCTP");
                //------------------
                ImageButton cmd_xuly_vt_vbd = (ImageButton)e.Item.FindControl("cmd_xuly_vt_vbd");
                String TRANG_THAI_XLY = ((DataRowView)e.Item.DataItem)["TRANG_THAI_XLY"].ToString();
                String CANBO_NHAN_ID = ((DataRowView)e.Item.DataItem)["CANBO_NHAN_ID"].ToString();
                String LOAIDON = ((DataRowView)e.Item.DataItem)["LOAIDON"].ToString();
                if (LOAIDON == "5")
                {
                    chkChon.Visible = false;
                }
                else
                {
                    chkChon.Visible = true;
                }
                if (ddlHinhthucdon.SelectedValue == "5")
                {
                    chkChon.Visible = true;
                }
                if (TRANG_THAI_XLY == "3")//du lieu don thu chưa xử lý
                {
                    cmd_xuly_vt_vbd.Visible = true;
                    pn_TTGQ.Visible = false;//Thông tin giải quyết
                    pn_TTGQ_VT.Visible = true;
                    pn_TTNGDVG.Visible = false;
                    pn_TTNGDVG_VT.Visible = true;
                    pn_TTD.Visible = false;
                    pn_TTD_VT.Visible = true;
                    pn_THAOTAC.Visible = false;
                    pn_THAOTAC_VT.Visible = true;
                    cmdSoDonTrung.Enabled = false;
                    pn_TTD_HCTP.Visible = false;
                }
                else
                {
                    cmd_xuly_vt_vbd.Visible = false;
                    pn_TTGQ.Visible = true;
                    pn_TTGQ_VT.Visible = false;
                    pn_TTNGDVG.Visible = true;
                    pn_TTNGDVG_VT.Visible = false;
                    pn_TTD.Visible = true;
                    pn_TTD_VT.Visible = false;
                    pn_THAOTAC.Visible = true;
                    pn_THAOTAC_VT.Visible = false;
                    pn_TTD_HCTP.Visible = true;
                    //-------------------------
                    cmdSoDonTrung.Enabled = true;
                    lblSua.Visible = oPer.CAPNHAT;
                    //-------
                    lbtXoa.Visible = oPer.XOA;
                    //if (CANBO_NHAN_ID != "")//trường hợp đã nhận rồi thì không được xóa
                    //{
                    //    lbtXoa.Visible = false;
                    //    //lbtXoa.ImageUrl = "~/UI/img/delete_dis.png";
                    //}
                    //else
                    //{
                    //    lbtXoa.Visible = oPer.XOA;
                    //    //lbtXoa.ImageUrl = "~/UI/img/delete.png";
                    //}
                    //---------------
                    if (e.Item.Cells[9].Text == null || e.Item.Cells[9].Text == "&nbsp;")
                    {
                        cmdKQGQ.Text = "Nhập kết quả giải quyết";
                    }
                    else if (e.Item.Cells[9].Text != null && e.Item.Cells[9].Text != "&nbsp;")
                    {
                        cmdKQGQ.Text = "<p style='color:#cc0000;'> KQ GQ: " + e.Item.Cells[9].Text + "</p>";//Đã có kết quả giải quyết
                    }
                    //if (ddlTrangthaichuyen.SelectedValue != "0" && ddlTrangthaichuyen.SelectedValue !="4")
                    //{
                    //    lbtXoa.Visible = false;
                    //    lblSua.ToolTip = "Chi tiết";
                    //}
                    cmdNhieuDonTrung.Visible = true;
                    cmdDonTrung.Visible = true;
                    cmdLichsu.Visible = true;
                    if (oCB.CHUCDANHID == ChucDanh_TPTATC)//nếu là thẩm phán
                    {
                        cmdNhieuDonTrung.Visible = false;
                        cmdDonTrung.Visible = false;
                        cmdLichsu.Visible = false;
                        cmdSoDonTrung.Enabled = false;
                        cmdKQGQ.Visible = true;
                    }
                }
            }
        }
        //protected void ddlTinh_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LoadNG_Huyen();
        //    }
        //    catch (Exception ex) { lstSobanghiT.Text = lstSobanghiB.Text = ex.Message; }
        //}
        private void LoadPhongban()
        {
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ddlPhongban.DataSource = dt.DM_PHONGBAN.Where(x => x.TOAANID == ToaAnID && (x.ISGIAIQUYETDON == 1 || x.ISGIAIQUYETDON == 2)).ToList();
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("--Chọn đơn vị--", "0"));
            //Load Loại án

        }
        private void LoadLoaiAn(decimal PBID)
        {
            ddlLoaiAn.Items.Clear();
            if (PBID > 0)
            {
                DM_PHONGBAN obj = dt.DM_PHONGBAN.Where(x => x.ID == PBID).FirstOrDefault();
                if (obj.ISHINHSU == 1) ddlLoaiAn.Items.Add(new ListItem("Hình sự", ENUM_LOAIVUVIEC.AN_HINHSU));
                if (obj.ISDANSU == 1) ddlLoaiAn.Items.Add(new ListItem("Dân sự", ENUM_LOAIVUVIEC.AN_DANSU));
                if (obj.ISHANHCHINH == 1) ddlLoaiAn.Items.Add(new ListItem("Hành chính", ENUM_LOAIVUVIEC.AN_HANHCHINH));
                if (obj.ISHNGD == 1) ddlLoaiAn.Items.Add(new ListItem("Hôn nhân gia đình", ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH));
                if (obj.ISKDTM == 1) ddlLoaiAn.Items.Add(new ListItem("Kinh doanh, thương mại", ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI));
                if (obj.ISLAODONG == 1) ddlLoaiAn.Items.Add(new ListItem("Lao động", ENUM_LOAIVUVIEC.AN_LAODONG));
                if (obj.ISPHASAN == 1) ddlLoaiAn.Items.Add(new ListItem("Phá sản", ENUM_LOAIVUVIEC.AN_PHASAN));
            }
            else
            {
                ddlLoaiAn.Items.Add(new ListItem("Hình sự", ENUM_LOAIVUVIEC.AN_HINHSU));
                ddlLoaiAn.Items.Add(new ListItem("Dân sự", ENUM_LOAIVUVIEC.AN_DANSU));
                ddlLoaiAn.Items.Add(new ListItem("Hành chính", ENUM_LOAIVUVIEC.AN_HANHCHINH));
                ddlLoaiAn.Items.Add(new ListItem("Hôn nhân gia đình", ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH));
                ddlLoaiAn.Items.Add(new ListItem("Kinh doanh, thương mại", ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI));
                ddlLoaiAn.Items.Add(new ListItem("Lao động", ENUM_LOAIVUVIEC.AN_LAODONG));
                ddlLoaiAn.Items.Add(new ListItem("Phá sản", ENUM_LOAIVUVIEC.AN_PHASAN));
            }
            ddlLoaiAn.Items.Add(new ListItem("Chưa xác định", "55"));
            ddlLoaiAn.Items.Insert(0, new ListItem("Tất cả", "0"));
            //----------anhvh add 20/08/2020 check thêm nếu là phó chánh an thì chỉ lấy những loại án của PCA đó-------------------------
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault();
            if (oCB.CHUCDANHID != null && oCB.CHUCDANHID != 0)
            {
                Decimal CurrChucVuID = string.IsNullOrEmpty(oCB.CHUCVUID + "") ? 0 : (decimal)oCB.CHUCVUID;

                DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == oCB.CHUCDANHID).FirstOrDefault();
                if (oCD.MA == "TPTATC")
                {
                    oCD = dt.DM_DATAITEM.Where(x => x.ID == CurrChucVuID).FirstOrDefault();
                    if (oCD != null)
                    {
                        if (oCD.MA == "PCA")
                        {
                            ddlLoaiAn.Items.Clear();
                            LoadLoaiAnPhuTrach(oCB);
                        }
                    }
                }
            }
        }
        void LoadLoaiAnPhuTrach(DM_CANBO obj)
        {
            if (obj.ISDANSU == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Dân sự", ENUM_LOAIVUVIEC.AN_DANSU));
            }
            if (obj.ISHANHCHINH == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Hành chính", ENUM_LOAIVUVIEC.AN_HANHCHINH));
            }
            if (obj.ISHNGD == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Hôn nhân gia đình", ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH));
            }
            if (obj.ISKDTM == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Kinh doanh, thương mại", ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI));
            }
            if (obj.ISLAODONG == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Lao động", ENUM_LOAIVUVIEC.AN_LAODONG));
            }
            if (obj.ISHINHSU == 1)
            {
                ddlLoaiAn.Items.Add(new ListItem("Hình sự", ENUM_LOAIVUVIEC.AN_HINHSU));
            }
        }
        private void LoadDropTinh()
        {
            //List<DM_HANHCHINH> lstTinh = dt.DM_HANHCHINH.Where(x => x.CAPCHAID == ROOT).OrderBy(x => x.THUTU).ToList<DM_HANHCHINH>();
            //if (lstTinh != null && lstTinh.Count > 0)
            //{
            //    ddlTinh.DataSource = lstTinh;
            //    ddlTinh.DataTextField = "TEN";
            //    ddlTinh.DataValueField = "ID";
            //    ddlTinh.DataBind();
            //}
            //ddlTinh.Items.Insert(0, new ListItem("--- Tỉnh/Thành phố ---", "0"));
            LoadNG_Huyen();

            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            QT_NGUOIDUNG_BL oNDBL = new QT_NGUOIDUNG_BL();

            chkNguoinhap.DataSource = oNDBL.QT_NGUOIDUNG_GETBYGDTTT(ToaAnID, 0, 0);
            chkNguoinhap.DataTextField = "USERNAME";
            chkNguoinhap.DataValueField = "USERNAME";
            chkNguoinhap.DataBind();
            LoadPhongban();

            DM_TOAAN_BL oTABL = new DM_TOAAN_BL();
            DataTable dtTA = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);
            ddlToaXetXu.DataSource = dtTA;
            ddlToaXetXu.DataTextField = "MA_TEN";
            ddlToaXetXu.DataValueField = "ID";
            ddlToaXetXu.DataBind();
            ddlToaXetXu.Items.Insert(0, new ListItem("--- Chọn --- ", "0"));

            DataTable dtTA2 = oTABL.DM_TOAAN_GETBYNOTCUR(0, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
            ddlToaKhac.DataSource = dtTA2;
            ddlToaKhac.DataTextField = "MA_TEN";
            ddlToaKhac.DataValueField = "ID";
            ddlToaKhac.DataBind();
            ddlToaKhac.Items.Insert(0, new ListItem("Các tòa địa phương", "-1"));
            ddlToaKhac.Items.Insert(0, new ListItem("--- Chọn tòa án --- ", "0"));
            //Load loại công văn
            DM_DATAITEM_BL cvBL = new DM_DATAITEM_BL();
            DataTable tbl = cvBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.LOAICVGDTTT);
            if (tbl.Rows.Count > 0)
            {
                ddlLoaiCV.DataSource = tbl;
                ddlLoaiCV.DataTextField = "MA_TEN";
                ddlLoaiCV.DataValueField = "ID";
                ddlLoaiCV.DataBind();
            }
            ddlLoaiCV.Items.Insert(0, new ListItem("Tất cả trừ 8.1", "-1"));
            ddlLoaiCV.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
            //Load Thẩm phán
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            DataTable oCBDT = oGDTBL.CANBO_GETBYDONVI_TP(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THAMPHAN, Session[ENUM_SESSION.SESSION_CANBOID] + "");
            //----------------
            Decimal ChucDanh_TPTATC = 0;
            try { ChucDanh_TPTATC = dt.DM_DATAITEM.Where(x => x.MA == "TPTATC").FirstOrDefault().ID; } catch (Exception ex) { }
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault();
            //----------------
            ddlThamphan.DataSource = oCBDT;
            ddlThamphan.DataTextField = "HOTEN";
            ddlThamphan.DataValueField = "ID";
            ddlThamphan.DataBind();
            if (oCB.CHUCDANHID != ChucDanh_TPTATC)
            {
                ddlThamphan.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
            }
            DataTable oCAPCA = oDMCBBL.DM_CANBO_GETBYDONVI_2CHUCVU(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCVU.CHUCVU_CA, ENUM_CHUCVU.CHUCVU_PCA);
            ddlChidao.DataSource = oCAPCA;
            ddlChidao.DataTextField = "MA_TEN";
            ddlChidao.DataValueField = "ID";
            ddlChidao.DataBind();
            //Load thêm Thẩm phán           
            DataTable oTPTATC = oGDTBL.CANBO_GETBYDONVI(ToaAnID, ENUM_CHUCDANH.CHUCDANH_THAMPHAN);
            foreach (DataRow r in oTPTATC.Rows)
            {
                bool isTPTATC = true;
                foreach (DataRow rPCA in oCAPCA.Rows)
                {
                    if ((rPCA["ID"] + "") == (r["ID"] + ""))
                    {
                        isTPTATC = false;
                        break;
                    }
                }
                if (isTPTATC)
                {
                    ddlChidao.Items.Add(new ListItem(r["HOTEN"] + "-Thẩm phán TANDTC", r["ID"] + ""));
                }
            }
            ddlChidao.Items.Insert(0, new ListItem("Tất cả lãnh đạo", "0"));
            ddlChidao.Items.Insert(0, new ListItem("---Tất cả--- ", "-1"));
        }
        private void LoadNG_Huyen()
        {
            ddlHuyen.Items.Clear();

            List<DM_HANHCHINH> lstTinhHuyen;
            if (Session["DMTINHHUYEN"] == null)
                lstTinhHuyen = dt.DM_HANHCHINH.OrderBy(x => x.ARRTHUTU).ToList();
            else
                lstTinhHuyen = (List<DM_HANHCHINH>)(Session["DMTINHHUYEN"]);
            ddlHuyen.DataSource = lstTinhHuyen;
            ddlHuyen.DataTextField = "MA_TEN";
            ddlHuyen.DataValueField = "ID";
            ddlHuyen.DataBind();
            ddlHuyen.Items.Insert(0, new ListItem("Tỉnh/Huyện", "0"));

        }
        protected void ddlNoichuyenden_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlNoichuyenden.SelectedValue)
            {
                case "0":
                    ddlPhongban.Visible = true;
                    ddlTrangthaidon.Visible = true;
                    pnToakhac.Visible = false;
                    txtNgoaitoaan.Visible = false;

                    divPrintNoibo.Visible = true;
                    divPrintToakhac.Visible = false;
                    divPrintNgoaiTA.Visible = false;
                    divPrintTralai.Visible = false;
                    break;
                case "1":
                    ddlPhongban.Visible = false;
                    ddlTrangthaidon.Visible = false;
                    pnToakhac.Visible = true;
                    txtNgoaitoaan.Visible = false;


                    divPrintNoibo.Visible = false;
                    divPrintToakhac.Visible = true;
                    divPrintNgoaiTA.Visible = false;
                    divPrintTralai.Visible = false;
                    break;
                case "2":
                    ddlPhongban.Visible = false;
                    ddlTrangthaidon.Visible = false;
                    pnToakhac.Visible = false;
                    txtNgoaitoaan.Visible = true;


                    divPrintNoibo.Visible = false;
                    divPrintToakhac.Visible = false;
                    divPrintNgoaiTA.Visible = true;
                    divPrintTralai.Visible = false;
                    break;
                case "3":
                    ddlPhongban.Visible = false;
                    ddlTrangthaidon.Visible = false;
                    pnToakhac.Visible = false;
                    txtNgoaitoaan.Visible = false;

                    divPrintNoibo.Visible = false;
                    divPrintToakhac.Visible = false;
                    divPrintNgoaiTA.Visible = false;
                    divPrintTralai.Visible = true;
                    break;
                default:
                    ddlPhongban.Visible = false;
                    ddlTrangthaidon.Visible = false;
                    pnToakhac.Visible = false;
                    txtNgoaitoaan.Visible = false;

                    divPrintNoibo.Visible = false;
                    divPrintToakhac.Visible = false;
                    divPrintNgoaiTA.Visible = false;
                    divPrintTralai.Visible = false;
                    break;

            }
            ShowButtonPrint();
        }
        //---------------CHỨC NĂNG-------------------
        protected void chkChonAll_CheckChange(object sender, EventArgs e)
        {
            CheckBox chkAll = (CheckBox)sender;

            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chk = (CheckBox)Item.FindControl("chkChon");
                chk.Checked = chkAll.Checked;
            }
        }
        protected void chkChon_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chk = (CheckBox)sender;
            decimal ID = Convert.ToDecimal(chk.ToolTip);
            foreach (DataGridItem Item in dgList.Items)
            {


            }
        }
        private void setButtonPrint(bool flag, Button cmd)
        {
            if (flag)
            {
                cmd.Enabled = flag;
                cmd.CssClass = "buttonprint";
            }
            else
            {
                cmd.Enabled = flag;
                cmd.CssClass = "buttonprintdisable";
            }
        }
        private void ShowButtonPrint()
        {

            switch (ddlNoichuyenden.SelectedValue)
            {
                case "0":
                    divKhieunai.Visible = false;
                    divPrintNoibo.Visible = true;
                    decimal IDPhongBan = Convert.ToDecimal(ddlPhongban.SelectedValue);
                    if (IDPhongBan > 0)
                    {
                        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == IDPhongBan).FirstOrDefault();
                        if (oPB.ISGIAIQUYETDON > 1)
                        {
                            divKhieunai.Visible = true;
                            divPrintNoibo.Visible = false;
                        }
                    }
                    setButtonPrint(true, btnNBInDS);
                    setButtonPrint(false, btnNBPhieuchuyen);

                    if (ddlThuLy.SelectedValue == "1")
                    {
                        setButtonPrint(true, btnNBTieuhoso);
                    }
                    else
                    {
                        setButtonPrint(false, btnNBTieuhoso);
                    }

                    if (ddlTrangthaidon.SelectedValue == "0")
                    {
                        if (ddlThuLy.SelectedValue == "1")
                        {
                            setButtonPrint(false, btnNBDSChuaDDK);
                            setButtonPrint(false, btnNBInThongbao);
                            setButtonPrint(false, btnNBInThongbaoTG);
                            setButtonPrint(true, btnNBInCoquanchuyendon);
                            setButtonPrint(false, btnNBTralai);
                            setButtonPrint(true, btnNBInTotrinh);
                            setButtonPrint(true, btnNBInTBC);
                            setButtonPrint(true, btnNBPhieuchuyen);
                            setButtonPrint(true, btnNBInDSTP);
                            setButtonPrint(false, btnNBInDSTrung);
                            setButtonPrint(false, btnNBInPCTrung);
                            setButtonPrint(false, btnNBInTotrinhDT);
                        }
                        else if (ddlThuLy.SelectedValue == "2")
                        {
                            setButtonPrint(false, btnNBInDS);
                            setButtonPrint(false, btnNBDSChuaDDK);
                            setButtonPrint(false, btnNBInThongbao);
                            setButtonPrint(false, btnNBInThongbaoTG);
                            setButtonPrint(true, btnNBInCoquanchuyendon);
                            setButtonPrint(false, btnNBTralai);
                            setButtonPrint(false, btnNBInTotrinh);
                            setButtonPrint(false, btnNBInTBC);
                            setButtonPrint(false, btnNBPhieuchuyen);
                            setButtonPrint(false, btnNBInDSTP);
                            setButtonPrint(true, btnNBInDSTrung);
                            setButtonPrint(true, btnNBInPCTrung);
                            setButtonPrint(true, btnNBInTotrinhDT);
                        }
                        else
                        {
                            setButtonPrint(true, btnNBInDS);
                            setButtonPrint(false, btnNBDSChuaDDK);
                            setButtonPrint(false, btnNBInThongbao);
                            setButtonPrint(false, btnNBInThongbaoTG);
                            setButtonPrint(true, btnNBInCoquanchuyendon);
                            setButtonPrint(false, btnNBTralai);
                            setButtonPrint(true, btnNBInTotrinh);
                            setButtonPrint(true, btnNBInTBC);
                            setButtonPrint(true, btnNBPhieuchuyen);
                            setButtonPrint(true, btnNBInDSTP);
                            setButtonPrint(true, btnNBInDSTrung);
                            setButtonPrint(true, btnNBInPCTrung);
                            setButtonPrint(true, btnNBInTotrinhDT);
                        }
                    }
                    else
                    {
                        setButtonPrint(true, btnNBDSChuaDDK);
                        setButtonPrint(true, btnNBInThongbao);
                        setButtonPrint(true, btnNBInThongbaoTG);
                        setButtonPrint(false, btnNBInCoquanchuyendon);
                        setButtonPrint(true, btnNBTralai);
                        setButtonPrint(false, btnNBInTotrinh);
                        setButtonPrint(false, btnNBInTotrinhDT);
                        setButtonPrint(false, btnNBInTBC);
                        setButtonPrint(false, btnNBPhieuchuyen);
                        setButtonPrint(false, btnNBInDSTP);
                        setButtonPrint(false, btnNBInDSTrung);
                        setButtonPrint(false, btnNBInPCTrung);
                    }

                    break;
                case "1":
                    if (ddlToaKhac.SelectedValue == "0" || ddlTrangthaidon.SelectedValue == "3")
                    {
                        setButtonPrint(true, btnTKPhieuchuyen);
                        setButtonPrint(true, btnTKPhieuchuyenN);
                        setButtonPrint(true, btnTKDanhsach);
                        setButtonPrint(true, btnTKDuongsu);
                        setButtonPrint(true, btnTKCoquan);
                    }
                    else
                    {
                        setButtonPrint(true, btnTKPhieuchuyen);
                        setButtonPrint(true, btnTKPhieuchuyenN);
                        setButtonPrint(true, btnTKDanhsach);
                        setButtonPrint(true, btnTKDuongsu);
                        setButtonPrint(true, btnTKCoquan);
                    }
                    divKhieunai.Visible = false;
                    break;
                case "2":
                case "3":
                    divKhieunai.Visible = false;
                    break;
            }
        }
        protected void ddlPhongban_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowButtonPrint();
            LoadLoaiAn(Convert.ToDecimal(ddlPhongban.SelectedValue));
        }
        protected void ddlToaKhac_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowButtonPrint();
        }
        private string getDiaDiem(decimal ToaAnID)
        {
            try
            {
                string strDiadiem = "";
                DM_TOAAN oT = dt.DM_TOAAN.Where(x => x.ID == ToaAnID).FirstOrDefault();
                strDiadiem = oT.TEN.Replace("Tòa án nhân dân ", "");
                switch (oT.LOAITOA)
                {
                    case "CAPHUYEN":
                        DM_TOAAN opT = dt.DM_TOAAN.Where(x => x.ID == oT.CAPCHAID).FirstOrDefault();
                        strDiadiem = opT.TEN.Replace("Tòa án nhân dân ", "");
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                    case "CAPTINH":
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                    case "CAPCAO":
                        strDiadiem = strDiadiem.Replace("cấp cao", "");
                        strDiadiem = strDiadiem.Replace("tại", "");
                        strDiadiem = strDiadiem.Replace("tỉnh", "");
                        strDiadiem = strDiadiem.Replace("Tỉnh", "");
                        strDiadiem = strDiadiem.Replace("thành phố", "");
                        strDiadiem = strDiadiem.Replace("Thành phố", "");
                        if (strDiadiem.Contains("Hồ Chí Minh"))
                            strDiadiem = "Tp." + strDiadiem;
                        break;
                }
                return strDiadiem;
            }
            catch (Exception ex) { return ""; }
        }
        //In danh sách chuyển ngoài tòa
        protected void btnNTADanhsach_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            String INSERT_PAGE_BREAK = "";
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(8, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                INSERT_PAGE_BREAK = row["INSERT_PAGE_BREAK"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_CHUYEN_NGOAI_TOA.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, INSERT_PAGE_BREAK));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        protected void btnNTADanhsach_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NTA_DANHSACH";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                DTGDTTT objds = new DTGDTTT();
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TT = i.ToString();
                    r.TENDONVI = strTendonvi;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    r.NOIDUNG = oT.NOIDUNGDON + "";
                    r.SO = oT.TB1_SO + "";
                    r.NGUOIKY = oT.CD_NGUOIKY + "";
                    if (oT.TB1_NGAY != null)
                    {
                        DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                        r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                        r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                        r.NAM = dtNTB1.Year.ToString();
                    }
                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    if ((obj["arrCongvan"] + "") != "")
                    {
                        if ((obj["arrCongvan"] + "") != "")
                        {
                            string strCV = obj["arrCongvan"] + ")";
                            strCV = strCV.Replace("; )", "");
                            r.NGUOIGUI = r.NGUOIGUI + " (Do " + strCV + ")";
                        }


                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        r.NGUOIGUI = r.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    r.SODON = obj["SODON"] + "";
                    r.GHICHU = obj["GHICHU"] + "";
                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        //In danh sách kết quả do cơ quan quốc hội
        protected void btnNBInKQ_QuocHoi_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            String INSERT_PAGE_BREAK = "";
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(9, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                INSERT_PAGE_BREAK = row["INSERT_PAGE_BREAK"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_KQ_DO_CQ_QUOC_HOI.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, INSERT_PAGE_BREAK));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        //In danh sách chuyển tòa án khác
        protected void btnTKDanhsach_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            String INSERT_PAGE_BREAK = "";
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(7, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                INSERT_PAGE_BREAK = row["INSERT_PAGE_BREAK"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_CHUYEN_TA_KHAC.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, INSERT_PAGE_BREAK));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        protected void btnTKDanhsach_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYENDS";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                //DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                //r.SOTOTRINH = txtBC_SoCV.Text;
                //r.NGUOIKY = txtBC_Nguoiky.Text;
                //DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                //if (dNgayCV != DateTime.MinValue)
                //{
                //    r.NGAY = dNgayCV.Day.ToString();
                //    r.THANG = dNgayCV.Month.ToString();
                //    r.NAM = dNgayCV.Year.ToString();
                //}
                //r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";             
                //r.TENPHONGBANNHAN = ddlToaKhac.SelectedItem.Text;
                //r.TONGSO = oDT.Rows.Count.ToString();
                //objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                //objds.AcceptChanges();
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NOIBO_DANHSACHRow rds = objds.DT_NOIBO_DANHSACH.NewDT_NOIBO_DANHSACHRow();
                    rds.TT = i.ToString();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    if ((obj["arrCongvan"] + "") != "")
                    {
                        if ((obj["arrCongvan"] + "") != "")
                        {
                            string strCV = obj["arrCongvan"] + ")";
                            strCV = strCV.Replace("; )", "");
                            rds.NGUOIGUI = rds.NGUOIGUI + " (Do " + strCV + ")";
                        }


                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    rds.DIAPHUONG = obj["Diachigui"] + "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        decimal DonID = Convert.ToDecimal(obj["ID"]);
                        GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                        rds.BA_SO = oT.BAQD_SO + "";
                        rds.BA_NGAY = GetDate(oT.BAQD_NGAYBA + "");
                        if (oT.BAQD_TOAANID > 0)
                        {
                            try
                            {
                                DM_TOAAN oTXX = dt.DM_TOAAN.Where(x => x.ID == oT.BAQD_TOAANID).FirstOrDefault();
                                rds.BA_TOAXX = oTXX.MA_TEN;
                            }
                            catch (Exception ex) { }
                        }

                        rds.QD_SO = obj["BAQD_SO"] + "";
                        rds.QD_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.QD_NGAY != "")
                            rds.QD_NGAY = GetDate(rds.QD_NGAY);
                        rds.QD_NGUOIKN = obj["TOAXX"] + "";
                    }
                    rds.SOTOTRINH = obj["CD_SOCV"] + "";
                    rds.NGAYTOTRINH = GetDate(obj["CD_NGAYCV"]);
                    rds.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.SODON = obj["SODON"] + "";
                    rds.GHICHU = obj["GHICHU"] + "";
                    objds.DT_NOIBO_DANHSACH.AddDT_NOIBO_DANHSACHRow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        protected void btnTLCoquan_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "TRALAI_THONGBAOCOQUAN";
            DataTable oDT = getDS(true, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có đơn kèm công văn";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strPBGui = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();

                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));

                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;
                    rds.SOTOTRINH = strSo;
                    rds.NGAY = strNgay;
                    rds.THANG = strThang;
                    rds.NAM = strNam;
                    rds.DIADIEM = strDiadiem;
                    rds.TENPHONGBANGUI = strPBGui;
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    if ((obj["CD_TRALAI_LYDOID"] + "") != "")
                    {
                        if ((obj["CD_TRALAI_LYDOID"] + "") == "0")
                        {
                            rds.LYDO = obj["CD_TRALAI_LYDOKHAC"] + "";
                        }
                        else
                        {
                            try
                            {
                                decimal IDDM = Convert.ToDecimal(obj["CD_TRALAI_LYDOID"]);
                                DM_DATAITEM obji = dt.DM_DATAITEM.Where(x => x.ID == IDDM).FirstOrDefault();
                                rds.LYDO = obji.TEN;
                            }
                            catch (Exception ex) { }
                        }
                    }
                    rds.SOCV = obj["CV_SO"] + "";
                    if (obj["CV_NGAY"] != null)
                        rds.NGAYCV = GetDate(obj["CV_NGAY"]);
                    rds.TENCOQUAN = obj["CV_TENDONVI"] + "";
                    rds.DIACHICOQUAN = obj["CVDIACHI"] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        //Danh sách đơn trùng
        protected void btnNBInDSTrung_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(6, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_TRUNG.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, null));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        protected void btnNBInDSTrung_Click_Older(object sender, EventArgs e)
        {
            {
                SetGetSessionTK(true);
                Session["GDTTT_MABM"] = "NOIBO_TOTRINHDSTRUNG";
                DataTable oDT = getDS(false, true, false, false, false);
                if (oDT != null)
                {
                    if (oDT.Rows.Count == 0)
                    {
                        lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                        return;
                    }
                    #region "Thông tin tờ trình"
                    DTGDTTT objds = new DTGDTTT();
                    DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                    bool isUpdateSCV = false;
                    r.SOTOTRINH = txtBC_SoCV.Text;
                    if (r.SOTOTRINH != "")
                        isUpdateSCV = true;
                    r.NGUOIKY = txtBC_Nguoiky.Text;
                    r.TUNGAY = txtNgaynhapTu.Text;
                    r.DENNGAY = txtNgaynhapDen.Text;
                    string strNgay = "", strThang = "", strNam = "";
                    DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (dNgayCV != DateTime.MinValue)
                    {
                        strNgay = dNgayCV.Day.ToString();
                        strThang = dNgayCV.Month.ToString();
                        strNam = dNgayCV.Year.ToString();
                    }
                    r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    //Phòng ban
                    r.TENPHONGBANGUI = "";
                    decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                    if (oNSD.PHONGBANID != null)
                    {
                        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                        r.TENPHONGBANGUI = oPB.TENPHONGBAN;
                    }
                    if (ddlPhongban.SelectedValue != "0")
                        r.TENPHONGBANNHAN = ddlPhongban.SelectedItem.Text;
                    //ĐỊa điểm
                    string strDiadiem = "";
                    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                        strDiadiem = "Hà Nội";
                    else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                    r.DIADIEM = strDiadiem;
                    #endregion
                    //DANH SÁCH đơn
                    int i = 0;
                    decimal SLDON = 0;
                    foreach (DataRow obj in oDT.Rows)
                    {
                        i += 1;
                        decimal vid = Convert.ToDecimal(obj["ID"]);
                        GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
                        DTGDTTT.DT_NOIBO_DANHSACHRow rds = objds.DT_NOIBO_DANHSACH.NewDT_NOIBO_DANHSACHRow();
                        rds.TT = i.ToString();
                        rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                        rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                        //rds.TUNGAY = txtNgaynhapTu.Text;
                        //rds.DENNGAY = txtNgaynhapDen.Text;

                        //manhnd 02/03/2020 nếu có số CV rồi thì lấy ra dùng
                        if ((obj["CD_SOCV"] + "") != "")
                        {
                            rds.NGUOIKY = obj["CD_NGUOIKY"].ToString();
                            DateTime vNgayCV = Convert.ToDateTime(obj["CD_NGAYCV"]);
                            rds.NGAY = vNgayCV.Day.ToString();
                            rds.THANG = vNgayCV.Month.ToString();
                            rds.NAM = vNgayCV.Year.ToString();
                            rds.SOTOTRINH = obj["CD_SOCV"].ToString();
                        }
                        else
                        {
                            //nếu chưa có số thì để rỗng
                            rds.NGUOIKY = "";
                            rds.NGAY = "";
                            rds.THANG = "";
                            rds.NAM = "";
                            rds.SOTOTRINH = "";
                        }

                        //rds.SOTOTRINH = txtBC_SoCV.Text;
                        //rds.NGAY = strNgay;
                        //rds.THANG = strThang;
                        //rds.NAM = strNam;
                        //rds.NGUOIKY = txtBC_Nguoiky.Text;
                        if ((obj["arrCongvan"] + "") != "")
                        {
                            string strCV = obj["arrCongvan"] + ")";
                            strCV = strCV.Replace("; )", "");
                            rds.NGUOIGUI = rds.NGUOIGUI + " (Do " + strCV + ")";
                        }
                        else if ((obj["LOAIDON"] + "") == "2")
                        {
                            rds.NGUOIGUI = rds.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                        }
                        rds.DIAPHUONG = obj["Diachigui"] + "";

                        if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                        {
                            rds.BA_SO = obj["BAQD_SO"] + "";
                            rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                            if (rds.BA_NGAY != "")
                                rds.BA_NGAY = GetDate(rds.BA_NGAY);
                            rds.BA_TOAXX = obj["TOAXX"] + "";
                        }
                        else//QĐKN
                        {
                            rds.QD_SO = obj["BAQD_SO"] + "";
                            rds.QD_NGAY = obj["BAQD_NGAYBA"] + "";
                            if (rds.QD_NGAY != "")
                                rds.QD_NGAY = GetDate(rds.QD_NGAY);
                            rds.QD_NGUOIKN = obj["TOAXX"] + "";
                        }

                        rds.NGAYNHANDON = obj["NGAYNHANDON"] + "";
                        if (rds.NGAYNHANDON != "")
                            rds.NGAYNHANDON = GetDate(rds.NGAYNHANDON);
                        rds.NGAYTHULY = obj["TL_NGAY"] + "";
                        if (rds.NGAYTHULY != "")
                            rds.NGAYTHULY = GetDate(rds.NGAYTHULY);
                        rds.SOTHULY = obj["TL_SO"] + "";
                        rds.SODON = obj["SODON"] + "";
                        if (rds.SODON != "")
                            SLDON = SLDON + Convert.ToDecimal(rds.SODON);

                        string strISNOTGDTTT = obj["ISNOTGDTTT"] + "";
                        if (strISNOTGDTTT == "1")

                            rds.GHICHU = obj["GHICHU"] + " - " + "(đơn không có nội dung GDT,TT)";
                        else
                            rds.GHICHU = obj["GHICHU"] + "";


                        objds.DT_NOIBO_DANHSACH.AddDT_NOIBO_DANHSACHRow(rds);
                        objds.AcceptChanges();
                        ////manhnd bỏ 02/03/2020 không cho chèn cv tự động
                        //if (isUpdateSCV)
                        //{
                        //    if (d.CD_TRANGTHAI == 0 || d.CD_TRANGTHAI == 3)
                        //    {
                        //        d.CD_SOCV = r.SOTOTRINH;
                        //        d.CD_NGAYCV = dNgayCV;
                        //        d.CD_NGUOIKY = r.NGUOIKY;
                        //        dt.SaveChanges();
                        //        //Update các đơn kèm theo
                        //        try
                        //        {
                        //            string strArrDon = obj["arrDonID"] + "";
                        //            string[] strarr = strArrDon.Split(',');


                        //            if (strarr.Length > 1)
                        //            {
                        //                for (int t = 0; t < strarr.Length; t++)
                        //                {
                        //                    decimal DTID = Convert.ToDecimal(strarr[t]);
                        //                    GDTTT_DON objdtk = dt.GDTTT_DON.Where(x => x.ID == DTID).FirstOrDefault();
                        //                    objdtk.CD_SOCV = r.SOTOTRINH;
                        //                    objdtk.CD_NGAYCV = dNgayCV;
                        //                    objdtk.CD_NGUOIKY = r.NGUOIKY;
                        //                }

                        //            }
                        //        }
                        //        catch (Exception ex) { }
                        //    }
                        //}
                    }
                    dt.SaveChanges();
                    r.TONGSO = SLDON.ToString();
                    objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                    objds.AcceptChanges();
                    Session["NOIBO_DATASET"] = objds;
                }
                string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }
        //Danh sách đơn chưa đủ điều kiện
        protected void btnNBDSChuaDDK_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(5, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_CHUA_DU_DK.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, null));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        protected void btnNBDSChuaDDK_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NOIBO_DSCHUADUDK";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_TRALAIRow rds = objds.DT_TRALAI.NewDT_TRALAIRow();
                    rds.TT = i.ToString();
                    rds.MADON = obj["MADON"] + "";
                    rds.DUONGSU = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    if ((obj["CD_TA_LYDO_ISBAQD"] + "") == "1")//BA
                    {
                        rds.ISBAQD = "X";
                    }
                    if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")//BA
                    {
                        rds.ISXACNHAN = "X";
                    }
                    rds.LYDOKHAC = obj["CD_TA_LYDO_KHAC"] + "";
                    rds.SOTBBS = obj["TB1_SO"] + "";
                    if ((obj["TB1_NGAY"] + "") != null)//BA
                    {
                        rds.NGAYTBBS = GetDate(obj["TB1_NGAY"] + "");
                    }
                    objds.DT_TRALAI.AddDT_TRALAIRow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Danh sách',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        //In danh sách đơn thụ lý mới
        protected void btnNBInDS_Click(object sender, EventArgs e)
        {

            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(4, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_TL_MOI.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, null));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }

        protected void btnNBInDS_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NOIBO_TOTRINHDS";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                bool isUpdateSCV = false;
                r.SOTOTRINH = reStr(txtBC_SoCV.Text);
                if (r.SOTOTRINH != "")
                    isUpdateSCV = true;
                r.NGUOIKY = txtBC_Nguoiky.Text;
                r.TUNGAY = txtNgaynhapTu.Text;
                r.DENNGAY = txtNgaynhapDen.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    r.NGAY = dNgayCV.Day.ToString();
                    r.THANG = dNgayCV.Month.ToString();
                    r.NAM = dNgayCV.Year.ToString();
                }
                r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                //Phòng ban
                r.TENPHONGBANGUI = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    r.TENPHONGBANGUI = oPB.TENPHONGBAN;
                }
                if (ddlPhongban.SelectedValue != "0")
                    r.TENPHONGBANNHAN = ddlPhongban.SelectedItem.Text;
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                r.DIADIEM = strDiadiem;
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                decimal SLDON = 0;

                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    decimal vid = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
                    DTGDTTT.DT_NOIBO_DANHSACHRow rds = objds.DT_NOIBO_DANHSACH.NewDT_NOIBO_DANHSACHRow();

                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.TUNGAY = txtNgaynhapTu.Text;
                    rds.DENNGAY = txtNgaynhapDen.Text;
                    rds.NGUOIKY = txtBC_Nguoiky.Text;
                    if ((obj["arrCongvan"] + "") != "")
                    {

                        if ((obj["arrCongvan"] + "") != "")
                        {
                            string strCV = obj["arrCongvan"] + ")";
                            strCV = strCV.Replace("; )", "");
                            rds.NGUOIGUI = rds.NGUOIGUI + " (Do " + strCV + ")";
                        }
                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    rds.DIAPHUONG = obj["Diachigui"] + "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    rds.NGAYNHANDON = obj["NGAYNHANDON"] + "";
                    if (rds.NGAYNHANDON != "")
                        rds.NGAYNHANDON = GetDate(rds.NGAYNHANDON);
                    rds.NGAYTHULY = obj["TL_NGAY"] + "";
                    if (rds.NGAYTHULY != "")
                        rds.NGAYTHULY = GetDate(rds.NGAYTHULY);
                    rds.SOTHULY = obj["TL_SO"] + "";
                    if (rds.SOTHULY.Length == 1)
                        rds.SOTHULY = "0" + rds.SOTHULY;
                    rds.TT = rds.SOTHULY;
                    if (rds.TT.Length == 1)
                        rds.TT = "000" + rds.TT;
                    else if (rds.TT.Length == 2)
                        rds.TT = "00" + rds.TT;
                    if (rds.TT.Length == 3)
                        rds.TT = "0" + rds.TT;
                    rds.SODON = obj["SODON"] + "";
                    if (rds.SODON != "")
                        SLDON = SLDON + Convert.ToDecimal(rds.SODON);
                    rds.GHICHU = obj["GHICHU"] + "";
                    rds.CD_SOCV = obj["CD_SOCV"] + "";
                    rds.CD_NGAYCV = GetDate(obj["CD_NGAYCV"]);
                    rds.NGAYTAO = DateTime.Parse(obj["NGAYNHAP"] + "", new CultureInfo("en-US"), DateTimeStyles.NoCurrentDateDefault);

                    objds.DT_NOIBO_DANHSACH.AddDT_NOIBO_DANHSACHRow(rds);
                    objds.AcceptChanges();
                    ////manhnd bỏ 02/03/2020 đóng không cho chèn số CV tại chức năng này
                    //if (isUpdateSCV)
                    //{
                    //    if (d.CD_TRANGTHAI == 0 || d.CD_TRANGTHAI == 3)
                    //    {
                    //        d.CD_SOCV = r.SOTOTRINH;
                    //        d.CD_NGAYCV = dNgayCV;
                    //        d.CD_NGUOIKY = r.NGUOIKY;

                    //        //Update các đơn kèm theo
                    //        try
                    //        {
                    //            string strArrDon = obj["arrDonID"] + "";
                    //            if (strArrDon != "")
                    //            {
                    //                string[] strarr = strArrDon.Split(',');

                    //                if (strarr.Length > 1)
                    //                {
                    //                    for (int t = 0; t < strarr.Length; t++)
                    //                    {
                    //                        decimal DTID = Convert.ToDecimal(strarr[t]);
                    //                        GDTTT_DON objdtk = dt.GDTTT_DON.Where(x => x.ID == DTID).FirstOrDefault();
                    //                        objdtk.CD_SOCV = r.SOTOTRINH;
                    //                        objdtk.CD_NGAYCV = dNgayCV;
                    //                        objdtk.CD_NGUOIKY = r.NGUOIKY;
                    //                    }

                    //                }
                    //            }
                    //        }
                    //        catch (Exception ex) { }
                    //    }
                    //}
                }


                dt.SaveChanges();
                r.TONGSO = SLDON.ToString();
                objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        //DS đơn & TP giải quyết
        protected void btnNBInDSTP_Click_Excel(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(3, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_TP_GQ.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write(AddExcelStyling(2, null));   // add the style props to get the page orientation
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
        protected void btnNBInDSTP_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(3, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //--------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=DS_DON_TP_GQ.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/msword";
            HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
            Response.Write("<html");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
            Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content=Microsoft Word 9>");
            Response.Write("<meta name=Originator content=Microsoft Word 9>");
            Response.Write("<style>");
            Response.Write("<!-- /* Style Definitions */" +
                                          "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                          "{margin:0in;" +
                                          "margin-bottom:.0001pt;" +
                                          "mso-pagination:widow-orphan;" +
                                          "tab-stops:center 3.0in right 6.0in;" +
                                          "font-size:12.0pt;}");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section1 {page:Section1;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5905511811in 0.2992125984in 0.5905511811in 0.5984251969in;mso-header: h1;mso-header-margin:.2in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("<style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section2>");//chỉ định khổ giấy
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</div>");
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();

        }
        protected void btnNBInDSTP_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NOIBO_TOTRINHTPGQ";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                r.SOTOTRINH = txtBC_SoCV.Text;
                r.NGUOIKY = txtBC_Nguoiky.Text;
                r.TUNGAY = txtNgaynhapTu.Text;
                r.DENNGAY = txtNgaynhapDen.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    r.NGAY = dNgayCV.Day.ToString();
                    r.THANG = dNgayCV.Month.ToString();
                    r.NAM = dNgayCV.Year.ToString();
                }
                r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                //Phòng ban
                r.TENPHONGBANGUI = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    r.TENPHONGBANGUI = oPB.TENPHONGBAN;
                }
                if (ddlPhongban.SelectedValue != "0")
                    r.TENPHONGBANNHAN = ddlPhongban.SelectedItem.Text;
                r.TONGSO = oDT.Rows.Count.ToString();
                objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                objds.AcceptChanges();
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {

                    decimal vid = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
                    i += 1;
                    DTGDTTT.DT_NOIBO_DANHSACHRow rds = objds.DT_NOIBO_DANHSACH.NewDT_NOIBO_DANHSACHRow();
                    rds.TT = i.ToString();
                    rds.SOTOTRINH = d.CD_SOTOTRINH;
                    if (d.CD_NGAYTOTRINH != null) rds.NGAYTOTRINH = ((DateTime)d.CD_NGAYTOTRINH).ToString("dd/MM/yyyy");
                    rds.NGUOIKY = txtBC_Nguoiky.Text;
                    rds.TUNGAY = txtNgaynhapTu.Text;
                    rds.DENNGAY = txtNgaynhapDen.Text;
                    if (dNgayCV != DateTime.MinValue)
                    {
                        rds.NGAY = r.NGAY;
                        rds.THANG = r.THANG;
                        rds.NAM = r.NAM;
                    }
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    if ((obj["LOAIDON"] + "") == "3")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Do " + obj["CV_TENDONVI"] + " chuyển đến theo Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    rds.DIAPHUONG = obj["Diachigui"] + "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    rds.LOAIAN = obj["BAQD_LOAIAN"] + "";
                    rds.NGAYNHANDON = obj["NGAYNHANDON"] + "";
                    if (rds.NGAYNHANDON != "")
                        rds.NGAYNHANDON = GetDate(rds.NGAYNHANDON);
                    rds.NGAYTHULY = obj["TL_NGAY"] + "";
                    if (rds.NGAYTHULY != "")
                        rds.NGAYTHULY = GetDate(rds.NGAYTHULY);
                    rds.SOTHULY = obj["TL_SO"] + "";
                    if (rds.SOTHULY.Length == 1)
                        rds.SOTHULY = "0" + rds.SOTHULY;
                    rds.SODON = obj["SODON"] + "";
                    rds.TENTHAMPHAN = obj["TENTHAMPHAN"] + "";
                    rds.GHICHU = obj["GHICHU"] + "";
                    if (rds.TENTHAMPHAN != "") rds.TENTHAMPHAN = "Thẩm phán " + rds.TENTHAMPHAN;
                    objds.DT_NOIBO_DANHSACH.AddDT_NOIBO_DANHSACHRow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        private string AddExcelStyling(Int32 landscape, String INSERT_PAGE_BREAK)
        {
            // add the style props to get the page orientation
            StringBuilder sb = new StringBuilder();
            sb.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'\n" +
            "xmlns:x='urn:schemas-microsoft-com:office:excel'\n" +
            "xmlns='http://www.w3.org/TR/REC-html40'>\n" +
            "<head>\n");
            sb.Append("<style>\n");
            sb.Append("@page");
            //page margin can be changed based on requirement.....            
            //sb.Append("{margin:0.5in 0.2992125984in 0.5in 0.5984251969in;\n");
            sb.Append("{margin:0.5905511811in 0.2992125984in 0.5905511811in 0.5984251969in;\n");
            sb.Append("mso-header-margin:.5in;\n");
            sb.Append("mso-footer-margin:.5in;\n");
            if (landscape == 2)//landscape orientation
            {
                sb.Append("mso-page-orientation:landscape;}\n");
            }
            sb.Append("</style>\n");
            sb.Append("<!--[if gte mso 9]><xml>\n");
            sb.Append("<x:ExcelWorkbook>\n");
            sb.Append("<x:ExcelWorksheets>\n");
            sb.Append("<x:ExcelWorksheet>\n");
            sb.Append("<x:Name>Projects 3 </x:Name>\n");
            sb.Append("<x:WorksheetOptions>\n");
            sb.Append("<x:Print>\n");
            sb.Append("<x:ValidPrinterInfo/>\n");
            sb.Append("<x:PaperSizeIndex>9</x:PaperSizeIndex>\n");
            sb.Append("<x:HorizontalResolution>600</x:HorizontalResolution\n");
            sb.Append("<x:VerticalResolution>600</x:VerticalResolution\n");
            sb.Append("</x:Print>\n");
            sb.Append("<x:Selected/>\n");
            sb.Append("<x:DoNotDisplayGridlines/>\n");
            sb.Append("<x:ProtectContents>False</x:ProtectContents>\n");
            sb.Append("<x:ProtectObjects>False</x:ProtectObjects>\n");
            sb.Append("<x:ProtectScenarios>False</x:ProtectScenarios>\n");
            sb.Append("</x:WorksheetOptions>\n");
            //-------------
            if (INSERT_PAGE_BREAK != null)
            {
                sb.Append("<x:PageBreaks> xmlns='urn:schemas-microsoft-com:office:excel'\n");
                sb.Append("<x:RowBreaks>\n");
                String[] rows_ = INSERT_PAGE_BREAK.Split(',');
                String Append_s = "";
                for (int i = 0; i < rows_.Length; i++)
                {
                    Append_s = Append_s + "<x:RowBreak><x:Row>" + rows_[i] + "</x:Row></x:RowBreak>\n";
                }
                sb.Append(Append_s);
                sb.Append("</x:RowBreaks>\n");
                sb.Append("</x:PageBreaks>\n");
            }
            //----------
            sb.Append("</x:ExcelWorksheet>\n");
            sb.Append("</x:ExcelWorksheets>\n");
            sb.Append("<x:WindowHeight>12780</x:WindowHeight>\n");
            sb.Append("<x:WindowWidth>19035</x:WindowWidth>\n");
            sb.Append("<x:WindowTopX>0</x:WindowTopX>\n");
            sb.Append("<x:WindowTopY>15</x:WindowTopY>\n");
            sb.Append("<x:ProtectStructure>False</x:ProtectStructure>\n");
            sb.Append("<x:ProtectWindows>False</x:ProtectWindows>\n");
            sb.Append("</x:ExcelWorkbook>\n");
            sb.Append("</xml><![endif]-->\n");
            sb.Append("</head>\n");
            sb.Append("<body>\n");
            return sb.ToString();
        }
        //Tờ trình phân công
        protected void btnNBInTotrinh_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(2, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //--------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ToTrinhPhanCong.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/msword";
            HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
            Response.Write("<html");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
            Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content=Microsoft Word 9>");
            Response.Write("<meta name=Originator content=Microsoft Word 9>");
            Response.Write("<style>");
            Response.Write("<!-- /* Style Definitions */" +
                                          "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                          "{margin:0in;" +
                                          "margin-bottom:.0001pt;" +
                                          "mso-pagination:widow-orphan;" +
                                          "tab-stops:center 3.0in right 6.0in;" +
                                          "font-size:12.0pt;}");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section1 {page:Section1;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5in 1.0in 0.5in 1.0in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("<style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section1>");//chỉ định khổ giấy
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</div>");
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }
        protected void btnNBInTotrinh_Click_older(object sender, EventArgs e)
        {
            //SetGetSessionTK(true);
            //string strLoaiCV8_1 = "";
            //if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            //{
            //    decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
            //    DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
            //    if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
            //    {
            //        strLoaiCV8_1 = " (do Đại biểu quốc hội, Các Cơ quan của Quốc hội, Đoàn đại biểu Quốc hội chuyển đến)";
            //    }
            //}
            //Session["GDTTT_MABM"] = "NOIBO_TOTRINH01";
            //DataTable oDT = getDS(false, true, false, false, false);
            //if (oDT != null)
            //{
            //    if (oDT.Rows.Count == 0)
            //    {
            //        lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
            //        return;
            //    }
            //    DTGDTTT objds = new DTGDTTT();
            //    //ĐỊa điểm
            //    string strDiadiem = "", strDVGui = "";
            //    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
            //        strDiadiem = "Hà Nội";
            //    else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
            //    decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            //    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
            //    if (oNSD.PHONGBANID != null)
            //    {
            //        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
            //        strDVGui = oPB.TENPHONGBAN;
            //    }
            //    string strNgay = "", strThang = "", strNam = "";
            //    DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //    if (dNgayCV != DateTime.MinValue)
            //    {
            //        strNgay = dNgayCV.Day.ToString();
            //        strThang = dNgayCV.Month.ToString();
            //        strNam = dNgayCV.Year.ToString();
            //    }


            //    DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
            //    r.SOTOTRINH = reStr(txtBC_SoCV.Text);
            //    r.NGUOIKY = txtBC_Nguoiky.Text;
            //    r.NGAY = strNgay;
            //    r.THANG = strThang;
            //    r.NAM = strNam;
            //    r.TUNGAY = txtNgaynhapTu.Text;
            //    r.DENNGAY = txtNgaynhapDen.Text;
            //    if (r.TUNGAY == "") r.TUNGAY = "         ";
            //    if (r.DENNGAY == "") r.DENNGAY = "         ";
            //    r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
            //    //Phòng ban
            //    r.TENPHONGBANGUI = strDVGui;
            //    r.TENPHONGBANNHAN = ddlPhongban.SelectedItem.Text;
            //    r.DIADIEM = strDiadiem;

            //    foreach (DataRow obj in oDT.Rows)
            //    {

            //        decimal vid = Convert.ToDecimal(obj["ID"]);
            //        GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
            //        if (txtBC_SoCV.Text != "")
            //        {
            //            //if (d.CD_TRANGTHAI == 0 || d.CD_TRANGTHAI == 3)
            //            //{
            //            d.CD_SOTOTRINH = reStr(txtBC_SoCV.Text);
            //            d.CD_NGAYTOTRINH = dNgayCV == DateTime.MinValue ? (DateTime?)null : dNgayCV;
            //            dt.SaveChanges();
            //            // }
            //            //Update Các đơn trùng kèm theo
            //            try
            //            {
            //                string strArrDon = obj["arrDonID"] + "";
            //                if (strArrDon.Length > 0)
            //                {
            //                    string[] strarr = strArrDon.Split(',');
            //                    if (strarr.Length > 0)
            //                    {
            //                        for (int k = 0; k < strarr.Length; k++)
            //                        {
            //                            decimal kID = Convert.ToDecimal(Cls_Comon.GetNumber(strarr[k]));
            //                            if (kID > 0)
            //                            {
            //                                GDTTT_DON kDon = dt.GDTTT_DON.Where(x => x.ID == kID).FirstOrDefault();
            //                                //if (kDon.CD_TRANGTHAI == 0 || kDon.CD_TRANGTHAI == 3)
            //                                //{
            //                                kDon.CD_SOTOTRINH = reStr(txtBC_SoCV.Text);
            //                                kDon.CD_NGAYTOTRINH = dNgayCV == DateTime.MinValue ? (DateTime?)null : dNgayCV;
            //                                //}
            //                            }
            //                        }

            //                    }
            //                }
            //            }
            //            catch (Exception ex) { }
            //        }
            //        else
            //        {
            //            r.SOTOTRINH = reStr(d.CD_SOTOTRINH + "");
            //            if (d.CD_NGAYTOTRINH != null)
            //            {
            //                DateTime dNgayTT = (DateTime)d.CD_NGAYTOTRINH;
            //                r.NGAY = Cls_Comon.toFullNumber(dNgayTT.Day.ToString(), false);
            //                r.THANG = Cls_Comon.toFullNumber(dNgayTT.Month.ToString(), true);
            //                r.NAM = dNgayTT.Year.ToString();
            //                r.NGUOIKY = d.CD_NGUOIKY + "";
            //            }
            //        }
            //    }
            //    dt.SaveChanges();
            //    r.LOAIDON8_1 = strLoaiCV8_1;
            //    objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
            //    objds.AcceptChanges();

            //    Session["NOIBO_DATASET"] = objds;

            //    string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            //    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            //}
        }
        //in phiếu chuyển đơn thụ lý mới
        protected void btnNBInTBC_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(1, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //--------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=phieu_chuyen_tl_moi.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/msword";
            HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
            Response.Write("<html");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
            Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content=Microsoft Word 9>");
            Response.Write("<meta name=Originator content=Microsoft Word 9>");
            Response.Write("<style>");
            Response.Write("<!-- /* Style Definitions */" +
                                          "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                          "{margin:0in;" +
                                          "margin-bottom:.0001pt;" +
                                          "mso-pagination:widow-orphan;" +
                                          "tab-stops:center 3.0in right 6.0in;" +
                                          "font-size:12.0pt;}");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section1 {page:Section1;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5in 1.0in 0.5in 1.0in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("<style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section1>");//chỉ định khổ giấy
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</div>");
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }
        protected void btnNBInTBC_Click_older(object sender, EventArgs e)
        {
            //SetGetSessionTK(true);
            //Session["GDTTT_MABM"] = "NOIBO_TBCHUYEN";
            //DataTable oDT = getDS(false, true, false, false, false);
            //if (oDT != null)
            //{
            //    if (oDT.Rows.Count == 0)
            //    {
            //        lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
            //        return;
            //    }
            //    //ĐỊa điểm
            //    string strDiadiem = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "", strTenPhongban = "";
            //    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
            //        strDiadiem = "Hà Nội";
            //    else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
            //    DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            //    if (dNgayCV != DateTime.MinValue)
            //    {
            //        strNgay = dNgayCV.Day.ToString();
            //        strThang = dNgayCV.Month.ToString();
            //        strNam = dNgayCV.Year.ToString();
            //    }
            //    decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            //    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
            //    if (oNSD.PHONGBANID != null)
            //    {
            //        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
            //        strTenPhongban = oPB.TENPHONGBAN;
            //    }
            //    strNguoiky = txtBC_Nguoiky.Text;
            //    string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
            //    DTGDTTT objds = new DTGDTTT();
            //    DataTable dtTP = oDT.AsEnumerable()
            //                   .GroupBy(r => new { NOICHUYEN = r["NOICHUYEN"] })
            //                   .Select(g => g.OrderBy(r => r["ID"]).First())
            //                   .CopyToDataTable();
            //    //anhvh 08/06/2020 lấy loại án -----
            //    DataTable dt_loaian = oDT.AsEnumerable()
            //                   .GroupBy(r => new { BAQD_LOAIAN = r["BAQD_LOAIAN_NAME"] })
            //                   .Select(g => g.OrderBy(r => r["BAQD_LOAIAN"]).First())
            //                   .CopyToDataTable();
            //    //--------
            //    String lOAI_AN = "", V_lOAI_AN_NAME = "";
            //    foreach (DataRow obj in dt_loaian.Rows)
            //    {
            //        lOAI_AN= lOAI_AN+", "+obj["BAQD_LOAIAN_NAME"].ToString();
            //    }
            //    V_lOAI_AN_NAME = lOAI_AN.TrimStart(',');
            //        decimal sotb = Cls_Comon.GetNumber(txtBC_SoCV.Text);
            //    foreach (DataRow obj in dtTP.Rows)
            //    {

            //        DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
            //        r.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";

            //        //manhnd 02/03/2020 nếu có số CV rồi thì lấy ra dùng
            //        if ((obj["CD_SOCV"] + "") != "")
            //        {

            //            r.NGUOIKY = obj["CD_NGUOIKY"].ToString();
            //            DateTime vNgayCV = Convert.ToDateTime(obj["CD_NGAYCV"]);
            //            r.NGAY = vNgayCV.Day.ToString();
            //            r.THANG = vNgayCV.Month.ToString();
            //            r.NAM = vNgayCV.Year.ToString();
            //            r.SOTOTRINH = obj["CD_SOCV"].ToString();
            //            sotb += 1;
            //        }
            //        else {
            //            //nếu chưa có số thì để rỗng
            //            r.NGUOIKY = "";
            //            r.NGAY = "";
            //            r.THANG = "";
            //            r.NAM = "";
            //            r.SOTOTRINH = "";
            //            sotb += 1;
            //        }
            //        //r.NGUOIKY = strNguoiky;
            //        //r.NGAY = strNgay;
            //        //r.THANG = strThang;
            //        //r.NAM = strNam;
            //        //if (txtBC_SoCV.Text != "")
            //        //{
            //        //    r.SOTOTRINH = reStr(sotb + "");
            //        //    sotb += 1;
            //        //}
            //        r.TUNGAY = txtNgaynhapTu.Text;
            //        r.DENNGAY = txtNgaynhapDen.Text;
            //        r.TENDONVI = strTendonvi;
            //        r.TENPHONGBANGUI = strTenPhongban;
            //        r.DIADIEM = strDiadiem;
            //        r.BAQD_LOAIAN_NAME = V_lOAI_AN_NAME;
            //        objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
            //        objds.AcceptChanges();
            //    }
            //    objds.AcceptChanges();
            //    Session["NOIBO_DATASET"] = objds;
            //}
            //string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        protected void btnNBInTotrinhDT_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);

            Session["GDTTT_MABM"] = "NOIBO_TOTRINH02";
            string strLoaiCV8_1 = "";
            if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
                {
                    strLoaiCV8_1 = " (do Đại biểu quốc hội, Các Cơ quan của Quốc hội, Đoàn đại biểu Quốc hội chuyển đến)";
                }
            }
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                DTGDTTT objds = new DTGDTTT();
                //ĐỊa điểm
                string strDiadiem = "", strDVGui = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strDVGui = oPB.TENPHONGBAN;
                }
                string strNgay = "", strThang = "", strNam = "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = dNgayCV.Day.ToString();
                    strThang = dNgayCV.Month.ToString();
                    strNam = dNgayCV.Year.ToString();
                }
                foreach (DataRow obj in oDT.Rows)
                {
                    if (txtBC_SoCV.Text != "")
                    {
                        decimal vid = Convert.ToDecimal(obj["ID"]);
                        GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
                        //if (d.CD_TRANGTHAI == 0 || d.CD_TRANGTHAI == 3)
                        //{
                        d.CD_SOTOTRINH = reStr(txtBC_SoCV.Text);
                        d.CD_NGAYTOTRINH = dNgayCV == DateTime.MinValue ? (DateTime?)null : dNgayCV;

                        //}
                        //Update Các đơn trùng kèm theo
                        try
                        {
                            string strArrDon = obj["arrDonID"] + "";
                            if (strArrDon != "")
                            {
                                string[] strarr = strArrDon.Split(',');
                                if (strarr.Length > 0)
                                {
                                    for (int k = 0; k < strarr.Length; k++)
                                    {
                                        decimal kID = Convert.ToDecimal(Cls_Comon.GetNumber(strarr[k]));
                                        if (kID > 0)
                                        {
                                            GDTTT_DON kDon = dt.GDTTT_DON.Where(x => x.ID == kID).FirstOrDefault();
                                            //if (kDon.CD_TRANGTHAI == 0 || kDon.CD_TRANGTHAI == 3)
                                            //{
                                            kDon.CD_SOTOTRINH = reStr(txtBC_SoCV.Text);
                                            kDon.CD_NGAYTOTRINH = dNgayCV == DateTime.MinValue ? (DateTime?)null : dNgayCV;
                                            //}
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex) { }
                        dt.SaveChanges();
                    }
                }

                DataTable dtTP = oDT.AsEnumerable()
                           .GroupBy(r => new { NOICHUYEN = r["NOICHUYEN"] })
                           .Select(g => g.OrderBy(r => r["ID"]).First())
                           .CopyToDataTable();
                foreach (DataRow rt in dtTP.Rows)
                {
                    DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                    r.SOTOTRINH = reStr(txtBC_SoCV.Text);
                    r.NGUOIKY = txtBC_Nguoiky.Text;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.TUNGAY = txtNgaynhapTu.Text;
                    r.DENNGAY = txtNgaynhapDen.Text;
                    if (r.TUNGAY == "") r.TUNGAY = "         ";
                    if (r.DENNGAY == "") r.DENNGAY = "         ";
                    r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    //Phòng ban
                    r.TENPHONGBANGUI = strDVGui;
                    r.TENPHONGBANNHAN = rt["NOICHUYEN"] + "";
                    r.DIADIEM = strDiadiem;
                    decimal vid = Convert.ToDecimal(rt["ID"]);
                    GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == vid).FirstOrDefault();
                    if (txtBC_SoCV.Text == "")
                    {
                        r.SOTOTRINH = reStr(d.CD_SOTOTRINH) + "";
                        r.NGUOIKY = d.CD_NGUOIKY + "";
                        if (d.CD_NGAYTOTRINH != null)
                        {
                            DateTime dNgayTT = (DateTime)d.CD_NGAYTOTRINH;
                            r.NGAY = Cls_Comon.toFullNumber(dNgayTT.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dNgayTT.Month.ToString(), true);
                            r.NAM = dNgayTT.Year.ToString();
                        }
                    }
                    r.LOAIDON8_1 = strLoaiCV8_1;
                    objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                    objds.AcceptChanges();
                }

                Session["NOIBO_DATASET"] = objds;

                string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }
        protected void btnNBPhieuchuyen_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NOIBO_PHIEUCHUYEN";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                //ĐỊa điểm
                string strDiadiem = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "", strTenPhongban = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                {
                    strDiadiem = "Hà Nội";
                }
                else
                {
                    strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                }
                ///----
                string TAND_NHAN_ = "";
                if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    TAND_NHAN_ = "TANDTC";
                }
                else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    TAND_NHAN_ = "TANDCC";
                }
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = dNgayCV.Day.ToString();
                    strThang = dNgayCV.Month.ToString();
                    strNam = dNgayCV.Year.ToString();
                }
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {

                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strTenPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                String _phong = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                {
                    _phong = "Vụ trưởng ";
                }
                else if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "6")
                {
                    _phong = "Trưởng phòng ";
                }
                strNguoiky = txtBC_Nguoiky.Text;
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DTGDTTT objds = new DTGDTTT();
                DataTable dtTP = oDT.AsEnumerable()
                               .GroupBy(r => new { TENTHAMPHAN = r["TENTHAMPHAN"], NOICHUYEN = r["NOICHUYEN"] })
                               .Select(g => g.OrderBy(r => r["ID"]).First())
                               .CopyToDataTable();
                decimal sotb = Cls_Comon.GetNumber(txtBC_SoCV.Text);

                // DataTable dtTP = objgr[0];
                foreach (DataRow obj in dtTP.Rows)
                {
                    //Kiểm tra đã tồn tại THẩm phán chưa
                    bool flag = false;
                    foreach (DTGDTTT.DT_NOIBO_PHIEUCHUYENRow p in objds.DT_NOIBO_PHIEUCHUYEN.Rows)
                    {
                        if (p.TENTHAMPHAN == (obj["TENTHAMPHAN"] + ""))
                        {
                            flag = true;
                            p.TENPHONGBANNHAN += "\n- Đồng chí " + _phong + obj["NOICHUYEN"] + ".";
                            objds.AcceptChanges();
                            break;
                        }
                    }
                    if (flag == false)
                    {
                        DTGDTTT.DT_NOIBO_PHIEUCHUYENRow r = objds.DT_NOIBO_PHIEUCHUYEN.NewDT_NOIBO_PHIEUCHUYENRow();
                        r.TENTHAMPHAN = obj["TENTHAMPHAN"] + "";
                        r.TENPHONGBANNHAN = "- Đồng chí " + _phong + obj["NOICHUYEN"] + ".";
                        r.SOTOTRINH = reStr(obj["CD_SOTOTRINH"] + "");
                        r.NGAYTOTRINH = GetDate(obj["CD_NGAYTOTRINH"]);
                        r.NGUOIKY = strNguoiky;
                        r.NGAY = strNgay;
                        r.THANG = strThang;
                        r.NAM = strNam;
                        if (txtBC_SoCV.Text != "")
                        {
                            r.SOTHONGBAO = sotb + "";
                            sotb += 1;
                        }
                        r.TENDONVI = strTendonvi;
                        r.TENPHONGBANGUI = strTenPhongban;
                        r.DIADIEM = strDiadiem;
                        r.TAND_NHAN = TAND_NHAN_;
                        objds.DT_NOIBO_PHIEUCHUYEN.AddDT_NOIBO_PHIEUCHUYENRow(r);
                        objds.AcceptChanges();
                    }
                }
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        protected void btnLuuCV_Click(object sender, EventArgs e)
        {
            if ((txtBC_Ngaydk.Text == "" || txtBC_SoCV.Text == "" || txtBC_Nguoiky.Text == ""))
            {
                //lbtthongbao.Text = "Chưa nhập đầy đủ thông tin số công văn, ngày chuyển, người ký!";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "myscript", "alert('Chưa nhập đầy đủ thông tin số công văn, ngày chuyển, người ký!')", true);
                return;
            }
            else
            {
                try
                {
                    decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                    string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
                    decimal PBID = 0;
                    if (strPBID != "") PBID = Convert.ToDecimal(strPBID);
                    //manhnd them kiem tra đã có số CV chưa
                    bool flag = false;
                    bool isCV_DON = false;
                    foreach (DataGridItem Item in dgList.Items)
                    {
                        CheckBox chkChon1 = (CheckBox)Item.FindControl("chkChon");
                        if (chkChon1.Checked)
                        {
                            flag = true;
                            string strID = Item.Cells[0].Text;
                            decimal ID = Convert.ToDecimal(strID);
                            GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                            if (oT.CD_SOCV != null)
                            {
                                isCV_DON = true;
                            }
                        }
                    }
                    if (flag == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "myscript", "alert('Chưa chọn đơn để thêm số Công văn!')", true);
                        //lbtthongbao.Text = "Chưa chọn đơn để thêm số Công văn!";
                        return;
                    }

                    if (isCV_DON == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "myscript", "alert('Đơn đã có Số Công Văn bạn kiểm tra lai!')", true);
                        //lbtthongbao.Text = "Một số đơn đã có Số Công Văn bạn kiểm tra lai!";
                        return;
                    }
                    else
                    {
                        //Kiem tra xem số công văn chuyển này đã có chưa 
                        string vCD_SOCV = txtBC_SoCV.Text;
                        DateTime vCD_NGAYCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        List<GDTTT_DON> DScv = dt.GDTTT_DON.Where(x => x.CD_SOCV == vCD_SOCV && x.CD_NGAYCV == vCD_NGAYCV && x.TOAANID == ToaAnID).ToList();
                        if (DScv.Count > 0)
                        {
                            string mess = "alert('Số Công Văn " + vCD_SOCV + " Ngày " + vCD_NGAYCV.ToString("dd/MM/yyyy") + " này đã tồn tại! Bạn kiểm tra lại')";
                            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "myscript", mess, true);
                            return;
                        }
                        else
                        {
                            foreach (DataGridItem Item in dgList.Items)
                            {
                                CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                                if (chkChon.Checked)
                                {

                                    string strID = Item.Cells[0].Text;
                                    decimal ID = Convert.ToDecimal(strID);
                                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();

                                    oT.CD_SOCV = txtBC_SoCV.Text;
                                    oT.CD_NGUOIKY = txtBC_Nguoiky.Text;
                                    oT.CD_NGAYCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                                    //Chuyển đồng thời các đơn trùng kèm theo
                                    string[] strarr = chkChon.ToolTip.Split(',');
                                    if (strarr.Length > 1)
                                    {
                                        for (int k = 0; k < strarr.Length; k++)
                                        {
                                            decimal kID = Convert.ToDecimal(strarr[k]);
                                            GDTTT_DON kDon = dt.GDTTT_DON.Where(x => x.ID == kID).FirstOrDefault();
                                            if (kDon.CD_SOCV == null)
                                            {
                                                kDon.CD_SOCV = oT.CD_SOCV;
                                                kDon.CD_NGUOIKY = oT.CD_NGUOIKY;
                                                kDon.CD_NGAYCV = oT.CD_NGAYCV;
                                            }

                                        }
                                    }
                                }
                            }
                            dt.SaveChanges();
                            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "myscript", "alert('Hoàn thành việc thêm số Công văn vào đơn!')", true);
                            Load_Data();
                            //Hiển thị nút chuyển đơn
                            btnChuyendon.Enabled = true;
                            btnChuyendon.CssClass = "buttoninput";

                        }

                    }
                }
                catch (Exception ex)
                {
                    lbtthongbao.Text = "Lỗi khi thêm số Công văn: " + ex.Message;
                }
            }
        }
        protected void btnSuaCV_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);

            string StrMsg = "PopupReport('/QLAN/GDTTT/Hoso/Popup/SuaCongvan.aspx','Sửa đổi công văn',1150,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        private string reStr(string str)
        {
            if (str.Length == 1)
                str = "0" + str;
            return str;
        }
        protected void ddlTrangthaidon_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowButtonPrint();
        }
        protected void ddlThuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowButtonPrint();
        }
        protected void btnNBInThongbao_Click(object sender, EventArgs e)
        {
            if (CurrDonViID != 1)
            {
                Literal Table_Str_Totals = new Literal();
                DataTable tbl = new DataTable();
                DataRow row = tbl.NewRow();
                //-------------
                tbl = getDS_BC(12, false, true, false, false, false);
                //-----------
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    row = tbl.Rows[0];
                    Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                    //Table_Str_Totals.Text = "<table></table>";
                }
                //--------------------------
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=THONG_BAO_YCBS.doc");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/msword";
                HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
                Response.Write("<html");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
                Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
                Response.Write("<meta name=ProgId content=Word.Document>");
                Response.Write("<meta name=Generator content=Microsoft Word 9>");
                Response.Write("<meta name=Originator content=Microsoft Word 9>");
                Response.Write("<style>");
                Response.Write("<!-- /* Style Definitions */" +
                                              "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                              "{margin:0in;" +
                                              "margin-bottom:.0001pt;" +
                                              "mso-pagination:widow-orphan;" +
                                              "tab-stops:center 3.0in right 6.0in;" +
                                              "font-size:12.0pt;}");
                Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section1 {page:Section1;}");
                Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5in 1.0in 0.5in 1.0in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section2 {page:Section2;}");
                Response.Write("<style>");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.Write("<div class=Section1>");//chỉ định khổ giấy
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Table_Str_Totals.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.Write("</div>");
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
            else
            {
                SetGetSessionTK(true);
                decimal IDPBNhan = Convert.ToDecimal(ddlPhongban.SelectedValue);
                Session["GDTTT_MABM"] = "NOIBO_THONGBAOLD";
                DataTable oDT = getDS(false, true, false, false, false);
                if (oDT != null)
                {
                    if (oDT.Rows.Count == 0)
                    {
                        lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                        return;
                    }
                    #region "Thông tin tờ trình"
                    DTGDTTT objds = new DTGDTTT();
                    string strTendonvi = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                    DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                    strSo = txtBC_SoCV.Text;
                    strNguoiky = txtBC_Nguoiky.Text;
                    DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (dNgayCV != DateTime.MinValue)
                    {
                        strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                        strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                        strNam = dNgayCV.Year.ToString();
                    }
                    strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                    string strHauto = "";
                    if (oNSD.PHONGBANID != null)
                    {
                        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                        strDiachi = oPB.DIACHI + "";
                        strHauto = oPB.HAUTOCV + "";
                    }
                    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                        strDiadiem = "Hà Nội";
                    else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                    decimal sotb = Cls_Comon.GetNumber(strSo);
                    #endregion
                    string TAND_NHAN_ = "";
                    if (Session["CAP_XET_XU"] + "" == "TOICAO")
                    {
                        TAND_NHAN_ = "TANDTC";
                    }
                    else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                    {
                        TAND_NHAN_ = "TANDCC";
                    }
                    //DANH SÁCH đơn
                    int i = 0;
                    foreach (DataRow obj in oDT.Rows)
                    {
                        decimal DonID = Convert.ToDecimal(obj["ID"]);
                        GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                        i += 1;
                        DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                        rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                        rds.DIACHI = obj["Diachigui"] + "";
                        rds.TENDONVI = strTendonvi;
                        bool isDacoSo = false;
                        if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                        {
                            if (strSo != "")
                            {
                                oT.TB1_SO = Cls_Comon.toFullNumber((sotb + i - 1), false).ToString();
                                if (dNgayCV != DateTime.MinValue)
                                    oT.TB1_NGAY = dNgayCV;
                                oT.CD_NGUOIKY = strNguoiky;
                                rds.SOTHONGBAO = Cls_Comon.toFullNumber((sotb + i - 1), false).ToString();
                                rds.NGUOIKY = strNguoiky;
                                rds.SOTOTRINH = strSo + strHauto;
                                rds.NGAY = strNgay;
                                rds.THANG = strThang;
                                rds.NAM = strNam;
                                isDacoSo = true;
                            }
                        }
                        if (isDacoSo == false)
                        {
                            rds.SOTHONGBAO = Cls_Comon.toFullNumber(oT.TB1_SO, false) + "";
                            rds.NGUOIKY = oT.CD_NGUOIKY + "";
                            rds.SOTOTRINH = Cls_Comon.toFullNumber(oT.CD_SOCV, false) + "";
                            if (oT.TB1_NGAY != null)
                            {
                                DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                                rds.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                                rds.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                                rds.NAM = dtNTB1.Year.ToString();
                            }
                        }
                        rds.DIADIEM = strDiadiem;
                        rds.LOAIAN = obj["BAQD_LOAIAN"] + "";
                        if (obj["NGAYGHITRENDON"] != null)
                            rds.NGAYGUIDON = GetDate(obj["NGAYGHITRENDON"]);
                        rds.GIOITINH = "";
                        rds.GIOITINHHOA = "";
                        if ((obj["DUNGDONLA"] + "") == "1")
                        {
                            if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                            {
                                rds.GIOITINH = "ông";
                                rds.GIOITINHHOA = "Ông";
                            }
                            else
                            {
                                rds.GIOITINH = "bà";
                                rds.GIOITINHHOA = "Bà";
                            }
                        }
                        else if ((obj["DUNGDONLA"] + "") == "2")
                        {
                            rds.GIOITINH = "các ông, bà";
                            rds.GIOITINHHOA = "Các ông, bà";
                        }
                        if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                        {
                            rds.BA_SO = obj["BAQD_SO"] + "";
                            rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                            if (rds.BA_NGAY != "")
                                rds.BA_NGAY = GetDate(rds.BA_NGAY);
                            rds.BA_TOAXX = obj["TOAXX"] + "";
                        }
                        else//QĐKN
                        {
                            rds.BA_SO = obj["BAQD_SO"] + "";
                            rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                            if (rds.BA_NGAY != "")
                                rds.BA_NGAY = GetDate(rds.BA_NGAY);
                            rds.BA_TOAXX = obj["TOAXX"] + "";
                        }
                        rds.NOIDUNG = "";
                        if (oT.NOIDUNGDON == "" || oT.NOIDUNGDON == null)//anhvh add 07/07/2020
                        {
                            if ((obj["CD_TA_LYDO_ISBAQD"] + "") == "1")
                            {
                                rds.NOIDUNG = "          - Cung cấp bản sao bản án/quyết định đã có hiệu lực pháp luật và các tài liệu, chứng cứ liên quan đến việc đề nghị giám đốc thẩm.";
                            }
                            if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")
                            {
                                if (rds.NOIDUNG != "")
                                    rds.NOIDUNG += "\n";
                                rds.NOIDUNG += "          - Xác nhận của Ủy ban nhân dân xã, phường, thị trấn nơi cư trú hoặc kèm theo bản photo giấy tờ tùy thân.";
                            }
                            if ((obj["CD_TA_LYDO_ISKHAC"] + "") == "1")
                            {
                                if (rds.NOIDUNG != "")
                                    rds.NOIDUNG += "\n";
                                rds.NOIDUNG += "          - " + obj["CD_TA_LYDO_KHAC"];
                            }
                        }
                        if (oT.NOIDUNGDON != "" && oT.NOIDUNGDON != null)
                        {
                            rds.NOIDUNG = "          - " + oT.NOIDUNGDON;
                            if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")
                            {
                                if (rds.NOIDUNG != "")
                                    rds.NOIDUNG += "\n";
                                rds.NOIDUNG += "          - Xác nhận của Ủy ban nhân dân xã, phường, thị trấn nơi cư trú hoặc kèm theo bản photo giấy tờ tùy thân.";
                            }
                            if ((obj["CD_TA_LYDO_ISKHAC"] + "") == "1")
                            {
                                if (rds.NOIDUNG != "")
                                    rds.NOIDUNG += "\n";
                                rds.NOIDUNG += "          - " + obj["CD_TA_LYDO_KHAC"];
                            }
                        }
                        rds.DIACHIPHONGBAN = strDiachi;
                        rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                        rds.TAND_NHAN = TAND_NHAN_;
                        objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                        objds.AcceptChanges();
                    }
                    dt.SaveChanges();
                    Session["NOIBO_DATASET"] = objds;
                }
                string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }

        protected void btnGXN_Click(object sender, EventArgs e)
        {
                Literal Table_Str_Totals = new Literal();
                DataTable tbl = new DataTable();
                DataRow row = tbl.NewRow();
                //-------------
                tbl = getDS_BC(11, false, true, false, false, false);
                //-----------
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    row = tbl.Rows[0];
                    Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                    //Table_Str_Totals.Text = "<table></table>";
                }
                //--------------------------
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=Giay_Xac_Nhan.doc");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/msword";
                HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
                Response.Write("<html");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
                Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
                Response.Write("<meta name=ProgId content=Word.Document>");
                Response.Write("<meta name=Generator content=Microsoft Word 9>");
                Response.Write("<meta name=Originator content=Microsoft Word 9>");
                Response.Write("<style>");
                Response.Write("<!-- /* Style Definitions */" +
                                              "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                              "{margin:0in;" +
                                              "margin-bottom:.0001pt;" +
                                              "mso-pagination:widow-orphan;" +
                                              "tab-stops:center 3.0in right 6.0in;" +
                                              "font-size:12.0pt;}");
                Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section1 {page:Section1;}");
                Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5in 1.0in 0.5in 1.0in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section2 {page:Section2;}");
                Response.Write("<style>");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.Write("<div class=Section1>");//chỉ định khổ giấy
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Table_Str_Totals.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.Write("</div>");
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
        }

        protected void btnNBTralai_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            decimal IDPBNhan = Convert.ToDecimal(ddlPhongban.SelectedValue);
            Session["GDTTT_MABM"] = "NOIBO_TRALAIDON";
            DataTable oDT = getDS(false, true, false, false, true);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có đơn quá hạn thông báo !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                string strHauto = "";
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strDiachi = oPB.DIACHI + "";
                    strHauto = oPB.HAUTOCV + "";
                }
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                decimal sotb = Cls_Comon.GetNumber(strSo);
                #endregion
                string TAND_NHAN_ = "";
                if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    TAND_NHAN_ = "TANDTC";
                }
                else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    TAND_NHAN_ = "TANDCC";
                }
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    i += 1;
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;
                    if (strSo != "")
                        rds.SOTHONGBAO = reStr((sotb + i - 1).ToString());
                    rds.SOTOTRINH = strSo + strHauto;
                    rds.NGAY = strNgay;
                    rds.THANG = strThang;
                    rds.NAM = strNam;
                    bool isDacoSo = false;

                    if (strSo != "")
                    {
                        oT.TBTLD_SO = reStr((sotb + i - 1).ToString());
                        if (dNgayCV != DateTime.MinValue)
                            oT.TBTLD_NGAY = dNgayCV;
                        oT.CD_NGUOIKY = strNguoiky;
                        rds.SOTHONGBAO = reStr((sotb + i - 1).ToString());
                        rds.NGUOIKY = strNguoiky;
                        rds.SOTOTRINH = strSo + strHauto;
                        rds.NGAY = strNgay;
                        rds.THANG = strThang;
                        rds.NAM = strNam;
                        isDacoSo = true;
                    }

                    if (isDacoSo == false)
                    {
                        rds.SOTHONGBAO = reStr(oT.TBTLD_SO + "");
                        rds.NGUOIKY = oT.CD_NGUOIKY + "";
                        rds.SOTOTRINH = reStr(oT.CD_SOCV + "");
                        if (oT.TBTLD_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TBTLD_NGAY;
                            rds.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            rds.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            rds.NAM = dtNTB1.Year.ToString();
                        }
                    }

                    rds.DIADIEM = strDiadiem;
                    rds.LOAIAN = obj["BAQD_LOAIAN"] + "";
                    if (obj["NGAYGHITRENDON"] != null)
                        rds.NGAYGUIDON = GetDate(obj["NGAYGHITRENDON"]);
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    rds.NOIDUNG = "";
                    if ((obj["CD_TA_LYDO_ISBAQD"] + "") == "1")
                    {
                        rds.NOIDUNG = "          - Cung cấp bản sao bản án/quyết định đã có hiệu lực pháp luật và các tài liệu, chứng cứ liên quan đến việc đề nghị giám đốc thẩm.";
                    }
                    if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")
                    {
                        if (rds.NOIDUNG != "")
                            rds.NOIDUNG += "\n";
                        rds.NOIDUNG += "          - Xác nhận của Ủy ban nhân dân xã, phường, thị trấn nơi cư trú hoặc kèm theo bản photo giấy tờ tùy thân.";
                    }
                    if ((obj["CD_TA_LYDO_ISKHAC"] + "") == "1")
                    {
                        if (rds.NOIDUNG != "")
                            rds.NOIDUNG += "\n";
                        rds.NOIDUNG += "          - " + obj["CD_TA_LYDO_KHAC"];
                    }
                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    rds.TB1_SO = obj["TB1_SO"] + "/TB-TA";
                    rds.TB1_NGAY = obj["TB1_NGAY"] + "";
                    if (rds.TB1_NGAY != "")
                        rds.TB1_NGAY = GetDate(rds.TB1_NGAY);
                    rds.TAND_NHAN = TAND_NHAN_;
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
                dt.SaveChanges();
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNBInThongbaoTG_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            decimal IDPBNhan = Convert.ToDecimal(ddlPhongban.SelectedValue);
            DM_PHONGBAN objPBN = dt.DM_PHONGBAN.Where(x => x.ID == IDPBNhan).FirstOrDefault();
            Session["GDTTT_MABM"] = "NOIBO_THONGBAOTG";
            DataTable oDT = getDS(false, true, true, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";

                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strDiachi = oPB.DIACHI + "";

                }

                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                decimal sotb = Cls_Comon.GetNumber(strSo);
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;
                    bool isDacoSo = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSo != "")
                        {
                            oT.TB1_SO = reStr((sotb + i - 1).ToString());
                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            oT.CD_NGUOIKY = strNguoiky;
                            rds.SOTHONGBAO = reStr((sotb + i - 1).ToString());
                            rds.NGUOIKY = strNguoiky;
                            rds.NGAY = strNgay;
                            rds.THANG = strThang;
                            rds.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        rds.SOTHONGBAO = reStr(oT.TB1_SO + "");
                        rds.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            rds.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            rds.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            rds.NAM = dtNTB1.Year.ToString();
                        }
                    }
                    rds.DIADIEM = strDiadiem;
                    if (obj["NGAYGHITRENDON"] != null)
                        rds.NGAYGUIDON = GetDate(obj["NGAYGHITRENDON"]);
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    rds.NOIDUNG = "";
                    if (oT.NOIDUNGDON == "" || oT.NOIDUNGDON == null)//anhvh add 07/07/2020
                    {
                        if ((obj["CD_TA_LYDO_ISBAQD"] + "") == "1")
                        {
                            rds.NOIDUNG = "          - Cung cấp bản sao bản án/quyết định đã có hiệu lực pháp luật và các tài liệu liên quan đến việc đề nghị giám đốc thẩm.";
                        }
                        if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")
                        {
                            if (rds.NOIDUNG != "")
                                rds.NOIDUNG += "\n";
                            rds.NOIDUNG += "          - Xác nhận của Ủy ban nhân dân xã, phường, thị trấn nơi cư trú hoặc kèm theo bản photo giấy tờ tùy thân.";
                        }
                        if ((obj["CD_TA_LYDO_ISKHAC"] + "") == "1")
                        {
                            if (rds.NOIDUNG != "")
                                rds.NOIDUNG += "\n";
                            rds.NOIDUNG += "          - " + obj["CD_TA_LYDO_KHAC"];
                        }
                    }
                    if (oT.NOIDUNGDON != "" && oT.NOIDUNGDON != null)
                    {
                        rds.NOIDUNG = "          - " + oT.NOIDUNGDON;
                        if ((obj["CD_TA_LYDO_ISXACNHAN"] + "") == "1")
                        {
                            if (rds.NOIDUNG != "")
                                rds.NOIDUNG += "\n";
                            rds.NOIDUNG += "          - Xác nhận của Ủy ban nhân dân xã, phường, thị trấn nơi cư trú hoặc kèm theo bản photo giấy tờ tùy thân.";
                        }
                        if ((obj["CD_TA_LYDO_ISKHAC"] + "") == "1")
                        {
                            if (rds.NOIDUNG != "")
                                rds.NOIDUNG += "\n";
                            rds.NOIDUNG += "          - " + obj["CD_TA_LYDO_KHAC"];
                        }
                    }
                    rds.TENCOQUAN = obj["CV_TENDONVI"] + "";
                    rds.TENTRAIGIAMCU = oT.CV_TENDONVI + "";
                    if ((oT.CV_TRAIGIAMHIENTAI + "") != "")
                    {
                        rds.TENCOQUAN = oT.CV_TRAIGIAMHIENTAI + "";
                    }

                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                    dt.SaveChanges();
                }

                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNBInCoquanchuyendon_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            bool isLOAICV_81 = false;
            Session["GDTTT_MABM"] = "NOIBO_THONGBAOCOQUAN";
            if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
                {
                    isLOAICV_81 = true;
                    Session["GDTTT_MABM"] = "NOIBO_THONGBAOCOQUAN81";
                }
            }
            decimal IDPBNhan = Convert.ToDecimal(ddlPhongban.SelectedValue);
            DM_PHONGBAN objPBN = dt.DM_PHONGBAN.Where(x => x.ID == IDPBNhan).FirstOrDefault();

            DataTable oDT = getDS(true, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có đơn kèm công văn";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strPBGui = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strDiachi = oPB.DIACHI + "";
                    strPBGui = oPB.TENPHONGBAN;
                }
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));

                #endregion
                string TAND_NHAN_ = "";
                if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    TAND_NHAN_ = "TANDTC";
                }
                else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    TAND_NHAN_ = "TANDCC";
                }
                //DANH SÁCH đơn
                decimal sotb = Cls_Comon.GetNumber(strSo);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    i += 1;
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;

                    rds.SOTHONGBAO = reStr((sotb + i - 1).ToString());
                    rds.SOTOTRINH = oT.CD_SOCV + "";
                    rds.NGUOIKY = strNguoiky;
                    rds.NGAY = strNgay;
                    rds.THANG = strThang;
                    rds.NAM = strNam;
                    if (oT.CD_NGAYCV != null)
                    {
                        DateTime dtNGAYTT = (DateTime)oT.CD_NGAYCV;
                        rds.NGAYTOTRINH = dtNGAYTT.ToString("dd/MM/yyyy");
                    }
                    rds.DIADIEM = strDiadiem;
                    rds.TENPHONGBANGUI = strPBGui;
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    rds.BA_TOAXX = obj["TOAXX"] + "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);

                    }
                    rds.SOCV = obj["CV_SO"] + "";
                    if (obj["CV_NGAY"] != null)
                        rds.NGAYCV = GetDate(obj["CV_NGAY"]);
                    rds.TENCOQUAN = obj["CV_TENDONVI"] + "";
                    rds.DIACHICOQUAN = obj["CVDIACHI"] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    if (isLOAICV_81)
                    {
                        rds.CD_NGAY = GetDate(obj["CD_NGAYCV"]);
                        if (oT.LOAICONGVAN != null)
                        {
                            DM_DATAITEM oLoaiCV = dt.DM_DATAITEM.Where(x => x.ID == oT.LOAICONGVAN).FirstOrDefault();
                            rds.NOIDUNG = "";
                            if (oLoaiCV.MA == ENUM_GDT_LOAICV.CV8_1_DBQH)
                            {
                                rds.DIACHICOQUAN = ", " + oT.CV_TRAIGIAMHIENTAI + "";
                                rds.NOIDUNG = strTendonvi + " nhận được Phiếu chuyển đơn của " + rds.TENCOQUAN + ", " + oT.CV_TRAIGIAMHIENTAI;
                                rds.NOIDUNG += ", chuyển đơn của " + rds.GIOITINH + " " + rds.NGUOIGUI + " (có địa chỉ " + rds.DIACHI + ") ";
                            }
                            else
                            {
                                rds.NOIDUNG = strTendonvi + " nhận được Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + " của " + rds.TENCOQUAN;
                                rds.NOIDUNG += " về việc chuyển đơn của " + rds.GIOITINH + " " + rds.NGUOIGUI + " (có địa chỉ " + rds.DIACHI + ") ";
                                rds.NOIDUNG += "đề ngày " + GetDate(oT.NGAYGHITRENDON) + " ";
                            }
                            rds.NOIDUNG += "có nội dung";
                            if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                            {
                                rds.NOIDUNG += " đề nghị xem xét theo thủ tục giám đốc thẩm đối với Bản án số ";
                                rds.NOIDUNG += rds.BA_SO + " ngày " + rds.BA_NGAY + " của " + rds.BA_TOAXX + ".";
                            }
                            else//Kháng nghị
                            {

                                rds.NOIDUNG += " kiến nghị đối với Quyết định kháng nghị giám đốc thẩm số " + oT.KN_SOQD + " ngày " + GetDate(oT.KN_NGAY);
                                rds.NOIDUNG += " của " + rds.BA_TOAXX + " kháng nghị đối với Bản án số " + oT.BAQD_SO + " ngày " + GetDate(oT.BAQD_NGAYBA);
                                rds.NOIDUNG += " của ";
                                try
                                {
                                    if (oT.BAQD_TOAANID != null && oT.BAQD_TOAANID != 0)
                                    {
                                        DM_TOAAN oTAIN = dt.DM_TOAAN.Where(x => x.ID == oT.BAQD_TOAANID).FirstOrDefault();
                                        rds.NOIDUNG += oTAIN.MA_TEN + ".";
                                    }
                                }
                                catch (Exception ex) { }

                            }

                        }
                        rds.LYDO = "thuộc " + strTendonvi + " ";
                    }
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    rds.TAND_NHAN = TAND_NHAN_;
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                }
                dt.SaveChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        //In phiếu chuyển tòa khác hoặc ngoài tòa
        protected void btnTKPhieuchuyen_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            bool isLOAICV_81 = false;
            Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYEN";
            if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
                {
                    isLOAICV_81 = true;
                    Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYEN81";
                }
            }
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    //Nội dung đơn


                    if (oT.BAQD_LOAIQDBA == 0)//BAQD
                    {
                        if ((oT.BAQD_SO + "") != "")
                        {
                            r.NOIDUNG = " có nội dung đề nghị xem xét theo thủ tục giám đốc thẩm/tái thẩm đối với Bản án/Quyết định số " + oT.BAQD_SO + " ngày ";
                            if (oT.BAQD_NGAYBA != null) r.NOIDUNG += Convert.ToDateTime(oT.BAQD_NGAYBA).ToString("dd/MM/yyyy") + " của ";
                            else r.NOIDUNG += "   của ";
                            if (oT.BAQD_TOAANID != null && oT.BAQD_TOAANID != 0)
                            {
                                r.NOIDUNG += obj["TOAXX"] + " đã có hiệu lực pháp luật;";
                            }
                            else
                                r.NOIDUNG += oT.BAQD_TENTOA + " đã có hiệu lực pháp luật;";
                        }
                        else
                        {
                            if ((oT.NOIDUNGDON + "").Trim() != "")
                                r.NOIDUNG = " có nội dung " + oT.NOIDUNGDON + ".";
                            else if ((oT.NOIDUNGDON + "").Trim() == "" && (oT.GHICHU + "").Trim() != "")
                                r.NOIDUNG = " có nội dung " + oT.GHICHU + ".";
                        }
                    }
                    else//Số QĐ KN
                    {
                        if ((oT.KN_SOQD + "") != "")
                        {
                            r.NOIDUNG = " có nội dung khiếu nại quyết định số " + oT.KN_SOQD + " ngày ";
                            if (oT.KN_NGAY != null) r.NOIDUNG += Convert.ToDateTime(oT.KN_NGAY).ToString("dd/MM/yyyy") + " của ";
                            else r.NOIDUNG += "   của ";
                            r.NOIDUNG += obj["TOAXX"];
                            r.NOIDUNG += " đối với bản án số " + oT.BAQD_SO + " ngày ";
                            if (oT.BAQD_NGAYBA != null) r.NOIDUNG += Convert.ToDateTime(oT.BAQD_NGAYBA).ToString("dd/MM/yyyy") + " của ";
                            if (oT.BAQD_TOAANID != null && oT.BAQD_TOAANID != 0)
                            {
                                try
                                {
                                    DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == oT.BAQD_TOAANID).FirstOrDefault();
                                    r.NOIDUNG += oTA.MA_TEN + ".";
                                }
                                catch (Exception ex) { }
                            }


                        }
                        else
                        {
                            if ((oT.NOIDUNGDON + "").Trim() != "")
                                r.NOIDUNG = " có nội dung " + oT.NOIDUNGDON + ".";
                            else if ((oT.NOIDUNGDON + "").Trim() == "" && (oT.GHICHU + "").Trim() != "")
                                r.NOIDUNG = " có nội dung " + oT.GHICHU + ".";
                        }
                    }
                    string strISNOTGDTTT = obj["ISNOTGDTTT"] + "";
                    if (strISNOTGDTTT == "1")
                    {
                        r.NOIDUNG = " có nội dung " + oT.NOIDUNGDON + "";
                    }
                    //------------------
                    r.CHIDAO_COKHONG = oT.CHIDAO_COKHONG + "";
                    r.NOIDUNGCHIDAO = oT.CHIDAO_NOIDUNG + "";
                    r.CHUCVULANHDAO = "Chánh án";
                    if (oT.CHIDAO_COKHONG > 0 && oT.CHIDAO_LANHDAOID > 0)
                    {
                        try
                        {
                            DM_CANBO oLD = dt.DM_CANBO.Where(x => x.ID == oT.CHIDAO_LANHDAOID).FirstOrDefault();
                            if (oLD.CHUCVUID == null || oLD.CHUCVUID == 0)
                            {
                                r.CHUCVULANHDAO = "Thẩm phán";
                                r.LANHDAO = " " + oLD.HOTEN;
                            }
                            else
                            {
                                DM_DATAITEM oCV = dt.DM_DATAITEM.Where(x => x.ID == oLD.CHUCVUID).FirstOrDefault();
                                if (oCV.MA == ENUM_CHUCVU.CHUCVU_PCA)
                                {
                                    r.CHUCVULANHDAO = "Phó Chánh án";
                                    r.LANHDAO = " " + oLD.HOTEN;
                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.GIOITINH = "";
                    r.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            r.GIOITINH = "ông";
                            r.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            r.GIOITINH = "bà";
                            r.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                    {
                        r.GIOITINH = "các ông, bà";
                        r.GIOITINHHOA = "Các ông, bà";
                    }
                    if (obj["NGAYGHITRENDON"] != null)
                        r.NGAYGUI = GetDate(obj["NGAYGHITRENDON"]);
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    if (oT.LOAIDON == 1)
                    {
                        string strTMP1 = "đơn của " + r.GIOITINH + " " + r.NGUOIGUI + " (địa chỉ " + r.DIACHIGUI + ")";
                        strTMP1 += " đề ngày " + r.NGAYGUI + "";
                        r.TMP3 = strTMP1 + ",";
                    }
                    else if (oT.LOAIDON == 2)
                    {
                        string strTMP1 = "Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + " của " + oT.CV_TENDONVI;
                        r.TMP3 = strTMP1 + ",";
                    }
                    else if (oT.LOAIDON == 3)
                    {
                        string strTMP1 = "đơn của " + r.GIOITINH + " " + r.NGUOIGUI + " (địa chỉ " + r.DIACHIGUI + ")";
                        strTMP1 += " đề ngày " + r.NGAYGUI + "";

                        DM_DATAITEM oLoaiCV = dt.DM_DATAITEM.Where(x => x.ID == oT.LOAICONGVAN).FirstOrDefault();
                        if (oLoaiCV.MA == ENUM_GDT_LOAICV.CV8_1_DBQH)
                            r.GHICHU = " (do " + oT.CV_TENDONVI + ", " + oT.CV_TRAIGIAMHIENTAI + " chuyển đến theo Phiếu chuyển ngày " + GetDate(oT.CV_NGAY) + ")";
                        else
                        {
                            if ((oT.CV_SO + "") == "")
                                r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến)";
                            else
                                r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến theo Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + ")";
                        }
                        r.CV_TENDONVI = oT.CV_TENDONVI;
                        strTMP1 += r.GHICHU;
                        r.TMP3 = strTMP1 + ",";
                    }
                    bool isDacoSo = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {
                            oT.TB1_SO = reStr((sotb + i - 1).ToString());
                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            ////manhnd bỏ 02/03/2020 bỏ đi không cho phép chèn số CV tại đây
                            //oT.CD_NGUOIKY = strNguoiKy;
                            //oT.CD_SOCV = oT.TB1_SO;
                            //oT.CD_NGAYCV = dNgayCV;
                            r.SO = reStr((sotb + i - 1).ToString());
                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = oT.TB1_SO + "";
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }


                    r.TMP1 = r.TENDONVINHAN;
                    if (isLOAICV_81)
                    {
                        if (oT.CD_TK_NOIGUI == 1)
                        {
                            r.TMP1 = "Đồng chí Chánh án " + r.TENDONVINHAN;
                            if (r.TMP1.Contains("cấp cao tại thành phố Hồ"))
                                r.TMP1 = r.TMP1.Replace("cấp cao tại", "cấp cao\n tại");
                            r.TMP2 = "đồng chí Chánh án ";

                        }
                        else
                        {
                            r.TMP1 = r.TENDONVINHAN;
                            r.TMP2 = "";

                        }

                    }
                    else
                        if (oT.CD_TK_NOIGUI == 1)
                    {
                        r.TMP1 = "Đồng chí Chánh án " + r.TENDONVINHAN;
                        if (r.TMP1.Contains("cấp cao tại thành phố Hồ"))
                            r.TMP1 = r.TMP1.Replace("cấp cao tại", "cấp cao\n tại");
                    }
                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnTKPhieuchuyenN_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            bool isLOAICV_81 = false;
            Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYENN";
            if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
                {
                    isLOAICV_81 = true;
                    Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYENN81";
                }
            }

            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                string strDVNhan = "";
                bool isNewDVNhan = false;
                //Order by lại Nơi chuyển đến
                DataView dv = new DataView(oDT);
                dv.Sort = "NOICHUYEN ASC";
                oDT = dv.ToTable();
                if (sotb > 0) sotb = sotb - 1;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    bool isDacoSo = false;

                    if (r.TENDONVINHAN != strDVNhan)
                    {
                        strDVNhan = r.TENDONVINHAN;
                        isNewDVNhan = true;
                    }
                    else
                        isNewDVNhan = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {

                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            ////manhnd bỏ 02/03/2020 bo khong cho chèn số CV
                            //oT.CD_NGUOIKY = strNguoiKy;

                            //oT.CD_NGAYCV = dNgayCV;
                            if (isNewDVNhan)
                            {
                                sotb = sotb + 1;
                                oT.TB1_SO = reStr(sotb.ToString());
                                //oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }
                            else
                            {
                                oT.TB1_SO = reStr(sotb.ToString());
                                //oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }

                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = reStr(oT.TB1_SO + "");
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }
                    dt.SaveChanges();

                    if (isNewDVNhan)
                    {
                        r.TMP1 = r.TENDONVINHAN;
                        if (isLOAICV_81)
                        {
                            if (oT.CD_TK_NOIGUI == 1)
                            {
                                r.TMP1 = "Đồng chí Chánh án " + r.TENDONVINHAN;
                                if (r.TMP1.Contains("cấp cao tại thành phố Hồ"))
                                    r.TMP1 = r.TMP1.Replace("cấp cao tại", "cấp cao\n tại");
                                r.TMP2 = "đồng chí Chánh án ";
                                r.TMP3 = "để chỉ đạo";
                            }
                            else
                            {
                                r.TMP1 = r.TENDONVINHAN;
                                r.TMP2 = "";
                                r.TMP3 = "để xem xét và";
                            }

                        }
                        objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                        objds.AcceptChanges();
                    }
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnTKDuongsu_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "TOAKHAC_PHIEUCHUYENGUIDS";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NOIBO_DANHSACHRow rds = objds.DT_NOIBO_DANHSACH.NewDT_NOIBO_DANHSACHRow();
                    rds.SOTOTRINH = txtBC_SoCV.Text;
                    rds.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    if (dNgayCV != DateTime.MinValue)
                    {
                        rds.NGAY = dNgayCV.Day.ToString();
                        rds.THANG = dNgayCV.Month.ToString();
                        rds.NAM = dNgayCV.Year.ToString();
                    }

                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    if ((obj["arrCongvan"] + "") != "")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Do " + obj["arrCongvan"] + ")";
                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        rds.NGUOIGUI = rds.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    rds.DIAPHUONG = obj["Diachigui"] + "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    else//QĐKN
                    {
                        rds.QD_SO = obj["BAQD_SO"] + "";
                        rds.QD_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.QD_NGAY != "")
                            rds.QD_NGAY = GetDate(rds.QD_NGAY);
                        rds.QD_NGUOIKN = obj["TOAXX"] + "";
                    }
                    rds.SOTOTRINH = obj["CD_SOCV"] + "";
                    rds.NGAYTOTRINH = GetDate(obj["CD_NGAYCV"]);
                    rds.SODON = obj["SODON"] + "";
                    rds.GHICHU = obj["GHICHU"] + "";
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    objds.DT_NOIBO_DANHSACH.AddDT_NOIBO_DANHSACHRow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        ////in phiếu Gửi cơ quan chuyển đơn
        protected void btnTKCoquan_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = getDS_BC(10, false, true, false, false, false);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //--------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=gui_cq_chuyendon.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/msword";
            HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
            Response.Write("<html");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
            Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content=Microsoft Word 9>");
            Response.Write("<meta name=Originator content=Microsoft Word 9>");
            Response.Write("<style>");
            Response.Write("<!-- /* Style Definitions */" +
                                          "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                          "{margin:0in;" +
                                          "margin-bottom:.0001pt;" +
                                          "mso-pagination:widow-orphan;" +
                                          "tab-stops:center 3.0in right 6.0in;" +
                                          "font-size:12.0pt;}");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.78740196228in 0.78740196228in 0.78740196228in 1.181in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section1 {page:Section1;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.5in 1.0in 0.5in 1.0in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("<style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section1>");//chỉ định khổ giấy
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</div>");
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }
        protected void btnTKCoquan_Click_Older(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            bool isLOAICV_81 = false;
            Session["GDTTT_MABM"] = "TOAKHAC_THONGBAOCOQUAN";
            if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
                {
                    isLOAICV_81 = true;
                    Session["GDTTT_MABM"] = "NOIBO_THONGBAOCOQUAN81";
                }
            }

            DataTable oDT = getDS(true, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có đơn kèm công văn";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strPBGui = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();

                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));

                #endregion
                //DANH SÁCH đơn
                decimal sotb = Cls_Comon.GetNumber(strSo);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;
                    rds.SOTHONGBAO = reStr((sotb + i - 1).ToString());
                    rds.NGAY = strNgay;
                    rds.THANG = strThang;
                    rds.NAM = strNam;
                    rds.DIADIEM = strDiadiem;
                    rds.TENPHONGBANGUI = strPBGui;
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    rds.BA_TOAXX = "";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    if ((oT.BAQD_SO + "") != "")
                    {
                        rds.NOIDUNG = "đề nghị xem xét theo thủ tục giám đốc thẩm/tái thẩm đối với Bản án/Quyết định số " + oT.BAQD_SO + " ngày ";
                        if (oT.BAQD_NGAYBA != null)
                            rds.NOIDUNG += Convert.ToDateTime(oT.BAQD_NGAYBA).ToString("dd/MM/yyyy") + " của ";
                        else rds.NOIDUNG += "   của ";
                        if (oT.BAQD_TOAANID != null && oT.BAQD_TOAANID != 0)
                        {
                            rds.NOIDUNG += obj["TOAXX"] + " đã có hiệu lực pháp luật;";
                        }
                        else
                            rds.NOIDUNG += oT.BAQD_TENTOA + " đã có hiệu lực pháp luật;";
                    }
                    else
                    {
                        if ((oT.NOIDUNGDON + "").Trim() != "")
                            rds.NOIDUNG = "" + oT.NOIDUNGDON + ".";
                        else if ((oT.NOIDUNGDON + "").Trim() == "" && (oT.GHICHU + "").Trim() != "")
                            rds.NOIDUNG = "" + oT.GHICHU + ".";
                    }
                    rds.SOTOTRINH = obj["CD_SOCV"] + "";
                    if (obj["CD_NGAYCV"] != null)
                        rds.NGAYTOTRINH = GetDate(obj["CD_NGAYCV"]);

                    rds.SOCV = obj["CV_SO"] + "";
                    if (obj["CV_NGAY"] != null)
                        rds.NGAYCV = GetDate(obj["CV_NGAY"]);
                    rds.TENCOQUAN = obj["CV_TENDONVI"] + ",";
                    rds.DIACHICOQUAN = obj["CVDIACHI"] + "";
                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    if (isLOAICV_81)
                    {
                        rds.CD_NGAY = GetDate(obj["CD_NGAYCV"]);
                        if (oT.LOAICONGVAN != null)
                        {
                            DM_DATAITEM oLoaiCV = dt.DM_DATAITEM.Where(x => x.ID == oT.LOAICONGVAN).FirstOrDefault();
                            rds.NOIDUNG = "";
                            if (oLoaiCV.MA == ENUM_GDT_LOAICV.CV8_1_DBQH)
                            {
                                rds.DIACHICOQUAN = ", " + oT.CV_TRAIGIAMHIENTAI + "";
                                rds.NOIDUNG = strTendonvi + " nhận được Phiếu chuyển đơn của " + rds.TENCOQUAN + ", " + oT.CV_TRAIGIAMHIENTAI;
                                rds.NOIDUNG += ", chuyển đơn của " + rds.GIOITINH + " " + rds.NGUOIGUI + " (có địa chỉ " + rds.DIACHI + ") ";
                            }
                            else
                            {
                                rds.NOIDUNG = strTendonvi + " nhận được Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + " của " + rds.TENCOQUAN;
                                rds.NOIDUNG += " về việc chuyển đơn của " + rds.GIOITINH + " " + rds.NGUOIGUI + " (có địa chỉ " + rds.DIACHI + ") ";
                                rds.NOIDUNG += "đề ngày " + GetDate(oT.NGAYGHITRENDON) + " ";
                            }
                            rds.NOIDUNG += "có nội dung";
                            if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                            {
                                if ((oT.BAQD_SO + "") != "")
                                {
                                    rds.NOIDUNG += " đề nghị xem xét theo thủ tục giám đốc thẩm/tái thẩm đối với Bản án số ";
                                    rds.NOIDUNG += rds.BA_SO + " ngày " + rds.BA_NGAY + " của " + rds.BA_TOAXX + ".";
                                }
                                else
                                {
                                    if ((oT.NOIDUNGDON + "").Trim() != "")
                                        rds.NOIDUNG += " " + oT.NOIDUNGDON + ".";
                                    else if ((oT.NOIDUNGDON + "").Trim() == "" && (oT.GHICHU + "").Trim() != "")
                                        rds.NOIDUNG += " " + oT.GHICHU + ".";
                                }

                            }
                            else//Kháng nghị
                            {

                                rds.NOIDUNG += " kiến nghị đối với Quyết định kháng nghị giám đốc thẩm số " + oT.KN_SOQD + " ngày " + GetDate(oT.KN_NGAY);
                                rds.NOIDUNG += " của " + rds.BA_TOAXX + " kháng nghị đối với Bản án số " + oT.BAQD_SO + " ngày " + GetDate(oT.BAQD_NGAYBA);
                                rds.NOIDUNG += " của ";
                                try
                                {
                                    if (oT.BAQD_TOAANID != null && oT.BAQD_TOAANID != 0)
                                    {
                                        DM_TOAAN oTAIN = dt.DM_TOAAN.Where(x => x.ID == oT.BAQD_TOAANID).FirstOrDefault();
                                        rds.NOIDUNG += oTAIN.MA_TEN + ".";
                                    }
                                }
                                catch (Exception ex) { }

                            }

                        }
                    }
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNTAPhieuchuyenN_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);

            Session["GDTTT_MABM"] = "NTA_PHIEUCHUYENN";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                string strDVNhan = "";
                bool isNewDVNhan = false;
                DataView dv = new DataView(oDT);
                dv.Sort = "NOICHUYEN ASC";
                oDT = dv.ToTable();
                if (sotb > 0) sotb = sotb - 1;
                string TAND_NHAN_ = "";
                if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    TAND_NHAN_ = "TANDTC";
                }
                else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    TAND_NHAN_ = "TANDCC";
                }
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    r.SO = reStr(strSOCV);
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    r.TAND_NHAN = TAND_NHAN_;
                    bool isDacoSo = false;

                    if (r.TENDONVINHAN != strDVNhan)
                    {
                        strDVNhan = r.TENDONVINHAN;
                        isNewDVNhan = true;
                    }
                    else
                        isNewDVNhan = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {

                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            //manhnd bỏ
                            //oT.CD_NGUOIKY = strNguoiKy;

                            //oT.CD_NGAYCV = dNgayCV;
                            if (isNewDVNhan)
                            {
                                sotb = sotb + 1;
                                oT.TB1_SO = reStr(sotb.ToString());
                                //oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }
                            else
                            {
                                oT.TB1_SO = reStr(sotb.ToString());
                                //oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }

                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = reStr(oT.TB1_SO + "");
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }

                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNTAPhieuchuyen_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NTA_PHIEUCHUYEN";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.SO = reStr(strSOCV);
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    r.NOIDUNG = oT.NOIDUNGDON + "";
                    r.CHIDAO_COKHONG = oT.CHIDAO_COKHONG + "";
                    r.NOIDUNGCHIDAO = oT.CHIDAO_NOIDUNG + "";
                    r.CHUCVULANHDAO = "Chánh án";
                    if (oT.CHIDAO_COKHONG > 0 && oT.CHIDAO_LANHDAOID > 0)
                    {
                        try
                        {
                            DM_CANBO oLD = dt.DM_CANBO.Where(x => x.ID == oT.CHIDAO_LANHDAOID).FirstOrDefault();
                            DM_DATAITEM oCV = dt.DM_DATAITEM.Where(x => x.ID == oLD.CHUCVUID).FirstOrDefault();
                            if (oCV.MA == ENUM_CHUCVU.CHUCVU_PCA)
                            {
                                r.CHUCVULANHDAO = "Phó Chánh án";
                                r.LANHDAO = " " + oLD.HOTEN;
                            }
                        }
                        catch (Exception ex) { }
                    }
                    if (oT.LOAIDON == 3)
                    {
                        if ((oT.CV_SO + "") == "")
                            r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến)";
                        else
                            r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến theo Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + ")";

                    }
                    bool isDacoSo = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {
                            oT.TB1_SO = reStr((sotb + i - 1).ToString());
                            if (dNgayCV != DateTime.MinValue)
                            {
                                oT.TB1_NGAY = dNgayCV;
                                //oT.CD_NGAYCV = dNgayCV;
                            }
                            ////manhnd bỏ 02/03/2020 bỏ chèn số CV tự động
                            //oT.CD_NGUOIKY = strNguoiKy;
                            //oT.CD_SOCV = oT.TB1_SO;
                            r.SO = (sotb + i - 1).ToString();
                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = reStr(oT.TB1_SO + "");
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }

                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.GIOITINH = "";
                    r.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            r.GIOITINH = "ông";
                            r.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            r.GIOITINH = "bà";
                            r.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                    {
                        r.GIOITINH = "các ông, bà";
                        r.GIOITINHHOA = "Các ông, bà";
                    }
                    if (obj["NGAYGHITRENDON"] != null)
                        r.NGAYGUI = GetDate(obj["NGAYGHITRENDON"]);
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";

                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnKinhtrinh_Click(object sender, EventArgs e)
        {
            Session["GDTTT_MABM"] = "KINHTRINHLANHDAO";
            DataTable oDT = getDS(false, true, false, true, false);

            if (oDT != null)
            {

                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    r.NOIDUNG = oT.NOIDUNGDON + "";
                    r.CHIDAO_COKHONG = oT.CHIDAO_COKHONG + "";
                    r.NOIDUNGCHIDAO = oT.CHIDAO_NOIDUNG + "";
                    r.CHUCVULANHDAO = "Chánh án";
                    r.HUONGDAN = oT.CHIDAO_KINHTRINH + "";
                    if (oT.CHIDAO_COKHONG > 0 && oT.CHIDAO_LANHDAOID > 0)
                    {
                        try
                        {
                            DM_CANBO oLD = dt.DM_CANBO.Where(x => x.ID == oT.CHIDAO_LANHDAOID).FirstOrDefault();
                            DM_DATAITEM oCV = dt.DM_DATAITEM.Where(x => x.ID == oLD.CHUCVUID).FirstOrDefault();

                            r.CHUCVULANHDAO = oCV.TEN;
                            r.LANHDAO = " " + oLD.HOTEN;

                        }
                        catch (Exception ex) { }
                    }
                    if (oT.LOAIDON == 3)
                    {
                        r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến)";
                    }
                    if (strSOCV != "")
                    {
                        r.SO = strSOCV;
                        r.NGUOIKY = strNguoiKy;
                        r.NGAY = strNgay;
                        r.THANG = strThang;
                        r.NAM = strNam;
                    }


                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.GIOITINH = "";
                    r.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            r.GIOITINH = "ông";
                            r.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            r.GIOITINH = "bà";
                            r.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        r.GIOITINH = "Các ông, bà";
                    if (obj["NGAYGHITRENDON"] != null)
                        r.NGAYGUI = GetDate(obj["NGAYGHITRENDON"]);
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";

                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }

                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNTADuongsu_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NTA_DANHSACHGUIDS";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                DTGDTTT objds = new DTGDTTT();
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TT = i.ToString();
                    r.TENDONVI = strTendonvi;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    r.NOIDUNG = oT.NOIDUNGDON + "";
                    r.SO = oT.TB1_SO + "";
                    r.NGUOIKY = oT.CD_NGUOIKY + "";
                    if (oT.TB1_NGAY != null)
                    {
                        DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                        r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                        r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                        r.NAM = dtNTB1.Year.ToString();
                    }

                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    if ((obj["arrCongvan"] + "") != "")
                    {
                        r.NGUOIGUI = r.NGUOIGUI + " (Do " + obj["arrCongvan"] + ")";
                    }
                    else if ((obj["LOAIDON"] + "") == "2")
                    {
                        r.NGUOIGUI = r.NGUOIGUI + " (Công văn số " + obj["CV_SO"] + " ngày " + GetDate(obj["CV_NGAY"]) + ")";
                    }
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    r.GHICHU = obj["GHICHU"] + "";
                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNTACoquan_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NTA_THONGBAOCOQUAN";
            DataTable oDT = getDS(true, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có đơn kèm công văn";
                    return;
                }
                #region "Thông tin tờ trình"
                DTGDTTT objds = new DTGDTTT();
                string strTendonvi = "", strPBGui = "", strDiachi = "", strDiadiem = "", strSo = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "";
                strSo = txtBC_SoCV.Text;
                strNguoiky = txtBC_Nguoiky.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();

                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));

                #endregion
                //DANH SÁCH đơn
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_NOIBO_THONGBAORow rds = objds.DT_NOIBO_THONGBAO.NewDT_NOIBO_THONGBAORow();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIACHI = obj["Diachigui"] + "";
                    rds.TENDONVI = strTendonvi;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();
                    rds.NOIDUNG = oT.NOIDUNGDON + "";
                    rds.SOTOTRINH = oT.TB1_SO + "";
                    rds.NGUOIKY = oT.CD_NGUOIKY + "";
                    if (oT.TB1_NGAY != null)
                    {
                        DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                        rds.NGAY = dtNTB1.Day.ToString();
                        rds.THANG = dtNTB1.Month.ToString();
                        rds.NAM = dtNTB1.Year.ToString();
                    }
                    rds.DIADIEM = strDiadiem;
                    rds.TENPHONGBANGUI = strPBGui;
                    rds.GIOITINH = "";
                    rds.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            rds.GIOITINH = "ông";
                            rds.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            rds.GIOITINH = "bà";
                            rds.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                        rds.GIOITINH = "Các ông, bà";
                    if ((obj["BAQD_LOAIQDBA"] + "") == "0")//BA
                    {
                        rds.BA_SO = obj["BAQD_SO"] + "";
                        rds.BA_NGAY = obj["BAQD_NGAYBA"] + "";
                        if (rds.BA_NGAY != "")
                            rds.BA_NGAY = GetDate(rds.BA_NGAY);
                        rds.BA_TOAXX = obj["TOAXX"] + "";
                    }
                    rds.SOCV = obj["CV_SO"] + "";
                    if (obj["CV_NGAY"] != null)
                        rds.NGAYCV = GetDate(obj["CV_NGAY"]);
                    rds.SOTOTRINH = obj["CD_SOCV"] + "";
                    if (obj["CD_NGAYCV"] != null)
                        rds.NGAYTOTRINH = GetDate(obj["CD_NGAYCV"]);

                    rds.TENCOQUAN = obj["CV_TENDONVI"] + "";
                    rds.DIACHICOQUAN = obj["CVDIACHI"] + "";

                    rds.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                    rds.NGUOIKY = strNguoiky;
                    rds.DIACHIPHONGBAN = strDiachi;
                    rds.BIDANH = obj["MADON"] + "-" + obj["BIDANH"];
                    objds.DT_NOIBO_THONGBAO.AddDT_NOIBO_THONGBAORow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnTralaidon_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NTA_TRALAI";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "", strBidanh = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                strBidanh = oNSD.GHICHU + "";
                string TAND_NHAN_ = "";
                if (Session["CAP_XET_XU"] + "" == "TOICAO")
                {
                    TAND_NHAN_ = "TANDTC";
                }
                else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    TAND_NHAN_ = "TANDCC";
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                foreach (DataRow obj in oDT.Rows)
                {
                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.BIDANH = obj["MADON"] + strBidanh;
                    r.GIOITINH = "";
                    r.GIOITINHHOA = "";
                    r.TAND_NHAN = TAND_NHAN_;
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            r.GIOITINH = "ông";
                            r.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            r.GIOITINH = "bà";
                            r.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                    {
                        r.GIOITINH = "các ông, bà";
                        r.GIOITINHHOA = "Các ông, bà";
                    }
                    if (obj["NGAYGHITRENDON"] != null)
                        r.NGAYGUI = GetDate(obj["NGAYGHITRENDON"]);
                    r.HUONGDAN = obj["CD_TRALAI_YEUCAU"] + "";
                    if ((obj["CD_TRALAI_LYDOID"] + "") != "")
                    {
                        if ((obj["CD_TRALAI_LYDOID"] + "") == "0")
                        {
                            r.LYDO = obj["CD_TRALAI_LYDOKHAC"] + "";
                        }
                        else
                        {
                            try
                            {
                                decimal IDDM = Convert.ToDecimal(obj["CD_TRALAI_LYDOID"]);
                                DM_DATAITEM obji = dt.DM_DATAITEM.Where(x => x.ID == IDDM).FirstOrDefault();
                                r.LYDO = obji.TEN;
                            }
                            catch (Exception ex) { }
                        }
                    }

                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnNBInPCTrung_Click(object sender, EventArgs e)
        {
            {
                SetGetSessionTK(true);
                Session["GDTTT_MABM"] = "NOIBO_TBCHUYENTRUNG";
                DataTable oDT = getDS(false, true, false, false, false);
                if (oDT != null)
                {
                    if (oDT.Rows.Count == 0)
                    {
                        lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                        return;
                    }
                    //ĐỊa điểm
                    string strDiadiem = "", strNgay = "", strThang = "", strNam = "", strNguoiky = "", strTenPhongban = "";
                    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                        strDiadiem = "Hà Nội";
                    else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                    DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    if (dNgayCV != DateTime.MinValue)
                    {
                        strNgay = dNgayCV.Day.ToString();
                        strThang = dNgayCV.Month.ToString();
                        strNam = dNgayCV.Year.ToString();
                    }
                    decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                    if (oNSD.PHONGBANID != null)
                    {
                        DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                        strTenPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                    }
                    string TAND_NHAN_ = "";
                    if (Session["CAP_XET_XU"] + "" == "TOICAO")
                    {
                        TAND_NHAN_ = "TANDTC";
                    }
                    else if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                    {
                        TAND_NHAN_ = "TANDCC";
                    }
                    strNguoiky = txtBC_Nguoiky.Text;
                    string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    DTGDTTT objds = new DTGDTTT();
                    DataTable dtTP = oDT.AsEnumerable()
                                   .GroupBy(r => new { NOICHUYEN = r["NOICHUYEN"] })
                                   .Select(g => g.OrderBy(r => r["ID"]).First())
                                   .CopyToDataTable();
                    decimal sotb = Cls_Comon.GetNumber(txtBC_SoCV.Text);
                    foreach (DataRow obj in dtTP.Rows)
                    {

                        DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                        r.TENPHONGBANNHAN = obj["NOICHUYEN"] + "";
                        //manhnd 02/03/2020 nếu có số CV rồi thì lấy ra dùng
                        if ((obj["CD_SOCV"] + "") != "")
                        {

                            r.NGUOIKY = obj["CD_NGUOIKY"].ToString();
                            DateTime vNgayCV = Convert.ToDateTime(obj["CD_NGAYCV"]);
                            r.NGAY = vNgayCV.Day.ToString();
                            r.THANG = vNgayCV.Month.ToString();
                            r.NAM = vNgayCV.Year.ToString();
                            r.SOTOTRINH = obj["CD_SOCV"].ToString();
                            sotb += 1;
                        }
                        else
                        {
                            //nếu chưa có số thì để rỗng
                            r.NGUOIKY = "";
                            r.NGAY = "";
                            r.THANG = "";
                            r.NAM = "";
                            r.SOTOTRINH = "";
                            sotb += 1;

                        }

                        //r.NGUOIKY = strNguoiky;
                        //r.NGAY = strNgay;
                        //r.THANG = strThang;
                        //r.NAM = strNam;
                        //if (txtBC_SoCV.Text != "")
                        //{
                        //    r.SOTOTRINH = sotb + "";
                        //    sotb += 1;
                        //}
                        r.TUNGAY = txtNgaynhapTu.Text;
                        r.DENNGAY = txtNgaynhapDen.Text;
                        r.TENDONVI = strTendonvi;
                        r.TENPHONGBANGUI = strTenPhongban;
                        r.DIADIEM = strDiadiem;
                        r.TAND_NHAN = TAND_NHAN_;
                        objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                        objds.AcceptChanges();

                    }
                    objds.AcceptChanges();
                    Session["NOIBO_DATASET"] = objds;
                }
                string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }
        protected void ddlTrangthaichuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            //setButtonPrint(false, btnChuyendon);
            //switch (ddlTrangthaichuyen.SelectedValue)
            //{
            //    case "0":
            //        setButtonPrint(true, btnChuyendon);
            //        //Cls_Comon.SetButton(cmdSua, true);
            //        break;
            //    case "3":
            //        setButtonPrint(true, btnChuyendon);
            //        // Cls_Comon.SetButton(cmdSua, true);
            //        break;
            //    case "-1":

            //        setButtonPrint(true, btnChuyendon);
            //        // Cls_Comon.SetButton(cmdSua, true);
            //        break;
            //    default:
            //        // Cls_Comon.SetButton(cmdSua, false);
            //        break;
            //}
            ShowButtonPrint();
        }
        protected void btnChuyendon_Click(object sender, EventArgs e)
        {
            if ((txtBC_Ngaydk.Text != "" || txtBC_SoCV.Text != "" || txtBC_Nguoiky.Text != "") && (txtBC_Ngaydk.Text == "" || txtBC_SoCV.Text == "" || txtBC_Nguoiky.Text == ""))
            {
                lbtthongbao.Text = "Chưa nhập đầy đủ thông tin số công văn, ngày chuyển, người ký để chuyển đơn !";
                return;
            }
            try
            {
                decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
                decimal PBID = 0;
                if (strPBID != "") PBID = Convert.ToDecimal(strPBID);

                bool flag = false;
                int iCount = 0;
                foreach (DataGridItem Item in dgList.Items)
                {
                    CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                    if (chkChon.Checked)
                    {
                        flag = true;
                        string strID = Item.Cells[0].Text;
                        decimal ID = Convert.ToDecimal(strID);
                        GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                        if ((oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3) && (oT.CD_LOAI > 0 || (oT.CD_LOAI == 0 && oT.CD_TA_TRANGTHAI == 0)))
                        {
                            //anhvh node:03.08.2020  
                            //COMMENT ON COLUMN GDTTT_DON.CD_TRANGTHAI IS 'TRANG THAI CHUYEN(0,null Chưa chuyển,1 Đã chuyển,2 Đã nhận,3 Bị trả lại)';
                            //COMMENT ON COLUMN GDTTT_DON.CD_LOAI  IS 'NOICHUYEN(0 nội bộ,1 tòa khác,2 ngoài tòa án)';
                            //COMMENT ON COLUMN GDTTT_DON.CD_TA_TRANGTHAI IS '(1 Đơn chưa đủ điều kiện,0 Đơn đủ điều kiện)';
                            //COMMENT ON COLUMN GDTTT_DON.BAQD_TOAANID IS 'Tòa xét xử';
                            //COMMENT ON COLUMN GDTTT_DON.BAQD_LOAIAN IS 'Loại án ID';
                            //COMMENT ON COLUMN GDTTT_DON.BAQD_CAPXETXU IS 'Cấp xét xử';
                            //COMMENT ON COLUMN GDTTT_DON.DONTRUNGID IS 'Đơn trùng';
                            //COMMENT ON COLUMN GDTTT_DON.CD_SOCV IS 'Số công văn chuyển';

                            oT.CD_TRANGTHAI = 1;
                            oT.CD_NGAYXULY = DateTime.Now;
                            //---------------------------------
                            if ((oT.CD_SOCV + "") != "")
                            {
                                iCount += 1;
                                //Lưu lịch sử
                                Update_DonChuyen(oT, PBID, chkChon.ToolTip);
                                //CHuyển đồng thời các đơn trùng kèm theo
                                string[] strarr = chkChon.ToolTip.Split(',');
                                if (strarr.Length > 1)
                                {
                                    for (int k = 0; k < strarr.Length; k++)
                                    {
                                        decimal kID = Convert.ToDecimal(strarr[k]);
                                        GDTTT_DON kDon = dt.GDTTT_DON.Where(x => x.ID == kID).FirstOrDefault();
                                        kDon.CD_TRANGTHAI = 1;
                                        kDon.CD_NGAYXULY = oT.CD_NGAYXULY;
                                    }
                                }
                            }
                        }
                    }
                }
                dt.SaveChanges();
                if (flag == false)
                {
                    lbtthongbao.Text = "Chưa chọn đơn để chuyển !";
                    return;
                }
                else
                {

                    Load_Data();
                    lbtthongbao.Text = "Hoàn thành chuyển " + iCount.ToString() + " đơn !";
                    //Đóng nút chuyển đơn
                    btnChuyendon.Enabled = false;
                    btnChuyendon.CssClass = "buttonprintdisable";
                }
            }
            catch (Exception ex)
            {
                lbtthongbao.Text = "Lỗi khi chuyển đơn: " + ex.Message;
            }
        }
        void Update_DonChuyen(GDTTT_DON oT, Decimal PBID, String ArrDonTrung)
        {
            Decimal DonID = oT.ID;
            GDTTT_DON_CHUYEN objLS;
            List<GDTTT_DON_CHUYEN> lstC = dt.GDTTT_DON_CHUYEN.Where(x => x.DONID == DonID
            && x.DONVINHANID == oT.TOAANID
            && x.PHONGBANNHANID == oT.CD_TA_DONVIID).OrderByDescending(x => x.NGAYCHUYEN).ToList();
            //List<GDTTT_DON_CHUYEN> lstC = dt.GDTTT_DON_CHUYEN.Where(x => x.DONID == DonID).OrderByDescending(x => x.NGAYCHUYEN).ToList();
            if (lstC.Count > 0)
                objLS = lstC[0];
            else
                objLS = new GDTTT_DON_CHUYEN();
            objLS.DONID = DonID;
            objLS.DONVICHUYENID = oT.TOAANID;
            objLS.PHONGBANCHUYENID = PBID;
            objLS.NGAYCHUYEN = DateTime.Now;
            objLS.TRANGTHAI = 1;//đã chuyển
            objLS.NGUOICHUYEN = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            objLS.SOCV = oT.CD_SOCV;
            objLS.NGUOIKY = oT.CD_NGUOIKY;
            objLS.NGAYCV = oT.CD_NGAYCV;
            objLS.LOAICHUYEN = oT.CD_LOAI;

            string[] strarr = ArrDonTrung.Split(',');
            objLS.SOLUONGDON = strarr.Length;//Số lượng đơn chuyển đến
            objLS.ARRDONTRUNG = ArrDonTrung;//Danh sách các đơn chuyển cùng 
            switch ((int)oT.CD_LOAI)
            {
                case 0:
                    objLS.DONVINHANID = oT.TOAANID;
                    objLS.PHONGBANNHANID = oT.CD_TA_DONVIID;
                    break;
                case 1:
                    objLS.DONVINHANID = oT.CD_TK_DONVIID;
                    objLS.PHONGBANNHANID = 0;
                    break;
            }
            if (lstC.Count == 0)
                dt.GDTTT_DON_CHUYEN.Add(objLS);
            dt.SaveChanges();
            ///////anhvh 03.08.2020 insert vào bảng trung gian chỉ insert các tòa cấp cao
            //GDTTT_TRUNG_GIAN O_bject = new GDTTT_TRUNG_GIAN();
            //Int32 _COUNT_RE = 0;
            /////manhnd phải bắt tòa chuyển đến là tòa cấp cao; hỏi lại vụ đơn trùng
            //if (strarr.Length == 1)
            //{
            //    O_bject.HOSO_CHUYEN_DON(ref _COUNT_RE, Guid.NewGuid().ToString(), DonID.ToString(), oT.TOAANID.ToString(), Session[ENUM_SESSION.SESSION_USERNAME] + "", objLS.DONVINHANID.ToString());
            //    //debug _COUNT_RE>0 đã insert vào bảng tạm thành công
            //}
            //else if (strarr.Length > 1)
            //{
            //    _COUNT_RE = 0;
            //    for (Int32 i = 0; i < strarr.Length; i++)
            //    {
            //        O_bject.HOSO_CHUYEN_DON(ref _COUNT_RE, Guid.NewGuid().ToString(), strarr[i].ToString(), oT.TOAANID.ToString(), Session[ENUM_SESSION.SESSION_USERNAME] + "", objLS.DONVINHANID.ToString());
            //        //debug _COUNT_RE>0 đã insert vào bảng tạm thành công
            //    }
            //}
        }
        protected void btnThuHoi_Click(object sender, EventArgs e)
        {

            try
            {
                decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
                decimal PBID = 0;
                if (strPBID != "") PBID = Convert.ToDecimal(strPBID);

                bool flag = false;
                int iCount = 0;
                String da_nhan = "";//anhvh add 06/04/2021
                foreach (DataGridItem Item in dgList.Items)
                {
                    CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                    if (chkChon.Checked)
                    {
                        flag = true;
                        string strID = Item.Cells[0].Text;
                        decimal ID = Convert.ToDecimal(strID);
                        GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                        //COMMENT ON COLUMN GDTTT_DON.CD_TRANGTHAI IS 'TRANG THAI CHUYEN(0,null Chưa chuyển,1 Đã chuyển,2 Đã nhận,3 Bị trả lại)';
                        //COMMENT ON COLUMN GDTTT_DON.CD_LOAI  IS 'NOICHUYEN(0 nội bộ,1 tòa khác,2 ngoài tòa án)';
                        //COMMENT ON COLUMN GDTTT_DON.CD_TA_TRANGTHAI IS '(1 Đơn chưa đủ điều kiện,0 Đơn đủ điều kiện)';
                        //COMMENT ON COLUMN GDTTT_DON.BAQD_TOAANID IS 'Tòa xét xử';
                        //COMMENT ON COLUMN GDTTT_DON.BAQD_LOAIAN IS 'Loại án ID';
                        //COMMENT ON COLUMN GDTTT_DON.BAQD_CAPXETXU IS 'Cấp xét xử';
                        //COMMENT ON COLUMN GDTTT_DON.DONTRUNGID IS 'Đơn trùng';
                        //COMMENT ON COLUMN GDTTT_DON.CD_SOCV IS 'Số công văn chuyển';
                        if (oT.CD_TRANGTHAI == 2)
                        {
                            da_nhan += oT.NGUOIGUI_HOTEN + ";";
                        }
                        if ((oT.CD_TRANGTHAI == 1) && (oT.CD_LOAI > 0 || (oT.CD_LOAI == 0 && oT.CD_TA_TRANGTHAI == 0)))
                        {
                            oT.CD_TRANGTHAI = 0;
                            oT.CD_NGAYXULY = DateTime.Now;
                            iCount += 1;
                            string strArrDon = chkChon.ToolTip;
                            string[] strarr = strArrDon.Split(',');

                            //CHuyển đồng thời các đơn trùng kèm theo
                            if (strarr.Length > 1)
                            {
                                for (int k = 0; k < strarr.Length; k++)
                                {
                                    decimal kID = Convert.ToDecimal(strarr[k]);
                                    GDTTT_DON kDon = dt.GDTTT_DON.Where(x => x.ID == kID).FirstOrDefault();
                                    kDon.CD_TRANGTHAI = 0;
                                    dt.SaveChanges();
                                }
                            }
                            //Xóa thông tin đẫ chuyển từ bảng đơn chuyển khi Thu Hoi đơn
                            //objLS.DONVINHANID = oT.CD_TK_DONVIID;  --Don vi nhan
                            xoa_thongtinchuyen(ID, Convert.ToDecimal(oT.CD_TK_DONVIID));
                            //manhnd thêm kt nếu không phải là nội bộ thi mới chạy đoạn này
                            //if (oT.CD_LOAI != 0)
                            //{
                            //    ///////anhvh 21.08.2020 insert vào bảng trung gian chỉ thu hồi các tòa cấp cao
                            //    GDTTT_TRUNG_GIAN O_bject = new GDTTT_TRUNG_GIAN();
                            //    Int32 _COUNT_RE = 0;
                            //    _COUNT_RE = 0;
                            //    O_bject.HOSO_THU_HOI_DON(ref _COUNT_RE, "," + strArrDon + ",");
                            //    //debug _COUNT_RE>0 đã insert vào bảng tạm thành công
                            //}
                        }
                        dt.SaveChanges();
                    }
                }
                dt.SaveChanges();
                if (flag == false)
                {
                    lbtthongbao.Text = "Chưa chọn đơn để thu hồi !";
                    return;
                }
                else
                {
                    Load_Data();
                    if (da_nhan != "")
                    {
                        lbtthongbao.Text = "đơn: '" + da_nhan + "' .đã nhận không thể thu hồi";
                    }
                    else
                    {
                        lbtthongbao.Text = "Hoàn thành thu hồi " + iCount.ToString() + " đơn !";
                    }
                }
            }
            catch (Exception ex)
            {
                lbtthongbao.Text = "Lỗi khi chuyển đơn: " + ex.Message;
            }
        }
        protected void lbtTTBC_Click(object sender, EventArgs e)
        {
            if (pnTTBC.Visible)
            {
                lbtTTBC.Text = "[ Mở ]";
                pnTTBC.Visible = false;
                Session["TTBCVISIBLE"] = "0";
            }
            else
            {
                lbtTTBC.Text = "[ Đóng ]";
                pnTTBC.Visible = true;
                Session["TTBCVISIBLE"] = "1";
            }
        }
        protected void lbt_vt_vbd_Click(object sender, EventArgs e)
        {
            if (pn_VT_VBD.Visible)
            {
                lbt_vt_vbd.Text = "[ Mở ]";
                pn_VT_VBD.Visible = false;
                Session["VT_VBD_VISIBLE"] = "0";
            }
            else
            {
                lbt_vt_vbd.Text = "[ Đóng ]";
                pn_VT_VBD.Visible = true;
                Session["VT_VBD_VISIBLE"] = "1";
            }
        }
        protected void lbtTTTK_Click(object sender, EventArgs e)
        {
            if (pnTTTK.Visible == false)
            {
                lbtTTTK.Text = "[ Thu gọn ]";
                pnTTTK.Visible = true;
                Session["TTTKVISIBLE"] = "0";
            }
            else
            {
                lbtTTTK.Text = "[ Nâng cao ]";
                pnTTTK.Visible = false;
                Session["TTTKVISIBLE"] = "1";
            }
        }
        protected void cmdSua_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            if (ddlNoichuyenden.SelectedValue == "1")
            {
                string StrMsg = "PopupReport('/QLAN/GDTTT/Hoso/Popup/SuaToaKhac.aspx','Sửa đổi danh sách',1150,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
            else if (ddlNoichuyenden.SelectedValue == "2")
            {
                string StrMsg = "PopupReport('/QLAN/GDTTT/Hoso/Popup/SuaNgoaiTA.aspx','Sửa đổi danh sách',1150,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
            else
            {
                string StrMsg = "PopupReport('/QLAN/GDTTT/Hoso/Popup/Suadanhsach.aspx','Sửa đổi danh sách',1150,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }
        protected void cmdLammoi_Click(object sender, EventArgs e)
        {
            txtNguoigui.Text = "";
            txtSoQDBA.Text = "";
            txtNgayBAQD.Text = "";
            ddlToaXetXu.SelectedIndex = 0;
            txtNgayNhanTu.Text = "";
            txtNgayNhanDen.Text = "";

            ddlHuyen.SelectedIndex = 0;
            txtDiachi.Text = "";
            ddlTraloi.SelectedIndex = 0;
            ddlHinhthucdon.SelectedIndex = 0;
            txtSoCMND.Text = "";
            txtSohieudon.Text = "";
            ddlNoichuyenden.SelectedIndex = 0;
            ddlTrangthaidon.SelectedIndex = 0;
            txtNgaychuyenTu.Text = txtNgaychuyenDen.Text = "";
            ddlThuLy.SelectedIndex = 0;
            //ddlPhanloaiDdon.SelectedIndex = 0;
            txtCV_So.Text = "";
            txtCV_Ngay.Text = "";
            ddlTrangthaichuyen.SelectedIndex = 0;
            ddlChidao.SelectedIndex = 0;
            ddlTraigiam.SelectedIndex = 0;
            txtThuly_Tu.Text = txtThuly_Den.Text = txtThuly_So.Text = "";
            ddlLoaiCV.SelectedIndex = 0;
            ddlThamphan.SelectedIndex = 0;
            txtNgaynhapTu.Text = txtNgaynhapDen.Text = "";
            ddlLoaiAn.SelectedIndex = 0;
            ddlAnTuHinh.SelectedIndex = 0;
            ddlChuyentoi.SelectedIndex = 0;
            lbtthongbao.Text = string.Empty;
            Session["VUANID_CC"] = string.Empty;
            Session["VUVIECID_CC"] = string.Empty;
            //--------
            Drop_NOI_NHAN_SEARCH.SelectedValue = string.Empty;
            Drop_TRANGTHAICHUYEN.SelectedValue = string.Empty;
            Drop_LOAI_VB_Search.SelectedValue = string.Empty;
            txt_SODEN_SEARCH.Text = string.Empty;
            txt_SODEN_SEARCH_DEN.Text = string.Empty;
            txt_NGAY_FROM.Text = string.Empty;
            txt_NGAY_TO.Text = string.Empty;
            txt_NGUOI_GUI_BT_SEARCH.Text = string.Empty;

            //--------------------------------
            Session[SS_TK.NGUOIGUI] = string.Empty;




            //--------
            foreach (ListItem i in chkNguoinhap.Items)
            {
                i.Selected = false;
            }
            ddlNoichuyenden_SelectedIndexChanged(new object(), new EventArgs());
        }
        protected void Drop_TRANGTHAICHUYEN_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session[SS_TK.TRANG_THAI_XLY_VT] = string.Empty;
        }
        protected void cmdGopdon_Click(object sender, EventArgs e)
        {
            string vArrSelectID = "";
            int count = 0;
            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                if (chkChon.Checked)
                {
                    count += 1;
                    if (vArrSelectID == "") vArrSelectID = chkChon.ToolTip;
                    else vArrSelectID = vArrSelectID + "," + chkChon.ToolTip;
                }
            }
            if (count == 0)
            {
                lbtthongbao.Text = "Chưa chọn các đơn để gộp đơn trùng !";
                return;
            }
            else if (count == 1)
            {
                lbtthongbao.Text = "Số lượng chọn để gộp đơn trùng phải lớn hơn 1 !";
                return;
            }
            string[] arrID = vArrSelectID.Split(',');
            decimal DonGocID = Convert.ToDecimal(arrID[0]);
            decimal SoDonTrung = 0;
            decimal dblVuViecID = 0;
            //Update các đơn trùng
            string strKQGQ = "";
            for (int i = 1; i < arrID.Length; i++)
            {
                SoDonTrung += 1;
                decimal ID = Convert.ToDecimal(arrID[i]);
                GDTTT_DON oDT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                oDT.DONTRUNGID = DonGocID;
                oDT.SOLUONGDON = 1;
                if (oDT.VUVIECID != null)
                {
                    if (oDT.VUVIECID > 0) dblVuViecID = (decimal)oDT.VUVIECID;
                }
                dt.SaveChanges();
                if ((oDT.CV_TRALOI_NOIDUNG + "") != "")
                {
                    if (strKQGQ == "")
                        strKQGQ = oDT.CV_TRALOI_NOIDUNG;
                    else
                        strKQGQ = oDT.CV_TRALOI_NOIDUNG + "\n" + strKQGQ;
                }
                List<GDTTT_DON> lstTrung = dt.GDTTT_DON.Where(x => x.DONTRUNGID == ID).ToList();
                if (lstTrung.Count > 0)
                {
                    foreach (GDTTT_DON d in lstTrung)
                    {
                        d.DONTRUNGID = DonGocID;
                        d.SOLUONGDON = 1;
                        SoDonTrung += 1;
                        dt.SaveChanges();
                        if ((d.CV_TRALOI_NOIDUNG + "") != "")
                        {
                            if (strKQGQ == "")
                                strKQGQ = d.CV_TRALOI_NOIDUNG;
                            else
                                strKQGQ = d.CV_TRALOI_NOIDUNG + "\n" + strKQGQ;
                        }
                    }
                }
            }
            //Update đơn gốc
            GDTTT_DON oDGoc = dt.GDTTT_DON.Where(x => x.ID == DonGocID).FirstOrDefault();
            oDGoc.DONTRUNGID = 0;
            oDGoc.SOLUONGDON = oDGoc.SOLUONGDON + SoDonTrung;
            if (oDGoc.VUVIECID == null || oDGoc.VUVIECID == 0)
            {
                oDGoc.VUVIECID = dblVuViecID;
            }
            if (strKQGQ != "")
            {
                oDGoc.CV_TRALOI_NOIDUNG = strKQGQ + "\n" + oDGoc.CV_TRALOI_NOIDUNG;
            }
            dt.SaveChanges();
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            oBL.UPDATESOLUONGDON(DonGocID);
            Load_Data();
            lbtthongbao.Text = "Hoàn thành gộp đơn !";
        }
        protected void btnKNPhieuchuyen_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);

            Session["GDTTT_MABM"] = "NOIBO_KNTCPHIEUCHUYEN1";

            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    r.NOIDUNG = oT.NOIDUNGDON + "";

                    r.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    r.DIACHIGUI = obj["Diachigui"] + "";
                    r.GIOITINH = "";
                    r.GIOITINHHOA = "";
                    if ((obj["DUNGDONLA"] + "") == "1")
                    {
                        if ((obj["NGUOIGUI_GIOITINH"] + "") == "1")
                        {
                            r.GIOITINH = "ông";
                            r.GIOITINHHOA = "Ông";
                        }
                        else
                        {
                            r.GIOITINH = "bà";
                            r.GIOITINHHOA = "Bà";
                        }
                    }
                    else if ((obj["DUNGDONLA"] + "") == "2")
                    {
                        r.GIOITINH = "các ông, bà";
                        r.GIOITINHHOA = "Các ông, bà";
                    }
                    if (obj["NGAYGHITRENDON"] != null)
                        r.NGAYGUI = GetDate(obj["NGAYGHITRENDON"]);
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";

                    bool isDacoSo = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {
                            oT.TB1_SO = (sotb + i - 1).ToString();
                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            //manhnd bỏ 02/03/2020
                            //oT.CD_NGUOIKY = strNguoiKy;
                            //oT.CD_SOCV = oT.TB1_SO;
                            //oT.CD_NGAYCV = dNgayCV;
                            r.SO = (sotb + i - 1).ToString();
                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = oT.TB1_SO + "";
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }


                    r.TMP1 = r.TENDONVINHAN;
                    if (oT.LOAIDON == 3)
                    {
                        if ((oT.CV_SO + "") == "")
                            r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến)";
                        else
                            r.GHICHU = " (do " + oT.CV_TENDONVI + " chuyển đến theo Công văn số " + oT.CV_SO + " ngày " + GetDate(oT.CV_NGAY) + ")";

                    }
                    objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                    objds.AcceptChanges();
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnKNPhieuchuyenN_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            bool isLOAICV_81 = false;
            Session["GDTTT_MABM"] = "NOIBO_KNTCPHIEUCHUYENN";
            //if (ddlLoaiCV.SelectedValue != "-1" && ddlLoaiCV.SelectedValue != "0")
            //{
            //    decimal IDLoai = Convert.ToDecimal(ddlLoaiCV.SelectedValue);
            //    DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
            //    if (oLoai.ID == 1023 || oLoai.CAPCHAID == 1023)
            //    {
            //        isLOAICV_81 = true;
            //        Session["GDTTT_MABM"] = "NOIBO_KNTCPHIEUCHUYENN81";
            //    }
            //}

            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }
                string strSOCV = txtBC_SoCV.Text;
                string strNguoiKy = txtBC_Nguoiky.Text;
                string strNgay = "", strThang = "", strNam = "";
                string strTendonvi = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    strNgay = Cls_Comon.toFullNumber(dNgayCV.Day.ToString(), false);
                    strThang = Cls_Comon.toFullNumber(dNgayCV.Month.ToString(), true);
                    strNam = dNgayCV.Year.ToString();
                }
                string strPhongban = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    strPhongban = oPB.TENPHONGBAN.Replace("TANDTC", " ");
                }
                //ĐỊa điểm
                string strDiadiem = "";
                if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == "1")
                    strDiadiem = "Hà Nội";
                else strDiadiem = getDiaDiem(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                DTGDTTT objds = new DTGDTTT();
                decimal sotb = Cls_Comon.GetNumber(strSOCV);
                int i = 0;
                string strDVNhan = "";
                bool isNewDVNhan = false;
                //Order by lại Nơi chuyển đến
                DataView dv = new DataView(oDT);
                dv.Sort = "NOICHUYEN ASC";
                oDT = dv.ToTable();
                if (sotb > 0) sotb = sotb - 1;
                foreach (DataRow obj in oDT.Rows)
                {
                    i = i + 1;
                    decimal DonID = Convert.ToDecimal(obj["ID"]);
                    GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == DonID).FirstOrDefault();

                    DTGDTTT.DT_NTA_PHIEUCHUYENRow r = objds.DT_NTA_PHIEUCHUYEN.NewDT_NTA_PHIEUCHUYENRow();
                    r.TENDONVI = strTendonvi;
                    r.TENPHONGBANGUI = strPhongban;
                    r.TENDONVINHAN = obj["NOICHUYEN"] + "";
                    r.SO = strSOCV;
                    r.DIADIEM = strDiadiem;
                    r.NGAY = strNgay;
                    r.THANG = strThang;
                    r.NAM = strNam;
                    r.NGUOIKY = strNguoiKy;
                    bool isDacoSo = false;

                    if (r.TENDONVINHAN != strDVNhan)
                    {
                        strDVNhan = r.TENDONVINHAN;
                        isNewDVNhan = true;
                    }
                    else
                        isNewDVNhan = false;
                    if (oT.CD_TRANGTHAI == 0 || oT.CD_TRANGTHAI == 3)
                    {
                        if (strSOCV != "")
                        {

                            if (dNgayCV != DateTime.MinValue)
                                oT.TB1_NGAY = dNgayCV;
                            //manhnd bỏ 02/03/2020
                            //oT.CD_NGUOIKY = strNguoiKy;

                            //oT.CD_NGAYCV = dNgayCV;
                            if (isNewDVNhan)
                            {
                                sotb = sotb + 1;
                                oT.TB1_SO = sotb.ToString();
                                // oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }
                            else
                            {
                                oT.TB1_SO = sotb.ToString();
                                //oT.CD_SOCV = oT.TB1_SO;
                                r.SO = sotb.ToString();
                            }

                            r.NGUOIKY = strNguoiKy;
                            r.NGAY = strNgay;
                            r.THANG = strThang;
                            r.NAM = strNam;
                            isDacoSo = true;
                        }
                    }
                    if (isDacoSo == false)
                    {
                        r.SO = oT.TB1_SO + "";
                        r.NGUOIKY = oT.CD_NGUOIKY + "";
                        if (oT.TB1_NGAY != null)
                        {
                            DateTime dtNTB1 = (DateTime)oT.TB1_NGAY;
                            r.NGAY = Cls_Comon.toFullNumber(dtNTB1.Day.ToString(), false);
                            r.THANG = Cls_Comon.toFullNumber(dtNTB1.Month.ToString(), true);
                            r.NAM = dtNTB1.Year.ToString();
                        }
                    }
                    dt.SaveChanges();

                    if (isNewDVNhan)
                    {
                        r.TMP1 = r.TENDONVINHAN;
                        if (isLOAICV_81)
                        {
                            if (oT.CD_TK_NOIGUI == 1)
                            {
                                r.TMP1 = "Đồng chí Chánh án " + r.TENDONVINHAN;
                                if (r.TMP1.Contains("cấp cao tại thành phố Hồ"))
                                    r.TMP1 = r.TMP1.Replace("cấp cao tại", "cấp cao\n tại");
                                r.TMP2 = "đồng chí Chánh án ";
                                r.TMP3 = "để chỉ đạo";
                            }
                            else
                            {
                                r.TMP1 = r.TENDONVINHAN;
                                r.TMP2 = "";
                                r.TMP3 = "để xem xét và";
                            }

                        }
                        objds.DT_NTA_PHIEUCHUYEN.AddDT_NTA_PHIEUCHUYENRow(r);
                        objds.AcceptChanges();
                    }
                }
                dt.SaveChanges();
                objds.AcceptChanges();
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Phiếu chuyển',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);

        }
        protected void btnKNDanhsach_Click(object sender, EventArgs e)
        {
            SetGetSessionTK(true);
            Session["GDTTT_MABM"] = "NOIBO_KNTCDS";
            DataTable oDT = getDS(false, true, false, false, false);
            if (oDT != null)
            {
                if (oDT.Rows.Count == 0)
                {
                    lbtthongbao.Text = "Không có dữ liệu theo yêu cầu tìm kiếm !";
                    return;
                }

                DTGDTTT objds = new DTGDTTT();
                #region "Thông tin tờ trình"

                DTGDTTT.DT_NOIBO_TOTRINHRow r = objds.DT_NOIBO_TOTRINH.NewDT_NOIBO_TOTRINHRow();
                r.SOTOTRINH = txtBC_SoCV.Text;
                r.NGUOIKY = txtBC_Nguoiky.Text;
                r.TUNGAY = txtNgaynhapTu.Text;
                r.DENNGAY = txtNgaynhapDen.Text;
                DateTime dNgayCV = (String.IsNullOrEmpty(txtBC_Ngaydk.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtBC_Ngaydk.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayCV != DateTime.MinValue)
                {
                    r.NGAY = dNgayCV.Day.ToString();
                    r.THANG = dNgayCV.Month.ToString();
                    r.NAM = dNgayCV.Year.ToString();
                }
                r.TENDONVI = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                //Phòng ban
                r.TENPHONGBANGUI = "";
                decimal IDNSD = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDNSD).FirstOrDefault();
                if (oNSD.PHONGBANID != null)
                {
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == oNSD.PHONGBANID).FirstOrDefault();
                    r.TENPHONGBANGUI = oPB.TENPHONGBAN;
                }
                if (ddlPhongban.SelectedValue != "0")
                    r.TENPHONGBANNHAN = ddlPhongban.SelectedItem.Text;

                objds.DT_NOIBO_TOTRINH.AddDT_NOIBO_TOTRINHRow(r);
                objds.AcceptChanges();
                #endregion
                int i = 0;
                foreach (DataRow obj in oDT.Rows)
                {
                    i += 1;
                    DTGDTTT.DT_KNTC_DANHSACHRow rds = objds.DT_KNTC_DANHSACH.NewDT_KNTC_DANHSACHRow();
                    rds.TT = i.ToString();
                    rds.NGUOIGUI = obj["DONGKHIEUNAI"] + "";
                    rds.DIAPHUONG = obj["Diachigui"] + "";
                    rds.SODON = obj["SODON"] + "";
                    rds.NOIDUNGDON = obj["NOIDUNGTOMTAT"] + "";
                    objds.DT_KNTC_DANHSACH.AddDT_KNTC_DANHSACHRow(rds);
                    objds.AcceptChanges();
                }
                Session["NOIBO_DATASET"] = objds;
            }
            string StrMsg = "PopupReport('/QLAN/GDTTT/In/ViewReport.aspx','Tờ trình',800,800);";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
        }
        public string CatXau(string str, int length)
        {
            if (str.Length > length)
            {
                if (str.Substring(length, 1) == " ")
                {
                    //Hết 1 từ
                    str = str.Substring(0, length);
                }
                else
                {
                    //Cắt giữa từ
                    str = str.Substring(0, length);
                    str = str.Substring(0, str.LastIndexOf(' '));
                }
                while (str.Substring(str.Length - 1, 1) == " ") str = str.Substring(0, str.Length - 1);
                str += "...";

            }

            return str;

        }
        void xoa_thongtinchuyen(Decimal CurrDonID, Decimal CurrDonViNhanID)
        {
            try
            {
                List<GDTTT_DON_CHUYEN> lstC = dt.GDTTT_DON_CHUYEN.Where(x => x.DONID == CurrDonID
                                                                            && x.DONVINHANID == CurrDonViNhanID).OrderByDescending(x => x.NGAYCHUYEN).ToList();
                if (lstC.Count > 0)
                {
                    GDTTT_DON_CHUYEN oC = lstC[0];
                    //Xóa thông tin đẫ chuyển từ bảng đơn chuyển
                    dt.GDTTT_DON_CHUYEN.Remove(oC);
                    dt.SaveChanges();

                }
            }
            catch (Exception ex) { }
        }
        //Tiểu hồ sơ
        protected void btnNBTieuhoso_Click(object sender, EventArgs e)
        {
                Literal Table_Str_Totals = new Literal();
                DataTable tbl = new DataTable();
                DataRow row = tbl.NewRow();
                //-------------
                tbl = getDS_BC(13, false, true, false, false, false);
                //-----------
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    row = tbl.Rows[0];
                    Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
                    //Table_Str_Totals.Text = "<table></table>";
                }
                //--------------------------
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=Tieu_ho_so.doc");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/msword";
                HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
                Response.Write("<html");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
                Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
                Response.Write("<meta name=ProgId content=Word.Document>");
                Response.Write("<meta name=Generator content=Microsoft Word 9>");
                Response.Write("<meta name=Originator content=Microsoft Word 9>");
                Response.Write("<style>");
                Response.Write("<!-- /* Style Definitions */" +
                                              "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                              "{margin:0in;" +
                                              "margin-bottom:.0001pt;" +
                                              "mso-pagination:widow-orphan;" +
                                              "tab-stops:center 3.0in right 6.0in;" +
                                              "font-size:12.0pt;}");
                Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.28in 0.39in 0.24in 0.17in;mso-header-margin:0.5in;mso-footer-margin:.5in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section1 {page:Section1;}");
                Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape; margin:0.28in 0.39in 0.24in 0.17in;mso-header: h1;mso-header-margin:.0in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
                Response.Write("div.Section2 {page:Section2;}");
                Response.Write("<style>");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.Write("<div class=Section1>");//chỉ định khổ giấy
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Table_Str_Totals.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.Write("</div>");
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
        }
        //Danh sách văn bản hành chính, tài liệu chung
        protected void btnIndanhsach_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();

            string UserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            QT_NGUOISUDUNG oT = dt.QT_NGUOISUDUNG.Where(x => x.USERNAME == UserName).FirstOrDefault();

            //-------------
            tbl = getDS_BC(14, false, true, false, false, false);
            //-------------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            //-------------------Export---------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=Danh sách văn bản hành chính tài liệu chung.xls");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
            // add the style props to get the page orientation
            Response.Write("<html xmlns:o='urn:schemas-microsoft-com:office:office'\n" + "xmlns:x='urn:schemas-microsoft-com:office:excel'\n" + "xmlns='http://www.w3.org/TR/REC-html40'>\n" + "<head>\n");
            Response.Write("<style>\n" + "@page\n" + "{margin:0.5905511811in 0.2992125984in 0.5905511811in 0.5984251969in;\n" + "mso-footer-data:\"" + oT.GHICHU + "\";\n" +
                           "mso-header-margin:.5in;\n" + "mso-footer-margin:.5in;\n" + "mso-page-orientation:landscape;}\n" + "</style>\n");
            Response.Write("<!--[if gte mso 9]><xml>\n");
            Response.Write("<x:ExcelWorkbook>\n" + "<x:ExcelWorksheets>\n" + "<x:ExcelWorksheet>\n");
            Response.Write("<x:Name>Projects 3 </x:Name>\n");
            Response.Write("<x:WorksheetOptions>\n");
            Response.Write("<x:Print>\n" + "<x:ValidPrinterInfo/>\n" + "<x:PaperSizeIndex>9</x:PaperSizeIndex>\n" + "<x:HorizontalResolution>600</x:HorizontalResolution\n" +
                           "<x:VerticalResolution>600</x:VerticalResolution\n" + "</x:Print>\n");
            Response.Write("<x:Selected/>\n");
            Response.Write("<x:DoNotDisplayGridlines/>\n");
            Response.Write("<x:ProtectContents>False</x:ProtectContents>\n" + "<x:ProtectObjects>False</x:ProtectObjects>\n" + "<x:ProtectScenarios>False</x:ProtectScenarios>\n");
            Response.Write("</x:WorksheetOptions>\n" + "</x:ExcelWorksheet>\n" + "</x:ExcelWorksheets>\n");
            Response.Write("<x:WindowHeight>12780</x:WindowHeight>\n" + "<x:WindowWidth>19035</x:WindowWidth>\n" + "<x:WindowTopX>0</x:WindowTopX>\n" +
                           "<x:WindowTopY>15</x:WindowTopY>\n" + "<x:ProtectStructure>False</x:ProtectStructure>\n" + "<x:ProtectWindows>False</x:ProtectWindows>\n");
            Response.Write("</x:ExcelWorkbook>\n" + "</xml><![endif]-->\n" + "</head>\n" + "<body>\n");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</body>");   // add the style props to get the page orientation
            Response.Write("</html>");   // add the style props to get the page orientation
            Response.End();
        }
    }
}
