﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL.GSTP.BANGSETGET;
using BL.GSTP.GDTTT;

namespace WEB.GSTP.QLAN.GDTTT.Hoso
{
    public partial class Thongtindon : System.Web.UI.Page
    {
        //private static GSTPContext dt = new GSTPContext();
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        private const decimal ROOT = 0;
        DataTable lstDonVi;
        DataTable tbl_temp = new DataTable();
        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            { return false; }
        }
        public string GetDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return Convert.ToDateTime(obj).ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                    Cls_Comon.SetButton(cmdUpdateAndNew, oPer.CAPNHAT);
                    Cls_Comon.SetButton(cmdUpdateB, oPer.CAPNHAT);
                    Cls_Comon.SetButton(cmdUpdateAndNewB, oPer.CAPNHAT);
                    LoadCombobox();

                    //Bỏ vì không có tác dụng - ng yêu cầu đ/c Mạnh
                    //chkIsCongVan.Visible = true;
                    string current_id = Request["ID"] + "";
                    GDTTT_DON_BL oBL = new GDTTT_DON_BL();

                    txtNgaynhandon.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    if (current_id != "" && current_id != "0")
                    {
                        decimal ID = Convert.ToDecimal(current_id);
                        string strType = Request["type"] + "";
                        if (strType == "dontrung")
                        {
                            pnDonTrung.Visible = false;
                            LoadInfo(ID, true);
                            txtSohieudon.Text = txtNgaynhandon.Text = txtNgayghidon.Text = "";
                            Cls_Comon.SetButton(cmdUpdate, true);
                            Cls_Comon.SetButton(cmdUpdateAndNew, true);
                            Cls_Comon.SetButton(cmdUpdateB, true);
                            Cls_Comon.SetButton(cmdUpdateAndNewB, true);
                            //Thêm số thụ lý, ngày cho đơn trùng
                        }
                        else
                        {
                            pnDonTrung.Visible = false;
                            hddID.Value = current_id.ToString();
                            LoadInfo(ID, false);
                            LoadBoSungTL(ID);
                            //Load danh sách trùng
                            LoadDSTrung();
                            pnKiemtraTrung.Visible = false;
                        }
                        if (Request["vt_id"] + "" != "")//anhvh add dùng cho văn thư đến 22/10/2020
                        {
                            txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
                            txtNgaythuly.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                        //---------------------------
                    }
                    else
                    {

                        pnDonTrung.Visible = true;
                        //Bỏ vì không có tác dụng - ng yêu cầu đ/c Mạnh
                        //chkIsCongVan.Visible = true;
                        txtSoCMND.Text = Session[SS_TK.SOCMND] + "";
                        txtNguoigui.Text = Session[SS_TK.NGUOIGUI] + "";
                        txtSoQDBA.Text = Session[SS_TK.SOBAQD] + "";
                        txtNgayBA.Text = Session[SS_TK.NGAYBAQD] + "";
                        try
                        {
                            if ((Session[SS_TK.HUYENID] + "") != "" && (Session[SS_TK.HUYENID] + "") != "0")
                            {
                                decimal IDHuyen = Convert.ToDecimal(Session[SS_TK.HUYENID]);
                                DM_HANHCHINH oHC = dt.DM_HANHCHINH.Where(x => x.ID == IDHuyen).FirstOrDefault();
                                hddNGDCID.Value = oHC.ID.ToString();
                                txtNGHuyen.Text = oHC.MA_TEN;
                                txtDiachi.Text = Session[SS_TK.DIACHICHITIET] + "";
                            }
                            if (Session[SS_TK.TOAANXX] != null) ddlToaXetXu.SelectedValue = Session[SS_TK.TOAANXX] + "";
                            txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
                            txtNgaythuly.Text = DateTime.Now.ToString("dd/MM/yyyy");
                            LoadIsTuHinh(ddlLoaiAn.SelectedValue);
                            
                           
                        }
                        catch(Exception ex) { }
                    }
                    if (ddlCapXetXu.SelectedValue == "3")
                    {
                        pnPhucTham.Visible = false;
                        pnAnST.Visible = true;
                    }

                }
            }
            catch (Exception ex) { lstMsgB.Text = lstMsgT.Text = ex.Message; }

        }
        //-----kiem tra an thoi hieu-----------
        private void Canhbao_thoihieu()
        {
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            decimal capxx = Convert.ToDecimal(ddlCapXetXu.SelectedValue);
            decimal loaian = Convert.ToDecimal(ddlLoaiAn.SelectedValue);
            DateTime ngayBA;
            DateTime ngayThuly;
            TimeSpan t;
            String strMsg;
            if(txtNgaythuly.Text !="" )
                ngayThuly = DateTime.Parse(txtNgaythuly.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            else
                ngayThuly = DateTime.Today;
            if (txtNgayBA.Text != "")
                ngayBA = DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            else
                ngayBA = DateTime.Today; 

            t = ngayThuly - ngayBA;
            
            if (capxx == 2)
            {
                if (loaian == 1)
                {
                    if ((1 * 365 - t.TotalDays) <= 60 && (1 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (1 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }else if ((1 * 365 - t.TotalDays) <= 0)
                    {
                        strMsg = "Đã hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                }
                //an hanh chinh
                else if (loaian == 6)
                {
                    if ((3 * 365 - t.TotalDays) <= 30 && (3 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (3 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                    else if ((3 * 365 - t.TotalDays) <= 0)
                    {
                        strMsg = "Đã hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                }
                else {

                    if ((3 * 365 - t.TotalDays) < 30 && (3 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (3 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết"; 
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                    else
                    {
                        if ((5 * 365 - t.TotalDays) < 30 && (5 * 365 - t.TotalDays) > 0)
                        {
                            strMsg = "Còn "+ (5 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        }
                        else if((5 * 365 - t.TotalDays) <= 0)
                        {
                            strMsg = "Đã hết thời hiệu giải quyết";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        }
                        
                    }
                }
            }
            else
            {

                if (loaian == 1)
                {
                    if ((1 * 365 - t.TotalDays) <= 60 && (1 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (1 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                    else if ((1 * 365 - t.TotalDays) <=0 )
                    {
                        strMsg = "Đã hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                }
                //an hanh chinh
                else if (loaian == 6)
                {
                    if ((3 * 365 - t.TotalDays) <= 30 && (3 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (3 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                    else if ((3 * 365 - t.TotalDays) <= 0)
                    {
                        strMsg = "Đã hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                }
                else
                {

                    if ((3 * 365 - t.TotalDays) <= 30 && (3 * 365 - t.TotalDays) > 0)
                    {
                        strMsg = "Còn " + (3 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                    }
                    else
                    {
                        if ((5 * 365 - t.TotalDays) <= 30 && (5 * 365 - t.TotalDays) > 0)
                        {
                            strMsg = "Còn " + (5 * 365 - t.TotalDays) + " ngày nữa là hết thời hiệu giải quyết";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        }
                        else if ((5 * 365 - t.TotalDays) <= 0)
                        {
                            strMsg = "Đã hết thời hiệu giải quyết";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        }

                    }
                }
            }
  
        }
       
        private void LoadInfo(decimal ID, bool isLoadTrung)
        {
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();

            GDTTT_DON oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
            if (oT != null)
            {
                ddl_LOAI_GDTTT.SelectedValue = Convert.ToString(oT.LOAI_GDTTTT);
                if (oT.BAQD_CAPXETXU == 2)
                    pnPhucTham.Visible = pnAnST.Visible = false;
                else if (oT.BAQD_CAPXETXU == 3)
                {
                    pnPhucTham.Visible =  false;
                    pnAnST.Visible = true;
                }else
                {
                    pnPhucTham.Visible = pnAnST.Visible = true;
                }

                if (oT.CD_TRANGTHAI > 0 && oT.CD_TRANGTHAI != 3 && isLoadTrung == false)
                {
                    Cls_Comon.SetButton(cmdUpdate, false);
                    Cls_Comon.SetButton(cmdUpdateAndNew, false);
                    Cls_Comon.SetButton(cmdUpdateB, false);
                    Cls_Comon.SetButton(cmdUpdateAndNewB, false);
                }
                if (oT.CD_LOAI != null)
                {
                    rdbLoaichuyen.SelectedValue = oT.CD_LOAI.ToString();
                    setLoaichuyen();
                    switch (rdbLoaichuyen.SelectedValue)
                    {
                        case "0"://Nội bộ tòa án                            
                            if (oT.CD_TA_DONVIID != null)
                                ddlChuyendenDV.SelectedValue = oT.CD_TA_DONVIID.ToString();
                            ChuyendenDVChange();
                            if (pnBAQDGDT.Visible)
                            {
                                if (oT.CD_TA_TRANGTHAI != null)
                                    ddlTrangthaidon.SelectedValue = oT.CD_TA_TRANGTHAI.ToString();
                                LoadLoaiAn();
                                try
                                {
                                    if (oT.BAQD_LOAIAN != null)
                                        ddlLoaiAn.SelectedValue = "0" + oT.BAQD_LOAIAN.ToString();
                                    LoadIsTuHinh(ddlLoaiAn.SelectedValue);
                                    if (trTuHinh.Visible)
                                    {
                                        chkIsTuHinh.Checked = oT.ISANTUHINH == 1 ? true : false;
                                        if (chkIsTuHinh.Checked)
                                        {
                                            chkIsAnGiam.Visible = chkKeuOan.Visible = true;
                                            chkIsAnGiam.Checked = oT.ISTH_ANGIAM == 1 ? true : false;
                                            chkKeuOan.Checked = oT.ISTH_KEUOAN == 1 ? true : false;
                                        }
                                    }
                                }
                                catch (Exception ex) { }
                                if (ddlTrangthaidon.SelectedValue == "1")
                                {
                                    lblLydoCDDK.Text = "Lý do";
                                    chkLydoCDDK.Visible = true;
                                    pnThuLy.Visible = false;
                                    rdbThuLy.Visible = false;
                                    chkLydoCDDK.Items[0].Selected = oT.CD_TA_LYDO_ISBAQD == 1 ? true : false;
                                    chkLydoCDDK.Items[1].Selected = oT.CD_TA_LYDO_ISXACNHAN == 1 ? true : false;
                                    chkLydoCDDK.Items[2].Selected = oT.CD_TA_LYDO_ISKHAC == 1 ? true : false;
                                    if (oT.CD_TA_LYDO_ISKHAC == 1)
                                    {
                                        trLydokhac.Visible = true;
                                        txtLydoCDDK_Khac.Text = oT.CD_TA_LYDO_KHAC + "";
                                    }
                                }
                                else
                                {
                                    lblLydoCDDK.Text = "Thụ lý đơn";
                                    chkLydoCDDK.Visible = false;
                                    rdbThuLy.Visible = true;
                                    if (oT.ISTHULY != null) rdbThuLy.SelectedValue = oT.ISTHULY + "";
                                    pnThuLy.Visible = true;
                                    txtSoThuLy.Text = oT.TL_SO + "";
                                    txtNgaythuly.Text = oT.TL_NGAY + "" == "" ? "" : ((DateTime)oT.TL_NGAY).ToString("dd/MM/yyyy");
                                    if (isLoadTrung)
                                    {
                                        rdbThuLy.SelectedValue = "2";
                                        pnThuLy.Visible = false;
                                    }
                                    else
                                    {
                                        if (rdbThuLy.SelectedValue == "2") pnThuLy.Visible = false;
                                    }
                                }
                            }
                            break;
                        case "1":
                            if (oT.CD_TK_DONVIID != null)
                            {
                                ddlToaKhac.SelectedValue = oT.CD_TK_DONVIID + "";

                            }
                            if (oT.CD_TK_NOIGUI != null)
                                rdbDonguitoi.SelectedValue = oT.CD_TK_NOIGUI.ToString();
                            break;
                        case "2":
                            txtNgoaitoaan.Text = oT.CD_NTA_TENDONVI;
                            break;
                        case "3":
                            if (oT.CD_TRALAI_LYDOID != null && oT.CD_TRALAI_LYDOID != 0)
                            {
                                ddlLydotralai.SelectedValue = oT.CD_TRALAI_LYDOID.ToString();
                            }
                            else
                            {
                                ddlLydotralai.SelectedValue = "0";
                                txtTralaikhac.Text = oT.CD_TRALAI_LYDOKHAC;
                                lblLydokhac.Visible = true;
                                txtTralaikhac.Visible = true;
                            }
                            txtTralaiYeucau.Text = oT.CD_TRALAI_YEUCAU;
                            txtTralaiSophieu.Text = oT.CD_TRALAI_SOPHIEU;
                            txtTralaiNgay.Text = oT.CD_TRALAI_NGAYTRA + "" == "" ? "" : ((DateTime)oT.CD_TRALAI_NGAYTRA).ToString("dd/MM/yyyy");
                            break;
                        default:
                            break;
                    }
                }
                if (pnBAQDGDT.Visible)
                {
                    if (oT.BAQD_LOAIQDBA != 1)
                    {
                        rdbBAQD.SelectedValue = "0";
                        pnSoKhangNghi.Visible = false;
                        pnSoBA.Visible = true;
                        if (oT.BAQD_CAPXETXU == 4)
                        {
                            txtSoQDBA.Text = oT.BAQD_SO;
                            txtNgayBA.Text = oT.BAQD_NGAYBA + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA).ToString("dd/MM/yyyy");
                            //if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID + "") != null)
                            //    ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID + "";
                            if (oT.BAQD_TOAANID > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID);
                                LoadDropToaPT_TheoGDT(Convert.ToDecimal(oT.BAQD_TOAANID));
                            }
                            

                            txtSoBA_PT.Text = oT.BAQD_SO_PT;
                            txtNgayBA_PT.Text = oT.BAQD_NGAYBA_PT + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_PT).ToString("dd/MM/yyyy");
                            //if (dropToaAnPT.Items.FindByValue(oT.BAQD_TOAANID_PT + "") != null)
                            //    dropToaAnPT.SelectedValue = oT.BAQD_TOAANID_PT + "";
                            if (oT.BAQD_TOAANID_PT > 0)
                            {
                                Cls_Comon.SetValueComboBox(dropToaAnPT, oT.BAQD_TOAANID_PT);
                                LoadDropToaST_TheoPT(Convert.ToDecimal(oT.BAQD_TOAANID_PT));
                            }
                            
                            txtSoBA_ST.Text = oT.BAQD_SO_ST;
                            txtNgayBA_ST.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                          
                            if (oT.BAQD_TOAANID_ST > 0)
                                Cls_Comon.SetValueComboBox(dropToaAnST, oT.BAQD_TOAANID_ST);
                        }
                        else if (oT.BAQD_CAPXETXU == 3)
                        {
                            txtSoQDBA.Text = oT.BAQD_SO_PT;
                            txtNgayBA.Text = oT.BAQD_NGAYBA_PT + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_PT).ToString("dd/MM/yyyy");
                            //if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID_PT + "") != null)
                            //    ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID_PT + "";
                            if (oT.BAQD_TOAANID_PT > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID_PT);
                                LoadDropToaST_TheoPT(Convert.ToDecimal(oT.BAQD_TOAANID_PT));
                            }

                            txtSoBA_ST.Text = oT.BAQD_SO_ST;
                            txtNgayBA_ST.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID_ST > 0)
                                Cls_Comon.SetValueComboBox(dropToaAnST, oT.BAQD_TOAANID_ST);
                        }
                        else if (oT.BAQD_CAPXETXU == 2)
                        {   txtSoQDBA.Text = oT.BAQD_SO_ST;
                            txtNgayBA.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                            if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID_ST + "") != null)
                                ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID_ST + "";
                        }else
                        {
                            txtSoQDBA.Text = oT.BAQD_SO;
                            txtNgayBA.Text = oT.BAQD_NGAYBA + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA).ToString("dd/MM/yyyy");
                            //if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID + "") != null)
                            //    ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID + "";
                            if (oT.BAQD_TOAANID > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID);
                                LoadDropToaPT_TheoGDT(Convert.ToDecimal(oT.BAQD_TOAANID));
                            }
                        }

                    }
                    else
                    {
                        rdbBAQD.SelectedValue = "1";
                        pnSoKhangNghi.Visible = true;
                        pnSoBA.Visible = true;
                        txtSoQDKN.Text = oT.KN_SOQD;
                        txtNgayKhangNghi.Text = oT.KN_NGAY + "" == "" ? "" : ((DateTime)oT.KN_NGAY).ToString("dd/MM/yyyy");
                        if (oT.NGUOIKHANGNGHI != null)
                            ddlNguoiKhangNghi.SelectedValue = oT.NGUOIKHANGNGHI + "";

                        if (oT.BAQD_CAPXETXU == 4)
                        {
                            txtSoQDBA.Text = oT.BAQD_SO;
                            txtNgayBA.Text = oT.BAQD_NGAYBA + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID);
                                LoadDropToaPT_TheoGDT(Convert.ToDecimal(oT.BAQD_TOAANID));
                            }

                            txtSoBA_PT.Text = oT.BAQD_SO_PT;
                            txtNgayBA_PT.Text = oT.BAQD_NGAYBA_PT + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_PT).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID_PT > 0)
                            {
                                Cls_Comon.SetValueComboBox(dropToaAnPT, oT.BAQD_TOAANID_PT);
                                LoadDropToaST_TheoPT(Convert.ToDecimal(oT.BAQD_TOAANID_PT));
                            }

                            txtSoBA_ST.Text = oT.BAQD_SO_ST;
                            txtNgayBA_ST.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID_ST > 0)
                                Cls_Comon.SetValueComboBox(dropToaAnST, oT.BAQD_TOAANID_ST);
                        }
                        else if (oT.BAQD_CAPXETXU == 3)
                        {
                            txtSoQDBA.Text = oT.BAQD_SO_PT;
                            txtNgayBA.Text = oT.BAQD_NGAYBA_PT + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_PT).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID_PT > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID_PT);
                                LoadDropToaST_TheoPT(Convert.ToDecimal(oT.BAQD_TOAANID_PT));
                            }

                            txtSoBA_ST.Text = oT.BAQD_SO_ST;
                            txtNgayBA_ST.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                            if (oT.BAQD_TOAANID_ST > 0)
                                Cls_Comon.SetValueComboBox(dropToaAnST, oT.BAQD_TOAANID_ST);
                        }
                        else if (oT.BAQD_CAPXETXU == 2)
                        {
                            txtSoQDBA.Text = oT.BAQD_SO_ST;
                            txtNgayBA.Text = oT.BAQD_NGAYBA_ST + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA_ST).ToString("dd/MM/yyyy");
                            if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID_ST + "") != null)
                                ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID_ST + "";
                        }else
                        {
                            txtSoQDBA.Text = oT.BAQD_SO;
                            txtNgayBA.Text = oT.BAQD_NGAYBA + "" == "" ? "" : ((DateTime)oT.BAQD_NGAYBA).ToString("dd/MM/yyyy");
                            //if (ddlToaXetXu.Items.FindByValue(oT.BAQD_TOAANID + "") != null)
                            //    ddlToaXetXu.SelectedValue = oT.BAQD_TOAANID + "";
                            if (oT.BAQD_TOAANID > 0)
                            {
                                Cls_Comon.SetValueComboBox(ddlToaXetXu, oT.BAQD_TOAANID);
                                LoadDropToaPT_TheoGDT(Convert.ToDecimal(oT.BAQD_TOAANID));
                            }
                        }

                    }
                    if (oT.KN_TRALOIDON != null)
                        chkKN_TBTLD.Checked = oT.KN_TRALOIDON == 1 ? true : false;
                    if (chkKN_TBTLD.Checked)
                    {
                        trThongbaoTLD.Visible = true;

                    }
                    //if (ddlToaXetXu.SelectedValue == "0")
                    //{
                    //    lblTentoaXX.Text = oT.BAQD_TENTOA + "";
                    //    trTenToaXX.Visible = true;
                    //}

                    if (ddlCapXetXu.Items.FindByValue(oT.BAQD_CAPXETXU + "") != null)
                        ddlCapXetXu.SelectedValue = oT.BAQD_CAPXETXU + "";
                    else
                        ddlCapXetXu.SelectedValue = "0";

                }
                else
                {
                    string strPhanloai = oT.ARRPHANLOAI + "";
                    foreach (ListItem i in chkPhanloai.Items)
                    {
                        if (("," + strPhanloai + ",").Contains("," + i.Value + ","))
                            i.Selected = true;
                    }
                    txtDoituongbiKN.Text = oT.DOITUONGBIKNTC;
                    rdbLoaiKNTC.SelectedValue = oT.LOAIKNTC + "";
                }
                if (ddlTraloi.Items.FindByValue(oT.TRALOIDON + "") != null)
                    ddlTraloi.SelectedValue = oT.TRALOIDON + "";

                if (oT.TRALOIDON == 1)
                {
                    if (ddlTraloi.SelectedValue == "1")
                        pnCV_traloi.Visible = true;
                    else
                        pnCV_traloi.Visible = false;
                    txtCV_Traloi_Noidung.Text = oT.CV_TRALOI_NOIDUNG;
                }
                else
                {
                    txtCV_Traloi_Noidung.Text = "";
                }
                txtMaDon.Text = oT.MADON;
                txtNguoigui.Text = oT.NGUOIGUI_HOTEN;
                txtSoCMND.Text = oT.NGUOIGUI_CMND;
                if (oT.NGUOIGUI_GIOITINH != null)
                    ddlGioitinh.SelectedValue = oT.NGUOIGUI_GIOITINH.ToString();
                txtNgaynhandon.Text = oT.NGAYNHANDON == null ? "" : ((DateTime)oT.NGAYNHANDON).ToString("dd/MM/yyyy");
                txtNgayghidon.Text = oT.NGAYGHITRENDON == null ? "" : ((DateTime)oT.NGAYGHITRENDON).ToString("dd/MM/yyyy");
                txtSohieudon.Text = oT.SOHIEUDON;
                if (oT.LOAIDON != null)
                    ddlHinhthucdon.SelectedValue = oT.LOAIDON.ToString();
                if (oT.DUNGDONLA != null)
                    ddlDungdonla.SelectedValue = oT.DUNGDONLA.ToString();
                if (oT.NGUOIGUI_TUCACHTOTUNG != null)
                    ddlTucachTT.SelectedValue = oT.NGUOIGUI_TUCACHTOTUNG.ToString();

                if (oT.NGUOIGUI_HUYENID != null)
                {
                    try
                    {
                        DM_HANHCHINH oDCHuyen = dt.DM_HANHCHINH.Where(x => x.ID == oT.NGUOIGUI_HUYENID).FirstOrDefault();
                        hddNGDCID.Value = oDCHuyen.ID.ToString();
                        txtNGHuyen.Text = oDCHuyen.MA_TEN;
                    }
                    catch (Exception ex) { }
                }
                txtDiachi.Text = oT.NGUOIGUI_DIACHI;
                txtNoidung.Text = oT.NOIDUNGDON;
                txtGhichu.Text = oT.GHICHU + "";
                if (oT.ISNOTGDTTT == 1)
                {
                    chkIsNotGDT.Checked = true;
                    chkIsNotGDTChange();

                }
                if (oT.ISGDTTT_KNTC == 1) chkGDTIsKNTC.Checked = true;
                lblTucach.Visible = ddlTucachTT.Visible = true;
                if (ddlHinhthucdon.SelectedValue == "3" || ddlHinhthucdon.SelectedValue == "2")// Loại đơn + công văn
                {
                    if (ddlLoaiCongVan.Items.FindByValue(oT.LOAICONGVAN + "") != null)
                        ddlLoaiCongVan.SelectedValue = oT.LOAICONGVAN + "";
                    ChangeLoaiCV();

                    decimal IDLoai = Convert.ToDecimal(ddlLoaiCongVan.SelectedValue);
                    DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();

                    if (oLoai.MA == ENUM_GDT_LOAICV.NHACLAI)// công văn nhắc lại
                    {
                        pnCongVanNhacLai.Visible = true;
                        txtCongVan.Text = oT.CV_NHACLAITEXT;
                        hddNhaclaiCVID.Value = oT.CV_NHACLAIID == null ? "0" : oT.CV_NHACLAIID.ToString();
                    }
                    else
                    {
                        pnCongVanNhacLai.Visible = false;
                    }
                    chkIsTrongNganh.Checked = oT.CV_ISTRONGNGANH == 1 ? true : false;
                    if (chkIsTrongNganh.Checked)
                    {
                        pnDonViGuiTrongNganh.Visible = true;
                        pnDonViGuiNgoaiNganh.Visible = false;
                        ddlCV_Donvi.SelectedValue = oT.CV_TOAANID + "";
                        chkTraigiam.Visible = false;
                    }
                    else
                    {
                        chkTraigiam.Checked = oT.CV_ISTRAIGIAM == 1 ? true : false;
                        pnDonViGuiTrongNganh.Visible = false;
                        pnDonViGuiNgoaiNganh.Visible = true;
                        txtCV_DonViGuiNgoaiNganh.Text = oT.CV_TENDONVI;
                        chkTraigiam.Visible = true;
                        if (chkTraigiam.Checked) lblTraigiamhientai.Visible = txtTraigiamhientai.Visible = true;

                    }
                    txtTraigiamhientai.Text = oT.CV_TRAIGIAMHIENTAI + "";
                    if (oT.CV_HUYENID != null)
                    {
                        try
                        {
                            DM_HANHCHINH oCVHuyen = dt.DM_HANHCHINH.Where(x => x.ID == oT.CV_HUYENID).FirstOrDefault();
                            hddCVDCID.Value = oCVHuyen.ID.ToString();
                            txtCVDC.Text = oCVHuyen.MA_TEN;
                        }
                        catch (Exception ex) { }
                    }
                    txtCVDiachi.Text = oT.CV_DIACHI + "";
                    txtCV_So.Text = oT.CV_SO;
                    txtCV_Ngay.Text = oT.CV_NGAY + "" == "" ? "" : ((DateTime)oT.CV_NGAY).ToString("dd/MM/yyyy");
                    txtCV_Nguoiky.Text = oT.CV_NGUOIKY;
                    txtCV_Chucvu.Text = oT.CV_CHUCVU;
                    pnCongvan.Visible = true;


                }

                ddlPhanloai.SelectedValue = oT.PHANLOAIXULY.ToString();
                if (ddlPhanloai.SelectedValue == "04")
                {
                    ddlTrangthaidon.SelectedValue = "1";
                    lblLydoCDDK.Text = "Lý do";
                    chkLydoCDDK.Visible = true;
                }
                ddlHinhthucdonChange();
                if (ddlPhanloai.SelectedValue == "2" || ddlPhanloai.SelectedValue == "3")//Đơn trùng hoặc đơn khiếu nại
                {
                    txtSoLuongDon.Text = oT.SOLUONGDON + "";
                }

                if (isLoadTrung)
                {
                    ddlPhanloai.SelectedValue = "3";
                    hddDontrungID.Value = oT.ID.ToString();
                    txtTenDonTrung.Text = "Số hiệu: " + oT.SOHIEUDON + ";Người gửi: " + oT.NGUOIGUI_HOTEN + "";
                    txtTenDonTrung.Enabled = false;
                    cmdCheckTrungDon.Visible = false;
                    cmdHuyTrungDon.Visible = true;
                }
                if (oT.CV_YEUCAUTHONGBAO != null) ddlYeucauthongbao.SelectedValue = oT.CV_YEUCAUTHONGBAO.ToString();

                if (oT.CHIDAO_COKHONG != null)
                {
                    if (oT.CHIDAO_COKHONG == 1)
                    {

                        trChidao.Visible = true;
                        if (oT.CHIDAO_LANHDAOID != null)
                            ddlCAChidao.SelectedValue = oT.CHIDAO_LANHDAOID + "";
                        txtCA_Noidung.Text = oT.CHIDAO_NOIDUNG + "";

                    }
                    else
                        ddlCAChidao.SelectedValue = "0";
                }
                divCommandT.Visible = divCommandB.Visible = true;
                List<GDTTT_DON_NGUOIKN> lstKN = dt.GDTTT_DON_NGUOIKN.Where(x => x.DONID == oT.ID).OrderBy(y => y.HOTEN).ToList();
                ddlSonguoiKN.SelectedValue = lstKN.Count.ToString();
                dgNguoiKN.DataSource = lstKN;
                dgNguoiKN.DataBind();
                if (chkKN_TBTLD.Checked)
                {
                    LoadTBTLD(oT.ID);
                }
                //Lưu session
                if (Request["vt_id"] + "" != "")
                {
                    VT_VANTHU_DEN_BL obj_M = new VT_VANTHU_DEN_BL();
                    DataTable tbl = obj_M.VT_VANBANDEN_LOAD_BYID(Session[ENUM_SESSION.SESSION_DONVIID] + "",Request["vt_id"] + "");
                    ddlChuyendenDV.SelectedValue = tbl.Rows[0]["CD_TA_DONVIID"] + "";
                    LoadLoaiAn();
                    ddlLoaiAn.SelectedValue = "0"+tbl.Rows[0]["LOAI_AN_DON"] + "";
                    txtNgaynhandon.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    rdbThuLy.SelectedValue = "1";
                }

            }
           
        }

        //------------Manhnd them thong tin BA PT, ST----------------------------
        protected void ddlCapXetXu_SelectedIndexChanged(object sender, EventArgs e)
        {
            //----Chech an thoi hieu------------
            Canhbao_thoihieu();
            //---------------
            if (ddlCapXetXu.SelectedValue == "3")
            {
                pnPhucTham.Visible = false;
                pnAnST.Visible = true;
                LoadDropToaST_TheoPT(Convert.ToDecimal(ddlToaXetXu.SelectedValue));
                
            }
            else if (ddlCapXetXu.SelectedValue == "2")
            {
                pnPhucTham.Visible = false;
                pnAnST.Visible = false;
                
               
            }
            else if (ddlCapXetXu.SelectedValue == "4")
            {
                pnPhucTham.Visible = true;
                pnAnST.Visible = true;
                LoadDropToaST_Full_ST();
            }
           
        }
        protected void dropToaAnPT_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnAnST.Visible = false;
            decimal toa_an_id = Convert.ToDecimal(dropToaAnPT.SelectedValue);
            if (toa_an_id > 0)
            {
                DM_TOAAN obj = dt.DM_TOAAN.Where(x => x.ID == toa_an_id).Single();
                if (obj != null)//&& (obj.LOAITOA == "CAPCAO" || obj.LOAITOA == "CAPTINH"))
                {
                    pnAnST.Visible = true;
                    Cls_Comon.SetFocus(this, this.GetType(), txtSoBA_ST.ClientID);
                    LoadDropToaST_TheoPT(toa_an_id);
                }
                else { dropToaAnST.Items.Clear(); }
                string banan_pt = txtSoBA_PT.Text.Trim();
                if (!string.IsNullOrEmpty(banan_pt)) Cls_Comon.SetFocus(this, this.GetType(), txtSoBA_PT.ClientID);

            }
            else
            {
                pnAnST.Visible = false;
                Cls_Comon.SetFocus(this, this.GetType(), dropToaAnPT.ClientID);
                dropToaAnST.Items.Clear();
            }

        }
        void LoadDropToaPT_TheoGDT(decimal ToaGDT_ID)
        {
            dropToaAnPT.Items.Clear();
            DM_TOAAN_BL bl = new DM_TOAAN_BL();
            //Decimal CapChaID = Convert.ToDecimal(dropToaGDT.SelectedValue);
            DataTable tbl = bl.GetByCapChaID(ToaGDT_ID);
            dropToaAnPT.DataSource = tbl;
            dropToaAnPT.DataTextField = "MA_TEN";
            dropToaAnPT.DataValueField = "ID";
            dropToaAnPT.DataBind();
            dropToaAnPT.Items.Insert(0, new ListItem("Chọn", "0"));
        }
        void LoadDropToaST_TheoPT(decimal ToaPT_ID)
        {
            dropToaAnST.Items.Clear();
            DM_TOAAN_BL bl = new DM_TOAAN_BL();
            //Decimal CapChaID = Convert.ToDecimal(dropToaAn.SelectedValue);
            DataTable tbl = bl.GetByCapChaID(ToaPT_ID);
            dropToaAnST.DataSource = tbl;
            dropToaAnST.DataTextField = "MA_TEN";
            dropToaAnST.DataValueField = "ID";
            dropToaAnST.DataBind();
            dropToaAnST.Items.Insert(0, new ListItem("Chọn", "0"));
        }
        void LoadDropToaST_Full_ST()
        {
            dropToaAnST.Items.Clear();
            DM_TOAAN_BL oTABL = new DM_TOAAN_BL();
            DataTable dtTA = oTABL.DM_TOAAN_GETBYNOTCUR_ST();
            dropToaAnST.DataSource = dtTA;
            dropToaAnST.DataTextField = "MA_TEN";
            dropToaAnST.DataValueField = "ID";
            dropToaAnST.DataBind();
            dropToaAnST.Items.Insert(0, new ListItem("Chọn", "0"));
        }
        //----------------------------------------
        private void LoadTBTLD(decimal DonID)
        {
            List<GDTTT_DON_TBTLD> lstTBTLD = dt.GDTTT_DON_TBTLD.Where(x => x.DONID == DonID).OrderBy(y => y.NGAY).ToList();
            if (ddlSoTBTLD.Items.FindByValue(lstTBTLD.Count.ToString()) != null)
                ddlSoTBTLD.SelectedValue = lstTBTLD.Count.ToString();
            dgTBTLD.Columns[4].Visible = true;
            dgTBTLD.DataSource = lstTBTLD;
            dgTBTLD.DataBind();
            trThongbaoTLD.Visible = true;
            if (lstTBTLD.Count == 0)
            {
                chkKN_TBTLD.Checked = false;
                trThongbaoTLD.Visible = false;
            }
        }
        private void LoadBoSungTL(decimal DonID)
        {
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            dgDS.DataSource = oBL.BOSUNGTAILIEU(DonID);
            dgDS.DataBind();

            if (dgDS.Items.Count > 0)
                pnBoSung.Visible = true;
            else
                pnBoSung.Visible = false;
        }
        private void LoadDSTrung()
        {
            decimal DonID = Convert.ToDecimal(hddID.Value);
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            DataTable tbl = oBL.DANHSACHDONTRUNG(DonID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                #region "Xác định số lượng trang"
                int total = tbl.Rows.Count;
                hddTotalPageDT.Value = Cls_Comon.GetTotalPage(total, Convert.ToInt32(dgDSDonTrung.PageSize)).ToString();
                lstSobanghiTDT.Text = lstSobanghiBDT.Text = "Có <b>" + total + " </b> đơn!";
                Cls_Comon.SetPageButton(hddTotalPageDT, hddPageIndexDT, lbTFirstDT, lbBFirstDT, lbTLastDT, lbBLastDT, lbTNextDT, lbBNextDT, lbTBackDT, lbBBackDT, lbTStep1DT, lbBStep1DT, lbTStep2DT,
                             lbBStep2DT, lbTStep3DT, lbBStep3DT, lbTStep4DT, lbBStep4DT, lbTStep5DT, lbBStep5DT, lbTStep6DT, lbBStep6DT);
                #endregion
                dgDSDonTrung.DataSource = tbl;
                dgDSDonTrung.DataBind();
                pnDSTrung.Visible = true;
            }
            else
            {
                pnDSTrung.Visible = false;
            }
        }
        private bool CheckValid()
        {
            lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
            if (rdbLoaichuyen.SelectedValue == "1" && ddlToaKhac.SelectedValue == "0")
            {
                lstMsgT.Text = lstMsgB.Text = "Chưa chọn tòa án để chuyển đơn !";
                ddlToaKhac.Focus();
                return false;
            }
            else if (rdbLoaichuyen.SelectedValue == "2" && txtNgoaitoaan.Text == "")
            {
                lstMsgT.Text = lstMsgB.Text = "Chưa nhập đơn vị ngoài tòa án !";
                txtNgoaitoaan.Focus();
                return false;
            }
            if (pnBAQDGDT.Visible)
            {
                if (rdbBAQD.SelectedValue == "0")
                {
                    if (ddlTrangthaidon.SelectedValue != "1")
                    {
                        if (txtSoQDBA.Text == "" && spSOBA.Visible && chkIsNotGDT.Checked == false)
                        {
                            lstMsgT.Text = lstMsgB.Text = "Chưa nhập số bản án. Hãy nhập lại!";
                            txtSoQDBA.Focus();
                            return false;
                        }
                        if ((txtNgayBA.Text == "" || Cls_Comon.IsValidDate(txtNgayBA.Text) == false) && spNGBA.Visible && chkIsNotGDT.Checked == false)
                        {
                            lstMsgT.Text = lstMsgB.Text = "Ngày bản án chưa nhập hoặc không hợp lệ. Hãy nhập lại!";
                            txtSoQDBA.Focus();
                            return false;
                        }
                    }
                }
                else
                {
                    if (txtSoQDKN.Text == "" && spSOKN.Visible && chkIsNotGDT.Checked == false)
                    {
                        lstMsgT.Text = lstMsgB.Text = "Chưa nhập Số QĐ kháng nghị. Hãy nhập lại!";
                        txtSoQDKN.Focus();
                        return false;
                    }
                    if ((txtNgayKhangNghi.Text == "" || Cls_Comon.IsValidDate(txtNgayKhangNghi.Text) == false) && spNGKN.Visible && chkIsNotGDT.Checked == false)
                    {
                        lstMsgT.Text = lstMsgB.Text = "Ngày quyết định chưa nhập hoặc không hợp lệ. Hãy nhập lại!";
                        txtNgayKhangNghi.Focus();
                        return false;
                    }
                }
                if (ddlToaXetXu.SelectedValue == "0" && spTOAXX.Visible && chkIsNotGDT.Checked == false)
                {
                    lstMsgT.Text = lstMsgB.Text = "Chưa chọn tòa án xét xử !";
                    ddlToaXetXu.Focus();
                    return false;
                }

                if (pnThuLy.Visible)
                {
                    //manh bo bát để duy nhập dữ liệu
                    //if (txtSoThuLy.Text == "")
                    //{
                    //    lstMsgT.Text = lstMsgB.Text = "Chưa nhập số thụ lý!";
                    //    txtSoThuLy.Focus();
                    //    return false;
                    //}
                    if (txtNgaythuly.Text == "" || Cls_Comon.IsValidDate(txtNgaythuly.Text) == false)
                    {
                        lstMsgT.Text = lstMsgB.Text = "Ngày thụ lý chưa nhập hoặc không hợp lệ. Hãy nhập lại!";
                        txtNgaythuly.Focus();
                        return false;
                    }
                }
            }
            else
            {
                if (txtDoituongbiKN.Text == "")
                {
                    lstMsgT.Text = lstMsgB.Text = "Chưa nhập đối tượng bị khiếu nại, tố cáo!";
                    txtDoituongbiKN.Focus();
                    return false;
                }
            }

            if (txtNgaynhandon.Text.Trim() == "")
            {
                lstMsgT.Text = lstMsgB.Text = "Bạn chưa nhập ngày nhận. Hãy nhập lại!";
                txtNgaynhandon.Focus();
                return false;
            }
            else if (Cls_Comon.IsValidDate(txtNgaynhandon.Text) == false)
            {
                lstMsgT.Text = lstMsgB.Text = "Bạn phải nhập ngày nhận theo định dạng (dd/MM/yyyy). Hãy nhập lại!";
                txtNgaynhandon.Focus();
                return false;
            }
            //Đơn hoặc Đơn + Công văn
            if (ddlHinhthucdon.SelectedValue != "2")
            {
                //Manh bỏ cho duy nhap
                //if (txtNgayghidon.Text.Trim() == "")
                //{
                //    lstMsgT.Text = lstMsgB.Text = "Bạn chưa nhập ngày ghi trên đơn. Hãy nhập lại!";
                //    txtNgayghidon.Focus();
                //    return false;
                //}
                //else if (Cls_Comon.IsValidDate(txtNgayghidon.Text) == false)
                //{
                //    lstMsgT.Text = lstMsgB.Text = "Bạn phải nhập ngày ghi trên đơn theo định dạng (dd/MM/yyyy). Hãy nhập lại!";
                //    txtNgayghidon.Focus();
                //    return false;
                //}
                int LengthNguoigui = txtNguoigui.Text.Trim().Length;
                if (LengthNguoigui == 0)
                {
                    lstMsgT.Text = lstMsgB.Text = "Bạn chưa nhập người gửi. Hãy nhập lại!";
                    txtNguoigui.Focus();
                    return false;
                }
                if (LengthNguoigui > 250)
                {
                    lstMsgT.Text = lstMsgB.Text = "Tên người gửi không quá 250 ký tự. Hãy nhập lại!";
                    txtNguoigui.Focus();
                    return false;
                }
                if (txtSoCMND.Text.Trim().Length > 50)
                {
                    lstMsgT.Text = lstMsgB.Text = "Số CMND không quá 50 ký tự. Hãy nhập lại!";
                    txtSoCMND.Focus();
                    return false;
                }
                if (hddNGDCID.Value == "" || hddNGDCID.Value == "0")
                {
                    lstMsgT.Text = lstMsgB.Text = "Chưa chọn địa chỉ gửi đơn !";
                    txtNGHuyen.Focus();
                    return false;
                }
            }
            //if(ddlCAChidao.SelectedIndex>0 && txtCA_Noidung.Text.Trim()=="")
            //{
            //    lstMsgT.Text = lstMsgB.Text = "Chưa nhập nội dung chỉ đạo !";
            //    txtCA_Noidung.Focus();
            //    return false;                
            //}
            if (ddlHinhthucdon.SelectedValue == "3" || ddlHinhthucdon.SelectedValue == "2")// Đơn + Công văn
            {

                if (chkIsTrongNganh.Checked)
                {
                    if (ddlCV_Donvi.SelectedValue == "0")
                    {
                        lstMsgT.Text = lstMsgB.Text = "Bạn chưa chọn đơn vị gửi công văn. Hãy nhập lại!";
                        ddlCV_Donvi.Focus();
                        return false;
                    }
                }
                else
                {
                    if (txtCV_DonViGuiNgoaiNganh.Text == "")
                    {
                        lstMsgT.Text = lstMsgB.Text = "Bạn chưa nhập tên đơn vị gửi công văn. Hãy nhập lại!";
                        txtCV_DonViGuiNgoaiNganh.Focus();
                        return false;
                    }
                }


            }

            return true;
        }
        private void ResetControls()
        {


            txtSoQDBA.Text = txtNgayBA.Text = "";
            txtSoQDKN.Text = txtNgayKhangNghi.Text = "";

            Cls_Comon.SetValueComboBox(ddlCapXetXu, "0");
            pnPhucTham.Visible = pnAnST.Visible =  false;
            txtSoBA_PT.Text = txtNgayBA_PT.Text = txtSoBA_ST.Text = txtNgayBA_ST.Text = "";
            Cls_Comon.SetValueComboBox(dropToaAnPT, "0");
            Cls_Comon.SetValueComboBox(dropToaAnST, "0");

            ddlNguoiKhangNghi.SelectedIndex = 0;
            txtDoituongbiKN.Text = "";
            chkKN_TBTLD.Checked = false;
            txtNguoigui.Text = txtSoCMND.Text = "";
            hddNGDCID.Value = "0";
            txtNGHuyen.Text = "";
            ddlToaXetXu.SelectedValue = "0";
            ddlGioitinh.SelectedIndex = 0;
            chkIsTuHinh.Checked = false; chkKeuOan.Visible = chkIsAnGiam.Visible = false;
            txtNgaynhandon.Text = txtNgayghidon.Text = txtSohieudon.Text = "";
            ddlHinhthucdon.SelectedIndex = ddlDungdonla.SelectedIndex = ddlTucachTT.SelectedIndex = 0;
            hddNGDCID.Value = "0";
            hddCVDCID.Value = "0";
            txtDiachi.Text = txtNoidung.Text = txtGhichu.Text = "";
            txtSoLuongDon.Text = "1";
            txtCA_Noidung.Text = "";
            ddlCAChidao.SelectedIndex = 0;
            trChidao.Visible = false;
            if (ddlHinhthucdon.SelectedValue == "3")// Loại đơn + công văn
            {
                chkIsTrongNganh.Visible = true;

                if (chkIsTrongNganh.Checked)
                {
                    pnDonViGuiTrongNganh.Visible = true;
                    pnDonViGuiNgoaiNganh.Visible = false;
                    ddlCV_Donvi.SelectedValue = "0";
                }
                else
                {
                    pnDonViGuiTrongNganh.Visible = false;
                    pnDonViGuiNgoaiNganh.Visible = true;
                    txtCV_DonViGuiNgoaiNganh.Text = "";
                }
                txtCV_So.Text = txtCV_Ngay.Text = txtCV_Nguoiky.Text = txtCV_Chucvu.Text = "";
                pnCongvan.Visible = true;
                txtTraigiamhientai.Text = "";
            }
            else
                pnCongvan.Visible = false;
            ddlCV_Donvi.SelectedIndex = 0;
            txtCV_DonViGuiNgoaiNganh.Text = "";
            hddCVDCID.Value = "0";
            txtCVDC.Text = "";
            txtCVDiachi.Text = "";
            txtCV_So.Text = "";
            txtCV_Ngay.Text = "";
            txtCV_Nguoiky.Text = txtCV_Chucvu.Text = "";
            ddlTraloi.SelectedIndex = ddlYeucauthongbao.SelectedIndex = 0;
            hddID.Value = "0";
            trDongkhieunai.Visible = true;
            pnDonTrung.Visible = true;
            ddlSoLuong.SelectedIndex = 0;

            dgList.DataSource = null;
            dgList.DataBind();
            dgList.Visible = false;
            ddlSonguoiKN.SelectedIndex = 0;
            ddlTraloi.SelectedValue = "2";
            txtCV_Traloi_Noidung.Text = "";
            pnCV_traloi.Visible = false;
            chkGDTIsKNTC.Checked = false;
            Cls_Comon.SetFocus(this, this.GetType(), txtSoQDBA.ClientID);
            dgNguoiKN.DataSource = null;
            dgNguoiKN.DataBind();
            dgNguoiKN.Visible = false;
            if (pnKNTCDoituong.Visible)
            {
                foreach (ListItem i in chkPhanloai.Items)
                {
                    i.Selected = false;
                }
            }
            if (pnBAQDGDT.Visible)
            {
                GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
            }
            pnKiemtraTrung.Visible = true;
            txtTenDonTrung.Enabled = true;
            txtTenDonTrung.Text = ""; hddDontrungID.Value = "0";
            trDSTrung.Visible = false;
            dgDSDonTrung.DataSource = null;
            dgDSDonTrung.DataBind();
            pnDSTrung.Visible = false;
            chkKN_TBTLD.Checked = false;
            dgTBTLD.DataSource = null;
            dgTBTLD.DataBind();
            ddlSoTBTLD.SelectedValue = "1";
            chkKN_TBTLD_CheckedChanged(null, null);
        }
        private void LoadCombobox()
        {
            DM_TOAAN_BL oTABL = new DM_TOAAN_BL();
            lstDonVi = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);
            for (int i = 1; i < 30; i++)
            {
                ddlSoLuong.Items.Add(new ListItem(i.ToString() + " đơn trùng", i.ToString()));
            }
            ddlSoLuong.Items.Insert(0, new ListItem("Không có đơn trùng kèm theo", "0"));
            for (int i = 1; i < 30; i++)
            {
                ddlSonguoiKN.Items.Add(new ListItem(i.ToString() + " người đồng khiếu nại", i.ToString()));
            }
            ddlSonguoiKN.Items.Insert(0, new ListItem("Không có người đồng khiếu nại", "0"));

            for (int i = 1; i < 10; i++)
            {
                ddlSoTBTLD.Items.Add(new ListItem(i.ToString() + " thông báo TLĐ", i.ToString()));
            }

            DataTable dtTA = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);// oTABL.DM_TOAAN_GETBY(1);
            ddlToaXetXu.DataSource = dtTA;
            ddlToaXetXu.DataTextField = "MA_TEN";
            ddlToaXetXu.DataValueField = "ID";
            ddlToaXetXu.DataBind();
            ddlToaXetXu.Items.Insert(0, new ListItem("--- Chọn --- ", "0"));

            DataTable dtTA2 = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);
            ddlToaKhac.DataSource = dtTA2;
            ddlToaKhac.DataTextField = "MA_TEN";
            ddlToaKhac.DataValueField = "ID";
            ddlToaKhac.DataBind();
            ddlToaKhac.Items.Insert(0, new ListItem("--- Chọn --- ", "0"));
            ddlCV_Donvi.DataSource = dtTA2;
            ddlCV_Donvi.DataTextField = "MA_TEN";
            ddlCV_Donvi.DataValueField = "ID";
            ddlCV_Donvi.DataBind();
            ddlCV_Donvi.Items.Insert(0, new ListItem("--- Chọn --- ", "0"));


            //Load cán bộ
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            DataTable oCAPCA = oDMCBBL.DM_CANBO_GETBYDONVI_2CHUCVU_HCTP(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCVU.CHUCVU_CA, ENUM_CHUCVU.CHUCVU_PCA);
            ddlCAChidao.DataSource = oCAPCA;
            ddlCAChidao.DataTextField = "MA_TEN";
            ddlCAChidao.DataValueField = "ID";
            ddlCAChidao.DataBind();
            //Load thêm Thẩm phán
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            DataTable oTPTATC = oGDTBL.CANBO_GETBYDONVI(ToaAnID, ENUM_CHUCDANH.CHUCDANH_THAMPHAN);
            foreach (DataRow r in oTPTATC.Rows)
            {
                bool isTPTATC = true;
                foreach (DataRow rPCA in oCAPCA.Rows)
                {
                    if ((rPCA["ID"] + "") == (r["ID"] + ""))
                    {
                        isTPTATC = false;
                        break;
                    }
                }
                if (isTPTATC)
                {
                    ddlCAChidao.Items.Add(new ListItem(r["HOTEN"] + "-Thẩm phán TANDTC", r["ID"] + ""));
                }
            }
            ddlCAChidao.Items.Insert(0, new ListItem("Không", "0"));
            LoadDropLoaiCongVan();
            LoadCapXetXu(0);
            LoadPhongban();
            LoadLoaiAn();
            //Người kháng nghị
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlNguoiKhangNghi.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.NGUOIKHANGNGHI);
            ddlNguoiKhangNghi.DataTextField = "TEN";
            ddlNguoiKhangNghi.DataValueField = "ID";
            ddlNguoiKhangNghi.DataBind();
            ddlNguoiKhangNghi.Items.Insert(0, new ListItem("--- Chọn ---", "0"));

            //Load Lý do trả lại

            ddlLydotralai.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME("LYDOTRADONGDT");
            ddlLydotralai.DataTextField = "TEN";
            ddlLydotralai.DataValueField = "ID";
            ddlLydotralai.DataBind();
            ddlLydotralai.Items.Add(new ListItem("Lý do khác ", "0"));
            LoadInfoTradon();
            //Load loại KNTC

            chkPhanloai.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.PHANLOAIKNTC);
            chkPhanloai.DataTextField = "TEN";
            chkPhanloai.DataValueField = "ID";
            chkPhanloai.DataBind();
        }
        private void LoadInfoTradon()
        {
            if (ddlLydotralai.SelectedValue != "0")
            {
                decimal ID = Convert.ToDecimal(ddlLydotralai.SelectedValue);
                DM_DATAITEM oT = dt.DM_DATAITEM.Where(x => x.ID == ID).FirstOrDefault();
                txtTralaiYeucau.Text = oT.MOTA;
                lblLydokhac.Visible = false;
                txtTralaikhac.Visible = false;
            }
            else
            {
                lblLydokhac.Visible = true;
                txtTralaikhac.Visible = true;
            }
        }
        private void LoadPhongban()
        {
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            ddlChuyendenDV.DataSource = dt.DM_PHONGBAN.Where(x => x.TOAANID == ToaAnID && (x.ISGIAIQUYETDON == 1 || x.ISGIAIQUYETDON == 2)).ToList();
            ddlChuyendenDV.DataTextField = "TENPHONGBAN";
            ddlChuyendenDV.DataValueField = "ID";
            ddlChuyendenDV.DataBind();
        }
        private void LoadCapXetXu(decimal LOAI)
        {
            ddlCapXetXu.Items.Clear();
            if (LOAI == ENUM_GIAIDOANVUAN.SOTHAM)
            {
                ddlCapXetXu.Items.Add(new ListItem("Sơ thẩm", ENUM_GIAIDOANVUAN.SOTHAM.ToString()));
            }
            else if (LOAI == ENUM_GIAIDOANVUAN.PHUCTHAM)
            {
                ddlCapXetXu.Items.Add(new ListItem("Phúc thẩm", ENUM_GIAIDOANVUAN.PHUCTHAM.ToString()));
                ddlCapXetXu.Items.Add(new ListItem("Sơ thẩm", ENUM_GIAIDOANVUAN.SOTHAM.ToString()));
            }
            else
            {
                ddlCapXetXu.Items.Add(new ListItem("--- Chọn ---", "0"));
                ddlCapXetXu.Items.Add(new ListItem("Phúc thẩm", ENUM_GIAIDOANVUAN.PHUCTHAM.ToString()));
                ddlCapXetXu.Items.Add(new ListItem("Sơ thẩm", ENUM_GIAIDOANVUAN.SOTHAM.ToString()));
                ddlCapXetXu.Items.Add(new ListItem("GĐT,TT", ENUM_GIAIDOANVUAN.THULYGDT.ToString()));
            }
        }
        private void LoadDropLoaiCongVan()
        {
            DM_DATAITEM_BL cvBL = new DM_DATAITEM_BL();
            DataTable tbl = cvBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.LOAICVGDTTT);
            if (tbl.Rows.Count > 0)
            {
                ddlLoaiCongVan.DataSource = tbl;
                ddlLoaiCongVan.DataTextField = "MA_TEN";
                ddlLoaiCongVan.DataValueField = "ID";
                ddlLoaiCongVan.DataBind();
            }
            else
            {
                ddlLoaiCongVan.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            }
        }
        private bool SaveData()
        {
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            try
            {
                if (!CheckValid()) return false;
                GDTTT_DON oT;
                if (hddID.Value == "" || hddID.Value == "0")
                {
                    //Kiểm tra số thụ lý
                    if (pnThuLy.Visible && txtSoThuLy.Text != "")
                    {
                        if (oBL.DON_TL_CHECK(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue), txtSoThuLy.Text, 0))
                        {
                            lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                            decimal strMax = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue));

                            lstMsgT.Text = lstMsgB.Text = "Số thụ lý " + txtSoThuLy.Text + " đã sử dụng, số gần nhất là " + (strMax - 1).ToString();
                            txtSoThuLy.Focus();
                            return false;
                        }
                    }
                    oT = new GDTTT_DON();
                }
                else
                {
                    //Kiểm tra số thụ lý khi update
                    //if (pnThuLy.Visible && txtSoThuLy.Text != "")
                    //{
                    //    if (oBL.DON_TL_CHECK(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), Convert.ToDateTime(txtNgaythuly.Text).Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue), txtSoThuLy.Text, Convert.ToDecimal(hddID.Value)))
                    //    {
                    //        lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                    //        lstMsgT.Text = lstMsgB.Text = "Đã tồn tại số thụ lý, hệ thống sẽ tự sinh số thụ lý mới, bạn hãy cập nhật lại !";
                    //        txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
                    //        return false;
                    //    }
                    //}
                    //Kiem tra so thu ly khi sưa đơn đã thụ lý thành đơn thụ lý mới 
                    //Xóa đơn gốc khi đơn được sửa là đơn thụ lý mới cần kiểm tra 

                    decimal ID = Convert.ToDecimal(hddID.Value);
                    oT = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                    //update khi luu kiem tra thoi hieu
                    if(oT.ISTHULY == 1)
                        Canhbao_thoihieu();
                }
                oT.CD_LOAI = Convert.ToDecimal(rdbLoaichuyen.SelectedValue);
                Decimal CurrTrangThai = (String.IsNullOrEmpty(oT.CD_TRANGTHAI + "") ? 0 : (decimal)oT.CD_TRANGTHAI);
                oT.CD_TRANGTHAI = CurrTrangThai; //oT.CD_TRANGTHAI=0;
                oT.LOAI_GDTTTT = Convert.ToDecimal(ddl_LOAI_GDTTT.SelectedValue);//anhvh add 26/04/2020

                switch (rdbLoaichuyen.SelectedValue)
                {
                    case "0"://Nội bộ tòa án                       
                        oT.CD_TA_DONVIID = Convert.ToDecimal(ddlChuyendenDV.SelectedValue);
                        if (pnBAQDGDT.Visible)
                        {
                            oT.CD_TA_TRANGTHAI = Convert.ToDecimal(ddlTrangthaidon.SelectedValue);
                            oT.BAQD_LOAIAN = Convert.ToDecimal(ddlLoaiAn.SelectedValue);
                            if (chkLydoCDDK.Visible)
                            {
                                oT.CD_TA_LYDO_ISBAQD = chkLydoCDDK.Items[0].Selected ? 1 : 0;
                                oT.CD_TA_LYDO_ISXACNHAN = chkLydoCDDK.Items[1].Selected ? 1 : 0;
                                oT.CD_TA_LYDO_ISKHAC = chkLydoCDDK.Items[2].Selected ? 1 : 0;
                                if (oT.CD_TA_LYDO_ISKHAC == 1)
                                    oT.CD_TA_LYDO_KHAC = txtLydoCDDK_Khac.Text;
                            }
                            else if (chkLydoCDDK.Visible == false)
                            {
                                oT.CD_TA_LYDO_ISBAQD = 0;
                                oT.CD_TA_LYDO_ISXACNHAN = 0;
                                oT.CD_TA_LYDO_ISKHAC = 0;
                                oT.CD_TA_LYDO_KHAC = string.Empty;
                                //--------------
                                oT.ISTHULY = null; oT.TL_SO = null; oT.TL_SOTT = null; oT.TL_NGAY = null;
                            }
                            oT.TL_SO = "";
                            oT.TL_NGAY = (DateTime?)null;
                            if (rdbThuLy.Visible)
                            {
                                oT.ISTHULY = Convert.ToDecimal(rdbThuLy.SelectedValue);
                                if (pnThuLy.Visible)
                                {
                                    //cần kiểm tra số thụ ly
                                    oT.TL_SO = txtSoThuLy.Text;
                                    oT.TL_SOTT = Cls_Comon.GetNumber(oT.TL_SO);
                                    oT.TL_NGAY = txtNgaythuly.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgaythuly.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                                }
                                else if (pnThuLy.Visible == false)
                                {
                                    oT.TL_SO = null; oT.TL_SOTT = null; oT.TL_NGAY = null;
                                }
                            }
                            else if (rdbThuLy.Visible == false)
                            {
                                oT.ISTHULY = null; oT.TL_SO = null; oT.TL_SOTT = null; oT.TL_NGAY = null;
                            }
                            oT.ISANTUHINH = 0;
                            oT.ISTH_ANGIAM = 0;
                            oT.ISTH_KEUOAN = 0;
                            if (trTuHinh.Visible && chkIsTuHinh.Checked)
                            {
                                oT.ISANTUHINH = 1;
                                oT.ISTH_ANGIAM = chkIsAnGiam.Checked ? 1 : 0;
                                oT.ISTH_KEUOAN = chkKeuOan.Checked ? 1 : 0;
                            }
                        }
                        break;
                    case "1":
                        oT.CD_TK_DONVIID = Convert.ToDecimal(ddlToaKhac.SelectedValue);
                        oT.CD_TK_NOIGUI = Convert.ToDecimal(rdbDonguitoi.SelectedValue);
                        break;
                    case "2":
                        oT.CD_NTA_TENDONVI = txtNgoaitoaan.Text;
                        //Kiểm tra và lưu lại thông tin danh mục
                        try
                        {
                            string strTenCQ = txtNgoaitoaan.Text.Trim().ToLower();
                            List<DM_COQUANNGOAI> lstCQN = dt.DM_COQUANNGOAI.Where(x => x.TENCOQUAN.ToLower().Contains(strTenCQ)).ToList();
                            if (lstCQN.Count == 0)
                            {
                                DM_COQUANNGOAI oCQN = new DM_COQUANNGOAI();
                                oCQN.TENCOQUAN = txtNgoaitoaan.Text.Trim();
                                dt.DM_COQUANNGOAI.Add(oCQN);
                                dt.SaveChanges();
                            }
                            else
                            {
                                DM_COQUANNGOAI objCQN = lstCQN[0];
                                objCQN.TENCOQUAN = txtNgoaitoaan.Text.Trim();
                                dt.SaveChanges();
                            }
                        }
                        catch (Exception ex) { }
                        break;
                    case "3":
                        oT.CD_TRALAI_LYDOID = Convert.ToDecimal(ddlLydotralai.SelectedValue);
                        if (ddlLydotralai.SelectedValue == "0")
                            oT.CD_TRALAI_LYDOKHAC = txtTralaikhac.Text;
                        else
                            oT.CD_TRALAI_LYDOKHAC = ddlLydotralai.SelectedItem.Text;
                        oT.CD_TRALAI_YEUCAU = txtTralaiYeucau.Text;
                        oT.CD_TRALAI_SOPHIEU = txtTralaiSophieu.Text;
                        oT.CD_TRALAI_NGAYTRA = txtTralaiNgay.Text == "" ? (DateTime?)null : DateTime.Parse(txtTralaiNgay.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                        break;
                    default:
                        break;
                }
                if (pnBAQDGDT.Visible)//Đơn GĐT,TT
                {
                    oT.LOAIKNTC = 0;
                    oT.KN_TRALOIDON = chkKN_TBTLD.Checked ? 1 : 0;
                    if (rdbBAQD.SelectedValue == "0")
                    {
                        oT.BAQD_LOAIQDBA = 0;
                        if (ddlCapXetXu.SelectedValue == "4")
                        {    //Thong tin BA bị đề nghị GĐT,TT
                            oT.BAQD_SO = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                            
                            //Thong tin BA Phúc thẩm
                            oT.BAQD_SO_PT =txtSoBA_PT.Text + "";
                            oT.BAQD_NGAYBA_PT = txtNgayBA_PT.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_PT.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnPT.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnPT.SelectedValue + ""))
                                oT.BAQD_TOAANID_PT = Convert.ToDecimal(dropToaAnPT.SelectedValue);
                            //Thong tin BA ST
                            oT.BAQD_SO_ST = txtSoBA_ST.Text + "";
                            oT.BAQD_NGAYBA_ST = txtNgayBA_ST.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_ST.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnST.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnST.SelectedValue + ""))
                                oT.BAQD_TOAANID_ST = Convert.ToDecimal(dropToaAnST.SelectedValue);
                        }
                        else if (ddlCapXetXu.SelectedValue == "3")
                        {
                            //Thong tin BA bị đề nghị Phuc Tham
                            oT.BAQD_SO_PT = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA_PT = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID_PT = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                            //Thong tin BA ST
                            oT.BAQD_SO_ST = txtSoBA_ST.Text + "";
                            oT.BAQD_NGAYBA_ST = txtNgayBA_ST.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_ST.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnST.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnST.SelectedValue + ""))
                                oT.BAQD_TOAANID_ST = Convert.ToDecimal(dropToaAnST.SelectedValue);
                        }
                        else if (ddlCapXetXu.SelectedValue == "2")
                        {
                            oT.BAQD_SO_ST = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA_ST = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID_ST = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                        }
                            //Thong tin bản án khác
                    }
                    else//Kháng nghị
                    {
                        oT.BAQD_LOAIQDBA = 1;
                        oT.KN_SOQD = txtSoQDKN.Text;
                        oT.KN_NGAY = txtNgayKhangNghi.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayKhangNghi.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGUOIKHANGNGHI = Convert.ToDecimal(ddlNguoiKhangNghi.SelectedValue);

                        if (ddlCapXetXu.SelectedValue == "4")
                        {    //Thong tin BA bị đề nghị GĐT,TT
                            oT.BAQD_SO = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID = Convert.ToDecimal(ddlToaXetXu.SelectedValue);

                            //Thong tin BA Phúc thẩm
                            oT.BAQD_SO_PT = txtSoBA_PT.Text + "";
                            oT.BAQD_NGAYBA_PT = txtNgayBA_PT.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_PT.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnPT.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnPT.SelectedValue + ""))
                                oT.BAQD_TOAANID_PT = Convert.ToDecimal(dropToaAnPT.SelectedValue);
                            //Thong tin BA ST
                            oT.BAQD_SO_ST = txtSoBA_ST.Text + "";
                            oT.BAQD_NGAYBA_ST = txtNgayBA_ST.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_ST.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnST.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnST.SelectedValue + ""))
                                oT.BAQD_TOAANID_ST = Convert.ToDecimal(dropToaAnST.SelectedValue);
                        }
                        else if (ddlCapXetXu.SelectedValue == "3")
                        {   //Thong tin BA bị đề nghị Phuc Tham
                            oT.BAQD_SO_PT = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA_PT = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID_PT = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                            //Thong tin BA ST
                            oT.BAQD_SO_ST = txtSoBA_ST.Text + "";
                            oT.BAQD_NGAYBA_ST = txtNgayBA_ST.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA_ST.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dropToaAnST.SelectedValue != "0" && !String.IsNullOrEmpty(dropToaAnST.SelectedValue + ""))
                                oT.BAQD_TOAANID_ST = Convert.ToDecimal(dropToaAnST.SelectedValue);
                        }
                        else if (ddlCapXetXu.SelectedValue == "2")
                        {
                            oT.BAQD_SO_ST = txtSoQDBA.Text.Trim();
                            oT.BAQD_NGAYBA_ST = txtNgayBA.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayBA.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                            oT.BAQD_TOAANID_ST = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                        }

                    }
                    oT.BAQD_CAPXETXU = Convert.ToDecimal(ddlCapXetXu.SelectedValue);
                }
                else//Đơn KN,TC
                {
                    oT.ISGDTTT_KNTC = 1;
                    oT.LOAIKNTC = Convert.ToDecimal(rdbLoaiKNTC.SelectedValue);
                    oT.DOITUONGBIKNTC = txtDoituongbiKN.Text;
                    //Phân loại KNTC
                    string strPhanloai = "";
                    foreach (ListItem i in chkPhanloai.Items)
                    {
                        if (i.Selected)
                        {
                            if (strPhanloai == "")
                                strPhanloai = i.Value;
                            else
                                strPhanloai = strPhanloai + "," + i.Value;
                        }
                    }
                    oT.ARRPHANLOAI = strPhanloai;
                }
                oT.NGAYNHANDON = (String.IsNullOrEmpty(txtNgaynhandon.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaynhandon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oT.SOHIEUDON = txtSohieudon.Text;
                oT.LOAIDON = Convert.ToDecimal(ddlHinhthucdon.SelectedValue);
                oT.PHANLOAIXULY = Convert.ToDecimal(ddlPhanloai.SelectedValue);
                oT.NOIDUNGDON = txtNoidung.Text;
                oT.NOIDUNGTOMTAT = Cls_Comon.CatXau(oT.NOIDUNGDON, 245);
                if (oT.NOIDUNGDON.Length <= 245)
                    oT.ISSHOWFULL = 0;
                else
                    oT.ISSHOWFULL = 1;
                oT.GHICHU = txtGhichu.Text;
                oT.ISGDTTT_KNTC = chkGDTIsKNTC.Checked ? 1 : 0;
                oT.ISNOTGDTTT = chkIsNotGDT.Checked ? 1 : 0;
                //Thông tin của Đơn hoặc Đơn + Công văn
                if (ddlHinhthucdon.SelectedValue != "2")
                {
                    //if(ddlDungdonla.SelectedValue=="3")
                    oT.NGUOIGUI_HOTEN = txtNguoigui.Text;
                    // else
                    // oT.NGUOIGUI_HOTEN = Cls_Comon.FormatTenRieng(txtNguoigui.Text);
                    oT.NGUOIGUI_CMND = txtSoCMND.Text;
                    oT.NGUOIGUI_GIOITINH = Convert.ToDecimal(ddlGioitinh.SelectedValue);
                    oT.NGAYGHITRENDON = (String.IsNullOrEmpty(txtNgayghidon.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayghidon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    oT.DUNGDONLA = Convert.ToDecimal(ddlDungdonla.SelectedValue);
                    oT.NGUOIGUI_TUCACHTOTUNG = Convert.ToDecimal(ddlTucachTT.SelectedValue);
                    if (hddNGDCID.Value != "0")
                    {
                        try
                        {
                            oT.NGUOIGUI_HUYENID = Convert.ToDecimal(hddNGDCID.Value);
                            DM_HANHCHINH oDCHuyen = dt.DM_HANHCHINH.Where(x => x.ID == oT.NGUOIGUI_HUYENID).FirstOrDefault();
                            oT.NGUOIGUI_TINHID = oDCHuyen.CAPCHAID;
                        }
                        catch (Exception ex) { }
                    }
                    oT.NGUOIGUI_DIACHI = txtDiachi.Text;
                }
                //Đơn + Công văn hoặc Công văn
                if (ddlHinhthucdon.SelectedValue == "3" || ddlHinhthucdon.SelectedValue == "2")
                {
                    oT.CV_ISTRONGNGANH = chkIsTrongNganh.Checked ? 1 : 0;
                    oT.LOAICONGVAN = Convert.ToDecimal(ddlLoaiCongVan.SelectedValue);

                    if (chkIsTrongNganh.Checked)
                    {
                        oT.CV_TENDONVI = ddlCV_Donvi.SelectedItem.Text;
                        oT.CV_TOAANID = Convert.ToDecimal(ddlCV_Donvi.SelectedValue);
                    }
                    else
                    {
                        oT.CV_TENDONVI = txtCV_DonViGuiNgoaiNganh.Text;
                        oT.CV_ISTRAIGIAM = chkTraigiam.Checked ? 1 : 0;
                        //Kiểm tra và lưu lại thông tin danh mục
                        try
                        {
                            string strTenCQ = txtCV_DonViGuiNgoaiNganh.Text.Trim().ToLower();
                            List<DM_COQUANNGOAI> lstCQN = dt.DM_COQUANNGOAI.Where(x => x.TENCOQUAN.ToLower().Contains(strTenCQ)).ToList();
                            if (lstCQN.Count == 0)
                            {
                                DM_COQUANNGOAI oCQN = new DM_COQUANNGOAI();
                                oCQN.TENCOQUAN = txtCV_DonViGuiNgoaiNganh.Text.Trim();
                                dt.DM_COQUANNGOAI.Add(oCQN);
                                dt.SaveChanges();
                            }
                            else
                            {
                                DM_COQUANNGOAI objCQN = lstCQN[0];
                                objCQN.TENCOQUAN = txtCV_DonViGuiNgoaiNganh.Text.Trim();
                                dt.SaveChanges();
                            }
                        }
                        catch (Exception ex) { }
                        oT.CV_TRAIGIAMHIENTAI = txtTraigiamhientai.Text;
                        //if (chkTraigiam.Checked) 
                        //else oT.CV_TRAIGIAMHIENTAI = "";
                    }

                    if (hddCVDCID.Value != "0")
                    {
                        try
                        {
                            oT.CV_HUYENID = Convert.ToDecimal(hddCVDCID.Value);
                            DM_HANHCHINH oDCHuyen = dt.DM_HANHCHINH.Where(x => x.ID == oT.CV_HUYENID).FirstOrDefault();
                            oT.CV_TINHID = oDCHuyen.CAPCHAID;
                        }
                        catch (Exception ex) { }
                    }
                    oT.CV_DIACHI = txtCVDiachi.Text;
                    oT.CV_SO = txtCV_So.Text;
                    oT.CV_NGAY = (String.IsNullOrEmpty(txtCV_Ngay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtCV_Ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    oT.CV_NGUOIKY = txtCV_Nguoiky.Text;
                    oT.CV_CHUCVU = txtCV_Chucvu.Text;

                    if (pnCongVanNhacLai.Visible)//Tham chiếu đến nhắc lại công văn
                    {
                        oT.CV_NHACLAIID = Convert.ToDecimal(hddNhaclaiCVID.Value);
                        oT.CV_NHACLAITEXT = txtCongVan.Text;
                    }

                    oT.CV_YEUCAUTHONGBAO = Convert.ToDecimal(ddlYeucauthongbao.SelectedValue);

                }
                if (ddlCAChidao.SelectedValue == "0")
                    oT.CHIDAO_COKHONG = 0;
                else
                {
                    oT.CHIDAO_COKHONG = 1;
                    oT.CHIDAO_NOIDUNG = txtCA_Noidung.Text;

                    oT.CHIDAO_LANHDAOID = Convert.ToDecimal(ddlCAChidao.SelectedValue);
                }


                oT.TRALOIDON = Convert.ToDecimal(ddlTraloi.SelectedValue);
                if (oT.TRALOIDON == 1)
                {
                    oT.CV_TRALOI_NOIDUNG = txtCV_Traloi_Noidung.Text;
                }

                if (ddlHinhthucdon.SelectedValue == "2")//Công văn
                {
                    oT.NGUOIGUI_HOTEN = oT.CV_TENDONVI;
                    oT.NGAYGHITRENDON = oT.CV_NGAY;
                    oT.NGUOIGUI_HUYENID = oT.CV_HUYENID;
                    oT.NGUOIGUI_DIACHI = oT.CV_DIACHI;
                }

                bool isNew = false;
                if (hddID.Value == "" || hddID.Value == "0")
                {
                    isNew = true;
                    GDTTT_DON_BL dsBL = new GDTTT_DON_BL();
                    oT.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                    oT.CD_TRANGTHAI = 0;
                    oT.TT = dsBL.GETNEWTT((decimal)oT.TOAANID);
                    oT.MADON = ENUM_LOAIVUVIEC.AN_GDTTT + Session[ENUM_SESSION.SESSION_MADONVI] + oT.TT.ToString();
                    oT.NGAYTAO = DateTime.Now;
                    oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    oT.MAGIAIDOAN = ENUM_GIAIDOANVUAN.SOTHAM;
                    oT.TRANGTHAIDON = 0;
                    oT.SOLUONGDON = 1;

                    dt.GDTTT_DON.Add(oT);

                    //bool saveFailed;
                    //do
                    //{
                    //    saveFailed = false;
                    try
                    {
                        dt.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        //var value = ex.Entries.Single();
                        //value.OriginalValues.SetValues(value.GetDatabaseValues());
                        //dt.SaveChanges();

                        // saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                        //dt.Entry<GDTTT_DON>(oT).Reload();
                        lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                        lstMsgT.Text = lstMsgB.Text = "Lỗi khi lưu, bạn hãy thử lại: " + ex.Message;
                        return false;
                    }

                    //} while (saveFailed);

                    if (hddDontrungID.Value == "0" || hddDontrungID.Value == "")
                    {
                        oT.CONLAI_SOLUONG = 1;
                        oT.CONLAI_ARRDONID = oT.ID.ToString();

                        dt.SaveChanges();
                    }
                    else
                    {

                        //Update Lại đơn trùng                         
                        decimal DonTrungID = Convert.ToDecimal(hddDontrungID.Value);
                        GDTTT_DON objDT = dt.GDTTT_DON.Where(x => x.ID == DonTrungID).FirstOrDefault();

                        //Trường hợp đơn trùng không phải đơn gốc
                        List<GDTTT_DON> lstTrung = null;
                        if (objDT.DONTRUNGID > 0)
                        {
                            lstTrung = dt.GDTTT_DON.Where(x => x.DONTRUNGID == objDT.DONTRUNGID || x.ID == objDT.DONTRUNGID).ToList();

                        }
                        else
                        {
                            objDT.SOLUONGDON = 1;
                            objDT.DONTRUNGID = oT.ID;
                            dt.SaveChanges();
                            lstTrung = dt.GDTTT_DON.Where(x => x.DONTRUNGID == objDT.ID).ToList();

                        }
                        if (lstTrung.Count > 0)
                        {
                            foreach (GDTTT_DON d in lstTrung)
                            {
                                d.DONTRUNGID = oT.ID;
                                d.SOLUONGDON = 1;
                                dt.SaveChanges();
                            }
                            dt.SaveChanges();
                        }

                        oBL.UPDATESOLUONGDON(oT.ID);

                    }
                    hddID.Value = oT.ID.ToString();
                }
                else
                {
                    oT.NGAYSUA = DateTime.Now;
                    oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    try
                    {
                        dt.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        //var value = ex.Entries.Single();
                        //value.OriginalValues.SetValues(value.GetDatabaseValues());
                        //dt.SaveChanges();

                        // saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                        //dt.Entry<GDTTT_DON>(oT).Reload();
                        lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                        lstMsgT.Text = lstMsgB.Text = "Lỗi khi lưu, bạn hãy thử lại: " + ex.Message;
                        return false;
                    }
                    catch (EntityDataSourceValidationException e)
                    {
                        lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                        lstMsgT.Text = lstMsgB.Text = "Lỗi Entities: " + e.Message;

                        return false;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        string strErr = "";
                        foreach (var eve in ex.EntityValidationErrors)
                        {
                            foreach (var ve in eve.ValidationErrors)
                            {
                                strErr += ve.PropertyName + " : " + ve.ErrorMessage;
                            }
                        }
                        lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                        lstMsgT.Text = lstMsgB.Text = "Có lỗi, hãy thử lại: " + strErr;

                        return false;
                    }
                    oBL.UPDATESOLUONGDON(oT.ID);
                }

                string strDSNguoiKN = oT.NGUOIGUI_HOTEN;
                //Lưu người khiếu nại
                foreach (DataGridItem item in dgNguoiKN.Items)
                {
                    TextBox txtHoten = (TextBox)item.FindControl("txtHoten");
                    TextBox txtDiachi = (TextBox)item.FindControl("txtDiachi");
                    string strKNID = item.Cells[0].Text;
                    if (txtHoten.Text.Trim() != "")
                    {
                        strDSNguoiKN = strDSNguoiKN + ", " + txtHoten.Text.Trim();
                        GDTTT_DON_NGUOIKN oKN = new GDTTT_DON_NGUOIKN();
                        if (strKNID != "" && strKNID != "0")
                        {
                            decimal KNID = Convert.ToDecimal(strKNID);
                            oKN = dt.GDTTT_DON_NGUOIKN.Where(x => x.ID == KNID).FirstOrDefault();
                            oKN.HOTEN = txtHoten.Text;
                            oKN.DIACHI = txtDiachi.Text;
                            oKN.DONID = oT.ID;
                        }
                        else
                        {
                            oKN.HOTEN = txtHoten.Text;
                            oKN.DIACHI = txtDiachi.Text;
                            oKN.DONID = oT.ID;
                            dt.GDTTT_DON_NGUOIKN.Add(oKN);
                        }
                    }
                    else
                    {
                        if (strKNID != "" && strKNID != "0")
                        {
                            decimal KNID = Convert.ToDecimal(strKNID);
                            GDTTT_DON_NGUOIKN oKN = dt.GDTTT_DON_NGUOIKN.Where(x => x.ID == KNID).FirstOrDefault();
                            dt.GDTTT_DON_NGUOIKN.Remove(oKN);
                        }
                    }
                }
                oT.DONGKHIEUNAI = strDSNguoiKN;

                //Lưu thông báo trả lời đơn
                try
                {
                    foreach (DataGridItem item in dgTBTLD.Items)
                    {
                        TextBox txtSo = (TextBox)item.FindControl("txtSo");
                        TextBox txtNgay = (TextBox)item.FindControl("txtNgay");
                        DropDownList ddlToaAn = (DropDownList)item.FindControl("ddlToaAn");
                        string strKNID = item.Cells[0].Text;
                        if (isNew)
                            strKNID = "0";
                        if (txtSo.Text.Trim() != "")
                        {
                            GDTTT_DON_TBTLD oKN = new GDTTT_DON_TBTLD();
                            if (strKNID != "" && strKNID != "0")
                            {
                                decimal KNID = Convert.ToDecimal(strKNID);
                                oKN = dt.GDTTT_DON_TBTLD.Where(x => x.ID == KNID).FirstOrDefault();
                                oKN.SO = txtSo.Text;
                                DateTime? dNgay = (String.IsNullOrEmpty(txtNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                                oKN.NGAY = dNgay;
                                oKN.DONVITB = Convert.ToDecimal(ddlToaAn.SelectedValue);
                                oKN.DONID = oT.ID;

                            }
                            else
                            {
                                if (dt.GDTTT_DON_TBTLD.Where(x => x.DONID == oT.ID && x.SO == txtSo.Text).ToList().Count == 0)
                                {
                                    oKN.SO = txtSo.Text;
                                    DateTime? dNgay = (String.IsNullOrEmpty(txtNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                                    oKN.NGAY = dNgay;
                                    oKN.DONID = oT.ID;
                                    oKN.DONVITB = Convert.ToDecimal(ddlToaAn.SelectedValue);
                                    dt.GDTTT_DON_TBTLD.Add(oKN);

                                }
                            }
                        }
                        else
                        {
                            if (strKNID != "" && strKNID != "0")
                            {
                                decimal KNID = Convert.ToDecimal(strKNID);
                                GDTTT_DON_TBTLD oKN = dt.GDTTT_DON_TBTLD.Where(x => x.ID == KNID).FirstOrDefault();
                                dt.GDTTT_DON_TBTLD.Remove(oKN);

                            }
                        }
                    }
                }
                catch (Exception ex) { }
                try
                {
                    dt.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    //var value = ex.Entries.Single();
                    //value.OriginalValues.SetValues(value.GetDatabaseValues());
                    //dt.SaveChanges();

                    // saveFailed = true;

                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                    //dt.Entry<GDTTT_DON>(oT).Reload();
                    lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                    lstMsgT.Text = lstMsgB.Text = "Lỗi khi lưu, bạn hãy thử lại: " + ex.Message;
                    return false;
                }

                return true;
            }
            catch (EntityDataSourceValidationException e)
            {
                lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                lstMsgT.Text = lstMsgB.Text = "Lỗi Entities: " + e.Message;

                return false;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                string strErr = "";
                foreach (var eve in ex.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        strErr += ve.PropertyName + " : " + ve.ErrorMessage;
                    }
                }
                lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                lstMsgT.Text = lstMsgB.Text = "Có lỗi, hãy thử lại: " + strErr;

                return false;
            }
            catch (Exception ex)
            {
                lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Red;
                lstMsgT.Text = lstMsgB.Text = "Lỗi: " + ex.Message + " | " + ex.InnerException.ToString();

                return false;
            }
        }
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (SaveData())
            {
                if (pnDonTrung.Visible && ddlSoLuong.SelectedValue != "0")
                {
                    hddNext.Value = "0";
                    SaveDontrung();
                    ddlSoLuong.SelectedValue = "0";
                    pnDonTrung.Visible = false;
                    LoadDSTrung();
                }
                //load lại TB trả lời đơn
                if (chkKN_TBTLD.Checked)
                {
                    LoadTBTLD(Convert.ToDecimal(hddID.Value));
                }
                // Response.Redirect("Danhsachdon.aspx");
                lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Blue;
                lstMsgT.Text = lstMsgB.Text = "Lưu thành công !";
                //anhvh add 22/10/2020 update trang thai da xu ly vào văn thư đến
                if (Request["vt_id"] + "" != "")
                {
                    VT_CHUYEN_NHAN o_Object = new VT_CHUYEN_NHAN();
                    VT_CHUYEN_NHAN_BL M_Object = new VT_CHUYEN_NHAN_BL();
                    M_Object.TRANGTHAI_XULY_UP(hddID.Value);
                }
                //--------------------------------
            }
        }
        protected void cmdUpdateAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveData())
                {
                    if (pnDonTrung.Visible && ddlSoLuong.SelectedValue != "0")
                    {
                        hddNext.Value = "1";
                        SaveDontrung();
                    }
                    //anhvh add 22/10/2020 update trang thai da xu ly vào văn thư đến
                    if (Request["vt_id"] + "" != "")
                    {
                        VT_CHUYEN_NHAN o_Object = new VT_CHUYEN_NHAN();
                        VT_CHUYEN_NHAN_BL M_Object = new VT_CHUYEN_NHAN_BL();
                        M_Object.TRANGTHAI_XULY_UP(hddID.Value);
                    }
                    //--------------------------------
                    ResetControls();
                    lstMsgT.ForeColor = lstMsgB.ForeColor = System.Drawing.Color.Blue;
                    lstMsgT.Text = lstMsgB.Text = "Hoàn thành Lưu, bạn hãy nhập thông tin đơn tiếp theo !";

                }
            }
            catch (Exception ex) { lstMsgB.Text = lstMsgT.Text = ex.Message; }
        }
        protected void cmdQuaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsachdon.aspx");
        }
        protected void cmdLamMoi_Click(object sender, EventArgs e)
        {
            ResetControls();
        }
        private void ddlHinhthucdonChange()
        {
            lblTucach.Visible = true;
            ddlTucachTT.Visible = true;
            lblTitleNgaytrendon.Visible = true;
            txtNgayghidon.Visible = true;
            lblTitleNoidung.Text = "Nội dung đơn";
            pnDonTrung.Visible = false;
            if (ddlHinhthucdon.SelectedValue == "3")//Đơn + Công văn
            {
                pnTTDon.Visible = true;
                trDongkhieunai.Visible = true;
                trTitleCongvan.Visible = true;
                pnCongvan.Visible = true;
                if (hddID.Value == "0") pnDonTrung.Visible = true;
                ChangeLoaiCV();

                if (pnCongvan.Visible)
                {
                    if (chkIsTrongNganh.Checked)
                    {
                        pnDonViGuiTrongNganh.Visible = true;
                        pnDonViGuiNgoaiNganh.Visible = false;
                        chkTraigiam.Visible = false;
                    }
                    else
                    {
                        chkTraigiam.Visible = true;
                        pnDonViGuiTrongNganh.Visible = false;
                        pnDonViGuiNgoaiNganh.Visible = true;
                    }
                }
            }
            else if (ddlHinhthucdon.SelectedValue == "2")//Công văn
            {
                lblTitleNgaytrendon.Visible = false;
                txtNgayghidon.Visible = false;
                pnTTDon.Visible = false;
                trDongkhieunai.Visible = false;
                trTitleCongvan.Visible = false;
                pnCongvan.Visible = true;
                pnDonTrung.Visible = false;
                lblTitleNoidung.Text = "Nội dung công văn";
            }
            else//ĐƠn
            {
                pnTTDon.Visible = true;
                trDongkhieunai.Visible = true;
                trTitleCongvan.Visible = true;
                pnCongvan.Visible = false;
                if (hddID.Value == "0") pnDonTrung.Visible = true;
            }
        }
        protected void ddlHinhthucdon_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlHinhthucdonChange();
            Cls_Comon.SetFocus(this, this.GetType(), ddlHinhthucdon.ClientID);
        }
        protected void chkIsTrongNganh_CheckedChanged(object sender, EventArgs e)
        {
            if (pnCongvan.Visible)
            {
                if (chkIsTrongNganh.Checked)
                {
                    pnDonViGuiTrongNganh.Visible = true;
                    pnDonViGuiNgoaiNganh.Visible = false;
                    chkTraigiam.Visible = false;
                }
                else
                {
                    chkTraigiam.Visible = true;
                    pnDonViGuiTrongNganh.Visible = false;
                    pnDonViGuiNgoaiNganh.Visible = true;
                }
            }
            Cls_Comon.SetFocus(this, this.GetType(), chkTraigiam.ClientID);
        }
        protected void chkTraigiam_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTraigiam.Checked)
            {
                lblTraigiamhientai.Visible = txtTraigiamhientai.Visible = true;
            }
            else
            {
                lblTraigiamhientai.Visible = txtTraigiamhientai.Visible = false;
            }
            Cls_Comon.SetFocus(this, this.GetType(), txtCV_DonViGuiNgoaiNganh.ClientID);
        }
        protected void rdbBAQD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbBAQD.SelectedValue == "0")
            {
                pnSoKhangNghi.Visible = false;
                pnSoBA.Visible = true;
            }
            else
            {
                pnSoKhangNghi.Visible = true;
                pnSoBA.Visible = true;
            }
            Cls_Comon.SetFocus(this, this.GetType(), chkKN_TBTLD.ClientID);
        }
        protected void ddlPhanloai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPhanloai.SelectedValue == "04")
            {
                ddlTrangthaidon.SelectedValue = "1";
                lblLydoCDDK.Text = "Lý do";
                chkLydoCDDK.Visible = true;
            }
            Cls_Comon.SetFocus(this, this.GetType(), ddlPhanloai.ClientID);
        }
        private void LoaiAnChange()
        {
            if ((hddID.Value == "" || hddID.Value == "0" || txtSoThuLy.Text == "") && pnBAQDGDT.Visible)
            {
                GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
            }
            LoadIsTuHinh(ddlLoaiAn.SelectedValue);
        }
        protected void ddlLoaiAn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((hddID.Value == "" || hddID.Value == "0") && pnBAQDGDT.Visible)
            {
                GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
                Cls_Comon.SetFocus(this, this.GetType(), ddlTrangthaidon.ClientID);
            }
            LoadIsTuHinh(ddlLoaiAn.SelectedValue);
        }
        private void ChangeLoaiCV()
        {
            lstNguoiguiTitle.Text = "Người gửi/Đơn vị gửi";
            lblTraigiamhientai.Text = "Trại giam hiện tại (nếu thay đổi)";
            if (pnCongvan.Visible)
            {
                decimal IDLoai = Convert.ToDecimal(ddlLoaiCongVan.SelectedValue);
                DM_DATAITEM oLoai = dt.DM_DATAITEM.Where(x => x.ID == IDLoai).FirstOrDefault();
                pnCongVanNhacLai.Visible = false;
                if (oLoai.MA == ENUM_GDT_LOAICV.CV8_1_DBQH)// công văn của đại biểu quốc hội
                {
                    chkIsTrongNganh.Checked = false;
                    chkIsTrongNganh.Visible = false;
                    chkIsTrongNganh_CheckedChanged(null, null);
                    chkTraigiam.Visible = false;
                    lstNguoiguiTitle.Text = "Tên Đại biểu";
                    lblTraigiamhientai.Text = "Đơn vị(Khóa?,Đoàn Đại biểu?)";
                    lblTraigiamhientai.Visible = true;
                    txtTraigiamhientai.Visible = true;
                    pnDonViGuiNgoaiNganh.Visible = true;
                    pnDonViGuiTrongNganh.Visible = false;
                }
                else if (oLoai.MA == ENUM_GDT_LOAICV.NHACLAI)// Công văn nhắc lại
                {
                    chkIsTrongNganh.Visible = true;
                    chkTraigiam.Visible = true;
                    lblTraigiamhientai.Visible = false;
                    txtTraigiamhientai.Visible = false;
                    pnCongVanNhacLai.Visible = true;
                }
                else
                {
                    chkIsTrongNganh.Visible = true;
                    chkTraigiam.Visible = true;
                    lblTraigiamhientai.Visible = false;
                    txtTraigiamhientai.Visible = false;
                }
            }
        }
        protected void ddlLoaiCongVan_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeLoaiCV();
            Cls_Comon.SetFocus(this, this.GetType(), ddlLoaiCongVan.ClientID);
        }
        protected void ddlTraloi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTraloi.SelectedValue == "1")
                pnCV_traloi.Visible = true;
            else
                pnCV_traloi.Visible = false;
            Cls_Comon.SetFocus(this, this.GetType(), ddlTraloi.ClientID);
        }
        protected void ddlTrangthaidon_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTrangthaidon.SelectedValue == "0")
            {
                lblLydoCDDK.Text = "Thụ lý đơn";
                chkLydoCDDK.Visible = false;
                rdbThuLy.Visible = true;
                pnThuLy.Visible = true;
                trLydokhac.Visible = false;
                LoaiAnChange();
            }
            else
            {
                lblLydoCDDK.Text = "Lý do";
                chkLydoCDDK.Visible = true;
                rdbThuLy.Visible = false;
                pnThuLy.Visible = false;
            }
            Cls_Comon.SetFocus(this, this.GetType(), ddlTrangthaidon.ClientID);
        }
        private void LoadLoaiAn()
        {
            ddlLoaiAn.Items.Clear();
            decimal PBID = Convert.ToDecimal(ddlChuyendenDV.SelectedValue);
            DM_PHONGBAN obj = dt.DM_PHONGBAN.Where(x => x.ID == PBID).FirstOrDefault();
            if (obj.ISHINHSU == 1) ddlLoaiAn.Items.Add(new ListItem("Hình sự", ENUM_LOAIVUVIEC.AN_HINHSU));
            if (obj.ISDANSU == 1) ddlLoaiAn.Items.Add(new ListItem("Dân sự", ENUM_LOAIVUVIEC.AN_DANSU));
            if (obj.ISHANHCHINH == 1) ddlLoaiAn.Items.Add(new ListItem("Hành chính", ENUM_LOAIVUVIEC.AN_HANHCHINH));
            if (obj.ISHNGD == 1) ddlLoaiAn.Items.Add(new ListItem("Hôn nhân gia đình", ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH));
            if (obj.ISKDTM == 1) ddlLoaiAn.Items.Add(new ListItem("Kinh doanh, thương mại", ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI));
            if (obj.ISLAODONG == 1) ddlLoaiAn.Items.Add(new ListItem("Lao động", ENUM_LOAIVUVIEC.AN_LAODONG));
            if (obj.ISPHASAN == 1) ddlLoaiAn.Items.Add(new ListItem("Phá sản", ENUM_LOAIVUVIEC.AN_PHASAN));
        }
        private void ChuyendenDVChange()
        {
            decimal IDPhongBan = Convert.ToDecimal(ddlChuyendenDV.SelectedValue);
            DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == IDPhongBan).FirstOrDefault();
            if (oPB.ISGIAIQUYETDON == 1)
            {

                trTrangthaidon.Visible = true;
                pnBAQDGDT.Visible = true;
                pnKNTCDoituong.Visible = false;

                ddlLoaiAn.Visible = true;
                chkGDTIsKNTC.Visible = true;
                lblLoaiAn.Visible = true;
                if (ddlTrangthaidon.SelectedValue == "0" && rdbThuLy.SelectedValue == "1")
                    pnThuLy.Visible = true;
                else
                    pnThuLy.Visible = false;
            }
            else
            {
                pnThuLy.Visible = false;
                trTrangthaidon.Visible = false;
                pnBAQDGDT.Visible = false;
                pnKNTCDoituong.Visible = true;

                ddlLoaiAn.Visible = false;
                chkGDTIsKNTC.Visible = false;
                lblLoaiAn.Visible = false;
            }
            LoadLoaiAn();
            LoaiAnChange();

        }
        protected void ddlChuyendenDV_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChuyendenDVChange();
            Cls_Comon.SetFocus(this, this.GetType(), ddlChuyendenDV.ClientID);
        }
        private void setLoaichuyen()
        {
            spSOBA.Visible = spSOKN.Visible = spTOAXX.Visible = spNGBA.Visible = spNGKN.Visible = false;

            switch (rdbLoaichuyen.SelectedValue)
            {
                case "0":
                    spSOBA.Visible = spSOKN.Visible = spTOAXX.Visible = spNGBA.Visible = spNGKN.Visible = true;
                    pnNoibo.Visible = true;
                    pnToakhac.Visible = false;
                    pnNgoaitoaan.Visible = false;
                    pnTralaidon.Visible = false;
                    pnBAQDGDT.Visible = true;
                    pnKNTCDoituong.Visible = false;
                    decimal IDPhongBan = Convert.ToDecimal(ddlChuyendenDV.SelectedValue);
                    DM_PHONGBAN oPB = dt.DM_PHONGBAN.Where(x => x.ID == IDPhongBan).FirstOrDefault();
                    if (oPB.ISGIAIQUYETDON == 1)
                    {

                        trTrangthaidon.Visible = true;
                        pnBAQDGDT.Visible = true;
                        pnKNTCDoituong.Visible = false;

                        ddlLoaiAn.Visible = true;
                        chkGDTIsKNTC.Visible = true;
                        lblLoaiAn.Visible = true;
                        if (ddlTrangthaidon.SelectedValue == "0")
                            pnThuLy.Visible = true;
                        else
                            pnThuLy.Visible = false;
                    }
                    else
                    {
                        pnThuLy.Visible = false;
                        trTrangthaidon.Visible = false;
                        pnBAQDGDT.Visible = false;
                        pnKNTCDoituong.Visible = true;

                        ddlLoaiAn.Visible = false;
                        chkGDTIsKNTC.Visible = false;
                        lblLoaiAn.Visible = false;
                    }
                    break;
                case "1":
                    pnNoibo.Visible = false;
                    pnToakhac.Visible = true;
                    pnNgoaitoaan.Visible = false;
                    pnTralaidon.Visible = false;
                    pnBAQDGDT.Visible = true;
                    pnKNTCDoituong.Visible = false;
                    lblLoaiAn.Visible = true;
                    chkGDTIsKNTC.Visible = true;
                    break;
                case "2":
                    pnNoibo.Visible = false;
                    pnToakhac.Visible = false;
                    pnNgoaitoaan.Visible = true;
                    pnTralaidon.Visible = false;
                    pnBAQDGDT.Visible = true;
                    pnKNTCDoituong.Visible = false;
                    chkGDTIsKNTC.Visible = true;
                    break;
                case "3":
                    pnNoibo.Visible = false;
                    pnToakhac.Visible = false;
                    pnNgoaitoaan.Visible = false;
                    pnTralaidon.Visible = true;
                    pnBAQDGDT.Visible = true;
                    pnKNTCDoituong.Visible = false;
                    chkGDTIsKNTC.Visible = true;
                    break;
                default:
                    pnNoibo.Visible = false;
                    pnToakhac.Visible = false;
                    pnNgoaitoaan.Visible = false;
                    pnTralaidon.Visible = false;
                    pnBAQDGDT.Visible = true;
                    pnKNTCDoituong.Visible = false;
                    chkGDTIsKNTC.Visible = true;
                    break;
            }
        }
        protected void rdbLoaichuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            setLoaichuyen();
            rdbLoaichuyen.Focus();
        }

        protected void ddlLydotralai_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadInfoTradon();
            Cls_Comon.SetFocus(this, this.GetType(), ddlLydotralai.ClientID);
        }

        protected void cmdDanhsach_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsachdon.aspx");
        }

        protected void chkLydoCDDK_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkLydoCDDK.Items[2].Selected)
                trLydokhac.Visible = true;
            else
                trLydokhac.Visible = false;
            Cls_Comon.SetFocus(this, this.GetType(), chkLydoCDDK.ClientID);

        }

        protected void chkIsCongVan_CheckedChanged(object sender, EventArgs e)
        {
            ddlHinhthucdon.SelectedValue = "3";
            ddlHinhthucdonChange();
            //Bỏ vì không có tác dụng - ng yêu cầu đ/c Mạnh
            //chkIsCongVan.Visible = true;
            Cls_Comon.SetFocus(this, this.GetType(), ddlCAChidao.ClientID);
        }
        private void SaveDontrung()
        {
            decimal ID = Convert.ToDecimal(hddID.Value);
            GDTTT_DON oDon = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
            GDTTT_DON_BL dsBL = new GDTTT_DON_BL();
            string strErr = "";
            foreach (DataGridItem item in dgList.Items)
            {
                TextBox txtNgaynhandon = (TextBox)item.FindControl("txtNgaynhandon");
                TextBox txtNgayghitrendon = (TextBox)item.FindControl("txtNgayghitrendon");
                DateTime dNgayNhan = (String.IsNullOrEmpty(txtNgaynhandon.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgaynhandon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                DateTime dNgaytrendon = (String.IsNullOrEmpty(txtNgayghitrendon.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgayghitrendon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                string strTT = item.Cells[0].Text;
                if (dNgayNhan != DateTime.MinValue && dNgaytrendon != DateTime.MinValue)
                {

                }
                else
                {
                    if (strErr == "") strErr = strTT;
                    else strErr = strErr + "," + strTT;
                }
            }
            if (strErr != "")
            {
                lstMsgB.Text = lstMsgT.Text = "Lỗi nhập ngày tháng đơn dòng số " + strErr;
            }
            else
            {
                foreach (DataGridItem item in dgList.Items)
                {
                    TextBox txtNgaynhandon = (TextBox)item.FindControl("txtNgaynhandon");
                    TextBox txtNgayghitrendon = (TextBox)item.FindControl("txtNgayghitrendon");
                    DateTime dNgayNhan = (String.IsNullOrEmpty(txtNgaynhandon.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgaynhandon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    DateTime dNgaytrendon = (String.IsNullOrEmpty(txtNgayghitrendon.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgayghitrendon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    string strTT = item.Cells[0].Text;
                    CheckBox chkIsCV = (CheckBox)item.FindControl("chkIsCV");

                    TextBox txtSoCV = (TextBox)item.FindControl("txtSoCV");
                    TextBox txtNgayCV = (TextBox)item.FindControl("txtNgayCV");
                    CheckBox chkIsTrongnganh = (CheckBox)item.FindControl("chkIsTrongnganh");
                    TextBox txtDonvi = (TextBox)item.FindControl("txtDonvi");
                    DropDownList ddlDonvi = (DropDownList)item.FindControl("ddlDonvi");

                    if (dNgayNhan != DateTime.MinValue && dNgaytrendon != DateTime.MinValue)
                    {
                        GDTTT_DON obj = new GDTTT_DON();
                        obj.TOAANID = oDon.TOAANID;
                     
                        obj.BAQD_LOAIAN = oDon.BAQD_LOAIAN;
                        obj.BAQD_LOAIQDBA = oDon.BAQD_LOAIQDBA;
                        obj.VUVIECID = oDon.VUVIECID;
                        obj.BAQD_CAPXETXU = oDon.BAQD_CAPXETXU;
      
                        obj.BAQD_SO = oDon.BAQD_SO;
                        obj.BAQD_NGAYBA = oDon.BAQD_NGAYBA;
                        obj.BAQD_TOAANID = oDon.BAQD_TOAANID;

                        obj.BAQD_SO_PT = oDon.BAQD_SO_PT;
                        obj.BAQD_NGAYBA_PT = oDon.BAQD_NGAYBA_PT;
                        obj.BAQD_TOAANID_PT = oDon.BAQD_TOAANID_PT;

                        obj.BAQD_SO_ST = oDon.BAQD_SO_ST;
                        obj.BAQD_NGAYBA_ST = oDon.BAQD_NGAYBA_ST;
                        obj.BAQD_TOAANID_ST = oDon.BAQD_TOAANID_ST;


                        obj.KN_LOAI = oDon.KN_LOAI;
                        obj.KN_SOQD = oDon.KN_SOQD;
                        obj.KN_NGAY = oDon.KN_NGAY;
                        obj.LOAIDON = oDon.LOAIDON;
                        //obj.CV_ISTRONGNGANH = oDon.CV_ISTRONGNGANH;
                        //obj.CV_TOAANID = oDon.CV_TOAANID;
                        //obj.CV_TENDONVI = oDon.CV_TENDONVI;
                        //obj.CV_SO = oDon.CV_SO;
                        //obj.CV_NGAY = oDon.CV_NGAY;
                        //obj.CV_TINHID = oDon.CV_TINHID;
                        //obj.CV_HUYENID = oDon.CV_HUYENID;
                        //obj.CV_DIACHI = oDon.CV_DIACHI;
                        //obj.CV_NGUOIKY = oDon.CV_NGUOIKY;
                        //obj.CV_CHUCVU = oDon.CV_CHUCVU;

                        obj.NGAYNHANDON = dNgayNhan;
                        obj.NGAYGHITRENDON = dNgaytrendon;
                        obj.SOHIEUDON = txtSohieudon.Text;
                        obj.DUNGDONLA = oDon.DUNGDONLA;
                        obj.NGUOIGUI_HOTEN = oDon.NGUOIGUI_HOTEN;
                        obj.NGUOIGUI_GIOITINH = oDon.NGUOIGUI_GIOITINH;
                        obj.NGUOIGUI_CMND = oDon.NGUOIGUI_CMND;
                        obj.NGUOIGUI_TINHID = oDon.NGUOIGUI_TINHID;
                        obj.NGUOIGUI_HUYENID = oDon.NGUOIGUI_HUYENID;
                        obj.NGUOIGUI_DIACHI = oDon.NGUOIGUI_DIACHI;
                        obj.NGUOIGUI_DIENTHOAI = oDon.NGUOIGUI_DIENTHOAI;
                        obj.NGUOIGUI_EMAIL = oDon.NGUOIGUI_EMAIL;
                        obj.NGUOIGUI_TUCACHTOTUNG = oDon.NGUOIGUI_TUCACHTOTUNG;
                        obj.DONGKHIEUNAI = obj.NGUOIGUI_HOTEN;

                        obj.NOIDUNGDON = oDon.NOIDUNGDON;
                        obj.SOLUONGDON = 1;
                        obj.SOTHUTUDON = oDon.SOTHUTUDON;


                        obj.PHANLOAIXULY = 3;
                        obj.TRALOIDON = oDon.TRALOIDON;
                        obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        obj.NGAYTAO = DateTime.Now;
                        obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        obj.NGAYSUA = DateTime.Now;
                        obj.TT = dsBL.GETNEWTT((decimal)obj.TOAANID);
                        obj.MADON = ENUM_LOAIVUVIEC.AN_GDTTT + Session[ENUM_SESSION.SESSION_MADONVI] + obj.TT.ToString();


                        obj.MAGIAIDOAN = oDon.MAGIAIDOAN;
                        obj.LOAICONGVAN = oDon.LOAICONGVAN;
                        obj.DONVIUUTIEN = oDon.DONVIUUTIEN;
                        obj.NGUOIKHANGNGHI = oDon.NGUOIKHANGNGHI;
                        obj.CV_NHACLAIID = oDon.CV_NHACLAIID;

                        obj.DONTRUNGID = ID;
                        obj.ISCHUYENXULY = oDon.ISCHUYENXULY;
                        obj.DONVIXULYID = oDon.DONVIXULYID;
                        obj.CV_NHACLAITEXT = oDon.CV_NHACLAITEXT;
                        obj.DONTRUNGTEXT = oDon.DONTRUNGTEXT;
                        obj.CV_TRALOI_SO = oDon.CV_TRALOI_SO;
                        obj.CV_TRALOI_NGAY = oDon.CV_TRALOI_NGAY;
                        obj.CV_TRALOI_NOIDUNG = oDon.CV_TRALOI_NOIDUNG;
                        obj.CD_LOAI = oDon.CD_LOAI;
                        obj.CD_TA_DONVIID = oDon.CD_TA_DONVIID;
                        obj.CD_TA_TRANGTHAI = oDon.CD_TA_TRANGTHAI;
                        obj.CD_TA_LYDO_ISBAQD = oDon.CD_TA_LYDO_ISBAQD;
                        obj.CD_TA_LYDO_ISXACNHAN = oDon.CD_TA_LYDO_ISXACNHAN;
                        obj.CD_TA_LYDO_ISKHAC = oDon.CD_TA_LYDO_ISKHAC;

                        obj.CD_TK_DONVIID = oDon.CD_TK_DONVIID;
                        obj.CD_TK_NOIGUI = oDon.CD_TK_NOIGUI;
                        obj.CD_NTA_TENDONVI = oDon.CD_NTA_TENDONVI;
                        obj.CD_TRALAI_LYDOID = oDon.CD_TRALAI_LYDOID;
                        obj.CD_TRALAI_YEUCAU = oDon.CD_TRALAI_YEUCAU;
                        obj.CD_TRALAI_SOPHIEU = oDon.CD_TRALAI_SOPHIEU;
                        obj.CD_TRALAI_NGAYTRA = oDon.CD_TRALAI_NGAYTRA;
                        obj.CD_TRANGTHAI = 0;
                        // obj.CD_NGAYXULY = oDon.CD_NGAYXULY;
                        obj.GHICHU = oDon.GHICHU;
                        obj.TRANGTHAIDON = 0;
                        if (oDon.ISTHULY == 1)
                        {
                            obj.ISTHULY = 2;
                            obj.TL_SO = oDon.TL_SO;
                            obj.TL_NGAY = oDon.TL_NGAY;
                            obj.TL_NGUOI = oDon.TL_NGUOI;
                        }
                        else
                        {
                            obj.ISTHULY = oDon.ISTHULY;
                            obj.TL_SO = oDon.TL_SO;
                            obj.TL_NGAY = oDon.TL_NGAY;
                            obj.TL_NGUOI = oDon.TL_NGUOI;
                        }
                        obj.THAMPHANID = oDon.THAMPHANID;
                        obj.CD_NGUOIKY = oDon.CD_NGUOIKY;
                        //obj.CD_SOCV = oDon.CD_SOCV;
                        //obj.CD_NGAYCV = oDon.CD_NGAYCV;
                        obj.NOIDUNGTOMTAT = oDon.NOIDUNGTOMTAT;
                        obj.ISSHOWFULL = oDon.ISSHOWFULL;

                        obj.CD_TRALAI_LYDOKHAC = oDon.CD_TRALAI_LYDOKHAC;
                        obj.CD_TA_LYDO_KHAC = oDon.CD_TA_LYDO_KHAC;
                        if (chkIsCV.Checked)
                        {
                            obj.LOAIDON = 3;
                            obj.CV_SO = txtSoCV.Text;
                            DateTime dNgayCV = (String.IsNullOrEmpty(txtNgayCV.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(txtNgayCV.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                            obj.CV_NGAY = dNgayCV;
                            if (chkIsTrongnganh.Checked)
                            {
                                obj.CV_ISTRONGNGANH = 1;
                                obj.CV_TOAANID = Convert.ToDecimal(ddlDonvi.SelectedValue);
                                obj.CV_TENDONVI = ddlDonvi.SelectedItem.Text;
                                obj.CV_NHACLAIID = 4;
                            }
                            else
                            {
                                obj.CV_ISTRONGNGANH = 0;
                                obj.CV_TENDONVI = txtDonvi.Text;
                            }
                        }else
                            obj.LOAIDON = 1;

                        dt.GDTTT_DON.Add(obj);
                        dt.SaveChanges();
                        // if (oDon.SOLUONGDON != null)
                        oDon.SOLUONGDON = 1;
                        //else
                        //    oDon.SOLUONGDON = 2;
                        //if (oDon.CONLAI_SOLUONG != null)
                        //    oDon.CONLAI_SOLUONG += 1;
                        //else
                        //    oDon.CONLAI_SOLUONG = 1;
                        //if ((oDon.CONLAI_ARRDONID+"") == "")
                        //    oDon.CONLAI_ARRDONID = obj.ID.ToString();
                        //else
                        //    oDon.CONLAI_ARRDONID = oDon.CONLAI_ARRDONID + "," + obj.ID.ToString();
                        dt.SaveChanges();
                    }
                }
                GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                oBL.UPDATESOLUONGDON(ID);
                //if (hddNext.Value == "0")
                //    Response.Redirect("Danhsachdon.aspx");
                //else
                //{
                //    ResetControls();
                //    lstMsgT.Text = lstMsgB.Text = "Hoàn thành Lưu, bạn hãy nhập thông tin đơn tiếp theo !";
                //    //divCommandT.Visible = divCommandB.Visible = false;
                //}
            }
        }
        private void getTable(int SL)
        {
            //anhvh 05/06/2020
            //lấy các giá trị đã nhập trước đó đổ vào một bảng tạm
            tbl_temp.Columns.Add("TT");
            tbl_temp.Columns.Add("SOHIEUDON");
            tbl_temp.Columns.Add("NGAYNHANDON");
            tbl_temp.Columns.Add("NGAYGHIDON");
            tbl_temp.Columns.Add("HINHTHUCDON");
            tbl_temp.Columns.Add("SOCV");
            tbl_temp.Columns.Add("NGAYCV");
            tbl_temp.Columns.Add("LOAIDONVI");
            tbl_temp.Columns.Add("DONVIGUI");
            tbl_temp.Columns.Add("DONVIGUI_TEXT");
            tbl_temp.AcceptChanges();
            foreach (DataGridItem Item in dgList.Items)
            {
                TextBox txtSohieudon = (TextBox)Item.FindControl("txtSohieudon");
                TextBox txtNgaynhandon = (TextBox)Item.FindControl("txtNgaynhandon");
                TextBox txtNgayghitrendon = (TextBox)Item.FindControl("txtNgayghitrendon");
                CheckBox chkIsCV = (CheckBox)Item.FindControl("chkIsCV");
                TextBox txtSoCV = (TextBox)Item.FindControl("txtSoCV");
                TextBox txtNgayCV = (TextBox)Item.FindControl("txtNgayCV");
                CheckBox chkIsTrongnganh = (CheckBox)Item.FindControl("chkIsTrongnganh");
                DropDownList ddlDonvi = (DropDownList)Item.FindControl("ddlDonvi");
                TextBox txtDonvi = (TextBox)Item.FindControl("txtDonvi");
                DataRow r_tpm = tbl_temp.NewRow();
                r_tpm["TT"] = Item.Cells[0].Text;
                r_tpm["SOHIEUDON"] = txtSohieudon.Text;
                r_tpm["NGAYNHANDON"] = txtNgaynhandon.Text;
                r_tpm["NGAYGHIDON"] = txtNgayghitrendon.Text;
                r_tpm["HINHTHUCDON"] = Convert.ToString(Convert.ToInt32(chkIsCV.Checked));
                r_tpm["SOCV"] = txtSoCV.Text;
                r_tpm["NGAYCV"] = txtNgayCV.Text;
                r_tpm["LOAIDONVI"] = Convert.ToString(Convert.ToInt32(chkIsTrongnganh.Checked));
                r_tpm["DONVIGUI"] = ddlDonvi.SelectedValue;
                r_tpm["DONVIGUI_TEXT"] = txtDonvi.Text;
                tbl_temp.Rows.Add(r_tpm);
            }
            tbl_temp.AcceptChanges();
            ////////////////////////
            DataTable obj = new DataTable();
            obj.Columns.Add("TT");
            obj.Columns.Add("SOHIEUDON");
            obj.Columns.Add("NGAYNHANDON");
            obj.Columns.Add("NGAYGHIDON");
            obj.Columns.Add("HINHTHUCDON");
            obj.Columns.Add("SOCV");
            obj.Columns.Add("NGAYCV");
            obj.Columns.Add("LOAIDONVI");
            obj.Columns.Add("DONVIGUI");
            obj.AcceptChanges();
            for (int i = 1; i <= SL; i++)
            {
                DataRow r = obj.NewRow();
                r["TT"] = i.ToString();
                r["HINHTHUCDON"] = "0";
                r["LOAIDONVI"] = "0";
                obj.Rows.Add(r);
            }
            obj.AcceptChanges();
            dgList.DataSource = obj;
            dgList.DataBind();
        }
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DropDownList ddlDonvi = (DropDownList)e.Item.FindControl("ddlDonvi");
                    ddlDonvi.DataSource = lstDonVi;
                    ddlDonvi.DataTextField = "TEN";
                    ddlDonvi.DataValueField = "ID";
                    ddlDonvi.DataBind();
                    //anhvh 05/06/2020---------lấy lại các giá trị đã nhập trước đó
                    if (e.Item.ItemIndex <tbl_temp.Rows.Count)
                    { 
                        TextBox txtSohieudon = (TextBox)e.Item.FindControl("txtSohieudon");
                        TextBox txtNgaynhandon = (TextBox)e.Item.FindControl("txtNgaynhandon");
                        TextBox txtNgayghitrendon = (TextBox)e.Item.FindControl("txtNgayghitrendon");
                        CheckBox chkIsCV = (CheckBox)e.Item.FindControl("chkIsCV");
                        TextBox txtSoCV = (TextBox)e.Item.FindControl("txtSoCV");
                        TextBox txtNgayCV = (TextBox)e.Item.FindControl("txtNgayCV");
                        CheckBox chkIsTrongnganh = (CheckBox)e.Item.FindControl("chkIsTrongnganh");
                        TextBox txtDonvi = (TextBox)e.Item.FindControl("txtDonvi");
                        //chuyển từ bảng tạm vào 
                        txtSohieudon.Text = tbl_temp.Rows[e.Item.ItemIndex][1].ToString();
                        txtNgaynhandon.Text = tbl_temp.Rows[e.Item.ItemIndex][2].ToString();
                        txtNgayghitrendon.Text = tbl_temp.Rows[e.Item.ItemIndex][3].ToString();
                        chkIsCV.Checked = Convert.ToBoolean(Convert.ToInt32(tbl_temp.Rows[e.Item.ItemIndex][4].ToString()));
                        txtSoCV.Text = tbl_temp.Rows[e.Item.ItemIndex][5].ToString();
                        txtNgayCV.Text = tbl_temp.Rows[e.Item.ItemIndex][6].ToString();
                        chkIsTrongnganh.Checked = Convert.ToBoolean(Convert.ToInt32(tbl_temp.Rows[e.Item.ItemIndex][7].ToString()));
                        ddlDonvi.SelectedValue = tbl_temp.Rows[e.Item.ItemIndex][8].ToString();
                        txtDonvi.Text = tbl_temp.Rows[e.Item.ItemIndex][9].ToString();
                        if (chkIsCV.Checked==true)
                        {
                            txtSoCV.Visible = true;
                            txtNgayCV.Visible = true;
                            chkIsTrongnganh.Visible = true;
                            txtDonvi.Visible = true;
                            ddlDonvi.Visible = false;
                            if (chkIsTrongnganh.Checked == true)
                            {
                                txtDonvi.Visible = false;
                                ddlDonvi.Visible = true;
                            }
                            else if (chkIsTrongnganh.Checked == false)
                            {
                                txtDonvi.Visible = true;
                                ddlDonvi.Visible = false;
                            }
                        }
                        else if (chkIsCV.Checked==false)
                        {
                            txtSoCV.Visible = false;
                            txtNgayCV.Visible = false;
                            chkIsTrongnganh.Visible = false;
                            txtDonvi.Visible = false;
                            ddlDonvi.Visible = false;
                        }
                    }
                    //---------lấy lại các giá trị đã nhập trước đó end
                }
                catch (Exception ex) { }
            }
        }
        protected void ddlSoLuong_SelectedIndexChanged(object sender, EventArgs e)
        {
            DM_TOAAN_BL oTABL = new DM_TOAAN_BL();
            lstDonVi = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);
            if (ddlSoLuong.SelectedValue == "0")
                dgList.Visible = false;
            else
            {
                getTable(Convert.ToInt32(ddlSoLuong.SelectedValue));
                dgList.Visible = true;
            }
        }
        public bool GetNumber(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            { return false; }
        }

        protected void chkIsCV_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            string ID = chk.ToolTip;
            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chkIsCV = (CheckBox)Item.FindControl("chkIsCV");
                TextBox txtSoCV = (TextBox)Item.FindControl("txtSoCV");
                TextBox txtNgayCV = (TextBox)Item.FindControl("txtNgayCV");
                CheckBox chkIsTrongnganh = (CheckBox)Item.FindControl("chkIsTrongnganh");
                TextBox txtDonvi = (TextBox)Item.FindControl("txtDonvi");
                DropDownList ddlDonvi = (DropDownList)Item.FindControl("ddlDonvi");
                if (chk.ToolTip == chkIsCV.ToolTip)
                {
                    if (chk.Checked)
                    {
                        txtSoCV.Visible = true;
                        txtNgayCV.Visible = true;
                        chkIsTrongnganh.Visible = true;
                        txtDonvi.Visible = true;
                        ddlDonvi.Visible = false;
                    }
                    else
                    {
                        txtSoCV.Visible = false;
                        txtNgayCV.Visible = false;
                        chkIsTrongnganh.Visible = false;
                        txtDonvi.Visible = false;
                        ddlDonvi.Visible = false;
                    }
                }
            }
        }
        protected void chkIsTrongnganh_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            string ID = chk.ToolTip;
            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chkIsTrongnganh = (CheckBox)Item.FindControl("chkIsTrongnganh");
                TextBox txtDonvi = (TextBox)Item.FindControl("txtDonvi");
                DropDownList ddlDonvi = (DropDownList)Item.FindControl("ddlDonvi");
                if (chk.ToolTip == chkIsTrongnganh.ToolTip)
                {
                    if (chk.Checked)
                    {
                        txtDonvi.Visible = false;
                        ddlDonvi.Visible = true;
                    }
                    else
                    {
                        txtDonvi.Visible = true;
                        ddlDonvi.Visible = false;
                    }
                }
            }
        }
        protected void ddlSonguoiKN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSonguoiKN.SelectedValue == "0")
                dgNguoiKN.Visible = false;
            else
            {
                if (hddID.Value == "" || hddID.Value == "0")
                    getNguoiKN(Convert.ToInt32(ddlSonguoiKN.SelectedValue), false);
                else
                    getNguoiKN(Convert.ToInt32(ddlSonguoiKN.SelectedValue), true);
                dgNguoiKN.Visible = true;
            }
        }
        private void getNguoiKN(int SL, bool isUpdate)
        {
            DataTable obj = new DataTable();
            obj.Columns.Add("ID");
            obj.Columns.Add("HOTEN");
            obj.Columns.Add("DIACHI");
            obj.AcceptChanges();

            if (isUpdate == false)
            {
                for (int i = 1; i <= SL; i++)
                {
                    DataRow r = obj.NewRow();
                    r["ID"] = "0";
                    r["HOTEN"] = "";
                    r["DIACHI"] = "";
                    obj.Rows.Add(r);
                }
                obj.AcceptChanges();
            }
            else
            {
                foreach (DataGridItem item in dgNguoiKN.Items)
                {
                    DataRow r = obj.NewRow();
                    TextBox txtHoten = (TextBox)item.FindControl("txtHoten");
                    TextBox txtDiachi = (TextBox)item.FindControl("txtDiachi");
                    string strKNID = item.Cells[0].Text;
                    if (txtHoten.Text.Trim() != "")
                    {
                        r["ID"] = strKNID;
                        r["HOTEN"] = txtHoten.Text;
                        r["DIACHI"] = txtDiachi.Text;
                        obj.Rows.Add(r);
                        obj.AcceptChanges();
                    }
                }
                if (SL > dgNguoiKN.Items.Count)
                {
                    for (int i = dgNguoiKN.Items.Count + 1; i <= SL; i++)
                    {
                        DataRow r = obj.NewRow();
                        r["ID"] = "0";
                        r["HOTEN"] = "";
                        r["DIACHI"] = "";
                        obj.Rows.Add(r);
                        obj.AcceptChanges();
                    }
                }
            }
            dgNguoiKN.DataSource = obj;
            dgNguoiKN.DataBind();
        }
        protected void ddlCAChidao_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCAChidao.SelectedValue == "0")
            {
                trChidao.Visible = false;
            }
            else
            {

                trChidao.Visible = true;

            }
        }

        protected void chkKN_TBTLD_CheckedChanged(object sender, EventArgs e)
        {
            if (chkKN_TBTLD.Checked)
            {
                trThongbaoTLD.Visible = true;
                if (dgTBTLD.Items.Count == 0)
                    getTBTLD(1, false);
            }
            else
            {
                trThongbaoTLD.Visible = false;
            }
            Cls_Comon.SetFocus(this, this.GetType(), txtSoQDBA.ClientID);
        }
        protected void rdbThuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            GDTTT_DON_BL oBL = new GDTTT_DON_BL();
            if (rdbThuLy.SelectedValue != "2" && (hddID.Value == "0" || txtSoThuLy.Text == ""))
            {
                txtSoThuLy.Text = oBL.TL_GETMAXTT(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), DateTime.Now.Year, Convert.ToDecimal(ddlLoaiAn.SelectedValue)).ToString();
            }
            if (rdbThuLy.SelectedValue == "2")
                pnThuLy.Visible = false;
            else
                pnThuLy.Visible = true;
            Cls_Comon.SetFocus(this, this.GetType(), rdbThuLy.ClientID);
        }

        protected void dgDSDonTrung_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Sua":
                    hddID.Value = e.CommandArgument.ToString();
                    LoadInfo(Convert.ToDecimal(hddID.Value), false);
                    txtNoidung.Focus();
                    break;
                case "Xoa":
                    decimal ID = Convert.ToDecimal(e.CommandArgument.ToString());
                    GDTTT_DON_BL oBL = new GDTTT_DON_BL();
                    GDTTT_DON d = dt.GDTTT_DON.Where(x => x.ID == ID).FirstOrDefault();
                    decimal DontrungID = d.DONTRUNGID == null ? 0 : (decimal)d.DONTRUNGID;
                    if (DontrungID > 0)
                    {
                        d.DONTRUNGID = 0;
                        d.SOLUONGDON = 1;
                        dt.SaveChanges();
                        oBL.UPDATESOLUONGDON(DontrungID);
                        LoadDSTrung();
                    }
                    else //Đơn Hủy chính là đơn gốc
                    {
                        d.SOLUONGDON = 1;
                        dt.SaveChanges();
                        List<GDTTT_DON> lstT = dt.GDTTT_DON.Where(x => x.DONTRUNGID == ID).OrderByDescending(x => x.NGAYTAO).ToList();
                        if (lstT.Count == 1)
                        {
                            d.SOLUONGDON = 1;
                            lstT[0].SOLUONGDON = 1;
                            lstT[0].DONTRUNGID = 0;
                            dt.SaveChanges();
                            LoadDSTrung();
                        }
                        else if (lstT.Count > 1)
                        {
                            GDTTT_DON dtGoc = lstT[0];
                            dtGoc.SOLUONGDON = 1;
                            dtGoc.DONTRUNGID = 0;
                            foreach (GDTTT_DON dt in lstT)
                            {
                                if (dt.ID != dtGoc.ID)
                                {
                                    dt.SOLUONGDON = 1;
                                    dt.DONTRUNGID = dtGoc.ID;
                                }
                            }
                            dt.SaveChanges();
                            oBL.UPDATESOLUONGDON(dtGoc.ID);
                            LoadDSTrung();
                        }
                    }
                    break;
            }
        }
        protected void ddlSoTBTLD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hddID.Value == "" || hddID.Value == "0")
                getTBTLD(Convert.ToInt32(ddlSoTBTLD.SelectedValue), false);
            else
                getTBTLD(Convert.ToInt32(ddlSoTBTLD.SelectedValue), true);
        }

        private void getTBTLD(int SL, bool isUpdate)
        {
            try
            {
                DataTable obj = new DataTable();
                obj.Columns.Add("ID");
                obj.Columns.Add("SO");
                obj.Columns.Add("NGAY", Type.GetType("System.DateTime"));
                obj.Columns.Add("DONVITB");
                obj.AcceptChanges();

                if (isUpdate == false)
                {
                    for (int i = 1; i <= SL; i++)
                    {
                        DataRow r = obj.NewRow();
                        r["ID"] = "0";
                        r["SO"] = "";
                        // r["NGAY"] = null;
                        r["DONVITB"] = "0";
                        obj.Rows.Add(r);
                    }
                    obj.AcceptChanges();
                }
                else
                {
                    foreach (DataGridItem item in dgTBTLD.Items)
                    {
                        DataRow r = obj.NewRow();
                        TextBox txtSo = (TextBox)item.FindControl("txtSo");
                        TextBox txtNgay = (TextBox)item.FindControl("txtNgay");
                        DropDownList ddlToaAn = (DropDownList)item.FindControl("ddlToaAn");
                        string strKNID = item.Cells[0].Text;
                        if (txtSo.Text.Trim() != "")
                        {
                            r["ID"] = strKNID;
                            r["SO"] = txtSo.Text;
                            DateTime? dNgay = (String.IsNullOrEmpty(txtNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                            if (dNgay != null)
                                r["NGAY"] = dNgay;
                            r["DONVITB"] = ddlToaAn.SelectedValue;
                            obj.Rows.Add(r);
                            obj.AcceptChanges();
                        }
                    }
                    if (SL > obj.Rows.Count)
                    {
                        for (int i = obj.Rows.Count + 1; i <= SL; i++)
                        {
                            DataRow r = obj.NewRow();
                            r["ID"] = "0";
                            r["SO"] = "";

                            r["DONVITB"] = "0";
                            obj.Rows.Add(r);
                            obj.AcceptChanges();
                        }
                    }
                }
                dgTBTLD.DataSource = obj;
                dgTBTLD.DataBind();
            }
            catch (Exception ex) { }
        }
        protected void dgTBTLD_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
               
                try
                {
                   
                    DropDownList ddlToaAn = (DropDownList)e.Item.FindControl("ddlToaAn");
                    HiddenField hddToaID = (HiddenField)e.Item.FindControl("hddToaID");

                    ddlToaAn.ToolTip = e.Item.Cells[0].Text;
                    ddlToaAn.DataSource = dt.DM_TOAAN.Where(x => (x.LOAITOA == "CAPCAO" || x.LOAITOA == "QSTRUNGUONG"||x.LOAITOA == "TOICAO") && x.HIEULUC == 1).ToList();
                    ddlToaAn.DataTextField = "TEN";
                    ddlToaAn.DataValueField = "ID";
                    ddlToaAn.DataBind();
                    ddlToaAn.Items.Insert(0, new ListItem("----", "0"));
                    if (hddToaID.Value != "" && hddToaID.Value != "0") ddlToaAn.SelectedValue = hddToaID.Value;
                }
                catch (Exception ex) { }
            }
        }

        protected void lbtGYNoidung_Click(object sender, EventArgs e)
        {
            if (pnBAQDGDT.Visible)
            {
                if (rdbBAQD.SelectedValue == "0" && txtSoQDBA.Text != "") //Số bản án/QĐ
                {
                    string strND = "khiếu nại Bản án/Quyết định số";
                    strND += " " + txtSoQDBA.Text + " ngày " + txtNgayBA.Text + " của ";
                    if (ddlToaXetXu.SelectedValue != "0")
                        strND += ddlToaXetXu.SelectedItem.Text + ";";
                    if (txtNoidung.Text == "")
                        txtNoidung.Text = strND;
                    else
                        txtNoidung.Text = txtNoidung.Text + "\n" + strND;
                }
                else//KN Quyết định
                {
                    string strND = "khiếu nại Quyết định kháng nghị số";
                    strND += " " + txtSoQDKN.Text + " ngày " + txtNgayKhangNghi.Text + " của ";
                    if (ddlNguoiKhangNghi.SelectedValue != "0")
                        strND += ddlNguoiKhangNghi.SelectedItem.Text + "";
                    strND += " đối với Bản án số " + txtSoQDBA.Text + " ngày " + txtNgayBA.Text + " của ";
                    if (ddlToaXetXu.SelectedValue != "0")
                        strND += ddlToaXetXu.SelectedItem.Text + ";";
                    if (txtNoidung.Text == "")
                        txtNoidung.Text = strND;
                    else
                        txtNoidung.Text = txtNoidung.Text + "\n" + strND + "";
                }
            }
        }
        protected void lbtGYGhichu_Click(object sender, EventArgs e)
        {
            if (trThongbaoTLD.Visible && dgTBTLD.Items.Count > 0)
            {
                string strND = "";
                foreach (DataGridItem item in dgTBTLD.Items)
                {
                    TextBox txtSo = (TextBox)item.FindControl("txtSo");
                    TextBox txtNgay = (TextBox)item.FindControl("txtNgay");
                    DropDownList ddlToaAn = (DropDownList)item.FindControl("ddlToaAn");
                    if (strND == "")
                    {
                        strND = "Thông báo giải quyết đơn số " + txtSo.Text + " ngày " + txtNgay.Text;
                        if (ddlToaAn.SelectedIndex > 0)
                            strND += " của " + ddlToaAn.SelectedItem.Text + ".";
                    }
                    else
                    {
                        strND += "\n" + "Thông báo giải quyết đơn số " + txtSo.Text + " ngày " + txtNgay.Text;
                        if (ddlToaAn.SelectedIndex > 0)
                            strND += " của " + ddlToaAn.SelectedItem.Text + ".";
                    }
                }

                if (txtGhichu.Text == "")
                    txtGhichu.Text = strND;
                else
                    txtGhichu.Text = txtGhichu.Text + "\n" + strND;
            }
        }

        protected void lbtGYChidao_Click(object sender, EventArgs e)
        {
            string strND = "Thực hiện ý kiến chỉ đạo của đồng chí ";
            try
            {
                decimal IDCB = Convert.ToDecimal(ddlCAChidao.SelectedValue);
                DM_CANBO oLD = dt.DM_CANBO.Where(x => x.ID == IDCB).FirstOrDefault();
                if (oLD.CHUCVUID == null || oLD.CHUCVUID == 0)
                {
                    strND += oLD.HOTEN + ", Thẩm phán Tòa án nhân dân tối cao";
                }
                else
                {
                    DM_DATAITEM oCV = dt.DM_DATAITEM.Where(x => x.ID == oLD.CHUCVUID).FirstOrDefault();
                    if (oCV.MA == ENUM_CHUCVU.CHUCVU_PCA)
                    {
                        strND += oLD.HOTEN + ", Phó Chánh án Tòa án nhân dân tối cao";
                    }
                    else
                        strND += "Chánh án Tòa án nhân dân tối cao";
                }
            }
            catch (Exception ex) { }
            strND += ", Văn phòng Tòa án nhân dân tối cao chuyển";
            if (ddlHinhthucdon.SelectedValue == "1")
                strND += " đơn nêu trên";
            else if (ddlHinhthucdon.SelectedValue == "2")
                strND += " Công văn nêu trên của " + txtCV_DonViGuiNgoaiNganh.Text;
            else if (ddlHinhthucdon.SelectedValue == "3")
                strND += " đơn nêu trên";
            if (rdbLoaichuyen.SelectedValue == "0")
                strND += " đến " + ddlChuyendenDV.SelectedItem.Text;
            else if (rdbLoaichuyen.SelectedValue == "1")
                strND += " đến " + ddlToaKhac.SelectedItem.Text;
            else if (rdbLoaichuyen.SelectedValue == "2")
                strND += " đến " + txtNgoaitoaan.Text;
            txtCA_Noidung.Text = strND;
        }
        public void Load_DonTrung()
        {
            GDTTT_DON_BL bl = new GDTTT_DON_BL();
            string strSoBAQD = txtSoQDBA.Text;
            string strNgayBAQD = txtNgayBA.Text;
            string strToaID = ddlToaXetXu.SelectedValue;
            decimal Currid = 0;
            if (rdbBAQD.SelectedValue == "1")
            {
                strSoBAQD = txtSoQDKN.Text;
                strNgayBAQD = txtNgayKhangNghi.Text;
                strToaID = ddlNguoiKhangNghi.SelectedValue;
            }
            if (strSoBAQD == "" && strNgayBAQD == "")
            {
                txtTenDonTrung.Text = "Chưa nhập thông tin bản án !";
                trDSTrung.Visible = false;
                return;
            }
            DataTable tbl = bl.GDTTT_DON_GETDONTRUNG(Currid, txtTenDonTrung.Text, strSoBAQD, strNgayBAQD, strToaID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                #region "Xác định số lượng trang"
                int total = tbl.Rows.Count;
                hddTotalPage.Value = Cls_Comon.GetTotalPage(total, Convert.ToInt32(dgListTrung.PageSize)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + total + " </b> đơn có thể trùng !";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
                trDSTrung.Visible = true;
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có đơn trùng !";
                trDSTrung.Visible = false;
                txtTenDonTrung.Text = "Không có đơn trùng";
            }

            dgListTrung.DataSource = tbl;
            dgListTrung.DataBind();
        }

        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                dgListTrung.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                Load_DonTrung();
            }
            catch (Exception ex)
            {
                lstMsgT.Text = lstMsgB.Text = ex.Message;
            }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                dgListTrung.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                Load_DonTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                dgListTrung.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                Load_DonTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                dgListTrung.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                Load_DonTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                dgListTrung.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
                hddPageIndex.Value = lbCurrent.Text;
                Load_DonTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        #endregion

        protected void dgListTrung_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Select":
                    hddDontrungID.Value = e.CommandArgument.ToString();
                    txtTenDonTrung.Text = "Ngày nhận: " + e.Item.Cells[2].Text + "; người gửi: " + e.Item.Cells[3].Text + ", số BAQĐ: " + e.Item.Cells[4].Text + ", ngày BAQĐ: " + e.Item.Cells[5].Text;
                    LoadInfo(Convert.ToDecimal(hddDontrungID.Value), true);
                    trDSTrung.Visible = false;
                    txtTenDonTrung.Enabled = true;
                    txtSohieudon.Text = txtNgaynhandon.Text = txtNgayghidon.Text = "";
                    txtSohieudon.Focus();
                    cmdCheckTrungDon.Visible = false;
                    cmdHuyTrungDon.Visible = true;
                    break;
            };
        }

        protected void cmdCheckTrungDon_Click(object sender, EventArgs e)
        {
            lstMsgT.Text = lstMsgB.Text = "";
            Load_DonTrung();
            if (trDSTrung.Visible == false)
                txtSohieudon.Focus();
            else
                dgListTrung.Focus();
        }
        protected void cmdHuyTrungDon_Click(object sender, EventArgs e)
        {
            hddDontrungID.Value = "0";
            txtTenDonTrung.Text = "";
            txtTenDonTrung.Enabled = true;
            cmdCheckTrungDon.Visible = true;
            cmdHuyTrungDon.Visible = false;
        }


        #region "Phân trang đơn trùng"
        protected void lbTBackDT_Click(object sender, EventArgs e)
        {
            try
            {
                dgDSDonTrung.CurrentPageIndex = Convert.ToInt32(hddPageIndexDT.Value) - 2;
                hddPageIndexDT.Value = (Convert.ToInt32(hddPageIndexDT.Value) - 1).ToString();
                LoadDSTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTFirstDT_Click(object sender, EventArgs e)
        {
            try
            {
                dgDSDonTrung.CurrentPageIndex = 0;
                hddPageIndexDT.Value = "1";
                LoadDSTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTLastDT_Click(object sender, EventArgs e)
        {
            try
            {
                dgDSDonTrung.CurrentPageIndex = Convert.ToInt32(hddTotalPageDT.Value) - 1;
                hddPageIndexDT.Value = Convert.ToInt32(hddTotalPageDT.Value).ToString();
                LoadDSTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTNextDT_Click(object sender, EventArgs e)
        {
            try
            {
                dgDSDonTrung.CurrentPageIndex = Convert.ToInt32(hddPageIndexDT.Value);
                hddPageIndexDT.Value = (Convert.ToInt32(hddPageIndexDT.Value) + 1).ToString();
                LoadDSTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        protected void lbTStepDT_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                dgDSDonTrung.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
                hddPageIndexDT.Value = lbCurrent.Text;
                LoadDSTrung();
            }
            catch (Exception ex) { lstMsgT.Text = lstMsgB.Text = ex.Message; }
        }
        #endregion

        protected void ddlToaXetXu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlToaXetXu.SelectedValue != "0")
            {
                decimal ToaAnID = Convert.ToDecimal(ddlToaXetXu.SelectedValue);
                DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == ToaAnID).FirstOrDefault();
                if (oTA.LOAITOA == "CAPHUYEN")
                {
                    LoadCapXetXu(ENUM_GIAIDOANVUAN.SOTHAM);
                    ddlCapXetXu.SelectedValue = ENUM_GIAIDOANVUAN.SOTHAM.ToString();
                    //-------
                    pnPhucTham.Visible = pnAnST.Visible = false;
                }
                else if (oTA.LOAITOA == "CAPTINH")
                {
                    LoadCapXetXu(ENUM_GIAIDOANVUAN.PHUCTHAM);
                    ddlCapXetXu.SelectedValue = ENUM_GIAIDOANVUAN.PHUCTHAM.ToString();
                    //-------
                    pnPhucTham.Visible = false;
                    pnAnST.Visible = true;
                    LoadDropToaST_TheoPT(Convert.ToDecimal(ddlToaXetXu.SelectedValue));
                }
                else if (oTA.LOAITOA == "CAPCAO")
                {
                    LoadCapXetXu(ENUM_GIAIDOANVUAN.THULYGDT);
                    ddlCapXetXu.SelectedValue = ENUM_GIAIDOANVUAN.THULYGDT.ToString();
                    //-------
                    pnPhucTham.Visible = pnAnST.Visible = true;
                    LoadDropToaPT_TheoGDT(Convert.ToDecimal(ddlToaXetXu.SelectedValue));
                    LoadDropToaST_Full_ST();
                }
            }
            Cls_Comon.SetFocus(this, this.GetType(), ddlToaXetXu.ClientID);
        }

        protected void dgTBTLD_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Xoa":
                    try
                    {
                        decimal TBID = Convert.ToDecimal(e.CommandArgument.ToString());
                        decimal DonID = Convert.ToDecimal(hddID.Value);
                        GDTTT_DON_TBTLD oTB = dt.GDTTT_DON_TBTLD.Where(x => x.ID == TBID).FirstOrDefault();
                        dt.GDTTT_DON_TBTLD.Remove(oTB);
                        dt.SaveChanges();
                        LoadTBTLD(DonID);
                    }
                    catch (Exception ex)
                    { }
                    break;
            }
        }

        private void chkIsNotGDTChange()
        {
            if (chkIsNotGDT.Checked || rdbLoaichuyen.SelectedValue != "0")
            {
                spSOBA.Visible = spSOKN.Visible = spTOAXX.Visible = spNGBA.Visible = spNGKN.Visible = false;
            }
            else
            {
                spSOBA.Visible = spSOKN.Visible = spTOAXX.Visible = spNGBA.Visible = spNGKN.Visible = true;
            }
        }
        protected void chkIsNotGDT_CheckedChanged(object sender, EventArgs e)
        {
            chkIsNotGDTChange();
            Cls_Comon.SetFocus(this, this.GetType(), txtSoQDBA.ClientID);
        }
        protected void chkIsTuHinh_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsTuHinh.Checked)
            {
                chkIsAnGiam.Visible = true;
                chkKeuOan.Visible = true;
            }
            else
            {
                chkIsAnGiam.Visible = false;
                chkKeuOan.Visible = false;
            }
        }
        private void LoadIsTuHinh(string strLoaiAn)
        {
            if (strLoaiAn == ENUM_LOAIVUVIEC.AN_HINHSU)
            {
                trTuHinh.Visible = true;
            }
            else
                trTuHinh.Visible = false;
        }

        protected void dgDS_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Xoa":
                    decimal ID = Convert.ToDecimal(e.CommandArgument.ToString());
                    GDTTT_DON_BOSUNG oT = dt.GDTTT_DON_BOSUNG.Where(x => x.ID == ID).FirstOrDefault();
                    dt.GDTTT_DON_BOSUNG.Remove(oT);
                    dt.SaveChanges();
                    string current_id = Request["ID"] + "";
                    LoadBoSungTL(Convert.ToDecimal(current_id));
                    break;
            }
        }
        protected void cmdThugon_Click(object sender, EventArgs e)
        {
            trDSTrung.Visible = false;
        }

    }
}