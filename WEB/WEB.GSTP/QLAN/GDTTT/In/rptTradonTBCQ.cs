﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.QLAN.GDTTT.In
{
    public partial class rptTradonTBCQ : DevExpress.XtraReports.UI.XtraReport
    {
        public rptTradonTBCQ()
        {
            InitializeComponent();
        }
        private string getTenToa(string strTenToa)
        {
            try
            {
                if (strTenToa.Contains("CẤP CAO"))
                {
                    strTenToa = strTenToa.Replace("CẤP CAO", "CẤP CAO\n");
                }

                return strTenToa;
            }
            catch { return ""; }
        }
        private void xrTableCell12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell12.Text = getTenToa(xrTableCell12.Text.ToUpper());
        }

        private void xrTable3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string strNoidung = GetCurrentColumnValue("DIACHICOQUAN") + "";
            if (strNoidung == "")
            {
                xrTableRow1.Visible = false;
            }
        }
    }
}
