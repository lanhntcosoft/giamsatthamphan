﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.QLAN.GDTTT.In
{
    public partial class rptNTAPhieuchuyen : DevExpress.XtraReports.UI.XtraReport
    {
        public rptNTAPhieuchuyen()
        {
            InitializeComponent();
        }
        private string getTenToa(string strTenToa)
        {
            try
            {
                if (strTenToa.Contains("CẤP CAO"))
                {
                    strTenToa = strTenToa.Replace("CẤP CAO", "CẤP CAO\n");
                }

                return strTenToa;
            }
            catch { return ""; }
        }
        private void xrTableCell12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell12.Text = getTenToa(xrTableCell12.Text.ToUpper());
        }

        private void xrTable3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string strLoaiAN = GetCurrentColumnValue("CHIDAO_COKHONG") + "";
            if (strLoaiAN == "1")
            {
                xrTableRow2.Visible = true;
                xrTableRow6.Visible = false;
                xrTableRow5.Visible = true;
            }
            else
            {
                xrTableRow2.Visible = false;
                xrTableRow6.Visible = true;
                xrTableRow5.Visible = false;
            }
            string strND = GetCurrentColumnValue("NOIDUNGCHIDAO") + "";
            if (strND == "") xrTableRow5.Visible = false;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string strLoaiAN = GetCurrentColumnValue("CHIDAO_COKHONG") + "";
            if (strLoaiAN == "1")
            {
                xrTableRow2.Visible = true;
                xrTableRow6.Visible = false;
            }
            else
            {
                xrTableRow2.Visible = false;
                xrTableRow6.Visible = true;
            }
            string strND= GetCurrentColumnValue("NOIDUNGCHIDAO") + "";
            if (strND == "") xrTableRow5.Visible = false;

        }
    }
}
