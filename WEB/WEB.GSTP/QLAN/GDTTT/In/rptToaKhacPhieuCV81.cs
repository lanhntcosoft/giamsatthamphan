﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WEB.GSTP.QLAN.GDTTT.In
{
    public partial class rptToaKhacPhieuCV81 : DevExpress.XtraReports.UI.XtraReport
    {
        public rptToaKhacPhieuCV81()
        {
            InitializeComponent();
        }

        private void xrTableCell12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell12.Text = xrTableCell12.Text.ToUpper();
        }
    }
}
