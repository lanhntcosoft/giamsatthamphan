﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.GDTTT;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.GDTTT.VuAn
{
    public partial class DanhsachAnCV : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        private const decimal ROOT = 0;
        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            { return false; }
        }
        public string GetDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return Convert.ToDateTime(obj).ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            { return ""; }
        }
        String SessionInBC = "GDTTTVA_INBC_VISIBLE";
        String SessionSearch = "TTTKVISIBLE";
        Decimal CurrUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUserID > 0)
            {
                if (!IsPostBack)
                {
                    //if ((Session[SessionInBC] + "") == "0")
                    //{
                    //    lkInBC_OpenForm.Text = "[ Thu gọn ]";
                    //    pnInBC.Visible = true;
                    //}
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetButton(btnThemmoi, oPer.TAOMOI);
                    //---------------------------
                    LoadDropBox();
                    Load_Data();  
                }
            }
            else
                Response.Redirect("/Login.aspx");
        }

        private void LoadDropBox()
        {
            decimal ToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            decimal PBID = strPBID == "" ? 0 : Convert.ToDecimal(strPBID);

            DM_TOAAN_BL oTABL = new DM_TOAAN_BL();
            DataTable dtTA = oTABL.DM_TOAAN_GETBYNOTCUR(0, 0);
            ddlToaXetXu.DataSource = dtTA;
            ddlToaXetXu.DataTextField = "MA_TEN";
            ddlToaXetXu.DataValueField = "ID";
            ddlToaXetXu.DataBind();
            ddlToaXetXu.Items.Insert(0, new ListItem("--- Chọn --- ", "0"));

            //GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            ////Thẩm tra viên
            //DataTable oTTVDT = oGDTBL.CANBO_GETBYPHONGBAN(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), PBID, ENUM_CHUCDANH.CHUCDANH_TTV);
            //ddlThamtravien.DataSource = oTTVDT;
            //ddlThamtravien.DataTextField = "HOTEN";
            //ddlThamtravien.DataValueField = "ID";
            //ddlThamtravien.DataBind();
            //ddlThamtravien.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
            //SetVisible_SearchDateTime();
            LoadDropTTV_TheoCanBoLogin();
        }


        void LoadDropTTV_TheoCanBoLogin()
        {
            decimal CurrNhomNSDID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID] + "");
            QT_NHOMNGUOIDUNG oGroup = dt.QT_NHOMNGUOIDUNG.Where(x => x.ID == CurrNhomNSDID).Single();
            int loai_hotro_db = (int)oGroup.LOAI;

            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            decimal PBID = strPBID == "" ? 0 : Convert.ToDecimal(strPBID);
            GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault();
            decimal chucdanh_id = (string.IsNullOrEmpty(oCB.CHUCDANHID + "")) ? 0 : Convert.ToDecimal(oCB.CHUCDANHID);
            decimal chucvu_id = (string.IsNullOrEmpty(oCB.CHUCVUID + "")) ? 0 : Convert.ToDecimal(oCB.CHUCVUID);
            if (chucvu_id > 0)
            {
                DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == chucvu_id).FirstOrDefault();
                if (oCD.MA == ENUM_CHUCVU.CHUCVU_PVT)
                {
                    LoadDropTTV_TheoLanhDao(CanboID);
                }
                else
                {
                    LoadAll_TTV();
                }
            }
            else if (chucdanh_id > 0)
            {
                DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == chucdanh_id).Single();
                // if (oCD.MA == ENUM_CHUCDANH.CHUCDANH_TTV || oCD.MA == "TTVC") 
                // 042 la Pho truong phong quyen nhu TTV 
                if ((oCD.MA == ENUM_CHUCDANH.CHUCDANH_TTV || oCD.MA == "042") && loai_hotro_db != 1)
                {
                    ddlThamtravien.Items.Clear();
                    ddlThamtravien.Items.Add(new ListItem(oCB.HOTEN, oCB.ID.ToString()));
                    hddLoaiTK.Value = ENUM_CHUCDANH.CHUCDANH_TTV;
                }
                else LoadAll_TTV();
            }
        }

        void LoadAll_TTV()
        {
            GDTTT_DON_BL oGDTBL = new GDTTT_DON_BL();
            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            decimal PBID = strPBID == "" ? 0 : Convert.ToDecimal(strPBID);
            Decimal LoginDonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);

            //Thẩm tra viên
            DataTable tblTheoPB = oGDTBL.GDTTT_VuAn_GetAllCBTheoPB(LoginDonViID, PBID, ENUM_CHUCDANH.CHUCDANH_TTV);
            ddlThamtravien.DataSource = tblTheoPB;
            ddlThamtravien.DataTextField = "HOTEN";
            ddlThamtravien.DataValueField = "ID";
            ddlThamtravien.DataBind();
            ddlThamtravien.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
        }

        void LoadDropTTV_TheoLanhDao(Decimal LanhDaoID)
        {
            string strPBID = Session[ENUM_SESSION.SESSION_PHONGBANID] + "";
            decimal PhongBanID = strPBID == "" ? 0 : Convert.ToDecimal(strPBID);
            ddlThamtravien.Items.Clear();
            try
            {
                DM_CANBO_BL obj = new DM_CANBO_BL();

                DataTable tbl = obj.DM_CANBO_PB_CHUCDANH(PhongBanID, "TTV", LanhDaoID, 0, 1, 200000);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    ddlThamtravien.DataSource = tbl;
                    ddlThamtravien.DataTextField = "HOTEN";
                    ddlThamtravien.DataValueField = "CanBoID";
                    ddlThamtravien.DataBind();
                    ddlThamtravien.Items.Insert(0, new ListItem("--- Tất cả ---", "0"));
                }
            }
            catch (Exception ex)
            { }
        }

        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            //string StrQLHS = "PopupReport('/QLAN/GDTTT/VuAn/Popup/pQLCongVAn.aspx','Quản lý công văn trao đổi',1000,600);";
            //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrQLHS, true);
            Response.Redirect("EditCVTraoDoi.aspx");
        }
        //-----------------------------------------
        private void Load_Data()
        {
            
            lbtthongbao.Text = "";
            lbTFirst.Visible = ddlPageCount.Visible = lbBFirst.Visible = ddlPageCount2.Visible = true;

            int page_size = Convert.ToInt32(ddlPageCount.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            DataTable oDT = getDS(page_size, pageindex);
            int count_all = 0;
            if (oDT.Rows.Count > 0)
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
            if (oDT != null && count_all > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(ddlPageCount.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString("#,#", cul) + " </b> vụ án trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
                
            }
            dgList.PageSize = Convert.ToInt32(ddlPageCount.SelectedValue);
            dgList.DataSource = oDT;
            dgList.DataBind();
            dgList.Visible = true;
        }
        private DataTable getDS(int page_size, int pageindex)
        {
            decimal vToaAnID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            decimal vPhongbanID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_PHONGBANID]);

            //------------------------------------
            decimal vToaAnChuyenCV_ID = Convert.ToDecimal(ddlToaXetXu.SelectedValue);

            string vSoCV = txtSoCV.Text.Trim();
            DateTime? vNgayCV = txtNgayCV.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayCV.Text, cul, DateTimeStyles.NoCurrentDateDefault);

            string vSoThuLy = txtSoThuLy.Text.Trim();
            DateTime? vNgayThuLy = txtNgayThuLy.Text == "" ? (DateTime?)null : DateTime.Parse(txtNgayThuLy.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                        
            decimal vThamtravien = Convert.ToDecimal(ddlThamtravien.SelectedValue);
            decimal IstoTrinh = Convert.ToDecimal(ddlTotrinh.SelectedValue);

            decimal is_ketqua_gq = Convert.ToDecimal(dropKQ.SelectedValue);
            DateTime? tungay = txtTuNgay.Text == "" ? (DateTime?)null : DateTime.Parse(txtTuNgay.Text, cul, DateTimeStyles.NoCurrentDateDefault);
            DateTime? denngay = txtDenNgay.Text == "" ? (DateTime?)null : DateTime.Parse(txtDenNgay.Text, cul, DateTimeStyles.NoCurrentDateDefault);

            string vNoidung = txtNoidung.Text.Trim();


            GDTTT_VUAN_TRAODOICONGVAN_BL oBL = new GDTTT_VUAN_TRAODOICONGVAN_BL();
            DataTable tbl = null;
            //if (dropKQ.SelectedValue != "1")
            //{
               tbl= oBL.Search(vToaAnID, vPhongbanID, vToaAnChuyenCV_ID
                                        , vSoCV, vNgayCV, vSoThuLy, vNgayThuLy
                                        , vThamtravien, IstoTrinh
                                        , is_ketqua_gq, tungay, denngay, vNoidung
                                        , pageindex, page_size);
            //}else
            //{
            //    tbl = oBL.SearchChuaGQ(vToaAnID, vPhongbanID, vToaAnChuyenCV_ID
            //                           , vSoCV, vNgayCV, vSoThuLy, vNgayThuLy
            //                           , vThamtravien, IstoTrinh
            //                           , denngay
            //                           , pageindex, page_size);
            //}
            return tbl;
        }
    
        protected void cmdTimkiem_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
            //lbtthongbao.Text = "load xong";
        }
        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            Decimal congvan_id = Convert.ToDecimal(e.CommandArgument);
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    //string StrQLHS = "PopupReport('/QLAN/GDTTT/VuAn/Popup/pQLCongVan.aspx?vID="+ congvan_id + "','Thông tin công văn trao đổi',1000,600);";
                    //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrQLHS, true);
                    Response.Redirect("EditCVTraoDoi.aspx?vID="+ congvan_id);
                    break;
                case "Xoa":
                    if (oPer.XOA == false)
                    {
                        lbtthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    Xoa(congvan_id);
                    break;
            }
        }
        void Xoa(Decimal CurrCongVanID)
        {       
            List<GDTTT_TRAODOICV_TOTRINH> lstHS = dt.GDTTT_TRAODOICV_TOTRINH.Where(x => x.CONGVANID == CurrCongVanID).ToList();
            if (lstHS != null && lstHS.Count > 0)
            {
                foreach(GDTTT_TRAODOICV_TOTRINH item in lstHS)
                    dt.GDTTT_TRAODOICV_TOTRINH.Remove(item);
            }
            //-----------------------------
            GDTTT_VUAN_TRAODOICONGVAN oT = dt.GDTTT_VUAN_TRAODOICONGVAN.Where(x => x.ID == CurrCongVanID).Single();
            dt.GDTTT_VUAN_TRAODOICONGVAN.Remove(oT);
            //------------------
            dt.SaveChanges();
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rv = (DataRowView)e.Item.DataItem;  
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                lblSua.Visible = oPer.CAPNHAT;
                lbtXoa.Visible = oPer.XOA;
            }
        }
        #region "Phân trang"

        protected void lbTBack_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            Load_Data();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            // dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            Load_Data();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            // dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            Load_Data();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            //dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            Load_Data();
        }

        protected void ddlPageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount2.SelectedValue = ddlPageCount.SelectedValue;
            //  dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void ddlPageCount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount.SelectedValue = ddlPageCount2.SelectedValue;
            // dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        #endregion
        //---------------CHỨC NĂNG-------------------
        protected void chkChonAll_CheckChange(object sender, EventArgs e)
        {
            CheckBox chkAll = (CheckBox)sender;

            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chk = (CheckBox)Item.FindControl("chkChon");
                chk.Checked = chkAll.Checked;
            }
        }
    

       protected void cmdLammoi_Click(object sender, EventArgs e)
        {
            txtSoCV.Text = "";
            txtNgayCV.Text = "";
            ddlToaXetXu.SelectedIndex = 0;
            txtSoThuLy.Text = "";
            txtNgayThuLy.Text = "";
            ddlThamtravien.SelectedIndex = 0;
            ddlTotrinh.SelectedIndex = 0;
        }
        //protected void lkInBC_OpenForm_Click(object sender, EventArgs e)
        //{            
        //    if (pnInBC.Visible == false)
        //    {
        //        lkInBC_OpenForm.Text = "[ Thu gọn ]";
        //        pnInBC.Visible = true;
        //        Session[SessionInBC] = "0";
        //    }
        //    else
        //    {
        //        lkInBC_OpenForm.Text = "[ Mở ]";
        //        pnInBC.Visible = false;
        //        Session[SessionInBC] = "1";
        //    }
        //}
        protected void cmdPrint_Click(object sender, EventArgs e)
        {
            //String SessionName = "GDTTT_ReportPL".ToUpper();
            ////--------------------------
            //String ReportName = "";
            //if (String.IsNullOrEmpty(txtTieuDeBC.Text))
            //    SetTieuDeBaoCao();
            //ReportName = txtTieuDeBC.Text.Trim();
            //Session[SS_TK.TENBAOCAO] = ReportName.ToUpper();
            //String Parameter = "type=giaiquyetdon&rID=" + dropMauBC.SelectedValue;
            //DataTable tbl = SearchNoPaging();
            //Session[SessionName] = tbl;
            //string URL = "/QLAN/GDTTT/VuAn/BaoCao/ViewBC.aspx" + (string.IsNullOrEmpty(Parameter) ? "" : "?" + Parameter);
            //Cls_Comon.CallFunctionJS(this, this.GetType(), "PopupReport('" + URL + "','In báo cáo',1000,600)");
        }
        private DataTable SearchNoPaging()
        {
            DataTable tbl = null;
            return tbl;
        }
        protected void ddlLoaiAn_SelectedIndexChanged(object sender, EventArgs e)
        {
           
           // LoadDropQHPL();
        }
        protected void ddlPhoVuTruong_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
       
        protected void ddlTotrinh_SelectedIndexChanged(object sender, EventArgs e)
        {
           //SetTieuDeBaoCao();
           // if (ddlTotrinh.SelectedValue != "2")
           //     dropMauBC.SelectedValue = "3";
        }

       
        protected void dropKQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetVisible_SearchDateTime();
        }
        void SetVisible_SearchDateTime()
        {
            string trangthai = dropKQ.SelectedValue;
            if (trangthai == "1")
            {
                txtTuNgay.Enabled = txtDenNgay.Enabled = true;
            }
            else if (trangthai == "0")
            {
                txtTuNgay.Enabled = false;
                txtDenNgay.Enabled = true;
            }
            else
            {
                txtTuNgay.Enabled = txtDenNgay.Enabled = false;
            }
        }
    }
}
