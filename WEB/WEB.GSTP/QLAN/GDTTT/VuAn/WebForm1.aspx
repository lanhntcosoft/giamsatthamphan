﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WEB.GSTP.QLAN.GDTTT.VuAn.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table cellpadding="1" cellspacing="1" style="font-family: times New Roman; font-size: 14pt; text-align: center; border-collapse: collapse;">
            <tr>
                <td style="text-align: center; vertical-align: top; font-size: 11pt">@TENDONVI@ </td>
                <th style="text-align: center; vertical-align: top; font-size: 11pt;">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</th>
            </tr>
            <tr style="text-align: center;">
                <td style="vertical-align: top; font-size: 11pt">
                    <table cellpadding="0" cellspacing="0">
                        <tr style="height: 1pt; padding-bottom: 3px;">
                            <th style="text-align: right; width: 15px;"><span>V</span></th>
                            <th style="border-bottom: 1px solid #000000; text-align: left;">
                                <span>ĂN PHÒN</span>
                            </th>
                            <th style="text-align: left;"><span>G</span></th>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;">
                    <table cellpadding="0" cellspacing="0">
                        <tr style="height: 1pt; padding-bottom: 3px; font-size: 13pt">
                            <th style="width: 30px; text-align: right;"><span>Đ</span></th>
                            <th style="border-bottom: 1px solid #000000; text-align: left;">
                                <span>ộc lập - Tự do - Hạnh ph</span>
                            </th>
                            <th style="text-align: left;"><span>úc</span></th>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-size: 13pt">Số: SO/TTr-@SDFSDF@-VP</td>
                <td style="font-size: 12pt; font-style: italic"><span style="color: #ffffff;">NGÀY THÁNG NĂM</span></td>
            </tr>
            <tr style="padding-top: 3px; font-size: 12pt;">
                <td></td>
                <td></td>
            </tr>
            <tr style="height: 0px;">
                <td style="width: 650pt;"></td>
                <td style="width: 750pt"></td>
            </tr>
        </table>
        <p style="font-size: 14pt; text-align: center; line-height: 100%; font-weight: bold; margin-bottom: 4px;">
            TỜ TRÌNH 
        </p>
        <p style="font-size: 14pt; text-align: center; line-height: 100%; font-weight: bold; margin-top: 3px;">
            Về việc thụ lý đơn và phân công Thẩm phán giải quyết đơn đề nghị<br />
            xem xét lại quyết định, bản án đã có hiệu lực pháp luật<br />
            theo trình tự giám đốc thẩm, tái thẩm<br />
        </p>
        <p style="font-size: 14pt; text-align: center; line-height: 100%; margin-top: 28pt;">
            Kính trình: Đồng chí Chánh án @@@@
        </p>
        <p style="font-size: 14pt; text-align: justify; margin-top: 28pt;"><span style="color: white;">............</span> SADASDASDSDASD </p>
        <p style="font-size: 14pt; text-align: justify"><span style="color: white;">............</span> ẤDASD báo cáo và kính đề nghị đồng chí Chánh án ÁDASD xem xét, cho ý kiến về việc phân công Thẩm phán ÁDASDAS giải quyết đơn.</p>
        <p style="font-size: 14pt; text-align: left; line-height: 100%;">
            <span style="color: white;">............</span>Kính trình Đồng chí./.
        </p>

        <table cellpadding="0" cellspacing="1" style="font-family: times New Roman; font-size: 14pt; text-align: center; border-collapse: collapse;">
            <tr>
                <td style="vertical-align: top;">
                    <p style="font-size: 12pt; text-align: left; line-height: 105%;">
                        <i><b>Nơi nhận:</b></i><br />
                        - Như kính trình;<br />
                        - Lưu: HCTP.<br />

                    </p>
                </td>
                <td>
                    <p style="font-size: 13pt;">
                        <strong>KT. CHÁNH VĂN PHÒNG<br />
                            PHÓ CHÁNH VĂN PHÒNG<br />
                        </strong>
                    </p>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <p style="font-size: 13pt;"><strong>ÁDASD</strong></p>
                </td>
            </tr>
            <tr style="height: 0px;">
                <td style="width: 500pt"></td>
                <td style="width: 500pt;"></td>
            </tr>
        </table>
    </form>
</body>
</html>
