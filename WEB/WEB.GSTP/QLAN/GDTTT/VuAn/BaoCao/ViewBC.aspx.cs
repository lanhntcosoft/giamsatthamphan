﻿using DAL.GSTP;
using Module.Common;
using System;
using System.Data;
using System.Globalization;
using System.Linq;

using BL.GSTP.GDTTT;

namespace WEB.GSTP.QLAN.GDTTT.VuAn.BaoCao
{
    public partial class ViewBC : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        GSTPContext dt = new GSTPContext();
        Decimal CurrentUserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrentUserID == 0)
                Response.Redirect("/Login.aspx");
            else
            {
                LoadReport();
            }
        }
        private void LoadReport()
        {
            Decimal ThamPhanID = 0;
            string TenThamPhan = "";
            DateTime vNgayThulyTu = DateTime.MinValue;
            DateTime vNgayThulyDen = DateTime.MinValue;
            string DenNgay = "", TuNgay = "";
            String ThoiGian = "";
            string report_name = Session[SS_TK.TENBAOCAO] + "";
            String SessionName = "GDTTT_ReportPL".ToUpper();
            DataTable tblData = (DataTable)Session[SessionName];
            string type = (Request["type"] != null) ? Request["type"].ToString() : "";
            int PhongBanID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_PHONGBANID] + "")) ? 0 : Convert.ToInt32(Session[ENUM_SESSION.SESSION_PHONGBANID] + "");
            string loai_report = type.ToLower();
            if (loai_report != "hoso")
            {
                try
                {
                    #region Lay thong tin tham so
                    if (Session[SS_TK.THAMPHAN].ToString() != "0")
                    {
                        ThamPhanID = Convert.ToDecimal(Session[SS_TK.THAMPHAN] + "");
                        TenThamPhan = dt.DM_CANBO.Where(x => x.ID == ThamPhanID).FirstOrDefault().HOTEN.ToUpper();
                    }
                    if (!String.IsNullOrEmpty(Session[SS_TK.THULY_TU] + ""))
                    {
                        vNgayThulyTu = DateTime.Parse(Session[SS_TK.THULY_TU] + "", cul, DateTimeStyles.NoCurrentDateDefault);
                        TuNgay = vNgayThulyTu.ToString("dd/MM/yyyy", cul);
                    }
                    if (!String.IsNullOrEmpty(Session[SS_TK.THULY_DEN] + ""))
                    {
                        vNgayThulyDen = DateTime.Parse(Session[SS_TK.THULY_DEN] + "", cul, DateTimeStyles.NoCurrentDateDefault);
                        DenNgay = vNgayThulyDen.ToString("dd/MM/yyyy", cul);
                    }
                    else DenNgay = DateTime.Now.ToString("dd/MM/yyyy", cul);


                    if (!String.IsNullOrEmpty(TuNgay))
                        ThoiGian = "Tính từ ngày " + TuNgay;
                    if (!String.IsNullOrEmpty(DenNgay))
                    {
                        if (!String.IsNullOrEmpty(ThoiGian))
                            ThoiGian += " đến ngày " + DenNgay;
                        else
                            ThoiGian = "Tính đến ngày " + DenNgay;
                    }

                    if (!String.IsNullOrEmpty(ThoiGian))
                        ThoiGian = "(" + ThoiGian + ")";
                    #endregion 

                }
                catch (Exception ex) { }

                switch (loai_report)
                {
                    case "va":
                        int bm = (Request["rID"] != null) ? Convert.ToInt16(Request["rID"].ToString()) : 0;
                        switch (bm)
                        {
                            case 1:
                                HanhChinh.PL1 bc = new HanhChinh.PL1();
                                bc.Parameters["TieuDeBC"].Value = report_name;
                                bc.Parameters["DenNgay"].Value = ThoiGian;
                                bc.DataSource = tblData;
                                reportcontrol.OpenReport(bc);
                                break;
                            case 2:
                                HanhChinh.PL2 bc2 = new HanhChinh.PL2();
                                bc2.Parameters["TieuDeBC"].Value = report_name;
                                bc2.Parameters["DenNgay"].Value = ThoiGian;
                                bc2.DataSource = tblData;
                                reportcontrol.OpenReport(bc2);
                                break;
                            case 3:
                                HanhChinh.PL3 bc3 = new HanhChinh.PL3();
                                bc3.Parameters["TieuDeBC"].Value = report_name;
                                bc3.Parameters["DenNgay"].Value = ThoiGian;
                                bc3.DataSource = tblData;
                                reportcontrol.OpenReport(bc3);
                                break;
                            case 4:
                                HanhChinh.PL4 bc4 = new HanhChinh.PL4();
                                bc4.Parameters["TieuDeBC"].Value = report_name;
                                bc4.Parameters["DenNgay"].Value = ThoiGian;
                                bc4.DataSource = tblData;
                                reportcontrol.OpenReport(bc4);
                                break;
                            case 5:
                                AnQH.BM5 bc5 = new AnQH.BM5();
                                bc5.Parameters["TieuDeBC"].Value = report_name;
                                bc5.Parameters["DenNgay"].Value = ThoiGian;
                                if (tblData != null && tblData.Rows.Count > 0)
                                {
                                    int count_all = tblData.Rows.Count;
                                    bc5.Parameters["CountAll"].Value = count_all;
                                }
                                else
                                    bc5.Parameters["CountAll"].Value = 0;
                                bc5.DataSource = tblData;
                                reportcontrol.OpenReport(bc5);
                                break;
                        }
                        break;
                    case "xetxugdt":
                        ThoiGian += "  " + "Tổng số: " + ((tblData != null && tblData.Rows.Count > 0) ? tblData.Rows.Count.ToString() : "0") + " vụ";
                        XetXuGDT.DS xx = new XetXuGDT.DS();
                        xx.Parameters["TieuDeBC"].Value = report_name;
                        xx.Parameters["ThoiGian"].Value = ThoiGian;
                        xx.Parameters["TongSo"].Value = "";
                        xx.DataSource = tblData;
                        reportcontrol.OpenReport(xx);
                        break;
                    case "dstotrinh":
                        //In toan bo quan ly HS theo VuAnID
                        ToTrinh.Ds tt = new ToTrinh.Ds();
                        tt.Parameters["TieuDeBC"].Value = report_name;
                        tt.Parameters["ThoiGian"].Value = ThoiGian;
                        tt.DataSource = tblData;
                        reportcontrol.OpenReport(tt);
                        break;
                    case "pc_ttv":
                        DenNgay = DateTime.Now.ToString("dd/MM/yyyy", cul);
                        if (!String.IsNullOrEmpty(DenNgay))
                            ThoiGian = "(Tính từ ngày " + DenNgay + ")";

                        //lay ds phan cong TTV
                        HoSo.PCTTV pc = new HoSo.PCTTV();
                        pc.Parameters["TieuDeBC"].Value = report_name;
                        pc.Parameters["ThoiGian"].Value = ThoiGian;
                        pc.DataSource = tblData;
                        reportcontrol.OpenReport(pc);
                        break;
                    case "hoso_tt":
                        ToTrinh.DSVuAn dsva = new ToTrinh.DSVuAn();
                        dsva.Parameters["TieuDeBC"].Value = report_name;
                        dsva.Parameters["ThoiGian"].Value = ThoiGian;
                        dsva.DataSource = tblData;
                        reportcontrol.OpenReport(dsva);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                string loai_bm = (Request["loai"] != null) ? Request["loai"].ToString() : "";
                switch (loai_bm)
                {
                    case "bm":
                        //in biểu mẫu muon hs cua 1 vu an
                        InHoSo_MotVuAn();
                        break;
                    case "ds":
                        DenNgay = DateTime.Now.ToString("dd/MM/yyyy", cul);
                        if (!String.IsNullOrEmpty(DenNgay))
                            ThoiGian = "(Tính từ ngày " + DenNgay + ")";

                        //In toan bo quan ly HS theo VuAnID
                        HoSo.HS hs = new HoSo.HS();
                        hs.Parameters["TieuDeBC"].Value = report_name.ToUpper();
                        hs.Parameters["ThoiGian"].Value = ThoiGian;
                        hs.DataSource = tblData;
                        reportcontrol.OpenReport(hs);
                        break;
                    case "mHS":
                        //In phieu muon ho so cua nhieu vu an
                        if (PhongBanID == 3)
                        {
                            HoSo.mMuonHS mMuonHS = new HoSo.mMuonHS();
                            mMuonHS.Parameters["TieuDeBC"].Value = report_name.ToUpper();
                            // mMuonHS.Parameters["ThoiGian"].Value = ThoiGian;
                            mMuonHS.DataSource = tblData;
                            reportcontrol.OpenReport(mMuonHS);
                        }
                        else InPhieuMuon_NhieuVuAn();
                        break;

                }
            }
        }
        void InHoSo_MotVuAn()
        {
            String Tab_Character = "     ";
            decimal temp_id = 0;
            string TenPhongBan = "", DiaChiGuiHS = "", Nguyendon = "", bidon = "";
            DateTime now = DateTime.Now;
            string quanhephapluat = "";
            String ToaAnThuLy = "", tenvuan = "", sobanan = "", ngaybanan = "";
            int PhongBanID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_PHONGBANID] + "")) ? 0 : Convert.ToInt32(Session[ENUM_SESSION.SESSION_PHONGBANID] + "");
            DM_PHONGBAN objPB = dt.DM_PHONGBAN.Where(x => x.ID == PhongBanID).Single();
            if (objPB != null)
            {
                TenPhongBan = objPB.TENPHONGBAN;
                DiaChiGuiHS = objPB.HAUTOCV;
            }
            //-----------------------------------
            string tenviettat = "";
            switch (PhongBanID)
            {
                case 2:
                    tenviettat = "GĐKTI";
                    break;
                case 3:
                    tenviettat = "GĐKTII";
                    break;
                case 4:
                    tenviettat = "GĐKTIII";
                    break;
                default:
                    tenviettat = TenPhongBan;
                    break;
            }
            //--------------------------------
            int loai_vu_an = 0;
            String Str_LoaiVuAn = "";
            Decimal hsId = String.IsNullOrEmpty(Request["hsID"] + "") ? 0 : Convert.ToDecimal(Request["hsID"] + "");
            Decimal VuAnId = String.IsNullOrEmpty(Request["vid"] + "") ? 0 : Convert.ToDecimal(Request["vID"] + "");
            if (VuAnId > 0)
            {
                GDTTT_VUAN objVA = dt.GDTTT_VUAN.Where(x => x.ID == VuAnId).Single();
                if (objVA != null)
                {
                    loai_vu_an = (int)objVA.LOAIAN;
                    if (loai_vu_an < 10)
                        Str_LoaiVuAn = "0" + loai_vu_an.ToString();

                    tenvuan = objVA.TENVUAN;
                    temp_id = (string.IsNullOrEmpty(objVA.QHPL_DINHNGHIAID + "")) ? 0 : (decimal)objVA.QHPL_DINHNGHIAID;
                    try
                    {
                        if (temp_id > 0)
                            quanhephapluat = dt.GDTTT_DM_QHPL.Where(x => x.ID == temp_id).Single().TENQHPL;
                    }
                    catch (Exception ex) { }
                    sobanan = objVA.SOANPHUCTHAM;
                    ngaybanan = ((DateTime)objVA.NGAYXUPHUCTHAM).ToString("dd/MM/yyyy", cul);
                    Nguyendon = objVA.NGUYENDON;
                    bidon = objVA.BIDON;

                    DM_TOAAN objTA = dt.DM_TOAAN.Where(x => x.ID == objVA.TOAPHUCTHAMID).Single();
                    ToaAnThuLy = objTA.MA_TEN;
                }
            }
            //----------------------------------
            GDTTT_QUANLYHS objHS = dt.GDTTT_QUANLYHS.Where(x => x.ID == hsId).Single();

            String SoHieu = objHS.SOPHIEU+"";
            DateTime ngay_hs = Convert.ToDateTime(objHS.NGAYTAO + "");
            String StrThoiGian_DiaDiem = "Hà Nội, ngày " + ngay_hs.Day.ToString() + " tháng " + ngay_hs.Month.ToString() + " năm " + ngay_hs.Year.ToString();

            HoSo.mHoSoV3 mhs_v3 = null;
            HoSo.mHoSo mhs = null;
            if (PhongBanID == 4)
            {
                mhs_v3 = new HoSo.mHoSoV3();
                mhs_v3.Parameters["TenDonVi"].Value = TenPhongBan.ToUpper();
                mhs_v3.Parameters["DonViLuu"].Value = "Vụ " + tenviettat + "-TANDTC";

                mhs_v3.Parameters["SoThongBao"].Value = "...../PM/TANDTC-" + tenviettat;
                mhs_v3.Parameters["DiaDiem"].Value = StrThoiGian_DiaDiem;

                mhs_v3.Parameters["CanCuBoLuatTT"].Value = Tab_Character + "Căn cứ vào khoản 1 Điều 20 Luật Tổ chức Tòa án nhân dân và Điều 18 Bộ luật tố tụng dân sự;";
            }
            else
            {
                mhs = new HoSo.mHoSo();
                mhs.Parameters["TenDonVi"].Value = TenPhongBan.ToUpper();
                mhs.Parameters["DonViLuu"].Value = "Vụ " + tenviettat + "-TANDTC";

                mhs.Parameters["SoThongBao"].Value =(string.IsNullOrEmpty(SoHieu)? ".....": SoHieu)+ "/ CV-" + tenviettat;
                mhs.Parameters["DiaDiem"].Value = StrThoiGian_DiaDiem;

                mhs.Parameters["CanCuBoLuatTT"].Value = Tab_Character + "Căn cứ vào khoản 1 Điều 20 Luật Tổ chức Tòa án nhân dân và Điều 18 Bộ luật tố tụng dân sự;";
            }

            //-----------------------------------------
            String CanCuBoLuatTT = "";
            string Noidung_2 = "";
            String Noidung_3 = "";


            int LoaiHs = Convert.ToInt16(objHS.LOAI);
            int IsMuonHS_VKS = (string.IsNullOrEmpty(objHS.ISMUONHOSOVKS + "")) ? 0 : Convert.ToInt16(objHS.ISMUONHOSOVKS);
            string Noidung = "";
            if (LoaiHs == 0)
            {
                if (PhongBanID == 4)
                {
                    //-------------vu 3---------------
                    Noidung = "Tòa án nhân dân tối cao nhận được " + "Đơn đề nghị ";/// Văn bản kiến nghị ";
                    Noidung += "xem xét theo thủ tục Giám đốc thẩm/tái thẩm đối với Bản án ";
                    Noidung += "số " + sobanan + " ngày " + ngaybanan + " ";
                    Noidung += "của " + ToaAnThuLy + " ";
                    Noidung += "về việc " + quanhephapluat + " giữa:";
                }
                else
                {
                    //-------------vu 2---------------
                    Noidung = "Để có cơ sở giải quyết đơn đề nghị xem xét theo thủ tục giám đốc thẩm của đương sự,";
                    Noidung += " đề nghị " + ToaAnThuLy + " chỉ đạo chuyển cho " + TenPhongBan
                            + " - Tòa án nhân dân tối cao hồ sơ vụ án \"" + quanhephapluat + "\" giữa các đương sự là:";
                }
            }

            //--------------------------------------------
            String DCGuiHS = "";
            if (PhongBanID == 4)
            {
                if (IsMuonHS_VKS == 0)
                    mhs_v3.Parameters["ToaAnThuLy"].Value = "Đồng chí Chánh án " + ToaAnThuLy;
                else
                    mhs_v3.Parameters["ToaAnThuLy"].Value = "Đồng chí Viện trưởng " + ToaAnThuLy;
                //-------------vu 3---------------
                CanCuBoLuatTT = "Căn cứ vào khoản 1 Điều 20 Luật Tổ chức Tòa án nhân dân; ";
                if (Str_LoaiVuAn == ENUM_LOAIVUVIEC.AN_HANHCHINH)
                {
                    if (IsMuonHS_VKS == 0)
                        CanCuBoLuatTT += "Điều 24, khoản 1 Điều 260 Luật tố tụng hành chính; ";
                    else
                        CanCuBoLuatTT += "Điều 24, khoản 1 Điều 260 Luật tố tụng hành chính"
                                         + " và Thông tư liên tịch số 03/2016/TTLT-VKSNDTC-TANDTC ngày 31/8/2016 của Viện kiểm sát nhân dân tối cao, Tòa án nhân dân tối cao; ";
                }
                else
                {
                    if (IsMuonHS_VKS == 0)
                        CanCuBoLuatTT += "Điều 18, khoản 1 Điều 331 Bộ luật tố tụng dân sự;";
                    else
                        CanCuBoLuatTT += "Điều 18, khoản 1 Điều 331 Bộ luật tố tụng dân sự"
                                         + " và Thông tư liên tịch số 02/2016/TTLT-VKSNDTC-TANDTC ngày 31/8/2016 của Viện kiểm sát nhân dân tối cao, Tòa án nhân dân tối cao; ";
                }
                CanCuBoLuatTT += " Tòa án nhân dân tối cao đề nghị " + ToaAnThuLy
                               + " chuyển hồ sơ vụ án nêu trên cho Tòa án nhân dân tối cao trong thời hạn 07 ngày, kể từ ngày nhận được công văn này.";
                DCGuiHS = "(Hồ sơ xin gửi chuyển phát nhanh về "
                        + "Vụ Giám đốc, kiểm tra III Tòa án nhân dân tối cao số 262 Đội Cấn, quận Ba Đình, TP. Hà Nội; "
                        + "bên ngoài bao bì ghi rõ họ tên nguyên đơn, bị đơn, số bản án và ngày xét xử để "
                            + TenPhongBan + " tiện theo dõi khi nhận hồ sơ);";

                Noidung_3 = "Trường hợp hồ sơ vụ án đã được chuyển cho cơ quan, đơn vị khác thì đề nghị Quý Tòa ghi rõ vào phiếu mượn này và gửi lại Vụ Giám đốc, kiểm tra III để theo dõi.";
            }
            else
            {
                //-------------vu 2---------------
                Noidung_2 = "Do " + ToaAnThuLy + " xét xử phúc thẩm tại Bản án phúc thẩm số " + sobanan + " ngày " + ngaybanan + ".";
                DCGuiHS = "(Hồ sơ xin gửi phát nhanh theo địa chỉ: " + TenPhongBan
                            + " Tòa án nhân dân tối cao ";
            }

            if (PhongBanID == 4)
            {
                mhs_v3.Parameters["NoiDung1"].Value = Tab_Character + Noidung;
                if (Str_LoaiVuAn != ENUM_LOAIVUVIEC.AN_HANHCHINH)
                {
                    mhs_v3.Parameters["NguyenDon"].Value = Tab_Character + "- Nguyên đơn: " + Nguyendon;
                    mhs_v3.Parameters["BiDon"].Value = Tab_Character + "- Bị đơn: " + bidon;
                }
                else
                {
                    mhs_v3.Parameters["NguyenDon"].Value = Tab_Character + "- Người khởi kiện: " + Nguyendon;
                    mhs_v3.Parameters["BiDon"].Value = Tab_Character + "- Người bị kiện: " + bidon;
                }
                mhs_v3.Parameters["CanCuBoLuatTT"].Value = Tab_Character + CanCuBoLuatTT;
                mhs_v3.Parameters["NoiDung3"].Value = Tab_Character + Noidung_3;

                mhs_v3.Parameters["DiaChiGuiHS"].Value = Tab_Character + DCGuiHS + ((!String.IsNullOrEmpty(DiaChiGuiHS)) ? " tại địa chỉ:" + DiaChiGuiHS : "") + ")";
                reportcontrol.OpenReport(mhs_v3);
            }
            else
            {
                mhs.Parameters["ToaAnThuLy"].Value = ToaAnThuLy;
                mhs.Parameters["NoiDung1"].Value = Tab_Character + Noidung;
                mhs.Parameters["NguyenDon"].Value = Tab_Character + "- Nguyên đơn: " + Nguyendon;
                mhs.Parameters["BiDon"].Value = Tab_Character + "- Bị đơn: " + bidon;
                mhs.Parameters["NoiDung2"].Value = Tab_Character + Noidung_2;
                mhs.Parameters["DiaChiGuiHS"].Value = Tab_Character + DCGuiHS + ((!String.IsNullOrEmpty(DiaChiGuiHS)) ? " tại địa chỉ:" + DiaChiGuiHS : "") + ")";
                reportcontrol.OpenReport(mhs);
            }

            //----------------------------------  
            //String CurrUser = Session[ENUM_SESSION.SESSION_USERTEN] + "";
            //Decimal CurrUserID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            //String ChucVu = "";
            //DM_CANBO objCB = dt.DM_CANBO.Where(x => x.ID == CurrUserID).Single();
            //if (objCB != null )
            //{
            //    CurrUser = objCB.HOTEN;
            //    Decimal ChucVuID = (String.IsNullOrEmpty(objCB.CHUCVUID + "")) ? 0 : (decimal)objCB.CHUCVUID;
            //    if (ChucVuID==0)
            //        ChucVuID = (String.IsNullOrEmpty(objCB.CHUCDANHID + "")) ? 0 : (decimal)objCB.CHUCDANHID;

            //    if (ChucVuID>0)
            //    {
            //        DM_DATAITEM objCV = dt.DM_DATAITEM.Where(x => x.ID == ChucVuID).Single();
            //        ChucVu = objCV.TEN;
            //    }
            //}
            //mhs.Parameters["TenNguoiKy"].Value = CurrUser;
            //mhs.Parameters["ChucDanhNguoiKy"].Value = ChucVu.ToUpper();
        }

        void InPhieuMuon_NhieuVuAn()
        {
            String Tab_Character = "     ", NgayTaoHS = "";
            String KyTuXuongDong = "\r\n";
            decimal temp_id = 0;
            string TenPhongBan = "", DiaChiGuiHS = "";

            int PhongBanID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_PHONGBANID] + "")) ? 0 : Convert.ToInt32(Session[ENUM_SESSION.SESSION_PHONGBANID] + "");
            DM_PHONGBAN objPB = dt.DM_PHONGBAN.Where(x => x.ID == PhongBanID).Single();
            if (objPB != null)
            {
                TenPhongBan = objPB.TENPHONGBAN;
                DiaChiGuiHS = objPB.HAUTOCV;
            }
            //-----------------------------------
            string tenviettat = "";
            switch (PhongBanID)
            {
                case 2:
                    tenviettat = "GĐKTI";
                    break;
                case 3:
                    tenviettat = "GĐKTII";
                    break;
                case 4:
                    tenviettat = "GĐKTIII";
                    break;
                default:
                    tenviettat = TenPhongBan;
                    break;
            }
            //--------------------------------
            HoSo.mMuonHoSoV3 mhs_v3 = new HoSo.mMuonHoSoV3();

            //--------------------------------
            String NoiDung1 = "Tòa án nhân dân tối cao nhận được đơn của các đương sự đề nghị xem xét theo thủ tục giám đốc thẩm đối với các bản án phúc thẩm sau:";
            mhs_v3.Parameters["NoiDung1"].Value = Tab_Character+ NoiDung1;

            //--------------------------------
            String SoHieu = "";
            String DSVuAn = "";
            String GroupID = Request["gID" + ""] + "";
            GDTTT_QUANLYHS_BL objBL = new GDTTT_QUANLYHS_BL();
            DataTable tblVuAn = objBL.GetByGroupID(GroupID);
            Decimal ToanAnId =0;
            string TenToaAn = "";

            Decimal LoaiAnID = 0;
            string BoLuatCanCu_TheoLoaiAn = "";
            
            if (tblVuAn != null && tblVuAn.Rows.Count>0)
            {
                int count_item = 0;
                temp_id = 0;
                NgayTaoHS = "ngày " +tblVuAn.Rows[0]["Ngay"] + " tháng " + tblVuAn.Rows[0]["Thang"] + " năm " + tblVuAn.Rows[0]["Nam"];
                //---------------------------------
                foreach (DataRow row in tblVuAn.Rows)
                {
                    SoHieu = row["SoHieu"] + "";
                    temp_id = Convert.ToDecimal(row["TOAPHUCTHAMID"]+"");
                    if (temp_id != ToanAnId)
                    {
                        if (TenToaAn.Length > 0)
                            TenToaAn += ", ";
                        TenToaAn += row["ToaXX"] + "";
                        ToanAnId = temp_id;
                    }
                   
                    //-------------------------
                    temp_id = Convert.ToDecimal(row["LoaiAn"] + "");
                    if (temp_id != LoaiAnID)
                    {
                        if (temp_id == Convert.ToDecimal(ENUM_LOAIVUVIEC.AN_HANHCHINH))
                            BoLuatCanCu_TheoLoaiAn += "Điều 24, khoản 1 Điều 260 Luật tố tụng hành chính; ";
                        else
                            BoLuatCanCu_TheoLoaiAn += "Điều 18, khoản 1 Điều 331 Bộ luật tố tụng dân sự;";

                        LoaiAnID = temp_id;
                    }

                    //------------------------------
                    count_item++;
                    if (count_item > 1)
                        DSVuAn += KyTuXuongDong;

                    DSVuAn += Tab_Character;
                    DSVuAn += count_item.ToString() + ". " + "Bản án " + row["TenLoaiAn"].ToString();
                    DSVuAn += " số " + row["SOANPHUCTHAM"].ToString();
                    DSVuAn += " ngày " + row["NgayXuPhucTham"].ToString();
                    DSVuAn += " của " + row["TOAXX"].ToString();
                    DSVuAn += " giữa:"+ KyTuXuongDong;
                    DSVuAn += Tab_Character + "- Người khởi kiện: " + row["NguyenDon"].ToString();
                    DSVuAn += KyTuXuongDong;
                    DSVuAn += Tab_Character + "- Người bị kiện: " + row["BiDon"].ToString();
                }
            }
            mhs_v3.Parameters["ToaAnThuLy"].Value = "Đồng chí Chánh án " + TenToaAn;
            mhs_v3.Parameters["DSVuAn"].Value = DSVuAn;

            //------------------------
            String CanCuBoLuatTT = Tab_Character + "Căn cứ vào khoản 1 Điều 20 Luật tổ chức Tòa án nhân dân; ";
            CanCuBoLuatTT += BoLuatCanCu_TheoLoaiAn;
            CanCuBoLuatTT += "Tòa án nhân dân tối cao đề nghị " + TenToaAn
                             + " chuyển hồ sơ các vụ án trên cho Tòa án nhân dân tối cao trong thời hạn 07 ngày, kể từ ngày nhận được công văn này.";
            mhs_v3.Parameters["CanCuBoLuatTT"].Value = CanCuBoLuatTT;

            //---------------------------------
            String DCGuiHS =  "(Hồ sơ xin gửi chuyển phát nhanh về "
                       + "Vụ Giám đốc, kiểm tra III Tòa án nhân dân tối cao số 262 Đội Cấn, quận Ba Đình, TP. Hà Nội; "
                       + "bên ngoài bao bì ghi rõ họ tên nguyên đơn (người khởi kiện), bị đơn (người bị kiện), số bản án và ngày xét xử để "
                           + TenPhongBan + " tiện theo dõi khi nhận hồ sơ);";
            mhs_v3.Parameters["DiaChiGuiHS"].Value = Tab_Character + DCGuiHS;
           
            //----------------------------
            String NoiDung3 = "Trường hợp hồ sơ vụ án đã được chuyển cho cơ quan, đơn vị khác thì đề nghị Quý Tòa ghi rõ vào phiếu mượn này và gửi lại Vụ Giám đốc, kiểm tra III để theo dõi.";
            mhs_v3.Parameters["NoiDung3"].Value = NoiDung3;
            
            //----------------------------------------------
             mhs_v3.Parameters["TenDonVi"].Value = TenPhongBan.ToUpper();
             mhs_v3.Parameters["DonViLuu"].Value = "Vụ " + tenviettat + "-TANDTC";

             mhs_v3.Parameters["SoThongBao"].Value = (string.IsNullOrEmpty(SoHieu) ? "....." : SoHieu) + "/ PM/TANDTC-" + tenviettat;
             mhs_v3.Parameters["DiaDiem"].Value = "Hà Nội, "+ NgayTaoHS ;
             reportcontrol.OpenReport(mhs_v3);
        }
    }
}