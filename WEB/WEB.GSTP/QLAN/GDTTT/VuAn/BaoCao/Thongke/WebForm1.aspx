﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WEB.GSTP.QLAN.GDTTT.VuAn.BaoCao.Thongke.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table cellpadding="1" style="font-family: times New Roman; font-size: 11pt; text-align: center; border-collapse: collapse;">
            <tr>
                <td colspan="19" style="height: 10pt;"></td>
            </tr>
            <tr style="font-weight: bold;">
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Họ tên</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Tổng số đơn xử lý trong ngày</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Tổng số đơn xử lý trong tháng</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Tổng số đơn xử lý trong kỳ</td>
            </tr>
            <tr style="font-style: italic; color: #adabab; font-size: 10pt; text-align: center;">
                <td style="text-align: center; vertical-align: middle; font-style: italic; border: 1pt solid Black; height: 25px"></td>
                <td style="text-align: center; vertical-align: middle; font-style: italic; border: 1pt solid Black;">1</td>
                <td style="text-align: center; vertical-align: middle; font-style: italic; border: 1pt solid Black;">2</td>
                <td style="text-align: center; vertical-align: middle; font-style: italic; border: 1pt solid Black;">3</td>
            </tr>
            <tr style="height: 1pt;">
                <td style="width: 350px"></td>
                <td style="width: 100px"></td>
                <td style="width: 100px"></td>
                <td style="width: 100px"></td>
            </tr>
        </table>
    </form>
</body>
</html>
