﻿using BL.GSTP;
using DAL.GSTP;
using BL.GSTP.GDTTT;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WEB.GSTP.QLAN.GDTTT.VuAn.Popup
{
    public partial class Ketqua : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        String temp_folder_upload = "~/TempUpload/";
        Decimal LoaiNguoiKhangNghiID = 1143;//ChanhAn
        Decimal CurrentUserID = 0;
        public String IsShowCol = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentUserID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrentUserID == 0)
                Response.Redirect("/Login.aspx");
            else
            {
                if (!IsPostBack)
                {
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";

                    if (Request["vid"] != null)
                        LoadInfoVuAn();
                }
            }
        }
        void LoadDropThamQuyenXX()
        {
            //Thẩm quyền xét xử
            ddlThamquyenXX.Items.Clear();
            ddlThamquyenXX.DataSource = dt.DM_TOAAN.Where(x => (x.LOAITOA == "TOICAO" || x.LOAITOA == "CAPCAO") && x.HIEULUC == 1 && x.HANHCHINHID > 0).OrderBy(x => x.ARRTHUTU).ToList();
            ddlThamquyenXX.DataTextField = "TEN";
            ddlThamquyenXX.DataValueField = "ID";
            ddlThamquyenXX.DataBind();
            ddlThamquyenXX.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }

        void LoadDropNguoiKy(DropDownList drop, DataTable tbl)
        {
            DM_CANBO_BL objBL = new DM_CANBO_BL();
            decimal donviID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            if (tbl == null)
                tbl = objBL.DM_CANBO_GETBYDONVI_2CHUCVU(donviID, ENUM_CHUCVU.CHUCVU_CA, ENUM_CHUCVU.CHUCVU_PCA);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                drop.DataSource = tbl;
                drop.DataTextField = "MA_TEN";
                drop.DataValueField = "ID";
                drop.DataBind();
            }
            drop.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        void LoadInfoVuAn()
        {
            try
            {
                decimal VuAnID = Convert.ToDecimal(Request["vid"] + "");
                GDTTT_VUAN oT = dt.GDTTT_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
                LoadThongTinVuAn(VuAnID, oT);

                if (oT.GQD_LOAIKETQUA != null)
                {
                    cmdXoa.Visible = true;
                    
                    rdbLoai.SelectedValue = oT.GQD_LOAIKETQUA.ToString();
                    lbl_GQD_NgayCV.Text = "Ngày phát hành của Vụ";
                    pnXuLyKhac.Visible = false;
                    switch (oT.GQD_LOAIKETQUA.ToString())
                    {
                        case "0":
                            pnVKS_GQ.Visible = pnQDKN.Visible = pnXepDon.Visible = false;
                            pnTLD.Visible = true;
                            txtTLD_So.Text = oT.GDQ_SO + "";
                            txtTLD_NguoiKy.Text = oT.GDQ_NGUOIKY + "";
                            if (oT.GDQ_NGAY != null) txtTLD_Ngay.Text = ((DateTime)oT.GDQ_NGAY).ToString("dd/MM/yyyy", cul);
                            rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                            break;
                        case "1":
                            LoadDropThamQuyenXX();
                            pnQDKN.Visible = true;
                            pnVKS_GQ.Visible = pnTLD.Visible = pnXepDon.Visible = false;
                            txtSoQD.Text = oT.GDQ_SO + "";
                            txtKN_NguoiKy.Text = oT.GDQ_NGUOIKY + "";
                            if (oT.GDQ_NGAY != null) txtNgayQD.Text = ((DateTime)oT.GDQ_NGAY).ToString("dd/MM/yyyy", cul);
                            if (oT.THAMQUYENXXGDT != null) ddlThamquyenXX.SelectedValue = oT.THAMQUYENXXGDT + "";
                            rdbLoai.Items[0].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                            break;
                        case "2":
                            pnQDKN.Visible = pnVKS_GQ.Visible = pnTLD.Visible = false;
                            pnXepDon.Visible = true;
                            txtNguoiDuyetXepDon.Text = oT.GDQ_NGUOIKY + "";
                            txtSoXepDon.Text = oT.GDQ_SO + "";
                            txtNgayXepDon.Text = ((DateTime)oT.GDQ_NGAY).ToString("dd/MM/yyyy", cul);
                            rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                            break;
                        case "3":
                            pnXuLyKhac.Visible = true;
                            pnVKS_GQ.Visible = pnTLD.Visible =  pnXepDon.Visible = pnQDKN.Visible = false;
                            lbl_GQD_NgayCV.Text = "Ngày xử lý";
                            txtNoiDung_xlk.Text = oT.GQD_KETQUA;
                            txtKN_NguoiKy_xlk.Text= oT.GDQ_NGUOIKY + "";
                            rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[4].Enabled = false;
                            break;
                        case "4":
                            pnTLD.Visible = pnQDKN.Visible = pnXepDon.Visible = false;
                            pnVKS_GQ.Visible = true;
                            txtTB_So.Text = oT.GDQ_SO + "";
                            txtTB_NguoiKy.Text = oT.GDQ_NGUOIKY + "";
                            if (oT.GDQ_NGAY != null) txtTB_Ngay.Text = ((DateTime)oT.GDQ_NGAY).ToString("dd/MM/yyyy", cul);
                            rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = false;
                            break;
                    }

                    txtGhichu.Text = oT.GQD_GHICHU + "";
                }
                else
                    cmdXoa.Visible = false;
                //--------------------------------
                lbtDownload.Visible = (String.IsNullOrEmpty(oT.KQ_TENFILE + "")) ? false : true;
                //--------------------------------
                int is_anqh = (string.IsNullOrEmpty(oT.ISANQUOCHOI + "")) ? 0 : (int)oT.ISANQUOCHOI;
                if (is_anqh > 0)
                    LoadDSTraloidon(oT.ID);
                else
                    pnThongBaoAQH.Visible = false;
            }
            catch (Exception ex)
            {
                //lbthongbao.Text = "Lỗi: " + ex.Message;
                //lbthongbao.ForeColor = System.Drawing.Color.Red;
            }
        }
        void LoadThongTinVuAn(Decimal VuAnID, GDTTT_VUAN oT)
        {
            if (oT != null)
            {
                // txtGQD_SoCV.Text = oT.GQD_SOCV;
                txtGQD_NgayCV.Text = (String.IsNullOrEmpty(oT.GQD_NGAYPHATHANHCV + "") || (oT.GQD_NGAYPHATHANHCV == DateTime.MinValue)) ? "" : ((DateTime)oT.GQD_NGAYPHATHANHCV).ToString("dd/MM/yyyy", cul);

                if (!String.IsNullOrEmpty(oT.TENVUAN + ""))
                    txtTenVuan.Text = oT.TENVUAN;
                else
                {
                    txtTenVuan.Text = oT.NGUYENDON + " - " + oT.BIDON;
                   
                    if (oT.QHPL_DINHNGHIAID != null && oT.QHPL_DINHNGHIAID > 0 && String.IsNullOrEmpty(oT.QHPL_TEXT + ""))
                    {
                        GDTTT_DM_QHPL oQHPL = dt.GDTTT_DM_QHPL.Where(x => x.ID == oT.QHPL_DINHNGHIAID).FirstOrDefault();
                        txtTenVuan.Text = txtTenVuan.Text + " - " + oQHPL.TENQHPL;
                    }
                    else
                    {
                        txtTenVuan.Text = txtTenVuan.Text + " - " + oT.QHPL_TEXT;
                    }

                }
                if (!string.IsNullOrEmpty(oT.GQD_ISHOANTHA + "") && ((int)oT.GQD_ISHOANTHA == 1))
                {
                    LoadDropNguoiKy(dropHTA_NguoiKy, null);
                    pnHoanTHA.Visible = true;
                    rdHoanTHA.SelectedValue = "1";
                    txtHTA_So.Text = oT.GQD_HOANTHA_SO + "";
                    if (oT.GQD_HOANTHA_NGAY != null)
                        txtHTA_Ngay.Text = ((DateTime)oT.GQD_HOANTHA_NGAY).ToString("dd/MM/yyyy", cul);
                    Cls_Comon.SetValueComboBox(dropHTA_NguoiKy, oT.GQD_HOANTHA_NGUOIKYID);
                    cmdXoaHoanTHA.Visible = true;
                }
                else pnHoanTHA.Visible = cmdXoaHoanTHA.Visible = false;

                //------------------------
               
                txtVuAn_SoThuLy.Text = oT.SOTHULYDON;
                txtVuAn_NgayThuLy.Text = (String.IsNullOrEmpty(oT.NGAYTHULYDON + "") || (oT.NGAYTHULYDON == DateTime.MinValue)) ? "" : ((DateTime)oT.NGAYTHULYDON).ToString("dd/MM/yyyy", cul);

                if (oT.LOAIAN == 1)
                    txtVuan_Loaian.Text = "Hình sự";
                else if (oT.LOAIAN == 2)
                    txtVuan_Loaian.Text = "Dân sự";
                else if (oT.LOAIAN == 3)
                    txtVuan_Loaian.Text = "Hôn nhân - Gia đình";
                else if (oT.LOAIAN == 4)
                    txtVuan_Loaian.Text = "Kinh doanh, thương mại";
                else if (oT.LOAIAN == 5)
                    txtVuan_Loaian.Text = "Lao động";
                else if (oT.LOAIAN == 6)
                    txtVuan_Loaian.Text = "Hành chính";
                else if (oT.LOAIAN == 7)
                    txtVuan_Loaian.Text = "Phá sản";

                if (oT.LOAI_GDTTTT == 2)
                    txtVuan_LoaiGDTT.Text = "Tái thẩm";
                else
                    txtVuan_LoaiGDTT.Text = "Giám đốc thẩm";

                if (oT.BAQD_CAPXETXU == 2)
                {
                    txtVuAn_SoBanAn.Text = oT.SOANSOTHAM;
                    txtVuAn_NgayBanAn.Text = (String.IsNullOrEmpty(oT.NGAYXUSOTHAM + "") || (oT.NGAYXUSOTHAM == DateTime.MinValue)) ? "" : ((DateTime)oT.NGAYXUSOTHAM).ToString("dd/MM/yyyy", cul);
                }
                else if (oT.BAQD_CAPXETXU == 3)
                {
                    txtVuAn_SoBanAn.Text = oT.SOANPHUCTHAM;
                    txtVuAn_NgayBanAn.Text = (String.IsNullOrEmpty(oT.NGAYXUPHUCTHAM + "") || (oT.NGAYXUPHUCTHAM == DateTime.MinValue)) ? "" : ((DateTime)oT.NGAYXUPHUCTHAM).ToString("dd/MM/yyyy", cul);
                }else
                {
                    txtVuAn_SoBanAn.Text = oT.SO_QDGDT;
                    txtVuAn_NgayBanAn.Text = (String.IsNullOrEmpty(oT.NGAYQD + "") || (oT.NGAYQD == DateTime.MinValue)) ? "" : ((DateTime)oT.NGAYQD).ToString("dd/MM/yyyy", cul);
                }
                //----------------------------------
                LoadDuongSu(VuAnID, ENUM_DANSU_TUCACHTOTUNG.NGUYENDON, txtVuAn_NguyenDon);
                LoadDuongSu(VuAnID, ENUM_DANSU_TUCACHTOTUNG.BIDON, txtVuAn_BiDon);

                txtVuan_NguoiGui.Text = oT.NGUOIKHIEUNAI;
                if (oT.ISHOSO == 1)
                    txtVuan_TrangthaiHS.Text = "Có hồ sơ";
                else
                    txtVuan_TrangthaiHS.Text = "";
                txtVuan_Ghichu.Text = oT.GHICHU;
            }
        }
        void LoadDuongSu(Decimal VuAnID, string tucachtt, Literal control_display)
        {
            string StrDisplay = "";
            GDTTT_VUANVUVIEC_DUONGSU_BL objBL = new GDTTT_VUANVUVIEC_DUONGSU_BL();
            DataTable tbl = objBL.GetByVuAnID(VuAnID, tucachtt);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                foreach (DataRow row in tbl.Rows)
                {
                    if (!String.IsNullOrEmpty(StrDisplay + ""))
                        StrDisplay += ", " + row["TenDuongSu"].ToString();
                    else
                        StrDisplay = row["TenDuongSu"].ToString();
                }
            }
            control_display.Text = StrDisplay;
        }
        private void LoadDSTraloidon(decimal VuAnID)
        {
            int is_visible = 0;
            GDTTT_VUAN_BL oBL = new GDTTT_VUAN_BL();
            // DataTable tbl = oBL.TRALOIDON_DANHSACH(VuAnID);
            GDTTT_DON_TRALOI_BL objBL = new GDTTT_DON_TRALOI_BL();
            DataTable tbl = objBL.GetAllTB_TheoLoai(VuAnID, 2);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                rpt.DataSource = tbl;
                rpt.DataBind();
                rpt.Visible = true;
                is_visible++;
            }
            else rpt.Visible = false;

            //---------------------------------
            /* List<GDTTT_DON_TRALOI> lst = dt.GDTTT_DON_TRALOI.Where(x => x.VUANID == VuAnID
                                                                      && x.DUONGSUID == 0
                                                                      && x.TYPETB == ENUM_LOAITHONGBAO_QH.TBKQTRALOI).ToList();
             if (lst != null && lst.Count > 0)
             {
                 rptTB.DataSource = lst;
                 rptTB.DataBind();
                 rptTB.Visible = true;
                 rptTB.Visible = true;
                 is_visible++;
             }
             else
                 rptTB.Visible = false;*/
            if (is_visible > 0)
                pnThongBaoAQH.Visible = true;
            else
                pnThongBaoAQH.Visible = false;
        }
        protected void cmdXoaHoanTHA_Click(object sender, EventArgs e)
        {
            string strvid = Request["vid"] + "";
            decimal VuAnID = Convert.ToDecimal(strvid);
            GDTTT_VUAN oVA = dt.GDTTT_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();

            oVA.GQD_HOANTHA_SO = "";
            oVA.GQD_HOANTHA_NGAY = (DateTime?)null;
            oVA.GQD_HOANTHA_NGUOIKYID = 0;
            oVA.GQD_HOANTHA_TENNGUOIKY = "";
            oVA.GQD_ISHOANTHA = 0;
            dt.SaveChanges();

            rdHoanTHA.SelectedValue = "0";
            pnHoanTHA.Visible = false;
            cmdXoaHoanTHA.Visible = false; ;

            lttMsgHoanTHA.ForeColor = System.Drawing.Color.Blue;
            lttMsgHoanTHA.Text = "Xóa dữ liệu thành công !";
            Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
        }

        protected void cmdUpdateHoanTHA_Click(object sender, EventArgs e)
        {
            try
            {
                string strvid = Request["vid"] + "";
                decimal VuAnID = Convert.ToDecimal(strvid);
                GDTTT_VUAN oVA = dt.GDTTT_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();

                oVA.GQD_HOANTHA_SO = "";
                oVA.GQD_HOANTHA_NGAY = (DateTime?)null;
                oVA.GQD_HOANTHA_NGUOIKYID = 0;
                oVA.GQD_HOANTHA_TENNGUOIKY = "";
                oVA.GQD_ISHOANTHA = 0;
                int ishoantha = Convert.ToInt16(rdHoanTHA.SelectedValue);
                if (ishoantha == 1)
                {
                    oVA.GQD_ISHOANTHA = 1;
                    //Update them thong tin Hoan thi hanh an
                    oVA.GQD_HOANTHA_SO = txtHTA_So.Text.Trim();
                    oVA.GQD_HOANTHA_NGAY = (String.IsNullOrEmpty(txtHTA_Ngay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtHTA_Ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    oVA.GQD_HOANTHA_NGUOIKYID = Convert.ToDecimal(dropHTA_NguoiKy.SelectedValue);
                    oVA.GQD_HOANTHA_TENNGUOIKY = dropHTA_NguoiKy.SelectedItem.Text;
                }

                //-----------------------------------              
                dt.SaveChanges();
                if (ishoantha == 1)
                    cmdXoaHoanTHA.Visible = true;
                else
                    cmdXoaHoanTHA.Visible = false;
                lttMsgHoanTHA.ForeColor = System.Drawing.Color.Blue;
                lttMsgHoanTHA.Text = "Cập nhật thành công !";
                Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
            }
            catch (Exception ex)
            {
                lttMsgHoanTHA.ForeColor = System.Drawing.Color.Red;
                lttMsgHoanTHA.Text = "Lỗi:" + ex.Message;
            }
        }
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                decimal VuAnID = String.IsNullOrEmpty(Request["vid"] + "") ? 0 : Convert.ToDecimal(Request["vid"] + "");
                GDTTT_VUAN oVA = dt.GDTTT_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();

                #region Update thong tin KQGQDon                
                // oVA.GQD_SOCV = txtGQD_SoCV.Text;
                oVA.GQD_NGAYPHATHANHCV = (String.IsNullOrEmpty(txtGQD_NgayCV.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtGQD_NgayCV.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                //-----------------------------                
                oVA.NGUOIKHANGNGHI = 0;
                oVA.THAMQUYENXXGDT = 0;
                oVA.GQD_GHICHU = txtGhichu.Text;

                //-----------------------------------
                string loai = rdbLoai.SelectedValue;
                oVA.GQD_LOAIKETQUA = Convert.ToDecimal(loai);
                string ngay_temp = "";
                switch (loai)
                {
                    case "0":
                        oVA.GDQ_SO = txtTLD_So.Text;
                        ngay_temp = txtTLD_Ngay.Text.Trim();
                        oVA.GDQ_NGAY = (String.IsNullOrEmpty(txtTLD_Ngay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTLD_Ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oVA.GDQ_NGUOIKY = txtTLD_NguoiKy.Text.Trim();
                        oVA.GQD_KETQUA = rdbLoai.SelectedItem.Text + ": số " + oVA.GDQ_SO + " ngày " + ngay_temp;
                        rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                        break;
                    case "1":
                        ngay_temp = txtNgayQD.Text.Trim();
                        oVA.GDQ_SO = txtSoQD.Text;
                        oVA.GDQ_NGAY = (String.IsNullOrEmpty(txtNgayQD.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayQD.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oVA.GDQ_NGUOIKY = txtKN_NguoiKy.Text.Trim();
                        oVA.NGUOIKHANGNGHI = LoaiNguoiKhangNghiID; //Convert.ToDecimal(ddlNguoiKN.SelectedValue);
                        oVA.THAMQUYENXXGDT = Convert.ToDecimal(ddlThamquyenXX.SelectedValue);
                        oVA.GQD_KETQUA = rdbLoai.SelectedItem.Text + ": số " + oVA.GDQ_SO + " ngày " + ngay_temp;
                        rdbLoai.Items[0].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                        break;
                    case "2":
                        ngay_temp = txtNgayXepDon.Text.Trim();
                        oVA.GDQ_NGUOIKY = txtNguoiDuyetXepDon.Text.Trim();
                        oVA.GDQ_SO = txtSoXepDon.Text;
                        oVA.GDQ_NGAY = (String.IsNullOrEmpty(txtNgayXepDon.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayXepDon.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oVA.GQD_KETQUA = rdbLoai.SelectedItem.Text + ": số " + oVA.GDQ_SO + " ngày " + ngay_temp;
                        rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = false;
                        break;
                    case "3":
                        oVA.GDQ_NGUOIKY = txtKN_NguoiKy_xlk.Text.Trim();
                        oVA.GQD_KETQUA =  txtNoiDung_xlk.Text;
                        rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[4].Enabled = false;
                        break;
                    case "4":
                        oVA.GDQ_SO = txtTB_So.Text;
                        ngay_temp = txtTB_Ngay.Text.Trim();
                        oVA.GDQ_NGAY = (String.IsNullOrEmpty(txtTB_Ngay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTB_Ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oVA.GDQ_NGUOIKY = txtTB_NguoiKy.Text.Trim();
                        oVA.GQD_KETQUA = "số " + oVA.GDQ_SO + " ngày " + ngay_temp;
                        rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = false;
                        break;
                }
               
                //-----------------------------
                //UpdateFile(oVA);
                #endregion

                //-----------------------------------
                oVA.QUATRINH_GHICHU = txtGhichu.Text;
                if (loai != ENUM_GDTTT_TRANGTHAI.THULY_XETXU_GDT.ToString())
                {
                    switch (loai)
                    {
                        case "0":
                            oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.TRALOIDON;
                            break;
                        case "1":
                            oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.KHANGNGHI;
                            break;
                        case "2":
                            oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.XEPDON;
                            break;
                        case "3":
                            oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.XUlY_KHAC;
                            break;
                        case "4":
                            oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.XUlY_KHAC;
                            break;
                    }
                }

                dt.SaveChanges();

                Update_ThongBaoTL(oVA);
                lbthongbao.ForeColor = System.Drawing.Color.Blue;
                lbthongbao.Text = "Cập nhật thành công !";
                cmdXoa.Visible = true;
                Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
            }
            catch (Exception ex)
            {
                lbthongbao.ForeColor = System.Drawing.Color.Red;
                lbthongbao.Text = "Lỗi:" + ex.Message;
            }
        }
        void Update_ThongBaoTL(GDTTT_VUAN oVA)
        {
            // if (oVA.ISANQUOCHOI==1)
            // {
            decimal VuAnID = String.IsNullOrEmpty(Request["vid"] + "") ? 0 : Convert.ToDecimal(Request["vid"] + "");
            //if (rdbLoai.SelectedValue == "0")
            //{
            Decimal CurrDSID = 0, ThongBaoID = 0;
            string temp = "";
            int LoaiTLD = 2; //0:Nguoi khiếu nại/1:Đương su/2:Co quan chuyen don/3:Khac

            foreach (RepeaterItem item in rpt.Items)
            {
                HiddenField hddThongBaoID = (HiddenField)item.FindControl("hddThongBaoID");
                HiddenField hddDonID = (HiddenField)item.FindControl("hddDonID");

                Literal lttHoTen = (Literal)item.FindControl("lttHoTen");
                Literal lttAddress = (Literal)item.FindControl("lttAddress");
                TextBox txtSo = (TextBox)item.FindControl("txtSo");
                TextBox txtNgay = (TextBox)item.FindControl("txtNgay");
                TextBox txtGhiChu = (TextBox)item.FindControl("txtGhiChu");

                ThongBaoID = Convert.ToDecimal(hddThongBaoID.Value);
                temp = "," + ThongBaoID.ToString() + ",";

                //----------------------------
                if ((!String.IsNullOrEmpty(txtSo.Text.Trim())) || (!String.IsNullOrEmpty(txtNgay.Text.Trim())))
                {
                    #region Insert hoac update
                    bool isupdate = false;
                    GDTTT_DON_TRALOI objEdit = null;
                    try
                    {
                        if (ThongBaoID > 0)
                        {
                            objEdit = dt.GDTTT_DON_TRALOI.Where(x => x.ID == ThongBaoID).Single();
                            if (objEdit != null)
                                isupdate = true;
                            else objEdit = new GDTTT_DON_TRALOI();
                        }
                        else objEdit = new GDTTT_DON_TRALOI();
                    }
                    catch (Exception ex) { objEdit = new GDTTT_DON_TRALOI(); }

                    objEdit.VUANID = VuAnID;
                    objEdit.DONID = (String.IsNullOrEmpty(hddDonID.Value)) ? 0 : Convert.ToDecimal(hddDonID.Value);
                    objEdit.DUONGSUID = CurrDSID;
                    objEdit.NGUOINHAN = lttHoTen.Text;
                    objEdit.DIACHINHAN = lttAddress.Text;
                    objEdit.TYPETB = ENUM_LOAITHONGBAO_QH.TBKQTRALOI;
                    objEdit.SO = txtSo.Text.Trim();
                    objEdit.NGAY = (String.IsNullOrEmpty(txtNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                    objEdit.LOAI = LoaiTLD;
                    //objEdit.LOAI " 0:Nguoi khiếu nại/1:Đương su/2:Co quan chuyen don/3:Khac

                    objEdit.GHICHU = txtGhiChu.Text;
                    if (!isupdate)
                        dt.GDTTT_DON_TRALOI.Add(objEdit);
                    try
                    {
                        dt.SaveChanges();
                    }
                    catch (EntityDataSourceValidationException e)
                    {
                        lbthongbao.Text = "Lỗi Entities: " + e.Message;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        string strErr = "";
                        foreach (var eve in ex.EntityValidationErrors)
                        {
                            foreach (var ve in eve.ValidationErrors)
                            {
                                strErr += ve.PropertyName + " : " + ve.ErrorMessage;
                            }
                        }
                        lbthongbao.Text = "Có lỗi, hãy thử lại: " + strErr;
                    }
                    #endregion
                }
                else
                {
                    #region  Delete
                    if (ThongBaoID > 0)
                    {
                        GDTTT_DON_TRALOI obj = dt.GDTTT_DON_TRALOI.Where(x => x.ID == ThongBaoID).Single();
                        if (obj != null)
                        {
                            dt.GDTTT_DON_TRALOI.Remove(obj);
                            dt.SaveChanges();
                        }
                    }
                    #endregion
                }
            }
            //}
            //else
            //{
            //    #region xoa thong bao tl kq
            //    List<GDTTT_DON_TRALOI> lst = dt.GDTTT_DON_TRALOI.Where(x => x.VUANID == VuAnID && x.TYPETB == ENUM_LOAITHONGBAO_QH.TBKQTRALOI).ToList();
            //    if (lst != null && lst.Count > 0)
            //    {
            //        foreach (GDTTT_DON_TRALOI item in lst)
            //            dt.GDTTT_DON_TRALOI.Remove(item);
            //        dt.SaveChanges();
            //    }
            //    #endregion  
            //}
            // }
        }
        protected void cmdXoa_Click(object sender, EventArgs e)
        {
            string strvid = Request["vid"] + "";
            decimal VuAnID = Convert.ToDecimal(strvid);
            decimal trangthai_id = 0;
            int count_totrinh = 0;
            GDTTT_VUAN oVA = dt.GDTTT_VUAN.Where(x => x.ID == VuAnID).FirstOrDefault();
            if (oVA != null)
            {
                trangthai_id = (decimal)oVA.TRANGTHAIID;
                oVA.GQD_LOAIKETQUA = null;
                oVA.GQD_KETQUA = "";
                oVA.GDQ_SO = oVA.GDQ_NGUOIKY = oVA.GQD_GHICHU = "";
                oVA.NGUOIKHANGNGHI = oVA.THAMQUYENXXGDT = 0;
                oVA.GDQ_NGAY = oVA.GQD_NGAYPHATHANHCV = null;
                //-------------------
                if (trangthai_id != ENUM_GDTTT_TRANGTHAI.THULY_XETXU_GDT)
                {
                    count_totrinh = (string.IsNullOrEmpty(oVA.ISTOTRINH + "")) ? 0 : (int)oVA.ISTOTRINH;
                    if (count_totrinh > 0)
                    {
                        try
                        {
                            List<GDTTT_TOTRINH> lstTT = dt.GDTTT_TOTRINH.Where(x => x.VUANID == VuAnID).OrderByDescending(x => x.NGAYTRINH).ToList();
                            if (lstTT != null && lstTT.Count > 0)
                            {
                                GDTTT_TOTRINH objToTrinh = lstTT[0];
                                oVA.TRANGTHAIID = objToTrinh.TINHTRANGID;
                            }
                            else
                                SetTrangThai(oVA);
                        }
                        catch (Exception ex)
                        {
                            SetTrangThai(oVA);
                        }
                    }
                    else SetTrangThai(oVA);
                }

                //------------------------------
                dt.SaveChanges();
                Xoa_ThongBaoTraLoiCV(VuAnID);
                //------------------------------
               
                ClearForm();
                lbthongbao.ForeColor = System.Drawing.Color.Blue;
                lbthongbao.Text = "Xóa kết quả giải quyết đơn thành công!";
                cmdXoa.Visible = false;
                Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
            }
        }
        void Xoa_ThongBaoTraLoiCV(decimal VuAnID)
        {
            try
            {
                List<GDTTT_DON_TRALOI> lst = dt.GDTTT_DON_TRALOI.Where(x => x.VUANID == VuAnID && x.TYPETB == 2).ToList();
                if (lst != null && lst.Count > 0)
                {
                    foreach (GDTTT_DON_TRALOI it in lst)
                        dt.GDTTT_DON_TRALOI.Remove(it);
                    dt.SaveChanges();
                }
                LoadDSTraloidon(VuAnID);
            }
            catch (Exception ex) { }
        }
        GDTTT_VUAN SetTrangThai(GDTTT_VUAN oVA)
        {
            oVA.ISTOTRINH = 0;
            decimal THAMTRAVIENID = (string.IsNullOrEmpty(oVA.THAMTRAVIENID + "")) ? 0 : (decimal)oVA.THAMTRAVIENID;
            if (THAMTRAVIENID > 0)
                oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.PHANCONG_TTV;
            else
                oVA.TRANGTHAIID = ENUM_GDTTT_TRANGTHAI.THULY_MOI;
            return oVA;
        }
        void ClearForm()
        {
            rdbLoai.SelectedValue = "0";
            pnVKS_GQ.Visible = pnQDKN.Visible = pnXepDon.Visible = false;
            pnTLD.Visible = true;
            
            rdbLoai.Items[0].Enabled = rdbLoai.Items[1].Enabled = rdbLoai.Items[2].Enabled = rdbLoai.Items[3].Enabled = rdbLoai.Items[4].Enabled = true;

            txtTLD_So.Text = txtTLD_Ngay.Text = txtTLD_NguoiKy.Text = "";

            txtSoQD.Text = txtNgayQD.Text = txtKN_NguoiKy.Text = "";
            txtTB_So.Text = txtTB_Ngay.Text = txtTB_NguoiKy.Text = "";
            try
            {
                ddlThamquyenXX.SelectedIndex = 0;
            }
            catch (Exception exx) { }
            txtSoXepDon.Text = txtNgayXepDon.Text = txtNguoiDuyetXepDon.Text = "";

            txtGQD_NgayCV.Text = txtGhichu.Text = "";
        }
        void UpdateFile(GDTTT_VUAN obj)
        {
            if (hddFilePath.Value != "")
            {
                try
                {
                    string strFilePath = "";
                    if (chkKySo.Checked)
                    {
                        string[] arr = hddFilePath.Value.Split('/');
                        strFilePath = arr[arr.Length - 1];
                        strFilePath = Server.MapPath(temp_folder_upload) + strFilePath;
                    }
                    else
                        strFilePath = hddFilePath.Value.Replace("/", "\\");
                    byte[] buff = null;
                    using (FileStream fs = File.OpenRead(strFilePath))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        FileInfo oF = new FileInfo(strFilePath);
                        long numBytes = oF.Length;
                        buff = br.ReadBytes((int)numBytes);
                        obj.KQ_NOIDUNGFILE = buff;
                        obj.KQ_TENFILE = oF.Name;
                        obj.KQ_KIEUFILE = oF.Extension;
                    }
                    File.Delete(strFilePath);
                }
                catch (Exception ex) { }
            }
        }

        protected void rdbLoai_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbthongbao.Text = "";
            string loai = rdbLoai.SelectedValue;
            lbl_GQD_NgayCV.Text = "Ngày phát hành của Vụ";
            pnXuLyKhac.Visible = false;
            switch (loai)
            {
                case "0":
                    pnQDKN.Visible = pnXepDon.Visible = pnVKS_GQ.Visible = false;
                    pnTLD.Visible = true;
                    break;
                case "1":
                    LoadDropThamQuyenXX();
                    pnQDKN.Visible = true;
                    pnTLD.Visible = pnXepDon.Visible = pnVKS_GQ.Visible = false;
                    break;
                case "2":
                    pnXepDon.Visible = true;
                    pnQDKN.Visible = pnTLD.Visible = pnVKS_GQ.Visible = false;
                    break;
                case "3":
                    pnXuLyKhac.Visible = true;
                    pnTLD.Visible = pnXepDon.Visible = pnQDKN.Visible = pnVKS_GQ.Visible = false;
                    lbl_GQD_NgayCV.Text = "Ngày xử lý";
                    break;
                case "4":
                    pnXuLyKhac.Visible = pnTLD.Visible = pnXepDon.Visible = pnQDKN.Visible = false;
                    pnVKS_GQ.Visible = true;
                    lbl_GQD_NgayCV.Text = "Ngày phát hành của Vụ";
                    break;
            }
        }
        //-----------------------------
        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                if (AsyncFileUpLoad.HasFile)
                {
                    string strFileName = Cls_Comon.ChuyenTenFileUpload(AsyncFileUpLoad.FileName);
                    string path = Server.MapPath(temp_folder_upload) + strFileName;
                    AsyncFileUpLoad.SaveAs(path);
                    path = path.Replace("\\", "/");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
                }
            }
            catch (Exception ex) { }
        }
        protected void lbtDownload_Click(object sender, EventArgs e)
        {
            try
            {
                decimal ID = Convert.ToDecimal(Request["vid"] + "");
                GDTTT_VUAN oND = dt.GDTTT_VUAN.Where(x => x.ID == ID).FirstOrDefault();
                if (oND.KQ_TENFILE != "")
                {
                    var cacheKey = Guid.NewGuid().ToString("N");
                    Context.Cache.Insert(key: cacheKey, value: oND.KQ_NOIDUNGFILE, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.KQ_TENFILE + "&Extension=" + oND.KQ_KIEUFILE + "';", true);
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void txtTLD_Ngay_TextChanged(object sender, EventArgs e)
        {
            //SetNgayCV(txtTLD_So, txtTLD_Ngay);
        }
        //protected void txtNgayQD_TextChanged(object sender, EventArgs e)
        //{
        //    SetNgayCV(txtSoQD, txtNgayQD);
        //}
        void SetNgayCV(TextBox control_so, TextBox control_ngay)
        {
            Decimal PhongBanID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_PHONGBANID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_PHONGBANID]);
            DateTime ngay = (DateTime)((String.IsNullOrEmpty(control_ngay.Text.Trim())) ? (DateTime?)DateTime.Now : DateTime.Parse(control_ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault));
            GDTTT_VUAN_BL objBL = new GDTTT_VUAN_BL();
            Decimal SoCV = objBL.GetLastGQD_SoCV(PhongBanID, ngay);
            control_so.Text = SoCV + "";
        }

        protected void rdHoanTHA_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ishoantha = Convert.ToInt16(rdHoanTHA.SelectedValue);
            if (ishoantha == 1)
            {
                LoadDropNguoiKy(dropHTA_NguoiKy, null);
                pnHoanTHA.Visible = true;
            }
            else
                pnHoanTHA.Visible = false;
        }
    }
}
