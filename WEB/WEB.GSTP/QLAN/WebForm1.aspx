﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WEB.GSTP.QLAN.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <table cellpadding="1" cellspacing="1" style="font-family: times New Roman; font-size: 11pt; text-align: center; border-collapse: collapse;">
            <tr>
                <td colspan="7" style="height: 0pt;"></td>
            </tr>
            <tr>
                <td colspan="7" style="line-height: 100%; font-size: 14pt"><b>TỔNG HỢP DANH SÁCH ÁN </b>
                    <br /><i style="font-size: 12pt;">(Số liệu tính từ ngày   đến ngày )</i>
                </td>
            </tr>
            <tr>
                <td colspan="7" style="height: 15pt; text-align: left;">Tổng số:</td>
            </tr>
            <tr style="font-weight: bold;">
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000; height: 50pt;">STT</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Thông tin vụ án </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Số QĐ<br />/Ngày quyết định<br />/Thường hợp thụ lý </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Ngày nhận phân công<br />/ Thẩm phán </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Ngày phân công<br />/ Người phân công </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Tình trạng GQ </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;">Ghi chú </td>
            </tr>
            <tr style="font-size: 11pt;">
                <td style="padding:3pt; text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid #000000;"></td>
            </tr>
            <tr style="height: 1pt;">
                <td style="width: 20pt"></td>
                <td style="width: 100pt"></td>
                <td style="width: 100pt"></td>
                <td style="width: 100pt"></td>
                <td style="width: 100pt"></td>
                <td style="width: 100pt"></td>
                <td style="width: 100pt"></td>
            </tr>
        </table>


    </form>
</body>
</html>
