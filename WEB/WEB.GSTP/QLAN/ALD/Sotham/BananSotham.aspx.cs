﻿using BL.GSTP;
using BL.GSTP.ALD;
using DAL.DKK;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.ALD.Sotham
{
    public partial class BananSotham : System.Web.UI.Page
    {
        DKKContextContainer dkk = new DKKContextContainer();
        GSTPContext dt = new GSTPContext();
        private static CultureInfo cul = new CultureInfo("vi-VN");
        public static bool GetNumber(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch
            { return false; }
        }
        public static string GetTextDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return (Convert.ToDateTime(obj).ToString("dd/MM/yyyy", cul));
            }
            catch
            { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                LoadCombobox();
                LoadBoLuat();
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
                hddID.Value = current_id.ToString();
                decimal ID = Convert.ToDecimal(current_id);
                CheckQuyen(ID);
                LoadBanAnInfo(ID);
                LoadDieuLuat();
                LoadAnPhi();
                LoadTGTT();
                GetTrangThaiBanDauDONKK_USER_DKNHANVB(ID);
            }
            LoadFile();
        }
        private void CheckQuyen(decimal ID)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdLuatUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdAnphi, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdTGTT, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdHuyBanAn, oPer.CAPNHAT);
            ALD_DON oT = dt.ALD_DON.Where(x => x.ID == ID).FirstOrDefault();
            if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
            {
                lstErr.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            List<ALD_SOTHAM_THULY> lstCount = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == ID).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin thụ lý sơ thẩm !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            else
            {
                hddNgayThuLy.Value = lstCount[0].NGAYTHULY + "" == "" ? "" : ((DateTime)lstCount[0].NGAYTHULY).ToString("dd/MM/yyyy");
            }
            //Kiểm tra xem đã có quyết định đưa vụ việc ra xét xử hay không
            decimal IDLQD = 0;
            DM_QD_LOAI oLQD = dt.DM_QD_LOAI.Where(x => x.MA == "DVARXX").FirstOrDefault();
            if (oLQD != null) IDLQD = oLQD.ID;
            List<ALD_SOTHAM_QUYETDINH> lstQDXX = dt.ALD_SOTHAM_QUYETDINH.Where(x => x.DONID == ID && x.LOAIQDID == IDLQD).ToList();
            if (lstQDXX.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật quyết định đưa vụ án ra xét xử !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            //--Kiểm tra đã phân công thẩm phán chủ tọa
            List<ALD_SOTHAM_HDXX> lstTPCT = dt.ALD_SOTHAM_HDXX.Where(x => x.DONID == ID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin hội đồng xét xử !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            ALD_SOTHAM_KHANGCAO kc = dt.ALD_SOTHAM_KHANGCAO.Where(x => x.DONID == ID).FirstOrDefault();
            if (kc != null)
            {
                lstErr.Text = "Vụ việc đã có kháng cáo. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            ALD_SOTHAM_KHANGNGHI kn = dt.ALD_SOTHAM_KHANGNGHI.Where(x => x.DONID == ID).FirstOrDefault();
            if (kn != null)
            {
                lstErr.Text = "Vụ việc đã có kháng nghị. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new ALD_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
            if (Result != "")
            {
                lstErr.Text = Result;
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdLuatUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdTGTT, false);
                Cls_Comon.SetButton(cmdHuyBanAn, false);
                hddShowCommand.Value = "False";
                return;
            }

            //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            //int counttk = oCBDT.Rows.Count;
            //if (counttk > 0)
            //{
            //    //là thư k
            //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
            //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
            //    //int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
            //    int countItem = dt.ALD_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == ID && s.MAVAITRO == "VTTP_GIAIQUYETSOTHAM");
            //    if (countItem > 0)
            //    {
            //        //được gán 
            //    }
            //    else
            //    {
            //        //không được gán
            //        StrMsg = "Người dùng không được sửa đổi thông tin của vụ việc do không được phân công giải quyết.";
            //        lstErr.Text = StrMsg;
            //        Cls_Comon.SetButton(cmdUpdate, false);
            //        Cls_Comon.SetButton(cmdLuatUpdate, false);
            //        Cls_Comon.SetButton(cmdAnphi, false);
            //        Cls_Comon.SetButton(cmdTGTT, false);
            //        Cls_Comon.SetButton(cmdHuyBanAn, false);
            //        hddShowCommand.Value = "False";
            //        return;
            //    }
            //}
        }
        private void LoadAnPhi()
        {
            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            ALD_SOTHAM_BL oBL = new ALD_SOTHAM_BL();
            dgAnPhi.DataSource = oBL.ALD_SOTHAM_BANAN_ANPHI_GET(DONID);
            dgAnPhi.DataBind();
            foreach (DataGridItem oItem in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                if (chkMien.Checked)
                {
                    txtAnphi.Text = "";
                    txtAnphi.Enabled = false;
                }
                else
                {
                    txtAnphi.Enabled = true;
                }
            }
        }
        private void LoadTGTT()
        {
            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            ALD_SOTHAM_BL oBL = new ALD_SOTHAM_BL();
            DataTable dtTGTT = oBL.ALD_SOTHAM_BANAN_TGTT_GET(DONID);
            if (dtTGTT != null && dtTGTT.Rows.Count > 0)
            {
                hddTGTTRowLastIndex.Value = dtTGTT.Rows.Count + "";
            }
            dgTGTT.DataSource = dtTGTT;
            dgTGTT.DataBind();
        }
        protected void dgTGTT_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtNgayTGTT = (TextBox)e.Item.FindControl("txtNgayTGTT");
                if (hddTGTTRowLastIndex.Value == (dgTGTT.Items.Count + 1) + "")
                {
                    txtNgayTGTT.Attributes.Add("onfocus", "myFunctionFocus();");
                }
            }
        }
        private void LoadBoLuat()
        {
            List<DM_BOLUAT> lst = dt.DM_BOLUAT.Where(x => x.LOAI == ENUM_LOAIVUVIEC.AN_LAODONG && x.HIEULUC == 1).OrderByDescending(y => y.NGAYBANHANH).ToList();
            if (lst != null && lst.Count > 0)
            {
                pnAnPhi.Visible = true;
                ddlBoLuat.DataSource = lst;
                ddlBoLuat.DataTextField = "TENBOLUAT";
                ddlBoLuat.DataValueField = "ID";
                ddlBoLuat.DataBind();
                LoadDieuKhoan();
            }
            else { pnAnPhi.Visible = false; }
        }
        private void LoadDieuKhoan()
        {
            if (ddlBoLuat.Items.Count > 0)
            {
                decimal IDBL = Convert.ToDecimal(ddlBoLuat.SelectedValue);
                ddlDieukhoan.DataSource = dt.DM_BOLUAT_TOIDANH.Where(x => x.LUATID == IDBL).ToList();
                ddlDieukhoan.DataTextField = "TENTOIDANH";
                ddlDieukhoan.DataValueField = "ID";
                ddlDieukhoan.DataBind();
            }
        }
        private void LoadFile()
        {
            decimal ID = Convert.ToDecimal(hddID.Value);
            dgFile.DataSource = dt.ALD_SOTHAM_BANAN_FILE.Where(x => x.DONID == ID).ToList();
            dgFile.DataBind();

            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            ALD_DON oT = dt.ALD_DON.Where(x => x.ID == DONID).FirstOrDefault();
            int ma_gd = (int)oT.MAGIAIDOAN;
            foreach (DataGridItem item in dgFile.Items)
            {
                LinkButton lbtXoa = (LinkButton)item.FindControl("lbtXoa");
                if (ma_gd == (int)ENUM_GIAIDOANVUAN.PHUCTHAM || ma_gd == (int)ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    Cls_Comon.SetLinkButton(lbtXoa, false);
                }
                if (hddShowCommand.Value == "False")
                {
                    lbtXoa.Visible = false;
                }
            }
        }
        private void LoadDieuLuat()
        {
            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            ALD_SOTHAM_BL oBL = new ALD_SOTHAM_BL();
            dgDieuLuat.DataSource = oBL.ALD_SOTHAM_BANAN_DIEULUAT_GET(DONID);
            dgDieuLuat.DataBind();
        }
        private void LoadBanAnInfo(decimal DonID)
        {
            List<ALD_SOTHAM_BANAN> lst = dt.ALD_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (lst.Count > 0)
            {
                ALD_SOTHAM_BANAN oT = lst[0];
                hddBanAnID.Value = oT.ID.ToString();
                txtSobanan.Text = oT.SOBANAN;
                ddlLoaiQuanhe.SelectedValue = oT.LOAIQUANHE.ToString();
                if (oT.QHPLTKID != null)
                    ddlQHPLTK.SelectedValue = oT.QHPLTKID.ToString();

                txtQuanhephapluat_name(oT);
                if (oT.NGAYMOPHIENTOA != null)
                {
                    txtNgaymophientoa.Text = ((DateTime)oT.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                }
                else
                {
                    txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                }
                if (oT.NGAYTUYENAN != null) txtNgaytuyenan.Text = ((DateTime)oT.NGAYTUYENAN).ToString("dd/MM/yyyy", cul);
                if (oT.NGAYHIEULUC != null) txtNgayhieuluc.Text = ((DateTime)oT.NGAYHIEULUC).ToString("dd/MM/yyyy", cul);

                if (oT.ISVKSTHAMGIA != null) rdbVKSThamgia.SelectedValue = oT.ISVKSTHAMGIA.ToString();

                ddlYeutonuocngoai.SelectedValue = oT.YEUTONUOCNGOAI.ToString();
                rdbAnle.SelectedValue = (string.IsNullOrEmpty(oT.APDUNGANLE + "")) ? "0" : oT.APDUNGANLE.ToString();

                rdVuAnQuaHan.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISQUAHAN + "")) ? "0" : oT.TK_ISQUAHAN.ToString();
                rdNNChuQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_CHUQUAN + "")) ? "0" : oT.TK_QUAHAN_CHUQUAN.ToString();
                rdNNKhachQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_KHACHQUAN + "")) ? "0" : oT.TK_QUAHAN_KHACHQUAN.ToString();
                txtSoQDTraiPL.Text = oT.TK_SOQDTRAIPLBIHUY + "";
                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;
                //Load File
                LoadFile();
            }
            else
            {
                ALD_DON oDon = dt.ALD_DON.Where(x => x.ID == DonID).FirstOrDefault();
                if (oDon != null)
                {
                    ddlLoaiQuanhe.SelectedValue = oDon.LOAIQUANHE.ToString();
                    if (oDon.QHPLTKID != null) ddlQHPLTK.SelectedValue = oDon.QHPLTKID.ToString();
                    ddlYeutonuocngoai.SelectedValue = oDon.YEUTONUOCNGOAI.ToString();
                }
                ALD_SOTHAM_THULY tl = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == DonID).OrderByDescending(x => x.NGAYTHULY).FirstOrDefault();
                if (tl != null)
                {
                    if (tl.QHPLTKID != null)
                        ddlQHPLTK.SelectedValue = tl.QHPLTKID.ToString();

                    txtQuanhephapluat_name(tl);
                }
                txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            }
        }
        private bool CheckValid()
        {
            if (txtQuanhephapluat.Text.Trim().Length >= 500)
            {
                lstErr.Text = "Quan hệ pháp luật nhập quá dài.";
                txtQuanhephapluat.Focus();
                return false;
            }
            if (txtQuanhephapluat.Text == null || txtQuanhephapluat.Text == "")
            {
                lstErr.Text = "Chưa nhập quan hệ pháp luật.";
                txtQuanhephapluat.Focus();
                return false;
            }
            if (ddlQHPLTK.SelectedValue == "0")
            {
                lstErr.Text = "Chưa chọn quan hệ pháp luật dùng cho thống kê!";
                return false;
            }
            decimal IDChitieuTK = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
            if (dt.DM_QHPL_TK.Where(x => x.PARENT_ID == IDChitieuTK).ToList().Count > 0)
            {
                lstErr.Text = "Quan hệ pháp luật dùng cho thống kê chỉ được chọn mã con, bạn hãy chọn lại !";
                return false;
            }
            if (txtSobanan.Text == "")
            {
                lstErr.Text = "Chưa nhập số bản án";
                txtSobanan.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaymophientoa.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày mở phiên tòa hoặc không hợp lệ !";
                txtNgaymophientoa.Focus();
                return false;
            }
            DateTime dNgayMoPhienToa = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayMoPhienToa > DateTime.Now)
            {
                lstErr.Text = "Ngày mở phiên tòa không được lớn hơn ngày hiện tại !";
                txtNgaymophientoa.Focus();
                return false;
            }
            DateTime NgayThuLy = hddNgayThuLy.Value == "" ? DateTime.MinValue : DateTime.Parse(hddNgayThuLy.Value, cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayMoPhienToa < NgayThuLy)
            {
                lstErr.Text = "Ngày mở phiên tòa không được nhỏ hơn ngày thụ lý " + hddNgayThuLy.Value;
                txtNgaymophientoa.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaytuyenan.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày tuyên án hoặc không hợp lệ !";
                return false;
            }
            DateTime dNgayTA = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayTA > DateTime.Now)
            {
                lstErr.Text = "Ngày tuyên án không được lớn hơn ngày hiện tại !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (dNgayTA < dNgayMoPhienToa)
            {
                lstErr.Text = "Ngày tuyên án phải lớn hơn ngày mở phiên tòa !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (txtNgayhieuluc.Text.Trim() != "")
            {
                if (Cls_Comon.IsValidDate(txtNgayhieuluc.Text) == false)
                {
                    lstErr.Text = "Chưa nhập ngày hiệu lực hoặc không hợp lệ !";
                    return false;
                }
                DateTime dNgayHieuLuc = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayHieuLuc < dNgayTA)
                {
                    lstErr.Text = "Ngày hiệu lực phải lớn hơn ngày tuyên án !";
                    txtNgayhieuluc.Focus();
                    return false;
                }
            }
            if (rdbAnle.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Áp dụng án lệ ?\"";
                return false;
            }
            if (rdbVKSThamgia.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Có VKS tham gia ?\"";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn \"Vụ án quá hạn luật định ?\"";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "1")
            {
                if (rdNNChuQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn \"Nguyên nhân chủ quan ?\"";
                    return false;
                }
                if (rdNNKhachQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn \"Nguyên nhân khách quan ?\"";
                    return false;
                }
            }

            //----------------------------
            string so = txtSobanan.Text;
            if (!String.IsNullOrEmpty(txtNgaytuyenan.Text))
            {
                DateTime ngayBA = DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "ALD", so, ngayBA);
                if (CheckID > 0)
                {
                    Decimal CurrBanAnId = (string.IsNullOrEmpty(hddBanAnID.Value)) ? 0 : Convert.ToDecimal(hddBanAnID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "ALD", ngayBA).ToString();
                    if (CheckID != CurrBanAnId)
                    {
                        strMsg = "Số bản án " + txtSobanan.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSobanan.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSobanan.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        private void LoadCombobox()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBY2GROUPNAME(ENUM_DANHMUC.QUANHEPL_YEUCAU_LD, ENUM_DANHMUC.QUANHEPL_TRANHCHAP_LD);

            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();
            //Load QHPL Thống kê.
            ddlQHPLTK.DataSource = dt.DM_QHPL_TK.Where(x => x.STYLES == ENUM_QHPLTK.LAODONG && x.ENABLE == 1).OrderBy(y => y.ARRTHUTU).ToList();
            ddlQHPLTK.DataTextField = "CASE_NAME";
            ddlQHPLTK.DataValueField = "ID";
            ddlQHPLTK.DataBind();
            ddlQHPLTK.Items.Insert(0, new ListItem("--Chọn QHPL dùng thống kê--", "0"));
        }
        protected void ddlQuanhephapluat_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal IDQHPL = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue);
            DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
            DM_DATAGROUP oGroup = dt.DM_DATAGROUP.Where(x => x.ID == obj.GROUPID).FirstOrDefault();
            if (oGroup.MA == ENUM_DANHMUC.QUANHEPL_TRANHCHAP)
                ddlLoaiQuanhe.SelectedValue = "1";
            else
                ddlLoaiQuanhe.SelectedValue = "2";

        }
        protected void ddlLoaiQuanhe_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadCombobox();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                List<ALD_SOTHAM_BANAN> lst = dt.ALD_SOTHAM_BANAN.Where(x => x.DONID == DONID).ToList();
                ALD_SOTHAM_BANAN oND;
                if (lst.Count == 0)
                    oND = new ALD_SOTHAM_BANAN();
                else
                {
                    oND = lst[0];
                }
                oND.DONID = DONID;

                oND.LOAIQUANHE = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue);

                oND.QUANHEPHAPLUATID = null;
                oND.QUANHEPHAPLUAT_NAME = txtQuanhephapluat.Text;
                //renameTenvuviec(txtQuanhephapluat.Text, DONID);

                oND.QHPLTKID = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
                oND.SOBANAN = txtSobanan.Text;
                oND.NGAYMOPHIENTOA = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYTUYENAN = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYHIEULUC = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                oND.ISVKSTHAMGIA = Convert.ToDecimal(rdbVKSThamgia.SelectedValue);

                oND.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);

                oND.YEUTONUOCNGOAI = Convert.ToDecimal(ddlYeutonuocngoai.SelectedValue);

                oND.APDUNGANLE = rdbAnle.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbAnle.SelectedValue);

                oND.ISVKSTHAMGIA = rdbVKSThamgia.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbVKSThamgia.SelectedValue);


                oND.TK_ISQUAHAN = rdVuAnQuaHan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVuAnQuaHan.SelectedValue);
                oND.TK_QUAHAN_CHUQUAN = rdNNChuQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNChuQuan.SelectedValue);
                oND.TK_QUAHAN_KHACHQUAN = rdNNKhachQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNKhachQuan.SelectedValue);
                oND.TK_SOQDTRAIPLBIHUY = (String.IsNullOrEmpty(txtSoQDTraiPL.Text + "")) ? 0 : Convert.ToDecimal(txtSoQDTraiPL.Text);

                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;
                if (lst.Count == 0)
                {
                    oND.NGAYTAO = DateTime.Now;
                    oND.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ALD_SOTHAM_BANAN.Add(oND);
                    dt.SaveChanges();
                }
                else
                {
                    oND.NGAYSUA = DateTime.Now;
                    oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.SaveChanges();
                }
                TamNgungDONKK_USER_DKNHANVB(DONID);
                lstErr.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lstErr.Text = "Lỗi: " + ex.Message;
            }
        }




        private void GetTrangThaiBanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 1);
            if (obj != null)
            {

                ttBanDauDONKK_USER_DKNHANVB.Value = obj.TRANGTHAI.Value.ToString();
            }
        }
        private void SetTrangThaibanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 3);
            if (obj != null)
            {

                obj.TRANGTHAI = Convert.ToDecimal(ttBanDauDONKK_USER_DKNHANVB.Value);
                dkk.SaveChanges();
            }
        }
        private void TamNgungDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 1);
            if (obj != null)
            {

                obj.TRANGTHAI = 3;
                dkk.SaveChanges();
            }
        }

        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (AsyncFileUpLoad.HasFile)
            {
                string strFileName = AsyncFileUpLoad.FileName;
                string path = Server.MapPath("~/TempUpload/") + strFileName;
                AsyncFileUpLoad.SaveAs(path);
                path = path.Replace("\\", "/");
                decimal DONID = Session[ENUM_LOAIAN.AN_LAODONG] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG]);
                ALD_SOTHAM_BANAN_FILE oTF = new ALD_SOTHAM_BANAN_FILE();
                // Lưu hồ sơ bản án
                string strFilePath = "";
                if (chkKySo.Checked)
                {
                    string[] arr = path.Split('/');
                    strFilePath = arr[arr.Length - 1];
                    strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                }
                else
                    strFilePath = path.Replace("/", "\\");
                byte[] buff = null;
                using (FileStream fs = File.OpenRead(strFilePath))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(strFilePath);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ALD_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                // Dùng cho tống đạt văn bản
                if (strFilePath.ToLower().Contains("52-ds"))
                {
                    ALD_DON don = dt.ALD_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    if (don != null)
                    {
                        UploadFileID(don, "52-DS", oTF);
                    }
                }
                File.Delete(strFilePath);
                //path = path.Replace("\\", "/");
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
            }
        }

        protected void cmdThemFileTL_Click(object sender, EventArgs e)
        {
            SaveFile_KySo();
            LoadFile();
        }
        protected void cmd_load_form_Click(object sender, EventArgs e)
        {
            LoadFile();
            Load_CheckBox();
        }
        protected void Load_CheckBox()
        {
            if (chkKySo.Checked == true)
            {
                zonekythuong.Style.Add("Display", "none");
                zonekyso.Style.Add("Display", "block");
            }
            else
            {
                zonekythuong.Style.Add("Display", "block");
                zonekyso.Style.Add("Display", "none");
            }
        }
        void SaveFile_KySo()
        {
            string folder_upload = "/TempUpload/";
            string file_kyso = hddFilePath.Value;
            if (!String.IsNullOrEmpty(hddFilePath.Value))
            {
                String[] arr = file_kyso.Split('/');
                string file_name = arr[arr.Length - 1] + "";

                String file_path = Path.Combine(Server.MapPath(folder_upload), file_name);
                decimal DONID = Convert.ToDecimal(hddID.Value);
                ALD_SOTHAM_BANAN_FILE oTF = new ALD_SOTHAM_BANAN_FILE();

                byte[] buff = null;
                using (FileStream fs = File.OpenRead(file_path))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(file_path);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ALD_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                //xoa file
                File.Delete(file_path);
            }
        }
        protected void dgFile_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false || cmdUpdate.Enabled == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new ALD_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
                    if (Result != "")
                    {
                        lstErr.Text = Result;
                        return;
                    }
                    ALD_SOTHAM_BANAN_FILE oT = dt.ALD_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.ALD_SOTHAM_BANAN_FILE.Remove(oT);
                    dt.SaveChanges();
                    LoadFile();
                    break;
                case "Download":
                    ALD_SOTHAM_BANAN_FILE oND = dt.ALD_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                    }
                    break;
            }

        }
        protected void cmdLuatUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlDieukhoan.Items.Count == 0)
                {
                    lstMsgDieuluat.Text = "Bạn chưa chọn điều khoản !";
                    return;
                }
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                decimal DONID = Convert.ToDecimal(current_id);

                decimal DIEUID = Convert.ToDecimal(ddlDieukhoan.SelectedValue);
                if (dt.ALD_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DONID && x.DIEULUATID == DIEUID).ToList().Count > 0)
                {
                    lstMsgDieuluat.Text = "Đã tồn tại điều luật này trong danh sách !";
                    return;
                }

                ALD_SOTHAM_BANAN_DIEULUAT oT = new ALD_SOTHAM_BANAN_DIEULUAT();
                oT.DONID = DONID;
                oT.DIEULUATID = DIEUID;
                oT.NGAYTAO = DateTime.Now;
                oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.ALD_SOTHAM_BANAN_DIEULUAT.Add(oT);
                dt.SaveChanges();
                LoadDieuLuat();
                lstMsgDieuluat.Text = "Lưu thành công!";

            }
            catch (Exception ex)
            {
                lstMsgDieuluat.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void ddlBoLuat_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDieuKhoan();
        }
        protected void dgDieuLuat_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new ALD_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
                    if (Result != "")
                    {
                        lstErr.Text = Result;
                        return;
                    }
                    ALD_SOTHAM_BANAN_DIEULUAT oT = dt.ALD_SOTHAM_BANAN_DIEULUAT.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.ALD_SOTHAM_BANAN_DIEULUAT.Remove(oT);
                    dt.SaveChanges();
                    LoadDieuLuat();
                    break;
            }

        }
        protected void cmdAnphi_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgAnPhi.Items.Count > 0)
                {
                    foreach (DataGridItem oItem in dgAnPhi.Items)
                    {
                        TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                        if (txtNgaynhanbanan.Text != "")
                        {
                            DateTime NgayNhanBA;
                            if (DateTime.TryParse(txtNgaynhanbanan.Text, cul, DateTimeStyles.NoCurrentDateDefault, out NgayNhanBA))
                            {
                                if (DateTime.Compare(NgayNhanBA, DateTime.Now) > 0)
                                {
                                    lstMsgAnphi.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                                    txtNgaynhanbanan.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                lstMsgAnphi.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                                txtNgaynhanbanan.Focus();
                                return;
                            }
                        }
                    }
                }
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                foreach (DataGridItem oItem in dgAnPhi.Items)
                {
                    string strID = oItem.Cells[0].Text;
                    decimal DSID = Convert.ToDecimal(strID);
                    CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                    TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                    CheckBox chkThamgia = (CheckBox)oItem.FindControl("chkThamgia");
                    TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                    List<ALD_SOTHAM_BANAN_ANPHI> lst = dt.ALD_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DONID && x.DUONGSU == DSID).ToList();
                    if (lst.Count == 0)
                    {
                        ALD_SOTHAM_BANAN_ANPHI oT = new ALD_SOTHAM_BANAN_ANPHI();
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYTAO = DateTime.Now;
                        oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.ALD_SOTHAM_BANAN_ANPHI.Add(oT);
                        dt.SaveChanges();
                    }
                    else
                    {
                        ALD_SOTHAM_BANAN_ANPHI oT = lst[0];
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYSUA = DateTime.Now;
                        oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.SaveChanges();
                    }

                }
                lstMsgAnphi.Text = "Lưu thành công !";
            }
            catch (Exception ex)
            {
                lstMsgAnphi.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void chkThamgia_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkThamgia = (CheckBox)Item.FindControl("chkThamgia");
                TextBox txtNgaynhanbanan = (TextBox)Item.FindControl("txtNgaynhanbanan");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtNgaynhanbanan.Text = txtNgaytuyenan.Text;
                    }
                }
            }
        }
        protected void chkMien_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)Item.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)Item.FindControl("txtAnphi");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtAnphi.Text = "";
                        txtAnphi.Enabled = false;
                    }
                    else
                    {
                        txtAnphi.Enabled = true;
                    }
                }
            }
        }
        protected void cmdTGTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTGTT.Items.Count > 0)
                {
                    foreach (DataGridItem oItem in dgTGTT.Items)
                    {
                        TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgayTGTT");
                        if (txtNgaynhanbanan.Text != "")
                        {
                            DateTime NgayNhanBA;
                            if (DateTime.TryParse(txtNgaynhanbanan.Text, cul, DateTimeStyles.NoCurrentDateDefault, out NgayNhanBA))
                            {
                                if (DateTime.Compare(NgayNhanBA, DateTime.Now) > 0)
                                {
                                    lblMsgTGTT.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                                    txtNgaynhanbanan.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                lblMsgTGTT.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                                txtNgaynhanbanan.Focus();
                                return;
                            }
                        }
                    }
                }
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                foreach (DataGridItem oItem in dgTGTT.Items)
                {
                    string strID = oItem.Cells[0].Text;
                    decimal DSID = Convert.ToDecimal(strID);
                    CheckBox chkThamgiaTGTT = (CheckBox)oItem.FindControl("chkThamgiaTGTT");
                    TextBox txtNgayTGTT = (TextBox)oItem.FindControl("txtNgayTGTT");
                    List<ALD_SOTHAM_BANAN_TGTT> lst = dt.ALD_SOTHAM_BANAN_TGTT.Where(x => x.DONID == DONID && x.THAMGIATOTUNGID == DSID).ToList();
                    if (lst.Count == 0)
                    {
                        ALD_SOTHAM_BANAN_TGTT oT = new ALD_SOTHAM_BANAN_TGTT();
                        oT.DONID = DONID;
                        oT.THAMGIATOTUNGID = DSID;
                        oT.ISTHAMGIA = chkThamgiaTGTT.Checked == true ? 1 : 0;
                        oT.NGAYNHANBANAN = (String.IsNullOrEmpty(txtNgayTGTT.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgayTGTT.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        dt.ALD_SOTHAM_BANAN_TGTT.Add(oT);
                        dt.SaveChanges();
                    }
                    else
                    {
                        ALD_SOTHAM_BANAN_TGTT oT = lst[0];
                        oT.DONID = DONID;
                        oT.THAMGIATOTUNGID = DSID;
                        oT.ISTHAMGIA = chkThamgiaTGTT.Checked == true ? 1 : 0;
                        oT.NGAYNHANBANAN = (String.IsNullOrEmpty(txtNgayTGTT.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgayTGTT.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        dt.ALD_SOTHAM_BANAN_TGTT.Add(oT);
                        dt.SaveChanges();
                    }
                }
                lblMsgTGTT.Text = "Lưu thành công !";
                cmdTGTT.Style.Add("margin-bottom", "0px");
            }
            catch (Exception ex)
            {
                lblMsgTGTT.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void chkThamgiaTGTT_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            foreach (DataGridItem Item in dgTGTT.Items)
            {
                CheckBox chkThamgiaTGTT = (CheckBox)Item.FindControl("chkThamgiaTGTT");
                TextBox txtNgayTGTT = (TextBox)Item.FindControl("txtNgayTGTT");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtNgayTGTT.Text = txtNgaytuyenan.Text;
                    }
                }
            }
        }
        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
                pnNguyenNhanQuaHan.Visible = true;
            else
                pnNguyenNhanQuaHan.Visible = false;
        }
        protected void txtNgaymophientoa_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgaymophientoa.Text))
            {
                if (String.IsNullOrEmpty(txtNgaytuyenan.Text))
                {
                    txtNgaytuyenan.Text = txtNgaymophientoa.Text;
                }
                if (String.IsNullOrEmpty(txtNgayhieuluc.Text))
                {
                    txtNgayhieuluc.Text = txtNgaymophientoa.Text;
                }
            }
        }
        protected void cmdHuyBanAn_Click(object sender, EventArgs e)
        {
            // Xóa thông tin bản án
            decimal DonID = Session[ENUM_LOAIAN.AN_LAODONG] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG] + "");
            //reset_TENVUVIEC(DonID);

            List<ALD_SOTHAM_BANAN> banans = dt.ALD_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (banans.Count > 0)
            {
                dt.ALD_SOTHAM_BANAN.RemoveRange(banans);
            }
            // Xóa thông tin file đính kèm
            List<ALD_SOTHAM_BANAN_FILE> files = dt.ALD_SOTHAM_BANAN_FILE.Where(x => x.DONID == DonID).ToList();
            if (files.Count > 0)
            {
                dt.ALD_SOTHAM_BANAN_FILE.RemoveRange(files);
            }
            // Xóa điều luật áp dụng, tội danh
            List<ALD_SOTHAM_BANAN_DIEULUAT> dieuLuats = dt.ALD_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DonID).ToList();
            if (dieuLuats.Count > 0)
            {
                dt.ALD_SOTHAM_BANAN_DIEULUAT.RemoveRange(dieuLuats);
            }
            // Xóa thông tin án phí
            List<ALD_SOTHAM_BANAN_ANPHI> anPhis = dt.ALD_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DonID).ToList();
            if (anPhis.Count > 0)
            {
                dt.ALD_SOTHAM_BANAN_ANPHI.RemoveRange(anPhis);
            }
            // Xóa thông tin người tham gia tố tụng
            List<ALD_SOTHAM_BANAN_TGTT> tGTTs = dt.ALD_SOTHAM_BANAN_TGTT.Where(x => x.DONID == DonID).ToList();
            if (tGTTs.Count > 0)
            {
                dt.ALD_SOTHAM_BANAN_TGTT.RemoveRange(tGTTs);
            }
            // Xóa file tống đạt bản án
            decimal BieuMauID = 0;
            DM_BIEUMAU bm = dt.DM_BIEUMAU.Where(x => x.MABM == "52-DS").FirstOrDefault();
            if (bm != null)
            {
                BieuMauID = bm.ID;
            }

            ALD_FILE file = dt.ALD_FILE.Where(x => x.DONID == DonID && x.BIEUMAUID == BieuMauID && x.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM).FirstOrDefault();
            if (file != null)
            {
                dt.ALD_FILE.Remove(file);
            }
            SetTrangThaibanDauDONKK_USER_DKNHANVB(DonID);
            dt.SaveChanges();
            ResetControl();
            lstErr.Text = "Xóa bản án thành công!";
        }
        private void ResetControl()
        {
            txtQuanhephapluat.Text = getQHPL_NAME_THULY();
            ddlQuanhephapluat.SelectedIndex = 0;
            ddlQHPLTK.SelectedIndex = 0;
            txtSobanan.Text = "";
            txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtNgaytuyenan.Text = "";
            txtNgayhieuluc.Text = "";
            rdbAnle.ClearSelection();
            rdbVKSThamgia.ClearSelection();
            ddlYeutonuocngoai.SelectedIndex = 0;
            txtSoQDTraiPL.Text = "";
            rdVuAnQuaHan.ClearSelection();
            rdNNChuQuan.ClearSelection();
            rdNNKhachQuan.ClearSelection();
            LoadFile();
            if (ddlBoLuat.Items.Count > 0)
                ddlBoLuat.SelectedIndex = 0;
            if (ddlDieukhoan.Items.Count > 0)
                ddlDieukhoan.SelectedIndex = 0;
            LoadDieuLuat();
            LoadAnPhi();
            LoadTGTT();
        }
        private void UploadFileID(ALD_DON oDon, string strMaBieumau, ALD_SOTHAM_BANAN_FILE fileDinhKem)
        {
            ALD_DON_BL oBL = new ALD_DON_BL();
            decimal IDBM = 0;
            string strTenBM = "";
            bool isNew = false;
            List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
            if (lstBM.Count > 0)
            {
                IDBM = lstBM[0].ID;
                strTenBM = lstBM[0].TENBM;
            }
            ALD_FILE objFile = dt.ALD_FILE.Where(x => x.DONID == oDon.ID && x.TOAANID == oDon.TOAANID && x.MAGIAIDOAN == oDon.MAGIAIDOAN && x.BIEUMAUID == IDBM).FirstOrDefault();
            if (objFile == null)
            {
                isNew = true;
                objFile = new ALD_FILE();
            }
            objFile.DONID = oDon.ID;
            objFile.TOAANID = oDon.TOAANID;
            objFile.MAGIAIDOAN = oDon.MAGIAIDOAN;
            objFile.LOAIFILE = 1;
            objFile.BIEUMAUID = IDBM;
            objFile.NAM = DateTime.Now.Year;
            objFile.NOIDUNG = fileDinhKem.NOIDUNG;
            objFile.TENFILE = fileDinhKem.TENFILE;
            objFile.KIEUFILE = fileDinhKem.KIEUFILE;
            objFile.NGAYTAO = DateTime.Now;
            objFile.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            objFile.STT = 1;
            if (isNew)
                dt.ALD_FILE.Add(objFile);
            dt.SaveChanges();
        }

        private decimal getcurrentid()
        {
            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
            return Convert.ToDecimal(current_id);
        }
        //private void renameTenvuviec(string obj, decimal DONID)
        //{
        //    ALD_DON oT = dt.ALD_DON.Where(x => x.ID == DONID).FirstOrDefault();
        //    ALD_DON_DUONGSU nguyendon = dt.ALD_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "NGUYENDON").FirstOrDefault();
        //    ALD_DON_DUONGSU bidon = dt.ALD_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "BIDON").FirstOrDefault();

        //    oT.TENVUVIEC = nguyendon.TENDUONGSU + " - " + bidon.TENDUONGSU + " - " + obj;
        //}
        //private void reset_TENVUVIEC(decimal DONID)
        //{
        //    ALD_SOTHAM_THULY oNT = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == DONID).FirstOrDefault();
        //    if (oNT.QUANHEPHAPLUAT_NAME != null)
        //    {
        //        renameTenvuviec(oNT.QUANHEPHAPLUAT_NAME, DONID);
        //    }
        //    else if (oNT.QUANHEPHAPLUATID != null)
        //    {
        //        decimal IDQHPL = Convert.ToDecimal(oNT.QUANHEPHAPLUATID.ToString());
        //        DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
        //        if (obj != null)
        //        {
        //            string quanhephapluat_name = obj.TEN.ToString();
        //            renameTenvuviec(quanhephapluat_name, DONID);
        //        }
        //    }
        //}
        private string getQHPL_NAME_THULY()
        {
            decimal ID = getcurrentid();
            ALD_SOTHAM_THULY oT = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == ID).FirstOrDefault();
            if (oT.QUANHEPHAPLUAT_NAME != null && oT.QUANHEPHAPLUAT_NAME != "")
            {
                return oT.QUANHEPHAPLUAT_NAME.ToString();
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) return obj.TEN.ToString();
                return "";
            }
            else
            {
                return "";
            }
        }
        private void txtQuanhephapluat_name(ALD_SOTHAM_BANAN oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ALD_SOTHAM_BANAN oTT = dt.ALD_SOTHAM_BANAN.Where(x => x.DONID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }
        private void txtQuanhephapluat_name(ALD_SOTHAM_THULY oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ALD_DON oTT = dt.ALD_DON.Where(x => x.ID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }

        private void txtQuanhephapluat_name(ALD_DON oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
                txtQuanhephapluat.Text = null;
        }

    }
}