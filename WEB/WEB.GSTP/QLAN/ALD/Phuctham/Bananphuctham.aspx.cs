﻿using BL.GSTP;
using BL.GSTP.Danhmuc;
using DAL.DKK;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.ALD.Phuctham
{
    public partial class Bananphuctham : System.Web.UI.Page
    {
        DKKContextContainer dkk = new DKKContextContainer();
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public bool GetNumber(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch { return false; }
        }
        public string GetTextDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return (Convert.ToDateTime(obj).ToString("dd/MM/yyyy", cul));
            }
            catch { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                    LoadDropQuanhephapluat();
                    string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                    if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
                    hddID.Value = current_id.ToString();
                    decimal ID = Convert.ToDecimal(current_id);
                    LoadDropKetQuaPhucTham();
                    LoadDropLyDoBanAn();
                    LoadBanAnInfo(ID);
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                    //Kiểm tra thẩm phán giải quyết đơn             
                    decimal DONID = Convert.ToDecimal(current_id);
                    GetTrangThaiBanDauDONKK_USER_DKNHANVB(DONID);
                    ALD_DON oT = dt.ALD_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    List<ALD_PHUCTHAM_THULY> lstCount = dt.ALD_PHUCTHAM_THULY.Where(x => x.DONID == DONID).ToList();
                    if (lstCount.Count == 0 || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.HOSO || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM)
                    {
                        lstErr.Text = "Chưa cập nhật thông tin thụ lý phúc thẩm!";
                        Cls_Comon.SetButton(cmdUpdate, false);
                        return;
                    }
                    List<ALD_DON_THAMPHAN> lstTP = dt.ALD_DON_THAMPHAN.Where(x => x.DONID == DONID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETPHUCTHAM).ToList();
                    if (lstTP.Count == 0)
                    {
                        lstErr.Text = "Chưa phân công thẩm phán giải quyết !";
                        Cls_Comon.SetButton(cmdUpdate, false);
                        return;
                    }
                    if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                    {
                        lstErr.Text = "Vụ việc đã được chuyển lên tòa án cấp trên, không được sửa đổi !";
                        Cls_Comon.SetButton(cmdUpdate, false);
                        return;
                    }
                    if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM)
                    {
                        lstErr.Text = "Vụ việc đã được chuyển xét xử lại cấp sơ thẩm, không được sửa đổi !";
                        Cls_Comon.SetButton(cmdUpdate, false);
                        return;
                    }
                    //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
                    //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
                    //int counttk = oCBDT.Rows.Count;
                    //if (counttk > 0)
                    //{
                    //    //là thư k
                    //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
                    //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
                    //    //int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
                    //    int countItem = dt.ALD_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == DONID && s.MAVAITRO == "VTTP_GIAIQUYETPHUCTHAM");
                    //    if (countItem > 0)
                    //    {
                    //        //được gán 
                    //    }
                    //    else
                    //    {
                    //        //không được gán
                    //        string StrMsg = "Người dùng không được sửa đổi thông tin của vụ việc do không được phân công giải quyết.";
                    //        lstErr.Text = StrMsg;
                    //        Cls_Comon.SetButton(cmdUpdate, false);
                    //        return;
                    //    }
                    //}
                }
            }
            catch (Exception ex) { lstErr.Text = ex.Message; }
        }
        private void LoadFile()
        {
            decimal ID = Convert.ToDecimal(hddID.Value);
            dgFile.CurrentPageIndex = 0;
            List<ALD_PHUCTHAM_BANAN_FILE> lst = dt.ALD_PHUCTHAM_BANAN_FILE.Where(x => x.BANANID == ID).ToList();
            if (lst != null && lst.Count > 0)
            {
                dgFile.DataSource = lst;
                dgFile.DataBind();
                dgFile.Visible = true;
            }
            else
            {
                dgFile.DataSource = null;
                dgFile.DataBind();
                dgFile.Visible = false;
            }
        }
        private void LoadBanAnInfo(decimal DonID)
        {
            ALD_PHUCTHAM_BANAN oT = dt.ALD_PHUCTHAM_BANAN.Where(x => x.DONID == DonID).FirstOrDefault<ALD_PHUCTHAM_BANAN>();
            if (oT != null)
            {
                ddlLoaiQuanhe.SelectedValue = oT.LOAIQUANHE.ToString();
                hddBanAnID.Value = oT.ID.ToString();
                txtQuanhephapluat_name(oT);
                if (oT.QHPLTKID != null)
                    ddlQHPLTK.SelectedValue = oT.QHPLTKID.ToString();
                txtSobanan.Text = oT.SOBANAN;
                txtNgaymophientoa.Text = string.IsNullOrEmpty(oT.NGAYMOPHIENTOA + "") ? DateTime.Now.ToString("dd/MM/yyyy", cul) : ((DateTime)oT.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                txtNgaytuyenan.Text = string.IsNullOrEmpty(oT.NGAYTUYENAN + "") ? "" : ((DateTime)oT.NGAYTUYENAN).ToString("dd/MM/yyyy", cul);
                txtNgayhieuluc.Text = string.IsNullOrEmpty(oT.NGAYHIEULUC + "") ? "" : ((DateTime)oT.NGAYHIEULUC).ToString("dd/MM/yyyy", cul);
              
                if (oT.ISVKSTHAMGIA != null) rdbVKSThamgia.SelectedValue = oT.ISVKSTHAMGIA.ToString();
            
           
                ddlKetQuaPhucTham.SelectedValue = oT.KETQUAPHUCTHAMID.ToString();
                LoadDropLyDoBanAn();
                ddlLyDoBanAn.SelectedValue = oT.LYDOBANANID.ToString();

                rdbAnle.SelectedValue = oT.APDUNGANLE.ToString();
                ddlYeutonuocngoai.SelectedValue = oT.YEUTONUOCNGOAI.ToString();
                rdVuAnQuaHan.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISQUAHAN + "")) ? "0" : oT.TK_ISQUAHAN.ToString();
                rdNNChuQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_CHUQUAN + "")) ? "0" : oT.TK_QUAHAN_CHUQUAN.ToString();
                rdNNKhachQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_KHACHQUAN + "")) ? "0" : oT.TK_QUAHAN_KHACHQUAN.ToString();

                rdVKSCoKN.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISVKSCOKN_KDCN + "")) ? "0" : oT.TK_ISVKSCOKN_KDCN.ToString();
                rdVKSRutKN.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISVKSRUTKN_DSKR + "")) ? "0" : oT.TK_ISVKSRUTKN_DSKR.ToString();

                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;

                //Load File
                LoadFile();
            }
            else
            {
                ALD_PHUCTHAM_THULY tlpt = dt.ALD_PHUCTHAM_THULY.Where(x => x.DONID == DonID).OrderByDescending(x => x.NGAYTHULY).FirstOrDefault();
                if (tlpt != null)
                {
                    if (tlpt.QHPLTKID != null)
                        ddlQHPLTK.SelectedValue = tlpt.QHPLTKID.ToString();
                    txtQuanhephapluat_name(tlpt);

                }
                ALD_DON oDon = dt.ALD_DON.Where(x => x.ID == DonID).FirstOrDefault();
                if (oDon != null)
                {
                    ddlLoaiQuanhe.SelectedValue = oDon.LOAIQUANHE.ToString();
                    if (oDon.QHPLTKID != null) ddlQHPLTK.SelectedValue = oDon.QHPLTKID.ToString();
                    ddlYeutonuocngoai.SelectedValue = oDon.YEUTONUOCNGOAI.ToString();
                }
                txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            }
        }
        private bool CheckValid()
        {
            if (txtQuanhephapluat.Text.Trim().Length >= 500)
            {
                lstErr.Text = "Quan hệ pháp luật nhập quá dài.";
                txtQuanhephapluat.Focus();
                return false;
            }
            if (txtQuanhephapluat.Text == null || txtQuanhephapluat.Text == "")
            {
                lstErr.Text = "Chưa nhập quan hệ pháp luật.";
                txtQuanhephapluat.Focus();
                return false;
            }

            if (ddlQHPLTK.SelectedIndex == 0)
            {
                lstErr.Text = "Chưa chọn quan hệ pháp luật dùng cho thống kê!";
                return false;
            }
            decimal IDChitieuTK = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
            if (dt.DM_QHPL_TK.Where(x => x.PARENT_ID == IDChitieuTK).ToList().Count > 0)
            {
                lstErr.Text = "Quan hệ pháp luật dùng cho thống kê chỉ được chọn mã con, bạn hãy chọn lại !";
                return false;
            }
            int lengthSoBanAn = txtSobanan.Text.Trim().Length;
            if (lengthSoBanAn == 0)
            {
                lstErr.Text = "Chưa nhập số bản án !";
                txtSobanan.Focus();
                return false;
            }
            if (lengthSoBanAn > 20)
            {
                lstErr.Text = "Số bản án không nhập quá 20 ký tự. Hãy nhập lại !";
                txtSobanan.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaymophientoa.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày mở phiên tòa hoặc không theo mẫu như sau: ngày/tháng/năm!";
                txtNgaymophientoa.Focus();
                return false;
            }
            if (Cls_Comon.IsValidDate(txtNgaytuyenan.Text) == false)
            {
                lstErr.Text = "Chưa nhập ngày tuyên án hoặc theo mẫu như sau: ngày/tháng/năm!";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (txtNgayhieuluc.Text.Trim() != "" && Cls_Comon.IsValidDate(txtNgayhieuluc.Text) == false)
            {
                lstErr.Text = "Bạn phải nhập ngày hiệu lực theo mẫu như sau: ngày/tháng/năm!";
                txtNgayhieuluc.Focus();
                return false;
            }
            if (ddlKetQuaPhucTham.SelectedIndex == 0)
            {
                lstErr.Text = "Chưa chọn kết quả bản án phúc thẩm !";
                return false;
            }
            if (ddlLyDoBanAn.SelectedIndex == 0)
            {
                lstErr.Text = "Chưa chọn lý do bản án phúc thẩm !";
                return false;
            }

            //----------------------------
            string so = txtSobanan.Text;
            if (!String.IsNullOrEmpty(txtNgaytuyenan.Text))
            {
                DateTime ngayBA = DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "ALD_PT", so, ngayBA);
                if (CheckID > 0)
                {
                    Decimal CurrBanAnId = (string.IsNullOrEmpty(hddBanAnID.Value)) ? 0 : Convert.ToDecimal(hddBanAnID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "ALD_PT", ngayBA).ToString();
                    if (CheckID != CurrBanAnId)
                    {
                        strMsg = "Số bản án " + txtSobanan.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSobanan.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSobanan.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        private void LoadDropQuanhephapluat()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
               ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBY2GROUPNAME(ENUM_DANHMUC.QUANHEPL_YEUCAU_LD, ENUM_DANHMUC.QUANHEPL_TRANHCHAP_LD);
            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();
            //Load QHPL Thống kê.
            ddlQHPLTK.DataSource = dt.DM_QHPL_TK.Where(x => x.STYLES == ENUM_QHPLTK.LAODONG && x.ENABLE == 1).OrderBy(y => y.ARRTHUTU).ToList();
            ddlQHPLTK.DataTextField = "CASE_NAME";
            ddlQHPLTK.DataValueField = "ID";
            ddlQHPLTK.DataBind();
            ddlQHPLTK.Items.Insert(0, new ListItem("--Chọn QHPL dùng thống kê--", "0"));
        }
        private void LoadDropKetQuaPhucTham()
        {
            ddlKetQuaPhucTham.Items.Clear();
            ddlKetQuaPhucTham.DataSource = dt.DM_KETQUA_PHUCTHAM.Where(x => x.ISALD == 1).OrderBy(y => y.THUTU).ToList();
            ddlKetQuaPhucTham.DataTextField = "TEN";
            ddlKetQuaPhucTham.DataValueField = "ID";
            ddlKetQuaPhucTham.DataBind();
            ddlKetQuaPhucTham.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        private void LoadDropLyDoBanAn()
        {
            ddlLyDoBanAn.Items.Clear();
            decimal KetQuaID = Convert.ToDecimal(ddlKetQuaPhucTham.SelectedValue);
            DM_KETQUA_PHUCTHAM_LYDO_BL kqptLyDoBL = new DM_KETQUA_PHUCTHAM_LYDO_BL();
            DataTable dtTable = kqptLyDoBL.DM_KETQUA_PT_LYDO_GETLIST(KetQuaID);
            if (dtTable != null && dtTable.Rows.Count > 0)
            {
                ddlLyDoBanAn.DataSource = dtTable;
                ddlLyDoBanAn.DataTextField = "TEN";
                ddlLyDoBanAn.DataValueField = "ID";
                ddlLyDoBanAn.DataBind();
            }
            ddlLyDoBanAn.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
        }
        protected void ddlLoaiQuanhe_SelectedIndexChanged(object sender, EventArgs e)
        {
         
            LoadDropQuanhephapluat();
        }
        protected void ddlQuanhephapluat_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal IDQHPL = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue);
            DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
            DM_DATAGROUP oGroup = dt.DM_DATAGROUP.Where(x => x.ID == obj.GROUPID).FirstOrDefault();
            if (oGroup.MA == ENUM_DANHMUC.QUANHEPL_TRANHCHAP_LD)
                ddlLoaiQuanhe.SelectedValue = "1";
            else
                ddlLoaiQuanhe.SelectedValue = "2";
        }
        protected void ddlKetQuaPhucTham_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { LoadDropLyDoBanAn(); } catch (Exception ex) { lstErr.Text = ex.Message; }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                bool isNew = false;
                ALD_PHUCTHAM_BANAN oND = dt.ALD_PHUCTHAM_BANAN.Where(x => x.DONID == DONID).FirstOrDefault<ALD_PHUCTHAM_BANAN>();
                if (oND == null)
                {
                    oND = new ALD_PHUCTHAM_BANAN(); isNew = true;
                    if (Session[ENUM_SESSION.SESSION_DONVIID] != null)
                    { oND.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]); }
                }
                oND.LOAIQUANHE = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue);

                oND.QUANHEPHAPLUATID = null;
                oND.QUANHEPHAPLUAT_NAME = txtQuanhephapluat.Text;
                //renameTenvuviec(txtQuanhephapluat.Text, DONID);

                oND.QHPLTKID = Convert.ToDecimal(ddlQHPLTK.SelectedValue);
                oND.SOBANAN = txtSobanan.Text;
                oND.NGAYMOPHIENTOA = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYTUYENAN = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYHIEULUC = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            
                oND.KETQUAPHUCTHAMID = Convert.ToDecimal(ddlKetQuaPhucTham.SelectedValue);
                oND.LYDOBANANID = Convert.ToDecimal(ddlLyDoBanAn.SelectedValue);
                oND.YEUTONUOCNGOAI = Convert.ToDecimal(ddlYeutonuocngoai.SelectedValue);

                oND.ISVKSTHAMGIA = rdbVKSThamgia.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbVKSThamgia.SelectedValue);
                oND.APDUNGANLE = rdbAnle.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbAnle.SelectedValue);
                oND.TK_ISQUAHAN = rdVuAnQuaHan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVuAnQuaHan.SelectedValue);
                oND.TK_QUAHAN_CHUQUAN = rdNNChuQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNChuQuan.SelectedValue);
                oND.TK_QUAHAN_KHACHQUAN = rdNNKhachQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNKhachQuan.SelectedValue);

                oND.TK_ISVKSCOKN_KDCN = rdVKSCoKN.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVKSCoKN.SelectedValue);
                oND.TK_ISVKSRUTKN_DSKR = rdVKSRutKN.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVKSRutKN.SelectedValue);

                if (isNew)
                {
                    oND.DONID = DONID;
                    oND.NGAYTAO = DateTime.Now;
                    oND.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ALD_PHUCTHAM_BANAN.Add(oND);
                }
                else
                {
                    oND.NGAYSUA = DateTime.Now;
                    oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                }
                TamNgungDONKK_USER_DKNHANVB(DONID);
                dt.SaveChanges();
                //Lưu file
                if (hddFilePath.Value != "")
                {
                    try
                    {
                        string strFilePath = "";
                        if (chkKySo.Checked)
                        {
                            string[] arr = hddFilePath.Value.Split('/');
                            strFilePath = arr[arr.Length - 1];
                            strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;

                            byte[] buff = null;
                            using (FileStream fs = File.OpenRead(strFilePath))
                            {
                                BinaryReader br = new BinaryReader(fs);
                                FileInfo oF = new FileInfo(strFilePath);

                                long numBytes = oF.Length;
                                buff = br.ReadBytes((int)numBytes);
                                ALD_PHUCTHAM_BANAN_FILE oTF = new ALD_PHUCTHAM_BANAN_FILE();
                                oTF.BANANID = DONID;
                                oTF.NOIDUNG = buff;
                                oTF.TENFILE = oF.Name;
                                oTF.KIEUFILE = oF.Extension;
                                oTF.NGAYTAO = DateTime.Now;
                                oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                                dt.ALD_PHUCTHAM_BANAN_FILE.Add(oTF);
                                dt.SaveChanges();

                            }
                            File.Delete(strFilePath);
                        }

                    }
                    catch (Exception ex) { }
                }
                //End
                LoadBanAnInfo(DONID);
                lstErr.Text = "Lưu thành công!";
            }
            catch (Exception ex)
            {
                lstErr.Text = "Lỗi: " + ex.Message;
            }
        }

        private void GetTrangThaiBanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 1);
            if (obj != null)
            {

                ttBanDauDONKK_USER_DKNHANVB.Value = obj.TRANGTHAI.Value.ToString();
            }
        }
        private void SetTrangThaibanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 3);
            if (obj != null)
            {

                obj.TRANGTHAI = Convert.ToDecimal(ttBanDauDONKK_USER_DKNHANVB.Value);
                dkk.SaveChanges();
            }
        }
        private void TamNgungDONKK_USER_DKNHANVB(decimal DONID)
        {
            ALD_DON oDon = dt.ALD_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_LAODONG && s.TRANGTHAI == 1);
            if (obj != null)
            {

                obj.TRANGTHAI = 3;
                dkk.SaveChanges();
            }
        }
        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (AsyncFileUpLoad.HasFile)
            {
                string strFileName = AsyncFileUpLoad.FileName;
                string path = Server.MapPath("~/TempUpload/") + strFileName;
                AsyncFileUpLoad.SaveAs(path);

                try
                {
                    decimal DONID = Convert.ToDecimal(hddID.Value);
                    string strFilePath = path;
                    byte[] buff = null;
                    using (FileStream fs = File.OpenRead(strFilePath))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        FileInfo oF = new FileInfo(strFilePath);
                        string strFN = oF.Name.ToLower();
                        //if (dt.ALD_PHUCTHAM_BANAN_FILE.Where(x => x.TENFILE.ToLower() == strFN).ToList().Count == 0)
                        //{
                            long numBytes = oF.Length;
                            buff = br.ReadBytes((int)numBytes);
                            ALD_PHUCTHAM_BANAN_FILE oTF = new ALD_PHUCTHAM_BANAN_FILE();
                            oTF.BANANID = DONID;
                            oTF.NOIDUNG = buff;
                            oTF.TENFILE = oF.Name;
                            oTF.KIEUFILE = oF.Extension;
                            oTF.NGAYTAO = DateTime.Now;
                            oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                            dt.ALD_PHUCTHAM_BANAN_FILE.Add(oTF);
                            dt.SaveChanges();
                        //}
                    }
                    File.Delete(strFilePath);
                }
                catch (Exception ex) { lstErr.Text = ex.Message; }
            }
        }

        protected void cmdThemFileTL_Click(object sender, EventArgs e)
        {
            SaveFile_KySo();
            LoadFile();
        }
        protected void cmd_load_form_Click(object sender, EventArgs e)
        {
            LoadFile();
            Load_CheckBox();
        }
        protected void Load_CheckBox()
        {
            if (chkKySo.Checked == true)
            {
                zonekythuong.Style.Add("Display", "none");
                zonekyso.Style.Add("Display", "block");
            }
            else
            {
                zonekythuong.Style.Add("Display", "block");
                zonekyso.Style.Add("Display", "none");
            }
        }
        void SaveFile_KySo()
        {
            string folder_upload = "/TempUpload/";
            string file_kyso = hddFilePath.Value;
            if (!String.IsNullOrEmpty(hddFilePath.Value))
            {
                String[] arr = file_kyso.Split('/');
                string file_name = arr[arr.Length - 1] + "";

                String file_path = Path.Combine(Server.MapPath(folder_upload), file_name);
                decimal DONID = Convert.ToDecimal(hddID.Value);
                ALD_PHUCTHAM_BANAN_FILE oTF = new ALD_PHUCTHAM_BANAN_FILE();

                byte[] buff = null;
                using (FileStream fs = File.OpenRead(file_path))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(file_path);
                    string strFN = oF.Name.ToLower();

                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.BANANID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.ALD_PHUCTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                //xoa file
                File.Delete(file_path);
            }
        }
        protected void dgFile_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false || cmdUpdate.Enabled == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    ALD_PHUCTHAM_BANAN_FILE oT = dt.ALD_PHUCTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.ALD_PHUCTHAM_BANAN_FILE.Remove(oT);
                    dt.SaveChanges();
                    LoadFile();
                    break;
                case "Download":
                    ALD_PHUCTHAM_BANAN_FILE oND = dt.ALD_PHUCTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                    }
                    break;
            }
        }
        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
                pnNguyenNhanQuaHan.Visible = true;
            else
                pnNguyenNhanQuaHan.Visible = false;
        }
        protected void txtNgaymophientoa_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtNgaymophientoa.Text))
            {
                if (String.IsNullOrEmpty(txtNgaytuyenan.Text))
                {
                    txtNgaytuyenan.Text = txtNgaymophientoa.Text;
                }
                if (String.IsNullOrEmpty(txtNgayhieuluc.Text))
                {
                    txtNgayhieuluc.Text = txtNgaymophientoa.Text;
                }
            }
        }

        protected void cmdHuyBanAn_Click(object sender, EventArgs e)
        {
            decimal DONID = Session[ENUM_LOAIAN.AN_LAODONG] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG] + "");
            //reset_TENVUVIEC(DONID);

            // Xóa thông tin bản án
            ALD_PHUCTHAM_BANAN banan = dt.ALD_PHUCTHAM_BANAN.Where(x => x.DONID == DONID).FirstOrDefault();
            if (banan != null)
            {
                dt.ALD_PHUCTHAM_BANAN.Remove(banan);
            }
            // Xóa thông tin file đính kèm
            List<ALD_PHUCTHAM_BANAN_FILE> files = dt.ALD_PHUCTHAM_BANAN_FILE.Where(x => x.BANANID == DONID).ToList();
            if (files.Count > 0)
            {
                dt.ALD_PHUCTHAM_BANAN_FILE.RemoveRange(files);
            }
            // Xóa thông tin người tham gia tố tụng
            List<ALD_PHUCTHAM_BANAN_TGTT> tGTTs = dt.ALD_PHUCTHAM_BANAN_TGTT.Where(x => x.DONID == DONID).ToList();
            if (tGTTs.Count > 0)
            {
                dt.ALD_PHUCTHAM_BANAN_TGTT.RemoveRange(tGTTs);
            }
            SetTrangThaibanDauDONKK_USER_DKNHANVB(DONID);
            dt.SaveChanges();
            ResetControl();
            lstErr.Text = "Xóa bản án thành công!";
        }
        private void ResetControl()
        {
            txtQuanhephapluat.Text = getQHPL_NAME_THULY();
            ddlQuanhephapluat.SelectedIndex = 0;
            ddlLoaiQuanhe.SelectedIndex = 0;
            ddlQHPLTK.SelectedIndex = 0;
            txtSobanan.Text = "";
            txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtNgaytuyenan.Text = "";
            txtNgayhieuluc.Text = "";
            ddlKetQuaPhucTham.SelectedIndex = 0;
            ddlLyDoBanAn.SelectedIndex = 0;
            ddlYeutonuocngoai.SelectedIndex = 0;
            rdbAnle.ClearSelection();
            rdbVKSThamgia.ClearSelection();
            rdVKSCoKN.ClearSelection();
            rdVKSRutKN.ClearSelection();
            rdVuAnQuaHan.ClearSelection();
            rdNNChuQuan.ClearSelection();
            rdNNKhachQuan.ClearSelection();
            lstErr.Text = "";
            LoadFile();
        }

        private decimal getcurrentid()
        {
            string current_id = Session[ENUM_LOAIAN.AN_LAODONG] + "";
            if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
            return Convert.ToDecimal(current_id);
        }
        //private void renameTenvuviec(string obj, decimal DONID)
        //{
        //    ALD_DON oT = dt.ALD_DON.Where(x => x.ID == DONID).FirstOrDefault();
        //    ALD_DON_DUONGSU nguyendon = dt.ALD_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "NGUYENDON").FirstOrDefault();
        //    ALD_DON_DUONGSU bidon = dt.ALD_DON_DUONGSU.Where(x => x.DONID == DONID && x.TUCACHTOTUNG_MA == "BIDON").FirstOrDefault();

        //    oT.TENVUVIEC = nguyendon.TENDUONGSU + " - " + bidon.TENDUONGSU + " - " + obj;
        //}
        //private void reset_TENVUVIEC(decimal DONID)
        //{
        //    ALD_PHUCTHAM_THULY oNT = dt.ALD_PHUCTHAM_THULY.Where(x => x.DONID == DONID).FirstOrDefault();
        //    if (oNT.QUANHEPHAPLUAT_NAME != null)
        //    {
        //        renameTenvuviec(oNT.QUANHEPHAPLUAT_NAME, DONID);
        //    }
        //    else if (oNT.QUANHEPHAPLUATID != null)
        //    {
        //        decimal IDQHPL = Convert.ToDecimal(oNT.QUANHEPHAPLUATID.ToString());
        //        DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
        //        if (obj != null)
        //        {
        //            string quanhephapluat_name = obj.TEN.ToString();
        //            renameTenvuviec(quanhephapluat_name, DONID);
        //        }
        //    }
        //}
        private string getQHPL_NAME_THULY()
        {
            decimal ID = getcurrentid();
            ALD_PHUCTHAM_THULY oT = dt.ALD_PHUCTHAM_THULY.Where(x => x.DONID == ID).FirstOrDefault();
            if (oT.QUANHEPHAPLUAT_NAME != null && oT.QUANHEPHAPLUAT_NAME != "")
            {
                return oT.QUANHEPHAPLUAT_NAME.ToString();
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) return obj.TEN.ToString();
                return "";
            }
            else
            {
                return "";
            }
        }
        private void txtQuanhephapluat_name(ALD_PHUCTHAM_BANAN oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ALD_PHUCTHAM_BANAN oTT = dt.ALD_PHUCTHAM_BANAN.Where(x => x.DONID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }
        private void txtQuanhephapluat_name(ALD_PHUCTHAM_THULY oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
            {
                ALD_DON oTT = dt.ALD_DON.Where(x => x.ID == oT.DONID).FirstOrDefault();
                if (oTT.QUANHEPHAPLUAT_NAME != null)
                {
                    txtQuanhephapluat.Text = oTT.QUANHEPHAPLUAT_NAME;
                }
                else if (oTT.QUANHEPHAPLUATID != null)
                {
                    decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                    DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                    if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
                }
                else
                    txtQuanhephapluat.Text = null;
            }
        }
        private void txtQuanhephapluat_name(ALD_DON oT)
        {
            if (oT.QUANHEPHAPLUAT_NAME != null)
            {
                txtQuanhephapluat.Text = oT.QUANHEPHAPLUAT_NAME;
            }
            else if (oT.QUANHEPHAPLUATID != null && oT.QUANHEPHAPLUATID != 0)
            {
                decimal IDQHPL = Convert.ToDecimal(oT.QUANHEPHAPLUATID.ToString());
                DM_DATAITEM obj = dt.DM_DATAITEM.Where(x => x.ID == IDQHPL).FirstOrDefault();
                if (obj != null) txtQuanhephapluat.Text = obj.TEN.ToString();
            }
            else
                txtQuanhephapluat.Text = null;
        }



    }
}