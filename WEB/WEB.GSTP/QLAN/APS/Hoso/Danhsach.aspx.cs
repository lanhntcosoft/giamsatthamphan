﻿using BL.GSTP;
using BL.GSTP.APS;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.DKK;

namespace WEB.GSTP.QLAN.APS.Hoso
{
    public partial class Danhsach : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");

        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch
            { return false; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string strSearch = Session["textsearch"] + "";
                if (strSearch != "")
                {
                    txtTenVuViec.Text = strSearch;
                    Session["textsearch"] = "";
                }

                LoadCombobox();
                Load_Data();
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    cmdThemmoi.Visible = false;
                }
                else
                {
                    Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
                }
            }
        }
        private void CheckChucDanhUser(ref decimal vCheckTk)
        {
            //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            //int counttk = oCBDT.Rows.Count;
            //if (counttk > 0)
            //{
            //    //là thư ký
            //    //kiểm tra user có thuộc hCTP hay không

            //    //kiểm  tra có phải là thư ký hay không


            //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
            //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
            //    int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
            //    if (count > 0)
            //    {
            //        //là thư ký của HCTP
            //        vCheckTk = 0;
            //    }
            //    else
            //    {
            //        vCheckTk = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
            //        //không là thư ký của hành chính tư pháp
            //        ///kiếm tra thư ký có quyền được xem hay không
            //        //foreach (DataRow item in dataTable.Rows)
            //        //{
            //        //    decimal DonID = Convert.ToDecimal(item["ID"]);
            //        //    int countItem = dt.ADS_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == DonID);
            //        //    if (countItem > 0)
            //        //    {
            //        //        //thư ký được xem vụ án này

            //        //    }
            //        //    else
            //        //    {
            //        //        //thư ký không được xem vụ án này
            //        //        item.Delete();
            //        //    }
            //        //}
            //        //dataTable.AcceptChanges();
            //    }
            //}
            //else
            //{
            //    vCheckTk = 0;
            //}

        }

        private void LoadCombobox()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.QUANHEPL_YEUCAUPS);
            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();
            ddlQuanhephapluat.Items.Insert(0, new ListItem("-- Tất cả --", "0"));
            //--------------------
            LoadDropThamphan();
        }
        void LoadDropThamphan()
        {
            Boolean IsLoadAll = true;
            ddlThamphan.Items.Clear();
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            // Kiểm tra nếu user login là thẩm phán thì chỉ load 1 user
            // nếu là chánh án, phó chánh án hoặc khác thẩm phán thì load all
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault<DM_CANBO>();
            if (oCB != null)
            {
                // Kiểm tra chức danh có là thẩm phán hay không
                if (oCB.CHUCDANHID != null && oCB.CHUCDANHID != 0)
                {
                    DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == oCB.CHUCDANHID).FirstOrDefault();
                    if (oCD.MA.Contains("TP"))
                    {
                        ddlThamphan.Items.Add(new ListItem(oCB.HOTEN, oCB.ID.ToString()));
                        IsLoadAll = false;
                    }
                }
                // Kiểm tra chức vụ có là Chánh án hoặc phó chánh án hay không
                if (oCB.CHUCVUID != null && oCB.CHUCVUID != 0)
                {
                    DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == oCB.CHUCVUID).FirstOrDefault();
                    if (oCD.MA.Contains("CA"))
                    {
                        IsLoadAll = true;
                    }
                }
            }
            if (IsLoadAll)
            {
                DM_CANBO_BL objBL = new DM_CANBO_BL();
                decimal LoginDonViID = Session[ENUM_SESSION.SESSION_DONVIID] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                DataTable tbl = objBL.DM_CANBO_GETBYDONVI_CHUCDANH(LoginDonViID, ENUM_CHUCDANH.CHUCDANH_THAMPHAN);
                ddlThamphan.DataSource = tbl;
                ddlThamphan.DataTextField = "HOTEN";
                ddlThamphan.DataValueField = "ID";
                ddlThamphan.DataBind();
                ddlThamphan.Items.Insert(0, new ListItem("-- Tất cả --", "0"));
            }
        }
        private void Load_Data()
        {
            decimal vchecktk = 0;
            CheckChucDanhUser(ref vchecktk);
            APS_DON_BL oBL = new APS_DON_BL();
            decimal vDonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]),
                    ThamPhanId = Convert.ToDecimal(ddlThamphan.SelectedValue),
                    LoaiQuanHe = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue),
                    QHPL = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue),
                    GiaiDoan = Convert.ToDecimal(ddlGiaiDoan.SelectedValue),
                    HinhThucNhanDon = Convert.ToDecimal(ddlHinhthucnhandon.SelectedValue),
                    dSothutu = txtSothutu.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSothutu.Text.Trim()),
                    PhanCongTP = Convert.ToDecimal(ddlPhanCongTP.SelectedValue);
            string vUTTP = dropUTTP.SelectedValue;
            DateTime? dFrom = (String.IsNullOrEmpty(txtTuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault),
                      dTo = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            string MaVuViec = txtMaVuViec.Text.Trim(),
                   TenVuViec = txtTenVuViec.Text.Trim(),
                   Tenduongsu = txtTenduongsu.Text.Trim();
            int page_size = Convert.ToInt32(ddlPageCount.SelectedValue),
                pageindex = Convert.ToInt32(hddPageIndex.Value),
                count_all = 0;
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            int count = oCBDT.Rows.Count;
            decimal ThuKyID = 0;
            if (count > 0)
            {
                ThuKyID = Convert.ToDecimal(oCBDT.Rows[0]["ID"]);
            }
            DataTable oDT = oBL.APS_DON_SEARCH(vDonViID, MaVuViec, TenVuViec, dFrom, dTo, LoaiQuanHe, QHPL, dSothutu, Tenduongsu, GiaiDoan, HinhThucNhanDon, ThamPhanId, ThuKyID, PhanCongTP, vUTTP,vchecktk, pageindex, page_size);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(ddlPageCount.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }
            dgList.PageSize = page_size;
            dgList.DataSource = oDT;
            dgList.DataBind();
        }


        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            Load_Data();
        }


        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            //---huy vu an da ghim
            Decimal IDVuViec = 0;
            decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
            oNSD.IDANPHASAN = IDVuViec;
            dt.SaveChanges();
            Session[ENUM_LOAIAN.AN_PHASAN] = IDVuViec;
            //-----------------------
            Session["PS_THEMDSK"] = null;
            Response.Redirect("Thongtindon.aspx?type=new");

        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal IDVuViec = Convert.ToDecimal(e.CommandArgument.ToString());
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Select"://Lựa chọn vụ việc cần Lưu thông tin

                    //Lưu vào người dùng
                    decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
                    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == oNSD.DONVIID.ToString())
                    {
                        oNSD.IDANPHASAN = IDVuViec;
                        dt.SaveChanges();
                    }
                    Session[ENUM_LOAIAN.AN_PHASAN] = IDVuViec;
                    APS_DON oDon = dt.APS_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                    if (oDon.TOAANID == oNSD.DONVIID && (oDon.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oDon.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT))
                        Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgDSDON", "Vụ việc đã được chuyển lên cấp trên, các thông tin sẽ không được phép thay đổi !", Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    else
                        Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    //Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgDSDON", "Bạn đã chọn vụ việc, tiếp theo hãy chọn chức năng cần thao tác trong danh sách bên trái !", Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    break;
                case "Sua":
                    Response.Redirect("Thongtindon.aspx?type=list&ID=" + e.CommandArgument.ToString());
                    break;
                case "Xoa":
                    decimal VuAnID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_PHASAN]);
                    APS_DON oT = dt.APS_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                    string strUserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    if (Session[ENUM_SESSION.SESSION_NHOMNSDID]+""=="1")
                    {
                        APS_DON_BL oBL = new APS_DON_BL();
                        if (oBL.DELETE_ALLDATA_BY_VUANID(IDVuViec + "") == true)
                        {
                            //anhvh add 26/06/2020
                            GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                            GD.GIAIDOAN_DELETES("7", IDVuViec,2);
                            if (oT.HINHTHUCNHANDON == 3)//Trực tuyến
                            {
                                //la don truc tuyen --> cho phep phan loai lai donkk
                                try { PhanLoaiLai_DonKK(oPer, IDVuViec); } catch { }
                            }
                        }
                        else
                        {
                            lbtthongbao.Text = "Lỗi khi xóa toàn bộ thông tin vụ việc !";
                            break;
                        }
                    }
                    else
                    {
                        Xoa_An(oPer, IDVuViec);
                        if (IDVuViec == VuAnID)
                            Session[ENUM_LOAIAN.AN_PHASAN] = 0;
                    }
                    decimal IDUser_ = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    //để sửa lỗi mất menu khi xóa vụ án, anhvh add trường hợp xóa vụ án và uppdate lại idvuan =0 để giải phóng việc gim vụ án
                    QT_NGUOISUDUNG oNSD_ = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser_).FirstOrDefault();
                    if (oNSD_.IDANPHASAN == IDVuViec)
                    {
                        oNSD_.IDANPHASAN = 0;
                        dt.SaveChanges();
                    }
                    hddPageIndex.Value = "1";
                    Load_Data();
                    break;
            }
        }
        void Xoa_An(MenuPermission oPer, decimal IDVuViec)
        {
            if (oPer.XOA == false)
            {
                lbtthongbao.Text = "Bạn không có quyền xóa!";
                return;
            }
            APS_DON oT = dt.APS_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
            if (oT != null)
            {
                int GiaiDoan = (int)oT.MAGIAIDOAN;
                if (GiaiDoan == ENUM_GIAIDOANVUAN.SOTHAM)
                {
                    #region Kiểm tra dữ liệu liên quan
                    // Kiểm tra Án phí
                    APS_ANPHI anphi = dt.APS_ANPHI.Where(x => x.DONID == IDVuViec).FirstOrDefault<APS_ANPHI>();
                    if (anphi != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu thông tin biên lai án phí, không được phép xóa!";
                        return;
                    }
                    // Kiểm tra giải quyết đơn
                    APS_DON_XULY xld = dt.APS_DON_XULY.Where(x => x.DONID == IDVuViec).FirstOrDefault<APS_DON_XULY>();
                    if (xld != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu giải quyết đơn, không được phép xóa!";
                        return;
                    }
                    // Kiểm tra thẩm phán giải quyết đơn
                    APS_DON_THAMPHAN gqd = dt.APS_DON_THAMPHAN.Where(x => x.DONID == IDVuViec && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETDON).FirstOrDefault<APS_DON_THAMPHAN>();
                    if (gqd != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu thẩm phán giải quyết đơn. Không được xóa.";
                        return;
                    }
                    // Kiểm tra Giao nhận tài liệu chứng cứ
                    APS_DON_TAILIEU tailieu = dt.APS_DON_TAILIEU.Where(x => x.DONID == IDVuViec).FirstOrDefault<APS_DON_TAILIEU>();
                    if (tailieu != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu giao nhận tài liệu chứng cứ, không được phép xóa!";
                        return;
                    }
                    // Kiểm tra người tham gia tố tụng khác
                    APS_DON_THAMGIATOTUNG tgtt = dt.APS_DON_THAMGIATOTUNG.Where(x => x.DONID == IDVuViec).FirstOrDefault<APS_DON_THAMGIATOTUNG>();
                    if (tgtt != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu người tham gia tố tụng khác, không được phép xóa!";
                        return;
                    }
                    // Kiểm tra danh sách đương sự
                    APS_DON_DUONGSU ds = dt.APS_DON_DUONGSU.Where(x => x.DONID == IDVuViec).FirstOrDefault<APS_DON_DUONGSU>();
                    if (ds != null)
                    {
                        lbtthongbao.Text = "Vụ việc đã có dữ liệu trong danh sách đương sự, không được phép xóa!";
                        return;
                    }
                    #endregion
                    List<APS_FILE> lstF = dt.APS_FILE.Where(x => x.DONID == IDVuViec).ToList<APS_FILE>();
                    if (lstF.Count > 0)
                    {
                        foreach (APS_FILE f in lstF)
                        {
                            dt.APS_FILE.Remove(f);
                        }
                        dt.SaveChanges();
                    }
                    dt.APS_DON.Remove(oT);
                    dt.SaveChanges();
                    //anhvh add 26/06/2020
                    GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                    GD.GIAIDOAN_DELETES("7", IDVuViec,2);
                    //---------------------------
                    if (oT.HINHTHUCNHANDON == 3)
                    {
                        //la don truc tuyen --> cho phep phan loai lai donkk
                        try { PhanLoaiLai_DonKK(oPer, IDVuViec); } catch (Exception ex) { }
                    }
                }
            }
        }

        void PhanLoaiLai_DonKK(MenuPermission oPer, decimal IDVuViec)
        {
            String MaLoaiVuAn = ENUM_LOAIAN.AN_PHASAN;
            DAL.DKK.DKKContextContainer dt = new DAL.DKK.DKKContextContainer();
            DAL.DKK.DONKK_DON obj = dt.DONKK_DON.Where(x => x.VUANID == IDVuViec
                                                         && x.MALOAIVUAN == MaLoaiVuAn).Single<DAL.DKK.DONKK_DON>();
            if (obj != null)
            {
                obj.VUANID = 0;
                obj.TRANGTHAI = 0;//da gui don nhung chua phan loai
                obj.MALOAIVUAN = MaLoaiVuAn;
                obj.NGAYSUA = DateTime.Now;
            }
            dt.SaveChanges();
        }


        #region "Phân trang"

        protected void lbTBack_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            Load_Data();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            Load_Data();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            Load_Data();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            Load_Data();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            hddPageIndex.Value = lbCurrent.Text;
            Load_Data();
        }

        protected void ddlPageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount2.SelectedValue = ddlPageCount.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void ddlPageCount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount.SelectedValue = ddlPageCount2.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        #endregion

        protected void ddlLoaiQuanhe_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombobox();
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                lbtXoa.Visible = false;
                HiddenField hddMAGIAIDOAN = (HiddenField)e.Item.FindControl("hddMAGIAIDOAN");
                string strUserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                if (hddMAGIAIDOAN.Value == ENUM_GIAIDOANVUAN.SOTHAM.ToString() || Session[ENUM_SESSION.SESSION_NHOMNSDID]+""=="1")
                {
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
                }
                DataRowView dv = (DataRowView)e.Item.DataItem;
                decimal VuAnID = Convert.ToDecimal(dv["ID"] + "");
                string Result = new APS_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, "");
                lblSua.Visible = true;
                if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    lblSua.Visible = false;
                }
                if (Result != "")
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
            }
        }
    }
}