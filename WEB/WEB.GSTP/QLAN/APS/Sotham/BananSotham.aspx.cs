﻿using BL.GSTP;
using BL.GSTP.APS;
using DAL.DKK;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.APS.Sotham
{
    public partial class BananSotham : System.Web.UI.Page
    {
        DKKContextContainer dkk = new DKKContextContainer();
        GSTPContext dt = new GSTPContext();
        private static CultureInfo cul = new CultureInfo("vi-VN");
        public static bool GetNumber(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch
            { return false; }
        }
        public static string GetTextDate(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return "";
                else
                    return (Convert.ToDateTime(obj).ToString("dd/MM/yyyy", cul));
            }
            catch
            { return ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadCombobox();
                    hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                    string current_id = Session[ENUM_LOAIAN.AN_PHASAN] + "";
                    if (current_id == "") Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/APS/Hoso/Danhsach.aspx");
                    hddID.Value = current_id.ToString();
                    decimal ID = Convert.ToDecimal(current_id);
                    CheckQuyen(ID);
                    LoadBanAnInfo(ID);
                    LoadAnPhi();
                    APS_DON_THAMPHAN TpGQDon = dt.APS_DON_THAMPHAN.Where(x => x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM && x.DONID == ID).OrderByDescending(x => x.NGAYNHANPHANCONG).FirstOrDefault();
                    if (TpGQDon != null)
                    {
                        hddNgayPCTPGQD.Value = TpGQDon.NGAYNHANPHANCONG + "" == "" ? "" : ((DateTime)TpGQDon.NGAYNHANPHANCONG).ToString("dd/MM/yyyy");
                    }
                    GetTrangThaiBanDauDONKK_USER_DKNHANVB(ID);
                }
                LoadFile();
            }
            catch (Exception ex) { lstErr.Text = ex.Message; }
        }
        private void CheckQuyen(decimal ID)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdAnphi, oPer.CAPNHAT);
            Cls_Comon.SetButton(cmdHuyQuyetDinh, oPer.CAPNHAT);
            APS_DON oT = dt.APS_DON.Where(x => x.ID == ID).FirstOrDefault();
            if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
            {
                lstErr.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }
            List<APS_SOTHAM_THULY> lstCount = dt.APS_SOTHAM_THULY.Where(x => x.DONID == ID).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin thụ lý sơ thẩm !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }

            //Kiểm tra xem đã có quyết định đưa vụ việc ra xét xử hay không
            decimal IDLQD = 0;
            DM_QD_LOAI oLQD = dt.DM_QD_LOAI.Where(x => x.MA == "MTTPS").FirstOrDefault();
            if (oLQD != null) IDLQD = oLQD.ID;
            
            List<APS_SOTHAM_QUYETDINH> lstQDXX = dt.APS_SOTHAM_QUYETDINH.Where(x => x.DONID == ID && x.LOAIQDID == IDLQD).ToList();
            List<APS_SOTHAM_QUYETDINH> lstQDXX2 = dt.APS_SOTHAM_QUYETDINH.Where(x => x.DONID == ID && x.QUYETDINHID == 124).ToList();
            
            if (lstQDXX.Count == 0 && lstQDXX2.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật quyết định mở thủ tục phá sản !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }
            //--Kiểm tra đã phân công thẩm phán chủ tọa
            List<APS_SOTHAM_HDXX> lstTPCT = dt.APS_SOTHAM_HDXX.Where(x => x.DONID == ID && x.MAVAITRO == ENUM_NGUOITIENHANHTOTUNG.THAMPHAN).ToList();
            if (lstCount.Count == 0)
            {
                lstErr.Text = "Chưa cập nhật thông tin hội đồng xét xử !";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }
            APS_SOTHAM_KHANGCAO kc = dt.APS_SOTHAM_KHANGCAO.Where(x => x.DONID == ID).FirstOrDefault();
            if (kc != null)
            {
                lstErr.Text = "Vụ việc đã có khiếu nại. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }
            APS_SOTHAM_KHANGNGHI kn = dt.APS_SOTHAM_KHANGNGHI.Where(x => x.DONID == ID).FirstOrDefault();
            if (kn != null)
            {
                lstErr.Text = "Vụ việc đã có kháng nghị. Không được sửa đổi.";
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new APS_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
            if (Result != "")
            {
                lstErr.Text = Result;
                Cls_Comon.SetButton(cmdUpdate, false);
                Cls_Comon.SetButton(cmdAnphi, false);
                Cls_Comon.SetButton(cmdHuyQuyetDinh, false);
                hddShowCommand.Value = "False";
                return;
            }

            //DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            //DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            //int counttk = oCBDT.Rows.Count;
            //if (counttk > 0)
            //{
            //    //là thư k
            //    decimal IdNhomNguoiSuDung = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_NHOMNSDID]);
            //    decimal CurrentUserId = (decimal)Session[ENUM_SESSION.SESSION_CANBOID];
            //    //int count = dt.QT_NHOMNGUOIDUNG.Count(s => s.ID == IdNhomNguoiSuDung && (s.TEN.Contains("HCTP") || s.TEN.Contains("TAND")));
            //    int countItem = dt.APS_DON_THAMPHAN.Count(s => s.THUKYID == CurrentUserId && s.DONID == ID && s.MAVAITRO == "VTTP_GIAIQUYETSOTHAM");
            //    if (countItem > 0)
            //    {
            //        //được gán 
            //    }
            //    else
            //    {
            //        //không được gán
            //        StrMsg = "Người dùng không được sửa đổi thông tin của vụ việc do không được phân công giải quyết.";
            //        lstErr.Text = StrMsg;
            //        Cls_Comon.SetButton(cmdUpdate, false);
            //        Cls_Comon.SetButton(cmdAnphi, false);
            //        hddShowCommand.Value = "False";
            //        return;
            //    }
            //}
        }
        private void LoadAnPhi()
        {
            string current_id = Session[ENUM_LOAIAN.AN_PHASAN] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            APS_SOTHAM_BL oBL = new APS_SOTHAM_BL();
            DataTable dtAnPhi = oBL.APS_SOTHAM_BANAN_ANPHI_GET(DONID);
            if (dtAnPhi != null && dtAnPhi.Rows.Count > 0)
            {
                hddTGTTRowLastIndex.Value = dtAnPhi.Rows.Count + "";
            }
            dgAnPhi.DataSource = dtAnPhi;
            dgAnPhi.DataBind();
            foreach (DataGridItem oItem in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                if (chkMien.Checked)
                {
                    txtAnphi.Text = "";
                    txtAnphi.Enabled = false;
                }
                else
                {
                    txtAnphi.Enabled = true;
                }
            }
        }
        protected void dgAnPhi_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtNgaynhanbanan = (TextBox)e.Item.FindControl("txtNgaynhanbanan");
                if (hddTGTTRowLastIndex.Value == (dgAnPhi.Items.Count + 1) + "")
                {
                    txtNgaynhanbanan.Attributes.Add("onfocus", "myFunctionFocus();");
                }
            }
        }
        private void LoadFile()
        {
            decimal ID = Convert.ToDecimal(hddID.Value);
            dgFile.DataSource = dt.APS_SOTHAM_BANAN_FILE.Where(x => x.DONID == ID).ToList();
            dgFile.DataBind();
            string current_id = Session[ENUM_LOAIAN.AN_PHASAN] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            APS_DON oT = dt.APS_DON.Where(x => x.ID == DONID).FirstOrDefault();
            int ma_gd = (int)oT.MAGIAIDOAN;
            foreach (DataGridItem item in dgFile.Items)
            {
                LinkButton lbtXoa = (LinkButton)item.FindControl("lbtXoa");
                if (ma_gd == (int)ENUM_GIAIDOANVUAN.PHUCTHAM || ma_gd == (int)ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    Cls_Comon.SetLinkButton(lbtXoa, false);
                }
                if (hddShowCommand.Value == "False")
                {
                    lbtXoa.Visible = false;
                }
            }
        }
        private void LoadBanAnInfo(decimal DonID)
        {
            List<APS_SOTHAM_BANAN> lst = dt.APS_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (lst.Count > 0)
            {
                APS_SOTHAM_BANAN oT = lst[0];
                hddBanAnID.Value = oT.ID.ToString();
                txtSobanan.Text = oT.SOBANAN;
                ddlLoaiQuanhe.SelectedValue = oT.LOAIQUANHE.ToString();

                ddlQuanhephapluat.SelectedValue = oT.QUANHEPHAPLUATID.ToString();
                if (oT.NGAYMOPHIENTOA != null)
                {
                    txtNgaymophientoa.Text = ((DateTime)oT.NGAYMOPHIENTOA).ToString("dd/MM/yyyy", cul);
                }
                else
                {
                    txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                }
                if (oT.NGAYTUYENAN != null) txtNgaytuyenan.Text = ((DateTime)oT.NGAYTUYENAN).ToString("dd/MM/yyyy", cul);
                if (oT.NGAYHIEULUC != null) txtNgayhieuluc.Text = ((DateTime)oT.NGAYHIEULUC).ToString("dd/MM/yyyy", cul);

                ddlYeutonuocngoai.SelectedValue = oT.YEUTONUOCNGOAI.ToString();
                rdVuAnQuaHan.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISQUAHAN + "")) ? "0" : oT.TK_ISQUAHAN.ToString();
                rdNNChuQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_CHUQUAN + "")) ? "0" : oT.TK_QUAHAN_CHUQUAN.ToString();
                rdNNKhachQuan.SelectedValue = (string.IsNullOrEmpty(oT.TK_QUAHAN_KHACHQUAN + "")) ? "0" : oT.TK_QUAHAN_KHACHQUAN.ToString();

                txtTKCamDNNN.Text = oT.TK_SONGUOIBICAM_DNNN + "" == "" ? "" : ((decimal)oT.TK_SONGUOIBICAM_DNNN).ToString("#,0.###", cul);
                txtTKCamHTX.Text = oT.TK_SONGUOIBICAM_HTX + "" == "" ? "" : ((decimal)oT.TK_SONGUOIBICAM_HTX).ToString("#,0.###", cul);

                txtTKTongtaisan.Text = oT.TK_TONGGIATRINGHIAVUTAISAN + "" == "" ? "" : ((decimal)oT.TK_TONGGIATRINGHIAVUTAISAN).ToString("#,0.###", cul);
                txtTKTHuhoi.Text = oT.TK_TONGGIATRITHUHOI + "" == "" ? "" : ((decimal)oT.TK_TONGGIATRITHUHOI).ToString("#,0.###", cul); ;

                rdbIsNiemyet.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISDOANHNGHIEPNIEMYET + "")) ? "0" : oT.TK_ISDOANHNGHIEPNIEMYET.ToString();
                rdbIsDNMoi3nam.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISDOANHNGHIEP3NAM + "")) ? "0" : oT.TK_ISDOANHNGHIEP3NAM.ToString();
                rdbIsQDDCTT.SelectedValue = (string.IsNullOrEmpty(oT.TK_ISDINHCHITHUTUCPHKD + "")) ? "0" : oT.TK_ISDINHCHITHUTUCPHKD.ToString();

                if (oT.TK_ISRUTGON == 1)
                    rdbTBPS.SelectedValue = "0";

                if (oT.TK_ISHNCN_KHONGTHANH == 1)
                    rdbTBPS.SelectedValue = "1";

                if (oT.TK_ISTHEONQHNCN == 1)
                    rdbTBPS.SelectedValue = "2";
                if (oT.TK_ISKHONGXAYDUNGPAPHKD == 1)
                    rdbTBPS.SelectedValue = "3";
                if (oT.TK_ISKHONGTHONGQUA == 1)
                    rdbTBPS.SelectedValue = "4";
                if (oT.TK_ISKHONGTHUCHIEN == 1)
                    rdbTBPS.SelectedValue = "5";
                if (oT.TK_ISTOCHUCTINDUNG == 1)
                    rdbTBPS.SelectedValue = "6";
                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;
                //Load File
                LoadFile();
            }
            else
            {
                APS_DON oDon = dt.APS_DON.Where(x => x.ID == DonID).FirstOrDefault();
                if (oDon != null)
                {
                    ddlLoaiQuanhe.SelectedValue = oDon.LOAIQUANHE.ToString();
                    ddlQuanhephapluat.SelectedValue = oDon.QUANHEPHAPLUATID.ToString();
                    ddlYeutonuocngoai.SelectedValue = oDon.YEUTONUOCNGOAI.ToString();
                }
                txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            }
        }
        private bool CheckValid()
        {

            //if (ddlQuanhephapluat.Items.Count == 0)
            //{
            //    lstErr.Text = "Chưa chọn quan hệ pháp luật !";
            //    return false;
            //}
            if (txtSobanan.Text == "")
            {
                lstErr.Text = "Bạn chưa nhập số quyết định !";
                txtSobanan.Focus();
                return false;
            }
            DateTime NgayTPGQD = DateTime.MinValue, NgayMoPhienHop = DateTime.MinValue;
            if (hddNgayPCTPGQD.Value != "")
            {
                NgayTPGQD = DateTime.Parse(hddNgayPCTPGQD.Value, cul, DateTimeStyles.NoCurrentDateDefault);
            }
            if (txtNgaymophientoa.Text != "")
            {
                if (Cls_Comon.IsValidDate(txtNgaymophientoa.Text) == false)
                {
                    lstErr.Text = "Bạn phải nhập ngày mở phiên họp theo mẫu như sau: ngày/tháng/năm !";
                    txtNgaymophientoa.Focus();
                    return false;
                }
                NgayMoPhienHop = DateTime.Parse(txtNgaymophientoa.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                if (NgayMoPhienHop > DateTime.Now)
                {
                    lstErr.Text = "Ngày mở phiên họp không được lớn hơn ngày hiện tại !";
                    txtNgaymophientoa.Focus();
                    return false;
                }
                if (NgayTPGQD != DateTime.MinValue && NgayMoPhienHop < NgayTPGQD)
                {
                    lstErr.Text = "Ngày mở phiên họp không được nhỏ hơn ngày phân công thẩm phán giải quyết đơn " + hddNgayPCTPGQD.Value + ".";
                    txtNgaymophientoa.Focus();
                    return false;
                }
            }
            if (Cls_Comon.IsValidDate(txtNgaytuyenan.Text) == false)
            {
                lstErr.Text = "Bạn phải nhập ngày quyết định theo mẫu như sau: ngày/tháng/năm !";
                txtNgaytuyenan.Focus();
                return false;
            }

            DateTime dNgayQD = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayQD > DateTime.Now)
            {
                lstErr.Text = "Ngày quyết định không được lớn hơn ngày hiện tại !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (NgayTPGQD != DateTime.MinValue && dNgayQD < NgayTPGQD)
            {
                lstErr.Text = "Ngày quyết định không được nhỏ hơn ngày phân công thẩm phán giải quyết đơn " + hddNgayPCTPGQD.Value + ".";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (NgayMoPhienHop != DateTime.MinValue && dNgayQD < NgayMoPhienHop)
            {
                lstErr.Text = "Ngày quyết định không được nhỏ hơn ngày mở phiên họp !";
                txtNgaytuyenan.Focus();
                return false;
            }
            if (txtNgayhieuluc.Text != "")
            {
                if (Cls_Comon.IsValidDate(txtNgayhieuluc.Text) == false)
                {
                    lstErr.Text = "Bạn phải nhập ngày hiệu lực theo mẫu như sau: ngày/tháng/năm !";
                    txtNgayhieuluc.Focus();
                    return false;
                }
                DateTime NgayHieuLuc = DateTime.Parse(txtNgayhieuluc.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                if (NgayHieuLuc < dNgayQD)
                {
                    lstErr.Text = "Ngày hiệu lực không được nhỏ hơn ngày quyết định !";
                    txtNgayhieuluc.Focus();
                    return false;
                }
            }
            if (rdbTBPS.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn Quyết định tuyên bố phá sản?";
                return false;
            }
            if (rdbIsQDDCTT.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn Quyết định đình chỉ thủ tục phục hồi HĐKD do đã thực hiện xong phương án phục hồi HĐKD?";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn vụ án quá hạn luật định?";
                return false;
            }
            if (rdVuAnQuaHan.SelectedValue == "1")
            {
                if (rdNNChuQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn nguyên nhân chủ quan?";
                    return false;
                }
                if (rdNNKhachQuan.SelectedValue == "")
                {
                    lstErr.Text = "Bạn chưa chọn nguyên nhân khách quan?";
                    return false;
                }
            }
            if (rdbIsNiemyet.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn doanh nghiệp đã niêm yết trên sàn giao dịch chứng khoán?";
                return false;
            }
            if (rdbIsDNMoi3nam.SelectedValue == "")
            {
                lstErr.Text = "Bạn chưa chọn doanh nghiệp mới thành lập trong vòng 3 năm?";
                return false;
            }

            //----------------------------
            string so = txtSobanan.Text;
            if (!String.IsNullOrEmpty(txtNgaytuyenan.Text))
            {
                DateTime ngayBA = DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                Decimal DonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                ADS_SOTHAM_BL oSTBL = new ADS_SOTHAM_BL();
                Decimal CheckID = oSTBL.CheckSoBATheoLoaiAn(DonViID, "APS", so, ngayBA);
                if (CheckID > 0)
                {
                    Decimal CurrBanAnId = (string.IsNullOrEmpty(hddBanAnID.Value)) ? 0 : Convert.ToDecimal(hddBanAnID.Value);
                    String strMsg = "";
                    String STTNew = oSTBL.GETSoBANEWTheoLoaiAn(DonViID, "APS", ngayBA).ToString();
                    if (CheckID != CurrBanAnId)
                    {
                        strMsg = "Số bản án " + txtSobanan.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                        txtSobanan.Text = STTNew;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                        txtSobanan.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        private void LoadCombobox()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.QUANHEPL_YEUCAUPS);
            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();

        }
        protected void ddlLoaiQuanhe_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadCombobox();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckValid()) return;
                string current_id = Session[ENUM_LOAIAN.AN_PHASAN] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                List<APS_SOTHAM_BANAN> lst = dt.APS_SOTHAM_BANAN.Where(x => x.DONID == DONID).ToList();
                APS_SOTHAM_BANAN oND;
                if (lst.Count == 0)
                    oND = new APS_SOTHAM_BANAN();
                else
                {
                    oND = lst[0];
                }
                oND.DONID = DONID;
                oND.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                oND.LOAIQUANHE = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue);
                oND.QUANHEPHAPLUATID = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue);
                oND.SOBANAN = txtSobanan.Text;
                oND.NGAYMOPHIENTOA = (String.IsNullOrEmpty(txtNgaymophientoa.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaymophientoa.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYTUYENAN = (String.IsNullOrEmpty(txtNgaytuyenan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaytuyenan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                oND.NGAYHIEULUC = (String.IsNullOrEmpty(txtNgayhieuluc.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayhieuluc.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                oND.YEUTONUOCNGOAI = Convert.ToDecimal(ddlYeutonuocngoai.SelectedValue);

                oND.TK_ISQUAHAN = rdVuAnQuaHan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVuAnQuaHan.SelectedValue);
                oND.TK_QUAHAN_CHUQUAN = rdNNChuQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNChuQuan.SelectedValue);
                oND.TK_QUAHAN_KHACHQUAN = rdNNKhachQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNKhachQuan.SelectedValue);
                oND.TK_SONGUOIBICAM_DNNN = (String.IsNullOrEmpty(txtTKCamDNNN.Text + "")) ? 0 : Convert.ToDecimal(txtTKCamDNNN.Text.Replace(".", ""));
                oND.TK_SONGUOIBICAM_HTX = (String.IsNullOrEmpty(txtTKCamHTX.Text + "")) ? 0 : Convert.ToDecimal(txtTKCamHTX.Text.Replace(".", ""));

                oND.TK_TONGGIATRINGHIAVUTAISAN = (String.IsNullOrEmpty(txtTKTongtaisan.Text + "")) ? 0 : Convert.ToDecimal(txtTKTongtaisan.Text.Replace(".", ""));
                oND.TK_TONGGIATRITHUHOI = (String.IsNullOrEmpty(txtTKTHuhoi.Text + "")) ? 0 : Convert.ToDecimal(txtTKTHuhoi.Text.Replace(".", ""));

                oND.TK_ISDOANHNGHIEPNIEMYET = rdbIsNiemyet.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbIsNiemyet.SelectedValue);
                oND.TK_ISDOANHNGHIEP3NAM = rdbIsDNMoi3nam.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbIsDNMoi3nam.SelectedValue);
                oND.TK_ISDINHCHITHUTUCPHKD = rdbIsQDDCTT.SelectedValue == "" ? 0 : Convert.ToDecimal(rdbIsQDDCTT.SelectedValue);
                if (rdbTBPS.SelectedValue == "0")
                {
                    oND.TK_ISRUTGON = 1;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "1")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 1;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "2")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 1;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "3")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 1;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "4")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 1;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "5")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 1;
                    oND.TK_ISTOCHUCTINDUNG = 0;
                }
                else if (rdbTBPS.SelectedValue == "6")
                {
                    oND.TK_ISRUTGON = 0;
                    oND.TK_ISHNCN_KHONGTHANH = 0;
                    oND.TK_ISTHEONQHNCN = 0;
                    oND.TK_ISKHONGXAYDUNGPAPHKD = 0;
                    oND.TK_ISKHONGTHONGQUA = 0;
                    oND.TK_ISKHONGTHUCHIEN = 0;
                    oND.TK_ISTOCHUCTINDUNG = 1;
                }
                if (lst.Count == 0)
                {
                    oND.NGAYTAO = DateTime.Now;
                    oND.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.APS_SOTHAM_BANAN.Add(oND);
                    dt.SaveChanges();
                }
                else
                {
                    oND.NGAYSUA = DateTime.Now;
                    oND.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.SaveChanges();
                }
                TamNgungDONKK_USER_DKNHANVB(DONID);
                lstErr.Text = "Lưu thành công!";

            }
            catch (Exception ex)
            {
                lstErr.Text = "Lỗi: " + ex.Message;
            }
        }

        private void GetTrangThaiBanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            APS_DON oDon = dt.APS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_PHASAN && s.TRANGTHAI == 1);
            if (obj != null)
            {

                ttBanDauDONKK_USER_DKNHANVB.Value = obj.TRANGTHAI.Value.ToString();
            }
        }
        private void SetTrangThaibanDauDONKK_USER_DKNHANVB(decimal DONID)
        {
            APS_DON oDon = dt.APS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_PHASAN && s.TRANGTHAI == 3);
            if (obj != null)
            {

                obj.TRANGTHAI = Convert.ToDecimal(ttBanDauDONKK_USER_DKNHANVB.Value);
                dkk.SaveChanges();
            }
        }
        private void TamNgungDONKK_USER_DKNHANVB(decimal DONID)
        {
            APS_DON oDon = dt.APS_DON.FirstOrDefault(s => s.ID == DONID);
            DONKK_USER_DKNHANVB obj = dkk.DONKK_USER_DKNHANVB.FirstOrDefault(s => s.MAVUVIEC == oDon.MAVUVIEC && s.VUVIECID == oDon.ID && s.MALOAIVUVIEC == ENUM_LOAIAN.AN_PHASAN && s.TRANGTHAI == 1);
            if (obj != null)
            {

                obj.TRANGTHAI = 3;
                dkk.SaveChanges();
            }
        }


        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (AsyncFileUpLoad.HasFile)
            {
                string strFileName = AsyncFileUpLoad.FileName;
                string path = Server.MapPath("~/TempUpload/") + strFileName;
                AsyncFileUpLoad.SaveAs(path);
                path = path.Replace("\\", "/");
                decimal DONID = Session[ENUM_LOAIAN.AN_PHASAN] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_PHASAN]);
                APS_SOTHAM_BANAN_FILE oTF = new APS_SOTHAM_BANAN_FILE();
                // Lưu hồ sơ bản án
                string strFilePath = "";
                if (chkKySo.Checked)
                {
                    string[] arr = path.Split('/');
                    strFilePath = arr[arr.Length - 1];
                    strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                }
                else
                    strFilePath = path.Replace("/", "\\");
                byte[] buff = null;
                using (FileStream fs = File.OpenRead(strFilePath))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(strFilePath);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.APS_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                // Dùng cho tống đạt văn bản
                if (strFilePath.ToLower().Contains("52-ds"))
                {
                    APS_DON don = dt.APS_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    if (don != null)
                    {
                        UploadFileID(don, "52-DS", oTF);
                    }
                }
                File.Delete(strFilePath);
                //path = path.Replace("\\", "/");
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
            }
        }

        protected void cmdThemFileTL_Click(object sender, EventArgs e)
        {
            SaveFile_KySo();
            LoadFile();
        }
        protected void cmd_load_form_Click(object sender, EventArgs e)
        {
            LoadFile();
            Load_CheckBox();
        }
        protected void Load_CheckBox()
        {
            if (chkKySo.Checked == true)
            {
                zonekythuong.Style.Add("Display", "none");
                zonekyso.Style.Add("Display", "block");
            }
            else
            {
                zonekythuong.Style.Add("Display", "block");
                zonekyso.Style.Add("Display", "none");
            }
        }
        void SaveFile_KySo()
        {
            string folder_upload = "/TempUpload/";
            string file_kyso = hddFilePath.Value;
            if (!String.IsNullOrEmpty(hddFilePath.Value))
            {
                String[] arr = file_kyso.Split('/');
                string file_name = arr[arr.Length - 1] + "";

                String file_path = Path.Combine(Server.MapPath(folder_upload), file_name);
                decimal DONID = Convert.ToDecimal(hddID.Value);
                APS_SOTHAM_BANAN_FILE oTF = new APS_SOTHAM_BANAN_FILE();

                byte[] buff = null;
                using (FileStream fs = File.OpenRead(file_path))
                {
                    BinaryReader br = new BinaryReader(fs);
                    FileInfo oF = new FileInfo(file_path);
                    long numBytes = oF.Length;
                    buff = br.ReadBytes((int)numBytes);
                    oTF.DONID = DONID;
                    oTF.NOIDUNG = buff;
                    oTF.TENFILE = Cls_Comon.ChuyenTenFileUpload(oF.Name);
                    oTF.KIEUFILE = oF.Extension;
                    oTF.NGAYTAO = DateTime.Now;
                    oTF.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    dt.APS_SOTHAM_BANAN_FILE.Add(oTF);
                    dt.SaveChanges();
                }
                //xoa file
                File.Delete(file_path);
            }
        }
        protected void dgFile_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal ND_id = Convert.ToDecimal(e.CommandArgument.ToString());
            switch (e.CommandName)
            {
                case "Xoa":
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    if (oPer.XOA == false || cmdUpdate.Enabled == false)
                    {
                        lstErr.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_PHASAN] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new APS_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
                    if (Result != "")
                    {
                        lstErr.Text = Result;
                        return;
                    }
                    APS_SOTHAM_BANAN_FILE oT = dt.APS_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    dt.APS_SOTHAM_BANAN_FILE.Remove(oT);
                    dt.SaveChanges();
                    LoadFile();
                    break;
                case "Download":
                    APS_SOTHAM_BANAN_FILE oND = dt.APS_SOTHAM_BANAN_FILE.Where(x => x.ID == ND_id).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                    }
                    break;
            }

        }
        protected void cmdAnphi_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgAnPhi.Items.Count > 0)
                {
                    foreach (DataGridItem oItem in dgAnPhi.Items)
                    {
                        TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                        if (txtNgaynhanbanan.Text != "")
                        {
                            DateTime NgayNhanBA;
                            if (DateTime.TryParse(txtNgaynhanbanan.Text, cul, DateTimeStyles.NoCurrentDateDefault, out NgayNhanBA))
                            {
                                if (DateTime.Compare(NgayNhanBA, DateTime.Now) > 0)
                                {
                                    lstMsgAnphi.Text = "Ngày nhận bản án không được lớn hơn ngày hiện tại.";
                                    txtNgaynhanbanan.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                lstMsgAnphi.Text = "Ngày nhận bản án không đúng kiểu ngày / tháng / năm.";
                                txtNgaynhanbanan.Focus();
                                return;
                            }
                        }
                    }
                }
                string current_id = Session[ENUM_LOAIAN.AN_PHASAN] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                foreach (DataGridItem oItem in dgAnPhi.Items)
                {
                    string strID = oItem.Cells[0].Text;
                    decimal DSID = Convert.ToDecimal(strID);
                    CheckBox chkMien = (CheckBox)oItem.FindControl("chkMien");
                    TextBox txtAnphi = (TextBox)oItem.FindControl("txtAnphi");
                    CheckBox chkThamgia = (CheckBox)oItem.FindControl("chkThamgia");
                    TextBox txtNgaynhanbanan = (TextBox)oItem.FindControl("txtNgaynhanbanan");
                    List<APS_SOTHAM_BANAN_ANPHI> lst = dt.APS_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DONID && x.DUONGSU == DSID).ToList();
                    if (lst.Count == 0)
                    {
                        APS_SOTHAM_BANAN_ANPHI oT = new APS_SOTHAM_BANAN_ANPHI();
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYTAO = DateTime.Now;
                        oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.APS_SOTHAM_BANAN_ANPHI.Add(oT);
                        dt.SaveChanges();
                    }
                    else
                    {
                        APS_SOTHAM_BANAN_ANPHI oT = lst[0];
                        oT.DONID = DONID;
                        oT.DUONGSU = DSID;
                        oT.MIENANPHI = chkMien.Checked == true ? 1 : 0;
                        oT.ANPHI = txtAnphi.Text == "" ? 0 : Convert.ToDecimal(txtAnphi.Text.Replace(".", ""));
                        oT.NGAYNHANAN = (String.IsNullOrEmpty(txtNgaynhanbanan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(txtNgaynhanbanan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.ISTHAMGIA = chkThamgia.Checked == true ? 1 : 0;
                        oT.NGAYSUA = DateTime.Now;
                        oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.SaveChanges();
                    }

                }
                lstMsgAnphi.Text = "Lưu thành công !";
                cmdAnphi.Style.Add("margin-bottom", "0px");
            }
            catch (Exception ex)
            {
                lstMsgAnphi.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void chkThamgia_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkThamgia = (CheckBox)Item.FindControl("chkThamgia");
                TextBox txtNgaynhanbanan = (TextBox)Item.FindControl("txtNgaynhanbanan");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtNgaynhanbanan.Text = txtNgaytuyenan.Text;
                    }
                }
            }
        }
        protected void chkMien_CheckChange(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            foreach (DataGridItem Item in dgAnPhi.Items)
            {
                CheckBox chkMien = (CheckBox)Item.FindControl("chkMien");
                TextBox txtAnphi = (TextBox)Item.FindControl("txtAnphi");
                if (Item.Cells[0].Text.Equals(chk.ToolTip))
                {
                    if (chk.Checked)
                    {
                        txtAnphi.Text = "";
                        txtAnphi.Enabled = false;
                    }
                    else
                    {
                        txtAnphi.Enabled = true;
                    }
                }
            }
        }
        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
                pnNguyenNhanQuaHan.Visible = true;
            else
                pnNguyenNhanQuaHan.Visible = false;
        }
        protected void cmdHuyQuyetDinh_Click(object sender, EventArgs e)
        {
            // Xóa thông tin bản án
            decimal DonID = Session[ENUM_LOAIAN.AN_PHASAN] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.AN_PHASAN] + "");
            List<APS_SOTHAM_BANAN> banans = dt.APS_SOTHAM_BANAN.Where(x => x.DONID == DonID).ToList();
            if (banans.Count > 0)
            {
                dt.APS_SOTHAM_BANAN.RemoveRange(banans);
            }
            // Xóa thông tin file đính kèm
            List<APS_SOTHAM_BANAN_FILE> files = dt.APS_SOTHAM_BANAN_FILE.Where(x => x.DONID == DonID).ToList();
            if (files.Count > 0)
            {
                dt.APS_SOTHAM_BANAN_FILE.RemoveRange(files);
            }
            // Xóa điều luật áp dụng, tội danh
            List<APS_SOTHAM_BANAN_DIEULUAT> dieuLuats = dt.APS_SOTHAM_BANAN_DIEULUAT.Where(x => x.DONID == DonID).ToList();
            if (dieuLuats.Count > 0)
            {
                dt.APS_SOTHAM_BANAN_DIEULUAT.RemoveRange(dieuLuats);
            }
            // Xóa thông tin án phí
            List<APS_SOTHAM_BANAN_ANPHI> anPhis = dt.APS_SOTHAM_BANAN_ANPHI.Where(x => x.DONID == DonID).ToList();
            if (anPhis.Count > 0)
            {
                dt.APS_SOTHAM_BANAN_ANPHI.RemoveRange(anPhis);
            }
            // Xóa file tống đạt bản án
            decimal BieuMauID = 0;
            DM_BIEUMAU bm = dt.DM_BIEUMAU.Where(x => x.MABM == "52-DS").FirstOrDefault();
            if (bm != null)
            {
                BieuMauID = bm.ID;
            }

            APS_FILE file = dt.APS_FILE.Where(x => x.DONID == DonID && x.BIEUMAUID == BieuMauID && x.MAGIAIDOAN == ENUM_GIAIDOANVUAN.SOTHAM).FirstOrDefault();
            if (file != null)
            {
                dt.APS_FILE.Remove(file);
            }
            SetTrangThaibanDauDONKK_USER_DKNHANVB(DonID);
            dt.SaveChanges();
            ResetControl();
            lstErr.Text = "Xóa quyết định thành công!";
        }
        private void ResetControl()
        {
            ddlQuanhephapluat.SelectedIndex = 0;
            txtSobanan.Text = "";
            txtNgaymophientoa.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
            txtNgaytuyenan.Text = "";
            txtNgayhieuluc.Text = "";
            rdbTBPS.ClearSelection();
            rdbIsQDDCTT.ClearSelection();
            ddlYeutonuocngoai.SelectedIndex = 0;
            rdVuAnQuaHan.ClearSelection();
            rdNNChuQuan.ClearSelection();
            rdNNKhachQuan.ClearSelection();
            txtTKTongtaisan.Text = "";
            txtTKTHuhoi.Text = "";
            rdbIsNiemyet.ClearSelection();
            rdbIsDNMoi3nam.ClearSelection();
            txtTKCamDNNN.Text = "";
            txtTKCamHTX.Text = "";
            LoadFile();
            LoadAnPhi();
        }
        private void UploadFileID(APS_DON oDon, string strMaBieumau, APS_SOTHAM_BANAN_FILE fileDinhKem)
        {
            APS_DON_BL oBL = new APS_DON_BL();
            decimal IDBM = 0;
            string strTenBM = "";
            bool isNew = false;
            List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
            if (lstBM.Count > 0)
            {
                IDBM = lstBM[0].ID;
                strTenBM = lstBM[0].TENBM;
            }
            APS_FILE objFile = dt.APS_FILE.Where(x => x.DONID == oDon.ID && x.TOAANID == oDon.TOAANID && x.MAGIAIDOAN == oDon.MAGIAIDOAN && x.BIEUMAUID == IDBM).FirstOrDefault();
            if (objFile == null)
            {
                isNew = true;
                objFile = new APS_FILE();
            }
            objFile.DONID = oDon.ID;
            objFile.TOAANID = oDon.TOAANID;
            objFile.MAGIAIDOAN = oDon.MAGIAIDOAN;
            objFile.LOAIFILE = 1;
            objFile.BIEUMAUID = IDBM;
            objFile.NAM = DateTime.Now.Year;
            objFile.NOIDUNG = fileDinhKem.NOIDUNG;
            objFile.TENFILE = fileDinhKem.TENFILE;
            objFile.KIEUFILE = fileDinhKem.KIEUFILE;
            objFile.NGAYTAO = DateTime.Now;
            objFile.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            objFile.STT = 1;
            if (isNew)
                dt.APS_FILE.Add(objFile);
            dt.SaveChanges();
        }

    }
}