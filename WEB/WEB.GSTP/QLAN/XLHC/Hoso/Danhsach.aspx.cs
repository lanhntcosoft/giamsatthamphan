﻿using BL.GSTP;

using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WEB.GSTP.QLAN.XLHC.Hoso
{
    public partial class Danhsach : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");

        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch
            { return false; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string strSearch = Session["textsearch"] + "";
                if (strSearch != "")
                {
                    txtTenVuViec.Text = strSearch;
                    Session["textsearch"] = "";
                }
                LoadCombobox();
                Load_Data();
                MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    cmdThemmoi.Visible = false;
                }
                else
                {
                    Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
                }
            }
        }

        private void LoadCombobox()
        {
            //Load Quan hệ pháp luật
            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            ddlQuanhephapluat.Items.Clear();
            ddlQuanhephapluat.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.QUANHEPL_DENGHI_XLHC);
            ddlQuanhephapluat.DataTextField = "TEN";
            ddlQuanhephapluat.DataValueField = "ID";
            ddlQuanhephapluat.DataBind();
            ddlQuanhephapluat.Items.Insert(0, new ListItem("-- Tất cả --", "0"));
            //--------------------
            LoadDropThamphan();
        }
        void LoadDropThamphan()
        {
            Boolean IsLoadAll = true;
            ddlThamphan.Items.Clear();
            Decimal CanboID = (String.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_CANBOID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_CANBOID] + "");
            // Kiểm tra nếu user login là thẩm phán thì chỉ load 1 user
            // nếu là chánh án, phó chánh án hoặc khác thẩm phán thì load all
            DM_CANBO oCB = dt.DM_CANBO.Where(x => x.ID == CanboID).FirstOrDefault<DM_CANBO>();
            if (oCB != null)
            {
                // Kiểm tra chức danh có là thẩm phán hay không
                if (oCB.CHUCDANHID != null && oCB.CHUCDANHID != 0)
                {
                    DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == oCB.CHUCDANHID).FirstOrDefault();
                    if (oCD.MA.Contains("TP"))
                    {
                        ddlThamphan.Items.Add(new ListItem(oCB.HOTEN, oCB.ID.ToString()));
                        IsLoadAll = false;
                    }
                }
                // Kiểm tra chức vụ có là Chánh án hoặc phó chánh án hay không
                if (oCB.CHUCVUID != null && oCB.CHUCVUID != 0)
                {
                    DM_DATAITEM oCD = dt.DM_DATAITEM.Where(x => x.ID == oCB.CHUCVUID).FirstOrDefault();
                    if (oCD.MA.Contains("CA"))
                    {
                        IsLoadAll = true;
                    }
                }
            }
            if (IsLoadAll)
            {
                DM_CANBO_BL objBL = new DM_CANBO_BL();
                decimal LoginDonViID = Session[ENUM_SESSION.SESSION_DONVIID] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                DataTable tbl = objBL.DM_CANBO_GETBYDONVI_CHUCDANH(LoginDonViID, ENUM_CHUCDANH.CHUCDANH_THAMPHAN);
                ddlThamphan.DataSource = tbl;
                ddlThamphan.DataTextField = "HOTEN";
                ddlThamphan.DataValueField = "ID";
                ddlThamphan.DataBind();
                ddlThamphan.Items.Insert(0, new ListItem("-- Tất cả --", "0"));
            }
        }

        private void Load_Data()
        {
            XLHC_DON_BL oBL = new XLHC_DON_BL();
            decimal vDonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]),
                    ThamPhanId = Convert.ToDecimal(ddlThamphan.SelectedValue),
                    LoaiQuanHe = Convert.ToDecimal(ddlLoaiQuanhe.SelectedValue),
                    QHPL = Convert.ToDecimal(ddlQuanhephapluat.SelectedValue),
                    GiaiDoan = Convert.ToDecimal(ddlGiaiDoan.SelectedValue),
                    HinhThucNhanDon = Convert.ToDecimal(ddlHinhthucnhandon.SelectedValue),
                    dSothutu = txtSothutu.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSothutu.Text.Trim()),
                    PhanCongTP = Convert.ToDecimal(ddlPhanCongTP.SelectedValue);
            DateTime? dFrom = (String.IsNullOrEmpty(txtTuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault),
                      dTo = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            string MaVuViec = txtMaVuViec.Text.Trim(),
                   TenVuViec = txtTenVuViec.Text.Trim(),
                   Tenduongsu = txtTenduongsu.Text.Trim();
            int page_size = Convert.ToInt32(ddlPageCount.SelectedValue),
                pageindex = Convert.ToInt32(hddPageIndex.Value),
                count_all = 0;
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.CHECK_CHUCDANH_THUKY_USER(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THUKY, (decimal)Session[ENUM_SESSION.SESSION_CANBOID]);
            int count = oCBDT.Rows.Count;
            decimal ThuKyID = 0;
            if (count > 0)
            {
                ThuKyID = Convert.ToDecimal(oCBDT.Rows[0]["ID"]);
            }
            DataTable oDT = oBL.XLHC_DON_SEARCH(vDonViID, MaVuViec, TenVuViec, dFrom, dTo, LoaiQuanHe, QHPL, dSothutu, Tenduongsu, GiaiDoan, HinhThucNhanDon, ThamPhanId, ThuKyID, PhanCongTP, pageindex, page_size);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                    count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
                    #region "Xác định số lượng trang"
                    hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(ddlPageCount.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }
            dgList.PageSize = page_size;
            dgList.DataSource = oDT;
            dgList.DataBind();

        }


        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            Load_Data();
        }


        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            //---huy vu an da ghim
            Decimal IDVuViec = 0;
            decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
            oNSD.IDBPXLHC = IDVuViec;
            dt.SaveChanges();
            Session[ENUM_LOAIAN.BPXLHC] = IDVuViec;
            //-----------------------
            Session["XLHC_THEMDSK"] = null;
            Response.Redirect("Thongtindon.aspx?type=new");

        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Select"://Lựa chọn vụ việc cần Lưu thông tin
                    decimal IDVuViec = Convert.ToDecimal(e.CommandArgument.ToString());
                    //Lưu vào người dùng
                    decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
                    if ((Session[ENUM_SESSION.SESSION_DONVIID] + "") == oNSD.DONVIID.ToString())
                    {
                        oNSD.IDBPXLHC = IDVuViec;
                        dt.SaveChanges();
                    }
                    Session[ENUM_LOAIAN.BPXLHC] = IDVuViec;
                    XLHC_DON oDon = dt.XLHC_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                    if (oDon.TOAANID == oNSD.DONVIID && (oDon.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oDon.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT))
                        Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgDSDON", "Vụ việc đã được chuyển lên cấp trên, các thông tin sẽ không được phép thay đổi !", Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    else
                        Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    //Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgDSDON", "Bạn đã chọn vụ việc, tiếp theo hãy chọn chức năng cần thao tác trong danh sách bên trái !", Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    break;
                case "Sua":
                    Response.Redirect("Thongtindon.aspx?type=list&ID=" + e.CommandArgument.ToString());
                    break;
                case "Xoa":
                    if (oPer.XOA == false)
                    {
                        lbtthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal ID = Convert.ToDecimal(e.CommandArgument);
                    string strUserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    if (Session[ENUM_SESSION.SESSION_NHOMNSDID]+""=="1")
                    {
                        XLHC_DON_BL oBL = new XLHC_DON_BL();
                        if (oBL.DELETE_ALLDATA_BY_VUANID(ID + "") == false)
                        {
                            lbtthongbao.Text = "Lỗi khi xóa toàn bộ thông tin vụ việc !";
                            break;
                        }
                    }
                    else
                    {
                        //Kiểm tra nếu đã có ràng buộc không cho phép xóa
                        if (dt.XLHC_DUONGSU.Where(x => x.DONID == ID).ToList().Count > 0)
                        {
                            lbtthongbao.Text = "Đã có dữ liệu liên quan, không được phép xóa!";
                            return;
                        }
                        XLHC_DON oT = dt.XLHC_DON.Where(x => x.ID == ID).FirstOrDefault();
                        if (oT != null)
                        {
                            int GiaiDoan = (int)oT.MAGIAIDOAN;
                            if (GiaiDoan == ENUM_GIAIDOANVUAN.HOSO)
                            {
                                #region Kiểm tra dữ liệu liên quan
                                // Kiểm tra giải quyết hồ sơ
                                XLHC_DON_XULY anphi = dt.XLHC_DON_XULY.Where(x => x.DONID == ID).FirstOrDefault<XLHC_DON_XULY>();
                                if (anphi != null)
                                {
                                    lbtthongbao.Text = "Vụ việc đã có dữ liệu giải quyết hồ sơ, không được phép xóa!";
                                    return;
                                }
                                // Kiểm tra thẩm phán giải quyết hồ sơ
                                XLHC_DON_THAMPHAN xld = dt.XLHC_DON_THAMPHAN.Where(x => x.DONID == ID).FirstOrDefault<XLHC_DON_THAMPHAN>();
                                if (xld != null)
                                {
                                    lbtthongbao.Text = "Vụ việc đã có dữ liệu thẩm phán giải quyết hồ sơ, không được phép xóa!";
                                    return;
                                }
                                // Kiểm tra người tham gia tố tụng
                                XLHC_DON_THAMGIATOTUNG tgtt = dt.XLHC_DON_THAMGIATOTUNG.Where(x => x.DONID == ID).FirstOrDefault<XLHC_DON_THAMGIATOTUNG>();
                                if (tgtt != null)
                                {
                                    lbtthongbao.Text = "Vụ việc đã có dữ liệu người tham gia tố tụng, không được phép xóa!";
                                    return;
                                }
                                // Kiểm tra Giao nhận tài liệu chứng cứ
                                XLHC_DON_TAILIEU tailieu = dt.XLHC_DON_TAILIEU.Where(x => x.DONID == ID).FirstOrDefault<XLHC_DON_TAILIEU>();
                                if (tailieu != null)
                                {
                                    lbtthongbao.Text = "Vụ việc đã có dữ liệu giao nhận tài liệu chứng cứ, không được phép xóa!";
                                    return;
                                }
                                #endregion
                                dt.XLHC_DON.Remove(oT);
                                dt.SaveChanges();
                            }
                        }
                    }
                    decimal IDVuViec_ = Convert.ToDecimal(e.CommandArgument.ToString());
                    decimal IDUser_ = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    //để sửa lỗi mất menu khi xóa vụ án, anhvh add trường hợp xóa vụ án và uppdate lại idvuan =0 để giải phóng việc gim vụ án
                    QT_NGUOISUDUNG oNSD_ = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser_).FirstOrDefault();
                    if (oNSD_.IDBPXLHC == IDVuViec_)
                    {
                        oNSD_.IDBPXLHC = 0;
                        dt.SaveChanges();
                    }
                    hddPageIndex.Value = "1";
                    Load_Data();
                    break;
            }


        }

        #region "Phân trang"

        protected void lbTBack_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            Load_Data();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            Load_Data();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            Load_Data();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            Load_Data();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            hddPageIndex.Value = lbCurrent.Text;
            Load_Data();
        }

        protected void ddlPageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount2.SelectedValue = ddlPageCount.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void ddlPageCount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageCount.SelectedValue = ddlPageCount2.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        #endregion

        protected void ddlLoaiQuanhe_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombobox();
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dv = (DataRowView)e.Item.DataItem;
                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                HiddenField hddMAGIAIDOAN = (HiddenField)e.Item.FindControl("hddMAGIAIDOAN");
                if (hddMAGIAIDOAN.Value == "1")
                {
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
                }
                else
                {
                    lbtXoa.Visible = false;
                }
                string strUserName = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                if (Session[ENUM_SESSION.SESSION_NHOMNSDID]+""=="1")
                {
                    lbtXoa.Visible = true;
                }
                decimal VuAnID = Convert.ToDecimal(dv["ID"] + "");
                string Result = new XLHC_CHUYEN_NHAN_AN_BL().Check_NhanAn(VuAnID, "");
                lblSua.Visible = true;
                if (Session["CAP_XET_XU"] + "" == "CAPCAO")
                {
                    lblSua.Visible = false;
                }
                if (Result != "")
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
            }
        }
    }
}