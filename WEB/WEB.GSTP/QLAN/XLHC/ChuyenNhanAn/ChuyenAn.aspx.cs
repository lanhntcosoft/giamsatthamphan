﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.GSTP.QLAN.XLHC.ChuyenNhanAn
{
    public partial class ChuyenAn : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadGrid();
                    Cls_Comon.SetButton(cmdNhanan, false);
                    Cls_Comon.SetButton(cmdHuyChuyen, false);
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        private void LoadGrid()
        {
            lbthongbao.Text = "";
            decimal vDonViID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            decimal vToaNhanID = 0;
            vToaNhanID = hddToaAnNhan.Value == "" ? 0 : Convert.ToDecimal(hddToaAnNhan.Value);
            DateTime? dFrom = DateTime.Now;
            DateTime? dTo = DateTime.Now;
            dFrom = (String.IsNullOrEmpty(txtTuNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtTuNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dTo = (String.IsNullOrEmpty(txtDenNgay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtDenNgay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);


            TongHop_BL oBL = new TongHop_BL();
            string current_id = Session[ENUM_SESSION.SESSION_DONVIID] + "";
            decimal ID = Convert.ToDecimal(current_id);
            DataTable oDT = oBL.XLHC_CHUYENAN(vDonViID, vToaNhanID, txtMaVuViec.Text, txtTenVuViec.Text, txtSoquyetdinh.Text, txtSobanan.Text, dFrom, dTo, txtTenduongsu.Text, Convert.ToDecimal(rdbTrangthai.SelectedValue));

            #region "Xác định số lượng trang"
            int Total = Convert.ToInt32(oDT.Rows.Count);
            hddTotalPage.Value = Cls_Comon.GetTotalPage(Total, dgList.PageSize).ToString();
            lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + Total.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
            Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                         lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
            #endregion
            dgList.DataSource = oDT;
            dgList.DataBind();
        }


        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
                hddPageIndex.Value = lbCurrent.Text;
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        #endregion

        protected void cmdTimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void cmdNhanan_Click(object sender, EventArgs e)
        {
            txtTN_Ten.Text = "";
            lbthongbao.Text = "";
            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                HiddenField hddLydo = (HiddenField)Item.FindControl("hddLydo");
                HiddenField hddTHGiaoNhan = (HiddenField)Item.FindControl("hddTHGiaoNhan");
                if (chkChon.Checked)
                {
                    hddVuViecID.Value = Item.Cells[0].Text;
                    txtN_Mavuviec.Text = Item.Cells[1].Text;
                    txtN_Tenvuviec.Text = Item.Cells[4].Text;
                    txtN_Ngaygiao.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtN_Toagiao.Text = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                    txtN_THGN.Text = hddTHGiaoNhan.Value;
                    hddTHGN.Value = hddLydo.Value;
                    if (hddTHGN.Value != "01")
                    {
                        DM_TOAAN toaAn = null;
                        decimal ToaAnSoThamID = 0, ToaAnCapChaID = 0;

                        ToaAnSoThamID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                        toaAn = dt.DM_TOAAN.Where(x => x.ID == ToaAnSoThamID).FirstOrDefault();
                        if (toaAn != null)
                        {
                            ToaAnCapChaID = toaAn.CAPCHAID == null ? 0 : (decimal)toaAn.CAPCHAID;
                        }

                        if (ToaAnCapChaID == 0)
                        {
                            toaAn = dt.DM_TOAAN.Where(x => x.HIEULUC == 1 && x.ID == ToaAnSoThamID).FirstOrDefault<DM_TOAAN>();
                        }
                        else
                        {
                            toaAn = dt.DM_TOAAN.Where(x => x.HIEULUC == 1 && x.ID == ToaAnCapChaID).FirstOrDefault<DM_TOAAN>();
                        }
                        if (toaAn != null)
                        {
                            hddTN_ID.Value = toaAn.ID.ToString();
                            txtTN_Ten.Text = toaAn.MA_TEN;
                        }
                        txtTN_Ten.Enabled = false;
                    }
                    else
                    {
                        txtTN_Ten.Enabled = true;
                    }
                }

            }
            pnDanhsach.Visible = false;
            pnCapnhat.Visible = true;
        }
        protected void chkChon_CheckedChanged(object sender, EventArgs e)
        {
            Cls_Comon.SetButton(cmdNhanan, true);
            Cls_Comon.SetButton(cmdHuyChuyen, true);
            if (rdbTrangthai.SelectedValue == "1")
            {
                Cls_Comon.SetButton(cmdNhanan, false);
                Cls_Comon.SetButton(cmdHuyChuyen, true);
            }
            else
            {
                Cls_Comon.SetButton(cmdNhanan, true);
                Cls_Comon.SetButton(cmdHuyChuyen, false);
            }
            CheckBox chkXem = (CheckBox)sender;
            foreach (DataGridItem Item in dgList.Items)
            {
                CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                if (chkXem.Checked)
                {
                    if (chkXem.ToolTip != chkChon.ToolTip) chkChon.Checked = false;
                }
            }
        }
        protected void cmdLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (hddTN_ID.Value == "" || hddTN_ID.Value == "0")
                {
                    lbthongbaoNA.Text = "Bạn chưa chọn tòa án cần chuyển !";
                    txtTN_Ten.Focus();
                    return;
                }
                decimal DONID = Convert.ToDecimal(hddVuViecID.Value);
                XLHC_CHUYEN_NHAN_AN oND = new XLHC_CHUYEN_NHAN_AN();
                oND.VUANID = DONID;
                oND.TOACHUYENID = Convert.ToDecimal(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
                oND.TOANHANID = Convert.ToDecimal(hddTN_ID.Value);
                oND.NGAYGIAO = (String.IsNullOrEmpty(txtN_Ngaygiao.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtN_Ngaygiao.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                DM_DATAGROUP oG = dt.DM_DATAGROUP.Where(x => x.MA == ENUM_DANHMUC.TRUONGHOP_GIAONHAN).FirstOrDefault();

                DM_DATAITEM oIT = dt.DM_DATAITEM.Where(x => x.GROUPID == oG.ID && x.MA == hddTHGN.Value).FirstOrDefault();
                oND.TRUONGHOPGIAONHANID = oIT.ID;
                oND.NGUOIGIAOID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
                oND.GHICHU_GIAO = txtTN_ghichu.Text;
                oND.TRANGTHAI = 0;// 0: Chuyển chờ nhận, 1: Nhận
                oND.NGAYTAO = DateTime.Now;
                dt.XLHC_CHUYEN_NHAN_AN.Add(oND);

                dt.SaveChanges();
                dgList.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                LoadGrid();
                lbthongbaoNA.Text = "Hoàn thành chuyển án !";
                pnDanhsach.Visible = true;
                pnCapnhat.Visible = false;
            }
            catch (Exception ex)
            {
                lbthongbaoNA.Text = "Lỗi: " + ex.Message;
            }
        }
        protected void cmdQuaylai_Click(object sender, EventArgs e)
        {
            pnDanhsach.Visible = true;
            pnCapnhat.Visible = false;
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DataRowView rv = (DataRowView)e.Item.DataItem;
                    Literal lstNgaythuly = (Literal)e.Item.FindControl("lstNgaythuly");
                    //Literal lstSoQDBA = (Literal)e.Item.FindControl("lstSoQDBA");
                    Literal lstNoiDung = (Literal)e.Item.FindControl("lstNoiDung");
                    HiddenField hddLydo = (HiddenField)e.Item.FindControl("hddLydo");
                    HiddenField hddTHGiaoNhan = (HiddenField)e.Item.FindControl("hddTHGiaoNhan");
                    string strID = e.Item.Cells[0].Text, strNoiDung = "";
                    decimal ID = Convert.ToDecimal(strID);
                    XLHC_SOTHAM_THULY oTL = dt.XLHC_SOTHAM_THULY.Where(x => x.DONID == ID).OrderByDescending(y => y.NGAYTHULY).Take(1).FirstOrDefault();
                    if (oTL != null) lstNgaythuly.Text = ((DateTime)oTL.NGAYTHULY).ToString("dd/MM/yyyy");
                    if (rdbTrangthai.SelectedValue == "0")
                    {
                        DM_QD_LOAI oDMQD = dt.DM_QD_LOAI.Where(x => x.MA == "CVA").FirstOrDefault();
                        bool flag = false;
                        if (oDMQD != null)
                        {
                            decimal IDToa = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                            XLHC_SOTHAM_QUYETDINH oQD = dt.XLHC_SOTHAM_QUYETDINH.Where(x => x.DONID == ID && x.LOAIQDID == oDMQD.ID && x.TOAANID == IDToa).OrderByDescending(y => y.NGAYQD).FirstOrDefault();
                            if (oQD != null)
                            {
                                //lstSoQDBA.Text = oQD.SOQD + " - " + ((DateTime)oQD.NGAYQD).ToString("dd/MM/yyyy");
                                hddLydo.Value = ENUM_TRUONGHOP_GIAONHAN.KHONGTHUOC_THAMQUYEN_XETXU;
                                strNoiDung = "Không thuộc thẩm quyền xét xử";
                                flag = true;
                            }
                        }
                        if (flag == false)
                        {
                            List<XLHC_SOTHAM_KHANGCAO> lstkc = dt.XLHC_SOTHAM_KHANGCAO.Where(x => x.DONID == ID).ToList();
                            List<XLHC_SOTHAM_KHANGNGHI> lstkn = dt.XLHC_SOTHAM_KHANGNGHI.Where(x => x.DONID == ID).ToList();
                            if (lstkc.Count > 0 && lstkn.Count > 0)
                            {
                                //lstSoQDBA.Text = lstkc[0].SOQDBA + " - " + ((DateTime)lstkc[0].NGAYQDBA).ToString("dd/MM/yyyy");
                                //lstSoQDBA.Text += "<br/>" + lstkn[0].SOKN + " - " + ((DateTime)lstkn[0].NGAYBANAN).ToString("dd/MM/yyyy");
                                hddLydo.Value = ENUM_TRUONGHOP_GIAONHAN.KHANGCAO_KHANGNGHI_PHUCTHAM;
                                strNoiDung = "Do có kháng cáo và kháng nghị phúc thẩm";
                            }
                            else if (lstkc.Count > 0 && lstkn.Count == 0)
                            {
                                //lstSoQDBA.Text = lstkc[0].SOQDBA + " - " + ((DateTime)lstkc[0].NGAYQDBA).ToString("dd/MM/yyyy");

                                hddLydo.Value = ENUM_TRUONGHOP_GIAONHAN.KHANGCAO_PHUCTHAM;
                                strNoiDung = "Do có kháng cáo phúc thẩm";
                            }
                            else if (lstkc.Count == 0 && lstkn.Count > 0)
                            {
                                //lstSoQDBA.Text = lstkn[0].SOKN + " - " + ((DateTime)lstkn[0].NGAYBANAN).ToString("dd/MM/yyyy");
                                hddLydo.Value = ENUM_TRUONGHOP_GIAONHAN.KHANGNGHI_PHUCTHAM;
                                strNoiDung = "Do có kháng nghị phúc thẩm";
                            }
                        }
                    }
                    else
                    {
                        if (hddLydo.Value != "")
                        {
                            decimal IDTHGN = Convert.ToDecimal(hddLydo.Value);
                            DM_DATAITEM oIT = dt.DM_DATAITEM.Where(x => x.ID == IDTHGN).FirstOrDefault();
                            strNoiDung = oIT.TEN;
                            //List<XLHC_SOTHAM_KHANGCAO> lstkc = dt.XLHC_SOTHAM_KHANGCAO.Where(x => x.DONID == ID).ToList();
                            //List<XLHC_SOTHAM_KHANGNGHI> lstkn = dt.XLHC_SOTHAM_KHANGNGHI.Where(x => x.DONID == ID).ToList();
                            //if (lstkc.Count > 0 && lstkn.Count > 0)
                            //{
                            //    lstSoQDBA.Text = lstkc[0].SOQDBA + " - " + ((DateTime)lstkc[0].NGAYQDBA).ToString("dd/MM/yyyy");
                            //    lstSoQDBA.Text += "<br/>" + lstkn[0].SOKN + " - " + ((DateTime)lstkn[0].NGAYBANAN).ToString("dd/MM/yyyy");
                            //}
                            //else if (lstkc.Count > 0 && lstkn.Count == 0)
                            //{
                            //    lstSoQDBA.Text = lstkc[0].SOQDBA + " - " + ((DateTime)lstkc[0].NGAYQDBA).ToString("dd/MM/yyyy");
                            //}
                            //else if (lstkc.Count == 0 && lstkn.Count > 0)
                            //{
                            //    lstSoQDBA.Text = lstkn[0].SOKN + " - " + ((DateTime)lstkn[0].NGAYBANAN).ToString("dd/MM/yyyy");
                            //}
                        }
                    }
                    hddTHGiaoNhan.Value = strNoiDung;
                    lstNoiDung.Text = "<b>- Lý do:</b> " + strNoiDung + "<br/>" + rv["v_NOIDUNG"];

                    LinkButton lbtHuyChuyen = (LinkButton)e.Item.FindControl("lbtHuyChuyen");
                    if (rdbTrangthai.SelectedValue == "1")
                        lbtHuyChuyen.Visible = true;
                    else
                        lbtHuyChuyen.Visible = false;
                }
                catch (Exception ex)
                {
                    lbthongbao.Text = "Lỗi: " + ex.Message;
                }
            }
        }
        protected void rdbTrangthai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dgList.CurrentPageIndex = 0;
                hddPageIndex.Value = "1";
                LoadGrid();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void cmdHuyChuyen_Click(object sender, EventArgs e)
        {
            try
            {
                decimal DonID = 0;
                foreach (DataGridItem Item in dgList.Items)
                {
                    CheckBox chkChon = (CheckBox)Item.FindControl("chkChon");
                    if (chkChon.Checked)
                    {
                        DonID = Convert.ToDecimal(chkChon.ToolTip);
                        break;
                    }
                }
                HuyChuyen(DonID);
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                decimal DonID = Convert.ToDecimal(e.CommandArgument.ToString());
                switch (e.CommandName)
                {
                    case "HuyChuyen":
                        HuyChuyen(DonID);
                        break;
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        
        private void HuyChuyen(decimal DonID)
        {
            // Án chưa được nhận mới cho phép hủy chuyển án.
            XLHC_CHUYEN_NHAN_AN_BL Bl = new XLHC_CHUYEN_NHAN_AN_BL();
            string Message_Ex = "Không được phép hủy chuyển án.";
            string Result = Bl.Check_NhanAn(DonID, Message_Ex);
            if (Result == "") // Án chưa nhận
            {
                XLHC_CHUYEN_NHAN_AN ObjChuyenNhan = dt.XLHC_CHUYEN_NHAN_AN.Where(x => x.VUANID == DonID).FirstOrDefault();
                if (ObjChuyenNhan != null)
                {
                    dt.XLHC_CHUYEN_NHAN_AN.Remove(ObjChuyenNhan);
                    dt.SaveChanges();
                    dgList.CurrentPageIndex = 0;
                    hddPageIndex.Value = "1";
                    LoadGrid();
                    lbthongbao.Text = "Hủy chuyển án thành công.";
                }
            }
            else
            {
                lbthongbao.Text = Result;
            }
        }

    }
}