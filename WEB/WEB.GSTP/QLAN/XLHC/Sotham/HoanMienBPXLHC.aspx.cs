﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WEB.GSTP.QLAN.XLHC.Sotham
{
    public partial class HoanMienBPXLHC : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    setTinhtrang(false);
                    txtNgaynhan.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                    LoadDropBienPhapGQ();
                    decimal DONID = Session[ENUM_LOAIAN.BPXLHC] + "" == "" ? 0 : Convert.ToDecimal(Session[ENUM_LOAIAN.BPXLHC] + "");
                    CheckQuyen(DONID);
                    LoadGrid_XuLyDon();
                    foreach(RepeaterItem item in rpt.Items)
                    {
                        LinkButton lblSua = (LinkButton)item.FindControl("lblSua");
                        LinkButton lbtXoa = (LinkButton)item.FindControl("lbtXoa");
                        if (hddShowCommand.Value == "False")
                        {
                            lblSua.Text = "Chi tiết";
                            lbtXoa.Visible = false;
                        }
                    }
                }
                catch (Exception ex) { lbtthongbao.Text = ex.Message; }
            }
        }
        private void CheckQuyen(decimal DONID)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
            Cls_Comon.SetButton(cmdCapNhat, oPer.CAPNHAT);

            List<XLHC_DON_THAMPHAN> lstCount = dt.XLHC_DON_THAMPHAN.Where(x => x.DONID == DONID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETSOTHAM).ToList();
            if (lstCount.Count == 0)
            {
                lbtthongbao.Text = "Chưa phân công thẩm phán giải quyết đơn !";
                Cls_Comon.SetButton(cmdThemmoi, false);
                Cls_Comon.SetButton(cmdCapNhat, false);
                hddShowCommand.Value = "False";
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new XLHC_CHUYEN_NHAN_AN_BL().Check_NhanAn(DONID, StrMsg);
            if (Result != "")
            {
                lbtthongbao.Text = Result;
                Cls_Comon.SetButton(cmdThemmoi, false);
                Cls_Comon.SetButton(cmdCapNhat, false);
                hddShowCommand.Value = "False";
                return;
            }
        }
        void LoadDropBienPhapGQ()
        {
            dropBienPhapGQ.Items.Clear();
            //dropBienPhapGQ.Items.Add(new ListItem("-------- Chọn --------",""));
            dropBienPhapGQ.Items.Add(new ListItem("Đề nghị hoãn việc chấp hành", ENUM_XLHC_DENGHI.HOAN));
            dropBienPhapGQ.Items.Add(new ListItem("Đề nghị miễn việc chấp hành", ENUM_XLHC_DENGHI.MIEN));
            dropBienPhapGQ.Items.Add(new ListItem("Đề nghị giảm thời hạn chấp hành", ENUM_XLHC_DENGHI.GIAM));
            dropBienPhapGQ.Items.Add(new ListItem("Đề nghị tạm đình chỉ chấp hành", ENUM_XLHC_DENGHI.TAM_DINHCHI));
            dropBienPhapGQ.Items.Add(new ListItem("Đề nghị miễn chấp hành phần thời gian còn lại", ENUM_XLHC_DENGHI.MIEN_CONLAI));
            DM_CANBO_BL oDMCBBL = new DM_CANBO_BL();
            ddlNguoiky.DataSource = oDMCBBL.DM_CANBO_GETBYDONVI_CHUCDANH(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]), ENUM_CHUCDANH.CHUCDANH_THAMPHAN);
            ddlNguoiky.DataTextField = "MA_TEN";
            ddlNguoiky.DataValueField = "ID";
            ddlNguoiky.DataBind();
            ddlNguoiky.Items.Insert(0, new ListItem("--Chọn thẩm phán--", "0"));
        }

        private void LoadGrid_XuLyDon()
        {
            XLHC_SOTHAM_BL oBL = new XLHC_SOTHAM_BL();
            string current_id = Session[ENUM_LOAIAN.BPXLHC] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            int page_size = 20;
            int pageindex = Convert.ToInt32(hddPageIndex.Value);

            DataTable oDT = oBL.XLHC_DONXIN_HOAN_MIEN_GETLIST(DONID, pageindex, page_size);
            if (oDT.Rows.Count > 0)
            {
                DataRow row_last = oDT.Rows[0];
                LoadInfo_XuLyDon(Convert.ToDecimal(row_last["ID"] + ""));

                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(oDT.Rows.Count, page_size).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + oDT.Rows.Count.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {

                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }

            rpt.DataSource = oDT;
            rpt.DataBind();
        }

        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            decimal CurrID = Convert.ToDecimal(e.CommandArgument.ToString());
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    hddCurrID.Value = CurrID.ToString();
                    LoadInfo_XuLyDon(CurrID);
                    break;
                case "Xoa":
                    if (oPer.XOA == false || cmdCapNhat.Enabled == false)
                    {
                        lbtthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    decimal DonID = Convert.ToDecimal(Session[ENUM_LOAIAN.BPXLHC] + "");
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new XLHC_CHUYEN_NHAN_AN_BL().Check_NhanAn(DonID, StrMsg);
                    if (Result != "")
                    {
                        lbtthongbao.Text = Result;
                        return;
                    }
                    XLHC_DONXIN_HOAN_MIEN oT = dt.XLHC_DONXIN_HOAN_MIEN.Where(x => x.ID == CurrID).FirstOrDefault();
                    dt.XLHC_DONXIN_HOAN_MIEN.Remove(oT);
                    dt.SaveChanges();

                    hddPageIndex.Value = "1";
                    LoadGrid_XuLyDon();
                    break;
            }
        }

        void LoadInfo_XuLyDon(Decimal CurrID)
        {
            XLHC_DONXIN_HOAN_MIEN obj = dt.XLHC_DONXIN_HOAN_MIEN.Where(x => x.ID == CurrID).Single<XLHC_DONXIN_HOAN_MIEN>();
            if (obj != null)
            {
                hddCurrID.Value = CurrID.ToString();
                if (obj.NGAYNHANDON != null) txtNgaynhan.Text = ((DateTime)obj.NGAYNHANDON).ToString("dd/MM/yyyy", cul);
                if (obj.NGAYVIETDON != null) txtNgayviet.Text = ((DateTime)obj.NGAYVIETDON).ToString("dd/MM/yyyy", cul);
                txtLyDo.Text = obj.LYDO;
                dropBienPhapGQ.SelectedValue = obj.LOAIDON + "";
                rdbIsGiaiquyet.SelectedValue = (string.IsNullOrEmpty(obj.ISGIAIQUYET + "")) ? "0" : obj.ISGIAIQUYET.ToString();
                if (rdbIsGiaiquyet.SelectedValue == "0")
                    setTinhtrang(false);
                else
                {
                    setTinhtrang(true);
                    if (obj.GQ_NGAY != null) txtNgayGQ.Text = ((DateTime)obj.GQ_NGAY).ToString("dd/MM/yyyy", cul);
                    if (obj.GQ_NGUOIKY != null) ddlNguoiky.SelectedValue = obj.GQ_NGUOIKY.ToString();
                    rdbKetqua.SelectedValue = (string.IsNullOrEmpty(obj.GQ_ISCHAPNHAN + "")) ? "0" : obj.GQ_ISCHAPNHAN.ToString();
                }

                rdVuAnQuaHan.SelectedValue = (string.IsNullOrEmpty(obj.GQ_ISQUAHAN + "")) ? "0" : obj.GQ_ISQUAHAN.ToString();
                rdNNChuQuan.SelectedValue = (string.IsNullOrEmpty(obj.GQ_QUAHAN_CHUQUAN + "")) ? "0" : obj.GQ_QUAHAN_CHUQUAN.ToString();
                rdNNKhachQuan.SelectedValue = (string.IsNullOrEmpty(obj.GQ_QUAHAN_KHACHQUAN + "")) ? "0" : obj.GQ_QUAHAN_KHACHQUAN.ToString();
                if (rdVuAnQuaHan.SelectedValue == "1")
                    pnNguyenNhanQuaHan.Visible = true;
                else
                    pnNguyenNhanQuaHan.Visible = false;
            }
        }


        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            //dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            LoadGrid_XuLyDon();
        }

        #endregion
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
        }

        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            Resetcontrol();
        }
        void Resetcontrol()
        {
            txtLyDo.Text = ""; txtNgayviet.Text = "";
            txtNgaynhan.Text = "";
            rdbIsGiaiquyet.SelectedValue = "0";
            setTinhtrang(false);
            lbtthongbao.Text = "";
            hddCurrID.Value = "0";
        }

        protected void rdVuAnQuaHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdVuAnQuaHan.SelectedValue == "1")
                pnNguyenNhanQuaHan.Visible = true;
            else
                pnNguyenNhanQuaHan.Visible = false;
        }
        protected void cmdCapNhat_Click(object sender, EventArgs e)
        {
            if (txtNgaynhan.Text == "")
            {
                lbtthongbao.Text = "Chưa nhập ngày nhận đơn !";
                return;
            }
            if (txtLyDo.Text == "")
            {
                lbtthongbao.Text = "Chưa nhập lý do !";
                return;
            }
            if (rdbIsGiaiquyet.SelectedValue == "1")
            {
                if (txtNgayGQ.Text == "")
                {
                    lbtthongbao.Text = "Chưa nhập ngày giải quyết đơn !";
                    return;
                }
                if (rdbKetqua.SelectedValue == "")
                {
                    lbtthongbao.Text = "Chưa chọn kết quả giải quyết đơn !";
                    return;
                }
            }
            string current_id = Session[ENUM_LOAIAN.BPXLHC] + "";
            decimal DONID = Convert.ToDecimal(current_id);

            int DonXyLyID = 0;
            XLHC_DONXIN_HOAN_MIEN obj = new XLHC_DONXIN_HOAN_MIEN();
            if (hddCurrID.Value != "" && hddCurrID.Value != "0")
            {
                DonXyLyID = Convert.ToInt32(hddCurrID.Value);
                obj = dt.XLHC_DONXIN_HOAN_MIEN.Where(x => x.ID == DonXyLyID).FirstOrDefault();
                if (obj != null)
                {
                    obj.NGAYSUA = DateTime.Now;
                    obj.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                }
                else obj = new XLHC_DONXIN_HOAN_MIEN();
            }
            else
                obj = new XLHC_DONXIN_HOAN_MIEN();
            obj.LYDO = txtLyDo.Text.Trim();
            obj.NGAYNHANDON = (String.IsNullOrEmpty(txtNgaynhan.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaynhan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.NGAYVIETDON = (String.IsNullOrEmpty(txtNgayviet.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayviet.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            obj.DONID = DONID;
            string bienphap = dropBienPhapGQ.SelectedValue;
            obj.LOAIDON = Convert.ToDecimal(bienphap);
            obj.ISGIAIQUYET = Convert.ToDecimal(rdbIsGiaiquyet.SelectedValue);
            if (obj.ISGIAIQUYET == 1)
            {
                obj.GQ_NGAY = (String.IsNullOrEmpty(txtNgayGQ.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayGQ.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                obj.GQ_ISCHAPNHAN = Convert.ToDecimal(rdbKetqua.SelectedValue);
                if (ddlNguoiky.Items.Count > 0) obj.GQ_NGUOIKY = Convert.ToDecimal(ddlNguoiky.SelectedValue);
                obj.GQ_ISQUAHAN = rdVuAnQuaHan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdVuAnQuaHan.SelectedValue);
                obj.GQ_QUAHAN_CHUQUAN = rdNNChuQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNChuQuan.SelectedValue);
                obj.GQ_QUAHAN_KHACHQUAN = rdNNKhachQuan.SelectedValue == "" ? 0 : Convert.ToDecimal(rdNNKhachQuan.SelectedValue);
            }
            if (DonXyLyID == 0)
            {
                obj.NGAYTAO = obj.NGAYSUA = DateTime.Now;
                obj.NGUOISUA = obj.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                dt.XLHC_DONXIN_HOAN_MIEN.Add(obj);
            }
            dt.SaveChanges();

            //-------------------------------------
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
            Resetcontrol();
            lbtthongbao.Text = "Lưu thành công!";
        }
        private void setTinhtrang(bool flag)
        {
            txtNgayGQ.Enabled = ddlNguoiky.Enabled = rdbKetqua.Enabled = rdVuAnQuaHan.Enabled = rdNNChuQuan.Enabled = rdNNKhachQuan.Enabled = flag;
        }
        protected void rdbIsGiaiquyet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbIsGiaiquyet.SelectedValue == "0")
                setTinhtrang(false);
            else
                setTinhtrang(true);
        }
    }
}