﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI;
using System.IO;
using BL.GSTP.AHN;
using BL.GSTP.TP_THADS;

namespace WEB.GSTP.QLAN.AHN.XuLyDon
{
    public partial class Danhsach : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public string sDONID = "0";
        public string sID = "0";
        private void SetDonGhep()
        {
            DONGHEP.Visible = false;
            string keyDonID = "THONGTIN.DONGHEP.DONID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            string keyLoaiAnId = "THONGTIN.DONGHEP.LOAIANID" + Session[ENUM_SESSION.SESSION_USERID].ToString();
            Session[keyDonID] = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH]);
            Session[keyLoaiAnId] = Convert.ToDecimal(ENUM_LOAIVUVIEC_NUMBER.AN_HONNHAN_GIADINH);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hddURLKS.Value = Cls_Comon.GetRootURL() + "/FileUploadHandler.aspx";
                sDONID = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                try
                {
                    SetDonGhep();
                    txtNgayGQ.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                    LoadDropBienPhapGQ();
                    pnCDTN.Visible = pnCDNN.Visible = pnTraDon.Visible = false;
                    pnThuLy.Visible = true;
                    string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                    decimal DONID = current_id==""?0: Convert.ToDecimal(current_id);
                    CheckQuyen(DONID);
                    AHN_DON oDon = dt.AHN_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    if (oDon.LOAIDON == 2)
                    {
                        lbtthongbao.Text = "Đơn đã chuyển sang tòa án khác xử lý !";
                        Cls_Comon.SetButton(cmdCapNhat, false);
                        Cls_Comon.SetButton(cmdThemmoi, false);
                    }
                    LoadGrid_XuLyDon();
                }
                catch (Exception ex) { lbtthongbao.Text = ex.Message; }
            }
        }
        private void CheckQuyen(decimal DONID)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
            Cls_Comon.SetButton(cmdCapNhat, oPer.CAPNHAT);

            AHN_DON oT = dt.AHN_DON.Where(x => x.ID == DONID).FirstOrDefault();
            if (oT != null)
            {
                hddNgayNhanDon.Value = oT.NGAYNHANDON + "" == "" ? "" : ((DateTime)oT.NGAYNHANDON).ToString("dd/MM/yyyy");
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    lbtthongbao.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                    Cls_Comon.SetButton(cmdThemmoi, false);
                    Cls_Comon.SetButton(cmdCapNhat, false);
                    hddShowCommand.Value = "False";
                    return;
                }
            }

            List<AHN_DON_THAMPHAN> lstCount = dt.AHN_DON_THAMPHAN.Where(x => x.DONID == DONID && x.MAVAITRO == ENUM_VAITROTHAMPHAN.VTTP_GIAIQUYETDON).ToList();
            if (lstCount.Count == 0)
            {
                lbtthongbao.Text = "Chưa phân công thẩm phán giải quyết đơn !";
                Cls_Comon.SetButton(cmdThemmoi, false);
                Cls_Comon.SetButton(cmdCapNhat, false);
                hddShowCommand.Value = "False";
                return;
            }
            List<AHN_SOTHAM_THULY> lstTL = dt.AHN_SOTHAM_THULY.Where(x => x.DONID == DONID).ToList();
            if (lstTL.Count > 0)
            {
                lbtthongbao.Text = "Đã thụ lý vụ việc không được sửa đổi !";
                Cls_Comon.SetButton(cmdThemmoi, false);
                Cls_Comon.SetButton(cmdCapNhat, false);
                hddShowCommand.Value = "False";
                return;
            }
            string StrMsg = "Không được sửa đổi thông tin.";
            string Result = new AHN_CHUYEN_NHAN_AN_BL().Check_NhanAn(DONID, StrMsg);
            if (Result != "")
            {
                lbtthongbao.Text = Result;
                Cls_Comon.SetButton(cmdThemmoi, false);
                Cls_Comon.SetButton(cmdCapNhat, false);
                hddShowCommand.Value = "False";
                return;
            }
        }
        void LoadDropBienPhapGQ()
        {
            dropBienPhapGQ.Items.Clear();
            //dropBienPhapGQ.Items.Add(new ListItem("-------- Chọn --------",""));
            dropBienPhapGQ.Items.Add(new ListItem("Chuyển đơn trong Hệ thống Tòa án", ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh));
            //dropBienPhapGQ.Items.Add(new ListItem("Chuyển đơn ngoài ngành", ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh));

            dropBienPhapGQ.Items.Add(new ListItem("Trả lại đơn", ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon));
            dropBienPhapGQ.Items.Add(new ListItem("Yêu cầu bổ sung đơn", ENUM_ADS_BIENPHAPGQ.ADS_YCBoSungDon));
            dropBienPhapGQ.Items.Add(new ListItem("Thụ lý vụ việc", ENUM_ADS_BIENPHAPGQ.ADS_ThuLy));
            dropBienPhapGQ.SelectedValue = ENUM_ADS_BIENPHAPGQ.ADS_ThuLy;

            DM_DATAITEM_BL oBL = new DM_DATAITEM_BL();
            //Danh mục lý do tra đơn
            ddlLyTradon.DataSource = oBL.DM_DATAITEM_GETBYGROUPNAME(ENUM_DANHMUC.LYDOTRADON);
            ddlLyTradon.DataTextField = "TEN";
            ddlLyTradon.DataValueField = "ID";
            ddlLyTradon.DataBind();
        }

        private void LoadGrid_XuLyDon()
        {
            AHN_DON_XULY_BL oBL = new AHN_DON_XULY_BL();
            string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            int page_size = 20;
            int pageindex = Convert.ToInt32(hddPageIndex.Value);

            DataTable oDT = oBL.GetByDonID(DONID, pageindex, page_size);
            if (oDT.Rows.Count > 0)
            {
                DataRow row_last = oDT.Rows[0];
                //  LoadInfo_XuLyDon(Convert.ToDecimal(row_last["ID"] + ""));

                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(oDT.Rows.Count, page_size).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + oDT.Rows.Count.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }

            rpt.DataSource = oDT;
            rpt.DataBind();
        }

        protected void rpt_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            decimal CurrID = Convert.ToDecimal(e.CommandArgument.ToString());
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            switch (e.CommandName)
            {
                case "Sua":
                    hddCurrID.Value = CurrID.ToString();
                    sID = CurrID.ToString();
                    btnin.Visible = false;
                    LoadInfo_XuLyDon(CurrID);
                    break;
                case "Xoa":
                    if (oPer.XOA == false || cmdCapNhat.Enabled == false)
                    {
                        lbtthongbao.Text = "Bạn không có quyền xóa!";
                        return;
                    }
                    AHN_DON_XULY oT = dt.AHN_DON_XULY.Where(x => x.ID == CurrID).FirstOrDefault();
                    if ((oT.CDTN_TOAANID + "") == (Session[ENUM_SESSION.SESSION_DONVIID] + ""))
                    {
                        lbtthongbao.Text = "Bạn không thể xóa nội dung tòa án khác cập nhật!";
                        return;
                    }
                    // khong duoc xoa khi da Tong dat
                    AHN_TONGDAT oTD = dt.AHN_TONGDAT.Where(x => x.DONID == oT.DONID).FirstOrDefault();
                    if (oTD != null)
                    {
                        lbtthongbao.Text = "Bạn không thể xóa khi đã tống đạt!";
                        return;
                    }
                    
                    DVCQG_THANH_TOAN_BL obj = new DVCQG_THANH_TOAN_BL();
                    decimal _VALUE = 0;
                    obj.DVCQG_THANH_TOAN_DELETE_XULY(CurrID, "3", ref _VALUE);
                    if (_VALUE == 1)
                    {
                        decimal ID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "");
                        string StrMsg = "Không được sửa đổi thông tin.";
                        string Result = new AHN_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
                        if (Result != "")
                        {
                            lbtthongbao.Text = Result;
                            return;
                        }
                        decimal FileID = 0;
                        if (oT.FILEID != null) FileID = (decimal)oT.FILEID;
                        //manh them xoa An phi
                        AHN_ANPHI oAnphi = dt.AHN_ANPHI.Where(x => x.DONID == oT.DONID).FirstOrDefault();
                        dt.AHN_ANPHI.Remove(oAnphi);
                        //-----

                        dt.AHN_DON_XULY.Remove(oT);
                        DeleteDonChuyenToaAnKhac(oT);
                        //DONGHEP.DeleteDonChiTiet(oT.ID);
                        dt.SaveChanges();
                        if (FileID > 0)
                        {
                            try
                            {
                                AHN_FILE objf = dt.AHN_FILE.Where(x => x.ID == FileID).FirstOrDefault();
                                dt.AHN_FILE.Remove(objf);
                                dt.SaveChanges();
                            }
                            catch (Exception ex) { }
                        }
                        hddPageIndex.Value = "1";
                        LoadGrid_XuLyDon();
                    }
                    else
                    {
                        lbtthongbao.Text = "Đã phát sinh giao dịch thanh toán, bạn không được Xóa!";
                    }
                    break;
                case "Download":
                    var oND = dt.AHN_FILE.Where(x => x.ID == CurrID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        var cacheKey = Guid.NewGuid().ToString("N");
                        Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                    }
                    break;
            }
        }

        void LoadInfo_XuLyDon(Decimal CurrID)
        {
            DONGHEP.Visible = false;
            AHN_DON_XULY obj = dt.AHN_DON_XULY.Where(x => x.ID == CurrID).Single<AHN_DON_XULY>();
            if (obj != null)
            {
                if ((obj.CDTN_TOAANID + "") == (Session[ENUM_SESSION.SESSION_DONVIID] + ""))
                {
                    lbtthongbao.Text = "Bạn không thể sửa nội dung của tòa án khác cập nhật!";
                    Cls_Comon.SetButton(cmdCapNhat, false);
                }
                txtNgayGQ.Text = (((DateTime)obj.NGAYGQ_YC) == DateTime.MinValue) ? "" : ((DateTime)obj.NGAYGQ_YC).ToString("dd/MM/yyyy", cul);
                txtLyDo.Text = obj.LYDO;

                txtSothongbao.Text = obj.SOTHONGBAO;                
                txtSothongbao.Enabled = false;

                txtNgaythongbao.Text = (((DateTime)obj.NGAYTHONGBAO) == DateTime.MinValue) ? "" : ((DateTime)obj.NGAYTHONGBAO).ToString("dd/MM/yyyy", cul);

                hddFileid.Value = obj.FILEID + "";
                if ((obj.FILEID + "") != "" && (obj.FILEID + "") != "0")
                {
                    AHN_FILE objFile = dt.AHN_FILE.Where(x => x.ID == obj.FILEID).FirstOrDefault();
                    if (objFile.TENFILE != null) lbtDownload.Visible = true;
                }
                else
                    lbtDownload.Visible = false;
                dropBienPhapGQ.SelectedValue = obj.LOAIGIAIQUYET + "";
                string bienphap = dropBienPhapGQ.SelectedValue;
                lblNgayGQ.Text = "Ngày GQ/YC";
                lblLydo.Text = "Lý do";
                switch (bienphap)
                {
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh:
                        pnCDNN.Visible = true;
                        pnCDTN.Visible = pnTraDon.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                        txtCDNN_NgayChuyen.Text = (((DateTime)obj.CDNN_NGAYCHUYEN) == DateTime.MinValue) ? "" : ((DateTime)obj.CDNN_NGAYCHUYEN).ToString("dd/MM/yyyy", cul);
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh:
                        //lblNgayGQ.Text = "Ngày chuyển";
                        pnCDTN.Visible = true;
                        pnCDNN.Visible = pnTraDon.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon:
                        DONGHEP.Visible = true;
                        DONGHEP.LoadGrid();
                        DONGHEP.SetCheckBox(CurrID);
                        pnTraDon.Visible = true;
                        pnCDTN.Visible = pnCDNN.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                        lblLydo.Text = "Ghi chú";
                        txtTradon_Ngay.Text = (((DateTime)obj.TRADON_NGAYTRA) == DateTime.MinValue) ? "" : ((DateTime)obj.TRADON_NGAYTRA).ToString("dd/MM/yyyy", cul);
                        if (obj.TRADON_LYDOID != null)
                            ddlLyTradon.SelectedValue = obj.TRADON_LYDOID.ToString();

                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_YCBoSungDon:
                        DONGHEP.Visible = true;
                        DONGHEP.LoadGrid();
                        DONGHEP.SetCheckBox(CurrID);
                        pnYCBS.Visible = true;
                        pnTraDon.Visible = false;
                        pnCDTN.Visible = pnCDNN.Visible = pnThuLy.Visible = false;
                        txtNgayYCBS.Text = (((DateTime)obj.YCBS_NGAYYEUCAU) == DateTime.MinValue) ? "" : ((DateTime)obj.YCBS_NGAYYEUCAU).ToString("dd/MM/yyyy", cul);
                        txtYCBS.Text = obj.YCBS_NOIDUNG + "";
                        txtThoihanBSYC.Text = obj.YCBS_THOIHAN == null ? "" : Convert.ToDecimal(obj.YCBS_THOIHAN).ToString();

                        break;
                    default:
                        pnThuLy.Visible = true;
                        pnCDNN.Visible = pnCDTN.Visible = pnYCBS.Visible = pnTraDon.Visible = false;
                        List<AHN_ANPHI> lstAP = dt.AHN_ANPHI.Where(x => x.DONID == obj.DONID).ToList();
                        if (lstAP.Count > 0)
                        {
                            AHN_ANPHI objAP = lstAP[0];
                            txtGiaTriTranhChap.Text = objAP.GIATRITRANHCHAP == null ? "" : Convert.ToDecimal(objAP.GIATRITRANHCHAP).ToString("#,#", cul);
                            txtMucGiamAnPhi.Text = objAP.MUCGIAMANPHI == null ? "" : Convert.ToDecimal(objAP.MUCGIAMANPHI).ToString("#,#", cul);
                            txtTamUngAnPhi.Text = objAP.TAMUNGANPHI == null ? "" : Convert.ToDecimal(objAP.TAMUNGANPHI).ToString("#,#", cul);
                            
                            txtHanNopAnPhi.Text = String.IsNullOrEmpty(objAP.HANNOP_SONGAY+"") ? "" : objAP.HANNOP_SONGAY.ToString();
                            txtSoNgayGiaHan.Text = objAP.SONGAYGIAHAN + "";
                            if (objAP.TINHTRANG != null)
                                if (objAP.TINHTRANG == 1)
                                {
                                    chkNopAnPhi.Checked = true;
                                    SetEnableZoneNopAnPhi(true);
                                }
                        }
                        break;
                }

                if (obj.CDTN_TOAANID > 0)
                {
                    hddToaAn.Value = obj.CDTN_TOAANID.ToString();
                    txtToaAn.Text = dt.DM_TOAAN.Where(x => x.ID == obj.CDTN_TOAANID).Single<DM_TOAAN>().MA_TEN;
                }
                if (obj.TRADON_CANCUID > 0)
                {


                }


                txtCDNN_TenCoQuan.Text = obj.CDNN_TENCQ + "";

            }
        }


        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            //dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            LoadGrid_XuLyDon();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            //dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            LoadGrid_XuLyDon();
        }

        #endregion
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
        }

        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            Resetcontrol();
        }
        void Resetcontrol()
        {
            DONGHEP.LoadGrid();
            txtCDNN_TenCoQuan.Text = txtCDNN_NgayChuyen.Text = txtLyDo.Text = txtNgayGQ.Text = txtToaAn.Text = "";

            hddToaAn.Value = "0"; lbtthongbao.Text = "";
            hddCurrID.Value = "0";
            btnin.Visible = false;
            lbtDownload.Visible = false;
            txtSothongbao.Text = "";
            txtSothongbao.Enabled = true;

            Cls_Comon.SetButton(cmdCapNhat, true);
        }

        protected void ddlBienPhapGQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            DONGHEP.LoadGrid();
            DONGHEP.Visible = false;
            //LoadCombobox();
            lblNgayGQ.Text = "Ngày GQ/YC";
            lblLydo.Text = "Lý do";
            string bienphap = dropBienPhapGQ.SelectedValue;
            switch (bienphap)
            {
                case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh:

                    pnCDNN.Visible = true;
                    pnCDTN.Visible = pnTraDon.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                    break;
                case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh:
                    //lblNgayGQ.Text = "Ngày chuyển";
                    pnCDTN.Visible = true;
                    pnCDNN.Visible = pnTraDon.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                    break;
                case ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon:
                    DONGHEP.Visible = true;
                    pnTraDon.Visible = true;
                    pnCDTN.Visible = pnCDNN.Visible = pnYCBS.Visible = pnThuLy.Visible = false;
                    lblLydo.Text = "Ghi chú";
                    break;
                case ENUM_ADS_BIENPHAPGQ.ADS_YCBoSungDon:
                    DONGHEP.Visible = true;
                    pnYCBS.Visible = true;
                    pnTraDon.Visible = false;
                    pnCDTN.Visible = pnCDNN.Visible = pnThuLy.Visible = false;
                    break;
                default:
                    pnThuLy.Visible = true;
                    pnCDNN.Visible = pnCDTN.Visible = pnYCBS.Visible = pnTraDon.Visible = false;
                    break;
            }
            txtNgayGQ.Focus();
        }

        protected void rpt_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView rowView = (DataRowView)e.Item.DataItem;

                LinkButton lblSua = (LinkButton)e.Item.FindControl("lblSua");
                Cls_Comon.SetLinkButton(lblSua, oPer.CAPNHAT);

                LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
                Cls_Comon.SetLinkButton(lbtXoa, oPer.XOA);
                string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                AHN_DON oT = dt.AHN_DON.Where(x => x.ID == DONID).FirstOrDefault();
                if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
                //Tong dat roi khong duoc xao
                decimal vFILEID = Convert.ToDecimal(rowView["FILEID"]);
                AHN_FILE oF = dt.AHN_FILE.Where(x => x.ID == vFILEID).FirstOrDefault();
                if (oF != null)
                {
                    if (oF.TENFILE != null)
                    {
                        lblSua.Text = "Chi tiết";
                        lbtXoa.Visible = false;
                    }
                }
                ImageButton lblDownload = (ImageButton)e.Item.FindControl("lblDownload");
                if (rowView["TENFILE"] + "" == "")
                {
                    lblDownload.Visible = false;
                }
                else
                {
                    lblDownload.Visible = true;
                }

                if (hddShowCommand.Value == "False")
                {
                    lblSua.Text = "Chi tiết";
                    lbtXoa.Visible = false;
                }
                Literal lttNoiTiepNhan = (Literal)e.Item.FindControl("lttNoiTiepNhan");

                String bienphap = rowView["LoaiGiaiQuyet"] + "";
                switch (bienphap)
                {
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh:
                        lttNoiTiepNhan.Text = rowView["Ten"] + "";
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh:
                        lttNoiTiepNhan.Text = rowView["CDNN_TenCQ"] + "";
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon:
                        lttNoiTiepNhan.Text = rowView["TraDon_CanCuID"] + "";
                        break;
                    default:
                        lttNoiTiepNhan.Text = "";
                        break;
                }
            }
        }

        private decimal UploadFileID(AHN_DON oDon, decimal FileID, string strMaBieumau, string STT)
        {
            String CurrUser = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            AHN_DON_BL oBL = new AHN_DON_BL();
            decimal IDFIle = 0;
            decimal IDBM = 0;
            string strTenBM = "";
            List<DM_BIEUMAU> lstBM = dt.DM_BIEUMAU.Where(x => x.MABM == strMaBieumau).ToList();
            if (lstBM.Count > 0)
            {
                IDBM = lstBM[0].ID;
                strTenBM = lstBM[0].TENBM;
            }
            AHN_FILE objFile = new AHN_FILE();
            if (FileID > 0)
                objFile = dt.AHN_FILE.Where(x => x.ID == FileID).FirstOrDefault();
            objFile.DONID = oDon.ID;
            objFile.TOAANID = oDon.TOAANID;
            objFile.MAGIAIDOAN = oDon.MAGIAIDOAN;
            objFile.LOAIFILE = 0;
            objFile.BIEUMAUID = IDBM;
            objFile.NAM = DateTime.Now.Year;
            if (hddFilePath.Value != "")
            {
                try
                {
                    string strFilePath = "";
                    if (chkKySo.Checked)
                    {
                        string[] arr = hddFilePath.Value.Split('/');
                        strFilePath = arr[arr.Length - 1];
                        strFilePath = Server.MapPath("~/TempUpload/") + strFilePath;
                    }
                    else
                        strFilePath = hddFilePath.Value.Replace("/", "\\");
                    byte[] buff = null;
                    using (FileStream fs = File.OpenRead(strFilePath))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        FileInfo oF = new FileInfo(strFilePath);
                        long numBytes = oF.Length;
                        buff = br.ReadBytes((int)numBytes);
                        objFile.NOIDUNG = buff;
                        objFile.TENFILE = Cls_Comon.ChuyenTVKhongDau(strTenBM) + oF.Extension;
                        objFile.KIEUFILE = oF.Extension;
                    }
                    File.Delete(strFilePath);
                }
                catch (Exception ex) { lbtthongbao.Text = ex.Message; }
            }
            objFile.NGAYTAO = DateTime.Now;
            objFile.NGUOITAO = CurrUser;
            if (STT != "") objFile.STT = Convert.ToDecimal(STT);
            if (FileID == 0)
                dt.AHN_FILE.Add(objFile);
            dt.SaveChanges();
            IDFIle = objFile.ID;
            return IDFIle;
        }
        private bool CheckValid()
        {
            if (txtNgayGQ.Text == "")
            {
                lbtthongbao.Text = "Bạn chưa nhập Ngày GQ/YC !";
                txtNgayGQ.Focus();
                return false;
            }
            if (!Cls_Comon.IsValidDate(txtNgayGQ.Text))
            {
                lbtthongbao.Text = "Bạn phải nhập ngày GQ/YC theo định dạng (dd/MM/yyyy) !";
                txtNgayGQ.Focus();
                return false;
            }
            DateTime dNgayGQ = (String.IsNullOrEmpty(txtNgayGQ.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayGQ.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            if (dNgayGQ > DateTime.Now)
            {
                lbtthongbao.Text = "Ngày GQ/YC không được lớn hơn ngày hiện tại !";
                txtNgayGQ.Focus();
                return false;
            }
            if (hddNgayNhanDon.Value != "")
            {
                DateTime NgayNhanDon = DateTime.Parse(hddNgayNhanDon.Value, cul, DateTimeStyles.NoCurrentDateDefault);
                if (dNgayGQ < NgayNhanDon)
                {
                    lbtthongbao.Text = "Ngày GQ/YC không được nhỏ hơn ngày nhận đơn " + NgayNhanDon.ToString("dd/MM/yyyy") + " !";
                    txtNgayGQ.Focus();
                    return false;
                }
            }
            string bienphap = dropBienPhapGQ.SelectedValue;
            if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh)
            {
                if (hddToaAn.Value == "0")
                {
                    lbtthongbao.Text = "Bạn chưa chọn tòa án nhận !";
                    txtToaAn.Focus();
                    return false;
                }
            }
            if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh)
            {
                if (txtCDNN_TenCoQuan.Text == "")
                {
                    lbtthongbao.Text = "Bạn chưa nhập CQ/TC nhận đơn !";
                    txtCDNN_TenCoQuan.Focus();
                    return false;
                }
                if (txtCDNN_NgayChuyen.Text != "")
                {
                    if (!Cls_Comon.IsValidDate(txtCDNN_NgayChuyen.Text))
                    {
                        lbtthongbao.Text = "Bạn phải nhập ngày chuyển theo định dạng (dd/MM/yyyy) !";
                        txtCDNN_NgayChuyen.Focus();
                        return false;
                    }
                    DateTime NgayChuyen = DateTime.Parse(txtCDNN_NgayChuyen.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                    if (NgayChuyen < dNgayGQ)
                    {
                        lbtthongbao.Text = "Ngày chuyển không được nhỏ hơn ngày GQ/YC !";
                        txtCDNN_NgayChuyen.Focus();
                        return false;
                    }
                    if (NgayChuyen > DateTime.Now)
                    {
                        lbtthongbao.Text = "Ngày chuyển không được lớn hơn ngày hiện tại !";
                        txtCDNN_NgayChuyen.Focus();
                        return false;
                    }
                }
            }
            if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon)
            {
                if (txtTradon_Ngay.Text != "")
                {
                    if (!Cls_Comon.IsValidDate(txtTradon_Ngay.Text))
                    {
                        lbtthongbao.Text = "Bạn phải nhập ngày trả đơn theo định dạng (dd/MM/yyyy) !";
                        txtTradon_Ngay.Focus();
                        return false;
                    }
                    DateTime NgayTraDon = DateTime.Parse(txtTradon_Ngay.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                    if (NgayTraDon < dNgayGQ)
                    {
                        lbtthongbao.Text = "Ngày trả đơn không được nhỏ hơn ngày GQ/YC !";
                        txtTradon_Ngay.Focus();
                        return false;
                    }
                    if (NgayTraDon > DateTime.Now)
                    {
                        lbtthongbao.Text = "Ngày trả đơn không được lớn hơn ngày hiện tại !";
                        txtTradon_Ngay.Focus();
                        return false;
                    }
                }
            }
            if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_YCBoSungDon)
            {
                if (txtNgayYCBS.Text != "")
                {
                    if (!Cls_Comon.IsValidDate(txtNgayYCBS.Text))
                    {
                        lbtthongbao.Text = "Bạn phải nhập ngày yêu cầu theo định dạng (dd/MM/yyyy) !";
                        txtNgayYCBS.Focus();
                        return false;
                    }
                    DateTime NgayYC = DateTime.Parse(txtNgayYCBS.Text, cul, DateTimeStyles.NoCurrentDateDefault);
                    if (NgayYC < dNgayGQ)
                    {
                        lbtthongbao.Text = "Ngày yêu cầu không được nhỏ hơn ngày GQ/YC !";
                        txtNgayYCBS.Focus();
                        return false;
                    }
                    if (NgayYC > DateTime.Now)
                    {
                        lbtthongbao.Text = "Ngày yêu cầu không được lớn hơn ngày hiện tại !";
                        txtNgayYCBS.Focus();
                        return false;
                    }
                }
                if (txtYCBS.Text.Length > 1000)
                {
                    lbtthongbao.Text = "Yêu cầu bổ sung không quá 1000 ký tự !";
                    txtYCBS.Focus();
                    return false;
                }
            }
            if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_ThuLy)
            {
                if (chkNopAnPhi.Checked == false)
                {
                    //if (txtAnPhi.Text == "")
                    //{
                    //    lbtthongbao.Text = "Bạn cần nhập án phí !";
                    //    txtAnPhi.Focus();
                    //    return false;
                    //}
                    if (txtTamUngAnPhi.Text == "")
                    {
                        lbtthongbao.Text = "Bạn cần nhập tạm ứng án phí !";
                        txtTamUngAnPhi.Focus();
                        return false;
                    }
                    if (txtHanNopAnPhi.Text == "")
                    {
                        lbtthongbao.Text = "Bạn chưa nhập hạn nộp án phí !";
                        txtHanNopAnPhi.Focus();
                        return false;
                    }
                   
                }
            }
            if (txtLyDo.Text.Length > 500)
            {
                if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon)
                {
                    lbtthongbao.Text = "Ghi chú không quá 500 ký tự !";
                }
                else
                {
                    lbtthongbao.Text = "Lý do không quá 500 ký tự !";
                }
                txtLyDo.Focus();
                return false;
            }

            //Ngay thong bao----------------------------
            if (String.IsNullOrEmpty(txtNgaythongbao.Text))
            {
                String strMsg = "";
                strMsg = "Chưa nhập Ngày Thông báo";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                txtNgaythongbao.Focus();
                return false;
            }

            return true;
        }
        protected void cmdCapNhat_Click(object sender, EventArgs e)
        {
            if (!CheckValid())
            {
                return;
            }
            String CurrUser = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
            decimal DONID = Convert.ToDecimal(current_id);
            DateTime now = DateTime.Now;

            //Kiểm tra xem đã thụ lý chưa
            AHN_DON oDon = dt.AHN_DON.Where(x => x.ID == DONID).FirstOrDefault();
            if (oDon.LOAIDON == 2)
            {
                lbtthongbao.Text = "Đơn đã chuyển sang tòa án khác xử lý !";
                return;
            }
            Decimal ToaAnNhanDonID = (Decimal)oDon.TOAANID;
            Decimal GiaiDoanDon = (Decimal)oDon.MAGIAIDOAN;

            //---------------------
            int DonXyLyID = 0;
            AHN_DON_XULY obj = new AHN_DON_XULY();
            decimal FileID = 0;
            string bienphap = dropBienPhapGQ.SelectedValue;
            decimal ToaAnCuEdit = 0;
            if (DONGHEP.IsCheckBox())
            {
                if (hddCurrID.Value != "" && hddCurrID.Value != "0")
                {
                    DonXyLyID = Convert.ToInt32(hddCurrID.Value);
                }
                else
                {
                    string lstIdChiTiet = DONGHEP.ListIdThemMoi();
                    var data = dt.AHN_DON_XULY.Where(x => x.LOAIGIAIQUYET == 3 && x.DON_CHITIETID.HasValue);
                    foreach (var item in data)
                    {
                        decimal id = item.DON_CHITIETID.HasValue ? item.DON_CHITIETID.Value : -1;
                        if (lstIdChiTiet.Contains("," + id + ","))
                        {
                            lbtthongbao.Text = "Đơn đã trả lại, không được phép thêm mới !";
                            return;
                        }
                    }
                }
                //cập nhật các đơn chi tiết có check chọn vào bảng đon xử lý
                DONGHEP.UpdateDonChiTiet(DonXyLyID, hddCurrID.Value, txtSothongbao.Text, txtNgaythongbao.Text, txtLyDo.Text, txtNgayGQ.Text, bienphap, Convert.ToDecimal(hddToaAn.Value), txtCDNN_TenCoQuan.Text, txtCDNN_NgayChuyen.Text, txtTradon_Ngay.Text, ddlLyTradon.SelectedValue, txtYCBS.Text, txtThoihanBSYC.Text);
            }
            else 
            {
                #region Update Bang Don_xuly
                if (hddCurrID.Value != "" && hddCurrID.Value != "0")
                {
                    DonXyLyID = Convert.ToInt32(hddCurrID.Value);
                    obj = dt.AHN_DON_XULY.Where(x => x.ID == DonXyLyID).FirstOrDefault();
                    ToaAnCuEdit = obj.CDTN_TOAANID.Value;
                    if (obj != null)
                    {
                        if (obj.FILEID != null) FileID = (decimal)obj.FILEID;
                        obj.NGAYSUA = now;
                        obj.NGUOISUA = CurrUser;
                    }
                    else obj = new AHN_DON_XULY();
                }
                else
                {

                    if (dt.AHN_DON_XULY.Where(x => x.DONID == DONID && x.LOAIGIAIQUYET == 5).ToList().Count > 0)
                    {
                        lbtthongbao.Text = "Đơn đã được thụ lý, không được phép thêm mới !";
                        return;
                    }
                    if (dt.AHN_DON_XULY.Where(x => x.DONID == DONID && x.LOAIGIAIQUYET == 3).ToList().Count > 0)
                    {
                        lbtthongbao.Text = "Đơn đã trả lại, không được phép thêm mới !";
                        return;
                    }
                    obj = new AHN_DON_XULY();
                    AHN_DON_BL oBL = new AHN_DON_BL();
                    //obj.SOTHONGBAO = oBL.GETFILENEWTT(ToaAnNhanDonID, GiaiDoanDon, now.Year, 0).ToString();
                    //Manhnd them kiem tra so Thong bao da co chua khi Them moi Thong bao
                    if (txtSothongbao.Text != "")
                    {
                        decimal check = oBL.CHECKSTT_AHN((decimal)oDon.TOAANID, (decimal)oDon.MAGIAIDOAN, DateTime.Now.Year, 0, Convert.ToDecimal(txtSothongbao.Text));
                        String strMsg = "";
                        String STTNew = oBL.GETFILENEWTT((decimal)oDon.TOAANID, (decimal)oDon.MAGIAIDOAN, DateTime.Now.Year, 0).ToString();
                        if (check == 1)
                        {
                            strMsg = "Số thông báo" + txtSothongbao.Text + " đã có trong hệ thống. Bạn có thể dùng số " + STTNew;
                            txtSothongbao.Text = STTNew;
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
                            txtSothongbao.Focus();
                            return;
                        }
                        else
                        {
                            obj.SOTHONGBAO = txtSothongbao.Text;
                        }
                    }

                }
                obj.NGAYTHONGBAO = (String.IsNullOrEmpty(txtNgaythongbao.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaythongbao.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                obj.LYDO = txtLyDo.Text.Trim();
                obj.NGAYGQ_YC = (String.IsNullOrEmpty(txtNgayGQ.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayGQ.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                obj.DONID = DONID;
                obj.TOAANID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);

                obj.LOAIGIAIQUYET = Convert.ToDecimal(bienphap);
                decimal rFileID = 0;
                switch (bienphap)
                {
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh:
                        obj.CDTN_TOAANID = hddToaAn.Value == "" ? 0 : Convert.ToDecimal(hddToaAn.Value);
                        //obj.CDTN_NGAYNHAN = (String.IsNullOrEmpty(txtCDTN_NgayNhan.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtCDTN_NgayNhan.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        obj.CDTN_NGAYCHUYEN = now;
                        obj.TRADON_CANCUID = 0;
                        obj.CDNN_TENCQ = "";
                        rFileID = UploadFileID(oDon, FileID, "25-DS", obj.SOTHONGBAO);
                        if (rFileID > 0) obj.FILEID = rFileID;
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonNgoaiNganh:
                        obj.CDNN_TENCQ = txtCDNN_TenCoQuan.Text.Trim();
                        obj.CDTN_NGAYNHAN = DateTime.MinValue;
                        obj.CDTN_TOAANID = 0;
                        obj.TRADON_CANCUID = 0;
                        obj.CDNN_NGAYCHUYEN = (String.IsNullOrEmpty(txtCDNN_NgayChuyen.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtCDNN_NgayChuyen.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_TraLaiDon:
                        // obj.TRADON_CANCUID = Convert.ToDecimal(txtToiDanh.Text.Trim());
                        obj.CDTN_NGAYNHAN = DateTime.MinValue;
                        obj.CDTN_TOAANID = 0;
                        obj.CDNN_TENCQ = "";
                        obj.TRADON_NGAYTRA = (String.IsNullOrEmpty(txtTradon_Ngay.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtTradon_Ngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        obj.TRADON_LYDOID = Convert.ToDecimal(ddlLyTradon.SelectedValue);
                        rFileID = UploadFileID(oDon, FileID, "27-DS", obj.SOTHONGBAO);
                        if (rFileID > 0) obj.FILEID = rFileID;
                        break;
                    case ENUM_ADS_BIENPHAPGQ.ADS_YCBoSungDon:
                        obj.TRADON_CANCUID = 0;
                        obj.CDTN_NGAYNHAN = DateTime.MinValue;
                        obj.CDTN_TOAANID = 0;
                        obj.CDNN_TENCQ = "";
                        obj.YCBS_NGAYYEUCAU = (String.IsNullOrEmpty(txtNgayYCBS.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayYCBS.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        obj.YCBS_NOIDUNG = txtYCBS.Text;
                        obj.YCBS_THOIHAN = (String.IsNullOrEmpty(txtThoihanBSYC.Text.Trim())) ? 0 : Convert.ToDecimal(txtThoihanBSYC.Text.Trim());

                        rFileID = UploadFileID(oDon, FileID, "26-DS", obj.SOTHONGBAO);
                        if (rFileID > 0) obj.FILEID = rFileID;
                        break;
                    default://Thụ lý
                        obj.TRADON_CANCUID = 0;
                        obj.CDTN_NGAYNHAN = DateTime.MinValue;
                        obj.CDTN_TOAANID = 0;
                        obj.CDNN_TENCQ = "";

                        //Cập nhật án phí
                        if (chkNopAnPhi.Checked == false)
                        {
                            rFileID = UploadFileID(oDon, FileID, "29-DS", obj.SOTHONGBAO);
                            if (rFileID > 0) obj.FILEID = rFileID;
                        }
                        break;
                }

                if (DonXyLyID == 0)
                {
                    obj.NGAYTAO = obj.NGAYSUA = now;
                    obj.NGUOISUA = obj.NGUOITAO = CurrUser;
                    dt.AHN_DON_XULY.Add(obj);
                }
                dt.SaveChanges();
                hddCurrID.Value = obj.ID.ToString();

                
                //Cập nhật thông tin án phí nếu thụ lý
                if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_ThuLy)
                {
                    List<AHN_ANPHI> lstAP = dt.AHN_ANPHI.Where(x => x.DONID == DONID).ToList();
                    AHN_ANPHI objAP = new AHN_ANPHI();
                    if (lstAP.Count > 0)
                    {
                        objAP = lstAP[0];
                        objAP.NGAYSUA = now;
                        objAP.NGUOISUA = CurrUser;
                    }
                    else
                    {
                        objAP.NGAYTAO = now;
                        objAP.NGUOITAO = CurrUser;

                    }
                    objAP.DONID = DONID;
                    objAP.TINHTRANG = chkNopAnPhi.Checked == true ? 1 : 0;
                    objAP.GIATRITRANHCHAP = (String.IsNullOrEmpty(txtGiaTriTranhChap.Text.Trim())) ? 0 : Convert.ToDecimal(txtGiaTriTranhChap.Text.Trim(), cul);
                    objAP.MUCGIAMANPHI = (String.IsNullOrEmpty(txtMucGiamAnPhi.Text.Trim())) ? 0 : Convert.ToDecimal(txtMucGiamAnPhi.Text.Trim(), cul);
                    objAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAnPhi.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAnPhi.Text.Trim(), cul);

                    objAP.SONGAYGIAHAN = (String.IsNullOrEmpty(txtSoNgayGiaHan.Text.Trim())) ? 0 : Convert.ToDecimal(txtSoNgayGiaHan.Text.Trim());
                    objAP.HANNOP_SONGAY = (String.IsNullOrEmpty(txtHanNopAnPhi.Text.Trim())) ? 0 : Convert.ToInt32(this.txtHanNopAnPhi.Text.Trim());
                    if (lstAP.Count > 0)
                    {
                        dt.SaveChanges();
                    }
                    else
                    {
                        objAP.NGAYTAO = now;
                        objAP.NGUOITAO = CurrUser;
                        dt.AHN_ANPHI.Add(objAP);
                        dt.SaveChanges();
                    }
                    ///insert vào bảng DVCQG_THANH_TOAN
                    DVCQG_THANH_TOAN_BL obj_anphi = new DVCQG_THANH_TOAN_BL();
                    obj_anphi.DVCQG_THANH_TOAN_INSERT_ANPHI(2,DONID, obj.ID, objAP.ID, "3");
                }
                else if (bienphap == ENUM_ADS_BIENPHAPGQ.ADS_ChuyenDonTrongNganh)
                {
                    //obj.CDTN_TOAANID
                    //AHN_DON oDon = dt.AHN_DON.Where(x => x.ID == DONID).FirstOrDefault();
                    oDon.LOAIDON = 2;
                    dt.SaveChanges();
                    ChuyenDonSangToaAnMoi(oDon, DonXyLyID, obj, ToaAnCuEdit);
                    //Hủy session
                    //decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    //QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
                    //if (oNSD != null)
                    //{
                    //    oNSD.IDANDANSU = 0;
                    //    dt.SaveChanges();
                    //    Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] = 0;
                    //    UpdateTrangThaiDonKK();
                    //    //Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgXLDON", "Hoàn thành chuyển đơn đến tòa án khác, bạn hãy chọn đơn khác để xử lý tiếp !", Cls_Comon.GetRootURL() + "/QLAN/AHN/Hoso/Danhsach.aspx");                    
                    //}
                }
                else
                {
                    //Hủy session
                    //decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                    //QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
                    //if (oNSD != null)
                    //{
                    //    oNSD.IDANDANSU = 0;
                    //    dt.SaveChanges();
                    //    Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] = 0;
                    //    UpdateTrangThaiDonKK();
                    //    Cls_Comon.ShowMessageAndRedirect(this, this.GetType(), "MsgXLDON", "Hoàn thành giải quyết đơn, bạn hãy chọn đơn khác để xử lý tiếp !", Cls_Comon.GetRootURL() + "/QLAN/AHN/Hoso/Danhsach.aspx");
                    //}
                }
                #endregion
            }


            //------------------------------

            //cập nhật các đơn chi tiết có check chọn vào bảng đon xử lý



            UpdateTrangThaiDonKK();
            //-------------------------------------
            hddPageIndex.Value = "1";
            LoadGrid_XuLyDon();
            Resetcontrol();
            lbtthongbao.Text = "Lưu thành công!";
        }


        private void ChuyenDonSangToaAnMoi(AHN_DON oDon, decimal DonXyLyID, AHN_DON_XULY obj, decimal ToaAnCuEdit)
        {
            if (DonXyLyID == 0)
            {
                AHN_DON oDonMoi = new AHN_DON();

                //oDonMoi.MAVUVIEC = oDon.MAVUVIEC;//////
                oDonMoi.TENVUVIEC = oDon.TENVUVIEC;
                oDonMoi.SOTHUTU = oDon.SOTHUTU;
                oDonMoi.HINHTHUCNHANDON = oDon.HINHTHUCNHANDON;
                oDonMoi.NGAYVIETDON = oDon.NGAYVIETDON;
                oDonMoi.NGAYNHANDON = oDon.NGAYNHANDON;
                oDonMoi.LOAIQUANHE = oDon.LOAIQUANHE;
                oDonMoi.QUANHEPHAPLUATID = oDon.QUANHEPHAPLUATID;
                oDonMoi.YEUTONUOCNGOAI = oDon.YEUTONUOCNGOAI;
                oDonMoi.DONKIENCUANGUOIKHAC = oDon.DONKIENCUANGUOIKHAC;
                oDonMoi.USERTT_EMAIL = oDon.USERTT_EMAIL;
                oDonMoi.USERTT_ID = oDon.USERTT_ID;
                oDonMoi.USERTT_NGAYTAO = oDon.USERTT_NGAYTAO;
                oDonMoi.USERTT_NGAYGUI = oDon.USERTT_NGAYGUI;
                oDonMoi.USERTT_NGAYBOSUNG = oDon.USERTT_NGAYBOSUNG;
                oDonMoi.NGUOITAO = oDon.NGUOITAO;
                oDonMoi.NGAYTAO = oDon.NGAYTAO;
                oDonMoi.NGUOISUA = oDon.NGUOISUA;
                oDonMoi.NGAYSUA = oDon.NGAYSUA;
                //oDonMoi.TT = oDon.TT;
                oDonMoi.MAGIAIDOAN = oDon.MAGIAIDOAN;
                oDonMoi.NOIDUNGKHOIKIEN = oDon.NOIDUNGKHOIKIEN;
                oDonMoi.MABAOMAT = oDon.MABAOMAT;
                oDonMoi.TOAPHUCTHAMID = oDon.TOAPHUCTHAMID;
                oDonMoi.QHPLTKID = oDon.QHPLTKID;
                //oDonMoi.THONGTINTHEM = oDon.THONGTINTHEM;
                oDonMoi.ID_HO_SO_FROM_TOA_CAP_CAO = oDon.ID_HO_SO_FROM_TOA_CAP_CAO;
                oDonMoi.QUANHEPHAPLUAT_NAME = oDon.QUANHEPHAPLUAT_NAME;
                oDonMoi.TENVUVIEC_PT = oDon.TENVUVIEC_PT;
                //mã vụ việc sinh ra thao tòa án mới
                AHN_DON_BL dsBL = new AHN_DON_BL();
                oDonMoi.TOAANID = obj.CDTN_TOAANID;
                DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == oDonMoi.TOAANID.Value).FirstOrDefault();
                oDonMoi.TT = dsBL.GETNEWTT((decimal)oDonMoi.TOAANID);
                oDonMoi.MAVUVIEC = ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH + oTA.MA + oDonMoi.TT.ToString();
                oDonMoi.CANBONHANDONID = 0;
                oDonMoi.THAMPHANKYNHANDON = 0;
                oDonMoi.LOAIDON = 1;
                oDonMoi.TRANGTHAI = 0;
                //lưu id đơn cũ khi chuyển đơn sang tòa án khác
                oDonMoi.DONID_TOACU = oDon.DONID_TOACU.HasValue ? oDon.DONID_TOACU.Value : oDon.ID;

                dt.AHN_DON.Add(oDonMoi);
                dt.SaveChanges();

                GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                GD.GAIDOAN_INSERT_UPDATE("3", oDonMoi.ID, 2, obj.CDTN_TOAANID.Value, 0, 0, 0, 0);
                List<DuongSuTemp> lstDuongSuTemp = new List<DuongSuTemp>();

                //lấy danh sách đương sự
                IQueryable<AHN_DON_DUONGSU> lstoDS = dt.AHN_DON_DUONGSU.Where(s => s.DONID == oDon.ID);
                foreach (var item in lstoDS)
                {
                    AHN_DON_DUONGSU oDS = new AHN_DON_DUONGSU();
                    oDS.DONID = oDonMoi.ID;
                    oDS.MADUONGSU = item.MADUONGSU;
                    oDS.TENDUONGSU = item.TENDUONGSU;
                    oDS.ISDAIDIEN = item.ISDAIDIEN;
                    oDS.TUCACHTOTUNG_MA = item.TUCACHTOTUNG_MA;
                    oDS.LOAIDUONGSU = item.LOAIDUONGSU;
                    oDS.SOCMND = item.SOCMND;
                    oDS.QUOCTICHID = item.QUOCTICHID;
                    oDS.TAMTRUID = item.TAMTRUID;
                    oDS.TAMTRUCHITIET = item.TAMTRUCHITIET;
                    oDS.HKTTID = item.HKTTID;
                    oDS.HKTTCHITIET = item.HKTTCHITIET;
                    oDS.NGAYSINH = item.NGAYSINH;
                    oDS.THANGSINH = item.THANGSINH;
                    oDS.NAMSINH = item.NAMSINH;
                    oDS.GIOITINH = item.GIOITINH;
                    oDS.NGUOIDAIDIEN = item.NGUOIDAIDIEN;
                    oDS.CHUCVU = item.CHUCVU;
                    oDS.NGUOITAO = item.NGUOITAO;
                    oDS.NGAYTAO = item.NGAYTAO;
                    oDS.NGUOISUA = item.NGUOISUA;
                    oDS.NGAYSUA = item.NGAYSUA;
                    oDS.NDD_DIACHIID = item.NDD_DIACHIID;
                    oDS.NDD_DIACHICHITIET = item.NDD_DIACHICHITIET;
                    oDS.ISSOTHAM = item.ISSOTHAM;
                    oDS.ISPHUCTHAM = item.ISPHUCTHAM;
                    oDS.ISGDT = item.ISGDT;
                    oDS.ISDON = item.ISDON;
                    oDS.EMAIL = item.EMAIL;
                    oDS.DIENTHOAI = item.DIENTHOAI;
                    oDS.FAX = item.FAX;
                    oDS.SINHSONG_NUOCNGOAI = item.SINHSONG_NUOCNGOAI;
                    oDS.HKTTTINHID = item.HKTTTINHID;
                    oDS.TAMTRUTINHID = item.TAMTRUTINHID;
                    oDS.ISBVQLNGUOIKHAC = item.ISBVQLNGUOIKHAC;
                    oDS.TUOI = item.TUOI;
                    oDS.DIACHICOQUAN = item.DIACHICOQUAN;
                    oDS.ID_DUONGSU_TACC = item.ID_DUONGSU_TACC;
                    dt.AHN_DON_DUONGSU.Add(oDS);
                    dt.SaveChanges();

                    lstDuongSuTemp.Add(new DuongSuTemp(item.ID, oDS.ID));
                }


                //lấy danh sách người tham gia tố tụng
                IQueryable<AHN_DON_THAMGIATOTUNG> lstoTT = dt.AHN_DON_THAMGIATOTUNG.Where(s => s.DONID == oDon.ID);
                foreach (var item in lstoTT)
                {
                    //lưu thông tin người tham gia tố tụng
                    AHN_DON_THAMGIATOTUNG oTT = new AHN_DON_THAMGIATOTUNG();
                    oTT.DONID = oDonMoi.ID;
                    oTT.HOTEN = item.HOTEN;
                    oTT.TAMTRUID = item.TAMTRUID;
                    oTT.TAMTRUCHITIET = item.TAMTRUCHITIET;
                    oTT.HKTTID = item.HKTTID;
                    oTT.HKTTCHITIET = item.HKTTCHITIET;
                    oTT.NGAYSINH = item.NGAYSINH;
                    oTT.THANGSINH = item.THANGSINH;
                    oTT.NAMSINH = item.NAMSINH;
                    oTT.GIOITINH = item.GIOITINH;
                    oTT.TUCACHTGTTID = item.TUCACHTGTTID;
                    oTT.NGUOIDAIDIEN = item.NGUOIDAIDIEN;
                    oTT.CHUCVU = item.CHUCVU;
                    oTT.NGAYTHAMGIA = item.NGAYTHAMGIA;
                    oTT.NGAYKETTHUC = item.NGAYKETTHUC;
                    oTT.NGAYTAO = item.NGAYTAO;
                    oTT.NGUOITAO = item.NGUOITAO;
                    oTT.NGAYSUA = item.NGAYSUA;
                    oTT.NGUOISUA = item.NGUOISUA;
                    oTT.EMAIL = item.EMAIL;
                    oTT.DIENTHOAI = item.DIENTHOAI;
                    oTT.FAX = item.FAX;
                    oTT.HKTTTINHID = item.HKTTTINHID;
                    oTT.TAMTRUTINHID = item.TAMTRUTINHID;
                    oTT.ID_DUONGSU_TACC = item.ID_DUONGSU_TACC;
                    UpdateDuongSuIdTGTT(item, oTT, lstDuongSuTemp);
                    //oTT.DUONGSUID = item.DUONGSUID;

                    dt.AHN_DON_THAMGIATOTUNG.Add(oTT);
                    dt.SaveChanges();
                }


                IQueryable<AHN_DON_TAILIEU> lstTailieu = dt.AHN_DON_TAILIEU.Where(s => s.DONID == oDon.ID);
                foreach (var item in lstTailieu)
                {
                    AHN_DON_TAILIEU objtl = new AHN_DON_TAILIEU();
                    objtl.DONID = oDonMoi.ID;
                    objtl.TENTAILIEU = item.TENTAILIEU;
                    objtl.TENFILE = item.TENFILE;
                    objtl.LOAIFILE = item.LOAIFILE;
                    objtl.NOIDUNG = item.NOIDUNG;
                    objtl.NGUOITAO = item.NGUOITAO;
                    objtl.NGAYTAO = item.NGAYTAO;
                    objtl.NGUOISUA = item.NGUOISUA;
                    objtl.NGAYSUA = item.NGAYSUA;
                    objtl.BANGIAOID = item.BANGIAOID;
                    objtl.NGAYBANGIAO = item.NGAYBANGIAO;
                    objtl.NGUOIBANGIAO = item.NGUOIBANGIAO;
                    objtl.LOAIDOITUONG = item.LOAIDOITUONG;
                    objtl.NGUOINHANID = item.NGUOINHANID;
                    dt.AHN_DON_TAILIEU.Add(objtl);
                    dt.SaveChanges();
                }
            }
            else
            {
                ///
                //kiểm tra tòa án mới xem có sửa hay không, nếu có thì thay dổi, không thì thôi
                AHN_DON oDonNew = dt.AHN_DON.FirstOrDefault(s => s.DONID_TOACU == obj.DONID && s.TOAANID == ToaAnCuEdit);
                if (oDonNew != null)
                {
                    if (oDonNew.TOAANID.Value != obj.CDTN_TOAANID)
                    {
                        //cập nhật lại thông tin tòa án
                        AHN_DON_BL dsBL = new AHN_DON_BL();
                        oDonNew.TOAANID = obj.CDTN_TOAANID;
                        DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == oDonNew.TOAANID.Value).FirstOrDefault();
                        oDonNew.TT = dsBL.GETNEWTT((decimal)oDonNew.TOAANID);
                        oDonNew.MAVUVIEC = ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH + oTA.MA + oDonNew.TT.ToString();
                        dt.SaveChanges();

                        GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                        GD.GAIDOAN_UPDATE("3", oDonNew.ID, 2, Convert.ToDecimal(oDonNew.TOAANID), 0, 0, 0, 0);
                    }
                }

            }
        }

        private void DeleteDonChuyenToaAnKhac(AHN_DON_XULY obj)
        {
            AHN_DON oDon = dt.AHN_DON.FirstOrDefault(s => s.DONID_TOACU == obj.DONID && s.TOAANID == obj.CDTN_TOAANID);
            if (oDon != null)
            {
                if (dt.AHN_DON_THAMGIATOTUNG.Count(s => s.DONID == oDon.ID) > 0)
                {
                    List<AHN_DON_THAMGIATOTUNG> lstTGTT = dt.AHN_DON_THAMGIATOTUNG.Where(s => s.DONID == oDon.ID).ToList();
                    dt.AHN_DON_THAMGIATOTUNG.RemoveRange(lstTGTT);
                }
                if (dt.AHN_DON_TAILIEU.Count(s => s.DONID == oDon.ID) > 0)
                {
                    List<AHN_DON_TAILIEU> lstTGTT = dt.AHN_DON_TAILIEU.Where(s => s.DONID == oDon.ID).ToList();
                    dt.AHN_DON_TAILIEU.RemoveRange(lstTGTT);
                }
                List<AHN_DON_DUONGSU> lstDS = dt.AHN_DON_DUONGSU.Where(s => s.DONID == oDon.ID).ToList();
                GIAI_DOAN_BL GD = new GIAI_DOAN_BL();
                GD.GIAIDOAN_DELETES("3", oDon.ID,2);

                dt.AHN_DON.Remove(oDon);
                dt.AHN_DON_DUONGSU.RemoveRange(lstDS);
                dt.SaveChanges();
            }
                

        }

        private void UpdateDuongSuIdTGTT(AHN_DON_THAMGIATOTUNG oTTOLD, AHN_DON_THAMGIATOTUNG oTTNEW, List<DuongSuTemp> lstDuongSuTemp)
        {
            string[] arrDuongSuId = oTTOLD.DUONGSUID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrDuongSuId.Length > 0)
            {
                List<decimal> lstDuongSuIdOld = arrDuongSuId.Select(s => Convert.ToDecimal(s)).ToList();
                string strDuongSuIdNew = string.Join(",", lstDuongSuTemp.Where(s => lstDuongSuIdOld.Contains(s.DuongSuIdOld)).Select(s => s.DuongSuIdNew));
                if (strDuongSuIdNew.Length > 1)
                {
                    strDuongSuIdNew = "," + strDuongSuIdNew + ",";
                }
                oTTNEW.DUONGSUID = strDuongSuIdNew;
            }
        }
        class DuongSuTemp
        {
            public decimal DuongSuIdOld { get; set; }
            public decimal DuongSuIdNew { get; set; }
            public DuongSuTemp(decimal DuongSuIdOld, decimal DuongSuIdNew)
            {
                this.DuongSuIdOld = DuongSuIdOld;
                this.DuongSuIdNew = DuongSuIdNew;
            }
        }
        void UpdateTrangThaiDonKK()
        {
            int bienphap_gd = Convert.ToInt16(dropBienPhapGQ.SelectedValue);
            string loai_an = ENUM_LOAIAN.AN_HONNHAN_GIADINH + "";
            decimal vuviecid = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "");
            string yeucau = "";
            switch (bienphap_gd)
            {
                case 1:
                    //chuyen don trong he thong
                    yeucau = txtToaAn.Text.Trim();
                    break;
                case 3:
                    //tra lai don
                    yeucau = ddlLyTradon.SelectedItem.Text;
                    break;
                case 4:
                    //yeu cau bo sung
                    yeucau = txtYCBS.Text.Trim();
                    break;
            }
            try
            {
                DAL.DKK.DKKContextContainer dkk_dt = new DAL.DKK.DKKContextContainer();
                BL.DonKK.DONKK_DON_BL objDonKK = new BL.DonKK.DONKK_DON_BL();
                objDonKK.UpdateTrangThaiDonKK(bienphap_gd, yeucau, loai_an, vuviecid);
            }
            catch (Exception ex) { }
        }

        protected void chkNopAnPhi_CheckedChanged(object sender, EventArgs e)
        {
            SetEnableZoneNopAnPhi(chkNopAnPhi.Checked);
            Cls_Comon.SetFocus(this, this.GetType(), chkNopAnPhi.ClientID);
        }
        void SetEnableZoneNopAnPhi(bool status)
        {
            txtGiaTriTranhChap.Enabled = txtMucGiamAnPhi.Enabled =  txtTamUngAnPhi.Enabled = txtHanNopAnPhi.Enabled = txtSoNgayGiaHan.Enabled = status == true ? false : true;
            if (status)
                txtGiaTriTranhChap.Text = txtMucGiamAnPhi.Text = txtTamUngAnPhi.Text = txtHanNopAnPhi.Text = txtSoNgayGiaHan.Text = "";
        }
        protected void AsyncFileUpLoad_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (AsyncFileUpLoad.HasFile)
            {
                string strFileName = AsyncFileUpLoad.FileName;
                string path = Server.MapPath("~/TempUpload/") + strFileName;
                AsyncFileUpLoad.SaveAs(path);

                path = path.Replace("\\", "/");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hddFilePath.ClientID + "\").value = '" + path + "';", true);
            }
        }
        protected void lbtDownload_Click(object sender, EventArgs e)
        {
            decimal FileID = Convert.ToDecimal(hddFileid.Value);
            AHN_FILE oND = dt.AHN_FILE.Where(x => x.ID == FileID).FirstOrDefault();
            if (oND.TENFILE != "")
            {
                var cacheKey = Guid.NewGuid().ToString("N");
                Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
            }
        }
    }
}