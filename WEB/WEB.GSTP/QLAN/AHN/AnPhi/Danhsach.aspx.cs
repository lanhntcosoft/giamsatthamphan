﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BL.GSTP.AHN;

namespace WEB.GSTP.QLAN.AHN.AnPhi
{
    public partial class Danhsach : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadDropNguoiNhan();
                    SetEnableZoneNopAnPhi(chkNopAnPhi.Checked);
                    txtHanNopAnPhi.Text = txtNgayNopAnPhi.Text = txtNgayNopBL.Text = DateTime.Now.ToString("dd/MM/yyyy", cul);
                    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    Cls_Comon.SetButton(cmdCapNhat, oPer.CAPNHAT);
                    LoadInfo_AnPhi();

                    string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                    decimal ID = Convert.ToDecimal(current_id);
                    AHN_DON oT = dt.AHN_DON.Where(x => x.ID == ID).FirstOrDefault();
                    if (oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.PHUCTHAM || oT.MAGIAIDOAN == ENUM_GIAIDOANVUAN.THULYGDT)
                    {
                        lbtthongbao.Text = "Vụ việc đã được chuyển lên tòa cấp trên, không được sửa đổi !";
                        Cls_Comon.SetButton(cmdCapNhat, false);
                        return;
                    }
                    //Kiểm tra Đơn đã trả lại đơn chưa

                    if (dt.AHN_DON_XULY.Where(x => x.DONID == ID && x.LOAIGIAIQUYET == 3).ToList().Count > 0)
                    {
                        lbtthongbao.Text = "Đơn đã trả lại, không được phép thụ lý !";
                        Cls_Comon.SetButton(cmdCapNhat, false);
                        return;
                    }
                    if (dt.AHN_DON_XULY.Where(x => x.DONID == ID && x.LOAIGIAIQUYET == 5).ToList().Count == 0)
                    {
                        lbtthongbao.Text = "Đơn chưa được giải quyết tại chức năng Giải quyết đơn !";
                        Cls_Comon.SetButton(cmdCapNhat, false);
                        return;
                    }
                    string StrMsg = "Không được sửa đổi thông tin.";
                    string Result = new AHN_CHUYEN_NHAN_AN_BL().Check_NhanAn(ID, StrMsg);
                    if (Result != "")
                    {
                        lbtthongbao.Text = Result;
                        Cls_Comon.SetButton(cmdCapNhat, false);
                        return;
                    }
                }
                catch (Exception ex) { lbtthongbao.Text = ex.Message; }
            }
        }
        private void LoadDropNguoiNhan()
        {
            ddlNguoiNhan.Items.Clear();
            DM_CANBO_BL dmCBBL = new DM_CANBO_BL();
            DataTable tbl = dmCBBL.DM_CANBO_GETBYDONVI(Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]));
            if (tbl != null && tbl.Rows.Count > 0)
            {
                ddlNguoiNhan.DataSource = tbl;
                ddlNguoiNhan.DataTextField = "MA_TEN";
                ddlNguoiNhan.DataValueField = "ID";
                ddlNguoiNhan.DataBind();
            }
            else
            {
                ddlNguoiNhan.Items.Insert(0, new ListItem("--- Chọn ---", "0"));
            }
            //Set mặc định cán bộ loginf
            try
            {
                string strCBID = Session[ENUM_SESSION.SESSION_CANBOID] + "";
                if (strCBID != "") ddlNguoiNhan.SelectedValue = strCBID;
            }
            catch { }
        }
        void LoadInfo_AnPhi()
        {
            string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
            decimal DONID = Convert.ToDecimal(current_id);

            List<AHN_ANPHI> lst = dt.AHN_ANPHI.Where(x => x.DONID == DONID).ToList<AHN_ANPHI>();
            if (lst != null && lst.Count > 0)
            {
                AHN_ANPHI obj = lst[0];
                if (obj.TINHTRANG == 1)
                    chkNopAnPhi.Checked = true;
                else
                    chkNopAnPhi.Checked = false;
                //chkNopAnPhi.Enabled = false;
                SetEnableZoneNopAnPhi(chkNopAnPhi.Checked);

                hddShowNopAnPhi.Value = obj.TINHTRANG.ToString();
           
                txtHanNopAnPhi.Text = ((obj.HANNOP == null) || ((DateTime)obj.HANNOP == DateTime.MinValue)) ? "" : ((DateTime)obj.HANNOP).ToString("dd/MM/yyyy", cul);
                txtGiaTriTranhChap.Text = (String.IsNullOrEmpty(obj.GIATRITRANHCHAP + "")) ? "" : ((decimal)obj.GIATRITRANHCHAP).ToString("#,0.###", cul);
                txtMucGiamAnPhi.Text = (String.IsNullOrEmpty(obj.MUCGIAMANPHI + "")) ? "" : ((decimal)obj.MUCGIAMANPHI).ToString("#,0.###", cul);
                txtTamUngAnPhi.Text = (String.IsNullOrEmpty(obj.TAMUNGANPHI + "")) ? "" : ((decimal)obj.TAMUNGANPHI).ToString("#,0.###", cul);
                txtAnPhi.Text = (String.IsNullOrEmpty(obj.ANPHI + "")) ? "" : ((decimal)obj.ANPHI).ToString("#,0.###", cul);                
                txtSoNgayGiaHan.Text = (String.IsNullOrEmpty(obj.SONGAYGIAHAN + "")) ? "" : ((decimal)obj.SONGAYGIAHAN).ToString("#,0.###", cul);


                txtNgayNopAnPhi.Text = (obj.NGAYNOPANPHI == null) ? "" : ((DateTime)obj.NGAYNOPANPHI).ToString("dd/MM/yyyy", cul);
                txtNgayNopBL.Text = (obj.NGAYNOPBIENLAI == null) ? "" : ((DateTime)obj.NGAYNOPBIENLAI).ToString("dd/MM/yyyy", cul);
                txtSoBienLai.Text = obj.SOBIENLAI;
                if (obj.NGUOINHANID != null) ddlNguoiNhan.SelectedValue = obj.NGUOINHANID.ToString();
                txtSoThongBao.Text = obj.SOTHONGBAO + "";
                if (((DateTime)obj.NGAYTHONGBAO).ToString("dd/MM/yyyy") != "01/01/0001")
                    txtNgayThongBao.Text = (obj.NGAYTHONGBAO == null) ? "" : ((DateTime)obj.NGAYTHONGBAO).ToString("dd/MM/yyyy", cul);

                //Cls_Comon.SetButton(cmdCapNhat, false);
            }
            else
            {
                Cls_Comon.SetButton(cmdCapNhat, true);
            }
        }
        protected void cmdxoa_Click(object sender, EventArgs e)
        {
            try
            {
                string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                decimal DONID = Convert.ToDecimal(current_id);
                AHN_ANPHI objAP = dt.AHN_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                dt.AHN_ANPHI.Remove(objAP);
                dt.SaveChanges();
                lbtthongbao.Text = "Xóa thành công!";
                resertInforAnPhi();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        void resertInforAnPhi()
        {
            txtSoBienLai.Text = null;
            txtNgayNopBL.Text = null;
            txtSoThongBao.Text = null;
            txtNgayThongBao.Text = null;
            Cls_Comon.SetButton(cmdCapNhat, true);
        }
        protected void cmdCapNhat_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkNopAnPhi.Checked == false)
                {
                    DateTime NgayNopAnPhi = DateTime.MinValue;
                    DateTime NgayNopBienLai = DateTime.MinValue;
                    if (txtNgayNopAnPhi.Text == "")
                    {
                        lbtthongbao.Text = "Chưa nhập ngày nộp tạm ứng án phí !";
                        return;
                    }
                    else
                    {
                        if (!DateTime.TryParseExact(txtNgayNopAnPhi.Text, "dd/MM/yyyy", cul, DateTimeStyles.NoCurrentDateDefault, out NgayNopAnPhi))
                        {
                            lbtthongbao.Text = "Bạn phải nhập ngày nộp tạm ứng án phí theo định dạng (dd/MM/yyyy) !";
                            txtNgayNopAnPhi.Focus();
                            return;
                        }
                        if (NgayNopAnPhi > DateTime.Today)
                        {
                            lbtthongbao.Text = "Ngày nộp tạm ứng án phí phải nhỏ hơn ngày hiện tại!";
                            txtNgayNopAnPhi.Focus();
                            return;
                        }
                    }
                    if (ddlNguoiNhan.SelectedValue == "0")
                    {
                        lbtthongbao.Text = "Chưa chọn cán bộ nhận biên lai !";
                        ddlNguoiNhan.Focus();
                        return;
                    }
                    if (txtSoBienLai.Text == "")
                    {
                        lbtthongbao.Text = "Chưa nhập số biên lai !";
                        txtSoBienLai.Focus();
                        return;
                    }
                    if (txtNgayNopBL.Text == "")
                    {
                        lbtthongbao.Text = "Chưa nhập ngày nộp biên lai !";
                        txtNgayNopBL.Focus();
                        return;
                    }
                    else
                    {
                        if (!DateTime.TryParseExact(txtNgayNopBL.Text, "dd/MM/yyyy", cul, DateTimeStyles.NoCurrentDateDefault, out NgayNopBienLai))
                        {
                            lbtthongbao.Text = "Bạn phải nhập ngày nộp biên lai theo định dạng (dd/MM/yyyy) !";
                            txtNgayNopBL.Focus();
                            return;
                        }
                        if (NgayNopBienLai > DateTime.Today)
                        {
                            lbtthongbao.Text = "Ngày nộp biên lai phải nhỏ hơn hoặc bằng ngày hiện tại!";
                            txtNgayNopBL.Focus();
                            return;
                        }
                        if (NgayNopBienLai < NgayNopAnPhi)
                        {
                            lbtthongbao.Text = "Ngày nộp biên lai phải lớn hơn hoặc bằng ngày nộp tạm ứng án phí!";
                            txtNgayNopBL.Focus();
                            return;
                        }
                    }
                }
                string current_id = Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] + "";
                decimal DONID = Convert.ToDecimal(current_id);

                AHN_DON_XULY obj = new AHN_DON_XULY();

                //-------Update bang an phi------------
                bool IsUpdate = false;
                AHN_ANPHI objAP = new AHN_ANPHI();
                #region Update bang an phi
                try
                {
                    try
                    {
                        objAP = dt.AHN_ANPHI.Where(x => x.DONID == DONID).Single<AHN_ANPHI>();
                        if (objAP != null)
                        {
                            objAP.NGAYSUA = DateTime.Now;
                            objAP.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                            IsUpdate = true;
                        }
                        else objAP = new AHN_ANPHI();
                    }
                    catch (Exception ex) { objAP = new AHN_ANPHI(); }

                    objAP.DONID = DONID;
                    objAP.GIATRITRANHCHAP = (String.IsNullOrEmpty(txtGiaTriTranhChap.Text.Trim())) ? 0 : Convert.ToDecimal(txtGiaTriTranhChap.Text.Trim().Replace(".", ""));
                    objAP.MUCGIAMANPHI = (String.IsNullOrEmpty(txtMucGiamAnPhi.Text.Trim())) ? 0 : Convert.ToDecimal(txtMucGiamAnPhi.Text.Trim().Replace(".", ""));
                    objAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAnPhi.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAnPhi.Text.Trim().Replace(".", ""));
                    objAP.ANPHI = (String.IsNullOrEmpty(txtAnPhi.Text.Trim())) ? 0 : Convert.ToDecimal(txtAnPhi.Text.Trim().Replace(".", ""));
                    objAP.SONGAYGIAHAN = (String.IsNullOrEmpty(txtSoNgayGiaHan.Text.Trim())) ? 0 : Convert.ToDecimal(txtSoNgayGiaHan.Text.Trim());
                    objAP.HANNOP = (String.IsNullOrEmpty(txtHanNopAnPhi.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtHanNopAnPhi.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    objAP.SOTHONGBAO = txtSoThongBao.Text.Trim();
                    objAP.NGAYTHONGBAO = (String.IsNullOrEmpty(txtNgayThongBao.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayThongBao.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);

                    if (chkNopAnPhi.Checked == false)
                    {
                        objAP.NGUOINHANID = Convert.ToDecimal(ddlNguoiNhan.SelectedValue);
                        objAP.SOBIENLAI = txtSoBienLai.Text.Trim();
                        objAP.NGAYNOPANPHI = (String.IsNullOrEmpty(txtNgayNopAnPhi.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayNopAnPhi.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        objAP.NGAYNOPBIENLAI = (String.IsNullOrEmpty(txtNgayNopBL.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNgayNopBL.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        //objAP.GHICHU = txtGhichu.Text.Trim();
                    }
                    else
                    {
                        objAP.TINHTRANG = 0;
                        objAP.SOBIENLAI = "";
                        objAP.NGAYNOPANPHI = DateTime.MinValue;
                        objAP.NGAYNOPBIENLAI = DateTime.MinValue;
                        //objAP.GHICHU = "";
                    }
                    if (!IsUpdate)
                    {
                        objAP.NGAYTAO = objAP.NGAYSUA = DateTime.Now;
                        objAP.NGUOISUA = objAP.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        dt.AHN_ANPHI.Add(objAP);
                    }
                    dt.SaveChanges();
                }
                catch (Exception ex) { }
                #endregion

                //-------------------------------------
                hddPageIndex.Value = "1";
                lbtthongbao.Text = "Lưu thành công!";
                cmdThuLy.Visible = true;
                Cls_Comon.SetButton(cmdCapNhat, false);
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void chkNopAnPhi_CheckedChanged(object sender, EventArgs e)
        {
            SetEnableZoneNopAnPhi(chkNopAnPhi.Checked);
        }
        void SetEnableZoneNopAnPhi(bool status)
        {
            if (status)
                hddShowNopAnPhi.Value = "1";
            else
                hddShowNopAnPhi.Value = "0";

            txtSoBienLai.Enabled = status == true ? false : true;
            txtNgayNopAnPhi.Enabled = status == true ? false : true;
            txtNgayNopBL.Enabled = status == true ? false : true;
            ddlNguoiNhan.Enabled = status == true ? false : true;
            if (hddShowNopAnPhi.Value == "0")
                pnThongTinAnPhi.Visible = false;
            else
                pnThongTinAnPhi.Visible = true;
        }
        protected void cmdThuLy_Click(object sender, EventArgs e)
        {
            Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHN/Sotham/ThuLy.aspx");
        }
        protected void txtNgayNopAnPhi_TextChanged(object sender, EventArgs e)
        {
            txtNgayNopBL.Text = txtNgayNopAnPhi.Text;
        }
    }
}