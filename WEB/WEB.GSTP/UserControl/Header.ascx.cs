﻿using DAL.GSTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Module.Common;
using BL.GSTP;
using System.Data;
using BL.GSTP.THA;
using System.Web.Services;
using System.IO;

namespace WEB.GSTP.UserControl
{
    public partial class Header : System.Web.UI.UserControl
    {
        GSTPContext dt = new GSTPContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strUserID = Session[ENUM_SESSION.SESSION_USERID] + "";

                if (strUserID == "") Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
                string strMaHeThong = Session["MaHeThong"] + "";
                if (strMaHeThong == "") Response.Redirect(Cls_Comon.GetRootURL() + "/Launcher.aspx");


                lstUserName.Text = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                if (!IsPostBack)
                {
                    //List<QT_CHUONGTRINH> lst = dt.QT_CHUONGTRINH.OrderBy(x => x.THUTU).ToList();
                    decimal IDHethong = Convert.ToDecimal(strMaHeThong);
                    //Load các Hệ thống khác
                    QT_NGUOIDUNG_BL oBL = new QT_NGUOIDUNG_BL();
                    decimal USERID = Convert.ToDecimal(strUserID);
                    DataTable lstHT;
                    string strSessionKeyHT = "HETHONGGETBY_" + USERID.ToString();
                    if (Session[strSessionKeyHT] == null)
                        lstHT = oBL.QT_HETHONG_GETBYUSER(USERID);
                    else
                        lstHT = (DataTable)Session[strSessionKeyHT];
                    liGSTP.Visible = liQLA.Visible = liTDKT.Visible = liTCCB.Visible = liQTHT.Visible = false;
                    if (lstHT.Rows.Count == 1) lbtBack.Visible = false;
                    if (lstHT.Rows.Count > 0)
                    {
                        foreach (DataRow r in lstHT.Rows)
                        {
                            switch (r["MA"] + "")
                            {
                                case "GSTP":
                                    liGSTP.Visible = true;
                                    break;
                                case "QLA":
                                    liQLA.Visible = true;
                                    break;
                                case "GDT":
                                    liGDT.Visible = true;
                                    break;
                                case "TDKT":
                                    liTDKT.Visible = true;
                                    break;
                                case "TCCB":
                                    liTCCB.Visible = true;
                                    break;
                                case "QTHT":
                                    liQTHT.Visible = true;
                                    break;
                            }
                        }
                    }

                    QT_HETHONG oHT = dt.QT_HETHONG.Where(x => x.ID == IDHethong).FirstOrDefault();
                    decimal TOAAN_ID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                    DM_TOAAN otToaAN = dt.DM_TOAAN.Where(x => x.ID == TOAAN_ID).FirstOrDefault();
                    lbtThemmoi.Visible = true;
                    if (otToaAN.LOAITOA == "CAPCAO" || otToaAN.LOAITOA == "TOICAO") lbtThemmoi.Visible = false;
                    lstHethong.Text = oHT.TEN.ToString().ToUpper() + "&nbsp;&nbsp; -&nbsp;&nbsp; " + otToaAN.MA_TEN.ToUpper();
                    if(Session[ENUM_SESSION.SESSION_PHONGBANID]+""=="62")//anhvh add 12/10/2020 phòng văn thư
                    {
                        lstHethong.Text = "QUẢN LÝ VĂN BẢN ĐẾN - TÒA ÁN NHÂN DÂN TỐI CAO";
                    }
                    DataTable lst;
                    string strSessionKey = "CHUONGTRINH_" + USERID.ToString() + "_" + IDHethong.ToString();
                    if (Session[strSessionKey] == null)
                        lst = oBL.QT_CHUONGTRINH_GETBYUSER(USERID, IDHethong);
                    else
                        lst = (DataTable)Session[strSessionKey];
                    rptMenu.DataSource = lst;
                    rptMenu.DataBind();
                    divSearch.Visible = false;
                    if (oHT.MA == "GSTP")
                    {
                        decimal ThamPhanID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_GSTP]);
                        LoadGhimThamPhan(ThamPhanID);
                        divGSTP.Visible = true;
                    }
                    else
                    {
                        divGSTP.Visible = false;
                    }

                    string strMaCT = Session["MaChuongTrinh"] + "";
                    //-------------
                    if (strMaCT == "HDSD_APP")
                    {
                        lk_HDSD.CssClass = "menubuttonActive";
                        lbtTrangchu.CssClass = "menubutton";
                    }
                    else
                        lk_HDSD.CssClass = "menubutton";
                    //----------
                    if (otToaAN.LOAITOA == "CAPTINH" && strMaCT == ENUM_LOAIAN.BPXLHC) lbtThemmoi.Visible = false;
                    if (strMaCT == ENUM_LOAIAN.AN_GDTTT) lbtThemmoi.Visible = false;
                    if (strMaCT != "" & strMaCT != "0")
                    {
                        bool flag = false;
                        foreach (RepeaterItem oItem in rptMenu.Items)
                        {
                            LinkButton cmdButton = (LinkButton)oItem.FindControl("lbtChuongTrinh");
                            String ND_id = cmdButton.CommandArgument.ToString();
                            String[] ND_id_arr = ND_id.Split(';');
                            if (ND_id_arr[0] == strMaCT)
                            {
                                cmdButton.CssClass = "menubuttonActive";
                                flag = true;
                            }
                            else
                                cmdButton.CssClass = "menubutton";
                        }
                        QT_CHUONGTRINH oT = dt.QT_CHUONGTRINH.Where(x => x.MA == strMaCT).FirstOrDefault();
                        if (oT.ISLOAIAN == 1) divSearch.Visible = true;
                        if (flag == true) lbtTrangchu.CssClass = "menubutton";

                        if (strMaCT == "AN_HINHSU")
                        {
                            txtSearch.Attributes.Add("placeholder", "Tên bị can");
                        }
                        else
                        {
                            txtSearch.Attributes.Add("placeholder", "Tên đương sự");
                        }

                        string strIDVUAN = Session[strMaCT] + "";
                        if ((strIDVUAN == "" || strIDVUAN == "0") && strMaCT != ENUM_LOAIAN.AN_GSTP)
                        {
                            lstTenvuan.Text = "Bạn chưa chọn vụ án để lưu thông tin !";
                            cmdIn.Visible = lbtChitiet.Visible = lbtHuyghim.Visible = lbtTongdat.Visible = false;
                            return;
                        }
                        decimal IDVuViec = 0;
                        DM_TOAAN oTA = new DM_TOAAN();
                        string strNgayVuViec = "";
                        string strTieudeNVV = "Ngày vụ việc";
                        if (strMaCT == ENUM_LOAIAN.AN_HINHSU)
                        {
                            lbtChitiet.Text = "Chi tiết vụ án";
                        }
                        else
                        {
                            lbtChitiet.Text = "Chi tiết vụ việc";
                        }
                        switch (strMaCT)
                        {
                            case ENUM_LOAIAN.AN_HINHSU:
                                #region "Hình sự"
                                //Thông tin vụ án dân sự
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HINHSU]);
                                AHS_VUAN obj = dt.AHS_VUAN.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = obj.MAVUAN;
                                lstTenvuan.Text = obj.TENVUAN;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == obj.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                string NgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                LoadBaocao(IDVuViec, 1, (decimal)obj.MAGIAIDOAN);
                                switch (Convert.ToInt16(obj.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Ngày giao hồ sơ";
                                        NgayVuViec = ((DateTime)obj.NGAYGIAO).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        AHS_SOTHAM_THULY objST = dt.AHS_SOTHAM_THULY.Where(x => x.VUANID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            NgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        AHS_PHUCTHAM_THULY objPhucTham = dt.AHS_PHUCTHAM_THULY.Where(x => x.VUANID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            NgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (obj.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == obj.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        NgayVuViec = ((DateTime)obj.NGAYXAYRA).ToString("dd/MM/yyyy");
                                        break;

                                }
                                lstNgayvuan.Text = NgayVuViec;
                                lstTitleNgay.Text = strTieudeNVV;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_DANSU:
                                #region "Dân sự"
                                //Thông tin vụ án dân sự
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_DANSU]);
                                ADS_DON oDon = dt.ADS_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDon.MAVUVIEC;
                                lstTenvuan.Text = oDon.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDon.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";

                                if (oDon.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 2, (decimal)oDon.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDon.MAGIAIDOAN == null ? 1 : oDon.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDon.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        ADS_SOTHAM_THULY objST = dt.ADS_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        ADS_PHUCTHAM_THULY objPhucTham = dt.ADS_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDon.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDon.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                                #region "HNGD"
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH]);
                                AHN_DON oDonHN = dt.AHN_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonHN.MAVUVIEC;
                                lstTenvuan.Text = oDonHN.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonHN.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                if (oDonHN.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 3, (decimal)oDonHN.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonHN.MAGIAIDOAN == null ? 1 : oDonHN.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonHN.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        AHN_SOTHAM_THULY objST = dt.AHN_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        AHN_PHUCTHAM_THULY objPhucTham = dt.AHN_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonHN.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonHN.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                                #region "KDTM"
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI]);
                                AKT_DON oDonKT = dt.AKT_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonKT.MAVUVIEC;
                                lstTenvuan.Text = oDonKT.TENVUVIEC;
                                
                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonKT.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                if (oDonKT.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 4, (decimal)oDonKT.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonKT.MAGIAIDOAN == null ? 1 : oDonKT.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonKT.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        AKT_SOTHAM_THULY objST = dt.AKT_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        AKT_PHUCTHAM_THULY objPhucTham = dt.AKT_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonKT.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonKT.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_LAODONG:
                                #region "LD"
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_LAODONG]);
                                ALD_DON oDonLD = dt.ALD_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonLD.MAVUVIEC;
                                lstTenvuan.Text = oDonLD.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonLD.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                if (oDonLD.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 5, (decimal)oDonLD.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonLD.MAGIAIDOAN == null ? 1 : oDonLD.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonLD.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        ALD_SOTHAM_THULY objST = dt.ALD_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        ALD_PHUCTHAM_THULY objPhucTham = dt.ALD_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonLD.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonLD.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_HANHCHINH:
                                #region "HC"
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_HANHCHINH]);
                                AHC_DON oDonHC = dt.AHC_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonHC.MAVUVIEC;
                                lstTenvuan.Text = oDonHC.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonHC.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                if (oDonHC.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 6, (decimal)oDonHC.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonHC.MAGIAIDOAN == null ? 1 : oDonHC.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonHC.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        AHC_SOTHAM_THULY objST = dt.AHC_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        AHC_PHUCTHAM_THULY objPhucTham = dt.AHC_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonHC.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonHC.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_PHASAN:
                                #region "PS"
                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_PHASAN]);
                                APS_DON oDonPS = dt.APS_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonPS.MAVUVIEC;
                                lstTenvuan.Text = oDonPS.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonPS.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                if (oDonPS.HINHTHUCNHANDON == 3) lttTitleMaVuAn.Text = "@Mã vụ việc";
                                LoadBaocao(IDVuViec, 7, (decimal)oDonPS.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonPS.MAGIAIDOAN == null ? 1 : oDonPS.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonPS.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        APS_SOTHAM_THULY objST = dt.APS_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        APS_PHUCTHAM_THULY objPhucTham = dt.APS_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonPS.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonPS.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.BPXLHC:
                                #region "XLHC"

                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.BPXLHC]);
                                XLHC_DON oDonXL = dt.XLHC_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonXL.MAVUVIEC;
                                lstTenvuan.Text = oDonXL.TENVUVIEC;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonXL.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN.Replace("Tòa án nhân dân", "TAND");
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày vụ việc";
                                LoadBaocao(IDVuViec, 8, (decimal)oDonXL.MAGIAIDOAN);
                                switch (Convert.ToInt16(oDonXL.MAGIAIDOAN == null ? 1 : oDonXL.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Nhận hồ sơ";
                                        strNgayVuViec = ((DateTime)oDonXL.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý sơ thẩm";
                                        XLHC_SOTHAM_THULY objST = dt.XLHC_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objST != null)
                                            strNgayVuViec = ((DateTime)objST.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.PHUCTHAM:
                                        strTieudeNVV = "Thụ lý phúc thẩm";
                                        XLHC_PHUCTHAM_THULY objPhucTham = dt.XLHC_PHUCTHAM_THULY.Where(x => x.DONID == IDVuViec).FirstOrDefault();
                                        if (objPhucTham != null)
                                            strNgayVuViec = ((DateTime)objPhucTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        if (oDonXL.TOAPHUCTHAMID != null)
                                        {
                                            DM_TOAAN oTAPT = dt.DM_TOAAN.Where(x => x.ID == oDonXL.TOAPHUCTHAMID).FirstOrDefault();
                                            lstToaan.Text = oTAPT.TEN.Replace("Tòa án nhân dân", "TAND");
                                        }
                                        break;
                                    case ENUM_GIAIDOANVUAN.THULYGDT:
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_GSTP:
                                #region "GSTP"
                                decimal ThamPhanID = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_GSTP]);
                                LoadGhimThamPhan(ThamPhanID);
                                divSearch.Visible = false;
                                divGSTP.Visible = true;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_GDTTT:
                                #region "GDT TT"

                                IDVuViec = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_GDTTT]);
                                GDTTT_DON oDonGDT = dt.GDTTT_DON.Where(x => x.ID == IDVuViec).FirstOrDefault();
                                lstMavuan.Text = oDonGDT.MADON;
                                lstTenvuan.Text = oDonGDT.NGUOIGUI_HOTEN;

                                oTA = dt.DM_TOAAN.Where(x => x.ID == oDonGDT.TOAANID).FirstOrDefault();
                                lstToaan.Text = oTA.TEN;
                                strNgayVuViec = "";
                                strTieudeNVV = "Ngày nhận";
                                switch (Convert.ToInt16(oDonGDT.MAGIAIDOAN == null ? 1 : oDonGDT.MAGIAIDOAN))
                                {
                                    case ENUM_GIAIDOANVUAN.HOSO:
                                        strTieudeNVV = "Ngày nhận";
                                        strNgayVuViec = ((DateTime)oDonGDT.NGAYNHANDON).ToString("dd/MM/yyyy");
                                        break;
                                    case ENUM_GIAIDOANVUAN.SOTHAM:
                                        strTieudeNVV = "Thụ lý GĐT, TT";
                                        //APS_SOTHAM_THULY objSoTham = dt.APS_SOTHAM_THULY.Where(x => x.DONID == IDVuViec).Single<APS_SOTHAM_THULY>();
                                        //if (objSoTham != null)
                                        //    strNgayVuViec = ((DateTime)objSoTham.NGAYTHULY).ToString("dd/MM/yyyy");
                                        break;
                                    default:
                                        strNgayVuViec = "";
                                        break;

                                }
                                lstTitleNgay.Text = strTieudeNVV;
                                lstNgayvuan.Text = strNgayVuViec;
                                #endregion
                                break;
                            case ENUM_LOAIAN.AN_THA:
                                #region "THA"
                                Decimal IDBiAn = Convert.ToDecimal(Session[ENUM_LOAIAN.AN_THA]);
                                THA_BIAN_BL objBA_BL = new THA_BIAN_BL();
                                try
                                {
                                    DataRow row = objBA_BL.GetInfo(IDBiAn);
                                    lttTitleMaVuAn.Text = "Mã bị án:";
                                    lstMavuan.Text = row["MaBiCan"] + "";

                                    lttTitleVuViec.Text = "Bị án:";
                                    lstTenvuan.Text = row["HoTen"] + "";

                                    lstToaan.Text = row["TenToaAn"] + "";

                                    strNgayVuViec = (String.IsNullOrEmpty(row["NgayThamGia"] + "")) ? "" : (Convert.ToDateTime(row["NgayThamGia"] + "")).ToString("dd/MM/yyyy");
                                    lstTitleNgay.Text = "Ngày tham gia vụ án:";
                                    lstTitleNgay.Text = strNgayVuViec;
                                }
                                catch (Exception ex) { }
                                #endregion
                                break;
                        }
                        lstTenvuan.Text = Cls_Comon.CatXau(lstTenvuan.Text, 150);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void LoadBaocao(decimal vDonID, decimal LoaiAn, decimal vGiaiDoan)
        {
            hddDonViID.Value = vDonID.ToString();
            hddLoaiAn.Value = LoaiAn.ToString();
            hddGiaidoan.Value = vGiaiDoan.ToString();
            //ADS_DON_BL oBL = new ADS_DON_BL();
            //rptBaocao.DataSource = oBL.ADS_FILE_GETBYDON(vDonID, LoaiAn, vGiaiDoan);
            //rptBaocao.DataBind();
        }
        protected void cmdTrangchu_Click(object sender, EventArgs e)
        {
            Session["MaChuongTrinh"] = "0";
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void lkTaiLieu_Click(object sender, EventArgs e)
        {

        }
        protected void rptMenu_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SELECT":
                    String ND_id = e.CommandArgument.ToString();
                    String[] ND_id_arr = ND_id.Split(';');
                    Session["MaChuongTrinh"] = ND_id_arr[0];
                    if (ND_id_arr[0] == ENUM_LOAIAN.AN_GSTP)
                    {
                        Response.Redirect(Cls_Comon.GetRootURL() + "/GSTP/Thongtintonghop.aspx");
                    }
                    else if (ND_id_arr[0] == ENUM_LOAIAN.PCTT)
                    {
                        Response.Redirect(Cls_Comon.GetRootURL() + "/PCTP/PhanCongNgauNhien.aspx");
                    }
                    else if (ND_id_arr[0] == "BC_VGDKT")
                    {
                        Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/GDTTT/VuAn/BaoCao/BAOCAO_VGDKT.aspx");
                    }
                    else
                    {
                        Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
                    }
                    break;
            }
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            int so = int.Parse(Application.Get("OnlineNow").ToString());
            if (so > 0) so--;
            else
                so = 0;
            Application.Set("OnlineNow", so);
            // Xóa file js khi hết phiên Logout
            string strFileName_js = "mapdata_" + Session[ENUM_SESSION.SESSION_DONVIID] + ".js";
            string path_js = Server.MapPath("~/GSTP/") + strFileName_js;
            FileInfo oF_js = new FileInfo(path_js);
            if (oF_js.Exists)// Nếu file đã tồn tại
            {
                File.Delete(path_js);
            }
            //
            Session.Clear();
            Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
        }
        protected void lblBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(Cls_Comon.GetRootURL() + "/Launcher.aspx");
        }
        protected void lbtChangePass_Click(object sender, EventArgs e)
        {
            Response.Redirect(Cls_Comon.GetRootURL() + "/ChangePass.aspx");
        }
        protected void cmdTracuu_Click(object sender, EventArgs e)
        {
            string strMCT = Session["MaChuongTrinh"] + "";
            Session["textsearch"] = txtSearch.Text;
            switch (strMCT)
            {
                case ENUM_LOAIAN.AN_HINHSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_DANSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHN/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AKT/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_LAODONG:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_HANHCHINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHC/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_PHASAN:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/APS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.BPXLHC:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/XLHC/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_GDTTT:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/GDTTT/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_THA:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/THA/Danhsach.aspx");
                    break;
            }
        }
        protected void cmdDanhsach_Click(object sender, EventArgs e)
        {
            string strMCT = Session["MaChuongTrinh"] + "";
            switch (strMCT)
            {
                case ENUM_LOAIAN.AN_HINHSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_DANSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHN/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AKT/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_LAODONG:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_HANHCHINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHC/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_GSTP:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/GSTP/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_PHASAN:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/APS/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.BPXLHC:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/XLHC/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_GDTTT:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/GDTTT/Hoso/Danhsach.aspx");
                    break;
                case ENUM_LOAIAN.AN_THA:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/THA/Danhsach.aspx");
                    break;
            }
        }
        void HuyGhim()
        {
            Decimal IDVuViec = 0;
            decimal IDUser = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            QT_NGUOISUDUNG oNSD = dt.QT_NGUOISUDUNG.Where(x => x.ID == IDUser).FirstOrDefault();
            if (oNSD != null)
            {
                string strMCT = Session["MaChuongTrinh"] + "";
                switch (strMCT)
                {
                    case ENUM_LOAIAN.AN_HINHSU:
                        //---huy vu an da ghim 
                        oNSD.IDAHINHSU = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_HINHSU] = IDVuViec;
                        break;
                    case ENUM_LOAIAN.AN_DANSU:
                        //---huy vu an da ghim 
                        oNSD.IDANDANSU = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_DANSU] = IDVuViec;
                        break;
                    case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                        oNSD.IDANHNGD = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_HONNHAN_GIADINH] = IDVuViec;
                        break;
                    case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                        oNSD.IDANKDTM = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI] = IDVuViec;
                        break;
                    case ENUM_LOAIAN.AN_LAODONG:
                        oNSD.IDANLAODONG = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_LAODONG] = IDVuViec;
                        break;
                    case ENUM_LOAIAN.AN_HANHCHINH:
                        oNSD.IDANHANHCHINH = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_HANHCHINH] = IDVuViec;

                        break;
                    case ENUM_LOAIAN.AN_PHASAN:
                        oNSD.IDANPHASAN = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_PHASAN] = IDVuViec;

                        break;
                    case ENUM_LOAIAN.BPXLHC:
                        oNSD.IDBPXLHC = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.BPXLHC] = IDVuViec;

                        break;
                    case ENUM_LOAIAN.AN_GDTTT:
                        oNSD.IDGDTTT = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_GDTTT] = IDVuViec;

                        break;
                    case ENUM_LOAIAN.AN_THA:
                        oNSD.IDTHA = IDVuViec;
                        dt.SaveChanges();
                        Session[ENUM_LOAIAN.AN_THA] = IDVuViec;

                        break;
                }
            }
        }
        protected void cmdThemmoi_Click(object sender, EventArgs e)
        {
            HuyGhim();
            Session["DS_THEMDSK"] = null;
            Session["HC_THEMDSK"] = null;
            Session["HN_THEMDSK"] = null;
            Session["KT_THEMDSK"] = null;
            Session["LD_THEMDSK"] = null;
            Session["PS_THEMDSK"] = null;
            Session["XLHC_THEMDSK"] = null;
            string strMCT = Session["MaChuongTrinh"] + "";
            switch (strMCT)
            {
                case ENUM_LOAIAN.AN_HINHSU:
                    //ko doi tham so type = list --> vao man hinh nhap co xu ly thong tin
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Hoso/Thongtinan.aspx?type=list");
                    break;
                case ENUM_LOAIAN.AN_DANSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHN/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AKT/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_LAODONG:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_HANHCHINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHC/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_PHASAN:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/APS/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.BPXLHC:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/XLHC/Hoso/Thongtindon.aspx?type=new");
                    break;
                case ENUM_LOAIAN.AN_GDTTT:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/GDTTT/Hoso/Thongtindon.aspx");
                    break;
                case ENUM_LOAIAN.AN_THA:
                    //ko doi tham so type = list --> vao man hinh nhap co xu ly thong tin
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/THA/Hoso/ThongtinVA.aspx?type=list");
                    break;
            }
        }
        protected void cmdTracuuTP_Click(object sender, EventArgs e)
        {
            DM_CANBO_BL bl = new DM_CANBO_BL();
            string Ten = txtTenTP.Text.Trim();
            decimal DonViLoginID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
            DataTable tbl = bl.DM_CANBO_GET_THAMPHAN_TEN_DV(Ten, DonViLoginID, ENUM_DANHMUC.CHUCDANH);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                Ten = ltrTenThamPhan.Text = tbl.Rows[0]["HOTEN"].ToString();
                ltrChucDanh.Text = tbl.Rows[0]["CHUCDANH"].ToString();
                ltrTenToaAn.Text = tbl.Rows[0]["TENTOAAN"].ToString();
                ltrQDBoNhiem.Text = tbl.Rows[0]["QDBONHIEM"].ToString();
                Session[ENUM_LOAIAN.AN_GSTP] = tbl.Rows[0]["ID"].ToString();
                Session["MaChuongTrinh"] = ENUM_LOAIAN.AN_GSTP;
            }
            else
            {
                ltrTenThamPhan.Text = "Chưa chọn thẩm phán!";
                ltrChucDanh.Text = ltrTenToaAn.Text = ltrQDBoNhiem.Text = "";
            }
            Session["TenThamPhan"] = Ten;
            Session["HSC"] = "1";
            Response.Redirect(Cls_Comon.GetRootURL() + "/GSTP/Danhsach.aspx");
        }
        protected void cmdDanhsachAnHuy_Click(object sender, EventArgs e)
        {
            Response.Redirect(Cls_Comon.GetRootURL() + "/GSTP/DanhsachAnHuy.aspx");
        }
        protected void btnGSTP_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "GSTP").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void btnQLA_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "QLA").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void btnGDT_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "GDT").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void btnTCCB_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "TCCB").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void btnTDKT_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "TDKT").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void btnQTHT_Click(object sender, EventArgs e)
        {
            QT_HETHONG oT = dt.QT_HETHONG.Where(x => x.MA == "QTHT").FirstOrDefault();
            Session["MaHeThong"] = oT.ID;
            Session["MaChuongTrinh"] = null;
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void cmbBack_Click(object sender, EventArgs e)
        {

            Response.Redirect(Cls_Comon.GetRootURL() + "/Launcher.aspx");
        }
        protected void cmdHuyghim_Click(object sender, EventArgs e)
        {
            HuyGhim();
            Response.Redirect(Cls_Comon.GetRootURL() + "/Trangchu.aspx");
        }
        protected void cmdTongdat_Click(object sender, EventArgs e)
        {

            string strMCT = Session["MaChuongTrinh"] + "";
            switch (strMCT)
            {
                case ENUM_LOAIAN.AN_HINHSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHS/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_DANSU:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ADS/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHN/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AKT/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_LAODONG:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/ALD/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_HANHCHINH:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/AHC/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.AN_PHASAN:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/APS/Tongdat.aspx");
                    break;
                case ENUM_LOAIAN.BPXLHC:
                    Response.Redirect(Cls_Comon.GetRootURL() + "/QLAN/XLHC/Tongdat.aspx");
                    break;
            }
        }
        protected void cmdChitiet_Click(object sender, EventArgs e)
        {
            Session["GSTP_CALL_VUAN_INFO"] = null;
            string strMCT = Session["MaChuongTrinh"] + "";
            switch (strMCT)
            {
                case ENUM_LOAIAN.AN_HINHSU:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_AHS")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Thông tin án hình sự", "CallPopupAHS();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_DANSU:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_ADS")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_ADS", "CallPopupADS();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_HONNHAN_GIADINH:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_AHN")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_AHN", "CallPopupAHN();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_KINHDOANH_THUONGMAI:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_AKT")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_AKT", "CallPopupAKT();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_LAODONG:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_ALD")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_ALD", "CallPopupALD();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_HANHCHINH:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_AHC")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_AHC", "CallPopupAHC();", true);
                    }
                    break;
                case ENUM_LOAIAN.AN_PHASAN:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_APS")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_APS", "CallPopupAPS();", true);
                    }
                    break;
                case ENUM_LOAIAN.BPXLHC:
                    if (!(Page.ClientScript.IsStartupScriptRegistered("pThongTin_BPXLHC")))
                    {
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pThongTin_BPXLHC", "CallPopupBPXLHC();", true);
                    }
                    break;
            }

        }
        private void LoadGhimThamPhan(decimal ThamPhanID)
        {
            ltrTenThamPhan.Text = ltrChucDanh.Text = ltrQDBoNhiem.Text = ltrTenToaAn.Text = "";
            DM_CANBO cbo = dt.DM_CANBO.Where(x => x.ID == ThamPhanID).FirstOrDefault();
            if (cbo != null)
            {
                ltrTenThamPhan.Text = cbo.HOTEN;
                DM_DATAITEM ChucDanh = dt.DM_DATAITEM.Where(x => x.ID == cbo.CHUCDANHID).FirstOrDefault();
                if (ChucDanh != null)
                {
                    ltrChucDanh.Text = ChucDanh.TEN;
                }
                DM_TOAAN oTA = dt.DM_TOAAN.Where(x => x.ID == cbo.TOAANID).FirstOrDefault();
                if (oTA != null)
                {
                    ltrTenToaAn.Text = oTA.TEN;
                }
                string boNhiem = cbo.NGAYBONHIEM == null ? "" : " ngày " + ((DateTime)cbo.NGAYBONHIEM).ToString("dd/MM/yyyy");
                ltrQDBoNhiem.Text = cbo.QDBONHIEM + boNhiem;

            }
            else
            {
                ltrTenThamPhan.Text = "Chưa chọn thẩm phán!";
            }
        }
        //-----------------------------
        protected void lk_HDSD_Click(object sender, EventArgs e)
        {
            Session["MaChuongTrinh"] = "HDSD_APP";
            Response.Redirect(Cls_Comon.GetRootURL() + "/HDSD/HDSD.aspx");
        }

    }
}