var simplemaps_usmap_mapdata={
  main_settings: {
	//General settings
	width: "600",
    background_color: "#F2F2F2",
    background_transparent: "no",
    border_color: "#F2F2F2",
    popups: "detect",
    
	//State defaults
	state_description: "",
    state_color: "#f59d63",
    state_hover_color: "#fb3413",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "no",
    
		//Location defaults
		location_description: "Store closest to you!",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_url: "",
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
		//Label defaults
		label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
   
		//Zoom settings
		manual_zoom: "no",
    back_image: "no",
    arrow_color: "#cecece",
    arrow_color_border: "#808080",
    initial_back: "no",
    initial_zoom: -1,
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
		//Popup settings
		popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
		//Advanced settings
		div: "map",
    auto_load: "yes",
    url_new_tab: "yes",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website"
  },
  state_specific: {
    VNM429: {
		name: "TAND tỉnh Quảng Ninh",
		description: "http://quangninh.toaan.gov.vn",
		color: "default",
		hover_color: "default",
		url: "http://quangninh.toaan.gov.vn"
    },
    VNM444: {
        name: "TAND tỉnh Tây Ninh",
		description: "http://tayninh.toaan.gov.vn",
		color: "default",
		hover_color: "default",
        url: "http://tayninh.toaan.gov.vn"
    },
    VNM450: {
      name: "TAND tỉnh Điện Biên",
	  description: "http://dienbien.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://dienbien.toaan.gov.vn"
    },
    VNM451: {
      name: "TAND tỉnh Bắc Kạn",
	  description: "http://backan.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://backan.toaan.gov.vn"
    },
    VNM452: {
      name: "TAND tỉnh Thái Nguyên",
	  description: "http://thainguyen.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://thainguyen.toaan.gov.vn"
    },
    VNM453: {
      name: "TAND tỉnh Lai Châu",
	  description: "http://laichau.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://laichau.toaan.gov.vn"
    },
    VNM454: {
      name: "TAND tỉnh Lạng Sơn",
	  description: "http://langson.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://langson.toaan.gov.vn"
    },
    VNM455: {
      name: "TAND tỉnh Sơn La",
	  description: "Tổng số vụ án: 150<br>Thụ lý án: 50 <br> Giải quyết án: 100<br>http://sonla.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://sonla.toaan.gov.vn"
    },
    VNM456: {
		name: "TAND tỉnh Thanh Hóa",
		description: "Tổng số vụ án: 150<br>Thụ lý án: 50 <br> Giải quyết án: 100<br>http://thanhhoa.toaan.gov.vn",
		color: "default",
		hover_color: "default",
		url: "http://thanhhoa.toaan.gov.vn"
    },
    VNM457: {
      name: "TAND tỉnh Tuyên Quang",
	  description: "http://tuyenquang.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://tuyenquang.toaan.gov.vn"
    },
    VNM458: {
      name: "TAND tỉnh Yên Bái",
	  description: "http://yenbai.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://yenbai.toaan.gov.vn"
    },
    VNM459: {
      name: "TAND tỉnh TAND tỉnh Hòa Bình",
	  description:"Tổng số vụ án: 150<br>Thụ lý án: 100 <br> Giải quyết án: 50<br>https://hoabinh.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url:"https://hoabinh.toaan.gov.vn"
    },
    VNM460: {
      name: "TAND tỉnh Hải Dương",
	  description: "http://haiduong.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://haiduong.toaan.gov.vn"
    },
    VNM4600: {
      name: "TAND tỉnh Hải Phòng",
	  description: "http://haiphong.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://haiphong.toaan.gov.vn"
    },
    VNM461: {
      name: "TAND tỉnh Hưng Yên",
	  description:"Tổng số vụ án: 150<br>Thụ lý án: 100 <br> Giải quyết án: 50<br>https://hungyen.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url:"https://hungyen.toaan.gov.vn"
    },
    VNM462: {
       name: "TAND thành phố Hà Nội",
       description: "Tổng số vụ án: 350<br>Thụ lý án: 100 <br> Giải quyết án: 250<br>http://hanoi.toaan.gov.vn",
	   color: "default",
	   hover_color: "default",
       url: "http://hanoi.toaan.gov.vn"
    },
    VNM463: {
      name: "TAND tỉnh Bắc Ninh",
	  description: "http://bacninh.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://bacninh.toaan.gov.vn"
    },
    VNM464: {
      name: "TAND tỉnh Vĩnh Phúc",
	  description: "http://vinhphuc.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://vinhphuc.toaan.gov.vn"
    },
    VNM466: {
      name: "TAND tỉnh Ninh Bình",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM467: {
      name: "TAND tỉnh TAND tỉnh Hà Nam",
	  description:"Tổng số vụ án: 250<br>Thụ lý án: 50 <br> Giải quyết án: 200<br>https://hanam.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url:"https://hanam.toaan.gov.vn"
    },
    VNM468: {
	  name: "TAND tỉnh Nam Định",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM469: {
      name: "TAND tỉnh Phú Thọ",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM470: {
      name: "TAND tỉnh Bắc Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM471: {
      name: "TAND tỉnh Thái Bình",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM474: {
      name: "TAND tỉnh Hà Tĩnh",
	  description: "http://hatinh.toaan.gov.vn",
	  color: "default",
	  hover_color: "default",
	  url: "http://hatinh.toaan.gov.vn"
    },
    VNM475: {
		name: "TAND tỉnh TAND tỉnh Nghệ An",
		description: "Tổng số vụ án: 50<br>Thụ lý án: 20 <br> Giải quyết án: 30<br>http://nghean.toaan.gov.vn",
		color: "default",
	    hover_color: "default",
		url: "http://nghean.toaan.gov.vn"
    },
    VNM476: {
		name: "TAND tỉnh TAND tỉnh Quảng Bình",
		description: "Tổng số vụ án: 90<br>Thụ lý án: 70 <br> Giải quyết án: 20<br>http://quangbinh.toaan.gov.vn",
		color: "default",
	    hover_color: "default",
		url: "http://quangbinh.toaan.gov.vn"
    },
    VNM477: {
      name: "TAND tỉnh Đắk Lắk",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM478: {
      name: "TAND tỉnh Gia Lai",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM479: {
      name: "TAND tỉnh Khánh Hòa",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM480: {
      name: "TAND tỉnh Lâm Đồng",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM481: {
      name: "TAND tỉnh Ninh Thuận",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM482: {
      name: "TAND tỉnh Phú Yên",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM483: {
      name: "TAND tỉnh Bình Dương",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM4834: {
      name: "TAND tỉnh Tiền Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM4835: {
      name: "TAND tỉnh Đắk Nông",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM484: {
      name: "TAND tỉnh Bình Phước",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM485: {
      name: "TAND tỉnh Bình Định",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM486: {
      name: "TAND tỉnh Kon Tum",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM487: {
      name: "TAND tỉnh Quảng Nam",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM488: {
      name: "TAND tỉnh Quảng Ngãi",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM489: {
      name: "TAND tỉnh Quảng Trị",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM490: {
      name: "TAND tỉnh Thừa Thiên - Huế",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM491: {
      name: "TAND tỉnh Đà Nẵng",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM495: {
      name: "TAND tỉnh Bà Rịa - Vũng Tàu",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM496: {
      name: "TAND tỉnh Bình Thuận",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM497: {
      name: "TAND tỉnh Đồng Nai",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM498: {
      name: "TAND tỉnh An Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM499: {
      name: "TAND tỉnh Cần Thơ",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM500: {
      name: "TAND tỉnh Ðồng Tháp",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM501: {
      name: "TAND thành phố Hồ Chí Minh",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM502: {
      name: "TAND tỉnh Kiên Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM503: {
      name: "TAND tỉnh Long An",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM504: {
      name: "TAND tỉnh Bến Tre",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM505: {
      name: "TAND tỉnh Hậu Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM506: {
      name: "TAND tỉnh Bạc Liêu",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM507: {
      name: "TAND tỉnh Cà Mau",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM508: {
      name: "TAND tỉnh Sóc Trăng",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM509: {
      name: "TAND tỉnh Trà Vinh",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM510: {
      name: "TAND tỉnh Vĩnh Long",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM511: {
      name: "TAND tỉnh Cao Bằng",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM512: {
      name: "TAND tỉnh Hà Giang",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    },
    VNM5483: {
      name: "TAND tỉnh Lào Cai",
	  description: "default",
	  color: "default",
	  hover_color: "default",
	  url: "default"
    }
  },
  locations: {
    
  },
  labels: {
    
  }
};