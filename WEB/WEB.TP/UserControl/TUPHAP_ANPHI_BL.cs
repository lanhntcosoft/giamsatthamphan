﻿using Module.Common;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using BL.GSTP.BANGSETGET;

namespace WEB.TP.UserControl
{
    public class TUPHAP_ANPHI_BL
    {
        public byte[] File_Attach_Return(String _ID, ref String _FILE_NAME) //get data blob
        {
            OracleConnection conn = Cls_Comon.OpenConnection();
            OracleCommand comm = new OracleCommand("PKG_TUPHAP_ANPHI_DVCQG.GET_FILE_ATTACH", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            comm.Parameters["V_TP_THANH_TOAN_ID"].Value = _ID;
            comm.Parameters["V_FILE_NAME"].Direction = ParameterDirection.Output;
            comm.Parameters["ITEMS_CURSOR"].Direction = ParameterDirection.Output;
            try
            {
                return Cls_Comon.Get_Blob_File(comm, "FILE_ATTACH");//FILE_ATTACH: paramerter column of table attach 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _FILE_NAME = Convert.ToString(comm.Parameters["V_FILE_NAME"].Value);//get name file
                conn.Close();
            }
        }
    }
}