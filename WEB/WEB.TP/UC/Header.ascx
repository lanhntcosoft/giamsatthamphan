﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="WEB.TP.UC.Header" %>
<div class="header">
    <div class="headertop">
        <a href="http://www.toaan.gov.vn" target="_blank">
            <div class="logo_toaan"></div>
        </a>
        <a href="http://thads.moj.gov.vn" target="_blank">
            <div class="logo_botuphap"></div>
        </a>
        <div class="logo_text" style="text-align: center;">
            THÔNG TIN TẠM ỨNG ÁN PHÍ
        </div>
        <div class="taikhoan">
            <div class="userinfo">
                <div class="dropdown">
                    <a href="javascript:;" class="dropbtn userinfo_ico">
                        <asp:Literal ID="lstUserName" runat="server"></asp:Literal></a>

                    <div class="menu_child">
                        <div class="arrow_up_border"></div>
                        <div class="arrow_up"></div>
                        <ul>
                            <li class="singout">
                                <asp:LinkButton ID="lkSigout" runat="server"
                                    OnClick="lkSigout_Click" Text="Đăng xuất"></asp:LinkButton>
                            </li>
                            <li class="changepass infors"><a href="/User_Infor.aspx">Thông tin người dùng</a></li>
                            <li class="changepass"><a href="/ChangePass.aspx">Đổi mật khẩu</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="content_form">
            <div class="msg_thongbao">
                <asp:Literal ID="lbthongbao" runat="server"></asp:Literal>
            </div>
            <div style="display:none;">
                <asp:Repeater ID="rptMenu" runat="server" OnItemCommand="rptMenu_ItemCommand" OnItemDataBound="rpt_ItemDataBound">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtChuongTrinh"
                            runat="server" CssClass="menubutton" Text='<%#Eval("TENMENU") %>'
                            CommandName="SELECT" CommandArgument='<%#Eval("ID").ToString() +";"+ Eval("DUONGDAN").ToString()%>' />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <asp:Literal ID="ltt" runat="server"></asp:Literal>
        </div>
    </div>
</div>
