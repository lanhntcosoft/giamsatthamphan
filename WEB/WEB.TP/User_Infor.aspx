﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/AN_PHI.Master" CodeBehind="User_Infor.aspx.cs" Inherits="WEB.TP.User_Infor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="/UI/css/style.css" rel="stylesheet" />
    <link href="/UI/img/spcLogo.png" type="image/png" rel="shortcut icon" />
    <link href="/UI/css/chosen.css" rel="stylesheet" />


    <link href="/UI/css/jquery-ui.css" rel="stylesheet" />
    <script src="/UI/js/jquery-3.3.1.js"></script>
    <script src="/UI/js/jquery-ui.min.js"></script>


    <style type="text/css">
        body {
            margin: 0 auto;
            padding: 0;
            font-family: arial;
            font-size: 13px;
            background-color: #ffffff;
        }

        #login_frame {
            margin: 0px;
            width: 100%;
            height: 520px;
            background: url(/UI/img/bglauncher.png);
            background-size: 100% 100%;
            float: left;
            border-top: 1px solid #a91d1f;
        }

        #logo {
            width: 100%;
            height: 130px;
            margin-left: auto;
            margin-right: auto;
            background: url(UI/img/lgheader.png) no-repeat center;
        }

        #signin_frame {
            width: 685px;
            height: 420px;
            margin-top: 57px;
            background: none;
            background-color: #ffffff;
            margin-left: auto;
            margin-right: auto;
        }

        #titleds {
            float: left;
            width: 100%;
            text-align: center;
            margin-top: 0px;
            height: 36px;
            margin-bottom: 3px;
            line-height: 36px;
            color: #ffffff;
            font-size: 16px;
            font-weight: bold;
            background-color: #be1e1e;
        }

        .ctitem {
            float: left;
            width: 20%;
            height: 250px;
            padding-top: 35px;
        }

            .ctitem:hover {
                background: url(UI/img/bg_item.png);
            }

        .ctitemActive {
            float: left;
            width: 20%;
            height: 250px;
            background: url(UI/img/bg_item.png);
        }

        .khungnhap {
            width: 60%;
            margin-top: 13px;
            margin-left: 20%;
        }

            .khungnhap .box {
                display: block;
                float: left;
                margin-left: 12px;
                width: 90%;
                margin-bottom: 1px;
                border: none;
            }

            .khungnhap .textfield {
                display: block;
                height: 30px;
                width: 90%;
                margin: 8px 0;
                text-indent: 15px;
                border: solid 1px #8c0a0c;
                border-radius: 4px 4px 4px 4px;
            }

        .button {
            margin-top: 8px;
            background: none;
        }

        .menu {
            margin-bottom: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel" runat="server">
        <ContentTemplate>

            <div id="login_frame">

                <div id="signin_frame">
                    <div id="titleds">THÔNG TIN NGƯỜI DÙNG</div>
                    <div class="khungnhap">

                        <div class="box">
                            <i>Tên đăng nhập</i>
                            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="true" Enabled="false" paceholder="Mã truy cập" CssClass="textfield"></asp:TextBox>
                        </div>

                        <div class="box">
                            <i>Họ tên</i>
                            <asp:TextBox ID="txt_HOTEN" runat="server" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div class="box">
                            <i>Điện thoại</i>
                            <asp:TextBox ID="txt_DIENTHOAI" runat="server" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div class="box">
                            <i>Địa chỉ Email</i>
                            <asp:TextBox ID="txt_EMAIL" runat="server" CssClass="textfield"></asp:TextBox>
                        </div>
                         <div style="width:100%;float: left; margin-left: 13px; color: #da0421; font-weight: bold; margin-top: 4px;">
                            <asp:Literal ID="lttMsg" runat="server"></asp:Literal>
                        </div>

                        <div style="margin-left:10px;margin-top:10px;float:left;">
                            <asp:Button ID="cmdLogIn" CssClass="button_checkchkso" Text="Lưu thông tin" runat="server" OnClick="cmdSave_Click" />
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel">
        <ProgressTemplate>
            <div class="processmodal">
                <div class="processcenter">
                    <img src="UI/img/process.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
