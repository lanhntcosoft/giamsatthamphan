﻿using DAL.GSTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Module.Common;

namespace WEB.TP
{
    public partial class ChangePass : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string strUserID = Session[ENUM_SESSION.SESSION_USERID] + "";
                    if (strUserID == "") Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
                    txtUserName.Text = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                }
                catch (Exception ex)
              { lttMsg.Text = ex.Message; }
            }
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
        }
        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            if(txtPass_new.Text.Trim()=="")
            {
                lttMsg.Text = "Chưa nhập mật khẩu mới !";
                return;
            }
            if (txtPass_new.Text.Length<6)
            {
                lttMsg.Text = "Mật khẩu phải trên 6 ký tự !";
                return;
            }
            if (txtPass_new.Text!=txtRePass.Text)
            {
                lttMsg.Text = "Nhập lại mật khẩu không đúng !";
                return;
            }
            decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            TUPHAP_NGUOISUDUNG oT = dt.TUPHAP_NGUOISUDUNG.Where(x => x.ID == current_id).FirstOrDefault();
            oT.PASSWORD = Cls_Comon.MD5Encrypt(txtPass_new.Text);
            oT.NGAYSUA = DateTime.Now;
            oT.NGUOISUA = Session[ENUM_SESSION.SESSION_USERNAME] + "";
            dt.SaveChanges();
            lttMsg.Text = "Đổi mật khẩu thành công !";
        }
    }
}