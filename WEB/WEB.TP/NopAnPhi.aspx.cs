﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.TP.In;

namespace WEB.TP
{
    public partial class NopAnPhi : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        public decimal DonID = 0, DuongSuID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Decimal CurrUser = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
                if (CurrUser == 0)
                {
                    Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
                }
                hdd_case.Value = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
                bmID.Value = (String.IsNullOrEmpty(Request["bmID"] + "")) ? "" : Convert.ToString(Request["bmID"] + "");
                DonID = (String.IsNullOrEmpty(Request["donID"] + "")) ? 0 : Convert.ToDecimal(Request["donID"] + "");
                DuongSuID = (String.IsNullOrEmpty(Request["dsID"] + "")) ? 0 : Convert.ToDecimal(Request["dsID"] + "");
                if (DuongSuID > 0) loadedit(DonID,DuongSuID);
                hddid.Value = DonID.ToString();
                MenuPermission oPer = QT_TUPHAP_BL.GetMenuPer_THADS(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                Cls_Comon.SetButton(cmdSave, oPer.CAPNHAT);
            }
        }
        public void loadedit(decimal _DonID, decimal _DuongSuID)
        {
            String _style_panel = (String.IsNullOrEmpty(Request["style_panel"] + "")) ? "" : Convert.ToString(Request["style_panel"] + "");
            hdd_case.Value = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = oBL.Get_DONID_AnPhi(hdd_case.Value,Session[ENUM_SESSION.SESSION_USERNAME].ToString(),_DonID);
            //-----------
            if (_style_panel == "2")
            {
                div_NopAnPhi.Style.Add("Display", "none");
                div_ANPHIHOANTRA.Style.Remove("Display");
                must_input_NguoiNhan_Hoten.Style.Remove("Display");
                lbl_title_anphi.Text = "Thông tin hoàn trả án phí";
            }
            else
            {
                lbl_title_anphi.Text = "Thông tin nộp án phí";
                div_NopAnPhi.Style.Remove("Display");
                div_ANPHIHOANTRA.Style.Add("Display", "none");
                must_input_NguoiNhan_Hoten.Style.Add("Display", "none");
            }
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                //---------------------
                if (row["LOAIDUONGSU"].ToString() == "1")//ca nhan
                {
                    txtNguoiNop_CMND_must_input.Style.Remove("Display");
                    if (_style_panel == "2")
                    {
                        txtNguoiNhan_CMND_must_input.Style.Remove("Display");
                    }
                    else
                    {
                        txtNguoiNhan_CMND_must_input.Style.Add("Display", "none");
                    }
                    div_gioitinh_namsinh.Style.Remove("Display");
                    div_gioitinh_namsinh_nhan.Style.Remove("Display");
                    lbl_gioitinh.Visible = true;
                    lbl_gioitinh_nhan.Visible = true;
                  
                    txtNguoiNop_HoTen.Width = 219;
                    txtNguoiNhan_Hoten.Width = 219;
                    txtNguoiNhan_DiaChiChitiet.Width = 543;
                    txtNguoiNop_Diachi.Width = 543;
                    txtNguoiNhan_Email.Width = 543;
                }
                else if (row["LOAIDUONGSU"].ToString() != "1")//to chuc
                {
                    txtNguoiNop_CMND_must_input.Style.Add("Display", "none");
                    txtNguoiNhan_CMND_must_input.Style.Add("Display", "none");
                    div_gioitinh_namsinh.Style.Add("Display", "none");
                    div_gioitinh_namsinh_nhan.Style.Add("Display", "none");
                    lbl_gioitinh.Visible = false;
                    lbl_gioitinh_nhan.Visible = false;
                   
                    txtNguoiNop_HoTen.Width = 543;
                    txtNguoiNhan_Hoten.Width = 543;
                    txtNguoiNhan_DiaChiChitiet.Width = 543;
                    txtNguoiNop_Diachi.Width = 543;
                    txtNguoiNhan_Email.Width = 543;
                }
                if (row["SOBIENLAI"].ToString() == "")
                {
                    if (rdLoaiNguoiNop.SelectedValue == "0")
                    {
                        txtNguoiNop_CMND.Text = txtNguoiNop_HoTen.Text = "";
                        txtNguoiNop_NamSinh.Text = txtNguoiNop_Diachi.Text = "";
                        txtNguoiNop_DienThoai.Text = "";
                        txtNguoiNop_CMND.Enabled = txtNguoiNop_HoTen.Enabled = true;
                        txtNguoiNop_NamSinh.Enabled = txtNguoiNop_Diachi.Enabled = true;
                        txtNguoiNop_DienThoai.Enabled = true;
                        dropNguoiNop_GioiTinh.Enabled = true;
                    }
                    else if (rdLoaiNguoiNop.SelectedValue == "1")
                    {
                        txtNguoiNop_HoTen.Enabled = false;
                        txtNguoiNop_CMND.Enabled = true;
                        txtNguoiNop_NamSinh.Enabled = txtNguoiNop_Diachi.Enabled = false;
                        txtNguoiNop_DienThoai.Enabled = true;
                        dropNguoiNop_GioiTinh.Enabled = false;
                        txtNguoiNop_HoTen.Text = row["TENDUONGSU"].ToString();
                        txtNguoiNop_CMND.Text = row["SOCMND"].ToString();
                        txtNguoiNop_NamSinh.Text = row["NAMSINH"].ToString();
                        txtNguoiNop_Diachi.Text = row["DIACHI"].ToString();
                        txtNguoiNop_DienThoai.Text = row["DIENTHOAI"].ToString();
                        dropNguoiNop_GioiTinh.SelectedValue = row["GIOITINH"].ToString();
                        txtTamUngAP.Text = ((decimal)row["TAMUNGANPHI"]).ToString("#,0.###", cul);
                    }
                    if (rdLoaiNguoiNhan.SelectedValue == "0")
                    {
                        txtNguoiNhan_Hoten.Text = txtNguoiNhan_Namsinh.Text = "";
                        txtNguoiNhan_CMND.Text = txtNguoiNhan_CMND.Text = "";
                        txtNguoiNhan_DiaChiChitiet.Text = txtNguoiNhan_Dienthoai.Text = "";
                        txtNguoiNhan_Hoten.Enabled = true;
                        txtNguoiNhan_DiaChiChitiet.Enabled = true;
                    }
                    else if (rdLoaiNguoiNhan.SelectedValue == "1")
                    {
                        txtNguoiNhan_Hoten.Text = row["TENDUONGSU"].ToString();
                        txtNguoiNhan_CMND.Text = row["SOCMND"].ToString();
                        txtNguoiNhan_Namsinh.Text = row["NAMSINH"].ToString();
                        txtNguoiNhan_DiaChiChitiet.Text = row["DIACHI"].ToString();
                        txtNguoiNhan_Dienthoai.Text = row["DIENTHOAI"].ToString();
                        dropNguoiNhan_GioiTinh.SelectedValue = row["GIOITINH"].ToString();
                        txtTamUngAP.Text = ((decimal)row["TAMUNGANPHI"]).ToString("#,0.###", cul);
                        txtNguoiNhan_Hoten.Enabled = false;
                        txtNguoiNhan_DiaChiChitiet.Enabled = false;
                    }
                    txtSoBienLai.Enabled = true; txtNGAYBIENLAI.Enabled = true;
                    btnNBInBL.Visible = false;
                }
                else
                {
                    if (_style_panel == "2")
                    {
                        btnNBInBL.Visible = false;
                    }
                    else
                    {
                        btnNBInBL.Visible = true;
                    }
                    rdLoaiNguoiNop.SelectedValue = row["NOP_ISNGUYENDON"].ToString();
                    txtNguoiNop_HoTen.Text = row["NOP_HOTEN"].ToString();
                    dropNguoiNop_GioiTinh.SelectedValue = row["NOP_GIOITINH"].ToString();
                    txtNguoiNop_NamSinh.Text = row["NOP_NAMSINH"].ToString();
                    txtNguoiNop_CMND.Text = row["NOP_CMND"].ToString();
                    txtNguoiNop_DienThoai.Text = row["NOP_TEL"].ToString();
                    txtNguoiNop_Diachi.Text = row["NOP_DIACHI"].ToString();
                    //---------------
                    rdLoaiNguoiNhan.SelectedValue = row["HOANTRAAP_ISNGUYENDON"].ToString();
                    txtNguoiNhan_Hoten.Text = row["HOANTRAAP_HOTEN"].ToString();
                    dropNguoiNhan_GioiTinh.SelectedValue = row["HOANTRAAP_GIOITINH"].ToString();
                    txtNguoiNhan_Namsinh.Text = row["HOANTRAAP_NAMSINH"].ToString();
                    txtNguoiNhan_CMND.Text = row["HOANTRAAP_CMND"].ToString();
                    txtNguoiNhan_Dienthoai.Text = row["HOANTRAAP_TEL"].ToString();
                    txtNguoiNhan_Email.Text = row["HOANTRAAP_EMAIL"].ToString();
                    txtNguoiNhan_DiaChiChitiet.Text = row["HOANTRAAP_DIACHI"].ToString();
                    //----------------
                    txtSoBienLai.Text = row["SOBIENLAI"].ToString();
                    txtTamUngAP.Text = ((decimal)row["TAMUNGANPHI"]).ToString("#,0.###", cul);
                    txtNGAYBIENLAI.Text = row["NGAYBIENLAI"].ToString();
                    //--------------
                    if (row["NOP_ISNGUYENDON"].ToString() == "1")
                    {
                        txtNguoiNop_HoTen.Enabled = false;
                        txtNguoiNop_Diachi.Enabled = false;
                    }
                    else if (row["NOP_ISNGUYENDON"].ToString() == "0")
                    {
                        txtNguoiNop_HoTen.Enabled = true;
                        txtNguoiNop_Diachi.Enabled = true;
                    }
                    if (row["HOANTRAAP_ISNGUYENDON"].ToString() == "1")
                    {
                        txtNguoiNhan_Hoten.Enabled = false;
                        txtNguoiNhan_DiaChiChitiet.Enabled = false;
                    }
                    else if (row["HOANTRAAP_ISNGUYENDON"].ToString() == "0")
                    {
                        txtNguoiNhan_Hoten.Enabled = true;
                        txtNguoiNhan_DiaChiChitiet.Enabled = true;
                    }
                    txtNguoiNop_CMND.Enabled = true;
                    txtNguoiNop_NamSinh.Enabled = true;
                    txtNguoiNop_DienThoai.Enabled = true;
                    dropNguoiNop_GioiTinh.Enabled = true;
                    if (row["SOTHULY"].ToString() != "")
                    {
                        txtSoBienLai.Enabled = false; txtNGAYBIENLAI.Enabled = false;
                    }
                     if (row["ANPHIHOANTRA"].ToString() != "0")
                    {
                            txt_ANPHIHOANTRA.Text = ((decimal)row["ANPHIHOANTRA"]).ToString("#,0.###", cul);
                            txt_NGAYHOANTRA.Text = row["NGAYHOANTRA"].ToString();
                    }
                    else if (row["ANPHIHOANTRA"].ToString() == "0")
                    {
                        if (_style_panel == "2")
                        {
                            txt_ANPHIHOANTRA.Text = txtTamUngAP.Text;
                            txt_NGAYHOANTRA.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txt_ANPHIHOANTRA.Text = "0";
                            txt_NGAYHOANTRA.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                    }
                }
                txtNGUOITHUTIEN.Text = row["NGUOITHUTIEN"].ToString();
                txt_GHICHU_HOANTRA.Text= row["GHICHU_HOANTRA"].ToString();
            }
        }
        protected void btnNBInBL_Click(object sender, EventArgs e)
        {
            hdd_case.Value = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
            decimal _DonID = Convert.ToDecimal(hddid.Value);
            TUPHAP_ANPHI oT = dt.TUPHAP_ANPHI.Where(x => x.VUVIECID == _DonID).FirstOrDefault();
            if (oT != null)
            {
                TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
                DataTable tbl = new DataTable();
                DataRow row = tbl.NewRow();
                tbl = oBL.Get_DONID_AnPhi(hdd_case.Value, Session[ENUM_SESSION.SESSION_USERNAME].ToString(), _DonID);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    row = tbl.Rows[0];
                }
                //-------------
                TP_ANPHI objds = new TP_ANPHI();//data set
                TP_ANPHI.TUPHAP_ANPHIRow r = objds.TUPHAP_ANPHI.NewTUPHAP_ANPHIRow();
                r.NGAY_BIENLAI = row["NGAY_BIENLAI"].ToString();
                r.THANG_BIENLAI = row["THANG_BIENLAI"].ToString();
                r.NAM_BIENLAI = row["NAM_BIENLAI"].ToString();
                r.SOBIENLAI = row["SOBIENLAI"].ToString();
                r.NGUOINOPTIEN = row["TENDUONGSU"].ToString();
                r.DIACHI = row["NOP_DIACHI"].ToString();
                r.SO_THONGBAO = row["SOTHONGBAO"].ToString();
                r.NGAY_THONGBAO = row["NGAY_YEUCAU"].ToString();
                r.THANG_THONGBAO = row["THANG_YEUCAU"].ToString();
                r.NAM_THONG_BAO = row["NAM_YEUCAU"].ToString();
                r.DONVI_THA = Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                r.TEN_TOAAN = row["MA_TEN"].ToString();
                r.TAMUNG_ANPHI = row["TAMUNGANPHI_01"].ToString();
                r.TAMUNGBANGCHU = Cls_Comon.NumberToTextVN(Convert.ToDecimal(row["TAMUNGANPHI"].ToString()));
                r.NGUOI_THU_TIEN = row["NGUOITHUTIEN"].ToString();
                //-----------------------------
                objds.TUPHAP_ANPHI.AddTUPHAP_ANPHIRow(r);
                objds.AcceptChanges();
                //-----------------------
                Session["BIENLAI_DATASET"] = objds;
                Session["CHON_DATASET"] = "BIENLAI_DATASET";
                string StrMsg = "PopupReport('In/ViewReport.aspx','Biên lai án phí',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(Ajax_Manager_Updata, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Ajax_Manager_Updata, this.GetType(), "Alert", "alert(' Bạn phải lưu thông tin án phí trước.' )", true);
            }
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            String _style_panel = (String.IsNullOrEmpty(Request["style_panel"] + "")) ? "" : Convert.ToString(Request["style_panel"] + "");
            DuongSuID = (String.IsNullOrEmpty(Request["dsID"] + "")) ? 0 : Convert.ToDecimal(Request["dsID"] + "");
            if (CheckValid() == true)
            {
                try
                {
                    decimal DONID = Convert.ToDecimal(hddid.Value);
                    TUPHAP_ANPHI oT = dt.TUPHAP_ANPHI.Where(x => x.VUVIECID == DONID).FirstOrDefault();
                    if (oT == null)
                    {
                        oT = new TUPHAP_ANPHI();
                        oT.MALOAIVUVIEC = hdd_case.Value;
                        oT.VUVIECID = DONID;
                        oT.SOBIENLAI = txtSoBienLai.Text;
                        oT.ANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                        oT.VBTONGDATID = Convert.ToDecimal(bmID.Value);
                        oT.NGAYTAO = DateTime.Now;
                        oT.NGUOITAO = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                        oT.NGUOITAOID = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
                        DateTime _NGAYBIENLAI;
                        _NGAYBIENLAI = (String.IsNullOrEmpty(txtNGAYBIENLAI.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNGAYBIENLAI.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYBIENLAI = _NGAYBIENLAI;
                        oT.NGUOITHUTIEN = txtNGUOITHUTIEN.Text;
                        oT.GHICHU_HOANTRA = txt_GHICHU_HOANTRA.Text;
                        oT.DONVI_THUTIEN_ID= Convert.ToDecimal(Session[ENUM_SESSION.SESSION_DONVIID]);
                        //----------------
                        oT.NOP_ISNGUYENDON = Convert.ToDecimal(rdLoaiNguoiNop.SelectedValue);
                        oT.NOP_HOTEN = txtNguoiNop_HoTen.Text;
                        oT.NOP_GIOITINH = Convert.ToDecimal(dropNguoiNop_GioiTinh.SelectedValue);
                        oT.NOP_NAMSINH = txtNguoiNop_NamSinh.Text == "" ? 0 : Convert.ToDecimal(txtNguoiNop_NamSinh.Text);
                        oT.NOP_CMND = txtNguoiNop_CMND.Text;
                        oT.NOP_TEL = txtNguoiNop_DienThoai.Text;
                        oT.NOP_EMAIL = txtNguoiNhan_Email.Text;
                        oT.NOP_DIACHI = txtNguoiNop_Diachi.Text;
                        //-----------------------
                        oT.HOANTRAAP_ISNGUYENDON=Convert.ToDecimal(rdLoaiNguoiNhan.SelectedValue);                      
                        oT.HOANTRAAP_HOTEN=txtNguoiNhan_Hoten.Text;
                        oT.HOANTRAAP_GIOITINH = Convert.ToDecimal(dropNguoiNhan_GioiTinh.SelectedValue);
                        oT.HOANTRAAP_NAMSINH = txtNguoiNhan_Namsinh.Text == "" ? 0 : Convert.ToDecimal(txtNguoiNhan_Namsinh.Text);
                        oT.HOANTRAAP_CMND = txtNguoiNhan_CMND.Text;
                        oT.HOANTRAAP_TEL = txtNguoiNhan_Dienthoai.Text;
                        oT.HOANTRAAP_EMAIL = txtNguoiNhan_Email.Text;
                        oT.HOANTRAAP_DIACHI = txtNguoiNhan_DiaChiChitiet.Text;
                        oT.ANPHIHOANTRA = (String.IsNullOrEmpty(txt_ANPHIHOANTRA.Text.Trim())) ? 0 : Convert.ToDecimal(txt_ANPHIHOANTRA.Text.Trim().Replace(".", ""));
                        DateTime _NGAYHOANTRA;
                        _NGAYHOANTRA = (String.IsNullOrEmpty(txt_NGAYHOANTRA.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txt_NGAYHOANTRA.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYHOANTRA = _NGAYHOANTRA;
                        //-----------------
                        dt.TUPHAP_ANPHI.Add(oT);
                        dt.SaveChanges();
                        //----------------
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_DANSU)
                        {
                            ADS_ANPHI oAP = dt.ADS_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            ADS_DON_DUONGSU oDs = dt.ADS_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND= txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH)
                        {
                            AHN_ANPHI oAP = dt.AHN_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AHN_DON_DUONGSU oDs = dt.AHN_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI)
                        {
                            AKT_ANPHI oAP = dt.AKT_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AKT_DON_DUONGSU oDs = dt.AKT_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_LAODONG)
                        {
                            ALD_ANPHI oAP = dt.ALD_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            ALD_DON_DUONGSU oDs = dt.ALD_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_HANHCHINH)
                        {
                            AHC_ANPHI oAP = dt.AHC_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AHC_DON_DUONGSU oDs = dt.AHC_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_PHASAN)
                        {
                            APS_ANPHI oAP = dt.APS_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            APS_DON_DUONGSU oDs = dt.APS_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        //----------------
                        lstMsgB.Text = "Bạn đã lưu thành công";
                    }
                    else
                    {
                        oT.NOP_ISNGUYENDON = Convert.ToDecimal(rdLoaiNguoiNop.SelectedValue);
                        oT.NOP_HOTEN = txtNguoiNop_HoTen.Text;
                        oT.NOP_GIOITINH = Convert.ToDecimal(dropNguoiNop_GioiTinh.SelectedValue);
                        oT.NOP_NAMSINH = txtNguoiNop_NamSinh.Text == "" ? 0 : Convert.ToDecimal(txtNguoiNop_NamSinh.Text);
                        oT.NOP_CMND = txtNguoiNop_CMND.Text;
                        oT.NOP_TEL = txtNguoiNop_DienThoai.Text;
                        oT.NOP_EMAIL = txtNguoiNhan_Email.Text;
                        oT.NOP_DIACHI = txtNguoiNop_Diachi.Text;
                        //---------------
                        oT.HOANTRAAP_ISNGUYENDON = Convert.ToDecimal(rdLoaiNguoiNhan.SelectedValue);
                        oT.HOANTRAAP_HOTEN = txtNguoiNhan_Hoten.Text;
                        oT.HOANTRAAP_GIOITINH = Convert.ToDecimal(dropNguoiNhan_GioiTinh.SelectedValue);
                        oT.HOANTRAAP_NAMSINH = txtNguoiNhan_Namsinh.Text == "" ? 0 : Convert.ToDecimal(txtNguoiNhan_Namsinh.Text);
                        oT.HOANTRAAP_CMND = txtNguoiNhan_CMND.Text;
                        oT.HOANTRAAP_TEL = txtNguoiNhan_Dienthoai.Text;
                        oT.HOANTRAAP_EMAIL = txtNguoiNhan_Email.Text;
                        oT.HOANTRAAP_DIACHI = txtNguoiNhan_DiaChiChitiet.Text;
                        //----------------
                        oT.SOBIENLAI = txtSoBienLai.Text;
                        oT.ANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".",""));
                        DateTime _NGAYBIENLAI;
                        _NGAYBIENLAI = (String.IsNullOrEmpty(txtNGAYBIENLAI.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txtNGAYBIENLAI.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYBIENLAI = _NGAYBIENLAI;
                        oT.NGUOITHUTIEN = txtNGUOITHUTIEN.Text;
                        oT.GHICHU_HOANTRA = txt_GHICHU_HOANTRA.Text;
                        oT.ANPHIHOANTRA = (String.IsNullOrEmpty(txt_ANPHIHOANTRA.Text.Trim())) ? 0 : Convert.ToDecimal(txt_ANPHIHOANTRA.Text.Trim().Replace(".", ""));
                        DateTime _NGAYHOANTRA;
                        _NGAYHOANTRA = (String.IsNullOrEmpty(txt_NGAYHOANTRA.Text.Trim())) ? DateTime.MinValue : DateTime.Parse(this.txt_NGAYHOANTRA.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        oT.NGAYHOANTRA = _NGAYHOANTRA;
                        dt.SaveChanges();
                        //----------------
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_DANSU)
                        {
                            ADS_ANPHI oAP = dt.ADS_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            ADS_DON_DUONGSU oDs = dt.ADS_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_HONNHAN_GIADINH)
                        {
                            AHN_ANPHI oAP = dt.AHN_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AHN_DON_DUONGSU oDs = dt.AHN_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_KINHDOANH_THUONGMAI)
                        {
                            AKT_ANPHI oAP = dt.AKT_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AKT_DON_DUONGSU oDs = dt.AKT_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_LAODONG)
                        {
                            ALD_ANPHI oAP = dt.ALD_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            ALD_DON_DUONGSU oDs = dt.ALD_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_HANHCHINH)
                        {
                            AHC_ANPHI oAP = dt.AHC_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            AHC_DON_DUONGSU oDs = dt.AHC_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        if (hdd_case.Value == ENUM_LOAIVUVIEC.AN_PHASAN)
                        {
                            APS_ANPHI oAP = dt.APS_ANPHI.Where(x => x.DONID == DONID).FirstOrDefault();
                            oAP.SOBIENLAI = txtSoBienLai.Text;
                            oAP.TAMUNGANPHI = (String.IsNullOrEmpty(txtTamUngAP.Text.Trim())) ? 0 : Convert.ToDecimal(txtTamUngAP.Text.Trim().Replace(".", ""));
                            oAP.NGAYNOPANPHI = _NGAYBIENLAI;
                            oAP.NGAYNOPBIENLAI = _NGAYBIENLAI;
                            dt.SaveChanges();
                            APS_DON_DUONGSU oDs = dt.APS_DON_DUONGSU.Where(x => x.ID == DuongSuID).FirstOrDefault();
                            oDs.SOCMND = txtNguoiNop_CMND.Text;
                            dt.SaveChanges();
                        }
                        //----------------
                        lstMsgB.Text = "Bạn đã cập nhật thành công";
                      
                    }
                    if (_style_panel == "2")
                    {
                        btnNBInBL.Visible = false;
                    }
                    else
                    {
                        btnNBInBL.Visible = true;
                    }
                    Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
                }
                catch (Exception ex)
                {
                    lstMsgB.Text = "Lỗi: " + ex.Message;
                }
            }

        }
        private bool CheckValid()
        {
            String _style_panel = (String.IsNullOrEmpty(Request["style_panel"] + "")) ? "" : Convert.ToString(Request["style_panel"] + "");
            String styleCase = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
            DonID = (String.IsNullOrEmpty(Request["donID"] + "")) ? 0 : Convert.ToDecimal(Request["donID"] + "");
            DuongSuID = (String.IsNullOrEmpty(Request["dsID"] + "")) ? 0 : Convert.ToDecimal(Request["dsID"] + "");
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = oBL.Get_DONID_AnPhi(styleCase, Session[ENUM_SESSION.SESSION_USERNAME].ToString(), DonID);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                if (_style_panel == "2")
                {
                    if (txt_ANPHIHOANTRA.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập số tiền hoàn trả";
                        txt_ANPHIHOANTRA.Focus();
                        return false;
                    }
                    if (txt_NGAYHOANTRA.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập ngày nhận hoàn trả";
                        txt_NGAYHOANTRA.Focus();
                        return false;
                    }
                    if (txtNguoiNhan_Hoten.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập họ tên người nhận hoàn trả tiền tạm ứng án phí";
                        txtNguoiNhan_Hoten.Focus();
                        return false;
                    }
                    if (row["LOAIDUONGSU"].ToString() == "1")
                    {
                        if (txtNguoiNhan_CMND.Text == "")
                        {
                            lstMsgB.Text = "Bạn chưa nhập số CMTND của người nhận hoàn trả tiền tạm ứng án phí";
                            txtNguoiNhan_CMND.Focus();
                            return false;
                        }
                    }
                }
                else
                {
                    if (txtNguoiNop_HoTen.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập họ tên người nộp án phí";
                        txtNguoiNop_HoTen.Focus();
                        return false;
                    }
                    if (row["LOAIDUONGSU"].ToString() == "1")
                    {
                        if (txtNguoiNop_CMND.Text == "")
                        {
                            lstMsgB.Text = "Bạn chưa nhập số chứng minh thư nhân dân của người nộp";
                            txtNguoiNop_CMND.Focus();
                            return false;
                        }
                    }

                    if (txtSoBienLai.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập số biên lai";
                        txtSoBienLai.Focus();
                        return false;
                    }
                    if (txtNGAYBIENLAI.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập ngày biên lai";
                        txtNGAYBIENLAI.Focus();
                        return false;
                    }
                    if (txtTamUngAP.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa nhập Tạm ứng án phí";
                        txtTamUngAP.Focus();
                        return false;
                    }
                    if (txtNGUOITHUTIEN.Text == "")
                    {
                        lstMsgB.Text = "Bạn chưa chọn người thu tiền";
                        txtNGUOITHUTIEN.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        protected void rdLoaiNguoiNop_SelectedIndexChanged(object sender, EventArgs e)
        {
            String styleCase = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
            DonID = (String.IsNullOrEmpty(Request["donID"] + "")) ? 0 : Convert.ToDecimal(Request["donID"] + "");
            DuongSuID = (String.IsNullOrEmpty(Request["dsID"] + "")) ? 0 : Convert.ToDecimal(Request["dsID"] + "");
            TUPHAP_ANPHI oT = dt.TUPHAP_ANPHI.Where(x => x.VUVIECID == DonID).FirstOrDefault();
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = oBL.Get_DONID_AnPhi(styleCase, Session[ENUM_SESSION.SESSION_USERNAME].ToString(), DonID);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            //--------------
            if (rdLoaiNguoiNop.SelectedValue == "0")
            {
                txtNguoiNop_CMND.Text = txtNguoiNop_HoTen.Text = "";
                txtNguoiNop_NamSinh.Text = txtNguoiNop_Diachi.Text = "";
                txtNguoiNop_DienThoai.Text = "";
                if (row["NOP_ISNGUYENDON"].ToString() == "0")
                {
                    txtNguoiNop_HoTen.Text = oT.NOP_HOTEN;
                    dropNguoiNop_GioiTinh.SelectedValue = Convert.ToString(oT.NOP_GIOITINH);
                    txtNguoiNop_NamSinh.Text = Convert.ToString(oT.NOP_NAMSINH);
                    txtNguoiNop_CMND.Text = oT.NOP_CMND;
                    txtNguoiNop_DienThoai.Text = oT.NOP_TEL;
                    txtNguoiNhan_Email.Text = oT.NOP_EMAIL;
                    txtNguoiNop_Diachi.Text = oT.NOP_DIACHI;
                }
                txtNguoiNop_HoTen.Enabled = true;
                txtNguoiNop_Diachi.Enabled = true;
                txtNguoiNop_CMND.Enabled = true;
                txtNguoiNop_NamSinh.Enabled = true;
                txtNguoiNop_DienThoai.Enabled = true;
                dropNguoiNop_GioiTinh.Enabled = true;
            }
            else
            {
                txtNguoiNop_HoTen.Enabled = false;
                txtNguoiNop_CMND.Enabled = true;
                txtNguoiNop_Diachi.Enabled = false;
                txtNguoiNop_NamSinh.Enabled = true;
                txtNguoiNop_DienThoai.Enabled = true;
                dropNguoiNop_GioiTinh.Enabled = true;
                txtNguoiNop_HoTen.Text = row["TENDUONGSU"].ToString();
                txtNguoiNop_CMND.Text = row["SOCMND"].ToString();
                txtNguoiNop_NamSinh.Text = row["NAMSINH"].ToString();
                txtNguoiNop_Diachi.Text = row["DIACHI"].ToString();
                txtNguoiNop_DienThoai.Text = row["DIENTHOAI"].ToString();
                dropNguoiNop_GioiTinh.SelectedValue = row["GIOITINH"].ToString();

            }
        }
        protected void rdLoaiNguoiNhan_SelectedIndexChanged(object sender, EventArgs e)
        {
            String styleCase = (String.IsNullOrEmpty(Request.QueryString["styleCase"] + "")) ? "" : Convert.ToString(Request.QueryString["styleCase"] + "");
            DonID = (String.IsNullOrEmpty(Request["donID"] + "")) ? 0 : Convert.ToDecimal(Request["donID"] + "");
            DuongSuID = (String.IsNullOrEmpty(Request["dsID"] + "")) ? 0 : Convert.ToDecimal(Request["dsID"] + "");
            TUPHAP_ANPHI oT = dt.TUPHAP_ANPHI.Where(x => x.VUVIECID == DonID).FirstOrDefault();
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = oBL.Get_DONID_AnPhi(styleCase, Session[ENUM_SESSION.SESSION_USERNAME].ToString(), DonID);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            //--------------
            if (rdLoaiNguoiNhan.SelectedValue == "0")
            {
                txtNguoiNhan_Hoten.Text = "";
                txtNguoiNhan_CMND.Text = "";
                txtNguoiNhan_Namsinh.Text = "";
                txtNguoiNhan_DiaChiChitiet.Text = "";
                txtNguoiNhan_Dienthoai.Text = "";
                txtNguoiNhan_Email.Text = "";
                txt_GHICHU_HOANTRA.Text = "";
                if (row["HOANTRAAP_ISNGUYENDON"].ToString() == "0")
                {
                    txtNguoiNhan_Hoten.Text = oT.HOANTRAAP_HOTEN.ToString();
                    dropNguoiNhan_GioiTinh.SelectedValue = oT.HOANTRAAP_GIOITINH.ToString();
                    txtNguoiNhan_Namsinh.Text = oT.HOANTRAAP_NAMSINH.ToString();
                    txtNguoiNhan_CMND.Text = oT.HOANTRAAP_CMND;
                    txtNguoiNhan_Dienthoai.Text = oT.HOANTRAAP_TEL;
                    txtNguoiNhan_Email.Text = oT.HOANTRAAP_EMAIL;
                    txtNguoiNhan_DiaChiChitiet.Text = oT.HOANTRAAP_DIACHI;
                    txt_GHICHU_HOANTRA.Text = oT.GHICHU_HOANTRA;
                }
                txtNguoiNhan_Hoten.Enabled = true;
                txtNguoiNhan_DiaChiChitiet.Enabled = true;
            }
            else if (rdLoaiNguoiNhan.SelectedValue == "1")
            {
                
                txtNguoiNhan_Hoten.Text = row["TENDUONGSU"].ToString();
                txtNguoiNhan_Hoten.Text = row["TENDUONGSU"].ToString();
                txtNguoiNhan_CMND.Text = row["SOCMND"].ToString();
                txtNguoiNhan_Namsinh.Text = row["NAMSINH"].ToString();
                txtNguoiNhan_DiaChiChitiet.Text = row["DIACHI"].ToString();
                txtNguoiNhan_Dienthoai.Text = row["DIENTHOAI"].ToString();
                txtNguoiNhan_Email.Text = row["HOANTRAAP_EMAIL"].ToString();
                dropNguoiNhan_GioiTinh.SelectedValue = row["GIOITINH"].ToString();
                txtTamUngAP.Text = ((decimal)row["TAMUNGANPHI"]).ToString("#,0.###", cul);
                txtNguoiNhan_Hoten.Enabled = false;
                txtNguoiNhan_DiaChiChitiet.Enabled = false;
                txt_GHICHU_HOANTRA.Text = row["GHICHU_HOANTRA"].ToString();
            }
            
        }
    }
}