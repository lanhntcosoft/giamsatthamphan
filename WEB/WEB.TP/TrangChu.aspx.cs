﻿using BL.GSTP;
using BL.GSTP.ADS;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using WEB.TP.In;
using WEB.TP.UserControl;
using System.Text;
namespace WEB.TP
{
    public partial class TrangChu : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            Decimal CurrUser = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUser == 0)
            {
                Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
            }
            if (!IsPostBack)
            {
                LoadData_ThongBao();
                LoadData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "load_file_attact", "load_file_attact();", true);
            }
        }
        protected void Xem_bien_lai(Decimal _DonID,String _styleCase)
        {
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            tbl = oBL.Get_DONID_AnPhi(_styleCase,Session[ENUM_SESSION.SESSION_USERNAME].ToString(), _DonID);
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                //-------------
                TP_ANPHI objds = new TP_ANPHI();//data set
                TP_ANPHI.TUPHAP_ANPHIRow r = objds.TUPHAP_ANPHI.NewTUPHAP_ANPHIRow();
                r.NGAY_BIENLAI = row["NGAY_BIENLAI"].ToString();
                r.THANG_BIENLAI = row["THANG_BIENLAI"].ToString();
                r.NAM_BIENLAI = row["NAM_BIENLAI"].ToString();
                r.SOBIENLAI = row["SOBIENLAI"].ToString();
                r.NGUOINOPTIEN = row["TENDUONGSU"].ToString();
                r.DIACHI = row["NOP_DIACHI"].ToString();
                r.SO_THONGBAO = row["SOTHONGBAO"].ToString();
                r.NGAY_THONGBAO = row["NGAY_YEUCAU"].ToString();
                r.THANG_THONGBAO = row["THANG_YEUCAU"].ToString();
                r.NAM_THONG_BAO = row["NAM_YEUCAU"].ToString();
                r.DONVI_THA = row["DONVI_THUTIEN_TEN"].ToString(); //Session[ENUM_SESSION.SESSION_TENDONVI] + "";
                r.TEN_TOAAN = row["MA_TEN"].ToString();
                r.TAMUNG_ANPHI = row["TAMUNGANPHI_01"].ToString();
                r.TAMUNGBANGCHU = Cls_Comon.NumberToTextVN(Convert.ToDecimal(row["TAMUNGANPHI"].ToString()));
                r.NGUOI_THU_TIEN = row["NGUOITHUTIEN"].ToString();
                //-----------------------------
                objds.TUPHAP_ANPHI.AddTUPHAP_ANPHIRow(r);
                objds.AcceptChanges();
                //-----------------------
                Session["BIENLAI_DATASET"] = objds;
                Session["CHON_DATASET"] = "BIENLAI_DATASET";
                string StrMsg = "PopupReport('In/ViewReport.aspx','Biên lai án phí',800,800);";
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), StrMsg, true);
            }
        }
        protected void cmdFile_Attach_Click(object sender, ImageClickEventArgs e)
        {
            TUPHAP_ANPHI_BL M_Object = new TUPHAP_ANPHI_BL();
            ImageButton img = (ImageButton)sender;
            String _Id = img.CommandArgument;
            String _FILE_NAME = "";
            byte[] b = M_Object.File_Attach_Return(_Id, ref _FILE_NAME);
            string fileEx = "";
            if (_FILE_NAME.LastIndexOf('.') > 0)
            {
                fileEx = _FILE_NAME.Substring(_FILE_NAME.LastIndexOf('.'));
            }
            Response.Clear();
            Response.ContentEncoding = Encoding.Unicode;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + _FILE_NAME);
            switch (fileEx)
            {
                case ".pdf": Response.ContentType = "application/pdf"; break;
                case ".dwg": Response.ContentType = "image/vnd.dwg"; break;
                case ".doc": Response.ContentType = "application/msword"; break;
                case ".docx": Response.ContentType = "application/vnd.openxmlformats-officeFileAttach.wordprocessingml.FileAttach"; break;
                case ".xls": Response.ContentType = "application/vnd.ms-excel"; break;
                case ".xlsx": Response.ContentType = "application/vnd.openxmlformats-officeFileAttach.spreadsheetml.sheet"; break;
                case ".gif": Response.ContentType = "image/gif"; break;
                case ".jpeg": Response.ContentType = "image/jpg"; break;
                case ".jpg": Response.ContentType = "image/jpg"; break;
                case ".png": Response.ContentType = "image/png"; break;
                default: Response.ContentType = "application/octet-stream"; break; //getMimeType(sExtention, oConnection);  
            }
            Response.BinaryWrite(b);
            Response.Flush();
            Response.End();
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                String ND_id = e.CommandArgument.ToString();
                String[] ND_id_arr = ND_id.Split(';');
                switch (e.CommandName)
                {
                    case "Dowload":
                        DowloadFile(Convert.ToDecimal(ND_id_arr[0]), ND_id_arr[1]);
                        break;
                    case "xembienlai":
                        Xem_bien_lai(Convert.ToDecimal(ND_id_arr[0]), ND_id_arr[1]);
                        break;
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dv = (DataRowView)e.Item.DataItem;
                ImageButton cmdDowload = (ImageButton)e.Item.FindControl("cmdDowload");
                String tenfile = string.IsNullOrEmpty(dv["TENFILE"] + "") ? "" : dv["TENFILE"].ToString();
                HtmlTableCell td_FILEID = (HtmlTableCell)e.Item.FindControl("td_FILEID");
                HtmlTableCell td_item_hoantra = (HtmlTableCell)e.Item.FindControl("td_item_hoantra");
                HtmlTableCell td_item_toaan = (HtmlTableCell)e.Item.FindControl("td_item_toaan");
                HtmlTableCell td_item_trangthai = (HtmlTableCell)e.Item.FindControl("td_item_trangthai");
                HtmlTableCell td_item_xacnhan = (HtmlTableCell)e.Item.FindControl("td_item_xacnhan");
                HtmlTableCell td_item_xacnhan_tt = (HtmlTableCell)e.Item.FindControl("td_item_xacnhan_tt");
                HtmlTableCell td_item_xacnhanhoantra = (HtmlTableCell)e.Item.FindControl("td_item_xacnhanhoantra");
                
                HtmlTableCell td_item_thaotac = (HtmlTableCell)e.Item.FindControl("td_item_thaotac");
                HtmlTableCell td_item_xembienlai = (HtmlTableCell)e.Item.FindControl("td_item_xembienlai");
                HtmlTableCell td_item_xemhoantra = (HtmlTableCell)e.Item.FindControl("td_item_xemhoantra");
                HtmlTableCell td_item_TAMUNGANPHI = (HtmlTableCell)e.Item.FindControl("td_item_TAMUNGANPHI");
                HtmlTableCell td_item_ANPHIHOANTRA = (HtmlTableCell)e.Item.FindControl("td_item_ANPHIHOANTRA");
                
                Label lbl_hoantra=(Label)e.Item.FindControl("lbl_hoantra");
                if(dv["ANPHIHOANTRA"].ToString()=="0")
                {
                    lbl_hoantra.Text = "Cập nhật thông tin hoàn trả án phí";
                }
                else
                {
                    lbl_hoantra.Text = "Sửa thông tin hoàn trả án phí";
                }

                if (tenfile.Length > 0)
                    cmdDowload.Visible = true;
                else
                    cmdDowload.Visible = false;
                if (rdTrangThai.SelectedValue == "0")//chua nop an phi
                {
                    td_FILEID.Visible = true;
                    td_item_hoantra.Visible = false;
                    td_item_toaan.Visible = true;
                    td_item_trangthai.Visible = true;
                    td_item_xacnhan.Visible = false;
                    td_item_xacnhan_tt.Visible = false;
                    td_item_xacnhanhoantra.Visible = false;
                    td_item_thaotac.Visible = true;
                    td_item_xembienlai.Visible = false;
                    td_item_xemhoantra.Visible = false;
                    td_item_TAMUNGANPHI.Visible = true;
                    td_item_ANPHIHOANTRA.Visible = false;
                }
                else if (rdTrangThai.SelectedValue == "1")//đã nộp án phí
                {
                    td_FILEID.Visible = false;
                    td_item_hoantra.Visible = false;
                    td_item_toaan.Visible = false;
                    td_item_trangthai.Visible = true;
                    if (dv["TT_TRUCTUYEN"].ToString() == "0")
                    {
                        td_item_xacnhan.Visible = true;
                        td_item_xacnhan_tt.Visible = false;
                    }
                    else if (dv["TT_TRUCTUYEN"].ToString() == "1")
                    {
                        td_item_xacnhan.Visible = false;
                        td_item_xacnhan_tt.Visible = true;
                    }
                    td_item_xacnhanhoantra.Visible = false;
                    td_item_thaotac.Visible = false;
                    td_item_xembienlai.Visible = true;
                    td_item_xemhoantra.Visible = false;
                    td_item_TAMUNGANPHI.Visible = true;
                    td_item_ANPHIHOANTRA.Visible = false;
                }
                else if (rdTrangThai.SelectedValue == "2")//đã hoàn trả án phí
                {
                    td_FILEID.Visible = false;
                    td_item_hoantra.Visible = true;
                    td_item_toaan.Visible = false;
                    td_item_trangthai.Visible = false;
                    td_item_xacnhan.Visible = false;
                    td_item_xacnhan_tt.Visible = false;
                    td_item_xacnhanhoantra.Visible = true;
                    td_item_thaotac.Visible = false;
                    td_item_xembienlai.Visible = false;
                    td_item_xemhoantra.Visible = true;
                    td_item_TAMUNGANPHI.Visible = false;
                    td_item_ANPHIHOANTRA.Visible = true;
                }
                else if (rdTrangThai.SelectedValue == "3")//đình chỉ nộp an phi
                {
                    td_FILEID.Visible = true;
                    td_item_hoantra.Visible = false;
                    td_item_toaan.Visible = true;
                    td_item_trangthai.Visible = true;
                    td_item_xacnhan.Visible = false;
                    td_item_xacnhan_tt.Visible = false;
                    td_item_xacnhanhoantra.Visible = false;
                    td_item_thaotac.Visible = false;
                    td_item_xembienlai.Visible = false;
                    td_item_xemhoantra.Visible = false;
                    td_item_TAMUNGANPHI.Visible = true;
                    td_item_ANPHIHOANTRA.Visible = false;
                }
                else if (rdTrangThai.SelectedValue == "")//Tất cả
                {
                    if (dv["SOBIENLAI"] + "" == "")
                    {
                        td_item_xembienlai.Visible = false;
                        td_item_thaotac.Visible = true;
                    }
                    else
                    {
                        td_item_xembienlai.Visible = true;
                        td_item_thaotac.Visible = false;
                    }
                    td_item_hoantra.Visible = false;
                    td_item_xacnhan.Visible = false;
                    td_item_xacnhan_tt.Visible = false;
                    td_item_xemhoantra.Visible = false;
                    td_item_TAMUNGANPHI.Visible = true;
                    td_item_ANPHIHOANTRA.Visible = false;
                    td_item_xacnhanhoantra.Visible = false;
                }
            }
            if (e.Item.ItemType == ListItemType.Header)
            {
                HtmlTableCell td_header_FILEID = (HtmlTableCell)e.Item.FindControl("td_header_FILEID");
                HtmlTableCell td_header_hoantra = (HtmlTableCell)e.Item.FindControl("td_header_hoantra");
                HtmlTableCell td_header_toaan = (HtmlTableCell)e.Item.FindControl("td_header_toaan");
                HtmlTableCell td_header_trangthai = (HtmlTableCell)e.Item.FindControl("td_header_trangthai");
                HtmlTableCell td_header_xacnhan = (HtmlTableCell)e.Item.FindControl("td_header_xacnhan");
                HtmlTableCell td_header_xacnhanhoantra = (HtmlTableCell)e.Item.FindControl("td_header_xacnhanhoantra");
                HtmlTableCell td_header_TAMUNGANPHI = (HtmlTableCell)e.Item.FindControl("td_header_TAMUNGANPHI");
                HtmlTableCell td_header_ANPHIHOANTRA = (HtmlTableCell)e.Item.FindControl("td_header_ANPHIHOANTRA");
                HtmlTableCell td_header_thaotac = (HtmlTableCell)e.Item.FindControl("td_header_thaotac");

                if (rdTrangThai.SelectedValue == "0")
                {
                    td_header_FILEID.Visible = true;
                    td_header_hoantra.Visible = false;
                    td_header_toaan.Visible = true;
                    td_header_trangthai.Visible = true;
                    td_header_xacnhan.Visible = false;
                    td_header_TAMUNGANPHI.Visible = true;
                    td_header_ANPHIHOANTRA.Visible = false;
                    td_header_xacnhanhoantra.Visible = false;
                    td_header_thaotac.Visible = true;
                }
                else if (rdTrangThai.SelectedValue == "1")//đã nộp án phí
                {
                    td_header_xacnhan.Visible = true;
                    td_header_hoantra.Visible = false;
                    td_header_FILEID.Visible = false;
                    td_header_toaan.Visible = false;
                    td_header_trangthai.Visible = true;
                    td_header_TAMUNGANPHI.Visible = true;
                    td_header_ANPHIHOANTRA.Visible = false;
                    td_header_xacnhanhoantra.Visible = false;
                    td_header_thaotac.Visible = true;
                }
                else if (rdTrangThai.SelectedValue == "2")//đã hoàn trả án phí
                {
                    td_header_xacnhan.Visible = false;
                    td_header_xacnhanhoantra.Visible = true;
                    td_header_hoantra.Visible = true;
                    td_header_FILEID.Visible = false;
                    td_header_toaan.Visible = false;
                    td_header_trangthai.Visible = false;
                    td_header_TAMUNGANPHI.Visible = false;
                    td_header_ANPHIHOANTRA.Visible = true;
                    td_header_thaotac.Visible = true;
                }
                else if (rdTrangThai.SelectedValue == "3")
                {
                    td_header_FILEID.Visible = true;
                    td_header_hoantra.Visible = false;
                    td_header_toaan.Visible = true;
                    td_header_trangthai.Visible = true;
                    td_header_xacnhan.Visible = false;
                    td_header_TAMUNGANPHI.Visible = true;
                    td_header_ANPHIHOANTRA.Visible = false;
                    td_header_xacnhanhoantra.Visible = false;
                    td_header_thaotac.Visible = false;
                }
                else if (rdTrangThai.SelectedValue == "")//Tất cả
                {
                    td_header_xacnhan.Visible = false;
                    td_header_hoantra.Visible = false;
                    td_header_TAMUNGANPHI.Visible = true;
                    td_header_ANPHIHOANTRA.Visible = false;
                    td_header_xacnhanhoantra.Visible = false;
                    td_header_thaotac.Visible = true;
                }
            }
        }
        void LoadData()
        {
            String THAid = "";
            if (Session["LOAITOA"].ToString() != "TOICAO")
            {
                THAid = Session[ENUM_SESSION.SESSION_DONVIID].ToString(); 
            }
            int page_size = Convert.ToInt32(dropPageSize.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable oDT = oBL.GetAll_AnPhi_Search(null,null, rdTrangThai.SelectedValue, Session[ENUM_SESSION.SESSION_USERNAME].ToString(), THAid, txtTuKhoaBasic.Text.Trim(),pageindex, page_size);
            int count_all = 0;
            if (oDT.Rows.Count > 0)
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
            if (oDT != null && count_all > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon.GetTotalPage(count_all, Convert.ToInt32(dropPageSize.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }
            rpt.DataSource = oDT;
            rpt.DataBind();
        }
        protected void rdTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        void LoadData_ThongBao()
        {
            String THAid = "";
            if (Session["LOAITOA"].ToString() != "TOICAO")
            {
                THAid = Session[ENUM_SESSION.SESSION_DONVIID].ToString();
            }
            TAM_UNG_AN_PHI_BL oBL = new TAM_UNG_AN_PHI_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            //-------------
            tbl = oBL.Get_ThongBaoSoLuong(Session[ENUM_SESSION.SESSION_USERNAME].ToString(),THAid);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            Li_thongke.Text = row["TEXT_REPORT"] +"";
        }
        protected void dropPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize2.SelectedValue = dropPageSize.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        protected void dropPageSize2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize.SelectedValue = dropPageSize2.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        void DowloadFile(Decimal VanBanID, String _styleCase)
        {
            try
            {
                if (_styleCase == "02")
                {
                    ADS_FILE oND = dt.ADS_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }
                if (_styleCase == "03")
                {
                    AHN_FILE oND = dt.AHN_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }
                if (_styleCase == "04")
                {
                    AKT_FILE oND = dt.AKT_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }
                if (_styleCase == "05")
                {
                    ALD_FILE oND = dt.ALD_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }
                if (_styleCase == "06")
                {
                    AHC_FILE oND = dt.AHC_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }
                if (_styleCase == "07")
                {
                    APS_FILE oND = dt.APS_FILE.Where(x => x.ID == VanBanID).FirstOrDefault();
                    if (oND.TENFILE != "")
                    {
                        if (oND.NOIDUNG != null)
                        {
                            var cacheKey = Guid.NewGuid().ToString("N");
                            Context.Cache.Insert(key: cacheKey, value: oND.NOIDUNG, dependencies: null, absoluteExpiration: DateTime.Now.AddSeconds(30), slidingExpiration: System.Web.Caching.Cache.NoSlidingExpiration);
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Download", "window.location='" + Cls_Comon.GetRootURL() + "/DownloadFile.aspx?cacheKey=" + cacheKey + "&FileName=" + oND.TENFILE + "&Extension=" + oND.KIEUFILE + "';", true);
                        }
                        else
                        {
                            lbthongbao.Text = "Tệp đính kèm không có nội dung. Không thể tải về được!";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                lbthongbao.Text = ex.Message;
            }
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        #endregion
        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
    }
}