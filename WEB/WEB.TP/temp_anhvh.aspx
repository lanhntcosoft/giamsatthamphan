﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="temp_anhvh.aspx.cs" Inherits="WEB.TP.temp_anhvh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="1" style="font-family: times New Roman; font-size: 11pt; text-align: center;">
            <tr style="text-align: center;">
                <th colspan="2" style="text-align: center; vertical-align: middle; height: 36px;">ĐƠN VỊ</th>
                <th colspan="8" style="text-align: center; vertical-align: middle; font-size: 14pt;">THÔNG KÊ THÔNG TIN TẠM ỨNG ÁN PHÍ</th>
                <th colspan="2" style="text-align: center; vertical-align: middle;">Mẫu 1</th>
            </tr>
            <tr>
                <th colspan="2" style="text-align: center; vertical-align: top; height: 30px; font-weight: bold;">.......</th>
                <td colspan="8" style="text-align: center; vertical-align: top; font-style: italic;">Từ ngày '||V_DATE_FROM||'..... đến ngày '||V_DATE_TO||'.....</td>
                <td colspan="2" style="text-align: center; vertical-align: top; font-style: italic;"></td>
            </tr>
            <tr style="">
                <th style="text-align: center; vertical-align: middle; border: 0.1pt solid Black; height: 135px;">STT</th>
                <th style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Đơn vị</th>
                <th style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số thông báo nộp tiền tạm ứng án phí</th>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số thông báo đã nộp tiền tạm ứng án phí</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số thông báo chưa nộp tiền tạm ứng án phí </td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số vụ việc hoàn trả tạm ứng án phí</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số vụ việc đình chỉ nộp tạm ứng án phí</td>
                <th style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số tiền tạm ứng án phí phải thu</th>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số tiền tạm ứng án phí đã thu</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số tiền tạm ứng án phí chưa nộp</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số tiền tạm ứng án phí hoàn trả</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">Tổng số tiền đình chỉ nộp tạm ứng án phí</td>
            </tr>
            <tr style="font-style: italic;">
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">1</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">2</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">3</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">4</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">5</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">6</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">7</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">8</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">9</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">10</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">11</td>
                <td style="text-align: center; vertical-align: middle; border: 0.1pt solid Black;">12</td>
            </tr>
            <tr style="height: 0px;">
                <td style="width: 47px"></td>
                <td style="width: 280px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
                <td style="width: 70px"></td>
            </tr>
        </table>
    </form>
</body>
</html>
