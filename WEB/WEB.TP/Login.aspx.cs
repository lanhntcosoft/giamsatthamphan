﻿
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BL.GSTP;
using DAL.GSTP;

using Module.Common;

namespace WEB.TP
{
    public partial class Login : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();
                //try
                //{
                //    List<DM_TRANGTINH> lstHT = dt.DM_TRANGTINH.Where(x => x.MATRANG == "hotrologingscm").ToList();
                //    if (lstHT.Count > 0)
                //    {
                //        lsthotro.Text = lstHT[0].NOIDUNG;
                //    }
                //}
                //catch (Exception ex) { }
            }
        }
        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() == "")
                {
                    lttMsg.Text = "Chưa nhập mã truy cập !";
                    return;
                }
                string strPass = Cls_Comon.MD5Encrypt(txtPass.Text);               
                string user_name = txtUserName.Text.Trim().ToLower();
                //int IsDomain = 0;
                //if (chkIsDomain.Checked)
                //    IsDomain = 1;
                QT_TUPHAP_BL objBL = new QT_TUPHAP_BL();
                DataTable tbl = objBL.CheckLogin(user_name, strPass);
                if (tbl != null && tbl.Rows.Count>0)
                {
                    DataRow row = tbl.Rows[0];
                    LoginSuccess(row);
                }
                else
                {
                    lttMsg.Text = "Mã truy cập hoặc mật khẩu không đúng !";
                    return;
                }
            }
            catch (Exception ex)
            {
                lttMsg.Text = ex.ToString();
            }
        }
        void LoginSuccess(DataRow row)
        {
            Session[ENUM_SESSION.SESSION_USERID] = row["ID"];
            Session[ENUM_SESSION.SESSION_USERNAME] = row["USERNAME"];
            Session[ENUM_SESSION.SESSION_USERTEN] = row["HOTEN"];
            Session[ENUM_SESSION.SESSION_DONVIID] = row["DONVITHA_ID"];           
            Session[ENUM_SESSION.SESSION_TENDONVI] = row["MA_TEN"];
            Session["TEN_DV"] = row["TEN"];
            Session["LOAITOA"] = row["LOAITOA"];
            Session.Timeout = 120;
            Response.Redirect("trangchu.aspx");
        }        
    }
}