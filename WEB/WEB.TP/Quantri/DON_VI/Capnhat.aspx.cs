﻿using BL.GSTP;
using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.TP.In;

namespace WEB.TP.Quantri.DON_VI
{
    public partial class Capnhat : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            Decimal CurrUser = (string.IsNullOrEmpty(Session[ENUM_SESSION.SESSION_USERID] + "")) ? 0 : Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID] + "");
            if (CurrUser == 0)
            {
                Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
            }
            if (!IsPostBack)
            {
                Decimal  Donvi_ID = (String.IsNullOrEmpty(Request["donviID"] + "")) ? 0 : Convert.ToDecimal(Request["donviID"] + "");
                DM_DONVITHIHANHAN oT = dt.DM_DONVITHIHANHAN.Where(x => x.ID == Donvi_ID).FirstOrDefault();
                if (oT != null)
                {
                    txt_TEN.Text = oT.TEN;
                    txt_DIACHI.Text = oT.DIACHI;
                    txt_DIENTHOAI.Text = oT.DIENTHOAI;
                    txt_EMAIL.Text = oT.EMAIL;
                }
                MenuPermission oPer = QT_TUPHAP_BL.GetMenuPer_THADS(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                Cls_Comon.SetButton(cmdSave, oPer.CAPNHAT);
            }
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            if (CheckValid() == true)
            {
                try
                {
                    string Ten = txt_TEN.Text.Trim();
                    Decimal Donvi_ID = (String.IsNullOrEmpty(Request["donviID"] + "")) ? 0 : Convert.ToDecimal(Request["donviID"] + "");
                    DM_DONVITHIHANHAN toaAn = dt.DM_DONVITHIHANHAN.Where(x => x.ID == Donvi_ID).FirstOrDefault<DM_DONVITHIHANHAN>();
                    toaAn.DIACHI = txt_DIACHI.Text.Trim();
                    toaAn.DIENTHOAI=txt_DIENTHOAI.Text.Trim();
                    toaAn.EMAIL=txt_EMAIL.Text.Trim();
                    //tam thời đóng lại chưa chp phép cập nhật
                    toaAn.TEN = Ten;
                    DM_DONVITHIHANHAN toaAnParent = dt.DM_DONVITHIHANHAN.Where(x => x.ID == toaAn.CAPCHAID).FirstOrDefault<DM_DONVITHIHANHAN>();
                    if (toaAnParent == null)
                    {
                        toaAn.MA_TEN = toaAn.TEN;
                    }
                    else
                    {
                        if (toaAn.LOAITOA == "CAPHUYEN" || toaAn.LOAITOA == "QSKHUVUC")
                            toaAn.MA_TEN = toaAn.TEN + "," + toaAnParent.TEN.Replace("Cục Thi hành án", "");
                        else
                            toaAn.MA_TEN = toaAn.TEN;
                    }
                    dt.SaveChanges();
                    lstMsgB.Text = "Bạn đã cập nhật thành công";
                    Cls_Comon.CallFunctionJS(this, this.GetType(), "ReloadParent();");
                }
                catch (Exception ex)
                {
                    lstMsgB.Text = "Lỗi: " + ex.Message;
                }
            }
        }
        private bool CheckValid()
        {
            if (txt_TEN.Text == "")
            {
                lstMsgB.Text = "Bạn chưa nhập tên đơn vị";
                txt_TEN.Focus();
                return false;
            }
            if (txt_DIACHI.Text == "")
            {
                lstMsgB.Text = "Bạn chưa nhập địa chỉ";
                txt_DIACHI.Focus();
                return false;
            }
            return true;
        }
    }
}