﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NopAnPhi.aspx.cs" Inherits="WEB.TP.NopAnPhi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../../UI/css/style.css" rel="stylesheet" />
    <script src="../../../UI/js/Common.js"></script>
    <script type="text/javascript" src="/UI/js/base64.js"></script>
    <script type="text/javascript" src="/UI/js/vgcaplugin.js"></script>
    <title>Nộp án phí</title>
    <style>
        .content_body {
            width: 96%;
            margin: 20px 2%;
        }

        .cell_label {
            margin-left: 5px;
        }

        .buttoninput {
            float: left;
            margin-right: 8px;
            padding: 5px 10px 6px 10px;
            box-shadow: 0 3px 7px rgba(0, 0, 0, 0.2);
            background: #d02629;
            border-radius: 5px;
            color: white;
            cursor: pointer;
            font-size: 15px;
            font-weight: bold;
            border: medium none;
        }

        .buttondisable {
            float: left;
            margin-right: 8px;
            padding: 5px 10px 6px 10px;
            box-shadow: 0 3px 7px rgba(0, 0, 0, 0.2);
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="Ajax_Manager_Updata" runat="server">
            <ContentTemplate>
                <div class="content_form">
                    <asp:HiddenField ID="hddid" runat="server" Value="0" />
                    <asp:HiddenField ID="hdd_case" runat="server" Value="0" />
                    <asp:HiddenField ID="bmID" runat="server" Value="0" />
                    <div class="content_body" style="width: 750px; margin-top: 0px;">
                        <div class="content_form_head" style="width: 750px;">
                            <div class="content_form_head_title">
                                <asp:Label ID="lbl_title_anphi" runat="server" Text="Nộp án phí"></asp:Label></div>
                            <div class="content_form_head_right"></div>
                        </div>
                        <div class="content_form_body" style="width: 750px;">
                            <div id="div_NopAnPhi" runat="server">
                                <div style="width: 700px; float: left;">
                                    <div>
                                        <h4 class="tleboxchung">1.Thông tin người nộp án phí</h4>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Người nộp là</div>
                                    <div style="float: left;">
                                        <asp:RadioButtonList ID="rdLoaiNguoiNop" runat="server"
                                            CssClass="radio_cts" RepeatDirection="Horizontal"
                                            AutoPostBack="true" OnSelectedIndexChanged="rdLoaiNguoiNop_SelectedIndexChanged">
                                            <asp:ListItem Text="Nguyên đơn" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Người khác" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Họ tên<span class="must_input">(*)</span></div>
                                    <div style="float: left;" id="td_txtNguoiNop_HoTen" runat="server">
                                        <asp:TextBox ID="txtNguoiNop_HoTen" CssClass="textbox" runat="server"
                                            MaxLength="250" Width="219px"></asp:TextBox>
                                    </div>
                                    <div style="float: left;" id="div_gioitinh_namsinh" runat="server">
                                        <div style="float: left; width: 80px; text-align: right; margin-right: 10px; padding-top: 6px;">
                                            <asp:Label ID="lbl_gioitinh" runat="server" Text="Giới tính"></asp:Label>
                                        </div>
                                        <div style="float: left;">
                                            <asp:DropDownList ID="dropNguoiNop_GioiTinh" CssClass="dropbox"
                                                runat="server" Width="92px">
                                                <asp:ListItem Value="1" Text="Nam" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float: left; width: 67px; margin-right: 10px; padding-top: 6px; text-align: right;">Năm sinh</div>
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtNguoiNop_NamSinh" CssClass="textbox"
                                                onkeypress="return isNumber(event)" runat="server"
                                                Width="50px" MaxLength="4"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Số CMND/Thẻ căn cước<span runat="server" id="txtNguoiNop_CMND_must_input" class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNop_CMND" CssClass="textbox"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="219px" MaxLength="20"></asp:TextBox>
                                    </div>
                                    <div style="float: left; width: 80px; text-align: right; margin-right: 10px; padding-top: 6px;">Điện thoại</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNop_DienThoai" runat="server"
                                            onkeypress="return isNumber(event)"
                                            CssClass="textbox" Width="219px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Địa chỉ</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNop_Diachi" CssClass="textbox"
                                            runat="server" MaxLength="250" Width="543px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div>
                                        <h4 class="tleboxchung">2.Nộp án phí</h4>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Số biên lại <span class="must_input">(*)</span></span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtSoBienLai" CssClass="textbox" runat="server" Width="219px"></asp:TextBox>
                                    </div>
                                    <div style="float: left; width: 80px; padding-top: 6px; text-align: right; padding-right: 10px;">Tạm ứng án phí<span class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtTamUngAP" runat="server" Enabled="false"
                                            CssClass="textbox align_left" Width="178px" onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                        <span style="margin-left: 3px; font-weight: bold;">(VNĐ)</span>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Ngày biên lai <span class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNGAYBIENLAI" runat="server" CssClass="user" Width="100px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtNgayQuyetDinh_CalendarExtender" runat="server" TargetControlID="txtNGAYBIENLAI" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtNGAYBIENLAI" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </div>
                                    <div style="float: left; width: 120px; padding-top: 6px; margin-left: 85px; text-align: right; padding-right: 10px;">Người thu tiền<span class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNGUOITHUTIEN" CssClass="user" runat="server" Width="219px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="div_hoantraAnphi" runat="server">
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left;">
                                        <h4 class="tleboxchung">3.Thông tin người nhận hoàn trả tiền tạm ứng án phí</h4>
                                    </div>
                                </div>
                                <div runat="server" id="div_ANPHIHOANTRA" style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Số tiền <span runat="server" id="must_input_ANPHIHOANTRA" class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txt_ANPHIHOANTRA" CssClass="user" runat="server"
                                            MaxLength="250" Width="178px" onkeyup="javascript:this.value=Comma(this.value);" onkeypress="return isNumber(event)"></asp:TextBox>
                                        <span style="margin-left: 3px; font-weight: bold;">(VNĐ)</span>
                                    </div>
                                    <div style="margin-left: 4px; float: left; width: 80px; padding-top: 0px; text-align: right; padding-right: 10px;">Ngày hoàn trả <span runat="server" id="must_input_NGAYHOANTRA" class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txt_NGAYHOANTRA" runat="server" CssClass="user" Width="219px" MaxLength="10"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_NGAYHOANTRA" Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txt_NGAYHOANTRA" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Người nhận là</div>
                                    <div style="float: left;">
                                        <asp:RadioButtonList ID="rdLoaiNguoiNhan" runat="server"
                                            CssClass="radio_cts" RepeatDirection="Horizontal"
                                            AutoPostBack="true" OnSelectedIndexChanged="rdLoaiNguoiNhan_SelectedIndexChanged">
                                            <asp:ListItem Text="Nguyên đơn" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Người khác" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Họ tên<span runat="server" id="must_input_NguoiNhan_Hoten" class="must_input">(*)</span></div>
                                    <div style="float: left;" id="td_txtNguoiNhan_Hoten" runat="server">
                                        <asp:TextBox ID="txtNguoiNhan_Hoten" CssClass="textbox" runat="server"
                                            MaxLength="250" Width="219px"></asp:TextBox>
                                    </div>
                                    <div style="float: left;" id="div_gioitinh_namsinh_nhan" runat="server">
                                        <div style="float: left; width: 80px; text-align: right; margin-right: 10px; padding-top: 6px;">
                                            <asp:Label ID="lbl_gioitinh_nhan" runat="server" Text="Giới tính"></asp:Label>
                                        </div>
                                        <div style="float: left;">
                                            <asp:DropDownList ID="dropNguoiNhan_GioiTinh" CssClass="dropbox"
                                                runat="server" Width="82px">
                                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float: left; width: 67px; margin-right: 10px; padding-top: 6px; text-align: right;">Năm sinh</div>
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtNguoiNhan_Namsinh" CssClass="textbox"
                                                onkeypress="return isNumber(event)" runat="server"
                                                Width="60px" MaxLength="4"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Số CMND/Thẻ căn cước<span runat="server" id="txtNguoiNhan_CMND_must_input" class="must_input">(*)</span></div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNhan_CMND" CssClass="textbox"
                                            onkeypress="return isNumber(event)" runat="server"
                                            Width="219px" MaxLength="4"></asp:TextBox>
                                    </div>
                                    <div style="float: left; width: 80px; text-align: right; margin-right: 10px; padding-top: 6px;">Điện thoại</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNhan_Dienthoai" runat="server"
                                            onkeypress="return isNumber(event)"
                                            CssClass="textbox" Width="219px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Email</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNhan_Email" CssClass="textbox"
                                            runat="server" Width="543px" MaxLength="250"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 6px;">Địa chỉ chi tiết</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNguoiNhan_DiaChiChitiet" CssClass="textbox"
                                            runat="server" MaxLength="250" Width="543px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 700px; float: left; margin-top: 10px;">
                                    <div style="float: left; width: 100px; padding-top: 16px;">Ghi chú</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txt_GHICHU_HOANTRA" CssClass="user" runat="server" TextMode="MultiLine" Width="543px" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 700px; float: left; margin-top: 0px;">
                                <div style="float: left; font-size: 16px; margin: 10px; text-align: left; width: 95%; color: red; padding-left: 90px;">
                                    <asp:Literal ID="lstMsgB" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div style="width: 600px; float: left; margin-top: 0px; margin-left: 100px;">
                                <div style="text-align: center;">
                                    <asp:Button ID="btnNBInBL" runat="server" CssClass="button" Text="In biên lai" OnClick="btnNBInBL_Click" />
                                    <asp:Button ID="cmdSave" runat="server" CssClass="button" Text="Lưu" OnClick="cmdSave_Click" />
                                    <a href="javascript:;" onclick="window.close();" class="button">Đóng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Ajax_Manager_Updata">
            <ProgressTemplate>
                <div class="processmodal">
                    <div class="processcenter">
                        <img src="/UI/img/process.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <script>
           <%-- function validate() {
                var txtSoBienLai = document.getElementById('<%=txtSoBienLai.ClientID%>');
                if (!Common_CheckTextBox(txtSoBienLai, "Số biên lai"))
                    return false;

                var txtTamUngAP = document.getElementById('<%=txtTamUngAP.ClientID%>');
                if (!Common_CheckTextBox(txtTamUngAP, "Tạm ứng án phí"))
                    return false;

                return true;
            }--%>
            function ReloadParent() {
                window.onunload = function (e) {
                    opener.Loadds_tha();
                };
                // window.close();
            }
        </script>
        <script type="text/javascript">
            function PopupReport(pageURL, title, w, h) {
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2);
                var targetWin = window.open(pageURL, title, 'toolbar=no, channelmode=no,location =no,scrollbars=yes,resizable=no,menubar=no,width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
                return targetWin;
            }
        </script>
    </form>
</body>
</html>
<script src="../../../UI/js/init.js"></script>
