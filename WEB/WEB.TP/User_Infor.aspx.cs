﻿using DAL.GSTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Module.Common;

namespace WEB.TP
{
    public partial class User_Infor : System.Web.UI.Page
    {
        GSTPContext dt = new GSTPContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string strUserID = Session[ENUM_SESSION.SESSION_USERID] + "";
                    if (strUserID == "") Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
                    txtUserName.Text = Session[ENUM_SESSION.SESSION_USERNAME] + "";
                    Load_User_Info();
                }
                catch (Exception ex)
                {
                    { lttMsg.Text = ex.Message; }
                }
            }
        }
        protected void Load_User_Info()
        {
            decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            TUPHAP_NGUOISUDUNG oT = dt.TUPHAP_NGUOISUDUNG.Where(x => x.ID == current_id).FirstOrDefault();
            txt_HOTEN.Text=oT.HOTEN;
            txt_DIENTHOAI.Text=oT.DIENTHOAI;
            txt_EMAIL.Text = oT.EMAIL;
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon.GetRootURL() + "/Login.aspx");
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            if (txt_HOTEN.Text.Trim() == "")
            {
                lttMsg.Text = "Bạn phải nhập họ tên !";
                return;
            }
            decimal current_id = Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]);
            TUPHAP_NGUOISUDUNG oT = dt.TUPHAP_NGUOISUDUNG.Where(x => x.ID == current_id).FirstOrDefault();
            oT.HOTEN = txt_HOTEN.Text;
            oT.DIENTHOAI = txt_DIENTHOAI.Text;
            oT.EMAIL = txt_EMAIL.Text;
            dt.SaveChanges();
            lttMsg.Text = "Bạn đã cập nhật thông tin thành công.";
        }
    }
}