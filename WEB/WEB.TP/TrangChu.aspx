﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/AN_PHI.Master" AutoEventWireup="true" CodeBehind="TrangChu.aspx.cs" Inherits="WEB.TP.TrangChu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content_form" style="margin-bottom: 30px;">
        <div class="leftzone">
            <asp:Literal ID="Li_thongke" runat="server"></asp:Literal>
            <div class="leftmenu">
                <div class="leftmenu_header arrow" style="">
                    <span style="color: #ffffff;">Điện thoại liên hệ: 02432.444.269</span>
                </div>
            </div>
        </div>
        <div class="rightzone">
            <div class="content_body">
                <div class="content_form_head">
                    <div class="content_form_head_title">Tìm kiếm</div>
                    <div class="content_form_head_right"></div>
                </div>
                <div class="content_form_body">
                    <div class="homesearch" style="margin-top: 20px; width: 710px;">
                        <asp:TextBox ID="txtTuKhoaBasic" runat="server" Width="640px"
                            placeholder="(Nhập số thông báo, số CMTND/Thẻ căn cước hoặc tên đương sự để thực hiện tra cứu thông tin nộp án phí)"
                            CssClass="textbox_home"></asp:TextBox>
                        <asp:Button ID="cmdSearch" runat="server"
                            CssClass="buttonsearch_home float_right" OnClick="cmdSearch_Click" />
                    </div>
                    <div style="margin-top: 20px; float: left;">
                        <asp:RadioButtonList ID="rdTrangThai"
                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="800"
                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdTrangThai_SelectedIndexChanged">
                            <asp:ListItem Text="Chưa nộp án phí" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Đã nộp án phí" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Đã hoàn trả án phí" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Đình chỉ nộp án phí" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="msg_thongbao">
                        <asp:Literal ID="lbthongbao" runat="server"></asp:Literal>
                    </div>

                    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
                    <asp:Panel ID="pnDS" runat="server">
                        <div class="phantrang" style="margin-top: 30px;">
                            <div class="sobanghi">
                                <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                            </div>
                            <div class="sotrang">
                                <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                    OnClick="lbTBack_Click"><</asp:LinkButton>
                                <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                    Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                    Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                    Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                    Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                    Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                    Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                    OnClick="lbTNext_Click">></asp:LinkButton>
                                <asp:DropDownList ID="dropPageSize" runat="server" Width="65px"
                                    CssClass="dropbox"
                                    AutoPostBack="True" OnSelectedIndexChanged="dropPageSize_SelectedIndexChanged">
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                    <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div>
                            <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand" Visible="true" OnItemDataBound="rpt_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="CSSTableGenerator">
                                        <table>
                                            <tr id="row_header">
                                                <td style="width: 20px;">TT</td>
                                                <td style="width: 100px;">Thông tin vụ việc</td>
                                                <td style="width: 300px;">Thông tin đương sự</td>
                                                <td style="width: 300px;" id="td_header_hoantra" runat="server">Thông tin người nhận hoàn trả tiền tạm ứng án phí</td>
                                                <td style="width: 200px;" runat="server" id="td_header_toaan">Tòa án giải quyết</td>
                                                <td style="width: 100px;" runat="server" id="td_header_TAMUNGANPHI">Số tiền tạm ứng án phí (VNĐ)</td>
                                                <td style="width: 100px;" runat="server" id="td_header_ANPHIHOANTRA">Số tiền hoàn trả (VNĐ)</td>
                                                <td style="width: 100px;" runat="server" id="td_header_trangthai">Trạng thái</td>
                                                <td style="width: 200px;" runat="server" id="td_header_xacnhan">Thông tin nộp tiền tạm ứng án phí</td>
                                                <td style="width: 200px;" runat="server" id="td_header_xacnhanhoantra">Thông tin nhận hoàn trả tiền tạm ứng án phí</td>
                                                <td style="width: 60px;" runat="server" id="td_header_FILEID">Tải thông báo</td>
                                                <td style="width: 120px;" runat="server" id="td_header_thaotac">Thao tác</td>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center;"><%#Eval("STT") %> </td>
                                        <td style="text-align: center;">
                                            <ul>
                                                <li style="text-align: left; font-style: italic;">Mã thông báo: <%#Eval("MA_THONGBAO")%></li>
                                                <li style="text-align: left; font-style: italic;">Số thông báo: <%#Eval("SOTHONGBAO")%></li>
                                                <li style="text-align: left; font-style: italic;">Ngày: <%#Eval("NGAYGQ_YC")%></li>
                                                <li style="text-align: left; font-style: italic;">Mã vụ việc: <%#Eval("MAVUVIEC")%></li>
                                                <li style="text-align: left; font-style: italic;">Loại án: <%#Eval("LOAIAN")%></li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Họ tên: <span style="font-weight: bold; font-size: 14px;"><%#Eval("TENDUONGSU")%></span></li>
                                                <li>Năm sinh: <%#Eval("NAMSINH")%></li>
                                                <li>CMND: <%#Eval("SOCMND")%></li>
                                                <li>Điện thoại: <%#Eval("DIENTHOAI")%></li>
                                                <li>Email: <%#Eval("EMAIL")%></li>
                                                <li>Địa chỉ: <%#Eval("DIACHI")%></li>
                                            </ul>
                                        </td>
                                        <td id="td_item_hoantra" runat="server">
                                            <ul>
                                                <li>Họ tên: <span style="font-weight: bold; font-size: 14px;"><%#Eval("HOANTRAAP_HOTEN")%></span></li>
                                                <li>Năm sinh: <%#Eval("HOANTRAAP_NAMSINH")%></li>
                                                <li>CMND: <%#Eval("HOANTRAAP_CMND")%></li>
                                                <li>Điện thoại: <%#Eval("HOANTRAAP_TEL")%></li>
                                                <li>Email: <%#Eval("HOANTRAAP_EMAIL")%></li>
                                                <li>Địa chỉ: <%#Eval("HOANTRAAP_DIACHI")%></li>
                                            </ul>
                                        </td>
                                        <td runat="server" id="td_item_toaan"><%#Eval("DONVI")%></td>
                                        <td style="text-align: right;" runat="server" id="td_item_TAMUNGANPHI"><%#Eval("TAMUNGANPHI")%> </td>
                                        <td style="text-align: right;" runat="server" id="td_item_ANPHIHOANTRA"><%#Eval("ANPHIHOANTRA")%> </td>
                                        <td id="td_item_trangthai" style="text-align: center;" runat="server">
                                            <ul>
                                                <li><%#Eval("STATUS")%></li>
                                                <li style="padding-top: 10px;"><%#Eval("STATUS_HOANTRA")%></li>
                                            </ul>
                                        </td>
                                        <td id="td_item_xacnhan" runat="server">
                                            <ul>
                                                <li>Ngày nộp: <%#Eval("NGAYBIENLAI")%></li>
                                                <li>Số biên lai: <%#Eval("SOBIENLAI")%></li>
                                                <li>Người thu: <%#Eval("NGUOITHUTIEN")%></li>
                                            </ul>
                                        </td>
                                        <td id="td_item_xacnhan_tt" runat="server">
                                            <div class="align_center">
                                                <div style="float: left">
                                                    Xem biên lai
                                                </div>
                                                <div style="float: left;margin-left:10px;">
                                                    <asp:ImageButton ID="Dowload_Bienlai" runat="server"
                                                      OnClick="cmdFile_Attach_Click"   CommandName="DowloadBienlais" CommandArgument='<%#Eval("MA_THONGBAO").ToString() %>'
                                                        ImageUrl="/UI/img/download_file26.png" />
                                                </div>
                                            </div>
                                        </td>
                                        <td id="td_item_xacnhanhoantra" runat="server">
                                            <ul>
                                                <li>Ngày nhận: <%#Eval("NGAYHOANTRA")%></li>
                                                <li>Số biên lai: <%#Eval("SOBIENLAI")%></li>
                                                <li>Người thu: <%#Eval("NGUOITHUTIEN")%></li>
                                            </ul>
                                        </td>
                                        <td id="td_FILEID" runat="server">
                                            <div class="align_center">
                                                <asp:ImageButton ID="cmdDowload" runat="server"
                                                    CommandName="Dowload" CommandArgument='<%#Eval("FILEID")+";"+ Eval("MALOAIVUVIEC").ToString() %>'
                                                    ImageUrl="/UI/img/download_file26.png" />
                                            </div>
                                        </td>
                                        <td id="td_item_thaotac" runat="server" style="text-align: center;">
                                            <a class="link_nopap" href="javascript:;" onclick="nopanphi('<%#Eval("DONID")%>','<%#Eval("ID")%>','<%#Eval("MALOAIVUVIEC").ToString()%>','<%#Eval("BIEUMAUID")%>','0')">Cập nhật thông tin nộp án phí</a>
                                        </td>
                                        <td style="text-align: center; color: blue" id="td_item_xembienlai" runat="server">
                                            <p style="text-align: center;"><a class="link_nopap" href="javascript:;" onclick="nopanphi('<%#Eval("DONID")%>','<%#Eval("ID")%>','<%#Eval("MALOAIVUVIEC").ToString()%>','<%#Eval("BIEUMAUID")%>','1')">Sửa thông tin nộp án phí</a></p>
                                            <p style="text-align: center; margin-top: 10px;">
                                                <a class="link_nopap" style="color: #9f1a12;" href="javascript:;" onclick="nopanphi('<%#Eval("DONID")%>','<%#Eval("ID")%>','<%#Eval("MALOAIVUVIEC").ToString()%>','<%#Eval("BIEUMAUID")%>','2')">
                                                    <asp:Label ID="lbl_hoantra" runat="server" Text="Cập nhật thông tin hoàn trả án phí"></asp:Label></a>
                                            </p>
                                            <p style="text-align: center; margin-top: 10px;">
                                                <asp:LinkButton ID="lblSua" runat="server" Text="Xem biên lai" CausesValidation="false" CommandName="xembienlai" ForeColor="#9f1a12" Font-Italic="true"
                                                    CommandArgument='<%#Eval("DONID").ToString() +";"+ Eval("MALOAIVUVIEC").ToString()%>'></asp:LinkButton>
                                            </p>
                                        </td>
                                        <td style="text-align: center; color: blue" id="td_item_xemhoantra" runat="server">
                                            <p style="text-align: center; margin-top: 8px;"><a class="link_nopap" style="color: #9f1a12;" href="javascript:;" onclick="nopanphi('<%#Eval("DONID")%>','<%#Eval("ID")%>','<%#Eval("MALOAIVUVIEC").ToString()%>','<%#Eval("BIEUMAUID")%>','2')">Sửa thông tin hoàn trả án phí</a></p>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table></div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="phantrang">
                            <div class="sobanghi">
                                <asp:HiddenField ID="hdicha" runat="server" />
                                <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                            </div>
                            <div class="sotrang">
                                <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                    OnClick="lbTBack_Click"><</asp:LinkButton>
                                <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                    Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>

                                <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                    Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                    Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                    Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                    Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                    Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                    OnClick="lbTNext_Click">></asp:LinkButton>
                                <asp:DropDownList ID="dropPageSize2" runat="server" Width="65px" CssClass="dropbox"
                                    AutoPostBack="True" OnSelectedIndexChanged="dropPageSize2_SelectedIndexChanged">
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                    <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <script>
                            function nopanphi(_DON_ID, _DUONGSU_ID, _MALOAIVUVIEC, _BIEUMAUID, _STYLE_PANEL) {
                                var link = "/NopAnPhi.aspx?donID=" + _DON_ID + "&dsID=" + _DUONGSU_ID + "&styleCase=" + _MALOAIVUVIEC + "&bmID=" + _BIEUMAUID + "&style_panel=" + _STYLE_PANEL;
                                var width = 850;
                                var height = 550;
                                if (_STYLE_PANEL != '2') {
                                    height = 750;
                                }
                                OpenPopupCenter(link, "Nộp án phí", width, height);
                            }
                            function OpenPopupCenter(pageURL, title, w, h) {
                                var left = (screen.width / 2) - (w / 2);
                                var top = (screen.height / 2) - (h / 2);
                                var targetWin = window.open(pageURL, title, 'toolbar=no,scrollbars=yes,resizable=yes,width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
                                return targetWin;
                            }
                            function PopupReport(pageURL, title, w, h) {
                                var left = (screen.width / 2) - (w / 2);
                                var top = (screen.height / 2) - (h / 2);
                                var targetWin = window.open(pageURL, title, 'toolbar=no, channelmode=no,location =no,scrollbars=yes,resizable=no,menubar=no,width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
                                return targetWin;
                            }
                            function Loadds_tha() {
                                $("#<%= cmdSearch.ClientID %>").click();
                            }
                        </script>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <style>
        .float_right {
            cursor: pointer;
        }

        .homesearch {
            width: 60%;
        }

        .phantrang {
            margin-top: 50px;
        }

        .CSSTableGenerator ul li {
            line-height: 20px;
        }
    </style>

</asp:Content>
