﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.ZCOVID.BL;
using System.Data;
using WEB.ZCOVID.Module;
namespace WEB.ZCOVID
{
    public partial class Login_bep_an : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();
            }

        }
        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() == "")
                {
                    lttMsg.Text = "Chưa nhập mã truy cập !";
                    txtUserName.Focus();
                    return;
                }
                string strPass = Cls_Comon_covid.MD5Encrypt(txtPass.Text);
                string user_name = txtUserName.Text.Trim();
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                DataTable tbl = obj.Login(user_name, strPass);
                if (tbl != null && tbl.Rows.Count > 0)
                {
                    DataRow row = tbl.Rows[0];
                    LoginSuccess(row);
                }
                else
                {
                    lttMsg.Text = "Mã truy cập hoặc mật khẩu không đúng";
                    return;
                }
            }
            catch (Exception ex)
            {
                lttMsg.Text = ex.ToString();
            }
        }
        void LoginSuccess(DataRow row)
        {
            Session["SESSION_USERID"] = row["USERID"];
            Session["SESSION_USERNAME"] = row["USERNAME"];
            Session["SESSION_CANBO_ID"] = row["CANBO_ID"];
            Session["SESSION_HOTEN"] = row["HOTEN"];
            Session["SESSION_DV_TRUCTHUOID"] = row["DV_TRUCTHUOID"];
            Session["SESSION_DV_TRUCTHUOC_TEN"] = row["DV_TRUCTHUOC_TEN"];
            Session["SESSION_PHONGBANID"] = row["PHONGBANID"];
            Session["SESSION_TENPHONGBAN"] = row["TENPHONGBAN"];
            Session["SESSION_SODIENTHOAI"] = row["SODIENTHOAI"];
            Session["SESSION_EMAIL"] = row["EMAIL"];
            Session["SESSION_NHOM_ND_ID"] = row["NHOM_ND_ID"];
            Session.Timeout = 120;
            Response.Redirect("TrangChu_bep_an.aspx");
        }
    }
}