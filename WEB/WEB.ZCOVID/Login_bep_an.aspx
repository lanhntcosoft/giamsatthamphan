﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login_bep_an.aspx.cs" Inherits="WEB.ZCOVID.Login_bep_an" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="UI/js/Common.js"></script>
    <link href="/UI/css/style.css" rel="stylesheet" />
    <link href="UI/img/spcLogo.png" type="image/png" rel="shortcut icon" />
    <title></title>
    <style>
        .processmodal {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .processcenter {
            z-index: 1000;
            margin: 300px 300px;
            padding: 10px;
            width: 180px;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

            .processcenter img {
                height: 100px;
                width: 100px;
            }

        .textfield {
            display: block;
            height: 42px;
            width: 87%;
            /*width: 300px;*/
            margin: 0px 0;
            text-indent: 15px;
            border: 0px;
            background-color: #fffac7;
        }

        .clearfix {
            width: 100%;
            float: left;
            margin-left: 10px;
            margin-top: 5px;
        }

        .matruycap {
            float: left;
            width: 42px;
            height: 44px;
            background: #fffac7 url(UI/img/bgmatruycap.png);
            border-right: solid 1px #aeaeae;
        }

        .box {
            display: block;
            float: left;
            /*margin-left: 35px;
                    width: 345px;*/
            width: 86%;
            margin-left: 7%;
            border: solid 1px #aeaeae;
            margin-bottom: 20px;
            border-radius: 4px 4px 4px 4px;
            background: #fffac7;
        }

        .matkhau {
            float: left;
            width: 42px;
            height: 44px;
            background: #fffac7 url(UI/img/bgmatkhau.png);
            border-right: solid 1px #aeaeae;
        }

        .button {
            margin-top: 10px;
        }
        #login_frame_covi {
            width: 100%;
            height:700px;
            /*background-color:#ffffff;*/
            background-size: auto;
            background-size: cover;
            float: left;
        }
    </style>
</head>
<body style="background-color:#eae0e0">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="header">
                    <div class="headertop" style="position: relative;">
                        <a href="http://www.toaan.gov.vn" target="_blank">
                            <div class="logo_toaan"></div>
                        </a>
                        <div class="logo_text" style="line-height:30px;top: 0px; left: 60px; position: absolute; text-align: left;">
                            TÒA ÁN NHÂN DÂN TỐI CAO <br/>
                           PHẦN MỀM HỖ TRỢ ĐĂNG KÝ ĂN TRƯA TẠI BẾP ĂN TAND TỐI CAO
                        </div>
                    </div>
                </div>
                <div id="login_frame_covi">
                    <div style="position: absolute; left: 35%; top: 25%; width: 416px; height: 335px; border: 1px solid #b72921; background-color: #ffffff;">
                        <div style="height: 60px; background-color: #d7342b; color: #ffffff;">
                            <div style="padding-left:0px; font-weight: bold; float: left; margin-top: 18px; text-align: center; width: 405px; font-family: Roboto; font-size: 20px;">
                                ĐĂNG NHẬP HỆ THỐNG
                            </div>
                        </div>
                        <div class="box" style="margin-top: 25px;">
                            <div class="matruycap"></div>
                            <asp:TextBox ID="txtUserName" runat="server" placeholder="Tài khoản" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div class="box" style="">
                            <div class="matkhau"></div>
                            <asp:TextBox ID="txtPass" runat="server" placeholder="Mật khẩu" TextMode="Password" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div style="float: left; width: 100%; text-align: center;">
                            <div class="button_login" style="">
                                <asp:ImageButton ID="cmdLogIn" ImageUrl="UI/img/lgdangnhap_button.png"
                                    runat="server" Width="86%" OnClientClick="return validate()" OnClick="cmdLogIn_Click" />

                            </div>
                        </div>
                        <div class="clearfix">
                            <div style="font-size: 14px; float: left; margin-left: 20px; color: #da0421; font-weight: bold; margin-top: 4px;">
                                <asp:Literal ID="lttMsg" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <script>
                        function Common_CheckEmpty(string_root) {
                            retval = true;
                            _pattern = /\s/g;
                            if (string_root.replace(_pattern, "") == "") {
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                        function validate() {
                            var txtUserName = document.getElementById('<%=txtUserName.ClientID%>');
                            if (!Common_CheckTextBox(txtUserName, "Tài khoản đăng nhập")) return false;

                            var txtPass = document.getElementById('<%=txtPass.ClientID%>');
                            if (!Common_CheckTextBox(txtPass, "Mật khẩu"))
                                return false;
                            return true;
                        }
                    </script>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="processmodal">
                    <div class="processcenter">
                        <img src="UI/img/process1.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
