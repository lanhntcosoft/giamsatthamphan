﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;
namespace WEB.ZCOVID.Danhmuc.Canbo
{
    public partial class Danhsach : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadDropDonVi_TrucThuoc();
                    LoadDropChucDanh();
                    LoadDropChucVu();
                    if (Session["SSCB_TA"] != null) DropDV_TRUCTHUOC.SelectedValue = Session["SSCB_TA"] + "";
                    if (Session["SSCB_PB"] != null) ddlPhongban.SelectedValue = Session["SSCB_PB"] + "";
                    if (Session["SSCB_CD"] != null) DropChucDanh.SelectedValue = Session["SSCB_CD"] + "";
                    if (Session["SSCB_CV"] != null) DropChucVu.SelectedValue = Session["SSCB_CV"] + "";
                    txKey.Text = Session["SSCB_TK"] + "";
                    hddPageIndex.Value = "1";
                    Load_Data();
                    //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    //Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
                }
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        private void LoadDropDonVi_TrucThuoc()
        {
            DropDV_TRUCTHUOC.Items.Clear();
            COVID_DV_TRUCTHUOC_BL oBL = new COVID_DV_TRUCTHUOC_BL();
            DropDV_TRUCTHUOC.DataSource = oBL.GET_DV_TRUCTHUOC_LIST();
            DropDV_TRUCTHUOC.DataTextField = "TEN_ALIAS";
            DropDV_TRUCTHUOC.DataValueField = "ID";
            DropDV_TRUCTHUOC.DataBind();
            DropDV_TRUCTHUOC.Items.Insert(0, new ListItem("-- Tất cả --", ""));
            LoadPhongban();
        }
        private void LoadPhongban()
        {

            ddlPhongban.Items.Clear();
            COVID_PHONGBAN_BL oBL = new COVID_PHONGBAN_BL();
            ddlPhongban.DataSource = oBL.GET_PHONGBAN_LIST(DropDV_TRUCTHUOC.SelectedValue);
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("-- Tất cả --", ""));
        }
        public void LoadDropChucDanh()
        {
            DropChucDanh.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucDanh = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCDANH");
            if (dmChucDanh != null && dmChucDanh.Rows.Count > 0)
            {
                DropChucDanh.DataSource = dmChucDanh;
                DropChucDanh.DataTextField = "TEN";
                DropChucDanh.DataValueField = "ID";
                DropChucDanh.DataBind();
            }
            DropChucDanh.Items.Insert(0, new ListItem("--- Tất cả ---", ""));
        }
        public void LoadDropChucVu()
        {
            DropChucVu.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucVu = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCVU");
            if (dmChucVu != null && dmChucVu.Rows.Count > 0)
            {
                DropChucVu.DataSource = dmChucVu;
                DropChucVu.DataTextField = "TEN";
                DropChucVu.DataValueField = "ID";
                DropChucVu.DataBind();
            }
            DropChucVu.Items.Insert(0, new ListItem("--- Tất cả ---", ""));
        }
        private void Load_Data()
        {
            int PageSize = Convert.ToInt32(dropPageSize.SelectedValue);
            COVID_DM_CANBO_BL CanBoBL = new COVID_DM_CANBO_BL();
            int CurPage = Convert.ToInt32(hddPageIndex.Value);
            DataTable oDT = CanBoBL.COVID_DM_CANBO_SEARCH(DropDV_TRUCTHUOC.SelectedValue, ddlPhongban.SelectedValue, DropChucDanh.SelectedValue, DropChucVu.SelectedValue, DropTrangThaiCongTac.SelectedValue, txKey.Text.Trim(),CurPage, PageSize);
            if (oDT != null && oDT.Rows.Count > 0)
            {
                int Total = Convert.ToInt32(oDT.Rows[0]["Total"].ToString());
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon_covid.GetTotalPage(Total, PageSize).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + Total.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }
            dgList.CurrentPageIndex = 0;
            dgList.PageSize = PageSize;
            dgList.DataSource = oDT;
            dgList.DataBind();

        }
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SSCB_TA"] = DropDV_TRUCTHUOC.SelectedValue;
                Session["SSCB_PB"] = ddlPhongban.SelectedValue;
                Session["SSCB_CD"] = DropChucDanh.SelectedValue;
                Session["SSCB_CV"] = DropChucVu.SelectedValue;
                Session["SSCB_TK"] = txKey.Text;
                hddPageIndex.Value = "1";
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void btnThemmoi_Click(object sender, EventArgs e)
        {
            Response.Redirect("Capnhat.aspx");
        }
        protected void cmdLamMoi_Click(object sender, EventArgs e)
        {
            Session.Remove("SSCB_TA");
            Session.Remove("SSCB_PB");
            Session.Remove("SSCB_CD");
            Session.Remove("SSCB_CV");
            Session.Remove("SSCB_TK");
            DropDV_TRUCTHUOC.SelectedValue = string.Empty;
            ddlPhongban.SelectedValue = string.Empty;
            DropChucDanh.SelectedValue = string.Empty;
            DropChucVu.SelectedValue = string.Empty;
            DropTrangThaiCongTac.SelectedValue = string.Empty;
            txKey.Text = string.Empty;
            lbtthongbao.Text = string.Empty;
        }
        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Sua":
                    Response.Redirect("Capnhat.aspx?cb=" + e.CommandArgument.ToString());
                    break;
                case "Xoa":
                    COVID_DM_CANBO_BL obj_bl = new COVID_DM_CANBO_BL();
                    decimal ID = Convert.ToDecimal(e.CommandArgument);
                    String v_thongbao = "";
                    if (obj_bl.DELETE_CANBO_BYID(ID, ref v_thongbao) == true)
                    {
                        if (v_thongbao != "")
                        {
                            lbtthongbao.Text = v_thongbao;
                        }
                        else
                        {
                            hddPageIndex.Value = "1";
                            Load_Data();
                            lbtthongbao.Text = "Bạn đã xóa thành công!";
                        }
                    }
                    break;
            }
        }
        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //try
            //{
            //    MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            //    if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            //    {
            //        LinkButton lbtSua = (LinkButton)e.Item.FindControl("lbtSua");
            //        LinkButton lbtXoa = (LinkButton)e.Item.FindControl("lbtXoa");
            //        lbtSua.Visible = oPer.CAPNHAT; lbtXoa.Visible = oPer.XOA;
            //    }
            //}
            //catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void dropPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize2.SelectedValue = dropPageSize.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        protected void dropPageSize2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize.SelectedValue = dropPageSize2.SelectedValue;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                Load_Data();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        #endregion
        protected void DV_TRUCTHUOC_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPhongban();
        }
    }
}