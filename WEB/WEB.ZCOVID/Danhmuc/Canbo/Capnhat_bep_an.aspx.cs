﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID.Danhmuc.Canbo
{
    public partial class Capnhat_bep_an : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadDropDonVi_TrucThuoc();
                    LoadPhongban();
                    LoadDropChucDanh();
                    LoadDropChucVu();
                    if (Request["cb"] != null)
                    {
                        decimal cbID = Convert.ToDecimal(Request["cb"].ToString());
                        hddCbID.Value = cbID.ToString();
                        LoadCanBoInfo(cbID);
                    }
                    //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    //Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        private void LoadCanBoInfo(decimal cbID)
        {
            try
            {
                COVID_DM_CANBO_BL obj = new COVID_DM_CANBO_BL();
                DataTable tbl = obj.GET_CANBO_ID(cbID);
                if (tbl.Rows.Count != 0)
                {
                    DropDV_TRUCTHUOC.SelectedValue = tbl.Rows[0]["DV_TRUCTHUOC"] + "";
                    LoadPhongban();
                    txtMaCanBo.Text = tbl.Rows[0]["MACANBO"] + "";
                    txtHoten.Text = tbl.Rows[0]["HOTEN"] + "";
                    txtDiaChi.Text = tbl.Rows[0]["DIACHI"] + "";
                    txtCMND.Text = tbl.Rows[0]["SOCMND"] + "";
                    txtNgaySinh.Text = tbl.Rows[0]["NGAYSINH"] + "";
                    if (tbl.Rows[0]["GIOITINH"] + "" != "")
                    {
                        ddlGioiTinh.SelectedValue = tbl.Rows[0]["GIOITINH"] + "";
                    }
                    txtDienThoai.Text = tbl.Rows[0]["SODIENTHOAI"] + "";
                    DropChucDanh.SelectedValue = tbl.Rows[0]["CHUCDANHID"] + "";
                    DropChucVu.SelectedValue = tbl.Rows[0]["CHUCVUID"] + "";
                    txtNgayNhanCT.Text = tbl.Rows[0]["NGAYNHANCONGTAC"] + "";
                    ddlTinhtrang.SelectedValue = string.IsNullOrEmpty(tbl.Rows[0]["HIEULUC"] + "") ? "0" : tbl.Rows[0]["HIEULUC"] + "";
                    txtBonhiemTungay.Text = tbl.Rows[0]["NGAYBONHIEM"] + "";
                    txtBonhiemDenngay.Text = tbl.Rows[0]["NGAYKETTHUC"] + "";
                    ddlPhongban.SelectedValue = tbl.Rows[0]["PHONGBANID"] + "";
                }
            }
            catch (Exception) { }
        }
        private void LoadDropDonVi_TrucThuoc()
        {
            DropDV_TRUCTHUOC.Items.Clear();
            COVID_DV_TRUCTHUOC_BL oBL = new COVID_DV_TRUCTHUOC_BL();
            DropDV_TRUCTHUOC.DataSource = oBL.GET_DV_TRUCTHUOC_LIST();
            DropDV_TRUCTHUOC.DataTextField = "TEN_ALIAS";
            DropDV_TRUCTHUOC.DataValueField = "ID";
            DropDV_TRUCTHUOC.DataBind();
        }
        private void LoadPhongban()
        {
            ddlPhongban.Items.Clear();
            COVID_PHONGBAN_BL oBL = new COVID_PHONGBAN_BL();
            ddlPhongban.DataSource = oBL.GET_PHONGBAN_LIST(DropDV_TRUCTHUOC.SelectedValue);
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("---chọn---", ""));
        }
        public void LoadDropChucDanh()
        {
            DropChucDanh.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucDanh = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCDANH");
            if (dmChucDanh != null && dmChucDanh.Rows.Count > 0)
            {
                DropChucDanh.DataSource = dmChucDanh;
                DropChucDanh.DataTextField = "TEN";
                DropChucDanh.DataValueField = "ID";
                DropChucDanh.DataBind();
            }
            DropChucDanh.Items.Insert(0, new ListItem("---chọn---", ""));
        }
        public void LoadDropChucVu()
        {
            DropChucVu.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucVu = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCVU");
            if (dmChucVu != null && dmChucVu.Rows.Count > 0)
            {
                DropChucVu.DataSource = dmChucVu;
                DropChucVu.DataTextField = "TEN";
                DropChucVu.DataValueField = "ID";
                DropChucVu.DataBind();
            }
            DropChucVu.Items.Insert(0, new ListItem("---chọn---", ""));
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            lbthongbao.Text = "";
            decimal cbID = Convert.ToDecimal(hddCbID.Value);
            string MaCanBo = txtMaCanBo.Text.Trim();
            if (!ValidateData(MaCanBo, cbID))
            {
                return;
            }
            COVID_DM_CANBO dmCanBo = new COVID_DM_CANBO();
            dmCanBo.ID = Convert.ToDecimal(hddCbID.Value);
            dmCanBo.TOAANID = 1;
            dmCanBo.MACANBO = MaCanBo;
            dmCanBo.HOTEN = txtHoten.Text.Trim();
            dmCanBo.SOCMND = txtCMND.Text.Trim();
            dmCanBo.HIEULUC = Convert.ToDecimal(ddlTinhtrang.SelectedValue);
            if (DropChucDanh.SelectedValue != "")
                dmCanBo.CHUCDANHID = Convert.ToDecimal(DropChucDanh.SelectedValue);
            if (DropChucVu.SelectedValue != "")
                dmCanBo.CHUCVUID = Convert.ToDecimal(DropChucVu.SelectedValue);
            dmCanBo.DIACHI = txtDiaChi.Text.Trim();
            dmCanBo.NGAYNHANCONGTAC = (String.IsNullOrEmpty(txtNgayNhanCT.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgayNhanCT.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dmCanBo.NGAYSINH = (String.IsNullOrEmpty(txtNgaySinh.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtNgaySinh.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dmCanBo.SODIENTHOAI = txtDienThoai.Text.Trim();
            dmCanBo.NGUOITAO = Session["SESSION_USERNAME"].ToString();
            dmCanBo.NGAYTAO = DateTime.Now;
            dmCanBo.NGAYBONHIEM = (String.IsNullOrEmpty(txtBonhiemTungay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtBonhiemTungay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dmCanBo.NGAYKETTHUC = (String.IsNullOrEmpty(txtBonhiemDenngay.Text.Trim())) ? (DateTime?)null : DateTime.Parse(this.txtBonhiemDenngay.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
            dmCanBo.GIOITINH = Convert.ToDecimal(ddlGioiTinh.SelectedValue);
            if (ddlPhongban.SelectedValue != "")
                dmCanBo.PHONGBANID = Convert.ToDecimal(ddlPhongban.SelectedValue);
            dmCanBo.DV_TRUCTHUOC = Convert.ToDecimal(DropDV_TRUCTHUOC.SelectedValue);
            COVID_DM_CANBO_BL obj_bl = new COVID_DM_CANBO_BL();
            if (obj_bl.COVID_DM_CANBO_INS_UP(dmCanBo) == true)
            {
                if (hddCbID.Value == "0")
                    lbthongbao.Text = "Bạn đã thêm mới thành công.";
                else
                    lbthongbao.Text = "Bạn đã cập nhật thành công.";
            }
            //Response.Redirect("Danhsach.aspx");
        }
        private bool ValidateData(string MaCanBo, decimal CbID)
        {
            int lengthmaCB = MaCanBo.Length;
            if (lengthmaCB == 0 || lengthmaCB > 50)
            {
                lbthongbao.Text = "Mã cán bộ không được trống hoặc quá 50 ký tự. Hãy nhập lại!";
                txtMaCanBo.Focus();
                return false;
            }
            COVID_DM_CANBO_BL obj = new COVID_DM_CANBO_BL();
            DataTable tbl = obj.GET_CANBO_MA(MaCanBo);
            // kiểm tra trùng mã
            if (tbl.Rows.Count != 0)
            {
                if (CbID != Convert.ToDecimal(tbl.Rows[0]["ID"] + ""))
                {
                    lbthongbao.Text = "Mã cán bộ này đã tồn tại. Hãy nhập lại!";
                    txtMaCanBo.Focus();
                    return false;
                }
            }
            //
            int lengthTenCB = txtHoten.Text.Trim().Length;
            if (lengthTenCB == 0 || lengthTenCB > 250)
            {
                lbthongbao.Text = "Tên cán bộ không được trống hoặc quá 250 ký tự. Hãy nhập lại!";
                txtHoten.Focus();
                return false;
            }
            if (txtDiaChi.Text.Trim().Length > 250)
            {
                lbthongbao.Text = "Địa chỉ không được quá 250 ký tự. Hãy nhập lại!";
                txtDiaChi.Focus();
                return false;
            }
            if (txtCMND.Text.Trim().Length > 50)
            {
                lbthongbao.Text = "Số CMND không được quá 50 ký tự. Hãy nhập lại!";
                txtCMND.Focus();
                return false;
            }
            if (txtNgaySinh.Text.Trim() != "" && !Cls_Comon_covid.IsValidDate(txtNgaySinh.Text.Trim()))
            {
                lbthongbao.Text = "Ngày sinh phải là kiểu ngày tháng (dd/MM/yyyy). Hãy nhập lại!";
                txtNgaySinh.Focus();
                return false;
            }
            if (txtDienThoai.Text.Trim().Length > 50)
            {
                lbthongbao.Text = "Số điện thoại không được quá 50 ký tự. Hãy nhập lại!";
                txtDienThoai.Focus();
                return false;
            }
            if (txtNgayNhanCT.Text.Trim() != "" && !Cls_Comon_covid.IsValidDate(txtNgayNhanCT.Text.Trim()))
            {
                lbthongbao.Text = "Ngày nhận công tác phải là kiểu ngày tháng (dd/MM/yyyy). Hãy nhập lại!";
                txtNgayNhanCT.Focus();
                return false;
            }
            //if (txtBonhiemTungay.Text.Trim() == "" && !Cls_Comon_covid.IsValidDate(txtBonhiemTungay.Text.Trim()))
            //{
            //    lbthongbao.Text = "Ngày bắt đầu bổ nhiệm chưa nhập hoặc không hợp lệ !";
            //    txtBonhiemTungay.Focus();
            //    return false;
            //}
            //if (txtBonhiemDenngay.Text.Trim() == "" && !Cls_Comon_covid.IsValidDate(txtBonhiemDenngay.Text.Trim()))
            //{
            //    lbthongbao.Text = "Ngày kết thúc bổ nhiệm chưa nhập hoặc không hợp lệ !";
            //    txtBonhiemDenngay.Focus();
            //    return false;
            //}
            return true;
        }
        protected void lblquaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsach_bep_an.aspx");
        }
        protected void DV_TRUCTHUOC_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPhongban();
        }
    }
}