﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dangkybep_an.Master" AutoEventWireup="true" CodeBehind="Danhsach_bep_an.aspx.cs" Inherits="WEB.ZCOVID.Danhmuc.Canbo.Danhsach_bep_an" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="hddPageSize" Value="20" runat="server" />
    <style type="text/css">
        .labelTimKiem_css {
            width: 65px;
        }

        .sobanghi, .sobanghi b {
            font-size: 14px;
        }
    </style>

    <div class="content_form">
        <div class="content_body">
            <div class="content_form_head">
                <div class="content_form_head_title">Danh sách cán bộ</div>
                <div class="content_form_head_right"></div>
            </div>
            <div class="content_form_body">
                <table class="table1" style="width: 100%">
                    <tr>
                        <td style="width: 100px;"><b>Đơn vị</b></td>
                        <td>
                            <asp:DropDownList CssClass="chosen-select" ID="DropDV_TRUCTHUOC" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="DV_TRUCTHUOC_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td style="width: 130px;"><b>Phòng ban</b></td>
                        <td>
                            <asp:DropDownList CssClass="chosen-select" ID="ddlPhongban" runat="server" Width="300px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="height: 40px;">
                        <td class="labelTimKiem_css"><b>Chức danh</b></td>
                        <td style="width: 310px;">
                            <asp:DropDownList CssClass="chosen-select" ID="DropChucDanh" runat="server" Width="300px"></asp:DropDownList>
                        </td>
                        <td class="labelTimKiem_css"><b>Chức vụ</b></td>
                        <td>
                            <asp:DropDownList CssClass="chosen-select" ID="DropChucVu" runat="server" Width="300px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Từ khóa</b></td>
                        <td>
                            <asp:TextBox ID="txKey" CssClass="user" runat="server" Width="292px" placeholder="Mã, tên, địa chỉ cán bộ"></asp:TextBox>
                        </td>
                        <td><b>Trạng thái công tác</b></td>
                        <td>
                            <asp:DropDownList CssClass="chosen-select" ID="DropTrangThaiCongTac" runat="server" Width="300px">
                                <asp:ListItem Text="--- Tất cả ---" Value=""></asp:ListItem>
                                <asp:ListItem Text="Đang công tác" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Nghỉ phép" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Nghỉ công tác" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dừng xét xử" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: left;">
                            <div style="margin-left: 105px; margin-top: 15px; margin-bottom: 15px">
                                <asp:Label runat="server" ID="lbtthongbao" ForeColor="Red" Font-Size="16px"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:Button ID="cmdTimkiem" runat="server" CssClass="buttoninput" Text="Tìm kiếm" OnClick="lbtimkiem_Click" />
                            <asp:Button ID="cmdThemmoi" runat="server" CssClass="buttoninput" Text="Thêm mới" OnClick="btnThemmoi_Click" />
                            <asp:Button ID="cmdLamMoi" runat="server" CssClass="buttoninput" Text="Làm mới" OnClick="cmdLamMoi_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="phantrang">
                                <div class="sobanghi">
                                    <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                                </div>
                                <div class="sotrang">
                                    <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                        OnClick="lbTBack_Click"><</asp:LinkButton>
                                    <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                        Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                    <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                    <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                        Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                        Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                        Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                        Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                    <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                        Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                        OnClick="lbTNext_Click">></asp:LinkButton>
                                    <asp:DropDownList ID="dropPageSize" runat="server" Width="65px"
                                        CssClass="dropbox"
                                        AutoPostBack="True" OnSelectedIndexChanged="dropPageSize_SelectedIndexChanged">
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                        <asp:ListItem Value="50" Text="50" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                        <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                AllowPaging="true" GridLines="None" PagerStyle-Mode="NumericPages"
                                CssClass="table2" HeaderStyle-CssClass="header" AlternatingItemStyle-CssClass="le"
                                ItemStyle-CssClass="chan" Width="100%"
                                OnItemCommand="dgList_ItemCommand" OnItemDataBound="dgList_ItemDataBound">
                                <Columns>
                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn ItemStyle-Width="40px" HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            STT
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Container.ItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="70px" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Mã cán bộ
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("MACANBO")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="150px" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            Tên cán bộ
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("HOTEN")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Chức danh
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("CHUCDANH")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="120px" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Chức vụ
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("CHUCVU")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="120px"></HeaderStyle>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="220px" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Đơn vị
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("DONVI_TEN")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="120px"></HeaderStyle>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="220px" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Phòng ban
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("TENPHONGBAN")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="120px"></HeaderStyle>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="100px" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Trạng thái
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("TRANGTHAI")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="60px" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Người tạo
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("NGUOITAO")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="60px"></HeaderStyle>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="60px" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Người sửa
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("NGUOISUA")%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="60px"></HeaderStyle>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="80px" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Thao tác
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtSua" runat="server" Text="Sửa" ForeColor="#0e7eee" CausesValidation="false" CommandName="Sua"
                                                CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                            &nbsp;&nbsp;
                                            <asp:LinkButton ID="lbtXoa" runat="server" ForeColor="#0e7eee" CausesValidation="false" Text="Xóa"
                                                CommandName="Xoa" CommandArgument='<%#Eval("ID") %>' ToolTip="Xóa" OnClientClick="return confirm('Bạn thực sự muốn xóa cán bộ này? ');"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" Visible="false"></PagerStyle>
                                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            </asp:DataGrid>
                            <div class="phantrang">
                                <div class="sobanghi">
                                    <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                                </div>
                                <div class="sotrang">
                                    <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                        OnClick="lbTBack_Click"><</asp:LinkButton>
                                    <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                        Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                    <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                    <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                        Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                        Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                        Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                        Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                    <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                    <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                        Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                        OnClick="lbTNext_Click">></asp:LinkButton>
                                    <asp:DropDownList ID="dropPageSize2" runat="server" Width="65px" CssClass="dropbox"
                                        AutoPostBack="True" OnSelectedIndexChanged="dropPageSize2_SelectedIndexChanged">
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                        <asp:ListItem Value="50" Text="50" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                        <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>
