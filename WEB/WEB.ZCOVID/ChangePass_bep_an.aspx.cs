﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;
using System.Data;

namespace WEB.ZCOVID
{
    public partial class ChangePass_bep_an : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string strUserID = Session["SESSION_USERID"] + "";
                    if (strUserID == "") Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login_bep_an.aspx");
                    txtUserName.Text = Session["SESSION_USERNAME"] + "";
                    decimal current_id = Convert.ToDecimal(Session["SESSION_USERID"] + "");
                }
                catch (Exception ex)
                { lttMsg.Text = ex.Message; }

            }
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login_bep_an.aspx");
        }
        protected void cmdLogIn_Click(object sender, ImageClickEventArgs e)
        {
            if (txtPass_new.Text.Trim() == "")
            {
                lttMsg.Text = "Chưa nhập mật khẩu mới !";
                return;
            }
            if (txtPass_new.Text.Length < 6)
            {
                lttMsg.Text = "Mật khẩu phải trên 6 ký tự !";
                return;
            }
            if (txtPass_new.Text != txtRePass.Text)
            {
                lttMsg.Text = "Nhập lại mật khẩu không đúng !";
                return;
            }
            COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
            if (obj.Users_Change_Password(Session["SESSION_USERNAME"] + "", txtPass_new.Text) == true)
            {
                lttMsg.Text = "Đổi mật khẩu thành công !";
            }
        }
    }
}