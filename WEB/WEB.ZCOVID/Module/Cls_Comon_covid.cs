﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Oracle.ManagedDataAccess.Client;


namespace WEB.ZCOVID.Module
{
    public class Cls_Comon_covid
    {
        public static OracleConnection Connection()
        {
            OracleConnection conn = new OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
            conn.Open();
            return conn;
        }
        public static DataTable GetTableByProcedurePaging(string procedure_name, OracleParameter[] parameters)
        {
            OracleConnection conn = Connection();
            OracleCommand cmd = new OracleCommand(procedure_name, conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddRange(parameters);

            DataTable tbl = new DataTable();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(tbl);
            conn.Close();
            conn.Dispose();
            return tbl;
        }
        public static string MD5Encrypt(string strValue)
        {
            byte[] data, output;
            UTF8Encoding encoder = new UTF8Encoding();
            MD5CryptoServiceProvider hasher = new MD5CryptoServiceProvider();
            data = encoder.GetBytes(strValue);
            output = hasher.ComputeHash(data);
            return BitConverter.ToString(output).Replace("-", "").ToLower();
        }
        public static string GetRootURL()
        {
            Uri path = HttpContext.Current.Request.Url;
            string strHttps = "http://";
            if (path.ToString().ToLower().Contains("https:"))
                strHttps = "https://";
            return strHttps + HttpContext.Current.Request.Url.Authority.ToString();
        }
        public static void SetFocus(System.Web.UI.Control control, Type type, String ControlClientID)
        {
            //System.Web.UI.ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), "Setfocus('" + strConvtrolid + "');", true);
            System.Web.UI.ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), "document.getElementById('" + ControlClientID + "').focus();", true);

        }
        public static bool SetPageButton(HiddenField hddTotalPage, HiddenField hddPageIndex, LinkButton lbTFirst, LinkButton lbBFirst, LinkButton lbTLast, LinkButton lbBLast,
                                     LinkButton lbTNext, LinkButton lbBNext, LinkButton lbTBack, LinkButton lbBBack,
                                     Label lbTStep1, Label lbBStep1, LinkButton lbTStep2, LinkButton lbBStep2,
                                     LinkButton lbTStep3, LinkButton lbBStep3, LinkButton lbTStep4, LinkButton lbBStep4,
                                     LinkButton lbTStep5, LinkButton lbBStep5, Label lbTStep6, Label lbBStep6)
        {
            int total_page = Convert.ToInt32(hddTotalPage.Value);
            int current_page = Convert.ToInt32(hddPageIndex.Value);
            lbTLast.Text = lbBLast.Text = total_page.ToString();
            lbTStep1.Visible = lbBStep1.Visible = lbTStep6.Visible = lbBStep6.Visible = false;
            lbTLast.Visible = lbBLast.Visible = lbTBack.Visible = lbBBack.Visible = lbTNext.Visible = lbBNext.Visible = false;
            lbTStep2.Visible = lbTStep3.Visible = lbTStep4.Visible = lbTStep5.Visible = false;
            lbBStep2.Visible = lbBStep3.Visible = lbBStep4.Visible = lbBStep5.Visible = false;
            lbTFirst.CssClass = lbBFirst.CssClass = lbTLast.CssClass = lbBLast.CssClass = lbTStep2.CssClass = lbBStep2.CssClass = lbTStep3.CssClass = lbBStep3.CssClass = lbTStep4.CssClass = lbBStep4.CssClass = lbTStep5.CssClass = lbBStep5.CssClass = lbTStep6.CssClass = lbBStep6.CssClass = "so";
            if (current_page == 1)
            {
                lbTFirst.CssClass = lbBFirst.CssClass = "active";
                lbTFirst.Visible = lbBFirst.Visible = true;
                lbTNext.Visible = lbBNext.Visible = true;
            }
            else if (current_page == total_page)
            {
                lbTLast.CssClass = lbBLast.CssClass = "active";
                lbTLast.Visible = lbBLast.Visible = true;
                lbTBack.Visible = lbBBack.Visible = true;
            }
            if (total_page == 1)
            {
                lbTFirst.CssClass = lbBFirst.CssClass = "active";
                lbTLast.Visible = lbBLast.Visible = false;
                lbTBack.Visible = lbBBack.Visible = false;
                lbTNext.Visible = lbBNext.Visible = false;
                return true;
            }
            else if (total_page < 7)
            {
                lbTStep2.Text = lbBStep2.Text = "2";
                lbTStep3.Text = lbBStep3.Text = "3";
                lbTStep4.Text = lbBStep4.Text = "4";
                lbTStep5.Text = lbBStep5.Text = "5";
                lbTLast.Visible = lbBLast.Visible = true;
                if (current_page > 1 && current_page < total_page)
                {
                    lbTNext.Visible = lbBNext.Visible = lbTBack.Visible = lbBBack.Visible = true;
                }

                if (total_page == 3)
                {
                    lbTStep2.Visible = lbBStep2.Visible = true;
                    if (current_page == 2) lbTStep2.CssClass = lbBStep2.CssClass = "active";
                }
                else if (total_page == 4)
                {
                    lbTStep2.Visible = lbBStep2.Visible = lbTStep3.Visible = lbBStep3.Visible = true;
                    if (current_page == 2) lbTStep2.CssClass = lbBStep2.CssClass = "active";
                    if (current_page == 3) lbTStep3.CssClass = lbBStep3.CssClass = "active";
                }
                else if (total_page == 5)
                {
                    lbTStep2.Visible = lbBStep2.Visible = lbTStep3.Visible = lbBStep3.Visible = lbTStep4.Visible = lbBStep4.Visible = true;
                    if (current_page == 2) lbTStep2.CssClass = lbBStep2.CssClass = "active";
                    if (current_page == 3) lbTStep3.CssClass = lbBStep3.CssClass = "active";
                    if (current_page == 4) lbTStep4.CssClass = lbBStep4.CssClass = "active";
                }
                else if (total_page == 6)
                {
                    lbTStep2.Visible = lbBStep2.Visible = lbTStep3.Visible = lbBStep3.Visible = lbTStep4.Visible = lbBStep4.Visible = lbTStep5.Visible = lbBStep5.Visible = true;
                    if (current_page == 2) lbTStep2.CssClass = lbBStep2.CssClass = "active";
                    if (current_page == 3) lbTStep3.CssClass = lbBStep3.CssClass = "active";
                    if (current_page == 4) lbTStep4.CssClass = lbBStep4.CssClass = "active";
                    if (current_page == 5) lbTStep5.CssClass = lbBStep5.CssClass = "active";
                }
            }
            else
            {
                lbTLast.Visible = lbBLast.Visible = true;
                lbTStep2.Visible = lbBStep2.Visible = lbTStep3.Visible = lbBStep3.Visible = true;
                lbTStep4.Visible = lbBStep4.Visible = lbTStep5.Visible = lbBStep5.Visible = true;
                if (current_page > 1 && current_page < total_page)
                {
                    lbTNext.Visible = lbBNext.Visible = lbTBack.Visible = lbBBack.Visible = true;
                }
                if (current_page > 4)
                {
                    lbTStep1.Visible = lbBStep1.Visible = true;
                }
                if (current_page < total_page - 3)
                {
                    lbTStep6.Visible = lbBStep6.Visible = true;
                }
                if (current_page <= 4)
                {
                    lbTStep2.Text = lbBStep2.Text = "2";
                    lbTStep3.Text = lbBStep3.Text = "3";
                    lbTStep4.Text = lbBStep4.Text = "4";
                    lbTStep5.Text = lbBStep5.Text = "5";
                    if (current_page == 2)
                        lbTStep2.CssClass = lbBStep2.CssClass = "active";
                    else if (current_page == 3)
                        lbTStep3.CssClass = lbBStep3.CssClass = "active";
                    if (current_page == 4)
                        lbTStep4.CssClass = lbBStep4.CssClass = "active";
                }
                if (current_page >= total_page - 3)
                {
                    lbTStep2.Text = lbBStep2.Text = (total_page - 4).ToString();
                    lbTStep3.Text = lbBStep3.Text = (total_page - 3).ToString();
                    lbTStep4.Text = lbBStep4.Text = (total_page - 2).ToString();
                    lbTStep5.Text = lbBStep5.Text = (total_page - 1).ToString();
                    if (current_page == total_page - 3)
                        lbTStep3.CssClass = lbBStep3.CssClass = "active";
                    else if (current_page == total_page - 2)
                        lbTStep4.CssClass = lbBStep4.CssClass = "active";
                    if (current_page == total_page - 1)
                        lbTStep5.CssClass = lbBStep5.CssClass = "active";
                }
                if (current_page > 4 && current_page < total_page - 3)
                {
                    lbTStep2.Text = lbBStep2.Text = (current_page - 2).ToString();
                    lbTStep3.Text = lbBStep3.Text = (current_page - 1).ToString();
                    lbTStep4.Text = lbBStep4.Text = (current_page).ToString();
                    lbTStep5.Text = lbBStep5.Text = (current_page + 1).ToString();
                    lbTStep4.CssClass = lbBStep4.CssClass = "active";
                }
            }

            return true;
        }
        public static int GetTotalPage(int intCount, int intPageSize)
        {
            int phan_du = intCount % intPageSize;
            int phan_nguyen = intCount / intPageSize;
            if (phan_du > 0)
                return (phan_nguyen + 1);
            else
            {
                if (phan_nguyen > 0)
                    return phan_nguyen;
                else
                    return (phan_nguyen + 1);
            }
        }
        public static Int64 GetTotalPage(Int64 TotalItem, Int16 PageSize)
        {
            Int64 phan_du = TotalItem % PageSize;
            Int64 phan_nguyen = TotalItem / PageSize;
            if (phan_du > 0)
                return (phan_nguyen + 1);
            else
            {
                if (phan_nguyen > 0)
                    return phan_nguyen;
                else
                    return (phan_nguyen + 1);
            }
        }
        public static bool IsValidDate(string strValue)
        {
            DateTime d;
            CultureInfo cul = new CultureInfo("vi-VN");
            try
            {
                d = Convert.ToDateTime(strValue, cul);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static List<T> Bind_List_Reader<T>(OracleCommand command) where T : class
        {
            List<T> collection = new List<T>();
            OracleDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    T item = Get_Item_From_Reader<T>(reader);
                    collection.Add(item);
                }
                return collection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
            }
        }
        private static T Get_Item_From_Reader<T>(OracleDataReader rdr) where T : class
        {
            Type type = typeof(T);
            T item = Activator.CreateInstance<T>();
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (rdr[property.Name].ToString().Trim() != string.Empty)
                {
                    property.SetValue(item, rdr[property.Name], null);
                }
            }
            return item;
        }
    }
}