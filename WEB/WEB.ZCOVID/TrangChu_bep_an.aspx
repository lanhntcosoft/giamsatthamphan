﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dangkybep_an.Master" AutoEventWireup="true" CodeBehind="TrangChu_bep_an.aspx.cs" Inherits="WEB.ZCOVID.TrangChu_bep_an" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="/UI/css/TAble.css" rel="stylesheet" />
    <style>
        .ajax__calendar_container {
            width: 180px;
        }

        .ajax__calendar_body {
            width: 100%;
            height: 145px;
        }

        .panding_left li span {
            padding-left: 40px;
        }

        .thongke li {
            line-height: 22px;
        }
    </style>
    <script src="UI/js/Common.js"></script>
    <div class="content_form" style="margin-bottom: 30px;">
        <div class="leftzone">
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Văn bản chỉ đạo điều hành</span></div>
                <div class="leftmenu_content">
                    <ul class="thongke">
                        <li><a href="http://www.toaan.gov.vn/webcenter/portal/tatc/chi-tiet-chi-dao-dieu-hanh?dDocName=TAND110877" target="_blank"><span style="color: #cf2b0e;">new*</span>Công văn số 32/TANDTC-TH ngày 18/3/2020</a></li>
                    </ul>
                </div>
            </div>
            <div class="leftmenu" style="display: none">
                <div class="leftmenu_header arrow"><span>Thống kê nhanh</span></div>
                <div class="leftmenu_content">
                    <asp:Literal ID="Li_THONGKE_F" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="leftmenu" style="display: none">
                <div class="leftmenu_header arrow"><span>Danh sách các trường hợp F0 - F4</span></div>
                <div class="leftmenu_content">
                    <asp:Literal ID="Li_THONGKE_F_HOTEN" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Lưu ý</span></div>
                <div class="leftmenu_content">
                    <ul class="thongke" style="color: #e01212; text-align: left;">
                        <li><span style="font-size: 16px;">1. Cập nhật thông tin trước 17 giờ hàng ngày, sau 17 giờ hệ thống sẽ không được 
                            thêm, sửa thông tin đăng ký.
                        </span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="rightzone">
            <div class="content_body">
                <div class="content_form_head" style="">
                    <div class="content_form_head_title">
                        <div style="">ĐĂNG KÝ</div>
                    </div>
                    <div class="content_form_head_right"></div>
                </div>
                <div class="content_form_body">
                    <div class="searchcontent" style="display: none;">
                        <h4 class="tracuu_tleboxchung" style="background-color: transparent;"></h4>
                        <div class="border_search" style="background-color: #f7eba9;">
                            <div class="contai_canhbao" style="float: left; width: 100%; line-height: 28px;">
                                <%--<span style="color: #cf2b0e;">+ Các cán bộ công chức bắt đầu nhập từ thứ 2 ngày 23/03/2020.</span><br />--%>
                                <span runat="server" id="canhbao_ld" style="color: #cf2b0e;">+ Ngày hôm nay đã có 
                                       <asp:LinkButton ID="Link_da_KBYT" runat="server" OnClick="Link_da_KBYT_OnClick"></asp:LinkButton>
                                    người thực hiện khai báo y tế,
                                       <asp:LinkButton ID="Link_chua_KBYT" runat="server" OnClick="Link_chua_KBYT_OnClick"></asp:LinkButton>
                                    người chưa thực hiện khai báo y tế trong đơn vị của bạn.
                                   (nhấn vào chữ số để xem danh sách)
                                     <br />
                                </span>
                                <style>
                                    .contai_canhbao span, .contai_canhbao a {
                                        font-size: 15px;
                                        font-style: italic;
                                    }
                                </style>
                                <asp:Literal ID="Li_TK_CANHBAO" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Thông tin người đăng ký</h4>
                        <div class="border_search">
                            <div style="float: left; width: 700px; font-size: 16px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Họ và tên</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txt_HOTEN" Text="Vũ Hoàng Anh" CssClass="user" runat="server" Enabled="false" Width="210px"></asp:TextBox>
                                </div>
                                <div style="width: 50px; float: left; font-size: 14px; margin-left: 30px;">Đơn vị</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txt_DV_TRUCTHUOC_TEN" CssClass="user" runat="server" Width="210px" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Điện thoại<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txtDienThoai" CssClass="user" runat="server" Width="210px" MaxLength="50"></asp:TextBox>
                                </div>
                                <div style="width: 50px; float: left; font-size: 14px; margin-left: 30px;">Email</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txtEmail" CssClass="user" runat="server" Width="210px" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Nơi ở hiện tại<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 517px; float: left; margin-left: 10px;">
                                    <asp:DropDownList CssClass="chosen-select" ID="Drop_DonVi" runat="server" Width="517px"></asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Địa chỉ chi tiết<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 510px; float: left; margin-left: 10px;">
                                    <asp:TextBox ID="txt_DIACHI" CssClass="user" runat="server" TextMode="MultiLine" Rows="1" Width="510px"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Đăng ký</h4>
                        <div class="border_search">
                            <style>
                                @media screen and (min-width: 960px) {
                                    .column_auto {
                                        float: left;
                                        width: 800px;
                                    }

                                    .ghichu_css {
                                        width: 700px;
                                    }

                                    .margin_auto {
                                        margin-top: unset;
                                    }

                                    .margin_left_auto {
                                        margin-left: 190px;
                                    }
                                }

                                @media screen and (max-width: 992px) {
                                    .column_auto {
                                        float: left;
                                        width: 400px;
                                    }

                                    .ghichu_css {
                                        width: 500px;
                                    }

                                    .margin_auto {
                                        margin-top: 15px;
                                    }

                                    .margin_left_auto {
                                        margin-left: 0px;
                                    }
                                }

                                @media screen and (max-width: 768px) {
                                    .column_auto {
                                        float: left;
                                        width: 400px;
                                    }

                                    .ghichu_css {
                                        width: 400px;
                                    }

                                    .margin_auto {
                                        margin-top: 15px;
                                    }

                                    .margin_left_auto {
                                        margin-left: 0px;
                                    }
                                }
                            </style>
                            <div class="column_auto">
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="float: left;margin-top: 5px;">Chọn lịch
                                    </div>
                                    <div style="float: left; margin-left: 7px;">
                                        <asp:DropDownList CssClass="chosen-select" ID="Drop_Loai" runat="server" Width="150px">
                                            <asp:ListItem Text="Theo tháng" Value="tháng" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Theo ngày" Value="Ngày"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; width: 800px; margin-top: 20px;">
                                <div style="float: left; margin-left: 5px;">
                                    <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput" Width="100px" Text="Gửi" OnClick="btnUpdate_Click" OnClientClick="return ValidateDataInput();" />
                                </div>
                                <div style="float: left; margin-left: 5px;">
                                    <asp:Button ID="cmdLammoi" runat="server" CssClass="buttoninput" Width="100px" Text="Làm mới" OnClick="btnLammoi_Click" />
                                </div>
                                <div style="float: left; margin-left: 25px; padding-top: 5px;">
                                    <asp:Label runat="server" ID="lbthongbao" Font-Size="17px" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Danh sách đăng ký</h4>
                        <div class="border_search">
                            <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
                            <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
                            <asp:Panel ID="pnDS" runat="server" Visible="false">
                                <div class="phantrang" style="margin-top: 30px;">
                                    <div class="sobanghi">
                                        <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"><</asp:LinkButton>
                                        <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click">></asp:LinkButton>
                                        <asp:DropDownList ID="dropPageSize" runat="server" Width="65px"
                                            CssClass="dropbox"
                                            AutoPostBack="True" OnSelectedIndexChanged="dropPageSize_SelectedIndexChanged">
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                            <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                            <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div>
                                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand" Visible="true" OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <div class="CSSTableGenerator">
                                                <table>
                                                    <tr id="row_header">
                                                        <td style="width: 20px;">TT</td>
                                                        <td style="width: 80px;">Ngày gửi</td>
                                                        <td style="width: 60px;">Sốt >=37.5 độ C</td>
                                                        <td style="width: 90px; text-align: center;">Tiếp xúc gần với người mắc bệnh hoặc nghi ngờ mắc bệnh viêm đường hô hấp do nCoV</td>
                                                        <td style="width: 80px;">Thời gian cách ly</td>
                                                        <td style="width: 100px;">Ghi chú</td>
                                                        <td style="width: 50px;">Tài khoản gửi</td>
                                                    </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;"><%#Eval("STT") %> </td>
                                                <td style="text-align: center;"><%#Eval("NGAYSUA") %> </td>
                                                <td style="text-align: center;"><%#Eval("SOT") %> </td>
                                                <td style="text-align: center;"><%#Eval("TIEPXUCGAN_BN") %> </td>
                                                <td style="text-align: center;"><%#Eval("TIME_CACHLY") %> </td>
                                                <td style="text-align: center;"><%#Eval("GHICHU") %> </td>
                                                <td style="text-align: center;"><%#Eval("NGUOITAO") %> </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="phantrang">
                                    <div class="sobanghi">
                                        <asp:HiddenField ID="hdicha" runat="server" />
                                        <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"><</asp:LinkButton>
                                        <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click">></asp:LinkButton>
                                        <asp:DropDownList ID="dropPageSize2" runat="server" Width="65px" CssClass="dropbox"
                                            AutoPostBack="True" OnSelectedIndexChanged="dropPageSize2_SelectedIndexChanged">
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                            <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                            <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <%-- content_form_body end--%>
            </div>
        </div>
    </div>
    <style>
        .sobanghi {
            font-size: 11pt;
        }
    </style>
    <script>
        function ValidateDataInput() {
            var txtDienThoai = document.getElementById('<%=txtDienThoai.ClientID %>');
            if (txtDienThoai.value.trim().length > 50) {
                alert('Điện thoại không được quá 50 ký tự. Hãy nhập lại!');
                txtDienThoai.focus();
                return false;
            }
            var txtEmail = document.getElementById('<%=txtEmail.ClientID %>');
            if (txtEmail.value.trim().length > 150) {
                alert('Email không được quá 150 ký tự. Hãy nhập lại!');
                txtEmail.focus();
                return false;
            }
            if (Common_CheckEmpty(txtEmail.value)) {
                if (!Common_ValidateEmail(txtEmail.value)) {
                    txtEmail.focus();
                    return false;
                }
            }
            var Drop_DonVi = document.getElementById('<%= Drop_DonVi.ClientID %>');
            if (!Common_CheckEmpty(Drop_DonVi.value)) {
                alert('Bạn chưa chọn mục "Nơi ở hiện tại(*)"!');
                Drop_DonVi.focus();
                return false;
            }
            var txt_DIACHI = document.getElementById('<%=txt_DIACHI.ClientID%>');
            if (!Common_CheckEmpty(txt_DIACHI.value)) {
                alert('Bạn chưa nhập mục "Địa chỉ chi tiết(*)"!');
                txt_DIACHI.focus();
                return false;
            }
            return true;
        }
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>
