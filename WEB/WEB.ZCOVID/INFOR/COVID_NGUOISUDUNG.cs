﻿using System;
using System.Collections.Generic;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;


namespace WEB.ZCOVID.INFOR
{
    public class COVID_NGUOISUDUNG
   {
        #region Private Member variables 
        private Decimal _ID;
        private String _USERNAME;
        private String _PASSWORD;
        private String _HOTEN;
        private Decimal _DONVIID;
        private Decimal _CANBOID;
        private Decimal _HIEULUC;
        private String _DIENTHOAI;
        private String _EMAIL;
        private String _GHICHU;
        private String _NGUOITAO;
        private DateTime _NGAYTAO;
        private String _NGUOISUA;
        private DateTime _NGAYSUA;
        private Decimal _NOT_IN_PMQLCB;
        private Decimal _NHOM_ND_ID;
        private Decimal _DV_TRUCTHUOC;
        private Decimal _PHONGBANID;
        private String _TENDONVI;
        private String _TENNHOMNSD;
        private String _LOAIUSER;
        private String _TENPHONGBAN;
        #endregion
        #region Public properties  
        public Decimal ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        public String USERNAME
        {
            get
            {
                return _USERNAME;
            }
            set
            {
                _USERNAME = value;
            }
        }
        public String PASSWORD
        {
            get
            {
                return _PASSWORD;
            }
            set
            {
                _PASSWORD = value;
            }
        }
        public String HOTEN
        {
            get
            {
                return _HOTEN;
            }
            set
            {
                _HOTEN = value;
            }
        }
        public Decimal DONVIID
        {
            get
            {
                return _DONVIID;
            }
            set
            {
                _DONVIID = value;
            }
        }
        public Decimal CANBOID
        {
            get
            {
                return _CANBOID;
            }
            set
            {
                _CANBOID = value;
            }
        }
        public Decimal HIEULUC
        {
            get
            {
                return _HIEULUC;
            }
            set
            {
                _HIEULUC = value;
            }
        }
        public String DIENTHOAI
        {
            get
            {
                return _DIENTHOAI;
            }
            set
            {
                _DIENTHOAI = value;
            }
        }
        public String EMAIL
        {
            get
            {
                return _EMAIL;
            }
            set
            {
                _EMAIL = value;
            }
        }
        public String GHICHU
        {
            get
            {
                return _GHICHU;
            }
            set
            {
                _GHICHU = value;
            }
        }
        public String NGUOITAO
        {
            get
            {
                return _NGUOITAO;
            }
            set
            {
                _NGUOITAO = value;
            }
        }
        public DateTime NGAYTAO
        {
            get
            {
                return _NGAYTAO;
            }
            set
            {
                _NGAYTAO = value;
            }
        }
        public String NGUOISUA
        {
            get
            {
                return _NGUOISUA;
            }
            set
            {
                _NGUOISUA = value;
            }
        }
        public DateTime NGAYSUA
        {
            get
            {
                return _NGAYSUA;
            }
            set
            {
                _NGAYSUA = value;
            }
        }
        public Decimal NOT_IN_PMQLCB
        {
            get
            {
                return _NOT_IN_PMQLCB;
            }
            set
            {
                _NOT_IN_PMQLCB = value;
            }
        }
        public Decimal NHOM_ND_ID
        {
            get
            {
                return _NHOM_ND_ID;
            }
            set
            {
                _NHOM_ND_ID = value;
            }
        }
        public Decimal DV_TRUCTHUOC
        {
            get
            {
                return _DV_TRUCTHUOC;
            }
            set
            {
                _DV_TRUCTHUOC = value;
            }
        }
        public Decimal PHONGBANID
        {
            get
            {
                return _PHONGBANID;
            }
            set
            {
                _PHONGBANID = value;
            }
        }
        public String TENDONVI
        {
            get
            {
                return _TENDONVI;
            }
            set
            {
                _TENDONVI = value;
            }
        }
        public String TENNHOMNSD
        {
            get
            {
                return _TENNHOMNSD;
            }
            set
            {
                _TENNHOMNSD = value;
            }
        }
        public String LOAIUSER
        {
            get
            {
                return _LOAIUSER;
            }
            set
            {
                _LOAIUSER = value;
            }
        }
        public String TENPHONGBAN
        {
            get
            {
                return _TENPHONGBAN;
            }
            set
            {
                _TENPHONGBAN = value;
            }
        }
        #endregion
        #region Constructors
        public COVID_NGUOISUDUNG()
        {

        }
        public COVID_NGUOISUDUNG(Decimal ID, String USERNAME, String PASSWORD, String HOTEN, Decimal DONVIID, Decimal CANBOID, Decimal HIEULUC, String DIENTHOAI, String EMAIL, String GHICHU, String NGUOITAO, DateTime NGAYTAO, String NGUOISUA, DateTime NGAYSUA, Decimal NOT_IN_PMQLCB, Decimal NHOM_ND_ID, Decimal DV_TRUCTHUOC, Decimal PHONGBANID, String TENDONVI, String TENNHOMNSD, String LOAIUSER, String TENPHONGBAN)
        {
            _ID = ID;
            _USERNAME = USERNAME;
            _PASSWORD = PASSWORD;
            _HOTEN = HOTEN;
            _DONVIID = DONVIID;
            _CANBOID = CANBOID;
            _HIEULUC = HIEULUC;
            _DIENTHOAI = DIENTHOAI;
            _EMAIL = EMAIL;
            _GHICHU = GHICHU;
            _NGUOITAO = NGUOITAO;
            _NGAYTAO = NGAYTAO;
            _NGUOISUA = NGUOISUA;
            _NGAYSUA = NGAYSUA;
            _NOT_IN_PMQLCB = NOT_IN_PMQLCB;
            _NHOM_ND_ID = NHOM_ND_ID;
            _DV_TRUCTHUOC = DV_TRUCTHUOC;
            _PHONGBANID = PHONGBANID;
            _TENDONVI = TENDONVI;
            _TENNHOMNSD = TENNHOMNSD;
            _LOAIUSER = LOAIUSER;
            _TENPHONGBAN = TENPHONGBAN;
        }
        #endregion
    }
}