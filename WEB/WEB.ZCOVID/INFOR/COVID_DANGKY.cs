﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.ZCOVID.INFOR
{
    public class COVID_DANGKY
    {
        #region Private Member variables 
        private Decimal _ID;
        private Decimal _CANBOID;
        private Decimal _TOAANID;
        private Decimal _DV_TRUCTHUOC;
        private Decimal _PHONGBANID;
        private Decimal _SOT;
        private Decimal _HO;
        private Decimal _KHO_THO;
        private Decimal _DAU_HONG;
        private Decimal _NON_BUON_NON;
        private Decimal _TIEU_CHAY;
        private Decimal _XUAT_HUYET_ND;
        private Decimal _NOIBAN_ND;
        private Decimal _TIEPXUCGAN_BN;
        private String  _XACDINH_F;
        private String  _TT_CACHLY;
        private DateTime _CACHLY_TUNGAY;
        private DateTime _CACHLY_DENGAY;
        private String  _LICHSU;
        private String  _GHICHU;
        private String  _NGUOITAO;
        private DateTime _NGAYTAO;
        private String  _NGUOISUA;
        private DateTime _NGAYSUA;
        #endregion
        #region Public properties  
        public Decimal ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        public Decimal CANBOID
        {
            get
            {
                return _CANBOID;
            }
            set
            {
                _CANBOID = value;
            }
        }
        public Decimal TOAANID
        {
            get
            {
                return _TOAANID;
            }
            set
            {
                _TOAANID = value;
            }
        }
        public Decimal DV_TRUCTHUOC
        {
            get
            {
                return _DV_TRUCTHUOC;
            }
            set
            {
                _DV_TRUCTHUOC = value;
            }
        }
        public Decimal PHONGBANID
        {
            get
            {
                return _PHONGBANID;
            }
            set
            {
                _PHONGBANID = value;
            }
        }
        public Decimal SOT
        {
            get
            {
                return _SOT;
            }
            set
            {
                _SOT = value;
            }
        }
        public Decimal HO
        {
            get
            {
                return _HO;
            }
            set
            {
                _HO = value;
            }
        }
        public Decimal KHO_THO
        {
            get
            {
                return _KHO_THO;
            }
            set
            {
                _KHO_THO = value;
            }
        }
        public Decimal DAU_HONG
        {
            get
            {
                return _DAU_HONG;
            }
            set
            {
                _DAU_HONG = value;
            }
        }
        public Decimal NON_BUON_NON
        {
            get
            {
                return _NON_BUON_NON;
            }
            set
            {
                _NON_BUON_NON = value;
            }
        }
        public Decimal TIEU_CHAY
        {
            get
            {
                return _TIEU_CHAY;
            }
            set
            {
                _TIEU_CHAY = value;
            }
        }
        public Decimal XUAT_HUYET_ND
        {
            get
            {
                return _XUAT_HUYET_ND;
            }
            set
            {
                _XUAT_HUYET_ND = value;
            }
        }
        public Decimal NOIBAN_ND
        {
            get
            {
                return _NOIBAN_ND;
            }
            set
            {
                _NOIBAN_ND = value;
            }
        }
        public Decimal TIEPXUCGAN_BN
        {
            get
            {
                return _TIEPXUCGAN_BN;
            }
            set
            {
                _TIEPXUCGAN_BN = value;
            }
        }
        public String XACDINH_F
        {
            get
            {
                return _XACDINH_F;
            }
            set
            {
                _XACDINH_F = value;
            }
        }
        public String TT_CACHLY
        {
            get
            {
                return _TT_CACHLY;
            }
            set
            {
                _TT_CACHLY = value;
            }
        }
        public DateTime CACHLY_TUNGAY
        {
            get
            {
                return _CACHLY_TUNGAY;
            }
            set
            {
                _CACHLY_TUNGAY = value;
            }
        }
        public DateTime CACHLY_DENGAY
        {
            get
            {
                return _CACHLY_DENGAY;
            }
            set
            {
                _CACHLY_DENGAY = value;
            }
        }
        public String LICHSU
        {
            get
            {
                return _LICHSU;
            }
            set
            {
                _LICHSU = value;
            }
        }
        public String GHICHU
        {
            get
            {
                return _GHICHU;
            }
            set
            {
                _GHICHU = value;
            }
        }
        public String NGUOITAO
        {
            get
            {
                return _NGUOITAO;
            }
            set
            {
                _NGUOITAO = value;
            }
        }
        public DateTime NGAYTAO
        {
            get
            {
                return _NGAYTAO;
            }
            set
            {
                _NGAYTAO = value;
            }
        }
        public String NGUOISUA
        {
            get
            {
                return _NGUOISUA;
            }
            set
            {
                _NGUOISUA = value;
            }
        }
        public DateTime NGAYSUA
        {
            get
            {
                return _NGAYSUA;
            }
            set
            {
                _NGAYSUA = value;
            }
        }
        #endregion
        #region Constructors
        public COVID_DANGKY()
        {

        }
        public COVID_DANGKY(Decimal ID, Decimal CANBOID, Decimal TOAANID, Decimal DV_TRUCTHUOC, Decimal PHONGBANID, Decimal SOT, Decimal HO, Decimal KHO_THO, Decimal DAU_HONG,
                      Decimal NON_BUON_NON, Decimal TIEU_CHAY, Decimal XUAT_HUYET_ND, Decimal NOIBAN_ND, Decimal TIEPXUCGAN_BN, String XACDINH_F, String TT_CACHLY, DateTime CACHLY_TUNGAY, DateTime CACHLY_DENGAY, String LICHSU, String GHICHU, String NGUOITAO,DateTime NGAYTAO, String NGUOISUA, DateTime NGAYSUA)
        {
            _ID = ID;
            _CANBOID = CANBOID;
            _TOAANID = TOAANID;
            _DV_TRUCTHUOC = DV_TRUCTHUOC;
            _PHONGBANID = PHONGBANID;
            _SOT = SOT;
            _HO = HO;
            _KHO_THO = KHO_THO;
            _DAU_HONG = DAU_HONG;
            _NON_BUON_NON = NON_BUON_NON;
            _TIEU_CHAY = TIEU_CHAY;
            _XUAT_HUYET_ND = XUAT_HUYET_ND;
            _NOIBAN_ND = NOIBAN_ND;
            _TIEPXUCGAN_BN = TIEPXUCGAN_BN;
            _XACDINH_F = XACDINH_F;
            _TT_CACHLY = TT_CACHLY;
            _CACHLY_TUNGAY = CACHLY_TUNGAY;
            _CACHLY_DENGAY = CACHLY_DENGAY;
            _LICHSU = LICHSU;
            _GHICHU = GHICHU;
            _NGUOITAO = NGUOITAO;
            _NGAYTAO = NGAYTAO;
            _NGUOISUA = NGUOISUA;
            _NGAYSUA = NGAYSUA;
        }
        #endregion
    }
}