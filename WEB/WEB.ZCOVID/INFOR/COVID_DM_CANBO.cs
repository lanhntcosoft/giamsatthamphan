﻿using System;
using System.Collections.Generic;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace WEB.ZCOVID.INFOR
{
    public class COVID_DM_CANBO
    {
        public decimal ID { get; set; }
        public Nullable<decimal> TOAANID { get; set; }
        public string MACANBO { get; set; }
        public string HOTEN { get; set; }
        public string SOCMND { get; set; }
        public Nullable<decimal> ISHOIDONG { get; set; }
        public Nullable<decimal> HIEULUC { get; set; }
        public Nullable<decimal> CHUCDANHID { get; set; }
        public Nullable<decimal> CHUCVUID { get; set; }
        public string DIACHI { get; set; }
        public Nullable<System.DateTime> NGAYNHANCONGTAC { get; set; }
        public Nullable<System.DateTime> NGAYSINH { get; set; }
        public string SODIENTHOAI { get; set; }
        public Nullable<System.DateTime> NGAYTAO { get; set; }
        public string NGUOITAO { get; set; }
        public Nullable<System.DateTime> NGAYSUA { get; set; }
        public string NGUOISUA { get; set; }
        public Nullable<System.DateTime> NGAYBONHIEM { get; set; }
        public Nullable<System.DateTime> NGAYKETTHUC { get; set; }
        public Nullable<decimal> ISHINHSU { get; set; }
        public Nullable<decimal> ISHNGD { get; set; }
        public Nullable<decimal> ISKDTM { get; set; }
        public Nullable<decimal> ISDANSU { get; set; }
        public Nullable<decimal> ISHANHCHINH { get; set; }
        public Nullable<decimal> ISLAODONG { get; set; }
        public Nullable<decimal> CAPTHAMPHAN { get; set; }
        public string QDBONHIEM { get; set; }
        public Nullable<decimal> GIOITINH { get; set; }
        public string MADONGBO { get; set; }
        public Nullable<decimal> ISPHASAN { get; set; }
        public Nullable<decimal> ISBPXLHC { get; set; }
        public Nullable<System.DateTime> NGAYBAINHIEM { get; set; }
        public Nullable<decimal> PHONGBANID { get; set; }
        public Nullable<decimal> DV_TRUCTHUOC { get; set; }
        public string EMAIL { get; set; }
        public Nullable<decimal> DONVI_ID { get; set; }
        public Nullable<decimal> NOT_IN_PMQLCB { get; set; }
    }
}