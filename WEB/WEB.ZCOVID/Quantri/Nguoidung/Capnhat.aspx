﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/COVIDS.Master" AutoEventWireup="true" CodeBehind="Capnhat.aspx.cs" Inherits="WEB.ZCOVID.Quantri.Nguoidung.Capnhat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../UI/js/Common.js"></script>
    <asp:HiddenField ID="hddID" Value="0" runat="server" />
    <div class="content_form">
        <div class="content_body">
            <div class="content_form_head">
                <div class="content_form_head_title ">Cập nhật người sử dụng</div>
                <div class="content_form_head_right"></div>
            </div>
            <div class="content_form_body">
                <table class="table1">
                    <tr>
                        <td style="width: 130px;" align="left">Đơn vị<asp:Label runat="server" ID="Label1" Text="(*)" ForeColor="Red"></asp:Label>
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList CssClass="chosen-select" ID="DropDV_TRUCTHUOC" runat="server" Width="522px" AutoPostBack="True" OnTextChanged="ddlDonvi_TextChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="height: 36px;">
                        <td align="left">Nhóm quyền<asp:Label runat="server" ID="Label3" Text="(*)" ForeColor="Red"></asp:Label>
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList CssClass="chosen-select" ID="ddlNhomquyen" runat="server" Width="522px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left">Phòng ban
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlPhongban_SelectedIndexChanged" CssClass="chosen-select" ID="ddlPhongban" runat="server" Width="522px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="height:38px;">
                        <td align="left">Chọn cán bộ
                            <asp:Label runat="server" ID="Label4" Text="(*)" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCanbo" CssClass="chosen-select" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCanbo_SelectedIndexChanged" Width="195px">
                            </asp:DropDownList>
                        </td>
                        <td  style="text-align:right;">Điện thoại
                        </td>
                        <td>
                            <asp:TextBox ID="txtDienthoai" CssClass="user" runat="server" Width="200px" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height: 36px;">
                        <td align="left">Tài khoản
                        <asp:Label runat="server" ID="Label2" Text="(*)" ForeColor="Red"></asp:Label>
                        </td>
                        <td style="width: 200px;" align="left">
                            <asp:TextBox ID="txtUsername" CssClass="user" runat="server" Width="95%" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Mật khẩu</td>
                        <td>
                            <asp:TextBox ID="txtPass" CssClass="user" TextMode="Password" runat="server" Width="95%" MaxLength="100"></asp:TextBox>
                        </td>
                        <td style="width: 105px; text-align:left;">Nhập lại mật khẩu
                        </td>
                        <td>
                            <asp:TextBox ID="txtRePass" CssClass="user" TextMode="Password" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height: 36px;">
                        <td>Email
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" CssClass="user" runat="server" Width="95%" MaxLength="250"></asp:TextBox>
                        </td>

                    </tr>

                    <tr>
                        <td align="left" colspan="4">
                            <div style="margin-left: 437px; margin-top: 5px;">
                                <asp:CheckBox ID="chkHieuluc" runat="server" Checked="true" Text="Hiệu lực" />
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <div style="margin-left: 130px; margin-top: 5px; margin-bottom: 15px;">
                                <asp:Label runat="server" ID="lbthongbao" ForeColor="Red" Font-Size="16px"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput"
                                Text="Lưu" OnClick="btnUpdate_Click" OnClientClick="return kiemtra();" />

                            <asp:Button ID="cmdQuaylai" runat="server" CssClass="buttoninput" Text="Quay lại" OnClick="lblquaylai_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script>        
        function kiemtra() {
            var ddlDonvi = document.getElementById('<%=DropDV_TRUCTHUOC.ClientID%>');
            var value_change = DropDV_TRUCTHUOC.options[DropDV_TRUCTHUOC.selectedIndex].value;
            if (value_change == "0") {
                alert('Bạn chưa chọn đơn vị !');
                DropDV_TRUCTHUOC.focus();
                return false;
            }

            var txtUsername = document.getElementById('<%=txtUsername.ClientID %>');
            if (!Common_CheckEmpty(txtUsername.value)) {
                alert('Chưa nhập tài khoản đăng nhập!');
                txtUsername.focus();
                return false;
            }

            var ddlCanbo = document.getElementById('<%=ddlCanbo.ClientID%>');
            value_change = ddlCanbo.options[ddlCanbo.selectedIndex].value;
            if (value_change == "0") {
                alert('Chưa chọn cán bộ sử dụng tài khoản!');
                ddlCanbo.focus();
                return false;
            }

            var txtEmailControl = document.getElementById('<%=txtEmail.ClientID %>');
            if (Common_CheckEmpty(txtEmailControl.value)) {
                var email_string = txtEmailControl.value;
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(email_string) && email_string.trim() != '') {
                    alert('Bạn phải nhập Email hợp lệ. \n VD: Example@gmail.com');
                    txtEmailControl.focus;
                    return false;
                }
            }
            return true;
        }
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>
