﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID.Quantri.Nguoidung
{
    public partial class ResetPass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CheckQuyen();
                decimal current_id = Convert.ToDecimal(Request["vID"] + "");
                hddID.Value = current_id.ToString();
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                DataTable tbl = obj.Get_UserByID(Request["vID"] + "");
                txtUserName.Text = tbl.Rows[0]["USERNAME"] + ""; 
            }
        }

        void CheckQuyen()
        {
            //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
            //Cls_Comon.SetButton(cmdSave, oPer.CAPNHAT);
        }
        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsach.aspx");
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                decimal current_id = Convert.ToDecimal(Request["vID"] + "");
                hddID.Value = current_id.ToString();
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                if (obj.Users_Change_Password_byID(Request["vID"] + "",Session["SESSION_USERNAME"] + "", txtPass.Text) == true)
                {
                    lttMsg.Text = "<div class='error_msg'  style='color: red; '>Bạn đã đổi mật khẩu thành công !</div>";
                }
                //Response.Redirect("Danhsach.aspx");
            }
        }
        Boolean validateForm()
        {
            bool val = true;
            if (txtPass.Text.Trim() == "")
            {
                lttMsg.Text = "<div class='error_msg'  style='color: red; '>Bạn chưa nhập mật khẩu !</div>";
                val = false;
            }
            else if (txtPass.Text.Length < 6)
            {
                lttMsg.Text = "<div class='error_msg'  style='color: red; '>Mật khẩu tối thiểu 6 ký tự !</div>";
                val = false;
            }
            else if (txtPass.Text.Length >= 100)
            {
                lttMsg.Text = "<div class='error_msg'  style='color: red; '>Mật khẩu nhập quá 100 ký tự !</div>";
                val = false;
            }
            else if (txtPass.Text != txtRepass.Text)
            {
                lttMsg.Text = "<div class='error_msg'  style='color: red; '>Nhập lại mật không chính xác !</div>";
                val = false;
            }
            return val;
        }
    }
}