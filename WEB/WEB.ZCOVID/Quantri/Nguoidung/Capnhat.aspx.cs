﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID.Quantri.Nguoidung
{
    public partial class Capnhat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (!(String.IsNullOrEmpty(txtPass.Text.Trim())))
                {
                    txtPass.Attributes["value"] = txtPass.Text;
                }
            }
            if (!IsPostBack)
            {

                LoadDropDonvi();
                LoadPhongban();
                LoadCanbo();
                LoadNhomquyen();
                txtUsername.Enabled = true;
                string strID = Request["CID"] + "";
                if (strID != "")
                {
                    COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                    DataTable tbl = obj.Get_UserByID(Request["CID"] + "");
                    //------------
                    DropDV_TRUCTHUOC.SelectedValue = tbl.Rows[0]["DV_TRUCTHUOC"] + "";
                    LoadPhongban();
                    ddlPhongban.SelectedValue = tbl.Rows[0]["PHONGBANID"] + "";
                    LoadCanbo();
                    txtUsername.Enabled = false;
                    txtUsername.Text = tbl.Rows[0]["USERNAME"] + "";
                    chkHieuluc.Checked = Convert.ToBoolean(Convert.ToInt32(tbl.Rows[0]["HIEULUC"]));
                    txtDienthoai.Text = tbl.Rows[0]["SODIENTHOAI"] + "";
                    txtEmail.Text = tbl.Rows[0]["EMAIL"] + "";
                    ddlNhomquyen.SelectedValue = tbl.Rows[0]["NHOM_ND_ID"] + "";
                    txtPass.Enabled = false; txtRePass.Enabled = false;
                    ddlCanbo.SelectedValue = tbl.Rows[0]["CANBOID"] + "";
                    hddID.Value = Request["CID"] + "";
                }
                //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                //Cls_Comon.SetButton(cmdUpdate, oPer.CAPNHAT);
            }
        }
        private void LoadCanbo()
        {
            //Load cán bộ
            ddlCanbo.Items.Clear();
            COVID_DM_CANBO_BL oDMCBBL = new COVID_DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.GET_CANBO_DV_PHONGBAN(DropDV_TRUCTHUOC.SelectedValue,ddlPhongban.SelectedValue);
            ddlCanbo.DataSource = oCBDT;
            ddlCanbo.DataTextField = "HOTEN";
            ddlCanbo.DataValueField = "ID";
            ddlCanbo.DataBind();
            ddlCanbo.Items.Insert(0, new ListItem("--Chọn cán bộ--", ""));
        }
        private void LoadDropDonvi()
        {
            DropDV_TRUCTHUOC.Items.Clear();
            COVID_DV_TRUCTHUOC_BL oBL = new COVID_DV_TRUCTHUOC_BL();
            DropDV_TRUCTHUOC.DataSource = oBL.GET_DV_TRUCTHUOC_LIST();
            DropDV_TRUCTHUOC.DataTextField = "TEN_ALIAS";
            DropDV_TRUCTHUOC.DataValueField = "ID";
            DropDV_TRUCTHUOC.DataBind();
        }
        private void LoadCombobox()
        {
            LoadDropDonvi();
            LoadPhongban();
            LoadCanbo();
            LoadNhomquyen();
        }
        private void LoadPhongban()
        {
            ddlPhongban.Items.Clear();
            COVID_PHONGBAN_BL oBL = new COVID_PHONGBAN_BL();
            ddlPhongban.DataSource = oBL.GET_PHONGBAN_LIST(DropDV_TRUCTHUOC.SelectedValue);
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("---chọn phòng ban---", ""));
        }
        private void LoadNhomquyen()
        {
            COVID_NGUOISUDUNG_BL oTA = new COVID_NGUOISUDUNG_BL();
             ddlNhomquyen.Items.Clear();
            ddlNhomquyen.DataSource = oTA.Get_NhomQuyenAll();
            ddlNhomquyen.DataTextField = "NAME";
            ddlNhomquyen.DataValueField = "ID";
            ddlNhomquyen.DataBind();
            ddlNhomquyen.Items.Insert(0, new ListItem("--- Chọn nhóm quyền---", ""));
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                lbthongbao.Text = "";
                if (ddlNhomquyen.SelectedIndex == 0)
                {
                    lbthongbao.Text = "Bạn chưa chọn nhóm quyền.";
                    return;
                }
                if (ddlCanbo.SelectedValue =="")
                {
                    lbthongbao.Text = "Bạn chưa chọn cán bộ.";
                    return;
                }
                if (txtUsername.Text.Trim() == "")
                {
                    lbthongbao.Text = "Bạn chưa nhập tài khoản.";
                    return;
                }
                if (hddID.Value == "0")
                {
                    COVID_NGUOISUDUNG_BL obj_bl = new COVID_NGUOISUDUNG_BL();
                    DataTable tbl = obj_bl.Get_ByUSERNAME(txtUsername.Text.Trim());
                    if (tbl.Rows.Count>0)
                    {
                        lbthongbao.Text = "Tài khoản này đã tồn tại, bạn phải chọn tài khoản khác.";
                        txtUsername.Focus();
                        return;
                    }
                    //Kiểm tra mật khẩu
                    if (txtPass.Text.Length < 6)
                    {
                        lbthongbao.Text = "Mật khẩu phải > 6 ký tự.";
                        txtPass.Focus();
                        return;
                    }
                    if (txtPass.Text != txtRePass.Text)
                    {
                        lbthongbao.Text = "Bạn nhập lại mật khẩu chưa đúng.";
                        txtRePass.Focus();
                        return;
                    }
                }
                string strPass = Cls_Comon_covid.MD5Encrypt(txtPass.Text);
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                COVID_NGUOISUDUNG oT = new COVID_NGUOISUDUNG();
                oT.ID = Convert.ToDecimal(hddID.Value);
                oT.USERNAME = txtUsername.Text.Trim();
                oT.PASSWORD = strPass;
                oT.HOTEN = ddlCanbo.SelectedItem.Text;
                oT.CANBOID = Convert.ToDecimal(ddlCanbo.SelectedValue);
                oT.HIEULUC = chkHieuluc.Checked ? 1 : 0;
                oT.DIENTHOAI = txtDienthoai.Text;
                oT.EMAIL = txtEmail.Text;
                oT.NGAYTAO = DateTime.Now;
                oT.NGUOITAO = Session["SESSION_USERNAME"]+"";
                oT.NHOM_ND_ID = Convert.ToDecimal(ddlNhomquyen.SelectedValue);
                oT.DV_TRUCTHUOC = Convert.ToDecimal(DropDV_TRUCTHUOC.SelectedValue);
                if(ddlPhongban.SelectedValue!="")
                oT.PHONGBANID = Convert.ToDecimal(ddlPhongban.SelectedValue);
                String V_THONGBAO = "";
                 if (obj.COVID_NGUOISUDUNG_INS_UP(oT,ref V_THONGBAO) ==true)
                {
                    if(V_THONGBAO!="")
                    {
                        lbthongbao.Text = V_THONGBAO;
                    }
                    else
                    {
                        if (hddID.Value != "0")
                        {
                            lbthongbao.Text = "Bạn đã cập nhật thành công";
                        }
                        else
                        {
                            lbthongbao.Text = "Bạn đã thêm mới thành công";
                            txtUsername.Text = txtPass.Text = txtRePass.Text = "";
                            ddlCanbo.SelectedIndex = 0;
                            txtDienthoai.Text = string.Empty;
                            txtEmail.Text = string.Empty;
                            //Response.Redirect("Danhsach.aspx");
                        }
                    }

                }
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lblquaylai_Click(object sender, EventArgs e)
        {
            Response.Redirect("Danhsach.aspx");
        }
        protected void ddlDonvi_TextChanged(object sender, EventArgs e)
        {
            LoadPhongban();
            LoadCanbo();
        }
        protected void ddlPhongban_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCanbo();
        }
        protected void ddlCanbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            COVID_DM_CANBO_BL obj = new COVID_DM_CANBO_BL();
            if (ddlCanbo.SelectedValue != "")
            {
                DataTable tbl = obj.GET_CANBO_ID(Convert.ToDecimal(ddlCanbo.SelectedValue));
                if (tbl.Rows.Count != 0)
                {
                    txtDienthoai.Text = tbl.Rows[0]["SODIENTHOAI"] + "";
                    txtEmail.Text = tbl.Rows[0]["EMAIL"] + "";
                }
            }
        }
        
    }
}