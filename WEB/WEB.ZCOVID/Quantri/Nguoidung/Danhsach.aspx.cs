﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID.Quantri.Nguoidung
{
    public partial class Danhsach : System.Web.UI.Page
    {
        public bool GetBool(object obj)
        {
            try
            {
                if ((obj + "") == "")
                    return false;
                else
                    return Convert.ToBoolean(obj);
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Decimal CurrUserID = (String.IsNullOrEmpty(Session["SESSION_USERID"] + "")) ? 0 : Convert.ToDecimal(Session["SESSION_USERID"] + "");
            if (CurrUserID > 0)
            {
                if (!IsPostBack)
                {
                    LoadDropDonvi();
                    LoadPhongban();
                    if (Session["SSND_TA"] != null) DropDV_TRUCTHUOC.SelectedValue = Session["SSND_TA"] + "";
                    if (Session["SSND_PB"] != null) ddlPhongban.SelectedValue = Session["SSND_PB"] + "";
                    txtUserName.Text = Session["SSND_US"] + "";
                    txtHoten.Text = Session["SSND_HT"] + "";
                    Load_Data();
                    //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    //Cls_Comon.SetButton(cmdThemmoi, oPer.TAOMOI);
                }
            }
            else Response.Redirect("/Login.aspx");
        }
        protected void ddlDonvi_TextChanged(object sender, EventArgs e)
        {
            LoadPhongban();
        }
        private void LoadDropDonvi()
        {
            DropDV_TRUCTHUOC.Items.Clear();
            COVID_DV_TRUCTHUOC_BL oBL = new COVID_DV_TRUCTHUOC_BL();
            DropDV_TRUCTHUOC.DataSource = oBL.GET_DV_TRUCTHUOC_LIST();
            DropDV_TRUCTHUOC.DataTextField = "TEN_ALIAS";
            DropDV_TRUCTHUOC.DataValueField = "ID";
            DropDV_TRUCTHUOC.DataBind();
            DropDV_TRUCTHUOC.Items.Insert(0, new ListItem("-- Tất cả --", ""));
        }
        private void LoadPhongban()
        {
            ddlPhongban.Items.Clear();
            COVID_PHONGBAN_BL oBL = new COVID_PHONGBAN_BL();
            ddlPhongban.DataSource = oBL.GET_PHONGBAN_LIST(DropDV_TRUCTHUOC.SelectedValue);
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("---chọn phòng ban---", ""));
        }
        private void Load_Data()
        {
            COVID_NGUOISUDUNG_BL oBL = new COVID_NGUOISUDUNG_BL();
            List<COVID_NGUOISUDUNG> li_sw = oBL.QT_NGUOIDUNG_SEARCH_LIST(DropDV_TRUCTHUOC.SelectedValue,ddlPhongban.SelectedValue,txtUserName.Text.Trim(),txtHoten.Text.Trim());
            if (li_sw.Count > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon_covid.GetTotalPage(li_sw.Count, 50).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + li_sw.Count.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không có kết quả nào phù hợp yêu cầu tìm kiếm !";
            }

            dgList.DataSource = li_sw;
            dgList.DataBind();

        }

        protected void cmdLamMoi_Click(object sender, EventArgs e)
        {
            Session.Remove("SSND_TA");
            Session.Remove("SSND_PB");
            Session.Remove("SSND_US");
            Session.Remove("SSND_HT"); 
            DropDV_TRUCTHUOC.SelectedValue = string.Empty;
            ddlPhongban.SelectedValue = string.Empty;
            txtHoten.Text = string.Empty;
            txtUserName.Text = string.Empty;
            lbtthongbao.Text = string.Empty;
        }
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            Session["SSND_TA"] = DropDV_TRUCTHUOC.SelectedValue;
            Session["SSND_PB"] = ddlPhongban.SelectedValue;
            Session["SSND_US"] = txtUserName.Text;
            Session["SSND_HT"] = txtHoten.Text;
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }
        
        protected void btnThemmoi_Click(object sender, EventArgs e)
        {

            Response.Redirect("Capnhat.aspx");

        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {

            switch (e.CommandName)
            { //--------    
                case "Reset":
                    Response.Redirect("ResetPass.aspx?vID=" + e.CommandArgument.ToString());
                    break;
                case "Sua":
                    Response.Redirect("Capnhat.aspx?CID=" + e.CommandArgument.ToString());
                    break;
                case "Xoa":
                    COVID_NGUOISUDUNG_BL obj_bl = new COVID_NGUOISUDUNG_BL();
                    decimal ID = Convert.ToDecimal(e.CommandArgument);
                    String v_thongbao = "";
                    if (obj_bl.DELETE_NGUOIDUNG_BYID(ID, ref v_thongbao) == true)
                    {
                        if (v_thongbao != "")
                        {
                            lbtthongbao.Text = v_thongbao;
                        }
                        else
                        {
                            dgList.CurrentPageIndex = 0;
                            hddPageIndex.Value = "1";
                            Load_Data();
                            lbtthongbao.Text = "Bạn đã xóa thành công!";
                        }
                    }
                  
                    break;
                    //MenuPermission oPer = Cls_Comon.GetMenuPer(Request.FilePath, Convert.ToDecimal(Session[ENUM_SESSION.SESSION_USERID]));
                    //if (oPer.XOA == false)
                    //{
                    //    lbtthongbao.Text = "Bạn không có quyền xóa!";
                    //    return;
                    //}
            }
        }

        #region "Phân trang"

        protected void lbTBack_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value) - 2;
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
            Load_Data();
        }

        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = 0;
            hddPageIndex.Value = "1";
            Load_Data();
        }

        protected void lbTLast_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddTotalPage.Value) - 1;
            hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
            Load_Data();
        }

        protected void lbTNext_Click(object sender, EventArgs e)
        {
            dgList.CurrentPageIndex = Convert.ToInt32(hddPageIndex.Value);
            hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
            Load_Data();
        }

        protected void lbTStep_Click(object sender, EventArgs e)
        {
            LinkButton lbCurrent = (LinkButton)sender;
            dgList.CurrentPageIndex = Convert.ToInt32(lbCurrent.Text) - 1;
            hddPageIndex.Value = lbCurrent.Text;
            Load_Data();
        }

        #endregion
    }
}