﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID
{
    public partial class DanhSach : System.Web.UI.Page
    {

        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            Decimal CurrUser = (string.IsNullOrEmpty(Session["SESSION_USERID"] + "")) ? 0 : Convert.ToDecimal(Session["SESSION_USERID"] + "");
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnXemBC);
            if (CurrUser == 0)
            {
                Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login.aspx");
            }
            if (!IsPostBack)
            {
                txtDenNgay.Text = DateTime.Now.ToString("dd/MM/yyyy");
                DropDV_TRUCTHUOC.Enabled = true;
                LoadDropDonVi_TrucThuoc();
                COVID_DM_CANBO_BL obj = new COVID_DM_CANBO_BL();
                DataTable tbl = obj.GET_CHUCVU_CHECK(Session["SESSION_CANBO_ID"] + "");
                //45 Chánh án,426 Viện trưởng,432 Vụ trưởng,444 Chánh Văn phòng
                //,451 Giám đốc,455 Cục trưởng,428 Tổng biên tập 
                if (tbl.Rows.Count != 0)
                {
                    DropDV_TRUCTHUOC.SelectedValue = tbl.Rows[0]["DV_TRUCTHUOC"]+"";
                    DropDV_TRUCTHUOC.Enabled = false;
                }
                if (Request["trangthai"] != null)
                {
                    Drop_TrangThai.SelectedValue = Request["trangthai"].ToString();
                }
                LoadPhongban();
                LoadCanbo();
                LoadDropChucDanh();
                LoadDropChucVu();
                LoadData();
            }
        }
        private void LoadDropDonVi_TrucThuoc()
        {
            DropDV_TRUCTHUOC.Items.Clear();
            COVID_DV_TRUCTHUOC_BL oBL = new COVID_DV_TRUCTHUOC_BL();
            DropDV_TRUCTHUOC.DataSource = oBL.GET_DV_TRUCTHUOC_LIST();
            DropDV_TRUCTHUOC.DataTextField = "TEN_ALIAS";
            DropDV_TRUCTHUOC.DataValueField = "ID";
            DropDV_TRUCTHUOC.DataBind();
            DropDV_TRUCTHUOC.Items.Insert(0, new ListItem("-- Tất cả --", ""));
        }
        private void LoadPhongban()
        {

            ddlPhongban.Items.Clear();
            COVID_PHONGBAN_BL oBL = new COVID_PHONGBAN_BL();
            ddlPhongban.DataSource = oBL.GET_PHONGBAN_LIST(DropDV_TRUCTHUOC.SelectedValue);
            ddlPhongban.DataTextField = "TENPHONGBAN";
            ddlPhongban.DataValueField = "ID";
            ddlPhongban.DataBind();
            ddlPhongban.Items.Insert(0, new ListItem("-- Tất cả --", ""));
        }
        public void LoadDropChucDanh()
        {
            DropChucDanh.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucDanh = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCDANH");
            if (dmChucDanh != null && dmChucDanh.Rows.Count > 0)
            {
                DropChucDanh.DataSource = dmChucDanh;
                DropChucDanh.DataTextField = "TEN";
                DropChucDanh.DataValueField = "ID";
                DropChucDanh.DataBind();
            }
            DropChucDanh.Items.Insert(0, new ListItem("--- Tất cả ---", ""));
        }
        public void LoadDropChucVu()
        {
            DropChucVu.Items.Clear();
            COVID_DM_CANBO_BL oBL = new COVID_DM_CANBO_BL();
            DataTable dmChucVu = oBL.DM_DATAITEM_GETBYGROUPNAME("CHUCVU");
            if (dmChucVu != null && dmChucVu.Rows.Count > 0)
            {
                DropChucVu.DataSource = dmChucVu;
                DropChucVu.DataTextField = "TEN";
                DropChucVu.DataValueField = "ID";
                DropChucVu.DataBind();
            }
            DropChucVu.Items.Insert(0, new ListItem("--- Tất cả ---", ""));
        }
        protected void DV_TRUCTHUOC_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPhongban();
            LoadCanbo();
        }
        protected void ddlPhongban_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCanbo();
        }
        private void LoadDonvi()
        {
            //Drop_DonVi.Items.Clear();
            //COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            //Drop_DonVi.DataSource = oBL.Get_DonViFull();
            //Drop_DonVi.DataTextField = "MA_TEN";
            //Drop_DonVi.DataValueField = "ID";
            //Drop_DonVi.DataBind();
            //Drop_DonVi.Items.Insert(0, new ListItem("-- Chọn đơn vị cấp quận, huyện --", ""));
        }
        private void LoadCanbo()
        {
            //Load cán bộ
            ddlCanbo.Items.Clear();
            COVID_DM_CANBO_BL oDMCBBL = new COVID_DM_CANBO_BL();
            DataTable oCBDT = oDMCBBL.GET_CANBO_DV_PHONGBAN(DropDV_TRUCTHUOC.SelectedValue, ddlPhongban.SelectedValue);
            ddlCanbo.DataSource = oCBDT;
            ddlCanbo.DataTextField = "HOTEN";
            ddlCanbo.DataValueField = "ID";
            ddlCanbo.DataBind();
            ddlCanbo.Items.Insert(0, new ListItem("--Chọn cán bộ--", ""));
        }
        protected void cmdLamMoi_Click(object sender, EventArgs e)
        {
            DropDV_TRUCTHUOC.SelectedValue = string.Empty;
            Drop_TrangThai.SelectedValue = "1";
            ddlCanbo.SelectedValue = string.Empty;
            ddlPhongban.SelectedValue = string.Empty;
            DropChucDanh.SelectedValue = string.Empty;
            DropChucVu.SelectedValue = string.Empty;
            txKey.Text = string.Empty;
            lbtthongbao.Text = string.Empty;
        }
        protected void lbtimkiem_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadData();
            }
            catch (Exception ex) { lbtthongbao.Text = ex.Message; }
        }
        protected void btnXemBC_Click(object sender, EventArgs e)
        {
            Literal Table_Str_Totals = new Literal();
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            ////-------------
            tbl = oBL.THONGKE_BAOCAO_EXPORT();
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
                Table_Str_Totals.Text = row["TEXT_REPORT"] + "";
            }
            ////--------------------------
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=Thong_Ke_BC.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/msword";
            HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8;
            Response.Write("<html");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]> <xml> <w:WordDocument> <w:View>Print</w:View> <w:Zoom>100</w:Zoom> <w:DoNotOptimizeForBrowser/> </w:WordDocument> </xml> <![endif]-->");
            Response.Write("<META HTTP-EQUIV=Content-Type CONTENT=text/html; charset=UTF-8>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content=Microsoft Word 9>");
            Response.Write("<meta name=Originator content=Microsoft Word 9>");
            Response.Write("<style>");
            Response.Write("<!-- /* Style Definitions */" +
                                             "p.MsoFooter, li.MsoFooter, div.MsoFooter" +
                                             "{margin:0in;" +
                                             "margin-bottom:.0001pt;" +
                                             "mso-pagination:widow-orphan;" +
                                             "tab-stops:center 3.0in right 6.0in;" +
                                             "font-size:12.0pt;}");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:.75in .5in .75in .5in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}");
            Response.Write("div.Section1 {page:Section1;}");
            //Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:1.25in 1.0in 1.25in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}");
            //Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.7in 0.5in 0.5in 0.5in;mso-header-margin:.1in;mso-footer-margin:.1in;mso-paper-source:0;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.2in 0.2in 0.5in 0.2in;mso-header: h1;mso-header-margin:.2in;mso-footer-margin:.3in;mso-paper-source:0;mso-footer: f1;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("<style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section1>");
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Table_Str_Totals.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.Write("</div>");
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }
        void LoadData()
        {
            int page_size = Convert.ToInt32(dropPageSize.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable oDT = oBL.Covid_Dangky_List_All(txtDenNgay.Text.Trim(),Drop_TrangThai.SelectedValue,DropDV_TRUCTHUOC.SelectedValue, ddlPhongban.SelectedValue, DropChucDanh.SelectedValue, DropChucVu.SelectedValue,txKey.Text.Trim(),ddlCanbo.SelectedValue, pageindex, page_size);
            int count_all = 0;
            if (oDT.Rows.Count > 0)
            {
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
                lstSobanghiT_Canbo.Text = oDT.Rows[0]["DANGKY_TONG"]+"";
            }
            if (oDT != null && count_all > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon_covid.GetTotalPage(count_all, Convert.ToInt32(dropPageSize.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không tồn tại dữ liệu kê khai";
            }
            rpt.DataSource = oDT;
            rpt.DataBind();
        }
        protected void dropPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize2.SelectedValue = dropPageSize.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        protected void dropPageSize2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize.SelectedValue = dropPageSize2.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        #endregion
    }
}