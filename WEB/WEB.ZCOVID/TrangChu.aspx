﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/COVIDS.Master" AutoEventWireup="true" CodeBehind="TrangChu.aspx.cs" Inherits="WEB.ZCOVID.TrangChu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="/UI/css/TAble.css" rel="stylesheet" />
    <style>
        .ajax__calendar_container {
            width: 180px;
        }

        .ajax__calendar_body {
            width: 100%;
            height: 145px;
        }

        .panding_left li span {
            padding-left: 40px;
        }

        .thongke li {
            line-height: 22px;
        }
    </style>
    <script src="UI/js/Common.js"></script>
    <div class="content_form" style="margin-bottom: 30px;">
        <div class="leftzone">
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Văn bản chỉ đạo điều hành</span></div>
                <div class="leftmenu_content">
                    <ul class="thongke">
                        <li><a href="http://www.toaan.gov.vn/webcenter/portal/tatc/chi-tiet-chi-dao-dieu-hanh?dDocName=TAND110877" target="_blank"><span style="color: #cf2b0e;">new*</span>Công văn số 32/TANDTC-TH ngày 18/3/2020: Khai báo y tế hàng ngày trên phần mềm</a></li>
                        <li><a href="http://www.toaan.gov.vn/webcenter/portal/tatc/chi-tiet-chi-dao-dieu-hanh?dDocName=TAND106793" target="_blank">Chỉ thị số 02/2020/CT-CA ngày 10/3/2020: về phòng, chống dịch dịch covid-19 trong hệ thống Toà án nhân dân</a></li>
                        <li><a href="http://www.toaan.gov.vn/webcenter/portal/tatc/chi-tiet-chi-dao-dieu-hanh?dDocName=TAND112031" target="_blank">Công văn số 82/TANDTC-VP ngày 11/3/2020: báo cáo nhanh về tình hình phòng, chống dịch covid 19</a></li>
                    </ul>
                </div>
            </div>
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Thống kê nhanh</span></div>
                <div class="leftmenu_content">
                    <asp:Literal ID="Li_THONGKE_F" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Danh sách các trường hợp F0 - F4</span></div>
                <div class="leftmenu_content">
                    <asp:Literal ID="Li_THONGKE_F_HOTEN" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="leftmenu">
                <div class="leftmenu_header arrow"><span>Lưu ý</span></div>
                <div class="leftmenu_content">
                    <ul class="thongke" style="color: #e01212; text-align: left;">
                        <li><span style="font-size: 16px;">1. Khai báo hàng ngày vào đầu giờ sáng</span></li>
                        <li><span style="font-size: 16px;">2. Trong trường hợp sức khỏe có diễn biến khác thường thì đề nghị khai báo bổ sung ngay</span> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="rightzone">
            <div class="content_body">
                <div class="content_form_head" style="">
                    <div class="content_form_head_title">
                        <div style="">KHAI THÔNG TIN </div>
                    </div>
                    <div class="content_form_head_right"></div>
                </div>
                <div class="content_form_body">
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung" style="background-color: transparent;"></h4>
                        <div class="border_search" style="background-color: #f7eba9;">
                            <div class="contai_canhbao" style="float: left; width: 100%; line-height: 28px;">
                                <%--<span style="color: #cf2b0e;">+ Các cán bộ công chức bắt đầu nhập từ thứ 2 ngày 23/03/2020.</span><br />--%>
                                <span runat="server" id="canhbao_ld" style="color: #cf2b0e;">+ Ngày hôm nay đã có 
                                       <asp:LinkButton ID="Link_da_KBYT" runat="server" OnClick="Link_da_KBYT_OnClick"></asp:LinkButton>
                                    người thực hiện khai báo y tế,
                                       <asp:LinkButton ID="Link_chua_KBYT" runat="server" OnClick="Link_chua_KBYT_OnClick"></asp:LinkButton>
                                    người chưa thực hiện khai báo y tế trong đơn vị của bạn.
                                   (nhấn vào chữ số để xem danh sách)
                                     <br />
                                </span>
                                <style>
                                    .contai_canhbao span, .contai_canhbao a {
                                        font-size: 15px;
                                        font-style: italic;
                                    }
                                </style>
                                <asp:Literal ID="Li_TK_CANHBAO" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Thông tin người khai</h4>
                        <div class="border_search">
                            <div style="float: left; width: 700px; font-size: 16px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Họ và tên</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txt_HOTEN" Text="Vũ Hoàng Anh" CssClass="user" runat="server" Enabled="false" Width="210px"></asp:TextBox>
                                </div>
                                <div style="width: 50px; float: left; font-size: 14px; margin-left: 30px;">Đơn vị</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txt_DV_TRUCTHUOC_TEN" CssClass="user" runat="server" Width="210px" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Điện thoại<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txtDienThoai" CssClass="user" runat="server" Width="210px" MaxLength="50"></asp:TextBox>
                                </div>
                                <div style="width: 50px; float: left; font-size: 14px; margin-left: 30px;">Email</div>
                                <div style="width: 210px; float: left; margin-left: 10px; font-size: 16px; font-weight: bold;">
                                    <asp:TextBox ID="txtEmail" CssClass="user" runat="server" Width="210px" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Nơi ở hiện tại<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 517px; float: left; margin-left: 10px;">
                                    <asp:DropDownList CssClass="chosen-select" ID="Drop_DonVi" runat="server" Width="517px"></asp:DropDownList>
                                </div>
                            </div>
                            <div style="float: left; width: 700px; font-size: 16px; margin-top: 10px;">
                                <div style="width: 100px; float: left; font-size: 14px;">Địa chỉ chi tiết<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                                <div style="width: 510px; float: left; margin-left: 10px;">
                                    <asp:TextBox ID="txt_DIACHI" CssClass="user" runat="server" TextMode="MultiLine" Rows="1" Width="510px"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Triệu chứng</h4>
                        <div class="border_search">
                            <style>
                                @media screen and (min-width: 960px) {
                                    .column_auto {
                                        float: left;
                                        width: 800px;
                                    }

                                    .ghichu_css {
                                        width: 700px;
                                    }

                                    .margin_auto {
                                        margin-top: unset;
                                    }

                                    .margin_left_auto {
                                        margin-left: 190px;
                                    }
                                }

                                @media screen and (max-width: 992px) {
                                    .column_auto {
                                        float: left;
                                        width: 400px;
                                    }

                                    .ghichu_css {
                                        width: 500px;
                                    }

                                    .margin_auto {
                                        margin-top: 15px;
                                    }

                                    .margin_left_auto {
                                        margin-left: 0px;
                                    }
                                }

                                @media screen and (max-width: 768px) {
                                    .column_auto {
                                        float: left;
                                        width: 400px;
                                    }

                                    .ghichu_css {
                                        width: 400px;
                                    }

                                    .margin_auto {
                                        margin-top: 15px;
                                    }

                                    .margin_left_auto {
                                        margin-left: 0px;
                                    }
                                }
                            </style>
                            <div class="column_auto">
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Sốt >=37.5 độ C<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rdSot"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Nôn/buồn nôn<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_non_buonnon"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="column_auto">
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Ho<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_ho"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Tiêu chảy<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_tieuchay"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="column_auto">
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Khó thở<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_khotho"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Xuất huyết ngoài da<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_xuathuyetND"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="column_auto">
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Đau họng<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_dauhong"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 400px; margin-top: 10px;">
                                    <div style="width: 180px; float: left; font-size: 15px; text-align: right;">
                                        Nổi ban ngoài da <span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; width: 210px;">
                                        <asp:RadioButtonList ID="rd_noibanND"
                                            CssClass="radio_cts" RepeatDirection="Horizontal" Width="190"
                                            runat="server">
                                            <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="column_auto" style="float: left; margin-top: 30px;">
                                <div style="width: 400px; float: left; font-size: 15px; text-align: left;">
                                    - Tiếp xúc gần với người mắc bệnh hoặc nghi ngờ mắc bệnh viêm đường hô hấp do nCoV<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                </div>
                                <div class="margin_auto" style="float: left; width: 210px;">
                                    <asp:RadioButtonList ID="rd_tiepxucgan"
                                        CssClass="radio_cts" RepeatDirection="Horizontal" Width="190" AutoPostBack="true" OnSelectedIndexChanged="rd_tiepxucgan_SelectedIndexChanged"
                                        runat="server">
                                        <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div runat="server" id="contai_tiepxucgan_div" style="float: left; display: none">
                                <div class="column_auto">
                                    <div style="float: left; width: 330px; margin-top: 10px;">
                                        <div style="width: 330px; float: left; font-size: 15px; text-align: left;">
                                            - Tự xác định F0-F4<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                        </div>
                                    </div>
                                    <div style="float: left; width: 400px; margin-top: 10px;">
                                        <div style="float: left; padding-left: 5px;">
                                            <asp:DropDownList ID="DropXACDINH_F" CssClass="chosen-select" runat="server" Width="380px">
                                                <asp:ListItem Value="" Text="--- chọn ---"></asp:ListItem>
                                                <asp:ListItem Value="F0" Text="F0: Người có kết luận dương tính với vi rút Covid 19"></asp:ListItem>
                                                <asp:ListItem Value="F1" Text="F1: Tiếp xúc trực tiếp với người dương tính với vi rút Covid 19 (F0)"></asp:ListItem>
                                                <asp:ListItem Value="F2" Text="F2: Tiếp xúc trực tiếp với người trong danh sách F1"></asp:ListItem>
                                                <asp:ListItem Value="F3" Text="F3:  Tiếp xúc trực tiếp với người trong danh sách F2"></asp:ListItem>
                                                <asp:ListItem Value="F4" Text="F4: Tiếp xúc trực tiếp với người trong danh sách F3"></asp:ListItem>
                                                <asp:ListItem Value="FN" Text="Nhóm có nguy cơ lây nhiễm: ở cùng tòa nhà, sử dụng chung thang máy, đến các địa điểm hội họp, sân golf... liên quan đến người F0, F1, F2"></asp:ListItem>
                                                <asp:ListItem Value="FF" Text="Chưa xác định được"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div style="float: left; width: 330px; margin-top: 10px;">
                                        <div style="width: 330px; float: left; font-size: 15px; text-align: left;">
                                            - Tình trạng cách ly y tế đang áp dụng (nếu có)<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                        </div>
                                    </div>
                                    <div style="float: left; width: 400px; margin-top: 10px;">
                                        <div style="float: left; padding-left: 5px;">
                                            <asp:DropDownList ID="Drop_TT_CACHLY" CssClass="chosen-select" AutoPostBack="true" OnSelectedIndexChanged="Drop_TT_CACHLY_SelectedIndexChanged" runat="server" Width="380px">
                                                <asp:ListItem Value="" Text="--- chọn ---"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Điều trị, theo dõi tập trung tại bệnh viện"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Cách ly tập trung tại cơ sở ý tế"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Cách ly tại nhà và có sự theo dõi của y tế địa phương"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Làm việc tại nhà theo yêu cầu của đơn vị"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="Đi làm việc bình thường tại cơ quan"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="Nghỉ phép theo quy định thông thường"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin_left_auto" runat="server" id="cachly_time_div" style="float: left; width: 500px; margin-top: 10px;">
                                    <div style="width: 140px; float: left; font-size: 15px; text-align: right;">
                                        + Cách ly từ ngày<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; margin-left: 5px;">
                                        <asp:TextBox ID="txt_CACHLY_TUNGAY" runat="server" CssClass="user"
                                            Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_CACHLY_TUNGAY"
                                            Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txt_CACHLY_TUNGAY"
                                            Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </div>
                                    <div style="width: 90px; float: left; font-size: 15px; text-align: right;">
                                        Đến ngày<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; margin-left: 5px;">
                                        <asp:TextBox ID="txt_CACHLY_DENGAY" runat="server" CssClass="user"
                                            Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_CACHLY_DENGAY"
                                            Format="dd/MM/yyyy" Enabled="true" />
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txt_CACHLY_DENGAY"
                                            Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                    </div>
                                </div>
                                <div class="column_auto" style="float: left; margin-top: 10px;">
                                    <div style="float: left; font-size: 15px; text-align: left;">
                                        - Lịch sử tiếp xúc với với người mắc bệnh hoặc nghi ngờ mắc bện viêm đường hô hấp do nCoV<span style="color: #ff0000; padding-left: 5px;">(*)</span>
                                    </div>
                                    <div style="float: left; padding-left: 5px; margin-top: 10px;">
                                        <asp:TextBox ID="txtLICHSU" CssClass="user ghichu_css" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="column_auto" style="float: left; margin-top: 10px;">
                                <div style="width: 400px; float: left; font-size: 15px; text-align: left;">
                                    - Ghi chú<span style="color: #ff0000; padding-left: 5px;"></span>
                                </div>
                                <div style="float: left; padding-left: 5px; margin-top: 10px;">
                                    <asp:TextBox ID="txt_GHICHU" CssClass="user ghichu_css" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left; width: 800px; margin-top: 20px;">
                                <div style="float: left; margin-left: 5px;">
                                    <asp:Button ID="cmdUpdate" runat="server" CssClass="buttoninput" Width="100px" Text="Gửi" OnClick="btnUpdate_Click" OnClientClick="return ValidateDataInput();" />
                                </div>
                                <div style="float: left; margin-left: 5px;">
                                    <asp:Button ID="cmdLammoi" runat="server" CssClass="buttoninput" Width="100px" Text="Làm mới" OnClick="btnLammoi_Click" />
                                </div>
                                <div style="float: left; margin-left: 25px; padding-top: 5px;">
                                    <asp:Label runat="server" ID="lbthongbao" Font-Size="17px" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="searchcontent">
                        <h4 class="tracuu_tleboxchung">Quá trình kê khai</h4>
                        <div class="border_search">
                            <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
                            <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
                            <asp:Panel ID="pnDS" runat="server">
                                <div class="phantrang" style="margin-top: 30px;">
                                    <div class="sobanghi">
                                        <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"><</asp:LinkButton>
                                        <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click">></asp:LinkButton>
                                        <asp:DropDownList ID="dropPageSize" runat="server" Width="65px"
                                            CssClass="dropbox"
                                            AutoPostBack="True" OnSelectedIndexChanged="dropPageSize_SelectedIndexChanged">
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                            <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                            <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div>
                                    <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand" Visible="true" OnItemDataBound="rpt_ItemDataBound">
                                        <HeaderTemplate>
                                            <div class="CSSTableGenerator">
                                                <table>
                                                    <tr id="row_header">
                                                        <td style="width: 20px;">TT</td>
                                                        <td style="width: 80px;">Ngày gửi</td>
                                                        <td style="width: 60px;">Sốt >=37.5 độ C</td>
                                                        <td style="width: 90px; text-align: center;">Tiếp xúc gần với người mắc bệnh hoặc nghi ngờ mắc bệnh viêm đường hô hấp do nCoV</td>
                                                        <td style="width: 80px;">Thời gian cách ly</td>
                                                        <td style="width: 100px;">Ghi chú</td>
                                                        <td style="width: 50px;">Tài khoản gửi</td>
                                                    </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;"><%#Eval("STT") %> </td>
                                                <td style="text-align: center;"><%#Eval("NGAYSUA") %> </td>
                                                <td style="text-align: center;"><%#Eval("SOT") %> </td>
                                                <td style="text-align: center;"><%#Eval("TIEPXUCGAN_BN") %> </td>
                                                <td style="text-align: center;"><%#Eval("TIME_CACHLY") %> </td>
                                                <td style="text-align: center;"><%#Eval("GHICHU") %> </td>
                                                <td style="text-align: center;"><%#Eval("NGUOITAO") %> </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table></div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="phantrang">
                                    <div class="sobanghi">
                                        <asp:HiddenField ID="hdicha" runat="server" />
                                        <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                                    </div>
                                    <div class="sotrang">
                                        <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                            OnClick="lbTBack_Click"><</asp:LinkButton>
                                        <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                            Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                            Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                            Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                            Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                            Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                                        <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                                        <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                            Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                            OnClick="lbTNext_Click">></asp:LinkButton>
                                        <asp:DropDownList ID="dropPageSize2" runat="server" Width="65px" CssClass="dropbox"
                                            AutoPostBack="True" OnSelectedIndexChanged="dropPageSize2_SelectedIndexChanged">
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                            <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                            <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <%-- content_form_body end--%>
            </div>
        </div>
    </div>
    <style>
        .sobanghi {
            font-size: 11pt;
        }
    </style>
    <script>
        function ValidateDataInput() {
            var txtDienThoai = document.getElementById('<%=txtDienThoai.ClientID %>');
            if (txtDienThoai.value.trim().length > 50) {
                alert('Điện thoại không được quá 50 ký tự. Hãy nhập lại!');
                txtDienThoai.focus();
                return false;
            }
            var txtEmail = document.getElementById('<%=txtEmail.ClientID %>');
            if (txtEmail.value.trim().length > 150) {
                alert('Email không được quá 150 ký tự. Hãy nhập lại!');
                txtEmail.focus();
                return false;
            }
            if (Common_CheckEmpty(txtEmail.value)) {
                if (!Common_ValidateEmail(txtEmail.value)) {
                    txtEmail.focus();
                    return false;
                }
            }
            var Drop_DonVi = document.getElementById('<%= Drop_DonVi.ClientID %>');
            if (!Common_CheckEmpty(Drop_DonVi.value)) {
                alert('Bạn chưa chọn mục "Nơi ở hiện tại(*)"!');
                Drop_DonVi.focus();
                return false;
            }
            var txt_DIACHI = document.getElementById('<%=txt_DIACHI.ClientID%>');
            if (!Common_CheckEmpty(txt_DIACHI.value)) {
                alert('Bạn chưa nhập mục "Địa chỉ chi tiết(*)"!');
                txt_DIACHI.focus();
                return false;
            }
            //------------------------------------
            var msg = "";
            //------
            var rdsot = document.getElementById('<%=rdSot.ClientID%>');
            msg = 'Mục "Triệu trứng sốt >=37.5 độ C(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rdsot, msg))
                return false;
            //------
            var rd_ho = document.getElementById('<%=rd_ho.ClientID%>');
            msg = 'Mục "Ho(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_ho, msg))
                return false;
            //------
            var rd_khotho = document.getElementById('<%=rd_khotho.ClientID%>');
            msg = 'Mục "Khó thở(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_khotho, msg))
                return false;
            //------
            var rd_dauhong = document.getElementById('<%=rd_dauhong.ClientID%>');
            msg = 'Mục "Đau họng(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_dauhong, msg))
                return false;
            //------
            var rd_non_buonnon = document.getElementById('<%=rd_non_buonnon.ClientID%>');
            msg = 'Mục "Nôn/buồn nôn(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_non_buonnon, msg))
                return false;
            //------
            var rd_tieuchay = document.getElementById('<%=rd_tieuchay.ClientID%>');
            msg = 'Mục "Tiêu chảy(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_tieuchay, msg))
                return false;
            //------
            var rd_xuathuyetND = document.getElementById('<%=rd_xuathuyetND.ClientID%>');
            msg = 'Mục "Xuất huyết ngoài da(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_xuathuyetND, msg))
                return false;
            //------
            var rd_noibanND = document.getElementById('<%=rd_noibanND.ClientID%>');
            msg = 'Mục "Nổi ban ngoài da(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_noibanND, msg))
                return false;
            //------
            var rd_tiepxucgan = document.getElementById('<%=rd_tiepxucgan.ClientID%>');
            msg = 'Mục "Tiếp xúc gần với người mắc bệnh hoặc nghi ngờ mắc bện viêm đường hô hấp do nCoV(*)" bắt buộc phải chọn. Hãy kiểm tra lại!';
            if (!CheckChangeRadioButtonList(rd_tiepxucgan, msg))
                return false;
            //------
            var selected_value = GetStatusRadioButtonList(rd_tiepxucgan);
            if (selected_value == '1') {
                var DropXACDINH_F = document.getElementById('<%= DropXACDINH_F.ClientID %>');
                if (!Common_CheckEmpty(DropXACDINH_F.value)) {
                    alert('Bạn chưa chọn mục "Tự xác định F0-F4(*)"!');
                    DropXACDINH_F.focus();
                    return false;
                }
                var Drop_TT_CACHLY = document.getElementById('<%= Drop_TT_CACHLY.ClientID %>');
                if (!Common_CheckEmpty(Drop_TT_CACHLY.value)) {
                    alert('Bạn chưa chọn mục "Tình trạng cách ly y tế đang áp dụng (nếu có)(*)"!');
                    Drop_TT_CACHLY.focus();
                    return false;
                }
                if (Drop_TT_CACHLY.value == '2' || Drop_TT_CACHLY.value == '3') {
                    var txt_CACHLY_TUNGAY = document.getElementById('<%=txt_CACHLY_TUNGAY.ClientID%>');
                    if (!Common_CheckEmpty(txt_CACHLY_TUNGAY.value)) {
                        alert('Bạn chưa nhập mục "Cách ly từ ngày(*)"!');
                        txt_CACHLY_TUNGAY.focus();
                        return false;
                    }
                    //if (!CheckDateTimeControl(txt_CACHLY_TUNGAY, 'Cách ly từ ngày(*)'))
                    //    return false;
                    var txt_CACHLY_DENGAY = document.getElementById('<%=txt_CACHLY_DENGAY.ClientID%>');
                    if (!Common_CheckEmpty(txt_CACHLY_DENGAY.value)) {
                        alert('Bạn chưa nhập mục "Đến ngày(*)"!');
                        txt_CACHLY_DENGAY.focus();
                        return false;
                    }
                    //if (!CheckDateTimeControl(txt_CACHLY_DENGAY, '"Đến ngày(*)'))
                    //    return false;
                }
                var txtLICHSU = document.getElementById('<%=txtLICHSU.ClientID%>');
                if (!Common_CheckEmpty(txtLICHSU.value)) {
                    alert('Bạn chưa nhập mục "Lịch sử tiếp xúc với với người mắc bệnh hoặc nghi ngờ mắc bệnh viêm đường hô hấp do nCoV(*)"!');
                    txtLICHSU.focus();
                    return false;
                }
            }
            //--------------
            return true;
        }
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>
