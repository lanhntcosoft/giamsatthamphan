﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;

namespace WEB.ZCOVID.UC
{
    public partial class Header_bep_an : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string strUserID = Session["SESSION_USERID"] + "";
                if (strUserID == "") Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login.aspx");
                lstUserName.Text = "Tài khoản: " + Session["SESSION_USERNAME"] + "";
                LoadMenu();
            }
        }
        protected void lkSigout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login_bep_an.aspx");
        }
        void LoadMenu()
        {
            ltt.Text = "";
            if (Session["SESSION_NHOM_ND_ID"] + "" == "1")//admin nhóm quản trị hệ thống
            {
                ltt.Text += "<a href='/TrangChu_bep_an.aspx?&suckhoeID=1' class='" + SetMenuClass("/TrangChu_bep_an.aspx") + "'>Đăng ký</a>";
                ltt.Text += "<a href='/DanhSach_bep_an.aspx' class='" + SetMenuClass("/DanhSach.aspx") + "'>Danh sách đăng ký</a>";
                ltt.Text += "<a href='/Danhmuc/Canbo/Danhsach_bep_an.aspx?&suckhoeID=2' class='" + SetMenuClass("/Danhmuc/Canbo/Danhsach_bep_an.aspx") + "'>Danh mục cán bộ</a>";
                ltt.Text += "<a href='/Quantri/Nguoidung/Danhsach_bep_an.aspx?&suckhoeID=3' class='" + SetMenuClass("/Quantri/Nguoidung/Danhsach_bep_an.aspx") + "'>Quản lý người dùng</a>";
                //ltt.Text += "<a href='/baocao.aspx?&suckhoeID=4' class='" + SetMenuClass("/baocao.aspx") + "'>Báo cáo</a>";
            }
            else if (Session["SESSION_NHOM_ND_ID"] + "" == "2")//admin nhóm người dùng
            {
                ltt.Text += "<a href='/TrangChu_bep_an.aspx?&suckhoeID=1' class='" + SetMenuClass("/TrangChu_bep_an.aspx") + "'>Đăng ký</a>";

                COVID_DM_CANBO_BL obj = new COVID_DM_CANBO_BL();
                DataTable tbl = obj.GET_CHUCVU_CHECK(Session["SESSION_CANBO_ID"] + "");
                //45 Chánh án,426 Viện trưởng,432 Vụ trưởng,444 Chánh Văn phòng
                //,451 Giám đốc,455 Cục trưởng,428 Tổng biên tập 
                if (tbl.Rows.Count != 0)
                {
                    ltt.Text += "<a href='/DanhSach_bep_an.aspx' class='" + SetMenuClass("/DanhSach_bep_an.aspx") + "'>Danh sách đăng ký</a>";
                }
                //ltt.Text += "<a href='/baocao.aspx?&suckhoeID=4' class='" + SetMenuClass("/baocao.aspx") + "'>Báo cáo</a>";
            }
            if (Session["SESSION_NHOM_ND_ID"] + "" == "3")//nhóm quyền quản lý 
            {
                ltt.Text += "<a href='/TrangChu_bep_an.aspx?&suckhoeID=1' class='" + SetMenuClass("/TrangChu_bep_an.aspx") + "'>Đăng ký</a>";
                ltt.Text += "<a href='/DanhSach_bep_an.aspx' class='" + SetMenuClass("/DanhSach_bep_an.aspx") + "'>Danh sách đăng ký</a>";
            }
        }
        string SetMenuClass(string pagename)
        {
            String mn_active = "menubuttonActive";
            String mn_noactive = "menubutton";

            String menu_class = "";
            string CurrPage = HttpContext.Current.Request.Url.PathAndQuery.ToLower();
            if (CurrPage.Contains(pagename.ToLower()))
                menu_class = mn_active;
            else
                menu_class = mn_noactive;
            return menu_class;
        }
    }
}