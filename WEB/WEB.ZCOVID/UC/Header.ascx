﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="WEB.ZCOVID.UC.Header" %>
<div class="header">
    <div class="headertop">
        <a href="http://kbyt.toaan.gov.vn/" target="_blank">
            <div class="logo_toaan"></div>
        </a>
        <div class="logo_text" style="line-height: 30px; top: 0px; left: 90px; position: absolute; text-align: left;">
            TÒA ÁN NHÂN DÂN TỐI CAO <br/>
            PHẦN MỀM HỖ TRỢ KHAI BÁO Ý TẾ PHỤC VỤ CÔNG TÁC PHÒNG, CHỐNG DỊCH COVID 19
        </div>
        <div class="taikhoan">
            <div class="userinfo">
                <div class="dropdown">
                    <a href="javascript:;" class="dropbtn userinfo_ico">
                        <asp:Literal ID="lstUserName" runat="server"></asp:Literal></a>

                    <div class="menu_child">
                        <div class="arrow_up_border"></div>
                        <div class="arrow_up"></div>
                        <ul>
                            <li class="singout">
                                <asp:LinkButton ID="lkSigout" runat="server"
                                    OnClick="lkSigout_Click" Text="Đăng xuất"></asp:LinkButton>
                            </li>
                            <li class="changepass infors"><a href="/User_Infor.aspx">Thông tin người dùng</a></li>
                            <li class="changepass"><a href="/ChangePass.aspx">Đổi mật khẩu</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="content_form">
            <div class="msg_thongbao">
                <asp:Literal ID="lbthongbao" runat="server"></asp:Literal>
            </div>
            <asp:Literal ID="ltt" runat="server"></asp:Literal>
        </div>
    </div>
</div>
<div style="clear: both;"></div>
