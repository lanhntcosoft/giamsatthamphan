﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;
using System.Data;

namespace WEB.ZCOVID
{
    public partial class User_Infor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string strUserID = Session["SESSION_USERID"] + "";
                    if (strUserID == "") Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login.aspx");
                    txtUserName.Text = Session["SESSION_USERNAME"] + "";
                    decimal current_id = Convert.ToDecimal(Session["SESSION_USERID"] + "");
                    Load_User_Info();
                }
                catch (Exception ex)
                { lttMsg.Text = ex.Message; }

            }
        }
        protected void Load_User_Info()
        {
            COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
            DataTable tbl = obj.Get_UserByID(Session["SESSION_USERID"] + "");
            txt_HOTEN.Text = tbl.Rows[0]["HOTEN"]+"";
            txt_DIENTHOAI.Text = tbl.Rows[0]["SODIENTHOAI"] + "";
            txt_EMAIL.Text = tbl.Rows[0]["EMAIL"] + "";
            if (tbl.Rows[0]["DONVI_ID"] + "" != "")
            {
                Drop_DonVi.SelectedValue = tbl.Rows[0]["DONVI_ID"] + "";
            }
            txt_DIACHI.Text = tbl.Rows[0]["DIACHI"] + "";
            LoadDonvi();
        }
        private void LoadDonvi()
        {
            Drop_DonVi.Items.Clear();
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            Drop_DonVi.DataSource = oBL.Get_DonViFull();
            Drop_DonVi.DataTextField = "MA_TEN";
            Drop_DonVi.DataValueField = "ID";
            Drop_DonVi.DataBind();
            Drop_DonVi.Items.Insert(0, new ListItem("-- Chọn đơn vị cấp quận, huyện --", ""));
        }
        protected void cmdThoat_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login.aspx");
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            if (txt_DIENTHOAI.Text.Trim() == "")
            {
                lttMsg.Text = "Bạn chưa nhập số điện thoại.";
                return;
            }
            else
            {
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                if (obj.Update_UserInfor(Session["SESSION_CANBO_ID"] + "", txt_DIENTHOAI.Text.Trim(), txt_EMAIL.Text.Trim(), Drop_DonVi.SelectedValue, txt_DIACHI.Text.Trim()))
                    lttMsg.Text = "Bạn đã cập nhật thông tin thành công.";
            }
        }
    }
}