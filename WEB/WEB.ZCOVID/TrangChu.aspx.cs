﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
using WEB.ZCOVID.BL;
namespace WEB.ZCOVID
{
    public partial class TrangChu : System.Web.UI.Page
    {
        CultureInfo cul = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            Decimal CurrUser = (string.IsNullOrEmpty(Session["SESSION_USERID"] + "")) ? 0 : Convert.ToDecimal(Session["SESSION_USERID"] + "");
            if (CurrUser == 0)
            {
                Response.Redirect(Cls_Comon_covid.GetRootURL() + "/Login.aspx");
            }
            if (!IsPostBack)
            {
                txt_DV_TRUCTHUOC_TEN.Text= Session["SESSION_DV_TRUCTHUOC_TEN"] + "";
                COVID_NGUOISUDUNG_BL obj = new COVID_NGUOISUDUNG_BL();
                DataTable tbl = obj.Get_UserByID(Session["SESSION_USERID"] + "");
                txt_HOTEN.Text = tbl.Rows[0]["HOTEN"] + "";
                txtDienThoai.Text = tbl.Rows[0]["SODIENTHOAI"] + "";
                txtEmail.Text = tbl.Rows[0]["EMAIL"] + "";
                txt_DIACHI.Text= tbl.Rows[0]["DIACHI"] + "";
                if (tbl.Rows[0]["DONVI_ID"] + "" != "")
                {
                    Drop_DonVi.SelectedValue = tbl.Rows[0]["DONVI_ID"] + "";
                }
                LoadDonvi();
                LoadData();
                Load_F();
                Load_THONGKE_F_HOTEN();
                Load_TK_CANHBAO();
                COVID_DM_CANBO_BL obj_cb = new COVID_DM_CANBO_BL();
                DataTable tbl_cb = obj_cb.GET_CHUCVU_CHECK(Session["SESSION_CANBO_ID"] + "");
                //45 Chánh án,426 Viện trưởng,432 Vụ trưởng,444 Chánh Văn phòng
                //,451 Giám đốc,455 Cục trưởng,428 Tổng biên tập 
                if (tbl_cb.Rows.Count != 0)
                {
                    canhbao_ld.Style.Remove("Display");
                }
                else if (tbl_cb.Rows.Count == 0)
                {
                    canhbao_ld.Style.Add("Display", "none");
                }
            }
        }
        protected void Link_da_KBYT_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("DanhSach.aspx?&trangthai=1");
        }
        protected void Link_chua_KBYT_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("DanhSach.aspx?&trangthai=2");
        }
        private void LoadDonvi()
        {
            Drop_DonVi.Items.Clear();
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            Drop_DonVi.DataSource = oBL.Get_DonViFull();
            Drop_DonVi.DataTextField = "MA_TEN";
            Drop_DonVi.DataValueField = "ID";
            Drop_DonVi.DataBind();
            Drop_DonVi.Items.Insert(0, new ListItem("-- Chọn đơn vị cấp quận, huyện --", ""));
        }
        protected void rd_tiepxucgan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rd_tiepxucgan.SelectedValue == "1")
            {
                contai_tiepxucgan_div.Style.Remove("Display");
                Cls_Comon_covid.SetFocus(this, this.GetType(), DropXACDINH_F.ClientID);
            }
            else
            {
                contai_tiepxucgan_div.Style.Add("Display", "none");
                Cls_Comon_covid.SetFocus(this, this.GetType(), txt_GHICHU.ClientID);
            }
            Load_TT_CACHLY();
        }
        public void Load_TT_CACHLY()
        {
            if (Drop_TT_CACHLY.SelectedValue == "2" || Drop_TT_CACHLY.SelectedValue == "3")
            {

                cachly_time_div.Style.Remove("Display");
                Cls_Comon_covid.SetFocus(this, this.GetType(), txt_CACHLY_TUNGAY.ClientID);
            }
            else
            {
                cachly_time_div.Style.Add("Display", "none");
                Cls_Comon_covid.SetFocus(this, this.GetType(), txtLICHSU.ClientID);
            }
        }
        protected void Drop_TT_CACHLY_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_TT_CACHLY();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if(Session["SESSION_USERNAME"] + "" =="admin")
            {
                lbthongbao.Text = "Tài khoản 'admin' không được phân quyền gửi khai báo y tế.";
                return;
            }
            if(txtDienThoai.Text.Trim()=="")
            {
                lbthongbao.Text = "Bạn chưa nhập số điện thoại.";
                return;
            }
            else
            {
                try
                {
                    COVID_DANGKY obj = new COVID_DANGKY();
                    obj.ID = 0;
                    obj.CANBOID = Convert.ToDecimal(Session["SESSION_CANBO_ID"] + "");
                    obj.TOAANID = 1;
                    obj.DV_TRUCTHUOC = Convert.ToDecimal(Session["SESSION_DV_TRUCTHUOID"] + "");
                    if (Session["SESSION_PHONGBANID"] + "" != "")
                        obj.PHONGBANID = Convert.ToDecimal(Session["SESSION_PHONGBANID"] + "");
                    obj.SOT = Convert.ToDecimal(rdSot.SelectedValue);
                    obj.HO = Convert.ToDecimal(rd_ho.SelectedValue);
                    obj.KHO_THO = Convert.ToDecimal(rd_khotho.SelectedValue);
                    obj.DAU_HONG = Convert.ToDecimal(rd_dauhong.SelectedValue);
                    obj.NON_BUON_NON = Convert.ToDecimal(rd_non_buonnon.SelectedValue);
                    obj.TIEU_CHAY = Convert.ToDecimal(rd_tieuchay.SelectedValue);
                    obj.XUAT_HUYET_ND = Convert.ToDecimal(rd_xuathuyetND.SelectedValue);
                    obj.NOIBAN_ND = Convert.ToDecimal(rd_noibanND.SelectedValue);
                    obj.TIEPXUCGAN_BN = Convert.ToDecimal(rd_tiepxucgan.SelectedValue);
                    obj.XACDINH_F = DropXACDINH_F.SelectedValue;
                    obj.TT_CACHLY = Drop_TT_CACHLY.SelectedValue;
                    if(Drop_TT_CACHLY.SelectedValue=="2" || Drop_TT_CACHLY.SelectedValue=="3")
                    {
                        obj.CACHLY_TUNGAY= DateTime.Parse(this.txt_CACHLY_TUNGAY.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                        obj.CACHLY_DENGAY = DateTime.Parse(this.txt_CACHLY_DENGAY.Text.Trim(), cul, DateTimeStyles.NoCurrentDateDefault);
                    }
                    obj.LICHSU = txtLICHSU.Text.Trim();
                    obj.GHICHU = txt_GHICHU.Text.Trim();
                    obj.NGUOITAO = Session["SESSION_USERNAME"] + "";
                    COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
                    if (oBL.Covid_Dangky_Ins_Up(obj, txtDienThoai.Text.Trim(), txtEmail.Text.Trim(),Drop_DonVi.SelectedValue,txt_DIACHI.Text.Trim()) == true)
                    {
                        lbthongbao.Text = "Bạn đã gửi thông tin khai báo y tế thành công.";
                        LoadData();
                        Load_F();
                        Load_THONGKE_F_HOTEN();
                        Load_TK_CANHBAO();
                        Clear_values();
                    }
                }
                catch (Exception ex)
                {
                    lbthongbao.Text = "Đã có lỗi xảy ra, hãy liên hệ với quản trị viên để được hỗ trợ " + ex.Message;
                    return;
                }

            }
        }
        protected void btnLammoi_Click(object sender, EventArgs e)
        {
            lbthongbao.Text = string.Empty;
            Clear_values();
        }
        public void Load_F()
        {
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            tbl = oBL.THONGKE_F_LIST(null);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            Li_THONGKE_F.Text = row["TEXT_REPORT"] + "";
        }
        public void Load_TK_CANHBAO()
        {
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            tbl = oBL.THONGKE_CANH_BAO_LIST(Session["SESSION_CANBO_ID"] + "");
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            Li_TK_CANHBAO.Text = row["TEXT_REPORT"] + "";
            Link_da_KBYT.Text= row["DADANGKY"] + "";
            Link_chua_KBYT.Text = row["CHUADANGKY"] + "";
        }
        public void Load_THONGKE_F_HOTEN()
        {
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable tbl = new DataTable();
            DataRow row = tbl.NewRow();
            tbl = oBL.THONGKE_F_HOTEN_LIST(null);
            //-----------
            if (tbl != null && tbl.Rows.Count > 0)
            {
                row = tbl.Rows[0];
            }
            Li_THONGKE_F_HOTEN.Text = row["TEXT_REPORT"] + "";
        }
        public void Clear_values()
        {
            rdSot.ClearSelection();
            rd_ho.ClearSelection();
            rd_khotho.ClearSelection();
            rd_dauhong.ClearSelection();
            rd_non_buonnon.ClearSelection();
            rd_noibanND.ClearSelection();
            rd_tieuchay.ClearSelection();
            rd_xuathuyetND.ClearSelection();
            rd_noibanND.ClearSelection();
            rd_tiepxucgan.ClearSelection();
            contai_tiepxucgan_div.Style.Add("Display", "none");
            DropXACDINH_F.SelectedValue = "";
            Drop_TT_CACHLY.SelectedValue = "";
            txt_CACHLY_TUNGAY.Text = string.Empty;
            txt_CACHLY_DENGAY.Text = string.Empty;
            txtLICHSU.Text = string.Empty;
            txt_GHICHU.Text = string.Empty;
        }
        void LoadData()
        {
            int page_size = Convert.ToInt32(dropPageSize.SelectedValue);
            int pageindex = Convert.ToInt32(hddPageIndex.Value);
            COVID_DANGKY_BL oBL = new COVID_DANGKY_BL();
            DataTable oDT = oBL.Covid_Dangky_List(Session["SESSION_CANBO_ID"]+"", pageindex, page_size);
            int count_all = 0;
            if (oDT.Rows.Count > 0)
                count_all = Convert.ToInt32(oDT.Rows[0]["CountAll"] + "");
            if (oDT != null && count_all > 0)
            {
                #region "Xác định số lượng trang"
                hddTotalPage.Value = Cls_Comon_covid.GetTotalPage(count_all, Convert.ToInt32(dropPageSize.SelectedValue)).ToString();
                lstSobanghiT.Text = lstSobanghiB.Text = "Có <b>" + count_all.ToString() + " </b> bản ghi trong <b>" + hddTotalPage.Value + "</b> trang";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                             lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                #endregion
            }
            else
            {
                hddTotalPage.Value = "1";
                Cls_Comon_covid.SetPageButton(hddTotalPage, hddPageIndex, lbTFirst, lbBFirst, lbTLast, lbBLast, lbTNext, lbBNext, lbTBack, lbBBack, lbTStep1, lbBStep1, lbTStep2,
                           lbBStep2, lbTStep3, lbBStep3, lbTStep4, lbBStep4, lbTStep5, lbBStep5, lbTStep6, lbBStep6);
                lstSobanghiT.Text = lstSobanghiB.Text = "Không tồn tại dữ liệu kê khai";
            }
            rpt.DataSource = oDT;
            rpt.DataBind();
        }
        protected void dropPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize2.SelectedValue = dropPageSize.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        protected void dropPageSize2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropPageSize.SelectedValue = dropPageSize2.SelectedValue;
            hddPageIndex.Value = "1";
            LoadData();
        }
        protected void rpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }
        #region "Phân trang"
        protected void lbTBack_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) - 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = "1";
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTLast_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = Convert.ToInt32(hddTotalPage.Value).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTNext_Click(object sender, EventArgs e)
        {
            try
            {
                hddPageIndex.Value = (Convert.ToInt32(hddPageIndex.Value) + 1).ToString();
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        protected void lbTStep_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbCurrent = (LinkButton)sender;
                hddPageIndex.Value = lbCurrent.Text;
                LoadData();
            }
            catch (Exception ex) { lbthongbao.Text = ex.Message; }
        }
        #endregion
    }
}