﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dangkybep_an.Master" AutoEventWireup="true" CodeBehind="User_Infor_bep_an.aspx.cs" Inherits="WEB.ZCOVID.User_Infor_bep_an" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="UI/js/Common.js"></script>
    <link href="/UI/css/style.css" rel="stylesheet" />
    <link href="/UI/img/spcLogo.png" type="image/png" rel="shortcut icon" />
    <link href="/UI/css/chosen.css" rel="stylesheet" />
    <link href="/UI/css/jquery-ui.css" rel="stylesheet" />
    <script src="/UI/js/jquery-3.3.1.js"></script>
    <script src="/UI/js/jquery-ui.min.js"></script>
    <style type="text/css">
        body {
            margin: 0 auto;
            padding: 0;
            font-family: arial;
            font-size: 13px;
            background-color: #ffffff;
        }

        #login_frame {
            margin: 0px;
            width: 100%;
            height: 520px;
            background: url(/UI/img/bglauncher.png);
            background-size: 100% 100%;
            float: left;
            border-top: 1px solid #a91d1f;
        }

        #logo {
            width: 100%;
            height: 130px;
            margin-left: auto;
            margin-right: auto;
            background: url(UI/img/lgheader.png) no-repeat center;
        }

        #signin_frame {
            width: 685px;
            height: 420px;
            margin-top: 57px;
            background: none;
            background-color: #ffffff;
            margin-left: auto;
            margin-right: auto;
        }

        #titleds {
            float: left;
            width: 100%;
            text-align: center;
            margin-top: 0px;
            height: 36px;
            margin-bottom: 3px;
            line-height: 36px;
            color: #ffffff;
            font-size: 16px;
            font-weight: bold;
            background-color: #be1e1e;
        }

        .ctitem {
            float: left;
            width: 20%;
            height: 250px;
            padding-top: 35px;
        }

            .ctitem:hover {
                background: url(UI/img/bg_item.png);
            }

        .ctitemActive {
            float: left;
            width: 20%;
            height: 250px;
            background: url(UI/img/bg_item.png);
        }

        .khungnhap {
            width: 60%;
            margin-top: 13px;
            margin-left: 20%;
        }

            .khungnhap .box {
                display: block;
                float: left;
                margin-left: 12px;
                width: 90%;
                margin-bottom: 1px;
                border: none;
            }

            .khungnhap .textfield {
                display: block;
                height: 30px;
                width: 90%;
                margin: 8px 0;
                text-indent: 15px;
                border: solid 1px #8c0a0c;
                border-radius: 4px 4px 4px 4px;
            }

        .button {
            margin-top: 8px;
            background: none;
        }

        .menu {
            margin-bottom: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel" runat="server">
        <ContentTemplate>
            <div id="login_frame" style="height: 650px">
                <div id="signin_frame" style="height: 650px">
                    <div id="titleds">THÔNG TIN NGƯỜI DÙNG</div>
                    <div class="khungnhap">

                        <div class="box">
                            <i>Tên đăng nhập</i>
                            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="true" Enabled="false" paceholder="Mã truy cập" CssClass="textfield" BorderColor="#808080"></asp:TextBox>
                        </div>

                        <div class="box">
                            <i>Họ tên</i>
                            <asp:TextBox ID="txt_HOTEN" runat="server" CssClass="textfield" Enabled="false" BorderColor="#808080"></asp:TextBox>
                        </div>
                        <div class="box">
                            <i>Điện thoại</i>
                            <asp:TextBox ID="txt_DIENTHOAI" runat="server" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div class="box">
                            <i>Địa chỉ Email</i>
                            <asp:TextBox ID="txt_EMAIL" runat="server" CssClass="textfield"></asp:TextBox>
                        </div>
                        <div style="float: left; width: 336px; font-size: 16px; margin-top: 10px;">
                            <div style="width: 200px; float: left; font-size: 14px; font-style: italic; padding-left: 12px;">Nơi ở hiện tại<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>

                        </div>
                        <div style="float: left; width: 336px; font-size: 16px; margin-top: 10px;">
                            <div style="width: 517px; float: left; margin-left: 10px;">
                                <asp:DropDownList CssClass="chosen-select" ID="Drop_DonVi" runat="server" Width="336px"></asp:DropDownList>
                            </div>
                        </div>
                        <div style="float: left; width: 336px; font-size: 16px; margin-top: 10px; font-style: italic;">
                            <div style="width: 200px; float: left; font-size: 14px; padding-left: 12px;">Địa chỉ chi tiết<span style="color: #ff0000; padding-left: 5px;">(*)</span></div>
                        </div>
                        <div style="float: left; width: 336px; font-size: 16px; margin-top: 10px;">
                            <div style="width: 336px; float: left; margin-left: 10px;">
                                <asp:TextBox ID="txt_DIACHI" CssClass="textfield height_fix" runat="server" TextMode="MultiLine" Rows="2" Width="336px"></asp:TextBox>
                            </div>
                            <style>
                                .height_fix {
                                    height: auto !important;
                                }
                            </style>
                        </div>
                        <div style="width: 100%; float: left; margin-left: 13px; color: #da0421; font-weight: bold; margin-top: 4px;">
                            <asp:Literal ID="lttMsg" runat="server"></asp:Literal>
                        </div>
                        <div style="margin-left: 10px; margin-top: 10px; float: left;">
                            <asp:Button ID="cmdLogIn" CssClass="button_checkchkso" Text="Lưu thông tin" runat="server" OnClick="cmdSave_Click" OnClientClick="return ValidateDataInput();" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel">
        <ProgressTemplate>
            <div class="processmodal">
                <div class="processcenter">
                    <img src="UI/img/process.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script>
        function ValidateDataInput() {
            var txt_EMAIL = document.getElementById('<%=txt_EMAIL.ClientID %>');
            if (txt_EMAIL.value.trim().length > 150) {
                alert('Email không được quá 150 ký tự. Hãy nhập lại!');
                txtEmail.focus();
                return false;
            }
            if (Common_CheckEmpty(txt_EMAIL.value)) {
                if (!Common_ValidateEmail(txt_EMAIL.value)) {
                    txt_EMAIL.focus();
                    return false;
                }
            }
            var Drop_DonVi = document.getElementById('<%= Drop_DonVi.ClientID %>');
            if (!Common_CheckEmpty(Drop_DonVi.value)) {
                alert('Bạn chưa chọn mục "Nơi ở hiện tại(*)"!');
                Drop_DonVi.focus();
                return false;
            }
            var txt_DIACHI = document.getElementById('<%=txt_DIACHI.ClientID%>');
            if (!Common_CheckEmpty(txt_DIACHI.value)) {
                alert('Bạn chưa nhập mục "Địa chỉ chi tiết(*)"!');
                txt_DIACHI.focus();
                return false;
            }
            //--------------
            return true;
        }
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>