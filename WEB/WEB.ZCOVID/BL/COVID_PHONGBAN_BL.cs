﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;

namespace WEB.ZCOVID.BL
{
    public class COVID_PHONGBAN_BL
    {
        public DataTable GET_PHONGBAN_LIST(String V_TRUCTHUOC_ID)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_TRUCTHUOC_ID",V_TRUCTHUOC_ID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_PHONGBAN", pr);
        }
    }
}