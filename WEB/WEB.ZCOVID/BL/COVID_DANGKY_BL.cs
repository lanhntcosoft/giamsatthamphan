﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
namespace WEB.ZCOVID.BL
{
    public class COVID_DANGKY_BL
    {
        public bool Covid_Dangky_Ins_Up(COVID_DANGKY obj,String _SODIENTHOAI, String _EMAIL,string _DONVI_ID, string _DIACHI)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.COVID_DANGKY_INS_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_ID"].Value = obj.ID;
            comm.Parameters["V_CANBOID"].Value = obj.CANBOID;
            comm.Parameters["V_TOAANID"].Value = obj.TOAANID;
            comm.Parameters["V_DV_TRUCTHUOC"].Value = obj.DV_TRUCTHUOC;
            comm.Parameters["V_PHONGBANID"].Value = obj.PHONGBANID;
            comm.Parameters["V_SOT"].Value = obj.SOT;
            comm.Parameters["V_HO"].Value = obj.HO;
            comm.Parameters["V_KHO_THO"].Value = obj.KHO_THO;
            comm.Parameters["V_DAU_HONG"].Value = obj.DAU_HONG;
            comm.Parameters["V_NON_BUON_NON"].Value = obj.NON_BUON_NON;
            comm.Parameters["V_TIEU_CHAY"].Value = obj.TIEU_CHAY;
            comm.Parameters["V_XUAT_HUYET_ND"].Value = obj.XUAT_HUYET_ND;
            comm.Parameters["V_NOIBAN_ND"].Value = obj.NOIBAN_ND;
            comm.Parameters["V_TIEPXUCGAN_BN"].Value = obj.TIEPXUCGAN_BN;
            comm.Parameters["V_XACDINH_F"].Value = obj.XACDINH_F;
            comm.Parameters["V_TT_CACHLY"].Value = obj.TT_CACHLY;
            comm.Parameters["V_CACHLY_TUNGAY"].Value = obj.CACHLY_TUNGAY;
            comm.Parameters["V_CACHLY_DENGAY"].Value = obj.CACHLY_DENGAY;
            comm.Parameters["V_LICHSU"].Value = obj.LICHSU;
            comm.Parameters["V_GHICHU"].Value = obj.GHICHU;
            comm.Parameters["V_NGUOITAO"].Value = obj.NGUOITAO;
            comm.Parameters["V_NGUOISUA"].Value = obj.NGUOISUA;
            //------
            comm.Parameters["V_SODIENTHOAI"].Value = _SODIENTHOAI;
            comm.Parameters["V_EMAIL"].Value = _EMAIL;
            comm.Parameters["V_DONVI_ID"].Value = _DONVI_ID;
            comm.Parameters["V_DIACHI"].Value = _DIACHI;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable Covid_Dangky_List(string V_CANBOID, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBOID",V_CANBOID),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
            };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.COVID_DANGKY_DS", parameters);
            return tbl;
        }
        public DataTable Covid_Dangky_List_All(String V_DENNGAY, String v_TRANGTHAI,String vDV_TRUCTHUOC, String vPhongbanID, String ChucDanhID, String ChucVuID, String KeySearch, string V_CANBOID, decimal PageIndex, decimal PageSize)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_DENNGAY",V_DENNGAY),
                new OracleParameter("v_TRANGTHAI",v_TRANGTHAI),
                new OracleParameter("vDV_TRUCTHUOC",vDV_TRUCTHUOC),
                new OracleParameter("vPhongbanID",vPhongbanID),
                new OracleParameter("vChucDanhID",ChucDanhID),
                new OracleParameter("vChucVuID",ChucVuID),
                new OracleParameter("KeySearch",KeySearch),
                new OracleParameter("V_CANBOID",V_CANBOID),
                new OracleParameter("PageIndex",PageIndex),
                new OracleParameter("PageSize",PageSize)
            };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID_APP.COVID_DANGKY_DS_ALL", parameters);
            return tbl;
        }
        public DataTable Get_DonViFull()
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_DONVI_FULL", pr);
        }
        public DataTable THONGKE_F_LIST(string V_CANBOID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBOID",V_CANBOID)
                };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.THONGKE_F", parameters);
            return tbl;
        }
        public DataTable THONGKE_F_HOTEN_LIST(string V_CANBOID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBOID",V_CANBOID)
                };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.THONGKE_F_HOTEN", parameters);
            return tbl;
        }
        public DataTable THONGKE_CANH_BAO_LIST(string V_CANBOID)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBOID",V_CANBOID)
                };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID_APP.CANH_BAO_TK", parameters);
            return tbl;
        }
        public DataTable THONGKE_BAOCAO_EXPORT()
        {
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue)
                };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID_APP.COVID_BAOCAO", parameters);
            return tbl;
        }
    }
}