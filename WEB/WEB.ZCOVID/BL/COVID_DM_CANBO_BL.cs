﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
namespace WEB.ZCOVID.BL
{
    public class COVID_DM_CANBO_BL
    {
        public DataTable GET_CANBO_MA(String V_MACANBO)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_MACANBO",V_MACANBO),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_CANBO_MA", pr);
        }
        public DataTable GET_CANBO_ID(decimal V_ID)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_ID",V_ID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_CANBO_ID", pr);
        }
        public DataTable GET_CHUCVU_CHECK(String V_CANBOID)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                 new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_CANBOID",V_CANBOID),
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID_APP.GET_CHUCVU_CHECK", pr);
        }
        public DataTable GET_CANBO_DV_PHONGBAN(String V_DV_TRUCTHUOC, String V_PHONGBANID)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_DV_TRUCTHUOC",V_DV_TRUCTHUOC),
                new OracleParameter("V_PHONGBANID",V_PHONGBANID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_CANBO_DV_PB", pr);
        }
        public bool DELETE_CANBO_BYID(Decimal v_ID,ref string V_THONGBAO)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.DELETE_CANBO_ID", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["v_ID"].Value = v_ID;
            comm.Parameters["V_THONGBAO"].Direction = ParameterDirection.Output;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                V_THONGBAO =Convert.ToString(comm.Parameters["V_THONGBAO"].Value);
                conn.Close();
            }
        }
        public bool COVID_DM_CANBO_INS_UP(COVID_DM_CANBO obj)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.COVID_DM_CANBO_INS_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["v_ID"].Value = obj.ID;
            comm.Parameters["v_TOAANID"].Value = obj.TOAANID;
            comm.Parameters["v_MACANBO"].Value = obj.MACANBO;
            comm.Parameters["v_HOTEN"].Value = obj.HOTEN;
            comm.Parameters["v_SOCMND"].Value = obj.SOCMND;
            comm.Parameters["v_HIEULUC"].Value = obj.HIEULUC;
            comm.Parameters["v_CHUCDANHID"].Value = obj.CHUCDANHID;
            comm.Parameters["v_CHUCVUID"].Value = obj.CHUCVUID;
            comm.Parameters["v_DIACHI"].Value = obj.DIACHI;
            comm.Parameters["v_NGAYNHANCONGTAC"].Value = obj.NGAYNHANCONGTAC;
            comm.Parameters["v_NGAYSINH"].Value = obj.NGAYSINH;
            comm.Parameters["v_SODIENTHOAI"].Value = obj.SODIENTHOAI;
            comm.Parameters["v_NGAYTAO"].Value = obj.NGAYTAO;
            comm.Parameters["v_NGUOITAO"].Value = obj.NGUOITAO;
            comm.Parameters["v_NGAYBONHIEM"].Value = obj.NGAYBONHIEM;
            comm.Parameters["v_NGAYKETTHUC"].Value = obj.NGAYKETTHUC;
            comm.Parameters["v_GIOITINH"].Value = obj.GIOITINH;
            comm.Parameters["v_PHONGBANID"].Value = obj.PHONGBANID;
            comm.Parameters["v_DV_TRUCTHUOC"].Value = obj.DV_TRUCTHUOC;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable COVID_DM_CANBO_SEARCH(String vDV_TRUCTHUOC, String vPhongbanID, String ChucDanhID, String ChucVuID, String v_TrangThaiCongTac, String KeySearch, int curPageIndex, int pageSize)
        {
            OracleParameter[] parameters = new OracleParameter[]
            {
                new OracleParameter("vDV_TRUCTHUOC",vDV_TRUCTHUOC),
                new OracleParameter("vPhongbanID",vPhongbanID),
                new OracleParameter("vChucDanhID",ChucDanhID),
                new OracleParameter("vChucVuID",ChucVuID),
                new OracleParameter("v_TrangThaiCongTac",v_TrangThaiCongTac),
                new OracleParameter("KeySearch",KeySearch),
                new OracleParameter("CurrPageIndex",curPageIndex),
                new OracleParameter("PageSize",pageSize),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
            };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.DM_CANBO_SEARCH", parameters);
            return tbl;
        }
        public DataTable DM_DATAITEM_GETBYGROUPNAME(string vGroupName)
        {
            OracleParameter[] parameters = new OracleParameter[] {
                                                                        new OracleParameter("vGroupName",vGroupName),
                                                                        new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output )
                                                                      };
            DataTable tbl = Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.DM_DATAITEM_GETBYGROUPNAME", parameters);
            return tbl;
        }

    }
}