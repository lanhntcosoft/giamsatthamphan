﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WEB.ZCOVID.Module;
using WEB.ZCOVID.INFOR;
namespace WEB.ZCOVID.BL
{
    public class COVID_NGUOISUDUNG_BL
    {
        public DataTable Login(String V_USERNAME, String V_PASSWORD)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_USERNAME",V_USERNAME),
                new OracleParameter("V_PASSWORD",V_PASSWORD),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.USER_LOGIN", pr);
        }
        public bool COVID_NGUOISUDUNG_INS_UP(COVID_NGUOISUDUNG obj, ref string V_THONGBAO)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.COVID_NGUOIDUNG_INS_UP", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_ID"].Value = obj.ID;
            comm.Parameters["V_USERNAME"].Value = obj.USERNAME;
            comm.Parameters["V_PASSWORD"].Value = obj.PASSWORD;
            comm.Parameters["V_HOTEN"].Value = obj.HOTEN;
            comm.Parameters["V_CANBOID"].Value = obj.CANBOID;
            comm.Parameters["V_HIEULUC"].Value = obj.HIEULUC;
            comm.Parameters["V_DIENTHOAI"].Value = obj.DIENTHOAI;
            comm.Parameters["V_EMAIL"].Value = obj.EMAIL;
            comm.Parameters["V_NGUOITAO"].Value = obj.NGUOITAO;
            comm.Parameters["V_NGAYTAO"].Value = obj.NGAYTAO;
            comm.Parameters["V_NHOM_ND_ID"].Value = obj.NHOM_ND_ID;
            comm.Parameters["V_DV_TRUCTHUOC"].Value = obj.DV_TRUCTHUOC;
            comm.Parameters["V_PHONGBANID"].Value = obj.PHONGBANID;
            comm.Parameters["V_THONGBAO"].Direction = ParameterDirection.Output;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                V_THONGBAO = Convert.ToString(comm.Parameters["V_THONGBAO"].Value);
                conn.Close();
            }
        }
        public DataTable Get_UserByID(String V_USERID)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_USERID",V_USERID),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_USERBYID", pr);
        }
        public DataTable Get_ByUSERNAME(String V_USERNAME)
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("V_USERNAME",V_USERNAME),
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_BY_USERNAME", pr);
        }
        public DataTable Get_NhomQuyenAll()
        {
            OracleParameter[] pr = new OracleParameter[]
            {
                new OracleParameter("curReturn",OracleDbType.RefCursor, ParameterDirection.Output)
            };
            return Cls_Comon_covid.GetTableByProcedurePaging("PKG_COVID.GET_ALL_NHOMQUYEN", pr);
        }
        public bool Users_Change_Password(String v_User, String v_PassWord)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.USERS_CHANGE_PASSWORD", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_USERNAME"].Value = v_User;
            comm.Parameters["V_PASSWORD"].Value = Cls_Comon_covid.MD5Encrypt(v_PassWord);
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public bool Users_Change_Password_byID(String V_ID,String v_User, String v_PassWord)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.USERS_CHANGE_PASSWORD_BYID", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_ID"].Value = V_ID;
            comm.Parameters["V_USERNAME"].Value = v_User;
            comm.Parameters["V_PASSWORD"].Value = Cls_Comon_covid.MD5Encrypt(v_PassWord);
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public bool Update_UserInfor(String V_CANBO_ID, String V_SODIENTHOAI, String V_EMAIL, string _DONVI_ID, string _DIACHI)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.UPDATE_USER_INFOR", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["V_CANBO_ID"].Value = V_CANBO_ID;
            comm.Parameters["V_SODIENTHOAI"].Value = V_SODIENTHOAI;
            comm.Parameters["V_EMAIL"].Value = V_EMAIL;
            comm.Parameters["V_DONVI_ID"].Value = _DONVI_ID;
            comm.Parameters["V_DIACHI"].Value = _DIACHI;
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch
            {
                tran.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public List<COVID_NGUOISUDUNG> QT_NGUOIDUNG_SEARCH_LIST(string V_DONVIID, string V_PHONGBANID,string V_USERNAME, string V_HOTEN)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand cmd = new OracleCommand("PKG_COVID.QT_NGUOIDUNG_SEARCH", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            OracleParameter[] parameters = new OracleParameter[] {
                new OracleParameter("v_cursor",OracleDbType.RefCursor, ParameterDirection.ReturnValue),
                new OracleParameter("V_DONVIID",V_DONVIID),
                new OracleParameter("V_PHONGBANID",V_PHONGBANID),
                new OracleParameter("V_USERNAME",V_USERNAME),
                new OracleParameter("V_HOTEN",V_HOTEN)
                 };
            cmd.Parameters.AddRange(parameters);
            try
            {
                return Cls_Comon_covid.Bind_List_Reader<COVID_NGUOISUDUNG>(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
        public bool DELETE_NGUOIDUNG_BYID(Decimal v_ID, ref string V_THONGBAO)
        {
            OracleConnection conn = Cls_Comon_covid.Connection();
            OracleCommand comm = new OracleCommand("PKG_COVID.DELETE_NGUOIDUNG_ID", conn);
            comm.CommandType = CommandType.StoredProcedure;
            OracleCommandBuilder.DeriveParameters(comm);
            OracleTransaction tran = conn.BeginTransaction();
            comm.Transaction = tran;
            comm.Parameters["v_ID"].Value = v_ID;
            comm.Parameters["V_THONGBAO"].Direction = ParameterDirection.Output;
            //----------
            try
            {
                comm.ExecuteNonQuery();
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                V_THONGBAO = Convert.ToString(comm.Parameters["V_THONGBAO"].Value);
                conn.Close();
            }
        }
    }
}