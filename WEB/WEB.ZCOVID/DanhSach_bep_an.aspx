﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dangkybep_an.Master" AutoEventWireup="true" CodeBehind="DanhSach_bep_an.aspx.cs" Inherits="WEB.ZCOVID.DanhSach_bep_an" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="/UI/css/TAble.css" rel="stylesheet" />
    <style>
        .ajax__calendar_container {
            width: 180px;
        }

        .ajax__calendar_body {
            width: 100%;
            height: 145px;
        }

        .panding_left li span {
            padding-left: 40px;
        }

        .thongke li {
            line-height: 22px;
        }

        .sobanghi, .sobanghi b {
            font-size: 14px;
        }

        .row_header_css td {
            color: #333;
            padding-top: 10px;
        }

        .row_header_css td {
            background: #ffefb4;
            border: 0px solid #dcdcdc;
            border-top-width: 0px;
            border-right-width: 0px;
            border-bottom-width: 0px;
            border-left-width: 0px;
            text-align: center;
            border-width: 0px 0px 1px 1px;
            font-size: 13px;
            font-family: Arial;
            font-weight: bold;
        }
    </style>
    <div class="content_form">
        <div class="content_body">
            <div class="content_form_head">
                <div class="content_form_head_title">Danh sách cán bộ thực hiện khai báo</div>
                <div class="content_form_head_right"></div>
            </div>
            <div class="content_form_body">
                <div class="searchcontent">
                    <h4 class="tracuu_tleboxchung">Tìm kiếm
                    </h4>
                    <div class="border_search">
                        <table class="table1" style="width: 100%">
                            <tr>
                                <td style="width: 100px;"><b>Đơn vị</b></td>
                                <td>
                                    <asp:DropDownList CssClass="chosen-select" ID="DropDV_TRUCTHUOC" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="DV_TRUCTHUOC_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                                <td style="width: 130px;"><b>Phòng ban</b></td>
                                <td>
                                    <asp:DropDownList CssClass="chosen-select" ID="ddlPhongban" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="ddlPhongban_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr style="height: 38px;">
                                <td style="width: 100px;"><b>Chọn cán bộ</b></td>
                                <td>
                                    <asp:DropDownList ID="ddlCanbo" CssClass="chosen-select" runat="server" AutoPostBack="true" Width="300px">
                                    </asp:DropDownList>
                                </td>
                                <td class="labelTimKiem_css"><b>Chức danh</b></td>
                                <td>
                                    <asp:DropDownList CssClass="chosen-select" ID="DropChucDanh" runat="server" Width="300px"></asp:DropDownList>
                                </td>

                            </tr>
                            <tr style="height: 40px;">
                                <td class="labelTimKiem_css"><b>Trạng thái khai báo</b></td>
                                <td style="width: 310px;">
                                    <asp:DropDownList CssClass="chosen-select" ID="Drop_TrangThai" runat="server" Width="300px">
                                        <asp:ListItem Text="--- Tất cả ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Đã gửi khai báo" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Chưa gửi khai báo" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="labelTimKiem_css"><b>Chức vụ</b></td>
                                <td>
                                    <asp:DropDownList CssClass="chosen-select" ID="DropChucVu" runat="server" Width="300px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Từ khóa</b></td>
                                <td>
                                    <asp:TextBox ID="txKey" CssClass="user" runat="server" Width="292px" placeholder="Mã, tên,tài toản đăng nhập, địa chỉ cán bộ"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Chọn ngày</b></td>
                                <td colspan="3">
                                    <div style="margin-top: 7px;">
                                        <span style="float: left">
                                            <asp:TextBox ID="txtDenNgay" runat="server" CssClass="textbox"
                                                placeholder="......./....../....." Width="100px" MaxLength="10"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDenNgay" Format="dd/MM/yyyy" Enabled="true" />
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDenNgay" Mask="99/99/9999" MaskType="Date" CultureName="vi-VN" ErrorTooltipEnabled="true" />
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: left;">
                                    <div style="margin-left: 105px; margin-top: 15px; margin-bottom: 15px">
                                        <asp:Label runat="server" ID="lbtthongbao" ForeColor="Red" Font-Size="16px"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3">
                                    <asp:Button ID="cmdTimkiem" runat="server" CssClass="buttoninput" Text="Tìm kiếm" OnClick="lbtimkiem_Click" />
                                    <asp:Button ID="cmdLamMoi" runat="server" CssClass="buttoninput" Text="Làm mới" OnClick="cmdLamMoi_Click" />
                                      <asp:Button ID="btnXemBC" runat="server" Width="100px" CssClass="buttoninput" Text="In Báo cáo" OnClick="btnXemBC_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="float: left; width: 800px; margin-top: 0px;">
                    <div style="float: left; margin-left: 25px; padding-top: 5px;">
                        <asp:Label runat="server" ID="lbthongbao" Font-Size="17px" ForeColor="Red"></asp:Label>
                    </div>
                </div>
                <asp:HiddenField ID="hddTotalPage" Value="1" runat="server" />
                <asp:HiddenField ID="hddPageIndex" Value="1" runat="server" />
                <asp:Panel ID="pnDS" runat="server">
                    <div class="phantrang" style="margin-top: 30px;">
                        <div class="sobanghi">
                            <asp:Literal ID="lstSobanghiT" runat="server"></asp:Literal>
                             <asp:Literal ID="lstSobanghiT_Canbo" runat="server"></asp:Literal>
                        </div>
                        <div class="sotrang">
                            <asp:LinkButton ID="lbTBack" runat="server" CausesValidation="false" CssClass="back"
                                OnClick="lbTBack_Click"><</asp:LinkButton>
                            <asp:LinkButton ID="lbTFirst" runat="server" CausesValidation="false" CssClass="active"
                                Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                            <asp:Label ID="lbTStep1" runat="server" Text="..."></asp:Label>
                            <asp:LinkButton ID="lbTStep2" runat="server" CausesValidation="false" CssClass="so"
                                Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbTStep3" runat="server" CausesValidation="false" CssClass="so"
                                Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbTStep4" runat="server" CausesValidation="false" CssClass="so"
                                Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbTStep5" runat="server" CausesValidation="false" CssClass="so"
                                Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:Label ID="lbTStep6" runat="server" Text="..."></asp:Label>
                            <asp:LinkButton ID="lbTLast" runat="server" CausesValidation="false" CssClass="so"
                                Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbTNext" runat="server" CausesValidation="false" CssClass="next"
                                OnClick="lbTNext_Click">></asp:LinkButton>
                            <asp:DropDownList ID="dropPageSize" runat="server" Width="75px"
                                CssClass="dropbox"
                                AutoPostBack="True" OnSelectedIndexChanged="dropPageSize_SelectedIndexChanged">
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                <asp:ListItem Value="500" Text="500"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div>
                        <asp:Repeater ID="rpt" runat="server" OnItemCommand="rpt_ItemCommand" Visible="true" OnItemDataBound="rpt_ItemDataBound">
                            <HeaderTemplate>
                                <div class="CSSTableGenerator">
                                    <table>
                                        <tr id="row_header">
                                            <td rowspan="2" style="width: 20px; height: 0px">TT</td>
                                            <td rowspan="2" style="width: 120px;">Họ tên</td>
                                            <td rowspan="2" style="width: 150px;">Chức vụ, đơn vị công tác</td>
                                            <td colspan="4">Đối tượng thuộc diện</td>
                                            <td rowspan="2" style="width: 60px;">Sốt >=37.5 độ C</td>
                                            <td rowspan="2" style="width: 100px; text-align: center;">Tiếp xúc gần với người mắc bệnh hoặc nghi ngờ mắc bệnh viêm đường hô hấp do nCoV</td>
                                            <td rowspan="2" style="width: 80px;">Thời gian cách ly</td>
                                            <td rowspan="2" style="width: 130px;">Ghi chú</td>
                                            <td rowspan="2" style="width: 80px;">Ngày gửi</td>
                                            <td rowspan="2" style="width: 50px;">Tài khoản gửi</td>
                                        </tr>
                                        <tr class="row_header_css">
                                            <td style="width: 30px;">F1</td>
                                            <td style="width: 30px;">F2</td>
                                            <td style="width: 30px;">F3</td>
                                            <td style="width: 30px;">F4</td>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;"><%#Eval("STT") %> </td>
                                    <td style="text-align: center;"><%#Eval("HOTEN") %> </td>
                                    <td style="text-align: left;"><%#Eval("TEN_ALIAS") %> </td>
                                    <td style="text-align: center;"><%#Eval("F1") %> </td>
                                    <td style="text-align: center;"><%#Eval("F2") %> </td>
                                    <td style="text-align: center;"><%#Eval("F3") %> </td>
                                    <td style="text-align: center;"><%#Eval("F4") %> </td>
                                    <td style="text-align: center;"><%#Eval("SOT") %> </td>
                                    <td style="text-align: center;"><%#Eval("TIEPXUCGAN_BN") %> </td>
                                    <td style="text-align: center;"><%#Eval("TIME_CACHLY") %> </td>
                                    <td style="text-align: center;"><%#Eval("GHICHU") %> </td>
                                    <td style="text-align: center;"><%#Eval("NGAYSUA") %> </td>
                                    <td style="text-align: center;"><%#Eval("NGUOITAO") %> </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table></div>
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>
                    <div class="phantrang">
                        <div class="sobanghi">
                            <asp:HiddenField ID="hdicha" runat="server" />
                            <asp:Literal ID="lstSobanghiB" runat="server"></asp:Literal>
                        </div>
                        <div class="sotrang">
                            <asp:LinkButton ID="lbBBack" runat="server" CausesValidation="false" CssClass="back"
                                OnClick="lbTBack_Click"><</asp:LinkButton>
                            <asp:LinkButton ID="lbBFirst" runat="server" CausesValidation="false" CssClass="active"
                                Text="1" OnClick="lbTFirst_Click"></asp:LinkButton>
                            <asp:Label ID="lbBStep1" runat="server" Text="..."></asp:Label>
                            <asp:LinkButton ID="lbBStep2" runat="server" CausesValidation="false" CssClass="so"
                                Text="2" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbBStep3" runat="server" CausesValidation="false" CssClass="so"
                                Text="3" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbBStep4" runat="server" CausesValidation="false" CssClass="so"
                                Text="4" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbBStep5" runat="server" CausesValidation="false" CssClass="so"
                                Text="5" OnClick="lbTStep_Click"></asp:LinkButton>
                            <asp:Label ID="lbBStep6" runat="server" Text="..."></asp:Label>
                            <asp:LinkButton ID="lbBLast" runat="server" CausesValidation="false" CssClass="so"
                                Text="100" OnClick="lbTLast_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lbBNext" runat="server" CausesValidation="false" CssClass="next"
                                OnClick="lbTNext_Click">></asp:LinkButton>
                            <asp:DropDownList ID="dropPageSize2" runat="server" Width="75px" CssClass="dropbox"
                                AutoPostBack="True" OnSelectedIndexChanged="dropPageSize2_SelectedIndexChanged">
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                <asp:ListItem Value="20" Text="20" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                <asp:ListItem Value="200" Text="200"></asp:ListItem>
                                <asp:ListItem Value="500" Text="500"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
    <script>
        function pageLoad(sender, args) {
            var config = { '.chosen-select': {}, '.chosen-select-deselect': { allow_single_deselect: true }, '.chosen-select-no-single': { disable_search_threshold: 10 }, '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' }, '.chosen-select-rtl': { rtl: true }, '.chosen-select-width': { width: '95%' } }
            for (var selector in config) { $(selector).chosen(config[selector]); }
        }
    </script>
</asp:Content>