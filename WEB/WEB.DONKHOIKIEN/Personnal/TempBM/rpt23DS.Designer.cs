﻿namespace WEB.DONKHOIKIEN.Personnal.TempBM
{
    partial class rpt23DS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.dtbieumau1 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.DetailReport_NguoiKK = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable_NguoiKK_Ten = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand1 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NguoiKK_SDT = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand2 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NguoiKK_Email = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable_NguoiKK = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.dtbieumau6 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.dtbieumau4 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.DetailReport_NguoiBK = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable_NguoiBK_Ten = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand3 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NguoiBK_SDT = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand4 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NguoiBK_Email = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable_NguoiBK = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_QuyenBV = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.dtbieumau2 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.xrTable_QuyenBV_Ten = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_QuyenBV_SDT = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_QuyenBV_SDT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_QuyenBV_Email = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_QuyenBV_Email = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport_QuyenBV = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.SubBand_QuyenBV_SDT = new DevExpress.XtraReports.UI.SubBand();
            this.SubBand_QuyenBV_Email = new DevExpress.XtraReports.UI.SubBand();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.dtbieumau3 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.DetailReport_NVLQ = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable_NVLQ_Ten = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand5 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NVLQ_SDT = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand6 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NVLQ_Email = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable_NVLQ = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport_NoiDungDon = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader6 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.dtbieumau5 = new WEB.DONKHOIKIEN.DTBIEUMAU();
            this.DetailReport_NhanChung = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable_NhanChung_Ten = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand7 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NhanChung_SDT = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SubBand8 = new DevExpress.XtraReports.UI.SubBand();
            this.xrTable_NhanChung_Email = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable_NhanChung = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport_TaiLieu = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail_TaiLieu = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader_TaiLieu = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport_ThongTinKhac = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport_ToaAn_DiaChi = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail9 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader7 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_SDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_Email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_SDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_Email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_SDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_Email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_SDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_Email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_Ten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_SDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_Email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Expanded = false;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 79F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 79F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable2});
            this.ReportHeader.HeightF = 167.8125F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 97.8125F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow7});
            this.xrTable1.SizeF = new System.Drawing.SizeF(629.9999F, 70F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 1.7999999999999998D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "ĐƠN KHỞI KIỆN";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "                  Kính gửi: [TenToaAn]";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell7.Weight = 1D;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable2.SizeF = new System.Drawing.SizeF(629.9999F, 85.4166F);
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.1712011175384138D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell3.Weight = 3.3849558216606015D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.91500044726409613D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Độc lập - Tự do - Hạnh phúc";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 3.3849558216606015D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseTextAlignment = false;
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow4.Weight = 0.33956810398564718D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 3.3849558216606015D;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(219.4584F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(198F, 2.083333F);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.7425629392437512D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Italic);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "[DiaDiem], ngày [Ngay] tháng [Thang] năm [Nam]  ";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell6.Weight = 3.3849558216606015D;
            // 
            // dtbieumau1
            // 
            this.dtbieumau1.DataSetName = "DTBIEUMAU";
            this.dtbieumau1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DetailReport_NguoiKK
            // 
            this.DetailReport_NguoiKK.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupHeader1});
            this.DetailReport_NguoiKK.DataMember = "DTNGUYENDON";
            this.DetailReport_NguoiKK.DataSource = this.dtbieumau6;
            this.DetailReport_NguoiKK.Expanded = false;
            this.DetailReport_NguoiKK.Level = 1;
            this.DetailReport_NguoiKK.Name = "DetailReport_NguoiKK";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiKK_Ten});
            this.Detail2.HeightF = 25F;
            this.Detail2.Name = "Detail2";
            this.Detail2.SubBands.AddRange(new DevExpress.XtraReports.UI.SubBand[] {
            this.SubBand1,
            this.SubBand2});
            // 
            // xrTable_NguoiKK_Ten
            // 
            this.xrTable_NguoiKK_Ten.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NguoiKK_Ten.Name = "xrTable_NguoiKK_Ten";
            this.xrTable_NguoiKK_Ten.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable_NguoiKK_Ten.SizeF = new System.Drawing.SizeF(629.9998F, 25F);
            this.xrTable_NguoiKK_Ten.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiKK_Ten_BeforePrint);
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UsePadding = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "      - [Ten] ; Địa chỉ: [DiaChi]";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell10.Weight = 1D;
            // 
            // SubBand1
            // 
            this.SubBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiKK_SDT});
            this.SubBand1.HeightF = 25F;
            this.SubBand1.Name = "SubBand1";
            // 
            // xrTable_NguoiKK_SDT
            // 
            this.xrTable_NguoiKK_SDT.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NguoiKK_SDT.Name = "xrTable_NguoiKK_SDT";
            this.xrTable_NguoiKK_SDT.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable_NguoiKK_SDT.SizeF = new System.Drawing.SizeF(629.9998F, 25F);
            this.xrTable_NguoiKK_SDT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiKK_SDT_BeforePrint);
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "      Số điện thoại: [SoDienThoai]; số fax: [Fax]";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell9.Weight = 1D;
            // 
            // SubBand2
            // 
            this.SubBand2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiKK_Email});
            this.SubBand2.HeightF = 25F;
            this.SubBand2.Name = "SubBand2";
            // 
            // xrTable_NguoiKK_Email
            // 
            this.xrTable_NguoiKK_Email.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_NguoiKK_Email.Name = "xrTable_NguoiKK_Email";
            this.xrTable_NguoiKK_Email.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable_NguoiKK_Email.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NguoiKK_Email.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiKK_Email_BeforePrint);
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "      Địa chỉ thư điện tử: [Email]";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell11.Weight = 1D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiKK});
            this.GroupHeader1.HeightF = 35F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable_NguoiKK
            // 
            this.xrTable_NguoiKK.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 10F);
            this.xrTable_NguoiKK.Name = "xrTable_NguoiKK";
            this.xrTable_NguoiKK.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable_NguoiKK.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NguoiKK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiKK_BeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "      Người khởi kiện: ";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell8.Weight = 1D;
            // 
            // dtbieumau6
            // 
            this.dtbieumau6.DataSetName = "DTBIEUMAU";
            this.dtbieumau6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dtbieumau4
            // 
            this.dtbieumau4.DataSetName = "DTBIEUMAU";
            this.dtbieumau4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DetailReport_NguoiBK
            // 
            this.DetailReport_NguoiBK.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.GroupHeader3});
            this.DetailReport_NguoiBK.DataMember = "DTBIDON";
            this.DetailReport_NguoiBK.DataSource = this.dtbieumau6;
            this.DetailReport_NguoiBK.Expanded = false;
            this.DetailReport_NguoiBK.Level = 2;
            this.DetailReport_NguoiBK.Name = "DetailReport_NguoiBK";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiBK_Ten});
            this.Detail3.HeightF = 25F;
            this.Detail3.Name = "Detail3";
            this.Detail3.SubBands.AddRange(new DevExpress.XtraReports.UI.SubBand[] {
            this.SubBand3,
            this.SubBand4});
            // 
            // xrTable_NguoiBK_Ten
            // 
            this.xrTable_NguoiBK_Ten.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NguoiBK_Ten.Name = "xrTable_NguoiBK_Ten";
            this.xrTable_NguoiBK_Ten.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable_NguoiBK_Ten.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            this.xrTable_NguoiBK_Ten.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiBK_Ten_BeforePrint);
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "      - [Ten] ; Địa chỉ: [DiaChi]";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell13.Weight = 1D;
            // 
            // SubBand3
            // 
            this.SubBand3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiBK_SDT});
            this.SubBand3.HeightF = 25F;
            this.SubBand3.Name = "SubBand3";
            // 
            // xrTable_NguoiBK_SDT
            // 
            this.xrTable_NguoiBK_SDT.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NguoiBK_SDT.Name = "xrTable_NguoiBK_SDT";
            this.xrTable_NguoiBK_SDT.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable_NguoiBK_SDT.SizeF = new System.Drawing.SizeF(629.9998F, 25F);
            this.xrTable_NguoiBK_SDT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiBK_SDT_BeforePrint);
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "      Số điện thoại: [SoDienThoai]; số fax: [Fax]";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell14.Weight = 1D;
            // 
            // SubBand4
            // 
            this.SubBand4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiBK_Email});
            this.SubBand4.HeightF = 25F;
            this.SubBand4.Name = "SubBand4";
            // 
            // xrTable_NguoiBK_Email
            // 
            this.xrTable_NguoiBK_Email.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NguoiBK_Email.Name = "xrTable_NguoiBK_Email";
            this.xrTable_NguoiBK_Email.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable_NguoiBK_Email.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            this.xrTable_NguoiBK_Email.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiBK_Email_BeforePrint);
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "      Địa chỉ thư điện tử: [Email]";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell15.Weight = 1D;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NguoiBK});
            this.GroupHeader3.HeightF = 25F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrTable_NguoiBK
            // 
            this.xrTable_NguoiBK.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_NguoiBK.Name = "xrTable_NguoiBK";
            this.xrTable_NguoiBK.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable_NguoiBK.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NguoiBK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NguoiBK_BeforePrint);
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell12.StylePriority.UseFont = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "      Người bị kiện: ";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell12.Weight = 1D;
            // 
            // xrTable_QuyenBV
            // 
            this.xrTable_QuyenBV.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_QuyenBV.Name = "xrTable_QuyenBV";
            this.xrTable_QuyenBV.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable_QuyenBV.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_QuyenBV.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_QuyenBV_BeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "      Người có quyền, lợi ích được bảo vệ:";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell5.Weight = 1D;
            // 
            // dtbieumau2
            // 
            this.dtbieumau2.DataSetName = "DTBIEUMAU";
            this.dtbieumau2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrTable_QuyenBV_Ten
            // 
            this.xrTable_QuyenBV_Ten.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_QuyenBV_Ten.Name = "xrTable_QuyenBV_Ten";
            this.xrTable_QuyenBV_Ten.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable_QuyenBV_Ten.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_QuyenBV_Ten.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_QuyenBV_Ten_BeforePrint);
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "      - [Ten]; Địa chỉ: [DiaChi]";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell24.Weight = 1D;
            // 
            // xrTable_QuyenBV_SDT
            // 
            this.xrTable_QuyenBV_SDT.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_QuyenBV_SDT.Name = "xrTable_QuyenBV_SDT";
            this.xrTable_QuyenBV_SDT.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable_QuyenBV_SDT.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_QuyenBV_SDT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_QuyenBV_SDT_BeforePrint);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_QuyenBV_SDT});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell_QuyenBV_SDT
            // 
            this.xrTableCell_QuyenBV_SDT.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell_QuyenBV_SDT.Name = "xrTableCell_QuyenBV_SDT";
            this.xrTableCell_QuyenBV_SDT.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell_QuyenBV_SDT.StylePriority.UseFont = false;
            this.xrTableCell_QuyenBV_SDT.StylePriority.UsePadding = false;
            this.xrTableCell_QuyenBV_SDT.StylePriority.UseTextAlignment = false;
            this.xrTableCell_QuyenBV_SDT.Text = "      Số điện thoại: [SoDienThoai]; số fax: [Fax]";
            this.xrTableCell_QuyenBV_SDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell_QuyenBV_SDT.Weight = 1D;
            // 
            // xrTable_QuyenBV_Email
            // 
            this.xrTable_QuyenBV_Email.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_QuyenBV_Email.Name = "xrTable_QuyenBV_Email";
            this.xrTable_QuyenBV_Email.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable_QuyenBV_Email.SizeF = new System.Drawing.SizeF(629.9998F, 25F);
            this.xrTable_QuyenBV_Email.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_QuyenBV_Email_BeforePrint);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_QuyenBV_Email});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell_QuyenBV_Email
            // 
            this.xrTableCell_QuyenBV_Email.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell_QuyenBV_Email.Name = "xrTableCell_QuyenBV_Email";
            this.xrTableCell_QuyenBV_Email.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell_QuyenBV_Email.StylePriority.UseFont = false;
            this.xrTableCell_QuyenBV_Email.StylePriority.UsePadding = false;
            this.xrTableCell_QuyenBV_Email.StylePriority.UseTextAlignment = false;
            this.xrTableCell_QuyenBV_Email.Text = "      Địa chỉ thư điện tử: [Email]";
            this.xrTableCell_QuyenBV_Email.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell_QuyenBV_Email.Weight = 1D;
            // 
            // DetailReport_QuyenBV
            // 
            this.DetailReport_QuyenBV.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupHeader2});
            this.DetailReport_QuyenBV.DataMember = "DTQUYENDUOCBAOBE";
            this.DetailReport_QuyenBV.DataSource = this.dtbieumau6;
            this.DetailReport_QuyenBV.Expanded = false;
            this.DetailReport_QuyenBV.Level = 3;
            this.DetailReport_QuyenBV.Name = "DetailReport_QuyenBV";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_QuyenBV_Ten});
            this.Detail4.HeightF = 25F;
            this.Detail4.Name = "Detail4";
            this.Detail4.SubBands.AddRange(new DevExpress.XtraReports.UI.SubBand[] {
            this.SubBand_QuyenBV_SDT,
            this.SubBand_QuyenBV_Email});
            // 
            // SubBand_QuyenBV_SDT
            // 
            this.SubBand_QuyenBV_SDT.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_QuyenBV_SDT});
            this.SubBand_QuyenBV_SDT.HeightF = 25F;
            this.SubBand_QuyenBV_SDT.Name = "SubBand_QuyenBV_SDT";
            // 
            // SubBand_QuyenBV_Email
            // 
            this.SubBand_QuyenBV_Email.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_QuyenBV_Email});
            this.SubBand_QuyenBV_Email.HeightF = 25F;
            this.SubBand_QuyenBV_Email.Name = "SubBand_QuyenBV_Email";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_QuyenBV});
            this.GroupHeader2.HeightF = 25F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // dtbieumau3
            // 
            this.dtbieumau3.DataSetName = "DTBIEUMAU";
            this.dtbieumau3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DetailReport_NVLQ
            // 
            this.DetailReport_NVLQ.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader4});
            this.DetailReport_NVLQ.DataMember = "DTNVLQ";
            this.DetailReport_NVLQ.DataSource = this.dtbieumau6;
            this.DetailReport_NVLQ.Expanded = false;
            this.DetailReport_NVLQ.Level = 4;
            this.DetailReport_NVLQ.Name = "DetailReport_NVLQ";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NVLQ_Ten});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            this.Detail1.SubBands.AddRange(new DevExpress.XtraReports.UI.SubBand[] {
            this.SubBand5,
            this.SubBand6});
            // 
            // xrTable_NVLQ_Ten
            // 
            this.xrTable_NVLQ_Ten.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NVLQ_Ten.Name = "xrTable_NVLQ_Ten";
            this.xrTable_NVLQ_Ten.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable_NVLQ_Ten.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            this.xrTable_NVLQ_Ten.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NVLQ_Ten_BeforePrint);
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "      - [Ten]; Địa chỉ: [DiaChi]";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell17.Weight = 1D;
            // 
            // SubBand5
            // 
            this.SubBand5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NVLQ_SDT});
            this.SubBand5.HeightF = 25F;
            this.SubBand5.Name = "SubBand5";
            // 
            // xrTable_NVLQ_SDT
            // 
            this.xrTable_NVLQ_SDT.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NVLQ_SDT.Name = "xrTable_NVLQ_SDT";
            this.xrTable_NVLQ_SDT.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable_NVLQ_SDT.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NVLQ_SDT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NVLQ_SDT_BeforePrint);
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "      Số điện thoại: [SoDienThoai]; số fax: [Fax]";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell18.Weight = 1D;
            // 
            // SubBand6
            // 
            this.SubBand6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NVLQ_Email});
            this.SubBand6.HeightF = 25F;
            this.SubBand6.Name = "SubBand6";
            // 
            // xrTable_NVLQ_Email
            // 
            this.xrTable_NVLQ_Email.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NVLQ_Email.Name = "xrTable_NVLQ_Email";
            this.xrTable_NVLQ_Email.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable_NVLQ_Email.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NVLQ_Email.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NVLQ_Email_BeforePrint);
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "      Địa chỉ thư điện tử: [Email]";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell19.Weight = 1D;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NVLQ});
            this.GroupHeader4.HeightF = 25F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // xrTable_NVLQ
            // 
            this.xrTable_NVLQ.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NVLQ.Name = "xrTable_NVLQ";
            this.xrTable_NVLQ.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable_NVLQ.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NVLQ.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NVLQ_BeforePrint);
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "      Người có quyền lợi, nghĩa vụ liên quan:";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell16.Weight = 1D;
            // 
            // DetailReport_NoiDungDon
            // 
            this.DetailReport_NoiDungDon.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader6});
            this.DetailReport_NoiDungDon.DataMember = "DTBM23DS";
            this.DetailReport_NoiDungDon.DataSource = this.dtbieumau6;
            this.DetailReport_NoiDungDon.Expanded = false;
            this.DetailReport_NoiDungDon.Level = 5;
            this.DetailReport_NoiDungDon.Name = "DetailReport_NoiDungDon";
            this.DetailReport_NoiDungDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailReport_NoiDungDon_BeforePrint);
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
            this.Detail5.HeightF = 25F;
            this.Detail5.Name = "Detail5";
            // 
            // xrTable17
            // 
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(0.0001589457F, 0F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable17.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "[NoiDungDon]";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell21.Weight = 1D;
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.GroupHeader6.HeightF = 25F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // xrTable8
            // 
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0.0001589457F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable8.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UsePadding = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "      Yêu cầu Tòa án giải quyết những vấn đề sau đây:";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell34.Weight = 1D;
            // 
            // dtbieumau5
            // 
            this.dtbieumau5.DataSetName = "DTBIEUMAU";
            this.dtbieumau5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DetailReport_NhanChung
            // 
            this.DetailReport_NhanChung.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupHeader5});
            this.DetailReport_NhanChung.DataMember = "DTNGUOILAMCHUNG";
            this.DetailReport_NhanChung.DataSource = this.dtbieumau6;
            this.DetailReport_NhanChung.Expanded = false;
            this.DetailReport_NhanChung.Level = 6;
            this.DetailReport_NhanChung.Name = "DetailReport_NhanChung";
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NhanChung_Ten});
            this.Detail6.HeightF = 25F;
            this.Detail6.Name = "Detail6";
            this.Detail6.SubBands.AddRange(new DevExpress.XtraReports.UI.SubBand[] {
            this.SubBand7,
            this.SubBand8});
            // 
            // xrTable_NhanChung_Ten
            // 
            this.xrTable_NhanChung_Ten.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NhanChung_Ten.Name = "xrTable_NhanChung_Ten";
            this.xrTable_NhanChung_Ten.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable_NhanChung_Ten.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            this.xrTable_NhanChung_Ten.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NhanChung_Ten_BeforePrint);
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "      - [Ten]; Địa chỉ: [DiaChi]";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell23.Weight = 1D;
            // 
            // SubBand7
            // 
            this.SubBand7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NhanChung_SDT});
            this.SubBand7.HeightF = 25F;
            this.SubBand7.Name = "SubBand7";
            // 
            // xrTable_NhanChung_SDT
            // 
            this.xrTable_NhanChung_SDT.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NhanChung_SDT.Name = "xrTable_NhanChung_SDT";
            this.xrTable_NhanChung_SDT.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable_NhanChung_SDT.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NhanChung_SDT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NhanChung_SDT_BeforePrint);
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "      Số điện thoại: [SoDienThoai]; số fax: [Fax]";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell25.Weight = 1D;
            // 
            // SubBand8
            // 
            this.SubBand8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NhanChung_Email});
            this.SubBand8.HeightF = 25F;
            this.SubBand8.Name = "SubBand8";
            // 
            // xrTable_NhanChung_Email
            // 
            this.xrTable_NhanChung_Email.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NhanChung_Email.Name = "xrTable_NhanChung_Email";
            this.xrTable_NhanChung_Email.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable_NhanChung_Email.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            this.xrTable_NhanChung_Email.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NhanChung_Email_BeforePrint);
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "      Địa chỉ thư điện tử: [Email]";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell26.Weight = 1D;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_NhanChung});
            this.GroupHeader5.HeightF = 25F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // xrTable_NhanChung
            // 
            this.xrTable_NhanChung.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable_NhanChung.Name = "xrTable_NhanChung";
            this.xrTable_NhanChung.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable_NhanChung.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            this.xrTable_NhanChung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable_NhanChung_BeforePrint);
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "      Người làm chứng:";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell22.Weight = 1D;
            // 
            // DetailReport_TaiLieu
            // 
            this.DetailReport_TaiLieu.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail_TaiLieu,
            this.GroupHeader_TaiLieu});
            this.DetailReport_TaiLieu.DataMember = "DTTAILIEU";
            this.DetailReport_TaiLieu.DataSource = this.dtbieumau6;
            this.DetailReport_TaiLieu.Expanded = false;
            this.DetailReport_TaiLieu.Level = 7;
            this.DetailReport_TaiLieu.Name = "DetailReport_TaiLieu";
            this.DetailReport_TaiLieu.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailReport_TaiLieu_BeforePrint);
            // 
            // Detail_TaiLieu
            // 
            this.Detail_TaiLieu.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail_TaiLieu.HeightF = 25F;
            this.Detail_TaiLieu.Name = "Detail_TaiLieu";
            // 
            // xrTable4
            // 
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable4.SizeF = new System.Drawing.SizeF(629.9997F, 25F);
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UsePadding = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "      [TT]. [Ten]";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell28.Weight = 1D;
            // 
            // GroupHeader_TaiLieu
            // 
            this.GroupHeader_TaiLieu.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader_TaiLieu.HeightF = 25F;
            this.GroupHeader_TaiLieu.Name = "GroupHeader_TaiLieu";
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable3.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "      Danh mục tài liệu, chứng cứ kèm theo đơn khởi kiện gồm có:";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell27.Weight = 1D;
            // 
            // DetailReport_ThongTinKhac
            // 
            this.DetailReport_ThongTinKhac.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7});
            this.DetailReport_ThongTinKhac.DataMember = "DTBM23DS";
            this.DetailReport_ThongTinKhac.DataSource = this.dtbieumau6;
            this.DetailReport_ThongTinKhac.Expanded = false;
            this.DetailReport_ThongTinKhac.Level = 8;
            this.DetailReport_ThongTinKhac.Name = "DetailReport_ThongTinKhac";
            this.DetailReport_ThongTinKhac.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailReport_ThongTinKhac_BeforePrint);
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail7.HeightF = 50F;
            this.Detail7.Name = "Detail7";
            // 
            // xrTable6
            // 
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow33});
            this.xrTable6.SizeF = new System.Drawing.SizeF(629.9999F, 50F);
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "      Các thông tin khác mà người khởi kiện xét thấy cần thiết cho việc giải quyế" +
    "t vụ án";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell30.Weight = 1D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "[ThongTinKhac]";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.xrTableCell31.Weight = 1D;
            // 
            // DetailReport_ToaAn_DiaChi
            // 
            this.DetailReport_ToaAn_DiaChi.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8});
            this.DetailReport_ToaAn_DiaChi.DataMember = "DTBM23DS";
            this.DetailReport_ToaAn_DiaChi.DataSource = this.dtbieumau6;
            this.DetailReport_ToaAn_DiaChi.Expanded = false;
            this.DetailReport_ToaAn_DiaChi.Level = 0;
            this.DetailReport_ToaAn_DiaChi.Name = "DetailReport_ToaAn_DiaChi";
            this.DetailReport_ToaAn_DiaChi.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DetailReport_ToaAn_DiaChi_BeforePrint);
            // 
            // Detail8
            // 
            this.Detail8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.Detail8.HeightF = 25F;
            this.Detail8.Name = "Detail8";
            // 
            // xrTable7
            // 
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable7.SizeF = new System.Drawing.SizeF(629.9999F, 25F);
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UsePadding = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "                  Địa chỉ: [TenToaAn_DiaChi]";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell33.Weight = 1D;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail9,
            this.GroupHeader7});
            this.DetailReport.DataMember = "DTNGUYENDON_KY";
            this.DetailReport.DataSource = this.dtbieumau6;
            this.DetailReport.Expanded = false;
            this.DetailReport.Level = 9;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail9
            // 
            this.Detail9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail9.HeightF = 152.9167F;
            this.Detail9.Name = "Detail9";
            // 
            // xrTable5
            // 
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(341.125F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow31});
            this.xrTable5.SizeF = new System.Drawing.SizeF(288.8748F, 152.9167F);
            this.xrTable5.StylePriority.UsePadding = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1.6744577973098143D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell20.Weight = 3.3849558216606015D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.8754428035920474D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "[Ten]";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 3.3849558216606015D;
            // 
            // GroupHeader7
            // 
            this.GroupHeader7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.GroupHeader7.HeightF = 25.00002F;
            this.GroupHeader7.Name = "GroupHeader7";
            // 
            // xrTable9
            // 
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(341.125F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable9.SizeF = new System.Drawing.SizeF(288.8748F, 25.00002F);
            this.xrTable9.StylePriority.UsePadding = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.41687759821766207D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Người khởi kiện";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell32.Weight = 3.3849558216606015D;
            // 
            // rpt23DS
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport_NguoiKK,
            this.DetailReport_NguoiBK,
            this.DetailReport_QuyenBV,
            this.DetailReport_NVLQ,
            this.DetailReport_NoiDungDon,
            this.DetailReport_NhanChung,
            this.DetailReport_TaiLieu,
            this.DetailReport_ThongTinKhac,
            this.DetailReport_ToaAn_DiaChi,
            this.DetailReport});
            this.DataMember = "DTBM23DS";
            this.DataSource = this.dtbieumau6;
            this.DisplayName = "23-DS. Đơn khởi kiện";
            this.ExportOptions.Docx.TableLayout = true;
            this.ExportOptions.Rtf.ExportMode = DevExpress.XtraPrinting.RtfExportMode.SingleFile;
            this.Margins = new System.Drawing.Printing.Margins(118, 79, 79, 79);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_SDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK_Email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiKK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_SDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK_Email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NguoiBK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_SDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_QuyenBV_Email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_SDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ_Email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NVLQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtbieumau5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_Ten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_SDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung_Email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_NhanChung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DTBIEUMAU dtbieumau1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_NguoiKK;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiKK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_NguoiBK;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable_QuyenBV;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DTBIEUMAU dtbieumau2;
        private DevExpress.XtraReports.UI.XRTable xrTable_QuyenBV_SDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_QuyenBV_SDT;
        private DevExpress.XtraReports.UI.XRTable xrTable_QuyenBV_Ten;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTable xrTable_QuyenBV_Email;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_QuyenBV_Email;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_QuyenBV;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.SubBand SubBand_QuyenBV_SDT;
        private DevExpress.XtraReports.UI.SubBand SubBand_QuyenBV_Email;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiKK_Ten;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.SubBand SubBand1;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiKK_SDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.SubBand SubBand2;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiKK_Email;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DTBIEUMAU dtbieumau3;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiBK_Ten;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.SubBand SubBand3;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiBK_SDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.SubBand SubBand4;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiBK_Email;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable_NguoiBK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_NVLQ;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable_NVLQ_Ten;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.SubBand SubBand5;
        private DevExpress.XtraReports.UI.XRTable xrTable_NVLQ_SDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.SubBand SubBand6;
        private DevExpress.XtraReports.UI.XRTable xrTable_NVLQ_Email;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable_NVLQ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_NoiDungDon;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_NhanChung;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.XRTable xrTable_NhanChung_Ten;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.SubBand SubBand7;
        private DevExpress.XtraReports.UI.XRTable xrTable_NhanChung_SDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.SubBand SubBand8;
        private DevExpress.XtraReports.UI.XRTable xrTable_NhanChung_Email;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable xrTable_NhanChung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DTBIEUMAU dtbieumau4;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_TaiLieu;
        private DevExpress.XtraReports.UI.DetailBand Detail_TaiLieu;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader_TaiLieu;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_ThongTinKhac;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport_ToaAn_DiaChi;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader6;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail9;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader7;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DTBIEUMAU dtbieumau5;
        private DTBIEUMAU dtbieumau6;
    }
}
