﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Service.Models
{
    public class DMHanhChinhDL
    {
        public string MaDongBo { get; set; }
        public string MaCapCha { get; set; }
        public string MaHanhChinh { get; set; }
        public string Ten { get; set; }
    }
}