﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Service.Models
{
    public class MessageResult
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}