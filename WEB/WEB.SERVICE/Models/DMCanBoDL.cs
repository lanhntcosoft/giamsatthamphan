﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Service.Models
{
    public class DMCanBoDL
    {
        public string MaDongBo { get; set; }
        public string MaToaAn { get; set; }
        public string MaCanBo { get; set; }
        public string HoTen { get; set; }
        public decimal GioiTinh { get; set; }
        public string DiaChi { get; set; }
        public string SoCMND { get; set; }
        public string NgaySinh { get; set; }
        public string DienThoai { get; set; }
        public string MaChucDanh { get; set; }
        public string MaChucVu { get; set; }
        public string QDBoNhiem { get; set; }
        public string NgayBoNhiem { get; set; }
        public string NgayKetThuc { get; set; }
        public string NgayNhanCongTac { get; set; }
        public decimal TinhTrang { get; set; }
        public string NgayBaiNhiem { get; set; }
    }
}