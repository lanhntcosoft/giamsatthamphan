﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Service.Models
{
    public class DMToaAnDL
    {
        public string MaDongBo { get; set; }
        public string MaToaAn { get; set; }
        public string Ten { get; set; }
        public string MaCapCha { get; set; }
        public string LoaiToa { get; set; }
        public string MaHanhChinh { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}