﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Service.Models
{
    public class DMChucVuDL
    {
        public string MaDongBo { get; set; }
        public string MaChucVu { get; set; }
        public string Ten { get; set; }
        public string MoTa { get; set; }
    }
}