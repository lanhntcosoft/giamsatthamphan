﻿using DAL.GSTP;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using WEB.Service.Models;

namespace WEB.Service.Controllers
{
    public class WS_ThongTinCanBoController : ApiController
    {
        private GSTPContext dt = new GSTPContext();
        private CultureInfo cul = new CultureInfo("vi-VN");
        // GET: api/WS_ThongTinCanBo
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS_ThongTinCanBo/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WS_ThongTinCanBo
        public MessageResult Post([FromBody]DMCanBoDL value)
        {
            //00: Đồng bộ thành công
            //01: Dữ liệu không đầy đủ
            //02: Mã tòa án không tồn tại
            //03: Mã cán bộ đã tồn tại
            //04: Tên cán bộ đã tồn tại
            //05: Mã chức danh không tồn tại
            //06: Mã chức vụ không tồn tại
            //07: Mã cấp thẩm phán không tồn tại
            //08: Lỗi khi đồng bộ
            try
            {
                MessageResult oMsg = new MessageResult();
                string MaDongBo = (value.MaDongBo + "").Trim(), MaToaAn = (value.MaToaAn + "").Trim(), MaCanBo = (value.MaCanBo + "").Trim(),
                    HoTen = (value.HoTen + "").Trim(), MaChucDanh = (value.MaChucDanh + "").Trim(), MaChucVu = (value.MaChucVu + "").Trim(),
                    QDBoNhiem = (value.QDBoNhiem + "").Trim(), NgayBoNhiem = (value.NgayBoNhiem + "").Trim(), NgayKetThuc = (value.NgayKetThuc + "").Trim();
                #region Valid data
                if (MaDongBo == "" || MaToaAn == "" || MaCanBo == "" || HoTen == "")
                {
                    oMsg.Status = "01";
                    oMsg.Message = "Dữ liệu không đầy đủ. Bạn cần phải nhập Mã đồng bộ, Mã tòa án, Mã cán bộ, Họ tên cán bộ.";
                    return oMsg;
                }
                DM_TOAAN oToaAn = dt.DM_TOAAN.Where(x => x.MADONGBO == MaToaAn).FirstOrDefault<DM_TOAAN>();
                if (oToaAn == null)
                {
                    oMsg.Status = "02";
                    oMsg.Message = "Mã tòa án không tồn tại";
                    return oMsg;
                }
                DM_CANBO oCheck = dt.DM_CANBO.Where(x => x.MACANBO.ToLower() == MaCanBo.ToLower()).FirstOrDefault<DM_CANBO>();
                if (oCheck != null)
                {
                    // nếu oCheck.MADONGBO là Null hoặc Empty thì thì gán luôn
                    if ((oCheck.MADONGBO + "") == "")
                    {
                        oCheck.MADONGBO = MaDongBo;
                        dt.SaveChanges();
                    }
                    if (oCheck.MADONGBO != MaDongBo)
                    {
                        oMsg.Status = "03";
                        oMsg.Message = "Mã cán bộ đã tồn tại";
                        return oMsg;
                    }
                }
                decimal ChucDanhID = 0, ChucVuID = 0;
                if (MaChucDanh != "")
                {
                    DM_DATAITEM oChucDanh = dt.DM_DATAITEM.Where(x => x.MADONGBO.ToLower() == MaChucDanh.ToLower()).FirstOrDefault<DM_DATAITEM>();
                    if (oChucDanh == null)
                    {
                        oMsg.Status = "05";
                        oMsg.Message = "Mã chức danh không tồn tại";
                        return oMsg;
                    }
                    else
                    {
                        ChucDanhID = oChucDanh.ID;
                    }
                }
                if (MaChucVu != "")
                {
                    DM_DATAITEM oChucVu = dt.DM_DATAITEM.Where(x => x.MADONGBO.ToLower() == MaChucVu.ToLower()).FirstOrDefault<DM_DATAITEM>();
                    if (oChucVu == null)
                    {
                        oMsg.Status = "06";
                        oMsg.Message = "Mã chức vụ không tồn tại";
                        return oMsg;
                    }
                    else
                    {
                        ChucVuID = oChucVu.ID;
                    }
                }
                decimal GioiTinh = value.GioiTinh, TinhTrang = value.TinhTrang;
                string DiaChi = (value.DiaChi + "").Trim(), SoCMND = (value.SoCMND + "").Trim(), NgaySinh = (value.NgaySinh + "").Trim(), DienThoai = (value.DienThoai + "").Trim(),
                    NgayNhanCongTac = (value.NgayNhanCongTac + "").Trim(), NgayBaiNhiem = (value.NgayBaiNhiem + "").Trim();

                string strValid = ValidateForm(MaCanBo, HoTen, GioiTinh, DiaChi, SoCMND, NgaySinh, DienThoai, QDBoNhiem, NgayBoNhiem, NgayKetThuc, NgayNhanCongTac, TinhTrang);
                if (strValid != "")
                {
                    oMsg.Status = "08";
                    oMsg.Message = "Lỗi khi đồng bộ. " + strValid;
                    return oMsg;
                }
                #endregion
                #region Update data
                bool isNew = false;
                DM_CANBO oCanBoCurent = dt.DM_CANBO.Where(x => x.MADONGBO.ToLower() == MaDongBo.ToLower()).FirstOrDefault<DM_CANBO>();
                if (oCanBoCurent == null)
                {
                    isNew = true;
                    oCanBoCurent = new DM_CANBO();
                    oCanBoCurent.MACANBO = MaCanBo;
                }
                oCanBoCurent.MADONGBO = MaDongBo;
                oCanBoCurent.TOAANID = oToaAn.ID;
                oCanBoCurent.HOTEN = HoTen;
                oCanBoCurent.SOCMND = SoCMND;
                oCanBoCurent.HIEULUC = TinhTrang;
                oCanBoCurent.CHUCDANHID = ChucDanhID;
                oCanBoCurent.CHUCVUID = ChucVuID;
                oCanBoCurent.DIACHI = DiaChi;
                oCanBoCurent.NGAYNHANCONGTAC = (String.IsNullOrEmpty(NgayNhanCongTac)) ? (DateTime?)null : DateTime.Parse(NgayNhanCongTac, cul, DateTimeStyles.NoCurrentDateDefault);
                oCanBoCurent.NGAYSINH = (String.IsNullOrEmpty(NgaySinh)) ? (DateTime?)null : DateTime.Parse(NgaySinh, cul, DateTimeStyles.NoCurrentDateDefault);
                oCanBoCurent.NGAYBONHIEM = (String.IsNullOrEmpty(NgayBoNhiem)) ? (DateTime?)null : DateTime.Parse(NgayBoNhiem, cul, DateTimeStyles.NoCurrentDateDefault);
                oCanBoCurent.NGAYKETTHUC = (String.IsNullOrEmpty(NgayKetThuc)) ? (DateTime?)null : DateTime.Parse(NgayKetThuc, cul, DateTimeStyles.NoCurrentDateDefault);
                oCanBoCurent.SODIENTHOAI = DienThoai;
                oCanBoCurent.QDBONHIEM = QDBoNhiem;
                oCanBoCurent.GIOITINH = GioiTinh;
                oCanBoCurent.NGAYBAINHIEM = (String.IsNullOrEmpty(NgayBaiNhiem)) ? (DateTime?)null : DateTime.Parse(NgayBaiNhiem, cul, DateTimeStyles.NoCurrentDateDefault);
                if (isNew)
                {
                    dt.DM_CANBO.Add(oCanBoCurent);
                }
                dt.SaveChanges();
                #endregion
                oMsg.Status = "00";
                oMsg.Message = "Đồng bộ thành công";
                return oMsg;
            }
            catch (Exception ex)
            {
                MessageResult oMsg = new MessageResult();
                oMsg.Status = "08";
                oMsg.Message = "Lỗi khi đồng bộ. " + ex.Message;
                return oMsg;
            }
        }

        // PUT: api/WS_ThongTinCanBo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WS_ThongTinCanBo/5
        public MessageResult Delete(string MaDongBo)
        {
            //09: Xóa thành công
            //10: Có dữ liệu liên quan không được xóa
            //11: Lỗi khi xóa dữ liệu
            try
            {
                MessageResult oMsg = new MessageResult();
                // Kiểm tra cán bộ muốn xóa có dữ liệu liên quan hay không. Nếu có không được xóa.
                string strMaDongBo = (MaDongBo + "").Trim();
                DM_CANBO oCanBoDel = dt.DM_CANBO.Where(x => x.MADONGBO.ToLower() == strMaDongBo.ToLower()).FirstOrDefault<DM_CANBO>();
                if (oCanBoDel != null)
                {
                    ADS_SOTHAM_HDXX oADS_ST = dt.ADS_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<ADS_SOTHAM_HDXX>();
                    if (oADS_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    ADS_PHUCTHAM_HDXX oADS_PT = dt.ADS_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<ADS_PHUCTHAM_HDXX>();
                    if (oADS_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHC_SOTHAM_HDXX oAHC_ST = dt.AHC_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHC_SOTHAM_HDXX>();
                    if (oAHC_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHC_PHUCTHAM_HDXX oAHC_PT = dt.AHC_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHC_PHUCTHAM_HDXX>();
                    if (oAHC_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHN_SOTHAM_HDXX oAHN_ST = dt.AHN_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHN_SOTHAM_HDXX>();
                    if (oAHN_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHN_PHUCTHAM_HDXX oAHN_PT = dt.AHN_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHN_PHUCTHAM_HDXX>();
                    if (oAHN_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHS_SOTHAM_HDXX oAHS_ST = dt.AHS_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHS_SOTHAM_HDXX>();
                    if (oAHS_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AHS_PHUCTHAM_HDXX oAHS_PT = dt.AHS_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AHS_PHUCTHAM_HDXX>();
                    if (oAHS_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AKT_SOTHAM_HDXX oAKT_ST = dt.AKT_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AKT_SOTHAM_HDXX>();
                    if (oAKT_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    AKT_PHUCTHAM_HDXX oAKT_PT = dt.AKT_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<AKT_PHUCTHAM_HDXX>();
                    if (oAHS_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    ALD_SOTHAM_HDXX oALD_ST = dt.ALD_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<ALD_SOTHAM_HDXX>();
                    if (oALD_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    ALD_PHUCTHAM_HDXX oALD_PT = dt.ALD_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<ALD_PHUCTHAM_HDXX>();
                    if (oALD_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    APS_SOTHAM_HDXX oAPS_ST = dt.APS_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<APS_SOTHAM_HDXX>();
                    if (oAPS_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    APS_PHUCTHAM_HDXX oAPS_PT = dt.APS_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<APS_PHUCTHAM_HDXX>();
                    if (oAPS_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    XLHC_SOTHAM_HDXX oXLHC_ST = dt.XLHC_SOTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<XLHC_SOTHAM_HDXX>();
                    if (oXLHC_ST != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    APS_PHUCTHAM_HDXX oXLHC_PT = dt.APS_PHUCTHAM_HDXX.Where(x => x.CANBOID == oCanBoDel.ID).FirstOrDefault<APS_PHUCTHAM_HDXX>();
                    if (oXLHC_PT != null)
                    {
                        oMsg.Status = "10";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa.";
                        return oMsg;
                    }
                    dt.DM_CANBO.Remove(oCanBoDel);
                    dt.SaveChanges();
                }
                oMsg.Status = "09";
                oMsg.Message = "Xóa thành công!";
                return oMsg;
            }
            catch (Exception ex)
            {
                MessageResult oMsg = new MessageResult();
                oMsg.Status = "10";
                oMsg.Message = "Lỗi khi xóa dữ liệu. " + ex.Message;
                return oMsg;
            }
        }
        private string ValidateForm(string MaCanBo, string HoTen, decimal GioiTinh, string DiaChi, string SoCMND, string strNgaySinh,
                                    string DienThoai, string QDBoNhiem, string strNgayBoNhiem, string strNgayKetThuc, string strNgayNhanCongTac,
                                    decimal TinhTrang)
        {
            if (MaCanBo.Length > 50)
            {
                return "Mã cán bộ không được quá 50 ký tự!";
            }
            if (HoTen.Length > 250)
            {
                return "Họ tên cán bộ không được quá 250 ký tự!";
            }
            if (GioiTinh < 0 || GioiTinh > 1)
            {
                return "Giới tính nhập kiểu số: 1 (Nam), 0 (Nữ).";
            }
            if (DiaChi.Length > 250)
            {
                return "Địa chỉ không được quá 250 ký tự!";
            }
            if (SoCMND.Length > 150)
            {
                return "Số CMND không được quá 50 ký tự!";
            }
            Regex rDate = new Regex(@"\d{2}/\d{2}/\d{4}");
            if (strNgaySinh != "" && !rDate.IsMatch(strNgaySinh))
            {
                return "Ngày sinh phải theo định dạng (dd/MM/yyyy). Ví dụ 22/09/2018.";
            }
            try
            {
                DateTime NgaySinh = DateTime.Parse(strNgaySinh, cul, DateTimeStyles.NoCurrentDateDefault);
                if ((DateTime.Now.Year - NgaySinh.Year) < 18)
                {
                    return "Cán bộ phải có tuổi lớn hơn 18.";
                }
            }
            catch
            {
                return "Ngày sinh phải theo định dạng (dd/MM/yyyy). Ví dụ 22/09/2018.";
            }
            if (DienThoai.Length > 50)
            {
                return "Số điện thoại không được quá 50 ký tự!";
            }
            if (QDBoNhiem.Length > 50)
            {
                return "Số Quyết định bổ nhiệm không được quá 50 ký tự!";
            }
            if (strNgayBoNhiem != "" && !rDate.IsMatch(strNgayBoNhiem))
            {
                return "Ngày bổ nhiệm phải theo định dạng (dd/MM/yyyy). Ví dụ 22/09/2018.";
            }
            DateTime NgayBoNhiem = DateTime.MinValue;
            try
            {
                NgayBoNhiem = DateTime.Parse(strNgayBoNhiem, cul, DateTimeStyles.NoCurrentDateDefault);
                if (DateTime.Compare(NgayBoNhiem, DateTime.Now) > 0)
                {
                    return "Ngày bổ nhiệm phải nhỏ hơn hoặc bằng ngày hiện tại.";
                }
            }
            catch { }
            if (strNgayKetThuc != "" && !rDate.IsMatch(strNgayKetThuc))
            {
                return "Ngày kết thúc phải theo định dạng (dd/MM/yyyy). Ví dụ 22/09/2018.";
            }
            DateTime NgayKetThuc = DateTime.MinValue;
            try
            {
                NgayKetThuc = DateTime.Parse(strNgayKetThuc, cul, DateTimeStyles.NoCurrentDateDefault);
                if (DateTime.Compare(NgayBoNhiem, NgayKetThuc) > 0)
                {
                    return "Ngày kết thúc phải lớn hơn hoặc bằng ngày bổ nhiệm.";
                }
            }
            catch { }

            if (strNgayNhanCongTac != "" && !rDate.IsMatch(strNgayNhanCongTac))
            {
                return "Ngày nhận công tác phải theo định dạng (dd/MM/yyyy). Ví dụ 22/09/2018.";
            }
            DateTime NgayNhanCongTac = DateTime.MinValue;
            try
            {
                NgayNhanCongTac = DateTime.Parse(strNgayNhanCongTac, cul, DateTimeStyles.NoCurrentDateDefault);
                if (DateTime.Compare(NgayBoNhiem, NgayNhanCongTac) > 0)
                {
                    return "Ngày nhận công tác phải lớn hơn hoặc bằng ngày bổ nhiệm.";
                }
            }
            catch { }
            if (TinhTrang < 0 || TinhTrang > 3)
            {
                return "Tình trạng cán bộ: 0 (Nghỉ công tác),1 (Đang công tác), 2 (Nghỉ phép), 3 (Bãi nhiệm).";
            }
            return "";
        }
    }
}
