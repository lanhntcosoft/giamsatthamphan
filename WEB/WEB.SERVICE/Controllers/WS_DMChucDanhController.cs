﻿using DAL.GSTP;
using Module.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEB.Service.Models;

namespace WEB.Service.Controllers
{
    public class WS_DMChucDanhController : ApiController
    {
        private GSTPContext dt = new GSTPContext();
        // GET: api/WS_DMChucDanh
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WS_DMChucDanh/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WS_DMChucDanh
        public MessageResult Post([FromBody]DMChucDanhDL value)
        {
            //00: Đồng bộ thành công
            //01: Dữ liệu không đầy đủ
            //02: Mã chức danh đã tồn tại
            //03: Tên chức danh đã tồn tại
            //04: Lỗi khi đồng bộ
            try
            {
                MessageResult oMsg = new MessageResult();
                string MaDongBo = (value.MaDongBo + "").Trim(), MaChucDanh = (value.MaChucDanh + "").Trim(), Ten = (value.Ten + "").Trim(), MoTa = (value.MoTa + "").Trim();
                #region Valid data
                if (MaDongBo == "" || MaChucDanh == "" || Ten == "")
                {
                    oMsg.Status = "01";
                    oMsg.Message = "Dữ liệu không đầy đủ. Bạn phải nhập Mã đồng bộ, Mã chức danh, Tên chức danh.";
                    return oMsg;
                }
                
                string strValid = ValidateForm(MaChucDanh, Ten, MoTa);
                if (strValid != "")
                {
                    oMsg.Status = "04";
                    oMsg.Message = "Lỗi khi đồng bộ. " + strValid;
                    return oMsg;
                }
                #endregion
                #region Update data
                bool isNew = false;
                DM_DATAITEM oChucDanhCurent = dt.DM_DATAITEM.Where(x => x.MADONGBO.ToLower() == MaDongBo.ToLower()).FirstOrDefault<DM_DATAITEM>();
                if (oChucDanhCurent == null)
                {
                    isNew = true;
                    oChucDanhCurent = new DM_DATAITEM();
                    oChucDanhCurent.MA = MaChucDanh;
                }
                oChucDanhCurent.MADONGBO = MaDongBo;
               
                oChucDanhCurent.TEN = Ten;
                oChucDanhCurent.MOTA = MoTa;
                DM_DATAGROUP Group = dt.DM_DATAGROUP.Where(x => x.MA == ENUM_DANHMUC.CHUCDANH).FirstOrDefault<DM_DATAGROUP>();
                if (Group != null)
                {
                    oChucDanhCurent.GROUPID = Group.ID;
                }
                else
                {
                    oChucDanhCurent.GROUPID = 0;
                }
                oChucDanhCurent.CAPCHAID = 0;
                oChucDanhCurent.SOCAP = 1;
                oChucDanhCurent.HIEULUC = 1;
                oChucDanhCurent.THUTU = 1;
                oChucDanhCurent.ARRTHUTU = "9001";
                if (isNew)
                {
                    dt.DM_DATAITEM.Add(oChucDanhCurent);
                }
                dt.SaveChanges();
                // Udate ARRSAPXEP
                oChucDanhCurent.ARRSAPXEP = "/" + oChucDanhCurent.ID;
                dt.SaveChanges();
                #endregion
                oMsg.Status = "00";
                oMsg.Message = "Đồng bộ thành công";
                return oMsg;
            }
            catch (Exception ex)
            {
                MessageResult oMsg = new MessageResult();
                oMsg.Status = "04";
                oMsg.Message = "Lỗi khi đồng bộ. " + ex.Message;
                return oMsg;
            }
        }

        // PUT: api/WS_DMChucDanh/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WS_DMChucDanh/5
        public MessageResult Delete(string MaDongBo)
        {
            //05: Xóa thành công
            //06: Có dữ liệu liên quan không được xóa
            //07: Lỗi khi xóa dữ liệu
            try
            {
                MessageResult oMsg = new MessageResult();
                // Kiểm tra chức danh muốn xóa có dữ liệu liên quan hay không. Nếu có không được xóa.
                string strMaDongBo = (MaDongBo + "").Trim();
                DM_DATAITEM oChucDanhDel = dt.DM_DATAITEM.Where(x => x.MADONGBO == strMaDongBo).FirstOrDefault<DM_DATAITEM>();
                if (oChucDanhDel != null)
                {
                    DM_CANBO oCanBo = dt.DM_CANBO.Where(x => x.CHUCDANHID == oChucDanhDel.ID).FirstOrDefault<DM_CANBO>();
                    if (oCanBo != null)
                    {
                        oMsg.Status = "06";
                        oMsg.Message = "Có dữ liệu liên quan không được xóa!";
                        return oMsg;
                    }
                    dt.DM_DATAITEM.Remove(oChucDanhDel);
                    dt.SaveChanges();
                }
                oMsg.Status = "05";
                oMsg.Message = "Xóa thành công!";
                return oMsg;
            }
            catch (Exception ex)
            {
                MessageResult oMsg = new MessageResult();
                oMsg.Status = "07";
                oMsg.Message = "Lỗi khi xóa dữ liệu. " + ex.Message;
                return oMsg;
            }
        }
        private string ValidateForm(string MaChucDanh, string Ten, string MoTa)
        {
            if (MaChucDanh.Length > 50)
            {
                return "Mã chức danh không được quá 50 ký tự!";
            }
            if (Ten.Length > 250)
            {
                return "Tên chức danh không được quá 250 ký tự!";
            }
            if (MoTa.Length > 250)
            {
                return "Mô tả chức danh không được quá 250 ký tự!";
            }
            return "";
        }
    }
}
